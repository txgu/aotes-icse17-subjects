import org.javelus.aotes.executor.*;
@Defined({"this.passivePorts.passivePorts","this.activeEnabled","this.passivePorts.passivePorts[*]","this.passivePorts","this.idleTime","this.passivePorts.reservedPorts","this.activeIpCheck","this.activeLocalPort"})
public class AOTES_org_apache_ftpserver_DataConnectionConfigurationFactory {
  @IM(clazz="org.apache.ftpserver.DataConnectionConfigurationFactory", name="<init>", desc="()V",definedOutput={"this.passivePorts.passivePorts","this.activeEnabled","this.passivePorts.passivePorts[*]","this.idleTime","this.passivePorts","this.passivePorts.reservedPorts","this.activeIpCheck","this.activeLocalPort"},delta={"this.passivePorts.passivePorts","this.activeEnabled","this.passivePorts.passivePorts[*]","this.idleTime","this.passivePorts","this.passivePorts.reservedPorts","this.activeIpCheck","this.activeLocalPort"})
  @DG(succ={"releasePassivePort3299","releasePassivePort3298","releasePassivePort3295"})
  static void _init_1299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", (int)1);
    vm.put("this", om.getTarget());
    vm.put("v1", om.popField(vm.get("this"), "activeLocalPort"));
    vm.put("v2", (int)0);
    vm.put("v3", om.popField(vm.get("this"), "idleTime"));
    vm.put("v4", (int)300);
    vm.put("v5", om.popField(vm.get("this"), "passivePorts"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.popField(vm.get("this"), "activeIpCheck"));
    vm.put("v8", om.popField(vm.get("this"), "activeEnabled"));
    vm.put("v9", om.popField(vm.get("v6"), "reservedPorts"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.popField(vm.get("v6"), "passivePorts"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getArrayLength(vm.get("v12")));
    vm.put("v14", om.getArrayLength(vm.get("v10")));
    vm.put("v15", om.popArrayElement(vm.get("v12"), vm.get("v2")));
    if (!em.eq(vm.get("v2"), (vm.get("v7")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v7")));
    if (!em.eq(vm.get("v0"), (vm.get("v8")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v8")));
    if (!em.eq(vm.get("v0"), (vm.get("v14")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v14")));
    if (!em.eq(vm.get("v0"), (vm.get("v13")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v13")));
    if (!em.eq(vm.get("v2"), (vm.get("v1")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v1")));
    if (!em.eq(vm.get("v4"), (vm.get("v3")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v3")));
  }
}
