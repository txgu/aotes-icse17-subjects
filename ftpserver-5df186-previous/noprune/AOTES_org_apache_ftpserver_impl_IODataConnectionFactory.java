import org.javelus.aotes.executor.*;
@Defined({"this.isZip","this.serverControlAddress","this.requestTime","this.secure"})
public class AOTES_org_apache_ftpserver_impl_IODataConnectionFactory {
  @IM(clazz="org.apache.ftpserver.impl.IODataConnectionFactory", name="setSecure", desc="(Z)V",definedOutput={"this.secure"},delta={"this.secure"})
  static void setSecure699(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "secure"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.ftpserver.impl.IODataConnectionFactory", name="closeDataConnection", desc="()V",definedOutput={"this.requestTime"},delta={"this.requestTime"})
  static void closeDataConnection999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "requestTime"));
    vm.put("v1", (long)0L);
    vm.put("v2", om.getField(vm.get("this"), "servSoc"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "dataSoc"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", null);
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v3"), vm.get("v6"))) e.abort(String.format("!((v3=%s) eq (v6=%s))", vm.get("v3"), vm.get("v6")));
    if (!em.eq(vm.get("v5"), vm.get("v6"))) e.abort(String.format("!((v5=%s) eq (v6=%s))", vm.get("v5"), vm.get("v6")));
  }
  @IM(clazz="org.apache.ftpserver.impl.IODataConnectionFactory", name="dispose", desc="()V",definedOutput={"this.requestTime"},delta={"this.requestTime"})
  static void dispose595(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "dataSoc"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "servSoc"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.popField(vm.get("this"), "requestTime"));
    vm.put("v5", (long)0L);
    vm.put("v6", null);
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    if (!em.eq(vm.get("v3"), vm.get("v6"))) e.abort(String.format("!((v3=%s) eq (v6=%s))", vm.get("v3"), vm.get("v6")));
    if (!em.eq(vm.get("v1"), vm.get("v6"))) e.abort(String.format("!((v1=%s) eq (v6=%s))", vm.get("v1"), vm.get("v6")));
  }
  @IM(clazz="org.apache.ftpserver.impl.IODataConnectionFactory", name="setServerControlAddress", desc="(Ljava/net/InetAddress;)V",definedOutput={"this.serverControlAddress"},delta={"this.serverControlAddress"})
  static void setServerControlAddress4099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "serverControlAddress"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.ftpserver.impl.IODataConnectionFactory", name="setZipMode", desc="(Z)V",definedOutput={"this.isZip"},delta={"this.isZip"})
  static void setZipMode1899(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "isZip"));
    vm.put("param0", vm.get("v0"));
  }
}
