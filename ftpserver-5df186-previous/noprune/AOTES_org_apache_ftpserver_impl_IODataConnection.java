import org.javelus.aotes.executor.*;
@Defined({"this.socket","this.session","this.factory"})
public class AOTES_org_apache_ftpserver_impl_IODataConnection {
  @IM(clazz="org.apache.ftpserver.impl.IODataConnection", name="<init>", desc="(Ljava/net/Socket;Lorg/apache/ftpserver/impl/FtpIoSession;Lorg/apache/ftpserver/impl/ServerDataConnectionFactory;)V",definedOutput={"this.socket","this.session","this.factory"},delta={"this.socket","this.session","this.factory"})
  static void _init_3999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "socket"));
    vm.put("v1", om.popField(vm.get("this"), "factory"));
    vm.put("v2", om.popField(vm.get("this"), "session"));
    vm.put("param1", vm.get("v2"));
    vm.put("param2", vm.get("v1"));
    vm.put("param0", vm.get("v0"));
  }
}
