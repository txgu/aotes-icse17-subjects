import org.javelus.aotes.executor.*;
@Defined({"this.logger","this.ready","this.result","this.otherListeners.modCount","this.lock","this.otherListeners.elementData","org.apache.sshd.common.future.DefaultSshFuture.CANCELED","this.otherListeners.elementData[*]","this.otherListeners","this.otherListeners.size","this.firstListener"})
public class AOTES_org_apache_sshd_common_future_DefaultSshFuture {
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="<init>", desc="(Ljava/lang/Object;)V",definedOutput={"this.logger","this.lock"},delta={"this.logger","this.lock"})
  static void _init_499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "lock"));
    vm.put("v1", om.popField(vm.get("this"), "logger"));
    vm.put("v2", null);
    vm.put("v3", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("this")));
    vm.put("v4", em.op("org.slf4j.LoggerFactory.getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;").eval(vm.get("v3")));
    if (!em.eq(vm.get("this"), (vm.get("v0")))) e.abort("Inconsistent value for \"this\": " + vm.get("this") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v4"), (vm.get("v1")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v1")));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.eq(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) eq (v2=%s))", vm.get("param0"), vm.get("v2")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="<init>", desc="(Ljava/lang/Object;)V",definedOutput={"this.logger","this.lock"},delta={"this.logger","this.lock"})
  static void _init_498(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "lock"));
    vm.put("v1", om.popField(vm.get("this"), "logger"));
    vm.put("v2", null);
    vm.put("v3", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("this")));
    vm.put("v4", em.op("org.slf4j.LoggerFactory.getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;").eval(vm.get("v3")));
    vm.put("param0", vm.get("v0"));
    if (!em.eq(vm.get("v4"), (vm.get("v1")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v1")));
    if (!em.ne(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) ne (v2=%s))", vm.get("param0"), vm.get("v2")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="addListener", desc="(Lorg/apache/sshd/common/future/SshFutureListener;)Lorg/apache/sshd/common/future/SshFuture;",revertedInput={"this.otherListeners.modCount","this.otherListeners.size"},definedOutput={"this.otherListeners.modCount","this.otherListeners.elementData[*]","this.otherListeners.size"},delta={"this.otherListeners.elementData[*]"})
  @DG(pred={"addListener2014"})
  static void addListener2032(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "lock"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "otherListeners"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "firstListener"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v3"), "modCount"));
    vm.put("v7", (int)1);
    vm.put("v8", om.getField(vm.get("v3"), "elementData"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("this"), "ready"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("v3"), "size"));
    vm.put("v13", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", (int)10);
    vm.put("v16", (int)0);
    vm.put("v17", null);
    vm.put("v18", om.getArrayLength(vm.get("v9")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", em.op("isub").eval(vm.get("v15"),vm.get("v19")));
    vm.put("v21", vm.get("v12"));
    vm.put("v22", em.op("isub").eval(vm.get("v21"),vm.get("v7")));
    om.revertField(vm.get("v3"), "size", vm.get("v22"));
    vm.put("v23", om.popArrayElement(vm.get("v9"), vm.get("v22")));
    vm.put("param0", vm.get("v23"));
    vm.put("v24", em.op("iadd").eval(vm.get("v22"),vm.get("v7")));
    vm.put("v25", vm.get("v6"));
    vm.put("v26", em.op("isub").eval(vm.get("v25"),vm.get("v7")));
    om.revertField(vm.get("v3"), "modCount", vm.get("v26"));
    if (!em.le(vm.get("v20"), vm.get("v16"))) e.abort(String.format("!((v20=%s) le (v16=%s))", vm.get("v20"), vm.get("v16")));
    if (!em.ge(vm.get("v15"), vm.get("v24"))) e.abort(String.format("!((v15=%s) ge (v24=%s))", vm.get("v15"), vm.get("v24")));
    if (!em.ne(vm.get("v5"), vm.get("v17"))) e.abort(String.format("!((v5=%s) ne (v17=%s))", vm.get("v5"), vm.get("v17")));
    if (!em.eq(vm.get("v11"), vm.get("v16"))) e.abort(String.format("!((v11=%s) eq (v16=%s))", vm.get("v11"), vm.get("v16")));
    if (!em.ne(vm.get("v3"), vm.get("v17"))) e.abort(String.format("!((v3=%s) ne (v17=%s))", vm.get("v3"), vm.get("v17")));
    if (!em.ne(vm.get("param0"), vm.get("v17"))) e.abort(String.format("!((param0=%s) ne (v17=%s))", vm.get("param0"), vm.get("v17")));
    if (!em.eq(vm.get("v9"), vm.get("v14"))) e.abort(String.format("!((v9=%s) eq (v14=%s))", vm.get("v9"), vm.get("v14")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="addListener", desc="(Lorg/apache/sshd/common/future/SshFutureListener;)Lorg/apache/sshd/common/future/SshFuture;",revertedInput={"this.otherListeners"},definedOutput={"this.otherListeners.modCount","this.otherListeners.elementData","this.otherListeners.elementData[*]","this.otherListeners","this.otherListeners.size"},delta={"this.otherListeners.modCount","this.otherListeners.elementData","this.otherListeners.elementData[*]","this.otherListeners.size"})
  @DG(succ={"addListener2058","removeListener1950","removeListener1934","addListener2021","addListener2032"})
  static void addListener2014(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "lock"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "ready"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", (int)0);
    vm.put("v5", om.getField(vm.get("this"), "otherListeners"));
    vm.put("v6", (int)1);
    vm.put("v7", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("this"), "firstListener"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", null);
    vm.put("v12", vm.get("v5"));
    vm.put("v13", om.popField(vm.get("v12"), "modCount"));
    vm.put("v14", om.popField(vm.get("v12"), "size"));
    vm.put("v15", om.popField(vm.get("v12"), "elementData"));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.popArrayElement(vm.get("v16"), vm.get("v4")));
    vm.put("param0", vm.get("v17"));
    vm.put("v18", om.getArrayLength(vm.get("v16")));
    if (!em.eq(vm.get("v6"), (vm.get("v13")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v13")));
    if (!em.eq(vm.get("v6"), (vm.get("v18")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v18")));
    if (!em.eq(vm.get("v6"), (vm.get("v14")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v14")));
    om.revertField(vm.get("this"), "otherListeners");
    vm.put("v19", om.getField(vm.get("this"), "otherListeners"));
    if (!em.ne(vm.get("v10"), vm.get("v11"))) e.abort(String.format("!((v10=%s) ne (v11=%s))", vm.get("v10"), vm.get("v11")));
    if (!em.eq(vm.get("v3"), vm.get("v4"))) e.abort(String.format("!((v3=%s) eq (v4=%s))", vm.get("v3"), vm.get("v4")));
    if (!em.ne(vm.get("v16"), vm.get("v8"))) e.abort(String.format("!((v16=%s) ne (v8=%s))", vm.get("v16"), vm.get("v8")));
    if (!em.eq(vm.get("v19"), vm.get("v11"))) e.abort(String.format("!((v19=%s) eq (v11=%s))", vm.get("v19"), vm.get("v11")));
    if (!em.ne(vm.get("param0"), vm.get("v11"))) e.abort(String.format("!((param0=%s) ne (v11=%s))", vm.get("param0"), vm.get("v11")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="addListener", desc="(Lorg/apache/sshd/common/future/SshFutureListener;)Lorg/apache/sshd/common/future/SshFuture;",revertedInput={"this.firstListener"},definedOutput={"this.firstListener"})
  static void addListener2062(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "ready"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "lock"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "firstListener"));
    vm.put("v5", null);
    vm.put("v6", (int)0);
    vm.put("param0", vm.get("v4"));
    om.revertField(vm.get("this"), "firstListener");
    vm.put("v7", om.getField(vm.get("this"), "firstListener"));
    if (!em.ne(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) ne (v5=%s))", vm.get("param0"), vm.get("v5")));
    if (!em.eq(vm.get("v7"), vm.get("v5"))) e.abort(String.format("!((v7=%s) eq (v5=%s))", vm.get("v7"), vm.get("v5")));
    if (!em.eq(vm.get("v1"), vm.get("v6"))) e.abort(String.format("!((v1=%s) eq (v6=%s))", vm.get("v1"), vm.get("v6")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="addListener", desc="(Lorg/apache/sshd/common/future/SshFutureListener;)Lorg/apache/sshd/common/future/SshFuture;",revertedInput={"this.otherListeners.modCount","this.otherListeners.size"},definedOutput={"this.otherListeners.modCount","this.otherListeners.elementData[*]","this.otherListeners.size"},delta={"this.otherListeners.elementData[*]"})
  @DG(pred={"addListener2014"})
  static void addListener2058(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "otherListeners"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("v1"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("v1"), "size"));
    vm.put("v5", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("v1"), "elementData"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("this"), "lock"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getField(vm.get("this"), "ready"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getField(vm.get("this"), "firstListener"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", (int)0);
    vm.put("v16", (int)10);
    vm.put("v17", null);
    vm.put("v18", om.getArrayLength(vm.get("v8")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", vm.get("v2"));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v3")));
    om.revertField(vm.get("v1"), "modCount", vm.get("v21"));
    vm.put("v22", vm.get("v4"));
    vm.put("v23", em.op("isub").eval(vm.get("v22"),vm.get("v3")));
    om.revertField(vm.get("v1"), "size", vm.get("v23"));
    vm.put("v24", em.op("iadd").eval(vm.get("v23"),vm.get("v3")));
    vm.put("v25", om.popArrayElement(vm.get("v8"), vm.get("v23")));
    vm.put("param0", vm.get("v25"));
    vm.put("v26", em.op("isub").eval(vm.get("v24"),vm.get("v19")));
    if (!em.ne(vm.get("param0"), vm.get("v17"))) e.abort(String.format("!((param0=%s) ne (v17=%s))", vm.get("param0"), vm.get("v17")));
    if (!em.eq(vm.get("v8"), vm.get("v6"))) e.abort(String.format("!((v8=%s) eq (v6=%s))", vm.get("v8"), vm.get("v6")));
    if (!em.eq(vm.get("v12"), vm.get("v15"))) e.abort(String.format("!((v12=%s) eq (v15=%s))", vm.get("v12"), vm.get("v15")));
    if (!em.ne(vm.get("v1"), vm.get("v17"))) e.abort(String.format("!((v1=%s) ne (v17=%s))", vm.get("v1"), vm.get("v17")));
    if (!em.le(vm.get("v26"), vm.get("v15"))) e.abort(String.format("!((v26=%s) le (v15=%s))", vm.get("v26"), vm.get("v15")));
    if (!em.lt(vm.get("v16"), vm.get("v24"))) e.abort(String.format("!((v16=%s) lt (v24=%s))", vm.get("v16"), vm.get("v24")));
    if (!em.ne(vm.get("v14"), vm.get("v17"))) e.abort(String.format("!((v14=%s) ne (v17=%s))", vm.get("v14"), vm.get("v17")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="addListener", desc="(Lorg/apache/sshd/common/future/SshFutureListener;)Lorg/apache/sshd/common/future/SshFuture;",revertedInput={"this.otherListeners.modCount","this.otherListeners.size"},definedOutput={"this.otherListeners.modCount","this.otherListeners.elementData[*]","this.otherListeners.size"},delta={"this.otherListeners.elementData[*]"})
  @DG(pred={"addListener2014"})
  static void addListener2021(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", om.getField(vm.get("this"), "lock"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "otherListeners"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("v4"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("v4"), "modCount"));
    vm.put("v10", om.getArrayLength(vm.get("v6")));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("this"), "firstListener"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("this"), "ready"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", (int)0);
    vm.put("v17", null);
    vm.put("v18", om.getField(vm.get("v4"), "size"));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", em.op("isub").eval(vm.get("v19"),vm.get("v0")));
    om.revertField(vm.get("v4"), "size", vm.get("v20"));
    vm.put("v21", om.popArrayElement(vm.get("v6"), vm.get("v20")));
    vm.put("param0", vm.get("v21"));
    vm.put("v22", em.op("iadd").eval(vm.get("v20"),vm.get("v0")));
    vm.put("v23", em.op("isub").eval(vm.get("v22"),vm.get("v11")));
    vm.put("v24", vm.get("v9"));
    vm.put("v25", em.op("isub").eval(vm.get("v24"),vm.get("v0")));
    om.revertField(vm.get("v4"), "modCount", vm.get("v25"));
    if (!em.ne(vm.get("v6"), vm.get("v8"))) e.abort(String.format("!((v6=%s) ne (v8=%s))", vm.get("v6"), vm.get("v8")));
    if (!em.eq(vm.get("v15"), vm.get("v16"))) e.abort(String.format("!((v15=%s) eq (v16=%s))", vm.get("v15"), vm.get("v16")));
    if (!em.ne(vm.get("param0"), vm.get("v17"))) e.abort(String.format("!((param0=%s) ne (v17=%s))", vm.get("param0"), vm.get("v17")));
    if (!em.le(vm.get("v23"), vm.get("v16"))) e.abort(String.format("!((v23=%s) le (v16=%s))", vm.get("v23"), vm.get("v16")));
    if (!em.ne(vm.get("v4"), vm.get("v17"))) e.abort(String.format("!((v4=%s) ne (v17=%s))", vm.get("v4"), vm.get("v17")));
    if (!em.ne(vm.get("v13"), vm.get("v17"))) e.abort(String.format("!((v13=%s) ne (v17=%s))", vm.get("v13"), vm.get("v17")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="setValue", desc="(Ljava/lang/Object;)V",revertedInput={"this.ready"},definedOutput={"this.ready","this.result"},delta={"this.result"})
  static void setValue898(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "ready"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "firstListener"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.popField(vm.get("this"), "result"));
    vm.put("v5", om.getField(vm.get("this"), "lock"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", null);
    vm.put("v8", (int)0);
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    vm.put("param0", vm.get("v4"));
    om.revertField(vm.get("this"), "ready");
    vm.put("v9", om.getField(vm.get("this"), "ready"));
    if (!em.eq(vm.get("v9"), vm.get("v8"))) e.abort(String.format("!((v9=%s) eq (v8=%s))", vm.get("v9"), vm.get("v8")));
    if (!em.eq(vm.get("v3"), vm.get("v7"))) e.abort(String.format("!((v3=%s) eq (v7=%s))", vm.get("v3"), vm.get("v7")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="removeListener", desc="(Lorg/apache/sshd/common/future/SshFutureListener;)Lorg/apache/sshd/common/future/SshFuture;",revertedInput={"this.firstListener"},definedOutput={"this.firstListener"})
  static void removeListener1978(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "firstListener"));
    vm.put("v1", null);
    vm.put("v2", om.getField(vm.get("this"), "lock"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "ready"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "otherListeners"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)0);
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    vm.put("param0", om.newDefaultValue("org.apache.sshd.common.future.SshFutureListener"));
    om.revertField(vm.get("this"), "firstListener");
    vm.put("v9", om.getField(vm.get("this"), "firstListener"));
    if (!em.eq(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) eq (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.ne(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) ne (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.eq(vm.get("v7"), vm.get("v1"))) e.abort(String.format("!((v7=%s) eq (v1=%s))", vm.get("v7"), vm.get("v1")));
    if (!em.eq(vm.get("v5"), vm.get("v8"))) e.abort(String.format("!((v5=%s) eq (v8=%s))", vm.get("v5"), vm.get("v8")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="removeListener", desc="(Lorg/apache/sshd/common/future/SshFutureListener;)Lorg/apache/sshd/common/future/SshFuture;",revertedInput={"this.firstListener"},definedOutput={"this.firstListener"})
  @DG(pred={"addListener2014"})
  static void removeListener1950(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "lock"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "firstListener"));
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("this"), "otherListeners"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "ready"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)0);
    vm.put("v9", om.getField(vm.get("v5"), "size"));
    vm.put("v10", vm.get("v9"));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    vm.put("param0", om.newDefaultValue("org.apache.sshd.common.future.SshFutureListener"));
    om.revertField(vm.get("this"), "firstListener");
    vm.put("v11", om.getField(vm.get("this"), "firstListener"));
    if (!em.eq(vm.get("v10"), vm.get("v8"))) e.abort(String.format("!((v10=%s) eq (v8=%s))", vm.get("v10"), vm.get("v8")));
    if (!em.ne(vm.get("v5"), vm.get("v3"))) e.abort(String.format("!((v5=%s) ne (v3=%s))", vm.get("v5"), vm.get("v3")));
    if (!em.eq(vm.get("v7"), vm.get("v8"))) e.abort(String.format("!((v7=%s) eq (v8=%s))", vm.get("v7"), vm.get("v8")));
    if (!em.ne(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) ne (v3=%s))", vm.get("param0"), vm.get("v3")));
    if (!em.eq(vm.get("param0"), vm.get("v11"))) e.abort(String.format("!((param0=%s) eq (v11=%s))", vm.get("param0"), vm.get("v11")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="removeListener", desc="(Lorg/apache/sshd/common/future/SshFutureListener;)Lorg/apache/sshd/common/future/SshFuture;",revertedInput={"this.otherListeners.modCount","this.otherListeners.size"},definedOutput={"this.otherListeners.modCount","this.otherListeners.elementData[*]","this.otherListeners.size"},delta={"this.otherListeners.elementData[*]"})
  @DG(pred={"addListener2014"})
  static void removeListener1934(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "firstListener"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", (int)1);
    vm.put("v4", null);
    vm.put("v5", om.getField(vm.get("this"), "ready"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "otherListeners"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("this"), "lock"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getField(vm.get("v8"), "modCount"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getField(vm.get("v8"), "size"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.getField(vm.get("v8"), "elementData"));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getArrayElement(vm.get("v16"), vm.get("v0")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.popArrayElement(vm.get("v16"), vm.get("v14")));
    vm.put("v20", em.op("iadd").eval(vm.get("v14"),vm.get("v3")));
    om.revertField(vm.get("v8"), "size", vm.get("v20"));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v0")));
    vm.put("v22", em.op("isub").eval(vm.get("v21"),vm.get("v3")));
    vm.put("v23", em.op("isub").eval(vm.get("v12"),vm.get("v3")));
    om.revertField(vm.get("v8"), "modCount", vm.get("v23"));
    vm.put("param0", om.newDefaultValue("org.apache.sshd.common.future.SshFutureListener"));
    if (!em.ne(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) ne (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.le(vm.get("v22"), vm.get("v0"))) e.abort(String.format("!((v22=%s) le (v0=%s))", vm.get("v22"), vm.get("v0")));
    if (!em.eq(vm.get("v6"), vm.get("v0"))) e.abort(String.format("!((v6=%s) eq (v0=%s))", vm.get("v6"), vm.get("v0")));
    if (!em.ne(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) ne (v2=%s))", vm.get("param0"), vm.get("v2")));
    if (!em.eq(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) eq (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.ne(vm.get("v8"), vm.get("v4"))) e.abort(String.format("!((v8=%s) ne (v4=%s))", vm.get("v8"), vm.get("v4")));
    if (!em.lt(vm.get("v0"), vm.get("v20"))) e.abort(String.format("!((v0=%s) lt (v20=%s))", vm.get("v0"), vm.get("v20")));
    if (!em.eq(vm.get("v18"), vm.get("v4"))) e.abort(String.format("!((v18=%s) eq (v4=%s))", vm.get("v18"), vm.get("v4")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="<clinit>", desc="()V",definedOutput={"org.apache.sshd.common.future.DefaultSshFuture.CANCELED"},delta={"org.apache.sshd.common.future.DefaultSshFuture.CANCELED"})
  static void _clinit_1699(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", om.popStatic("org.apache.sshd.common.future.DefaultSshFuture.CANCELED"));
    vm.put("v1", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="cancel", desc="()V",revertedInput={"this.ready"},definedOutput={"this.ready","this.result"},delta={"this.result"})
  static void cancel797(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "firstListener"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.popField(vm.get("this"), "result"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "lock"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("org.apache.sshd.common.future.DefaultSshFuture.CANCELED"));
    vm.put("v7", om.getField(vm.get("this"), "ready"));
    vm.put("v8", (int)1);
    vm.put("v9", null);
    vm.put("v10", (int)0);
    if (!em.eq(vm.get("v8"), (vm.get("v7")))) e.abort("Inconsistent value for \"v8\": " + vm.get("v8") + " ne " + (vm.get("v7")));
    if (!em.eq(vm.get("v3"), (vm.get("v6")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v6")));
    om.revertField(vm.get("this"), "ready");
    vm.put("v11", om.getField(vm.get("this"), "ready"));
    if (!em.eq(vm.get("v1"), vm.get("v9"))) e.abort(String.format("!((v1=%s) eq (v9=%s))", vm.get("v1"), vm.get("v9")));
    if (!em.eq(vm.get("v11"), vm.get("v10"))) e.abort(String.format("!((v11=%s) eq (v10=%s))", vm.get("v11"), vm.get("v10")));
  }
}
