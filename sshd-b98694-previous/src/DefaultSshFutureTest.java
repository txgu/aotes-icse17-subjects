import org.apache.sshd.common.future.DefaultSshFuture;
import org.apache.sshd.common.future.SshFuture;
import org.apache.sshd.common.future.SshFutureListener;
import org.javelus.aotes.asm.Options;
import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;


@SuppressWarnings({ "rawtypes", "unchecked" })
public class DefaultSshFutureTest {

    static class DummyListener<T extends SshFuture<T>> implements SshFutureListener<DefaultSshFuture<T>> {

        private String name;

        public DummyListener(String name) {
            this.name = name;
        }

        public String toString() {
            return name;
        }

        @Override
        public void operationComplete(DefaultSshFuture<T> arg0) {
        }
    }

    public static void test() {
        Options.setDebug("executor", "true");
        DefaultSshFuture dsf = new DefaultSshFuture(null);
        DummyListener l1 = new DummyListener("listener1");
        DummyListener l2 = new DummyListener("listener2");
        DummyListener l3 = new DummyListener("listener3");

        dsf.addListener(l1);
        dsf.addListener(l2);
        dsf.addListener(l3);
        //dsf.removeListener(l2);
        System.out.println(org.javelus.aotes.executor.java.JavaExecutorDriver.transform(dsf));
    }

    public static class Test0 extends AOTESTestCase<DefaultSshFuture> {

        @Override
        protected DefaultSshFuture buildInternal() {
            DefaultSshFuture future = new DefaultSshFuture(null);
            return future;
        }

        @Override
        protected void checkInternal(DefaultSshFuture updated) throws Exception {
            assertFieldEquals(updated, "listeners", null);
        }
    }

    public static class Test1 extends AOTESTestCase<DefaultSshFuture> {

        DummyListener listener1;
        
        @Override
        protected DefaultSshFuture buildInternal() {
            DefaultSshFuture future = new DefaultSshFuture(null);
            listener1 = new DummyListener("Listener1");
            future.addListener(listener1);

            return future;
        }

        @Override
        protected void checkInternal(DefaultSshFuture updated) throws Exception {
            assertFieldEquals(updated, "listeners", listener1);
        }
    }

    public static class Test2 extends AOTESTestCase<DefaultSshFuture> {

        DummyListener[] listeners;
        
        @Override
        protected DefaultSshFuture buildInternal() {
            DefaultSshFuture future = new DefaultSshFuture(null);
            listeners = new DummyListener[] {
                    new DummyListener("Listener1"),
                    new DummyListener("Listener2")
            };
            future.addListener(listeners[0]);
            future.addListener(listeners[1]);

            return future;
        }

        @Override
        protected void checkInternal(DefaultSshFuture updated) throws Exception {
            assertArrayEquals(readField(updated, "listeners"), listeners);
        }
    }

    public static class Test3 extends AOTESTestCase<DefaultSshFuture> {

        DummyListener[] listeners;

        @Override
        protected DefaultSshFuture buildInternal() {
            DefaultSshFuture future = new DefaultSshFuture(null);
            listeners = new DummyListener[] {
                    new DummyListener("Listener1"),
                    new DummyListener("Listener2"),
                    new DummyListener("Listener3")
            };
            future.addListener(listeners[0]);
            future.addListener(listeners[1]);
            future.addListener(listeners[2]);
            return future;
        }

        @Override
        protected void checkInternal(DefaultSshFuture updated) throws Exception {
            assertArrayEquals(readField(updated, "listeners"), listeners);
        }
    }

    public static class Test4 extends AOTESTestCase<DefaultSshFuture> {

        @Override
        protected DefaultSshFuture buildInternal() {
            DefaultSshFuture future = new DefaultSshFuture(null);
            DummyListener dl1 = new DummyListener("Listener1");
            future.addListener(dl1);
            future.removeListener(dl1);
            return future;
        }

        @Override
        protected void checkInternal(DefaultSshFuture updated) throws Exception {
            assertFieldEquals(updated, "listeners", null);
        }
    }

    public static class Test5 extends AOTESTestCase<DefaultSshFuture> {

        DummyListener listener;
        @Override
        protected DefaultSshFuture buildInternal() {
            DefaultSshFuture future = new DefaultSshFuture(null);
            DummyListener dl1 = new DummyListener("Listener1");
            future.addListener(dl1);
            future.removeListener(dl1);
            listener = new DummyListener("Listener2");
            future.addListener(listener);

            return future;
        }

        @Override
        protected void checkInternal(DefaultSshFuture updated) throws Exception {
            assertFieldEquals(updated, "listeners", listener);
        }
    }

    public static class Test6 extends AOTESTestCase<DefaultSshFuture> {

        DummyListener[] listeners;

        @Override
        protected DefaultSshFuture buildInternal() {
            DefaultSshFuture future = new DefaultSshFuture(null);
            DummyListener dl1 = new DummyListener("Listener1");
            future.addListener(dl1);
            DummyListener dl2 = new DummyListener("Listener2");
            future.addListener(dl2);
            future.removeListener(dl1);
            future.removeListener(dl2);
            future.addListener(dl2);
            future.addListener(dl1);

            listeners = new DummyListener[] {
                    dl2, dl1
            };
            
            return future;
        }

        @Override
        protected void checkInternal(DefaultSshFuture updated) throws Exception {
            Object[] newListeners = (Object[]) readField(updated, "listeners");
            outer: for (Object listener : listeners) {
                for (Object newListener : newListeners) {
                    if (listener == newListener) {
                        continue outer;
                    }
                }
                assertTrue(false);
            }
        }
    }

    public static void main(String[] args) throws Exception {
        // Options.setDebug("executor", true);
        AOTESTestRunner.run(
                Test0.class,
                Test1.class,
                Test2.class,
                Test3.class,
                Test4.class,
                Test5.class,
                Test6.class
                );
    }

}
