
import java.util.Collection;

import org.apache.sshd.server.SshFile;
import org.apache.sshd.server.filesystem.NativeFileSystemView;
import org.apache.sshd.server.sftp.SftpSubsystem;
import org.javelus.aotes.asm.Options;
import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;

public class DirectoryHandleTest extends SftpSubsystem {

    public static void test() {
        SftpSubsystem.DirectoryHandle t = new SftpSubsystem.DirectoryHandle(null);
        System.out.println(org.javelus.aotes.executor.java.JavaExecutorDriver.transform(t));
    }

    public static class Test1 extends AOTESTestCase<SftpSubsystem.DirectoryHandle> {

        SshFile file;

        @Override
        protected DirectoryHandle buildInternal() {
            NativeFileSystemView view = new NativeFileSystemView(".", false);
            file = view.getFile(".");
            SftpSubsystem.DirectoryHandle t = new SftpSubsystem.DirectoryHandle(file);
            return t;
        }

        @Override
        protected void checkInternal(DirectoryHandle updated) throws Exception {
            Collection<?> newFileList = (Collection<?>) readMixField(updated, "fileList");
            outer: for (Object file : file.listSshFiles()) {
                for (Object newFile : newFileList) {
                    if (file.equals(newFile)) {
                        continue outer;
                    }
                }
                assertTrue(false);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        Options.setDebug("executor", false);
        AOTESTestRunner.run(
                Test1.class
                );
    }
}
