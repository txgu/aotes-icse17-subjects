#! /usr/bin/python

import os, sys, subprocess, traceback, StringIO
import shutil

try:
    import ConfigParser as CP
except:
    import configparser as CP

if os.name == 'nt':
    EXE = 'run.bat'
else:
    EXE = 'run.sh'

DIR=os.path.abspath(os.path.dirname(__file__))
AOTES_DIST = os.path.abspath(os.getenv('AOTES_DIST',os.path.join(DIR, '../aotes-asm/dist'))
AOTES_BIN=os.path.join(AOTES_DIST, EXE)

JAVA='java'


def collect_jar_in(root):
    if not root:
        return ''
    jars = []
    for dirpath, dirnames, filenames in os.walk(root):
        for filename in filenames:
            if filename.endswith('.jar'):
                jars.append(os.path.join(dirpath, filename))

    return os.pathsep.join(jars)


def generate_class_path(*args):
    return os.pathsep.join(args)

def run(proj_path):
    proj_path = os.path.abspath(proj_path)
    bin_dir = os.path.join(proj_path, 'bin')

    aotes_class_path = collect_jar_in(AOTES_DIST)
    class_path = generate_class_path(bin_dir, aotes_class_path)

    cmd_base = [JAVA, '-cp', class_path, 'Main']

    logfile_name = os.path.join(proj_path, 'main.log')
    package_name = 'java.util.'
    classes = ['ArrayList', 'LinkedList', 'Vector', 'HashMap', 'Hashtable']
    iteration = 10
    totals = range(0, 100, 10)
    with open(logfile_name, 'w') as logfile:
        for cls in classes:
            class_name = package_name + cls;
            for total in totals:
                cmd = cmd_base + [class_name, str(total), str(iteration)]
                print(' '.join(cmd))
                run_cmd(cmd, logfile)


def run_cmd(cmd, logfile=None):
    if logfile:
        subprocess.check_call(cmd, stdout=logfile, stderr=logfile)
    else:
        subprocess.check_call(cmd)

if __name__ == '__main__':
    try:
        exit(run(sys.argv[1]))
    except Exception as e:
        traceback.print_exc(e)
        exit(1)
