import org.apache.catalina.deploy.ApplicationListener;
import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;


public class ApplicationListenerTest {

    public static class Test1 extends AOTESTestCase<ApplicationListener> {

        @Override
        protected ApplicationListener buildInternal() {
            ApplicationListener al = new ApplicationListener("AppliactionListenerTest", true);
            return al;
        }

        @Override
        protected void checkInternal(ApplicationListener updated) throws Exception {
            assertFieldEquals(updated, "allowedPluggability", true);
        }
        
    }
    
    public static class Test2 extends AOTESTestCase<ApplicationListener> {

        @Override
        protected ApplicationListener buildInternal() {
            ApplicationListener al = new ApplicationListener("AppliactionListenerTest", false);
            return al;
        }

        @Override
        protected void checkInternal(ApplicationListener updated) throws Exception {
            assertFieldEquals(updated, "allowedPluggability", false);
        }
        
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        AOTESTestRunner.run(
                Test1.class,
                Test2.class
                );

    }

}
