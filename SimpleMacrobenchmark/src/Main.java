import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.javelus.aotes.asm.Options;
import org.javelus.aotes.executor.InverseMutatorGraph;
import org.javelus.aotes.executor.java.img.InverseMutatorGraphImpl;
import org.javelus.aotes.executor.java.mov.InversedMethodSequenceElement;
import org.javelus.aotes.executor.java.mov.MethodSequenceElement;
import org.javelus.aotes.executor.java.mov.SimpleMethodSequenceObject;

@SuppressWarnings("unchecked")
public class Main {
    static class Pair {
        final Object target;
        final Object cloned;

        public Pair(Object target, Object cloned) {
            this.target = target;
            this.cloned = cloned;
        }
    }
    public static Pair build(Class<?> collectionType, int totalElements) throws Exception {
        if (List.class.isAssignableFrom(collectionType)) {
            List<String> collection1 = (List<String>) collectionType.newInstance();
            List<String> collection2 = (List<String>) collectionType.newInstance();
            for (int i = 0; i < totalElements; i++) {
                String ei = "e" + i;
                collection1.add(ei);
                collection2.add(ei);
            }
            return new Pair(collection1, collection2);
        } else if (Map.class.isAssignableFrom(collectionType)) {
            Map<String, String> map1 = (Map<String, String>) collectionType.newInstance();
            Map<String, String> map2 = (Map<String, String>) collectionType.newInstance();
            for (int i = 0; i < totalElements; i++) {
                String ei = "e" + i;
                map1.put(ei, ei);
                map2.put(ei, ei);
            }
            return new Pair(map1, map2);
        } else {
            throw new RuntimeException("Unsupported type: " + collectionType.getName());
        }
    }

    public static boolean run(Class<?> collectionType, int totalElements) throws Exception {
        Pair pair = build(collectionType, totalElements);
        SimpleMethodSequenceObject mso = (SimpleMethodSequenceObject)
                org.javelus.aotes.executor.java.JavaExecutorDriver.transform(pair.target, true);
        Object check = org.javelus.aotes.executor.java.JavaExecutorDriver.replay(mso);
        return pair.cloned.equals(check);
    }

    private static int getIMGSize(Class<?> clazz) throws Exception {
        String mutatorName = "AOTES_" + clazz.getName().replace('.', '_');

        Class<?> mutatorClass = null;

        try {
            mutatorClass = Class.forName(mutatorName);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Fail to load class " + mutatorName);
        }
        InverseMutatorGraph graph = InverseMutatorGraphImpl.build(mutatorClass.getDeclaredMethods());
        return graph.getInverseMutators().size();
    }
    
    public static void main(String [] argv) throws Exception {
        Options.setDebug("executor", false);
        Class<?> collectionType = Class.forName(argv[0]);
        int totalElements = Integer.parseInt(argv[1]);
        int iteration = Integer.parseInt(argv[2]);
        int imgSize = getIMGSize(collectionType);
        System.setProperty("aotes.synthesis.bound", String.valueOf(1000));
        for (int i = 1; i < iteration; i++) {
            System.out.format("Microbenchmark for %s with %d elements, warmup %d/%d...\n",
                    collectionType.getName(), totalElements, i, iteration);
            boolean result = run(collectionType, totalElements);
            System.out.format("Result of iteration %d: %s\n", i, (result ? "passed" : "failed"));
        }

        System.out.format("Microbenchmark for %s with %d elements\n",
                collectionType.getName(), totalElements);
        Pair pair = build(collectionType, totalElements);
        long begin = System.nanoTime();
        SimpleMethodSequenceObject mso = (SimpleMethodSequenceObject)
                org.javelus.aotes.executor.java.JavaExecutorDriver.transform(pair.target, true);
        long end = System.nanoTime();
        long timeInMillis = TimeUnit.NANOSECONDS.toMillis(end - begin);
        Object check = org.javelus.aotes.executor.java.JavaExecutorDriver.replay(mso);
        boolean result = pair.cloned.equals(check);
        System.out.format("Result noprune: %s\n", (result ? "passed" : "failed"));
        System.out.format("##: noprune\t%20s\t%d\t%d\t%d\t%d\t%d\n",
                collectionType.getName(), (result ? 1 : 0), imgSize, totalElements, mso.getSequence().length, timeInMillis);
        
        Set<String> whiteList = new HashSet<String>();
        for (MethodSequenceElement me : mso.getSequence()) {
            InversedMethodSequenceElement ime = (InversedMethodSequenceElement) me;
            whiteList.add(ime.getInverseMutator().getUniqueName());
        }
        String whiteListString = join(",", whiteList.toArray());
        System.setProperty("aotes.executor.whitelist", whiteListString);
        
        pair = build(collectionType, totalElements);
        begin = System.nanoTime();
        mso = (SimpleMethodSequenceObject)
                org.javelus.aotes.executor.java.JavaExecutorDriver.transform(pair.target, true);
        end = System.nanoTime();
        timeInMillis = TimeUnit.NANOSECONDS.toMillis(end - begin);

        check = org.javelus.aotes.executor.java.JavaExecutorDriver.replay(mso);
        result = pair.cloned.equals(check);
        System.out.format("Result   prune: %s\n", (result ? "passed" : "failed"));
        System.out.format("##: prune\t%20s\t%d\t%d\t%d\t%d\t%d\n",
                collectionType.getName(), (result ? 1 : 0), whiteList.size(), totalElements, mso.getSequence().length, timeInMillis);
        
    }
    
    private static String join(String sep, Object [] objects) {
        if (objects.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        if (objects.length == 1) {
            return objects[0].toString();
        }
        sb.append(objects[0]);
        for (int i = 1; i < objects.length; i++) {
            sb.append(sep);
            sb.append(objects[i]);
        }
        return sb.toString();
    }
}
