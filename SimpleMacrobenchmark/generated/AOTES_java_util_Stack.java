import org.javelus.aotes.executor.*;
@Defined({"this.modCount","this.elementData","this.elementData[*]","this.elementCount","this.capacityIncrement"})
public class AOTES_java_util_Stack {
  @IM(clazz="java.util.Vector", name="removeElement", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void removeElement4824(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)2);
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "elementCount"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayElement(vm.get("v7"), vm.get("v1")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)3);
    vm.put("v11", null);
    vm.put("v12", om.getArrayElement(vm.get("v7"), vm.get("v0")));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getArrayElement(vm.get("v7"), vm.get("v2")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", vm.get("v5"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.ne(vm.get("v9"), vm.get("v11"))) e.abort(String.format("!((v9=%s) ne (v11=%s))", vm.get("v9"), vm.get("v11")));
    if (!em.ge(vm.get("v10"), vm.get("v4"))) e.abort(String.format("!((v10=%s) ge (v4=%s))", vm.get("v10"), vm.get("v4")));
    if (!em.ne(vm.get("v13"), vm.get("v11"))) e.abort(String.format("!((v13=%s) ne (v11=%s))", vm.get("v13"), vm.get("v11")));
    if (!em.lt(vm.get("v1"), vm.get("v4"))) e.abort(String.format("!((v1=%s) lt (v4=%s))", vm.get("v1"), vm.get("v4")));
    if (!em.eq(vm.get("param0"), vm.get("v11"))) e.abort(String.format("!((param0=%s) eq (v11=%s))", vm.get("param0"), vm.get("v11")));
    if (!em.lt(vm.get("v0"), vm.get("v4"))) e.abort(String.format("!((v0=%s) lt (v4=%s))", vm.get("v0"), vm.get("v4")));
    if (!em.lt(vm.get("v2"), vm.get("v4"))) e.abort(String.format("!((v2=%s) lt (v4=%s))", vm.get("v2"), vm.get("v4")));
    if (!em.ne(vm.get("v15"), vm.get("v11"))) e.abort(String.format("!((v15=%s) ne (v11=%s))", vm.get("v15"), vm.get("v11")));
  }
  @IM(clazz="java.util.Vector", name="removeAllElements", desc="()V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void removeAllElements4696(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", null);
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.popArrayElement(vm.get("v6"), vm.get("v1")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    vm.put("v8", vm.get("v2"));
    vm.put("v9", em.op("isub").eval(vm.get("v8"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v9"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v10", om.getField(vm.get("this"), "elementCount"));
    if (!em.lt(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) lt (v10=%s))", vm.get("v1"), vm.get("v10")));
    if (!em.ge(vm.get("v3"), vm.get("v10"))) e.abort(String.format("!((v3=%s) ge (v10=%s))", vm.get("v3"), vm.get("v10")));
  }
  @IM(clazz="java.util.Vector", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void add198(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayLength(vm.get("v4")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", (int)2);
    vm.put("v9", vm.get("v2"));
    vm.put("v10", em.op("isub").eval(vm.get("v9"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v10"));
    vm.put("v11", em.op("iadd").eval(vm.get("v10"),vm.get("v0")));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v6")));
    vm.put("v13", vm.get("v7"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    vm.put("v15", om.guessArrayIndex(vm.get("v4")));
    vm.put("param0", vm.get("v15"));
    vm.put("v16", om.popArrayElement(vm.get("v4"), vm.get("param0")));
    vm.put("param1", vm.get("v16"));
    vm.put("v17", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v18", om.getArrayElement(vm.get("v4"), vm.get("v17")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", em.op("isub").eval(vm.get("v10"),vm.get("param0")));
    vm.put("v21", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v22", om.getArrayElement(vm.get("v4"), vm.get("v21")));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v25", em.op("iadd").eval(vm.get("v24"),vm.get("v0")));
    vm.put("v26", om.popArrayElement(vm.get("v4"), vm.get("v25")));
    vm.put("v27", em.op("iadd").eval(vm.get("v24"),vm.get("v1")));
    vm.put("v28", om.popArrayElement(vm.get("v4"), vm.get("v27")));
    if (!em.ge(vm.get("v8"), vm.get("v20"))) e.abort(String.format("!((v8=%s) ge (v20=%s))", vm.get("v8"), vm.get("v20")));
    if (!em.lt(vm.get("v1"), vm.get("v20"))) e.abort(String.format("!((v1=%s) lt (v20=%s))", vm.get("v1"), vm.get("v20")));
    if (!em.le(vm.get("v12"), vm.get("v1"))) e.abort(String.format("!((v12=%s) le (v1=%s))", vm.get("v12"), vm.get("v1")));
    if (!em.le(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) le (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.lt(vm.get("v0"), vm.get("v20"))) e.abort(String.format("!((v0=%s) lt (v20=%s))", vm.get("v0"), vm.get("v20")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void remove3896(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", null);
    vm.put("v5", om.getField(vm.get("this"), "elementCount"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", om.popArrayElement(vm.get("v3"), vm.get("v6")));
    vm.put("v9", em.op("iadd").eval(vm.get("v6"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v9"));
    vm.put("v10", vm.get("v7"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    vm.put("v12", om.guessArrayIndex(vm.get("v3")));
    vm.put("param0", vm.get("v12"));
    vm.put("v13", om.getArrayElement(vm.get("v3"), vm.get("param0")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v9"),vm.get("param0")));
    vm.put("v16", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v17", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v18", om.popArrayElement(vm.get("v3"), vm.get("v17")));
    vm.put("v19", em.op("isub").eval(vm.get("v15"),vm.get("v0")));
    vm.put("v20", em.op("iadd").eval(vm.get("v16"),vm.get("v1")));
    vm.put("v21", om.getArrayElement(vm.get("v3"), vm.get("v20")));
    vm.put("v22", vm.get("v21"));
    if (!em.ge(vm.get("v0"), vm.get("v19"))) e.abort(String.format("!((v0=%s) ge (v19=%s))", vm.get("v0"), vm.get("v19")));
    if (!em.lt(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) lt (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.gt(vm.get("v19"), vm.get("v1"))) e.abort(String.format("!((v19=%s) gt (v1=%s))", vm.get("v19"), vm.get("v1")));
    if (!em.lt(vm.get("v1"), vm.get("v19"))) e.abort(String.format("!((v1=%s) lt (v19=%s))", vm.get("v1"), vm.get("v19")));
  }
  @IM(clazz="java.util.Stack", name="pop", desc="()Ljava/lang/Object;",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void pop3299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", om.getField(vm.get("this"), "elementData"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "elementCount"));
    vm.put("v4", null);
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", (int)0);
    vm.put("v7", vm.get("v5"));
    vm.put("v8", em.op("isub").eval(vm.get("v7"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v8"));
    vm.put("v9", vm.get("v3"));
    vm.put("v10", om.popArrayElement(vm.get("v2"), vm.get("v9")));
    vm.put("v11", em.op("iadd").eval(vm.get("v9"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v11"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v0")));
    vm.put("v13", om.getArrayElement(vm.get("v2"), vm.get("v12")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v11"),vm.get("v0")));
    vm.put("v16", em.op("isub").eval(vm.get("v11"),vm.get("v15")));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    if (!em.lt(vm.get("v15"), vm.get("v11"))) e.abort(String.format("!((v15=%s) lt (v11=%s))", vm.get("v15"), vm.get("v11")));
    if (!em.ne(vm.get("v11"), vm.get("v6"))) e.abort(String.format("!((v11=%s) ne (v6=%s))", vm.get("v11"), vm.get("v6")));
    if (!em.lt(vm.get("v12"), vm.get("v11"))) e.abort(String.format("!((v12=%s) lt (v11=%s))", vm.get("v12"), vm.get("v11")));
    if (!em.ge(vm.get("v15"), vm.get("v6"))) e.abort(String.format("!((v15=%s) ge (v6=%s))", vm.get("v15"), vm.get("v6")));
    if (!em.le(vm.get("v17"), vm.get("v6"))) e.abort(String.format("!((v17=%s) le (v6=%s))", vm.get("v17"), vm.get("v6")));
  }
  @IM(clazz="java.util.Vector", name="insertElementAt", desc="(Ljava/lang/Object;I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void insertElementAt1750(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)4);
    vm.put("v2", (int)5);
    vm.put("v3", (int)8);
    vm.put("v4", (int)3);
    vm.put("v5", (int)7);
    vm.put("v6", (int)6);
    vm.put("v7", (int)1);
    vm.put("v8", (int)2);
    vm.put("v9", om.getField(vm.get("this"), "elementCount"));
    vm.put("v10", om.getField(vm.get("this"), "elementData"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("this"), "modCount"));
    vm.put("v13", (int)9);
    vm.put("v14", om.getArrayLength(vm.get("v11")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", vm.get("v12"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v7")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("v18", vm.get("v9"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v7")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v19"));
    vm.put("v20", em.op("iadd").eval(vm.get("v19"),vm.get("v7")));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v15")));
    vm.put("v22", om.guessArrayIndex(vm.get("v11")));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.getArrayElement(vm.get("v11"), vm.get("v23")));
    vm.put("v25", vm.get("v24"));
    vm.put("param1", em.op("isub").eval(vm.get("v23"),vm.get("v4")));
    vm.put("v26", em.op("iadd").eval(vm.get("param1"),vm.get("v8")));
    vm.put("v27", em.op("iadd").eval(vm.get("param1"),vm.get("v6")));
    vm.put("v28", em.op("iadd").eval(vm.get("param1"),vm.get("v7")));
    vm.put("v29", em.op("iadd").eval(vm.get("v28"),vm.get("v1")));
    vm.put("v30", em.op("iadd").eval(vm.get("v28"),vm.get("v6")));
    vm.put("v31", em.op("iadd").eval(vm.get("v28"),vm.get("v7")));
    vm.put("v32", em.op("iadd").eval(vm.get("param1"),vm.get("v5")));
    vm.put("v33", em.op("iadd").eval(vm.get("v28"),vm.get("v5")));
    vm.put("v34", om.popArrayElement(vm.get("v11"), vm.get("v29")));
    vm.put("v35", em.op("isub").eval(vm.get("v19"),vm.get("param1")));
    vm.put("v36", om.getArrayElement(vm.get("v11"), vm.get("v32")));
    vm.put("v37", vm.get("v36"));
    vm.put("v38", em.op("iadd").eval(vm.get("v28"),vm.get("v0")));
    vm.put("v39", om.popArrayElement(vm.get("v11"), vm.get("v38")));
    vm.put("v40", em.op("iadd").eval(vm.get("v28"),vm.get("v3")));
    vm.put("v41", om.popArrayElement(vm.get("v11"), vm.get("v40")));
    vm.put("v42", om.popArrayElement(vm.get("v11"), vm.get("v33")));
    vm.put("v43", om.popArrayElement(vm.get("v11"), vm.get("param1")));
    vm.put("param0", vm.get("v43"));
    vm.put("v44", em.op("iadd").eval(vm.get("v28"),vm.get("v8")));
    vm.put("v45", em.op("iadd").eval(vm.get("param1"),vm.get("v7")));
    vm.put("v46", em.op("iadd").eval(vm.get("param1"),vm.get("v1")));
    vm.put("v47", om.getArrayElement(vm.get("v11"), vm.get("v46")));
    vm.put("v48", vm.get("v47"));
    vm.put("v49", em.op("iadd").eval(vm.get("v28"),vm.get("v4")));
    vm.put("v50", om.popArrayElement(vm.get("v11"), vm.get("v44")));
    vm.put("v51", vm.get("v50"));
    vm.put("v52", om.getArrayElement(vm.get("v11"), vm.get("v26")));
    vm.put("v53", om.popArrayElement(vm.get("v11"), vm.get("v49")));
    vm.put("v54", em.op("iadd").eval(vm.get("v28"),vm.get("v2")));
    vm.put("v55", om.popArrayElement(vm.get("v11"), vm.get("v54")));
    vm.put("v56", em.op("iadd").eval(vm.get("param1"),vm.get("v3")));
    vm.put("v57", om.getArrayElement(vm.get("v11"), vm.get("v56")));
    vm.put("v58", vm.get("v57"));
    vm.put("v59", om.popArrayElement(vm.get("v11"), vm.get("v31")));
    vm.put("v60", vm.get("v59"));
    vm.put("v61", em.op("iadd").eval(vm.get("param1"),vm.get("v0")));
    vm.put("v62", om.popArrayElement(vm.get("v11"), vm.get("v30")));
    vm.put("v63", vm.get("v62"));
    vm.put("v64", om.getArrayElement(vm.get("v11"), vm.get("v45")));
    vm.put("v65", om.getArrayElement(vm.get("v11"), vm.get("v61")));
    vm.put("v66", vm.get("v65"));
    vm.put("v67", em.op("iadd").eval(vm.get("param1"),vm.get("v2")));
    vm.put("v68", om.getArrayElement(vm.get("v11"), vm.get("v67")));
    vm.put("v69", vm.get("v68"));
    vm.put("v70", om.getArrayElement(vm.get("v11"), vm.get("v27")));
    if (!em.lt(vm.get("v6"), vm.get("v35"))) e.abort(String.format("!((v6=%s) lt (v35=%s))", vm.get("v6"), vm.get("v35")));
    if (!em.lt(vm.get("v8"), vm.get("v35"))) e.abort(String.format("!((v8=%s) lt (v35=%s))", vm.get("v8"), vm.get("v35")));
    if (!em.lt(vm.get("v5"), vm.get("v35"))) e.abort(String.format("!((v5=%s) lt (v35=%s))", vm.get("v5"), vm.get("v35")));
    if (!em.lt(vm.get("v0"), vm.get("v35"))) e.abort(String.format("!((v0=%s) lt (v35=%s))", vm.get("v0"), vm.get("v35")));
    if (!em.le(vm.get("v21"), vm.get("v0"))) e.abort(String.format("!((v21=%s) le (v0=%s))", vm.get("v21"), vm.get("v0")));
    if (!em.lt(vm.get("v4"), vm.get("v35"))) e.abort(String.format("!((v4=%s) lt (v35=%s))", vm.get("v4"), vm.get("v35")));
    if (!em.lt(vm.get("v7"), vm.get("v35"))) e.abort(String.format("!((v7=%s) lt (v35=%s))", vm.get("v7"), vm.get("v35")));
    if (!em.lt(vm.get("v1"), vm.get("v35"))) e.abort(String.format("!((v1=%s) lt (v35=%s))", vm.get("v1"), vm.get("v35")));
    if (!em.ge(vm.get("v13"), vm.get("v35"))) e.abort(String.format("!((v13=%s) ge (v35=%s))", vm.get("v13"), vm.get("v35")));
    if (!em.lt(vm.get("v3"), vm.get("v35"))) e.abort(String.format("!((v3=%s) lt (v35=%s))", vm.get("v3"), vm.get("v35")));
    if (!em.le(vm.get("param1"), vm.get("v19"))) e.abort(String.format("!((param1=%s) le (v19=%s))", vm.get("param1"), vm.get("v19")));
    if (!em.lt(vm.get("v2"), vm.get("v35"))) e.abort(String.format("!((v2=%s) lt (v35=%s))", vm.get("v2"), vm.get("v35")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1353(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", (int)1);
    vm.put("v8", (int)0);
    vm.put("v9", (int)7);
    vm.put("v10", java.lang.Object[].class);
    vm.put("v11", null);
    vm.put("v12", em.op("isub").eval(vm.get("v1"),vm.get("v8")));
    vm.put("v13", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v5")));
    vm.put("v14", em.op("isub").eval(vm.get("v1"),vm.get("v8")));
    vm.put("v15", om.getArrayLength(vm.get("v5")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v8")));
    vm.put("v18", vm.get("v6"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v7")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.lt(vm.get("v14"), vm.get("v9"))) e.abort(String.format("!((v14=%s) lt (v9=%s))", vm.get("v14"), vm.get("v9")));
    if (!em.ne(vm.get("v3"), vm.get("v8"))) e.abort(String.format("!((v3=%s) ne (v8=%s))", vm.get("v3"), vm.get("v8")));
    if (!em.le(vm.get("v1"), vm.get("v16"))) e.abort(String.format("!((v1=%s) le (v16=%s))", vm.get("v1"), vm.get("v16")));
    if (!em.ne(vm.get("v13"), vm.get("v10"))) e.abort(String.format("!((v13=%s) ne (v10=%s))", vm.get("v13"), vm.get("v10")));
    if (!em.le(vm.get("v8"), vm.get("v1"))) e.abort(String.format("!((v8=%s) le (v1=%s))", vm.get("v8"), vm.get("v1")));
    if (!em.ge(vm.get("v8"), vm.get("v1"))) e.abort(String.format("!((v8=%s) ge (v1=%s))", vm.get("v8"), vm.get("v1")));
    if (!em.gt(vm.get("v17"), vm.get("v12"))) e.abort(String.format("!((v17=%s) gt (v12=%s))", vm.get("v17"), vm.get("v12")));
    if (!em.ge(vm.get("v12"), vm.get("v8"))) e.abort(String.format("!((v12=%s) ge (v8=%s))", vm.get("v12"), vm.get("v8")));
    if (!em.eq(vm.get("param0"), vm.get("v11"))) e.abort(String.format("!((param0=%s) eq (v11=%s))", vm.get("param0"), vm.get("v11")));
    if (!em.ge(vm.get("v8"), vm.get("v12"))) e.abort(String.format("!((v8=%s) ge (v12=%s))", vm.get("v8"), vm.get("v12")));
  }
  @IM(clazz="java.util.Vector", name="removeElement", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void removeElement4870(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "elementCount"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", (int)1);
    vm.put("v7", null);
    vm.put("v8", om.getArrayElement(vm.get("v4"), vm.get("v0")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", vm.get("v5"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v6")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.ge(vm.get("v6"), vm.get("v2"))) e.abort(String.format("!((v6=%s) ge (v2=%s))", vm.get("v6"), vm.get("v2")));
    if (!em.lt(vm.get("v0"), vm.get("v2"))) e.abort(String.format("!((v0=%s) lt (v2=%s))", vm.get("v0"), vm.get("v2")));
    if (!em.ne(vm.get("v9"), vm.get("v7"))) e.abort(String.format("!((v9=%s) ne (v7=%s))", vm.get("v9"), vm.get("v7")));
    if (!em.eq(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) eq (v7=%s))", vm.get("param0"), vm.get("v7")));
  }
  @IM(clazz="java.util.Vector", name="trimToSize", desc="()V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void trimToSize999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getArrayLength(vm.get("v1")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", vm.get("v4"));
    vm.put("v9", em.op("isub").eval(vm.get("v8"),vm.get("v5")));
    om.revertField(vm.get("this"), "modCount", vm.get("v9"));
    if (!em.ge(vm.get("v3"), vm.get("v7"))) e.abort(String.format("!((v3=%s) ge (v7=%s))", vm.get("v3"), vm.get("v7")));
  }
  @IM(clazz="java.util.Vector", name="removeAllElements", desc="()V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementCount"})
  static void removeAllElements4699(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", vm.get("v2"));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    vm.put("v5", em.op("isub").eval(vm.get("v4"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v5"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v6", om.getField(vm.get("this"), "elementCount"));
    if (!em.ge(vm.get("v1"), vm.get("v6"))) e.abort(String.format("!((v1=%s) ge (v6=%s))", vm.get("v1"), vm.get("v6")));
  }
  @IM(clazz="java.util.Vector", name="removeElement", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void removeElement4890(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", null);
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", om.getField(vm.get("this"), "elementCount"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.popArrayElement(vm.get("v7"), vm.get("v5")));
    vm.put("v9", om.getArrayElement(vm.get("v7"), vm.get("v0")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", vm.get("v3"));
    vm.put("v12", em.op("iadd").eval(vm.get("v5"),vm.get("v1")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v12"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v0")));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v1")));
    vm.put("v15", em.op("isub").eval(vm.get("v11"),vm.get("v1")));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v16"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.lt(vm.get("v0"), vm.get("v12"))) e.abort(String.format("!((v0=%s) lt (v12=%s))", vm.get("v0"), vm.get("v12")));
    if (!em.lt(vm.get("v0"), vm.get("v12"))) e.abort(String.format("!((v0=%s) lt (v12=%s))", vm.get("v0"), vm.get("v12")));
    if (!em.eq(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) eq (v2=%s))", vm.get("param0"), vm.get("v2")));
    if (!em.le(vm.get("v14"), vm.get("v0"))) e.abort(String.format("!((v14=%s) le (v0=%s))", vm.get("v14"), vm.get("v0")));
    if (!em.eq(vm.get("v10"), vm.get("v2"))) e.abort(String.format("!((v10=%s) eq (v2=%s))", vm.get("v10"), vm.get("v2")));
  }
  @IM(clazz="java.util.Vector", name="clear", desc="()V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void clear2861(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2);
    vm.put("v1", null);
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", (int)0);
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popArrayElement(vm.get("v5"), vm.get("v3")));
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", (int)1);
    vm.put("v9", om.popArrayElement(vm.get("v5"), vm.get("v8")));
    vm.put("v10", (int)3);
    vm.put("v11", om.popArrayElement(vm.get("v5"), vm.get("v0")));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    vm.put("v12", vm.get("v7"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v8")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v14", om.getField(vm.get("this"), "elementCount"));
    if (!em.ge(vm.get("v10"), vm.get("v14"))) e.abort(String.format("!((v10=%s) ge (v14=%s))", vm.get("v10"), vm.get("v14")));
    if (!em.lt(vm.get("v8"), vm.get("v14"))) e.abort(String.format("!((v8=%s) lt (v14=%s))", vm.get("v8"), vm.get("v14")));
    if (!em.lt(vm.get("v3"), vm.get("v14"))) e.abort(String.format("!((v3=%s) lt (v14=%s))", vm.get("v3"), vm.get("v14")));
    if (!em.lt(vm.get("v0"), vm.get("v14"))) e.abort(String.format("!((v0=%s) lt (v14=%s))", vm.get("v0"), vm.get("v14")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1380(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementCount"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)0);
    vm.put("v9", (int)7);
    vm.put("v10", null);
    vm.put("v11", java.lang.Object[].class);
    vm.put("v12", em.op("isub").eval(vm.get("v7"),vm.get("v8")));
    vm.put("v13", em.op("isub").eval(vm.get("v7"),vm.get("v8")));
    vm.put("v14", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v3")));
    vm.put("v15", om.getArrayLength(vm.get("v3")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v8")));
    vm.put("v18", vm.get("v0"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ge(vm.get("v8"), vm.get("v17"))) e.abort(String.format("!((v8=%s) ge (v17=%s))", vm.get("v8"), vm.get("v17")));
    if (!em.le(vm.get("v17"), vm.get("v12"))) e.abort(String.format("!((v17=%s) le (v12=%s))", vm.get("v17"), vm.get("v12")));
    if (!em.ne(vm.get("v14"), vm.get("v11"))) e.abort(String.format("!((v14=%s) ne (v11=%s))", vm.get("v14"), vm.get("v11")));
    if (!em.ne(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ne (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.le(vm.get("v7"), vm.get("v16"))) e.abort(String.format("!((v7=%s) le (v16=%s))", vm.get("v7"), vm.get("v16")));
    if (!em.ge(vm.get("v12"), vm.get("v8"))) e.abort(String.format("!((v12=%s) ge (v8=%s))", vm.get("v12"), vm.get("v8")));
    if (!em.ne(vm.get("v5"), vm.get("v8"))) e.abort(String.format("!((v5=%s) ne (v8=%s))", vm.get("v5"), vm.get("v8")));
    if (!em.ne(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ne (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.ge(vm.get("v8"), vm.get("v7"))) e.abort(String.format("!((v8=%s) ge (v7=%s))", vm.get("v8"), vm.get("v7")));
    if (!em.le(vm.get("v8"), vm.get("v7"))) e.abort(String.format("!((v8=%s) le (v7=%s))", vm.get("v8"), vm.get("v7")));
    if (!em.lt(vm.get("v13"), vm.get("v9"))) e.abort(String.format("!((v13=%s) lt (v9=%s))", vm.get("v13"), vm.get("v9")));
  }
  @IM(clazz="java.util.Vector", name="setSize", desc="(I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void setSize4098(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.getField(vm.get("this"), "modCount"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "elementCount"));
    vm.put("param0", vm.get("v3"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popArrayElement(vm.get("v5"), vm.get("param0")));
    vm.put("v7", em.op("iinc").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v8", vm.get("v1"));
    vm.put("v9", em.op("isub").eval(vm.get("v8"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v9"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v10", om.getField(vm.get("this"), "elementCount"));
    if (!em.le(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) le (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.ge(vm.get("v7"), vm.get("v10"))) e.abort(String.format("!((v7=%s) ge (v10=%s))", vm.get("v7"), vm.get("v10")));
    if (!em.lt(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) lt (v10=%s))", vm.get("param0"), vm.get("v10")));
  }
  @IM(clazz="java.util.Vector", name="clear", desc="()V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void clear2876(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)4);
    vm.put("v1", null);
    vm.put("v2", (int)6);
    vm.put("v3", (int)0);
    vm.put("v4", (int)10);
    vm.put("v5", (int)1);
    vm.put("v6", (int)7);
    vm.put("v7", (int)9);
    vm.put("v8", om.getField(vm.get("this"), "elementCount"));
    vm.put("v9", om.getField(vm.get("this"), "elementData"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)11);
    vm.put("v12", (int)5);
    vm.put("v13", (int)12);
    vm.put("v14", (int)8);
    vm.put("v15", (int)14);
    vm.put("v16", om.getField(vm.get("this"), "modCount"));
    vm.put("v17", (int)2);
    vm.put("v18", (int)13);
    vm.put("v19", (int)3);
    vm.put("v20", (int)15);
    vm.put("v21", om.popArrayElement(vm.get("v10"), vm.get("v11")));
    vm.put("v22", om.popArrayElement(vm.get("v10"), vm.get("v19")));
    vm.put("v23", om.popArrayElement(vm.get("v10"), vm.get("v7")));
    vm.put("v24", om.popArrayElement(vm.get("v10"), vm.get("v12")));
    vm.put("v25", om.popArrayElement(vm.get("v10"), vm.get("v6")));
    vm.put("v26", om.popArrayElement(vm.get("v10"), vm.get("v4")));
    vm.put("v27", om.popArrayElement(vm.get("v10"), vm.get("v17")));
    vm.put("v28", om.popArrayElement(vm.get("v10"), vm.get("v5")));
    vm.put("v29", om.popArrayElement(vm.get("v10"), vm.get("v2")));
    vm.put("v30", om.popArrayElement(vm.get("v10"), vm.get("v15")));
    vm.put("v31", om.popArrayElement(vm.get("v10"), vm.get("v18")));
    vm.put("v32", om.popArrayElement(vm.get("v10"), vm.get("v13")));
    vm.put("v33", om.popArrayElement(vm.get("v10"), vm.get("v0")));
    vm.put("v34", om.popArrayElement(vm.get("v10"), vm.get("v3")));
    vm.put("v35", om.popArrayElement(vm.get("v10"), vm.get("v14")));
    if (!em.eq(vm.get("v3"), (vm.get("v8")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v8")));
    vm.put("v36", vm.get("v16"));
    vm.put("v37", em.op("isub").eval(vm.get("v36"),vm.get("v5")));
    om.revertField(vm.get("this"), "modCount", vm.get("v37"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v38", om.getField(vm.get("this"), "elementCount"));
    if (!em.lt(vm.get("v2"), vm.get("v38"))) e.abort(String.format("!((v2=%s) lt (v38=%s))", vm.get("v2"), vm.get("v38")));
    if (!em.lt(vm.get("v12"), vm.get("v38"))) e.abort(String.format("!((v12=%s) lt (v38=%s))", vm.get("v12"), vm.get("v38")));
    if (!em.ge(vm.get("v20"), vm.get("v38"))) e.abort(String.format("!((v20=%s) ge (v38=%s))", vm.get("v20"), vm.get("v38")));
    if (!em.lt(vm.get("v19"), vm.get("v38"))) e.abort(String.format("!((v19=%s) lt (v38=%s))", vm.get("v19"), vm.get("v38")));
    if (!em.lt(vm.get("v7"), vm.get("v38"))) e.abort(String.format("!((v7=%s) lt (v38=%s))", vm.get("v7"), vm.get("v38")));
    if (!em.lt(vm.get("v5"), vm.get("v38"))) e.abort(String.format("!((v5=%s) lt (v38=%s))", vm.get("v5"), vm.get("v38")));
    if (!em.lt(vm.get("v4"), vm.get("v38"))) e.abort(String.format("!((v4=%s) lt (v38=%s))", vm.get("v4"), vm.get("v38")));
    if (!em.lt(vm.get("v14"), vm.get("v38"))) e.abort(String.format("!((v14=%s) lt (v38=%s))", vm.get("v14"), vm.get("v38")));
    if (!em.lt(vm.get("v3"), vm.get("v38"))) e.abort(String.format("!((v3=%s) lt (v38=%s))", vm.get("v3"), vm.get("v38")));
    if (!em.lt(vm.get("v6"), vm.get("v38"))) e.abort(String.format("!((v6=%s) lt (v38=%s))", vm.get("v6"), vm.get("v38")));
    if (!em.lt(vm.get("v0"), vm.get("v38"))) e.abort(String.format("!((v0=%s) lt (v38=%s))", vm.get("v0"), vm.get("v38")));
    if (!em.lt(vm.get("v15"), vm.get("v38"))) e.abort(String.format("!((v15=%s) lt (v38=%s))", vm.get("v15"), vm.get("v38")));
    if (!em.lt(vm.get("v13"), vm.get("v38"))) e.abort(String.format("!((v13=%s) lt (v38=%s))", vm.get("v13"), vm.get("v38")));
    if (!em.lt(vm.get("v17"), vm.get("v38"))) e.abort(String.format("!((v17=%s) lt (v38=%s))", vm.get("v17"), vm.get("v38")));
    if (!em.lt(vm.get("v11"), vm.get("v38"))) e.abort(String.format("!((v11=%s) lt (v38=%s))", vm.get("v11"), vm.get("v38")));
    if (!em.lt(vm.get("v18"), vm.get("v38"))) e.abort(String.format("!((v18=%s) lt (v38=%s))", vm.get("v18"), vm.get("v38")));
  }
  @IM(clazz="java.util.Vector", name="set", desc="(ILjava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.elementData[*]"},definedOutput={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void set3999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.guessArrayIndex(vm.get("v3")));
    vm.put("param0", vm.get("v4"));
    vm.put("v5", om.getArrayElement(vm.get("v3"), vm.get("param0")));
    vm.put("param1", vm.get("v5"));
    om.revertArrayElement(vm.get("v3"), vm.get("param0"));
    vm.put("v6", om.getArrayElement(vm.get("v3"), vm.get("param0")));
    if (!em.lt(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) lt (v1=%s))", vm.get("param0"), vm.get("v1")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1397(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getStatic("java.util.ComparableTimSort.$assertionsDisabled"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementCount"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "elementData"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)2);
    vm.put("v12", null);
    vm.put("v13", em.op("isub").eval(vm.get("v7"),vm.get("v10")));
    vm.put("v14", om.getArrayLength(vm.get("v9")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", vm.get("v2"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.lt(vm.get("v13"), vm.get("v11"))) e.abort(String.format("!((v13=%s) lt (v11=%s))", vm.get("v13"), vm.get("v11")));
    if (!em.eq(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) eq (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.le(vm.get("v7"), vm.get("v15"))) e.abort(String.format("!((v7=%s) le (v15=%s))", vm.get("v7"), vm.get("v15")));
    if (!em.ne(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) ne (v10=%s))", vm.get("v1"), vm.get("v10")));
    if (!em.le(vm.get("v10"), vm.get("v7"))) e.abort(String.format("!((v10=%s) le (v7=%s))", vm.get("v10"), vm.get("v7")));
    if (!em.eq(vm.get("v5"), vm.get("v10"))) e.abort(String.format("!((v5=%s) eq (v10=%s))", vm.get("v5"), vm.get("v10")));
  }
  @IM(clazz="java.util.Vector", name="removeElementAt", desc="(I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void removeElementAt2485(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementCount"));
    vm.put("v5", null);
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", (int)2);
    vm.put("v8", vm.get("v4"));
    vm.put("v9", om.popArrayElement(vm.get("v3"), vm.get("v8")));
    vm.put("v10", vm.get("v6"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    vm.put("v12", em.op("iadd").eval(vm.get("v8"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v12"));
    vm.put("v13", om.guessArrayIndex(vm.get("v3")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.popArrayElement(vm.get("v3"), vm.get("v14")));
    vm.put("param0", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    vm.put("v16", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v17", em.op("iadd").eval(vm.get("v16"),vm.get("v1")));
    vm.put("v18", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v19", om.popArrayElement(vm.get("v3"), vm.get("v18")));
    vm.put("v20", vm.get("v19"));
    vm.put("v21", em.op("iadd").eval(vm.get("v16"),vm.get("v0")));
    vm.put("v22", om.getArrayElement(vm.get("v3"), vm.get("v17")));
    vm.put("v23", em.op("isub").eval(vm.get("v12"),vm.get("param0")));
    vm.put("v24", om.getArrayElement(vm.get("v3"), vm.get("v21")));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", em.op("isub").eval(vm.get("v23"),vm.get("v0")));
    if (!em.ge(vm.get("v7"), vm.get("v26"))) e.abort(String.format("!((v7=%s) ge (v26=%s))", vm.get("v7"), vm.get("v26")));
    if (!em.gt(vm.get("v26"), vm.get("v1"))) e.abort(String.format("!((v26=%s) gt (v1=%s))", vm.get("v26"), vm.get("v1")));
    if (!em.lt(vm.get("v0"), vm.get("v26"))) e.abort(String.format("!((v0=%s) lt (v26=%s))", vm.get("v0"), vm.get("v26")));
    if (!em.lt(vm.get("v1"), vm.get("v26"))) e.abort(String.format("!((v1=%s) lt (v26=%s))", vm.get("v1"), vm.get("v26")));
    if (!em.ge(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) ge (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.lt(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) lt (v12=%s))", vm.get("param0"), vm.get("v12")));
  }
  @IM(clazz="java.util.Vector", name="removeAllElements", desc="()V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void removeAllElements4660(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", (int)2);
    vm.put("v3", null);
    vm.put("v4", (int)5);
    vm.put("v5", (int)3);
    vm.put("v6", om.getField(vm.get("this"), "elementCount"));
    vm.put("v7", (int)0);
    vm.put("v8", (int)6);
    vm.put("v9", (int)4);
    vm.put("v10", om.getField(vm.get("this"), "elementData"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", (int)7);
    vm.put("v13", om.popArrayElement(vm.get("v11"), vm.get("v7")));
    vm.put("v14", om.popArrayElement(vm.get("v11"), vm.get("v1")));
    vm.put("v15", (int)8);
    vm.put("v16", om.popArrayElement(vm.get("v11"), vm.get("v8")));
    vm.put("v17", om.popArrayElement(vm.get("v11"), vm.get("v4")));
    vm.put("v18", om.popArrayElement(vm.get("v11"), vm.get("v2")));
    vm.put("v19", om.popArrayElement(vm.get("v11"), vm.get("v9")));
    vm.put("v20", om.popArrayElement(vm.get("v11"), vm.get("v12")));
    vm.put("v21", om.popArrayElement(vm.get("v11"), vm.get("v5")));
    if (!em.eq(vm.get("v7"), (vm.get("v6")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v6")));
    vm.put("v22", vm.get("v0"));
    vm.put("v23", em.op("isub").eval(vm.get("v22"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v23"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v24", om.getField(vm.get("this"), "elementCount"));
    if (!em.lt(vm.get("v7"), vm.get("v24"))) e.abort(String.format("!((v7=%s) lt (v24=%s))", vm.get("v7"), vm.get("v24")));
    if (!em.lt(vm.get("v4"), vm.get("v24"))) e.abort(String.format("!((v4=%s) lt (v24=%s))", vm.get("v4"), vm.get("v24")));
    if (!em.lt(vm.get("v5"), vm.get("v24"))) e.abort(String.format("!((v5=%s) lt (v24=%s))", vm.get("v5"), vm.get("v24")));
    if (!em.lt(vm.get("v8"), vm.get("v24"))) e.abort(String.format("!((v8=%s) lt (v24=%s))", vm.get("v8"), vm.get("v24")));
    if (!em.lt(vm.get("v9"), vm.get("v24"))) e.abort(String.format("!((v9=%s) lt (v24=%s))", vm.get("v9"), vm.get("v24")));
    if (!em.ge(vm.get("v15"), vm.get("v24"))) e.abort(String.format("!((v15=%s) ge (v24=%s))", vm.get("v15"), vm.get("v24")));
    if (!em.lt(vm.get("v2"), vm.get("v24"))) e.abort(String.format("!((v2=%s) lt (v24=%s))", vm.get("v2"), vm.get("v24")));
    if (!em.lt(vm.get("v1"), vm.get("v24"))) e.abort(String.format("!((v1=%s) lt (v24=%s))", vm.get("v1"), vm.get("v24")));
    if (!em.lt(vm.get("v12"), vm.get("v24"))) e.abort(String.format("!((v12=%s) lt (v24=%s))", vm.get("v12"), vm.get("v24")));
  }
  @IM(clazz="java.util.Vector", name="setElementAt", desc="(Ljava/lang/Object;I)V",definedOutput={"this.elementData[*]"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void setElementAt2999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.guessArrayIndex(vm.get("v1")));
    vm.put("param1", vm.get("v4"));
    vm.put("v5", om.popArrayElement(vm.get("v1"), vm.get("param1")));
    vm.put("param0", vm.get("v5"));
    if (!em.lt(vm.get("param1"), vm.get("v3"))) e.abort(String.format("!((param1=%s) lt (v3=%s))", vm.get("param1"), vm.get("v3")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1367(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)0);
    vm.put("v9", (int)7);
    vm.put("v10", null);
    vm.put("v11", java.lang.Object[].class);
    vm.put("v12", em.op("isub").eval(vm.get("v3"),vm.get("v8")));
    vm.put("v13", em.op("isub").eval(vm.get("v3"),vm.get("v8")));
    vm.put("v14", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v5")));
    vm.put("v15", om.getArrayLength(vm.get("v5")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v8")));
    vm.put("v18", vm.get("v0"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ne(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ne (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.ge(vm.get("v8"), vm.get("v3"))) e.abort(String.format("!((v8=%s) ge (v3=%s))", vm.get("v8"), vm.get("v3")));
    if (!em.ne(vm.get("v7"), vm.get("v8"))) e.abort(String.format("!((v7=%s) ne (v8=%s))", vm.get("v7"), vm.get("v8")));
    if (!em.lt(vm.get("v12"), vm.get("v9"))) e.abort(String.format("!((v12=%s) lt (v9=%s))", vm.get("v12"), vm.get("v9")));
    if (!em.ne(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ne (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.gt(vm.get("v17"), vm.get("v13"))) e.abort(String.format("!((v17=%s) gt (v13=%s))", vm.get("v17"), vm.get("v13")));
    if (!em.le(vm.get("v3"), vm.get("v16"))) e.abort(String.format("!((v3=%s) le (v16=%s))", vm.get("v3"), vm.get("v16")));
    if (!em.eq(vm.get("v14"), vm.get("v11"))) e.abort(String.format("!((v14=%s) eq (v11=%s))", vm.get("v14"), vm.get("v11")));
    if (!em.ge(vm.get("v13"), vm.get("v8"))) e.abort(String.format("!((v13=%s) ge (v8=%s))", vm.get("v13"), vm.get("v8")));
    if (!em.ge(vm.get("v8"), vm.get("v13"))) e.abort(String.format("!((v8=%s) ge (v13=%s))", vm.get("v8"), vm.get("v13")));
    if (!em.le(vm.get("v8"), vm.get("v3"))) e.abort(String.format("!((v8=%s) le (v3=%s))", vm.get("v8"), vm.get("v3")));
  }
  @IM(clazz="java.util.Stack", name="pop", desc="()Ljava/lang/Object;",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void pop3235(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)3);
    vm.put("v2", (int)6);
    vm.put("v3", (int)0);
    vm.put("v4", (int)5);
    vm.put("v5", (int)2);
    vm.put("v6", (int)4);
    vm.put("v7", om.getField(vm.get("this"), "elementCount"));
    vm.put("v8", null);
    vm.put("v9", om.getField(vm.get("this"), "modCount"));
    vm.put("v10", om.getField(vm.get("this"), "elementData"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", (int)7);
    vm.put("v13", vm.get("v9"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    vm.put("v15", vm.get("v7"));
    vm.put("v16", om.popArrayElement(vm.get("v11"), vm.get("v15")));
    vm.put("v17", em.op("iadd").eval(vm.get("v15"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v17"));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v0")));
    vm.put("v19", em.op("iadd").eval(vm.get("v18"),vm.get("v4")));
    vm.put("v20", em.op("iadd").eval(vm.get("v18"),vm.get("v5")));
    vm.put("v21", om.popArrayElement(vm.get("v11"), vm.get("v19")));
    vm.put("v22", em.op("iadd").eval(vm.get("v18"),vm.get("v3")));
    vm.put("v23", om.popArrayElement(vm.get("v11"), vm.get("v22")));
    vm.put("v24", em.op("iadd").eval(vm.get("v18"),vm.get("v6")));
    vm.put("v25", em.op("iadd").eval(vm.get("v18"),vm.get("v2")));
    vm.put("v26", om.popArrayElement(vm.get("v11"), vm.get("v25")));
    vm.put("v27", em.op("iadd").eval(vm.get("v18"),vm.get("v1")));
    vm.put("v28", om.popArrayElement(vm.get("v11"), vm.get("v27")));
    vm.put("v29", em.op("isub").eval(vm.get("v17"),vm.get("v18")));
    vm.put("v30", em.op("isub").eval(vm.get("v29"),vm.get("v0")));
    vm.put("v31", em.op("iadd").eval(vm.get("v18"),vm.get("v0")));
    vm.put("v32", em.op("isub").eval(vm.get("v17"),vm.get("v0")));
    vm.put("v33", om.getArrayElement(vm.get("v11"), vm.get("v32")));
    vm.put("v34", vm.get("v33"));
    vm.put("v35", em.op("iadd").eval(vm.get("v18"),vm.get("v0")));
    vm.put("v36", em.op("iadd").eval(vm.get("v35"),vm.get("v5")));
    vm.put("v37", em.op("iadd").eval(vm.get("v35"),vm.get("v4")));
    vm.put("v38", om.getArrayElement(vm.get("v11"), vm.get("v37")));
    vm.put("v39", vm.get("v38"));
    vm.put("v40", em.op("iadd").eval(vm.get("v35"),vm.get("v1")));
    vm.put("v41", em.op("iadd").eval(vm.get("v35"),vm.get("v2")));
    vm.put("v42", om.getArrayElement(vm.get("v11"), vm.get("v40")));
    vm.put("v43", vm.get("v42"));
    vm.put("v44", em.op("iadd").eval(vm.get("v35"),vm.get("v6")));
    vm.put("v45", om.getArrayElement(vm.get("v11"), vm.get("v44")));
    vm.put("v46", vm.get("v45"));
    vm.put("v47", om.getArrayElement(vm.get("v11"), vm.get("v41")));
    vm.put("v48", vm.get("v47"));
    vm.put("v49", om.popArrayElement(vm.get("v11"), vm.get("v31")));
    vm.put("v50", om.getArrayElement(vm.get("v11"), vm.get("v36")));
    vm.put("v51", vm.get("v50"));
    vm.put("v52", om.popArrayElement(vm.get("v11"), vm.get("v20")));
    vm.put("v53", om.popArrayElement(vm.get("v11"), vm.get("v24")));
    vm.put("v54", em.op("iadd").eval(vm.get("v35"),vm.get("v3")));
    vm.put("v55", om.getArrayElement(vm.get("v11"), vm.get("v54")));
    vm.put("v56", vm.get("v55"));
    vm.put("v57", em.op("iadd").eval(vm.get("v35"),vm.get("v0")));
    vm.put("v58", om.getArrayElement(vm.get("v11"), vm.get("v57")));
    vm.put("v59", vm.get("v58"));
    if (!em.lt(vm.get("v0"), vm.get("v30"))) e.abort(String.format("!((v0=%s) lt (v30=%s))", vm.get("v0"), vm.get("v30")));
    if (!em.lt(vm.get("v4"), vm.get("v30"))) e.abort(String.format("!((v4=%s) lt (v30=%s))", vm.get("v4"), vm.get("v30")));
    if (!em.gt(vm.get("v30"), vm.get("v3"))) e.abort(String.format("!((v30=%s) gt (v3=%s))", vm.get("v30"), vm.get("v3")));
    if (!em.lt(vm.get("v1"), vm.get("v30"))) e.abort(String.format("!((v1=%s) lt (v30=%s))", vm.get("v1"), vm.get("v30")));
    if (!em.ge(vm.get("v12"), vm.get("v30"))) e.abort(String.format("!((v12=%s) ge (v30=%s))", vm.get("v12"), vm.get("v30")));
    if (!em.ge(vm.get("v18"), vm.get("v3"))) e.abort(String.format("!((v18=%s) ge (v3=%s))", vm.get("v18"), vm.get("v3")));
    if (!em.lt(vm.get("v6"), vm.get("v30"))) e.abort(String.format("!((v6=%s) lt (v30=%s))", vm.get("v6"), vm.get("v30")));
    if (!em.lt(vm.get("v2"), vm.get("v30"))) e.abort(String.format("!((v2=%s) lt (v30=%s))", vm.get("v2"), vm.get("v30")));
    if (!em.lt(vm.get("v5"), vm.get("v30"))) e.abort(String.format("!((v5=%s) lt (v30=%s))", vm.get("v5"), vm.get("v30")));
    if (!em.ne(vm.get("v17"), vm.get("v3"))) e.abort(String.format("!((v17=%s) ne (v3=%s))", vm.get("v17"), vm.get("v3")));
    if (!em.lt(vm.get("v3"), vm.get("v30"))) e.abort(String.format("!((v3=%s) lt (v30=%s))", vm.get("v3"), vm.get("v30")));
    if (!em.lt(vm.get("v32"), vm.get("v17"))) e.abort(String.format("!((v32=%s) lt (v17=%s))", vm.get("v32"), vm.get("v17")));
    if (!em.lt(vm.get("v18"), vm.get("v17"))) e.abort(String.format("!((v18=%s) lt (v17=%s))", vm.get("v18"), vm.get("v17")));
  }
  @IM(clazz="java.util.Vector", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void add189(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", (int)2);
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayLength(vm.get("v4")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", om.getField(vm.get("this"), "elementCount"));
    vm.put("v9", (int)3);
    vm.put("v10", vm.get("v7"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    vm.put("v12", vm.get("v8"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v13"));
    vm.put("v14", em.op("iadd").eval(vm.get("v13"),vm.get("v0")));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v6")));
    vm.put("v16", om.guessArrayIndex(vm.get("v4")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.getArrayElement(vm.get("v4"), vm.get("v17")));
    vm.put("v19", vm.get("v18"));
    vm.put("param0", em.op("isub").eval(vm.get("v17"),vm.get("v2")));
    vm.put("v20", om.popArrayElement(vm.get("v4"), vm.get("param0")));
    vm.put("param1", vm.get("v20"));
    vm.put("v21", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v22", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v23", em.op("iadd").eval(vm.get("v22"),vm.get("v0")));
    vm.put("v24", om.popArrayElement(vm.get("v4"), vm.get("v23")));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", em.op("iadd").eval(vm.get("v22"),vm.get("v1")));
    vm.put("v27", om.popArrayElement(vm.get("v4"), vm.get("v26")));
    vm.put("v28", em.op("iadd").eval(vm.get("v22"),vm.get("v2")));
    vm.put("v29", om.popArrayElement(vm.get("v4"), vm.get("v28")));
    vm.put("v30", om.getArrayElement(vm.get("v4"), vm.get("v21")));
    vm.put("v31", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v32", om.getArrayElement(vm.get("v4"), vm.get("v31")));
    vm.put("v33", vm.get("v32"));
    vm.put("v34", em.op("isub").eval(vm.get("v13"),vm.get("param0")));
    if (!em.le(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) le (v13=%s))", vm.get("param0"), vm.get("v13")));
    if (!em.ge(vm.get("v9"), vm.get("v34"))) e.abort(String.format("!((v9=%s) ge (v34=%s))", vm.get("v9"), vm.get("v34")));
    if (!em.le(vm.get("v15"), vm.get("v1"))) e.abort(String.format("!((v15=%s) le (v1=%s))", vm.get("v15"), vm.get("v1")));
    if (!em.lt(vm.get("v1"), vm.get("v34"))) e.abort(String.format("!((v1=%s) lt (v34=%s))", vm.get("v1"), vm.get("v34")));
    if (!em.lt(vm.get("v2"), vm.get("v34"))) e.abort(String.format("!((v2=%s) lt (v34=%s))", vm.get("v2"), vm.get("v34")));
    if (!em.lt(vm.get("v0"), vm.get("v34"))) e.abort(String.format("!((v0=%s) lt (v34=%s))", vm.get("v0"), vm.get("v34")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void remove4587(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "modCount"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "elementCount"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", null);
    vm.put("v8", om.getArrayElement(vm.get("v6"), vm.get("v0")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", vm.get("v1"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.ge(vm.get("v2"), vm.get("v4"))) e.abort(String.format("!((v2=%s) ge (v4=%s))", vm.get("v2"), vm.get("v4")));
    if (!em.eq(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) eq (v7=%s))", vm.get("param0"), vm.get("v7")));
    if (!em.ne(vm.get("v9"), vm.get("v7"))) e.abort(String.format("!((v9=%s) ne (v7=%s))", vm.get("v9"), vm.get("v7")));
    if (!em.lt(vm.get("v0"), vm.get("v4"))) e.abort(String.format("!((v0=%s) lt (v4=%s))", vm.get("v0"), vm.get("v4")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void remove4574(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "elementCount"));
    vm.put("v2", (int)1);
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getArrayElement(vm.get("v6"), vm.get("v0")));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", vm.get("v1"));
    vm.put("v10", om.popArrayElement(vm.get("v6"), vm.get("v9")));
    vm.put("v11", vm.get("v4"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v2")));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    vm.put("v14", em.op("iadd").eval(vm.get("v9"),vm.get("v2")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v14"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v2")));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.eq(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) eq (v3=%s))", vm.get("param0"), vm.get("v3")));
    if (!em.lt(vm.get("v0"), vm.get("v14"))) e.abort(String.format("!((v0=%s) lt (v14=%s))", vm.get("v0"), vm.get("v14")));
    if (!em.lt(vm.get("v0"), vm.get("v14"))) e.abort(String.format("!((v0=%s) lt (v14=%s))", vm.get("v0"), vm.get("v14")));
    if (!em.eq(vm.get("v8"), vm.get("v3"))) e.abort(String.format("!((v8=%s) eq (v3=%s))", vm.get("v8"), vm.get("v3")));
    if (!em.le(vm.get("v16"), vm.get("v0"))) e.abort(String.format("!((v16=%s) le (v0=%s))", vm.get("v16"), vm.get("v0")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void remove4579(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "elementData"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getArrayElement(vm.get("v2"), vm.get("v0")));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "elementCount"));
    vm.put("v6", (int)1);
    vm.put("v7", null);
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", vm.get("v5"));
    vm.put("v10", om.popArrayElement(vm.get("v2"), vm.get("v9")));
    vm.put("v11", em.op("iadd").eval(vm.get("v9"),vm.get("v6")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v11"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v0")));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v6")));
    vm.put("v14", vm.get("v8"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v6")));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v6")));
    om.revertField(vm.get("this"), "modCount", vm.get("v16"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.eq(vm.get("v4"), vm.get("v7"))) e.abort(String.format("!((v4=%s) eq (v7=%s))", vm.get("v4"), vm.get("v7")));
    if (!em.eq(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) eq (v7=%s))", vm.get("param0"), vm.get("v7")));
    if (!em.lt(vm.get("v0"), vm.get("v11"))) e.abort(String.format("!((v0=%s) lt (v11=%s))", vm.get("v0"), vm.get("v11")));
    if (!em.ge(vm.get("v0"), vm.get("v13"))) e.abort(String.format("!((v0=%s) ge (v13=%s))", vm.get("v0"), vm.get("v13")));
    if (!em.lt(vm.get("v0"), vm.get("v11"))) e.abort(String.format("!((v0=%s) lt (v11=%s))", vm.get("v0"), vm.get("v11")));
    if (!em.gt(vm.get("v13"), vm.get("v0"))) e.abort(String.format("!((v13=%s) gt (v0=%s))", vm.get("v13"), vm.get("v0")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1344(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", om.getField(vm.get("this"), "elementCount"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "elementData"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", (int)7);
    vm.put("v10", null);
    vm.put("v11", (int)2);
    vm.put("v12", java.lang.Object[].class);
    vm.put("v13", om.getArrayElement(vm.get("v8"), vm.get("v1")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v8")));
    vm.put("v16", em.op("isub").eval(vm.get("v4"),vm.get("v0")));
    vm.put("v17", om.getArrayLength(vm.get("v8")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v0")));
    vm.put("v20", om.getArrayElement(vm.get("v8"), vm.get("v0")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", em.op("isub").eval(vm.get("v4"),vm.get("v0")));
    vm.put("v23", vm.get("v2"));
    vm.put("v24", em.op("isub").eval(vm.get("v23"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v24"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ne(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ne (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.le(vm.get("v4"), vm.get("v18"))) e.abort(String.format("!((v4=%s) le (v18=%s))", vm.get("v4"), vm.get("v18")));
    if (!em.le(vm.get("v0"), vm.get("v4"))) e.abort(String.format("!((v0=%s) le (v4=%s))", vm.get("v0"), vm.get("v4")));
    if (!em.ge(vm.get("v22"), vm.get("v0"))) e.abort(String.format("!((v22=%s) ge (v0=%s))", vm.get("v22"), vm.get("v0")));
    if (!em.lt(vm.get("v16"), vm.get("v9"))) e.abort(String.format("!((v16=%s) lt (v9=%s))", vm.get("v16"), vm.get("v9")));
    if (!em.ge(vm.get("v0"), vm.get("v4"))) e.abort(String.format("!((v0=%s) ge (v4=%s))", vm.get("v0"), vm.get("v4")));
    if (!em.lt(vm.get("v1"), vm.get("v22"))) e.abort(String.format("!((v1=%s) lt (v22=%s))", vm.get("v1"), vm.get("v22")));
    if (!em.gt(vm.get("v19"), vm.get("v22"))) e.abort(String.format("!((v19=%s) gt (v22=%s))", vm.get("v19"), vm.get("v22")));
    if (!em.ne(vm.get("v6"), vm.get("v0"))) e.abort(String.format("!((v6=%s) ne (v0=%s))", vm.get("v6"), vm.get("v0")));
    if (!em.lt(vm.get("v0"), vm.get("v22"))) e.abort(String.format("!((v0=%s) lt (v22=%s))", vm.get("v0"), vm.get("v22")));
    if (!em.eq(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) eq (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.eq(vm.get("v15"), vm.get("v12"))) e.abort(String.format("!((v15=%s) eq (v12=%s))", vm.get("v15"), vm.get("v12")));
    if (!em.ge(vm.get("v11"), vm.get("v22"))) e.abort(String.format("!((v11=%s) ge (v22=%s))", vm.get("v11"), vm.get("v22")));
  }
  @IM(clazz="java.util.Stack", name="push", desc="(Ljava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void push5097(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getArrayLength(vm.get("v1")));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementCount"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", (int)0);
    vm.put("v8", vm.get("v4"));
    vm.put("v9", em.op("isub").eval(vm.get("v8"),vm.get("v5")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v9"));
    vm.put("v10", om.popArrayElement(vm.get("v1"), vm.get("v9")));
    vm.put("param0", vm.get("v10"));
    vm.put("v11", em.op("iadd").eval(vm.get("v9"),vm.get("v5")));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v3")));
    vm.put("v13", vm.get("v6"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v5")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    if (!em.le(vm.get("v12"), vm.get("v7"))) e.abort(String.format("!((v12=%s) le (v7=%s))", vm.get("v12"), vm.get("v7")));
  }
  @IM(clazz="java.util.Vector", name="removeAllElements", desc="()V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void removeAllElements4693(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", null);
    vm.put("v2", (int)2);
    vm.put("v3", (int)0);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "elementCount"));
    vm.put("v8", (int)3);
    vm.put("v9", om.popArrayElement(vm.get("v6"), vm.get("v0")));
    vm.put("v10", om.popArrayElement(vm.get("v6"), vm.get("v2")));
    vm.put("v11", om.popArrayElement(vm.get("v6"), vm.get("v3")));
    vm.put("v12", vm.get("v4"));
    if (!em.eq(vm.get("v3"), (vm.get("v7")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v7")));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v14", om.getField(vm.get("this"), "elementCount"));
    if (!em.lt(vm.get("v2"), vm.get("v14"))) e.abort(String.format("!((v2=%s) lt (v14=%s))", vm.get("v2"), vm.get("v14")));
    if (!em.ge(vm.get("v8"), vm.get("v14"))) e.abort(String.format("!((v8=%s) ge (v14=%s))", vm.get("v8"), vm.get("v14")));
    if (!em.lt(vm.get("v0"), vm.get("v14"))) e.abort(String.format("!((v0=%s) lt (v14=%s))", vm.get("v0"), vm.get("v14")));
    if (!em.lt(vm.get("v3"), vm.get("v14"))) e.abort(String.format("!((v3=%s) lt (v14=%s))", vm.get("v3"), vm.get("v14")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void remove4566(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayElement(vm.get("v4"), vm.get("v1")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getArrayElement(vm.get("v4"), vm.get("v0")));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("this"), "modCount"));
    vm.put("v10", null);
    vm.put("v11", vm.get("v2"));
    vm.put("v12", om.popArrayElement(vm.get("v4"), vm.get("v11")));
    vm.put("v13", em.op("iadd").eval(vm.get("v11"),vm.get("v1")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v1")));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v1")));
    vm.put("v16", vm.get("v9"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v1")));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v18"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.lt(vm.get("v0"), vm.get("v13"))) e.abort(String.format("!((v0=%s) lt (v13=%s))", vm.get("v0"), vm.get("v13")));
    if (!em.eq(vm.get("v6"), vm.get("v10"))) e.abort(String.format("!((v6=%s) eq (v10=%s))", vm.get("v6"), vm.get("v10")));
    if (!em.gt(vm.get("v15"), vm.get("v0"))) e.abort(String.format("!((v15=%s) gt (v0=%s))", vm.get("v15"), vm.get("v0")));
    if (!em.ge(vm.get("v0"), vm.get("v15"))) e.abort(String.format("!((v0=%s) ge (v15=%s))", vm.get("v0"), vm.get("v15")));
    if (!em.ne(vm.get("v8"), vm.get("v10"))) e.abort(String.format("!((v8=%s) ne (v10=%s))", vm.get("v8"), vm.get("v10")));
    if (!em.eq(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) eq (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.lt(vm.get("v1"), vm.get("v13"))) e.abort(String.format("!((v1=%s) lt (v13=%s))", vm.get("v1"), vm.get("v13")));
    if (!em.lt(vm.get("v1"), vm.get("v13"))) e.abort(String.format("!((v1=%s) lt (v13=%s))", vm.get("v1"), vm.get("v13")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void remove4578(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getArrayElement(vm.get("v5"), vm.get("v1")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", om.getArrayElement(vm.get("v5"), vm.get("v0")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", vm.get("v2"));
    vm.put("v12", om.popArrayElement(vm.get("v5"), vm.get("v11")));
    vm.put("v13", vm.get("v8"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v1")));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    vm.put("v16", em.op("iadd").eval(vm.get("v11"),vm.get("v1")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v16"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v1")));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v1")));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.le(vm.get("v18"), vm.get("v0"))) e.abort(String.format("!((v18=%s) le (v0=%s))", vm.get("v18"), vm.get("v0")));
    if (!em.eq(vm.get("v7"), vm.get("v3"))) e.abort(String.format("!((v7=%s) eq (v3=%s))", vm.get("v7"), vm.get("v3")));
    if (!em.ne(vm.get("v10"), vm.get("v3"))) e.abort(String.format("!((v10=%s) ne (v3=%s))", vm.get("v10"), vm.get("v3")));
    if (!em.lt(vm.get("v1"), vm.get("v16"))) e.abort(String.format("!((v1=%s) lt (v16=%s))", vm.get("v1"), vm.get("v16")));
    if (!em.lt(vm.get("v1"), vm.get("v16"))) e.abort(String.format("!((v1=%s) lt (v16=%s))", vm.get("v1"), vm.get("v16")));
    if (!em.lt(vm.get("v0"), vm.get("v16"))) e.abort(String.format("!((v0=%s) lt (v16=%s))", vm.get("v0"), vm.get("v16")));
    if (!em.eq(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) eq (v3=%s))", vm.get("param0"), vm.get("v3")));
  }
  @IM(clazz="java.util.Vector", name="removeElement", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void removeElement4866(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "elementCount"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)2);
    vm.put("v8", null);
    vm.put("v9", om.getArrayElement(vm.get("v3"), vm.get("v1")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", vm.get("v4"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.ne(vm.get("v10"), vm.get("v8"))) e.abort(String.format("!((v10=%s) ne (v8=%s))", vm.get("v10"), vm.get("v8")));
    if (!em.ge(vm.get("v7"), vm.get("v6"))) e.abort(String.format("!((v7=%s) ge (v6=%s))", vm.get("v7"), vm.get("v6")));
    if (!em.lt(vm.get("v1"), vm.get("v6"))) e.abort(String.format("!((v1=%s) lt (v6=%s))", vm.get("v1"), vm.get("v6")));
    if (!em.eq(vm.get("param0"), vm.get("v8"))) e.abort(String.format("!((param0=%s) eq (v8=%s))", vm.get("param0"), vm.get("v8")));
    if (!em.ne(vm.get("v12"), vm.get("v8"))) e.abort(String.format("!((v12=%s) ne (v8=%s))", vm.get("v12"), vm.get("v8")));
    if (!em.lt(vm.get("v0"), vm.get("v6"))) e.abort(String.format("!((v0=%s) lt (v6=%s))", vm.get("v0"), vm.get("v6")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1327(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayLength(vm.get("v5")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)7);
    vm.put("v12", java.lang.Object[].class);
    vm.put("v13", null);
    vm.put("v14", em.op("isub").eval(vm.get("v1"),vm.get("v10")));
    vm.put("v15", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v5")));
    vm.put("v16", em.op("isub").eval(vm.get("v9"),vm.get("v10")));
    vm.put("v17", em.op("isub").eval(vm.get("v1"),vm.get("v10")));
    vm.put("v18", vm.get("v2"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ge(vm.get("v14"), vm.get("v10"))) e.abort(String.format("!((v14=%s) ge (v10=%s))", vm.get("v14"), vm.get("v10")));
    if (!em.ne(vm.get("v7"), vm.get("v10"))) e.abort(String.format("!((v7=%s) ne (v10=%s))", vm.get("v7"), vm.get("v10")));
    if (!em.le(vm.get("v1"), vm.get("v9"))) e.abort(String.format("!((v1=%s) le (v9=%s))", vm.get("v1"), vm.get("v9")));
    if (!em.ge(vm.get("v3"), vm.get("v1"))) e.abort(String.format("!((v3=%s) ge (v1=%s))", vm.get("v3"), vm.get("v1")));
    if (!em.lt(vm.get("v10"), vm.get("v1"))) e.abort(String.format("!((v10=%s) lt (v1=%s))", vm.get("v10"), vm.get("v1")));
    if (!em.le(vm.get("v10"), vm.get("v1"))) e.abort(String.format("!((v10=%s) le (v1=%s))", vm.get("v10"), vm.get("v1")));
    if (!em.ne(vm.get("v15"), vm.get("v12"))) e.abort(String.format("!((v15=%s) ne (v12=%s))", vm.get("v15"), vm.get("v12")));
    if (!em.lt(vm.get("v17"), vm.get("v11"))) e.abort(String.format("!((v17=%s) lt (v11=%s))", vm.get("v17"), vm.get("v11")));
    if (!em.ge(vm.get("v10"), vm.get("v14"))) e.abort(String.format("!((v10=%s) ge (v14=%s))", vm.get("v10"), vm.get("v14")));
    if (!em.gt(vm.get("v16"), vm.get("v14"))) e.abort(String.format("!((v16=%s) gt (v14=%s))", vm.get("v16"), vm.get("v14")));
    if (!em.eq(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) eq (v13=%s))", vm.get("param0"), vm.get("v13")));
  }
  @IM(clazz="java.util.Vector", name="setSize", desc="(I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void setSize4090(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", (int)1);
    vm.put("v5", null);
    vm.put("param0", vm.get("v2"));
    vm.put("v6", om.popArrayElement(vm.get("v1"), vm.get("param0")));
    vm.put("v7", em.op("iinc").eval(vm.get("param0"),vm.get("v4")));
    vm.put("v8", om.popArrayElement(vm.get("v1"), vm.get("v7")));
    vm.put("v9", em.op("iinc").eval(vm.get("v7"),vm.get("v4")));
    vm.put("v10", vm.get("v3"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v4")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v12", om.getField(vm.get("this"), "elementCount"));
    if (!em.ge(vm.get("v9"), vm.get("v12"))) e.abort(String.format("!((v9=%s) ge (v12=%s))", vm.get("v9"), vm.get("v12")));
    if (!em.le(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) le (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.lt(vm.get("v7"), vm.get("v12"))) e.abort(String.format("!((v7=%s) lt (v12=%s))", vm.get("v7"), vm.get("v12")));
    if (!em.lt(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) lt (v12=%s))", vm.get("param0"), vm.get("v12")));
  }
  @IM(clazz="java.util.Vector", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void add195(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", (int)0);
    vm.put("v6", om.getArrayLength(vm.get("v3")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", vm.get("v0"));
    vm.put("v9", vm.get("v4"));
    vm.put("v10", em.op("isub").eval(vm.get("v9"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v10"));
    vm.put("v11", em.op("isub").eval(vm.get("v8"),vm.get("v1")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v11"));
    vm.put("v12", em.op("iadd").eval(vm.get("v11"),vm.get("v1")));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v7")));
    vm.put("v14", om.guessArrayIndex(vm.get("v3")));
    vm.put("param0", vm.get("v14"));
    vm.put("v15", om.popArrayElement(vm.get("v3"), vm.get("param0")));
    vm.put("param1", vm.get("v15"));
    vm.put("v16", em.op("isub").eval(vm.get("v11"),vm.get("param0")));
    if (!em.ge(vm.get("v5"), vm.get("v16"))) e.abort(String.format("!((v5=%s) ge (v16=%s))", vm.get("v5"), vm.get("v16")));
    if (!em.le(vm.get("param0"), vm.get("v11"))) e.abort(String.format("!((param0=%s) le (v11=%s))", vm.get("param0"), vm.get("v11")));
    if (!em.le(vm.get("v13"), vm.get("v5"))) e.abort(String.format("!((v13=%s) le (v5=%s))", vm.get("v13"), vm.get("v5")));
  }
  @IM(clazz="java.util.Vector", name="replaceAll", desc="(Ljava/util/function/UnaryOperator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  static void replaceAll2099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", (int)0);
    vm.put("v5", null);
    vm.put("v6", vm.get("v0"));
    vm.put("v7", em.op("isub").eval(vm.get("v6"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v7"));
    vm.put("param0", om.newDefaultValue("java.util.function.UnaryOperator"));
    if (!em.ge(vm.get("v4"), vm.get("v3"))) e.abort(String.format("!((v4=%s) ge (v3=%s))", vm.get("v4"), vm.get("v3")));
    if (!em.ne(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) ne (v5=%s))", vm.get("param0"), vm.get("v5")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1384(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementCount"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getStatic("java.util.ComparableTimSort.$assertionsDisabled"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getArrayLength(vm.get("v5")));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", (int)0);
    vm.put("v13", (int)2);
    vm.put("v14", null);
    vm.put("v15", em.op("isub").eval(vm.get("v7"),vm.get("v12")));
    vm.put("v16", vm.get("v0"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.lt(vm.get("v15"), vm.get("v13"))) e.abort(String.format("!((v15=%s) lt (v13=%s))", vm.get("v15"), vm.get("v13")));
    if (!em.le(vm.get("v7"), vm.get("v11"))) e.abort(String.format("!((v7=%s) le (v11=%s))", vm.get("v7"), vm.get("v11")));
    if (!em.le(vm.get("v7"), vm.get("v11"))) e.abort(String.format("!((v7=%s) le (v11=%s))", vm.get("v7"), vm.get("v11")));
    if (!em.eq(vm.get("v3"), vm.get("v12"))) e.abort(String.format("!((v3=%s) eq (v12=%s))", vm.get("v3"), vm.get("v12")));
    if (!em.le(vm.get("v12"), vm.get("v7"))) e.abort(String.format("!((v12=%s) le (v7=%s))", vm.get("v12"), vm.get("v7")));
    if (!em.eq(vm.get("v9"), vm.get("v12"))) e.abort(String.format("!((v9=%s) eq (v12=%s))", vm.get("v9"), vm.get("v12")));
    if (!em.le(vm.get("v12"), vm.get("v7"))) e.abort(String.format("!((v12=%s) le (v7=%s))", vm.get("v12"), vm.get("v7")));
    if (!em.ne(vm.get("v5"), vm.get("v14"))) e.abort(String.format("!((v5=%s) ne (v14=%s))", vm.get("v5"), vm.get("v14")));
    if (!em.eq(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) eq (v14=%s))", vm.get("param0"), vm.get("v14")));
  }
  @IM(clazz="java.util.Stack", name="pop", desc="()Ljava/lang/Object;",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void pop3238(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)2);
    vm.put("v2", (int)0);
    vm.put("v3", om.getField(vm.get("this"), "elementCount"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", null);
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", (int)3);
    vm.put("v9", vm.get("v3"));
    vm.put("v10", om.popArrayElement(vm.get("v5"), vm.get("v9")));
    vm.put("v11", vm.get("v7"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", em.op("iadd").eval(vm.get("v9"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v0")));
    vm.put("v15", em.op("iadd").eval(vm.get("v14"),vm.get("v2")));
    vm.put("v16", em.op("iadd").eval(vm.get("v14"),vm.get("v0")));
    vm.put("v17", em.op("iadd").eval(vm.get("v16"),vm.get("v0")));
    vm.put("v18", em.op("iadd").eval(vm.get("v14"),vm.get("v1")));
    vm.put("v19", em.op("iadd").eval(vm.get("v16"),vm.get("v1")));
    vm.put("v20", em.op("isub").eval(vm.get("v13"),vm.get("v0")));
    vm.put("v21", em.op("isub").eval(vm.get("v13"),vm.get("v14")));
    vm.put("v22", om.popArrayElement(vm.get("v5"), vm.get("v15")));
    vm.put("v23", em.op("iadd").eval(vm.get("v16"),vm.get("v2")));
    vm.put("v24", om.popArrayElement(vm.get("v5"), vm.get("v18")));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", om.getArrayElement(vm.get("v5"), vm.get("v23")));
    vm.put("v27", vm.get("v26"));
    vm.put("v28", om.getArrayElement(vm.get("v5"), vm.get("v20")));
    vm.put("v29", vm.get("v28"));
    vm.put("v30", em.op("isub").eval(vm.get("v21"),vm.get("v0")));
    vm.put("v31", om.getArrayElement(vm.get("v5"), vm.get("v19")));
    vm.put("v32", em.op("iadd").eval(vm.get("v14"),vm.get("v0")));
    vm.put("v33", om.popArrayElement(vm.get("v5"), vm.get("v32")));
    vm.put("v34", vm.get("v33"));
    vm.put("v35", om.getArrayElement(vm.get("v5"), vm.get("v17")));
    if (!em.lt(vm.get("v14"), vm.get("v13"))) e.abort(String.format("!((v14=%s) lt (v13=%s))", vm.get("v14"), vm.get("v13")));
    if (!em.gt(vm.get("v30"), vm.get("v2"))) e.abort(String.format("!((v30=%s) gt (v2=%s))", vm.get("v30"), vm.get("v2")));
    if (!em.ge(vm.get("v14"), vm.get("v2"))) e.abort(String.format("!((v14=%s) ge (v2=%s))", vm.get("v14"), vm.get("v2")));
    if (!em.ne(vm.get("v13"), vm.get("v2"))) e.abort(String.format("!((v13=%s) ne (v2=%s))", vm.get("v13"), vm.get("v2")));
    if (!em.lt(vm.get("v2"), vm.get("v30"))) e.abort(String.format("!((v2=%s) lt (v30=%s))", vm.get("v2"), vm.get("v30")));
    if (!em.lt(vm.get("v20"), vm.get("v13"))) e.abort(String.format("!((v20=%s) lt (v13=%s))", vm.get("v20"), vm.get("v13")));
    if (!em.ge(vm.get("v8"), vm.get("v30"))) e.abort(String.format("!((v8=%s) ge (v30=%s))", vm.get("v8"), vm.get("v30")));
    if (!em.lt(vm.get("v1"), vm.get("v30"))) e.abort(String.format("!((v1=%s) lt (v30=%s))", vm.get("v1"), vm.get("v30")));
    if (!em.lt(vm.get("v0"), vm.get("v30"))) e.abort(String.format("!((v0=%s) lt (v30=%s))", vm.get("v0"), vm.get("v30")));
  }
  @IM(clazz="java.util.Vector", name="clear", desc="()V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementCount"})
  static void clear2898(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", vm.get("v2"));
    vm.put("v5", em.op("isub").eval(vm.get("v4"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v5"));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v6", om.getField(vm.get("this"), "elementCount"));
    if (!em.ge(vm.get("v1"), vm.get("v6"))) e.abort(String.format("!((v1=%s) ge (v6=%s))", vm.get("v1"), vm.get("v6")));
  }
  @IM(clazz="java.util.Vector", name="insertElementAt", desc="(Ljava/lang/Object;I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void insertElementAt1793(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getArrayLength(vm.get("v3")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "elementCount"));
    vm.put("v8", (int)2);
    vm.put("v9", vm.get("v4"));
    vm.put("v10", em.op("isub").eval(vm.get("v9"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v10"));
    vm.put("v11", vm.get("v7"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v1")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v12"));
    vm.put("v13", em.op("iadd").eval(vm.get("v12"),vm.get("v1")));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v6")));
    vm.put("v15", om.guessArrayIndex(vm.get("v3")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.popArrayElement(vm.get("v3"), vm.get("v16")));
    vm.put("v18", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    vm.put("v19", em.op("iadd").eval(vm.get("v18"),vm.get("v1")));
    vm.put("v20", om.popArrayElement(vm.get("v3"), vm.get("v19")));
    vm.put("param1", em.op("isub").eval(vm.get("v18"),vm.get("v1")));
    vm.put("v21", em.op("iadd").eval(vm.get("param1"),vm.get("v0")));
    vm.put("v22", om.popArrayElement(vm.get("v3"), vm.get("param1")));
    vm.put("param0", vm.get("v22"));
    vm.put("v23", em.op("isub").eval(vm.get("v12"),vm.get("param1")));
    vm.put("v24", om.getArrayElement(vm.get("v3"), vm.get("v21")));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", em.op("iadd").eval(vm.get("param1"),vm.get("v1")));
    vm.put("v27", om.getArrayElement(vm.get("v3"), vm.get("v26")));
    vm.put("v28", vm.get("v27"));
    if (!em.lt(vm.get("v1"), vm.get("v23"))) e.abort(String.format("!((v1=%s) lt (v23=%s))", vm.get("v1"), vm.get("v23")));
    if (!em.lt(vm.get("v0"), vm.get("v23"))) e.abort(String.format("!((v0=%s) lt (v23=%s))", vm.get("v0"), vm.get("v23")));
    if (!em.ge(vm.get("v8"), vm.get("v23"))) e.abort(String.format("!((v8=%s) ge (v23=%s))", vm.get("v8"), vm.get("v23")));
    if (!em.le(vm.get("v14"), vm.get("v0"))) e.abort(String.format("!((v14=%s) le (v0=%s))", vm.get("v14"), vm.get("v0")));
    if (!em.le(vm.get("param1"), vm.get("v12"))) e.abort(String.format("!((param1=%s) le (v12=%s))", vm.get("param1"), vm.get("v12")));
  }
  @IM(clazz="java.util.Vector", name="removeElementAt", desc="(I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void removeElementAt2499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", null);
    vm.put("v6", om.getField(vm.get("this"), "elementCount"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.popArrayElement(vm.get("v3"), vm.get("v7")));
    vm.put("v9", em.op("iadd").eval(vm.get("v7"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v9"));
    vm.put("v10", vm.get("v4"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    vm.put("v12", om.guessArrayIndex(vm.get("v3")));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getArrayElement(vm.get("v3"), vm.get("v13")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", em.op("isub").eval(vm.get("v13"),vm.get("v1")));
    vm.put("param0", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    vm.put("v17", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v18", em.op("isub").eval(vm.get("v9"),vm.get("param0")));
    vm.put("v19", om.popArrayElement(vm.get("v3"), vm.get("v17")));
    vm.put("v20", em.op("isub").eval(vm.get("v18"),vm.get("v0")));
    if (!em.lt(vm.get("v1"), vm.get("v20"))) e.abort(String.format("!((v1=%s) lt (v20=%s))", vm.get("v1"), vm.get("v20")));
    if (!em.gt(vm.get("v20"), vm.get("v1"))) e.abort(String.format("!((v20=%s) gt (v1=%s))", vm.get("v20"), vm.get("v1")));
    if (!em.lt(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) lt (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.ge(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) ge (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.ge(vm.get("v0"), vm.get("v20"))) e.abort(String.format("!((v0=%s) ge (v20=%s))", vm.get("v0"), vm.get("v20")));
  }
  @IM(clazz="java.util.Vector", name="removeAllElements", desc="()V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void removeAllElements4698(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", (int)1);
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "elementCount"));
    vm.put("v6", (int)0);
    vm.put("v7", om.popArrayElement(vm.get("v1"), vm.get("v6")));
    vm.put("v8", (int)2);
    vm.put("v9", om.popArrayElement(vm.get("v1"), vm.get("v2")));
    if (!em.eq(vm.get("v6"), (vm.get("v5")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v5")));
    vm.put("v10", vm.get("v4"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v12", om.getField(vm.get("this"), "elementCount"));
    if (!em.lt(vm.get("v2"), vm.get("v12"))) e.abort(String.format("!((v2=%s) lt (v12=%s))", vm.get("v2"), vm.get("v12")));
    if (!em.lt(vm.get("v6"), vm.get("v12"))) e.abort(String.format("!((v6=%s) lt (v12=%s))", vm.get("v6"), vm.get("v12")));
    if (!em.ge(vm.get("v8"), vm.get("v12"))) e.abort(String.format("!((v8=%s) ge (v12=%s))", vm.get("v8"), vm.get("v12")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1313(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementCount"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayLength(vm.get("v7")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)7);
    vm.put("v12", null);
    vm.put("v13", java.lang.Object[].class);
    vm.put("v14", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v7")));
    vm.put("v15", em.op("isub").eval(vm.get("v5"),vm.get("v10")));
    vm.put("v16", em.op("isub").eval(vm.get("v5"),vm.get("v10")));
    vm.put("v17", em.op("isub").eval(vm.get("v9"),vm.get("v10")));
    vm.put("v18", vm.get("v0"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.lt(vm.get("v16"), vm.get("v11"))) e.abort(String.format("!((v16=%s) lt (v11=%s))", vm.get("v16"), vm.get("v11")));
    if (!em.gt(vm.get("v17"), vm.get("v15"))) e.abort(String.format("!((v17=%s) gt (v15=%s))", vm.get("v17"), vm.get("v15")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ge(vm.get("v1"), vm.get("v5"))) e.abort(String.format("!((v1=%s) ge (v5=%s))", vm.get("v1"), vm.get("v5")));
    if (!em.ne(vm.get("v3"), vm.get("v10"))) e.abort(String.format("!((v3=%s) ne (v10=%s))", vm.get("v3"), vm.get("v10")));
    if (!em.ge(vm.get("v15"), vm.get("v10"))) e.abort(String.format("!((v15=%s) ge (v10=%s))", vm.get("v15"), vm.get("v10")));
    if (!em.le(vm.get("v10"), vm.get("v5"))) e.abort(String.format("!((v10=%s) le (v5=%s))", vm.get("v10"), vm.get("v5")));
    if (!em.lt(vm.get("v10"), vm.get("v5"))) e.abort(String.format("!((v10=%s) lt (v5=%s))", vm.get("v10"), vm.get("v5")));
    if (!em.ne(vm.get("v14"), vm.get("v13"))) e.abort(String.format("!((v14=%s) ne (v13=%s))", vm.get("v14"), vm.get("v13")));
    if (!em.ge(vm.get("v10"), vm.get("v15"))) e.abort(String.format("!((v10=%s) ge (v15=%s))", vm.get("v10"), vm.get("v15")));
    if (!em.le(vm.get("v5"), vm.get("v9"))) e.abort(String.format("!((v5=%s) le (v9=%s))", vm.get("v5"), vm.get("v9")));
    if (!em.eq(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) eq (v12=%s))", vm.get("param0"), vm.get("v12")));
  }
  @IM(clazz="java.util.Vector", name="insertElementAt", desc="(Ljava/lang/Object;I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void insertElementAt1799(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "elementCount"));
    vm.put("v6", om.getArrayLength(vm.get("v4")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", vm.get("v2"));
    vm.put("v9", vm.get("v5"));
    vm.put("v10", em.op("isub").eval(vm.get("v9"),vm.get("v1")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v10"));
    vm.put("v11", em.op("iadd").eval(vm.get("v10"),vm.get("v1")));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v7")));
    vm.put("v13", em.op("isub").eval(vm.get("v8"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    vm.put("v14", om.guessArrayIndex(vm.get("v4")));
    vm.put("param1", vm.get("v14"));
    vm.put("v15", om.popArrayElement(vm.get("v4"), vm.get("param1")));
    vm.put("param0", vm.get("v15"));
    vm.put("v16", em.op("isub").eval(vm.get("v10"),vm.get("param1")));
    vm.put("v17", em.op("iadd").eval(vm.get("param1"),vm.get("v0")));
    vm.put("v18", om.getArrayElement(vm.get("v4"), vm.get("v17")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", em.op("iadd").eval(vm.get("param1"),vm.get("v1")));
    vm.put("v21", em.op("iadd").eval(vm.get("v20"),vm.get("v0")));
    vm.put("v22", om.popArrayElement(vm.get("v4"), vm.get("v21")));
    if (!em.lt(vm.get("v0"), vm.get("v16"))) e.abort(String.format("!((v0=%s) lt (v16=%s))", vm.get("v0"), vm.get("v16")));
    if (!em.ge(vm.get("v1"), vm.get("v16"))) e.abort(String.format("!((v1=%s) ge (v16=%s))", vm.get("v1"), vm.get("v16")));
    if (!em.le(vm.get("v12"), vm.get("v0"))) e.abort(String.format("!((v12=%s) le (v0=%s))", vm.get("v12"), vm.get("v0")));
    if (!em.le(vm.get("param1"), vm.get("v10"))) e.abort(String.format("!((param1=%s) le (v10=%s))", vm.get("param1"), vm.get("v10")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1356(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getStatic("java.util.TimSort.$assertionsDisabled"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getField(vm.get("this"), "elementCount"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "elementData"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)32);
    vm.put("v12", (int)120);
    vm.put("v13", (int)512);
    vm.put("v14", (int)2);
    vm.put("v15", null);
    vm.put("v16", om.getArrayLength(vm.get("v9")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", em.op("isub").eval(vm.get("v7"),vm.get("v10")));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v5")));
    vm.put("v20", em.op("iadd").eval(vm.get("v18"),vm.get("v10")));
    vm.put("v21", vm.get("v4"));
    vm.put("v22", em.op("isub").eval(vm.get("v21"),vm.get("v5")));
    om.revertField(vm.get("this"), "modCount", vm.get("v22"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ne(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) ne (v10=%s))", vm.get("v1"), vm.get("v10")));
    if (!em.ge(vm.get("v18"), vm.get("v11"))) e.abort(String.format("!((v18=%s) ge (v11=%s))", vm.get("v18"), vm.get("v11")));
    if (!em.eq(vm.get("v3"), vm.get("v10"))) e.abort(String.format("!((v3=%s) eq (v10=%s))", vm.get("v3"), vm.get("v10")));
    if (!em.le(vm.get("v10"), vm.get("v7"))) e.abort(String.format("!((v10=%s) le (v7=%s))", vm.get("v10"), vm.get("v7")));
    if (!em.eq(vm.get("v5"), vm.get("v7"))) e.abort(String.format("!((v5=%s) eq (v7=%s))", vm.get("v5"), vm.get("v7")));
    if (!em.ne(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) ne (v15=%s))", vm.get("param0"), vm.get("v15")));
    if (!em.lt(vm.get("v18"), vm.get("v11"))) e.abort(String.format("!((v18=%s) lt (v11=%s))", vm.get("v18"), vm.get("v11")));
    if (!em.ge(vm.get("v18"), vm.get("v14"))) e.abort(String.format("!((v18=%s) ge (v14=%s))", vm.get("v18"), vm.get("v14")));
    if (!em.le(vm.get("v10"), vm.get("v7"))) e.abort(String.format("!((v10=%s) le (v7=%s))", vm.get("v10"), vm.get("v7")));
    if (!em.ne(vm.get("v9"), vm.get("v15"))) e.abort(String.format("!((v9=%s) ne (v15=%s))", vm.get("v9"), vm.get("v15")));
    if (!em.lt(vm.get("v17"), vm.get("v13"))) e.abort(String.format("!((v17=%s) lt (v13=%s))", vm.get("v17"), vm.get("v13")));
    if (!em.lt(vm.get("v10"), vm.get("v7"))) e.abort(String.format("!((v10=%s) lt (v7=%s))", vm.get("v10"), vm.get("v7")));
    if (!em.ne(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) ne (v10=%s))", vm.get("v1"), vm.get("v10")));
    if (!em.ge(vm.get("v5"), vm.get("v20"))) e.abort(String.format("!((v5=%s) ge (v20=%s))", vm.get("v5"), vm.get("v20")));
    if (!em.lt(vm.get("v17"), vm.get("v12"))) e.abort(String.format("!((v17=%s) lt (v12=%s))", vm.get("v17"), vm.get("v12")));
    if (!em.ne(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) ne (v15=%s))", vm.get("param0"), vm.get("v15")));
    if (!em.eq(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) eq (v10=%s))", vm.get("v1"), vm.get("v10")));
    if (!em.eq(vm.get("v19"), vm.get("v10"))) e.abort(String.format("!((v19=%s) eq (v10=%s))", vm.get("v19"), vm.get("v10")));
    if (!em.eq(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) eq (v10=%s))", vm.get("v1"), vm.get("v10")));
    if (!em.le(vm.get("v7"), vm.get("v17"))) e.abort(String.format("!((v7=%s) le (v17=%s))", vm.get("v7"), vm.get("v17")));
    if (!em.eq(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) eq (v10=%s))", vm.get("v1"), vm.get("v10")));
    if (!em.le(vm.get("v7"), vm.get("v17"))) e.abort(String.format("!((v7=%s) le (v17=%s))", vm.get("v7"), vm.get("v17")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void remove3817(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)3);
    vm.put("v2", (int)0);
    vm.put("v3", (int)2);
    vm.put("v4", om.getField(vm.get("this"), "elementCount"));
    vm.put("v5", null);
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", (int)4);
    vm.put("v10", vm.get("v8"));
    vm.put("v11", vm.get("v4"));
    vm.put("v12", om.popArrayElement(vm.get("v7"), vm.get("v11")));
    vm.put("v13", em.op("iadd").eval(vm.get("v11"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    vm.put("v15", om.guessArrayIndex(vm.get("v7")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.popArrayElement(vm.get("v7"), vm.get("v16")));
    vm.put("param0", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    vm.put("v18", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v19", em.op("iadd").eval(vm.get("v18"),vm.get("v0")));
    vm.put("v20", om.getArrayElement(vm.get("v7"), vm.get("param0")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", em.op("iadd").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v23", em.op("iadd").eval(vm.get("v18"),vm.get("v3")));
    vm.put("v24", em.op("iadd").eval(vm.get("param0"),vm.get("v3")));
    vm.put("v25", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v26", om.getArrayElement(vm.get("v7"), vm.get("v19")));
    vm.put("v27", vm.get("v26"));
    vm.put("v28", em.op("iadd").eval(vm.get("v18"),vm.get("v2")));
    vm.put("v29", om.getArrayElement(vm.get("v7"), vm.get("v28")));
    vm.put("v30", vm.get("v29"));
    vm.put("v31", em.op("iadd").eval(vm.get("v18"),vm.get("v1")));
    vm.put("v32", om.getArrayElement(vm.get("v7"), vm.get("v31")));
    vm.put("v33", vm.get("v32"));
    vm.put("v34", om.popArrayElement(vm.get("v7"), vm.get("v24")));
    vm.put("v35", vm.get("v34"));
    vm.put("v36", em.op("isub").eval(vm.get("v13"),vm.get("param0")));
    vm.put("v37", em.op("isub").eval(vm.get("v36"),vm.get("v0")));
    vm.put("v38", om.popArrayElement(vm.get("v7"), vm.get("v25")));
    vm.put("v39", om.popArrayElement(vm.get("v7"), vm.get("v22")));
    vm.put("v40", om.getArrayElement(vm.get("v7"), vm.get("v23")));
    if (!em.lt(vm.get("v0"), vm.get("v37"))) e.abort(String.format("!((v0=%s) lt (v37=%s))", vm.get("v0"), vm.get("v37")));
    if (!em.lt(vm.get("v2"), vm.get("v37"))) e.abort(String.format("!((v2=%s) lt (v37=%s))", vm.get("v2"), vm.get("v37")));
    if (!em.lt(vm.get("v1"), vm.get("v37"))) e.abort(String.format("!((v1=%s) lt (v37=%s))", vm.get("v1"), vm.get("v37")));
    if (!em.ge(vm.get("v9"), vm.get("v37"))) e.abort(String.format("!((v9=%s) ge (v37=%s))", vm.get("v9"), vm.get("v37")));
    if (!em.lt(vm.get("v3"), vm.get("v37"))) e.abort(String.format("!((v3=%s) lt (v37=%s))", vm.get("v3"), vm.get("v37")));
    if (!em.gt(vm.get("v37"), vm.get("v2"))) e.abort(String.format("!((v37=%s) gt (v2=%s))", vm.get("v37"), vm.get("v2")));
    if (!em.lt(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) lt (v13=%s))", vm.get("param0"), vm.get("v13")));
  }
  @IM(clazz="java.util.Vector", name="removeElement", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount","this.elementData[*]","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"})
  @DG(pred={"_init_2599"})
  static void removeElement4818(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", (int)2);
    vm.put("v3", om.getField(vm.get("this"), "elementCount"));
    vm.put("v4", null);
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayElement(vm.get("v7"), vm.get("v1")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getArrayElement(vm.get("v7"), vm.get("v2")));
    vm.put("v11", om.getArrayElement(vm.get("v7"), vm.get("v0")));
    vm.put("v12", vm.get("v11"));
    om.revertArrayElement(vm.get("v7"), vm.get("v1"), vm.get("v12"));
    vm.put("v13", vm.get("v3"));
    vm.put("v14", om.popArrayElement(vm.get("v7"), vm.get("v13")));
    vm.put("v15", em.op("iadd").eval(vm.get("v13"),vm.get("v1")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v15"));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v0")));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v1")));
    vm.put("v18", vm.get("v5"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v1")));
    vm.put("v20", em.op("isub").eval(vm.get("v19"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v20"));
    om.revertArrayElement(vm.get("v7"), vm.get("v0"));
    vm.put("v21", om.getArrayElement(vm.get("v7"), vm.get("v0")));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.ge(vm.get("v2"), vm.get("v17"))) e.abort(String.format("!((v2=%s) ge (v17=%s))", vm.get("v2"), vm.get("v17")));
    if (!em.eq(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) eq (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.eq(vm.get("v21"), vm.get("v4"))) e.abort(String.format("!((v21=%s) eq (v4=%s))", vm.get("v21"), vm.get("v4")));
    if (!em.lt(vm.get("v1"), vm.get("v17"))) e.abort(String.format("!((v1=%s) lt (v17=%s))", vm.get("v1"), vm.get("v17")));
    if (!em.lt(vm.get("v0"), vm.get("v15"))) e.abort(String.format("!((v0=%s) lt (v15=%s))", vm.get("v0"), vm.get("v15")));
    if (!em.gt(vm.get("v17"), vm.get("v0"))) e.abort(String.format("!((v17=%s) gt (v0=%s))", vm.get("v17"), vm.get("v0")));
    if (!em.lt(vm.get("v0"), vm.get("v15"))) e.abort(String.format("!((v0=%s) lt (v15=%s))", vm.get("v0"), vm.get("v15")));
    if (!em.lt(vm.get("v0"), vm.get("v17"))) e.abort(String.format("!((v0=%s) lt (v17=%s))", vm.get("v0"), vm.get("v17")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void remove3895(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", null);
    vm.put("v6", (int)0);
    vm.put("v7", vm.get("v0"));
    vm.put("v8", vm.get("v2"));
    vm.put("v9", om.popArrayElement(vm.get("v4"), vm.get("v8")));
    vm.put("v10", em.op("iadd").eval(vm.get("v8"),vm.get("v1")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v10"));
    vm.put("v11", em.op("isub").eval(vm.get("v7"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    vm.put("v12", om.guessArrayIndex(vm.get("v4")));
    vm.put("param0", vm.get("v12"));
    vm.put("v13", om.getArrayElement(vm.get("v4"), vm.get("param0")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v10"),vm.get("param0")));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v1")));
    if (!em.ge(vm.get("v6"), vm.get("v16"))) e.abort(String.format("!((v6=%s) ge (v16=%s))", vm.get("v6"), vm.get("v16")));
    if (!em.lt(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) lt (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.gt(vm.get("v16"), vm.get("v6"))) e.abort(String.format("!((v16=%s) gt (v6=%s))", vm.get("v16"), vm.get("v6")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1355(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("java.util.ComparableTimSort.$assertionsDisabled"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getArrayLength(vm.get("v3")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", (int)1);
    vm.put("v10", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", (int)0);
    vm.put("v13", (int)32);
    vm.put("v14", (int)2);
    vm.put("v15", null);
    vm.put("v16", em.op("isub").eval(vm.get("v1"),vm.get("v12")));
    vm.put("v17", vm.get("v8"));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v9")));
    om.revertField(vm.get("this"), "modCount", vm.get("v18"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.le(vm.get("v12"), vm.get("v1"))) e.abort(String.format("!((v12=%s) le (v1=%s))", vm.get("v12"), vm.get("v1")));
    if (!em.eq(vm.get("v9"), vm.get("v1"))) e.abort(String.format("!((v9=%s) eq (v1=%s))", vm.get("v9"), vm.get("v1")));
    if (!em.le(vm.get("v1"), vm.get("v7"))) e.abort(String.format("!((v1=%s) le (v7=%s))", vm.get("v1"), vm.get("v7")));
    if (!em.le(vm.get("v9"), vm.get("v1"))) e.abort(String.format("!((v9=%s) le (v1=%s))", vm.get("v9"), vm.get("v1")));
    if (!em.ge(vm.get("v16"), vm.get("v14"))) e.abort(String.format("!((v16=%s) ge (v14=%s))", vm.get("v16"), vm.get("v14")));
    if (!em.le(vm.get("v1"), vm.get("v7"))) e.abort(String.format("!((v1=%s) le (v7=%s))", vm.get("v1"), vm.get("v7")));
    if (!em.ne(vm.get("v5"), vm.get("v12"))) e.abort(String.format("!((v5=%s) ne (v12=%s))", vm.get("v5"), vm.get("v12")));
    if (!em.eq(vm.get("v5"), vm.get("v12"))) e.abort(String.format("!((v5=%s) eq (v12=%s))", vm.get("v5"), vm.get("v12")));
    if (!em.eq(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) eq (v15=%s))", vm.get("param0"), vm.get("v15")));
    if (!em.eq(vm.get("v11"), vm.get("v12"))) e.abort(String.format("!((v11=%s) eq (v12=%s))", vm.get("v11"), vm.get("v12")));
    if (!em.le(vm.get("v12"), vm.get("v1"))) e.abort(String.format("!((v12=%s) le (v1=%s))", vm.get("v12"), vm.get("v1")));
    if (!em.lt(vm.get("v16"), vm.get("v13"))) e.abort(String.format("!((v16=%s) lt (v13=%s))", vm.get("v16"), vm.get("v13")));
    if (!em.eq(vm.get("v5"), vm.get("v12"))) e.abort(String.format("!((v5=%s) eq (v12=%s))", vm.get("v5"), vm.get("v12")));
    if (!em.ne(vm.get("v3"), vm.get("v15"))) e.abort(String.format("!((v3=%s) ne (v15=%s))", vm.get("v3"), vm.get("v15")));
    if (!em.ge(vm.get("v9"), vm.get("v1"))) e.abort(String.format("!((v9=%s) ge (v1=%s))", vm.get("v9"), vm.get("v1")));
  }
  @IM(clazz="java.util.Vector", name="setSize", desc="(I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementCount"})
  @DG(pred={"_init_2599"})
  static void setSize4097(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", (int)0);
    vm.put("v6", om.getArrayLength(vm.get("v4")));
    vm.put("v7", vm.get("v6"));
    vm.put("param0", vm.get("v2"));
    vm.put("v8", em.op("isub").eval(vm.get("param0"),vm.get("v7")));
    vm.put("v9", vm.get("v0"));
    vm.put("v10", em.op("isub").eval(vm.get("v9"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v10"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v11", om.getField(vm.get("this"), "elementCount"));
    if (!em.le(vm.get("v8"), vm.get("v5"))) e.abort(String.format("!((v8=%s) le (v5=%s))", vm.get("v8"), vm.get("v5")));
    if (!em.gt(vm.get("param0"), vm.get("v11"))) e.abort(String.format("!((param0=%s) gt (v11=%s))", vm.get("param0"), vm.get("v11")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1371(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getArrayElement(vm.get("v3"), vm.get("v1")));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "elementCount"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getArrayLength(vm.get("v3")));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("this"), "modCount"));
    vm.put("v15", (int)7);
    vm.put("v16", null);
    vm.put("v17", (int)2);
    vm.put("v18", java.lang.Object[].class);
    vm.put("v19", em.op("isub").eval(vm.get("v9"),vm.get("v0")));
    vm.put("v20", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v3")));
    vm.put("v21", em.op("isub").eval(vm.get("v9"),vm.get("v0")));
    vm.put("v22", em.op("isub").eval(vm.get("v13"),vm.get("v0")));
    vm.put("v23", vm.get("v14"));
    vm.put("v24", em.op("isub").eval(vm.get("v23"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v24"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ge(vm.get("v0"), vm.get("v9"))) e.abort(String.format("!((v0=%s) ge (v9=%s))", vm.get("v0"), vm.get("v9")));
    if (!em.le(vm.get("v22"), vm.get("v21"))) e.abort(String.format("!((v22=%s) le (v21=%s))", vm.get("v22"), vm.get("v21")));
    if (!em.le(vm.get("v0"), vm.get("v9"))) e.abort(String.format("!((v0=%s) le (v9=%s))", vm.get("v0"), vm.get("v9")));
    if (!em.ge(vm.get("v17"), vm.get("v22"))) e.abort(String.format("!((v17=%s) ge (v22=%s))", vm.get("v17"), vm.get("v22")));
    if (!em.lt(vm.get("v1"), vm.get("v22"))) e.abort(String.format("!((v1=%s) lt (v22=%s))", vm.get("v1"), vm.get("v22")));
    if (!em.le(vm.get("v9"), vm.get("v13"))) e.abort(String.format("!((v9=%s) le (v13=%s))", vm.get("v9"), vm.get("v13")));
    if (!em.ne(vm.get("param0"), vm.get("v16"))) e.abort(String.format("!((param0=%s) ne (v16=%s))", vm.get("param0"), vm.get("v16")));
    if (!em.ne(vm.get("v20"), vm.get("v18"))) e.abort(String.format("!((v20=%s) ne (v18=%s))", vm.get("v20"), vm.get("v18")));
    if (!em.ne(vm.get("param0"), vm.get("v16"))) e.abort(String.format("!((param0=%s) ne (v16=%s))", vm.get("param0"), vm.get("v16")));
    if (!em.lt(vm.get("v19"), vm.get("v15"))) e.abort(String.format("!((v19=%s) lt (v15=%s))", vm.get("v19"), vm.get("v15")));
    if (!em.ge(vm.get("v21"), vm.get("v0"))) e.abort(String.format("!((v21=%s) ge (v0=%s))", vm.get("v21"), vm.get("v0")));
    if (!em.ne(vm.get("v7"), vm.get("v0"))) e.abort(String.format("!((v7=%s) ne (v0=%s))", vm.get("v7"), vm.get("v0")));
    if (!em.lt(vm.get("v0"), vm.get("v22"))) e.abort(String.format("!((v0=%s) lt (v22=%s))", vm.get("v0"), vm.get("v22")));
  }
  @IM(clazz="java.util.Vector", name="removeElement", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void removeElement4861(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayElement(vm.get("v4"), vm.get("v1")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", null);
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", om.getArrayElement(vm.get("v4"), vm.get("v0")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", vm.get("v8"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v0")));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    vm.put("v14", vm.get("v2"));
    vm.put("v15", om.popArrayElement(vm.get("v4"), vm.get("v14")));
    vm.put("v16", em.op("iadd").eval(vm.get("v14"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v16"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v0")));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.lt(vm.get("v0"), vm.get("v16"))) e.abort(String.format("!((v0=%s) lt (v16=%s))", vm.get("v0"), vm.get("v16")));
    if (!em.ge(vm.get("v1"), vm.get("v18"))) e.abort(String.format("!((v1=%s) ge (v18=%s))", vm.get("v1"), vm.get("v18")));
    if (!em.lt(vm.get("v0"), vm.get("v16"))) e.abort(String.format("!((v0=%s) lt (v16=%s))", vm.get("v0"), vm.get("v16")));
    if (!em.lt(vm.get("v1"), vm.get("v16"))) e.abort(String.format("!((v1=%s) lt (v16=%s))", vm.get("v1"), vm.get("v16")));
    if (!em.gt(vm.get("v18"), vm.get("v1"))) e.abort(String.format("!((v18=%s) gt (v1=%s))", vm.get("v18"), vm.get("v1")));
    if (!em.eq(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) eq (v7=%s))", vm.get("param0"), vm.get("v7")));
    if (!em.eq(vm.get("v10"), vm.get("v7"))) e.abort(String.format("!((v10=%s) eq (v7=%s))", vm.get("v10"), vm.get("v7")));
    if (!em.ne(vm.get("v6"), vm.get("v7"))) e.abort(String.format("!((v6=%s) ne (v7=%s))", vm.get("v6"), vm.get("v7")));
  }
  @IM(clazz="java.util.Vector", name="addElement", desc="(Ljava/lang/Object;)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void addElement699(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", (int)0);
    vm.put("v6", om.getArrayLength(vm.get("v4")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", vm.get("v0"));
    vm.put("v9", vm.get("v2"));
    vm.put("v10", em.op("isub").eval(vm.get("v9"),vm.get("v1")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v10"));
    vm.put("v11", em.op("iadd").eval(vm.get("v10"),vm.get("v1")));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v7")));
    vm.put("v13", om.popArrayElement(vm.get("v4"), vm.get("v10")));
    vm.put("param0", vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v8"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    if (!em.le(vm.get("v12"), vm.get("v5"))) e.abort(String.format("!((v12=%s) le (v5=%s))", vm.get("v12"), vm.get("v5")));
  }
  @IM(clazz="java.util.Vector", name="clear", desc="()V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void clear2878(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)3);
    vm.put("v1", null);
    vm.put("v2", (int)2);
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", (int)1);
    vm.put("v7", om.popArrayElement(vm.get("v4"), vm.get("v6")));
    vm.put("v8", om.getField(vm.get("this"), "elementCount"));
    vm.put("v9", (int)0);
    vm.put("v10", om.popArrayElement(vm.get("v4"), vm.get("v9")));
    vm.put("v11", (int)4);
    vm.put("v12", om.popArrayElement(vm.get("v4"), vm.get("v0")));
    vm.put("v13", om.popArrayElement(vm.get("v4"), vm.get("v2")));
    if (!em.eq(vm.get("v9"), (vm.get("v8")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v8")));
    vm.put("v14", vm.get("v5"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v6")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v16", om.getField(vm.get("this"), "elementCount"));
    if (!em.lt(vm.get("v9"), vm.get("v16"))) e.abort(String.format("!((v9=%s) lt (v16=%s))", vm.get("v9"), vm.get("v16")));
    if (!em.lt(vm.get("v2"), vm.get("v16"))) e.abort(String.format("!((v2=%s) lt (v16=%s))", vm.get("v2"), vm.get("v16")));
    if (!em.lt(vm.get("v0"), vm.get("v16"))) e.abort(String.format("!((v0=%s) lt (v16=%s))", vm.get("v0"), vm.get("v16")));
    if (!em.ge(vm.get("v11"), vm.get("v16"))) e.abort(String.format("!((v11=%s) ge (v16=%s))", vm.get("v11"), vm.get("v16")));
    if (!em.lt(vm.get("v6"), vm.get("v16"))) e.abort(String.format("!((v6=%s) lt (v16=%s))", vm.get("v6"), vm.get("v16")));
  }
  @IM(clazz="java.util.Stack", name="pop", desc="()Ljava/lang/Object;",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void pop3297(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", vm.get("v4"));
    vm.put("v8", em.op("isub").eval(vm.get("v7"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v8"));
    vm.put("v9", vm.get("v2"));
    vm.put("v10", om.popArrayElement(vm.get("v6"), vm.get("v9")));
    vm.put("v11", em.op("iadd").eval(vm.get("v9"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v11"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v0")));
    vm.put("v13", em.op("isub").eval(vm.get("v11"),vm.get("v0")));
    vm.put("v14", em.op("iadd").eval(vm.get("v13"),vm.get("v1")));
    vm.put("v15", em.op("isub").eval(vm.get("v11"),vm.get("v13")));
    vm.put("v16", om.getArrayElement(vm.get("v6"), vm.get("v12")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.popArrayElement(vm.get("v6"), vm.get("v14")));
    vm.put("v19", em.op("isub").eval(vm.get("v15"),vm.get("v0")));
    vm.put("v20", em.op("iadd").eval(vm.get("v13"),vm.get("v0")));
    vm.put("v21", em.op("iadd").eval(vm.get("v20"),vm.get("v1")));
    vm.put("v22", om.getArrayElement(vm.get("v6"), vm.get("v21")));
    vm.put("v23", vm.get("v22"));
    if (!em.gt(vm.get("v19"), vm.get("v1"))) e.abort(String.format("!((v19=%s) gt (v1=%s))", vm.get("v19"), vm.get("v1")));
    if (!em.ge(vm.get("v0"), vm.get("v19"))) e.abort(String.format("!((v0=%s) ge (v19=%s))", vm.get("v0"), vm.get("v19")));
    if (!em.lt(vm.get("v13"), vm.get("v11"))) e.abort(String.format("!((v13=%s) lt (v11=%s))", vm.get("v13"), vm.get("v11")));
    if (!em.lt(vm.get("v12"), vm.get("v11"))) e.abort(String.format("!((v12=%s) lt (v11=%s))", vm.get("v12"), vm.get("v11")));
    if (!em.ge(vm.get("v13"), vm.get("v1"))) e.abort(String.format("!((v13=%s) ge (v1=%s))", vm.get("v13"), vm.get("v1")));
    if (!em.ne(vm.get("v11"), vm.get("v1"))) e.abort(String.format("!((v11=%s) ne (v1=%s))", vm.get("v11"), vm.get("v1")));
    if (!em.lt(vm.get("v1"), vm.get("v19"))) e.abort(String.format("!((v1=%s) lt (v19=%s))", vm.get("v1"), vm.get("v19")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  static void remove4570(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", (int)0);
    vm.put("v5", null);
    vm.put("v6", vm.get("v0"));
    vm.put("v7", em.op("isub").eval(vm.get("v6"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v7"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.ne(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) ne (v5=%s))", vm.get("param0"), vm.get("v5")));
    if (!em.ge(vm.get("v4"), vm.get("v3"))) e.abort(String.format("!((v4=%s) ge (v3=%s))", vm.get("v4"), vm.get("v3")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1325(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", (int)2);
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", om.getField(vm.get("this"), "elementCount"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getArrayElement(vm.get("v7"), vm.get("v2")));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", (int)7);
    vm.put("v13", (int)3);
    vm.put("v14", java.lang.Object[].class);
    vm.put("v15", null);
    vm.put("v16", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v7")));
    vm.put("v17", om.getArrayElement(vm.get("v7"), vm.get("v0")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", em.op("isub").eval(vm.get("v5"),vm.get("v0")));
    vm.put("v20", em.op("isub").eval(vm.get("v5"),vm.get("v0")));
    vm.put("v21", om.getArrayElement(vm.get("v7"), vm.get("v1")));
    vm.put("v22", vm.get("v21"));
    vm.put("v23", om.getArrayLength(vm.get("v7")));
    vm.put("v24", vm.get("v23"));
    vm.put("v25", em.op("isub").eval(vm.get("v24"),vm.get("v0")));
    vm.put("v26", vm.get("v3"));
    vm.put("v27", em.op("isub").eval(vm.get("v26"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v27"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.lt(vm.get("v0"), vm.get("v5"))) e.abort(String.format("!((v0=%s) lt (v5=%s))", vm.get("v0"), vm.get("v5")));
    if (!em.lt(vm.get("v1"), vm.get("v25"))) e.abort(String.format("!((v1=%s) lt (v25=%s))", vm.get("v1"), vm.get("v25")));
    if (!em.lt(vm.get("v0"), vm.get("v25"))) e.abort(String.format("!((v0=%s) lt (v25=%s))", vm.get("v0"), vm.get("v25")));
    if (!em.ge(vm.get("v20"), vm.get("v0"))) e.abort(String.format("!((v20=%s) ge (v0=%s))", vm.get("v20"), vm.get("v0")));
    if (!em.lt(vm.get("v2"), vm.get("v25"))) e.abort(String.format("!((v2=%s) lt (v25=%s))", vm.get("v2"), vm.get("v25")));
    if (!em.le(vm.get("v5"), vm.get("v24"))) e.abort(String.format("!((v5=%s) le (v24=%s))", vm.get("v5"), vm.get("v24")));
    if (!em.ne(vm.get("v16"), vm.get("v14"))) e.abort(String.format("!((v16=%s) ne (v14=%s))", vm.get("v16"), vm.get("v14")));
    if (!em.le(vm.get("v0"), vm.get("v5"))) e.abort(String.format("!((v0=%s) le (v5=%s))", vm.get("v0"), vm.get("v5")));
    if (!em.lt(vm.get("v19"), vm.get("v12"))) e.abort(String.format("!((v19=%s) lt (v12=%s))", vm.get("v19"), vm.get("v12")));
    if (!em.eq(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) eq (v15=%s))", vm.get("param0"), vm.get("v15")));
    if (!em.ge(vm.get("v1"), vm.get("v5"))) e.abort(String.format("!((v1=%s) ge (v5=%s))", vm.get("v1"), vm.get("v5")));
    if (!em.ne(vm.get("v9"), vm.get("v0"))) e.abort(String.format("!((v9=%s) ne (v0=%s))", vm.get("v9"), vm.get("v0")));
    if (!em.ge(vm.get("v13"), vm.get("v25"))) e.abort(String.format("!((v13=%s) ge (v25=%s))", vm.get("v13"), vm.get("v25")));
    if (!em.le(vm.get("v25"), vm.get("v20"))) e.abort(String.format("!((v25=%s) le (v20=%s))", vm.get("v25"), vm.get("v20")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1303(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getArrayLength(vm.get("v3")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "elementCount"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)7);
    vm.put("v12", null);
    vm.put("v13", java.lang.Object[].class);
    vm.put("v14", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v3")));
    vm.put("v15", em.op("isub").eval(vm.get("v7"),vm.get("v10")));
    vm.put("v16", em.op("isub").eval(vm.get("v9"),vm.get("v10")));
    vm.put("v17", em.op("isub").eval(vm.get("v9"),vm.get("v10")));
    vm.put("v18", vm.get("v4"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v5")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ge(vm.get("v17"), vm.get("v10"))) e.abort(String.format("!((v17=%s) ge (v10=%s))", vm.get("v17"), vm.get("v10")));
    if (!em.le(vm.get("v10"), vm.get("v9"))) e.abort(String.format("!((v10=%s) le (v9=%s))", vm.get("v10"), vm.get("v9")));
    if (!em.eq(vm.get("v14"), vm.get("v13"))) e.abort(String.format("!((v14=%s) eq (v13=%s))", vm.get("v14"), vm.get("v13")));
    if (!em.ge(vm.get("v10"), vm.get("v15"))) e.abort(String.format("!((v10=%s) ge (v15=%s))", vm.get("v10"), vm.get("v15")));
    if (!em.ge(vm.get("v10"), vm.get("v9"))) e.abort(String.format("!((v10=%s) ge (v9=%s))", vm.get("v10"), vm.get("v9")));
    if (!em.le(vm.get("v15"), vm.get("v17"))) e.abort(String.format("!((v15=%s) le (v17=%s))", vm.get("v15"), vm.get("v17")));
    if (!em.lt(vm.get("v16"), vm.get("v11"))) e.abort(String.format("!((v16=%s) lt (v11=%s))", vm.get("v16"), vm.get("v11")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ne(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) ne (v10=%s))", vm.get("v1"), vm.get("v10")));
    if (!em.le(vm.get("v9"), vm.get("v7"))) e.abort(String.format("!((v9=%s) le (v7=%s))", vm.get("v9"), vm.get("v7")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("java.util.TimSort.$assertionsDisabled"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)32);
    vm.put("v12", (int)2);
    vm.put("v13", null);
    vm.put("v14", om.getArrayLength(vm.get("v5")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", em.op("isub").eval(vm.get("v3"),vm.get("v10")));
    vm.put("v17", vm.get("v0"));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v18"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.lt(vm.get("v16"), vm.get("v11"))) e.abort(String.format("!((v16=%s) lt (v11=%s))", vm.get("v16"), vm.get("v11")));
    if (!em.ge(vm.get("v1"), vm.get("v3"))) e.abort(String.format("!((v1=%s) ge (v3=%s))", vm.get("v1"), vm.get("v3")));
    if (!em.eq(vm.get("v9"), vm.get("v10"))) e.abort(String.format("!((v9=%s) eq (v10=%s))", vm.get("v9"), vm.get("v10")));
    if (!em.le(vm.get("v10"), vm.get("v3"))) e.abort(String.format("!((v10=%s) le (v3=%s))", vm.get("v10"), vm.get("v3")));
    if (!em.eq(vm.get("v7"), vm.get("v10"))) e.abort(String.format("!((v7=%s) eq (v10=%s))", vm.get("v7"), vm.get("v10")));
    if (!em.ge(vm.get("v16"), vm.get("v12"))) e.abort(String.format("!((v16=%s) ge (v12=%s))", vm.get("v16"), vm.get("v12")));
    if (!em.ne(vm.get("v7"), vm.get("v10"))) e.abort(String.format("!((v7=%s) ne (v10=%s))", vm.get("v7"), vm.get("v10")));
    if (!em.ne(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) ne (v13=%s))", vm.get("param0"), vm.get("v13")));
    if (!em.le(vm.get("v3"), vm.get("v15"))) e.abort(String.format("!((v3=%s) le (v15=%s))", vm.get("v3"), vm.get("v15")));
    if (!em.le(vm.get("v1"), vm.get("v3"))) e.abort(String.format("!((v1=%s) le (v3=%s))", vm.get("v1"), vm.get("v3")));
    if (!em.eq(vm.get("v1"), vm.get("v3"))) e.abort(String.format("!((v1=%s) eq (v3=%s))", vm.get("v1"), vm.get("v3")));
    if (!em.ne(vm.get("v7"), vm.get("v10"))) e.abort(String.format("!((v7=%s) ne (v10=%s))", vm.get("v7"), vm.get("v10")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1385(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "modCount"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayLength(vm.get("v4")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("this"), "elementCount"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)7);
    vm.put("v12", null);
    vm.put("v13", java.lang.Object[].class);
    vm.put("v14", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    vm.put("v15", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    vm.put("v16", em.op("isub").eval(vm.get("v6"),vm.get("v0")));
    vm.put("v17", om.getArrayElement(vm.get("v4"), vm.get("v0")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v4")));
    vm.put("v20", vm.get("v1"));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v21"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ge(vm.get("v0"), vm.get("v10"))) e.abort(String.format("!((v0=%s) ge (v10=%s))", vm.get("v0"), vm.get("v10")));
    if (!em.ge(vm.get("v2"), vm.get("v15"))) e.abort(String.format("!((v2=%s) ge (v15=%s))", vm.get("v2"), vm.get("v15")));
    if (!em.ge(vm.get("v15"), vm.get("v0"))) e.abort(String.format("!((v15=%s) ge (v0=%s))", vm.get("v15"), vm.get("v0")));
    if (!em.lt(vm.get("v14"), vm.get("v11"))) e.abort(String.format("!((v14=%s) lt (v11=%s))", vm.get("v14"), vm.get("v11")));
    if (!em.le(vm.get("v0"), vm.get("v10"))) e.abort(String.format("!((v0=%s) le (v10=%s))", vm.get("v0"), vm.get("v10")));
    if (!em.eq(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) eq (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ne(vm.get("v8"), vm.get("v0"))) e.abort(String.format("!((v8=%s) ne (v0=%s))", vm.get("v8"), vm.get("v0")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.gt(vm.get("v16"), vm.get("v15"))) e.abort(String.format("!((v16=%s) gt (v15=%s))", vm.get("v16"), vm.get("v15")));
    if (!em.le(vm.get("v10"), vm.get("v6"))) e.abort(String.format("!((v10=%s) le (v6=%s))", vm.get("v10"), vm.get("v6")));
    if (!em.lt(vm.get("v0"), vm.get("v15"))) e.abort(String.format("!((v0=%s) lt (v15=%s))", vm.get("v0"), vm.get("v15")));
    if (!em.ne(vm.get("v19"), vm.get("v13"))) e.abort(String.format("!((v19=%s) ne (v13=%s))", vm.get("v19"), vm.get("v13")));
  }
  @IM(clazz="java.util.Vector", name="removeElementAt", desc="(I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void removeElementAt2451(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)3);
    vm.put("v2", (int)2);
    vm.put("v3", (int)4);
    vm.put("v4", (int)0);
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", null);
    vm.put("v9", om.getField(vm.get("this"), "elementCount"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)5);
    vm.put("v12", om.popArrayElement(vm.get("v7"), vm.get("v10")));
    vm.put("v13", vm.get("v5"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    vm.put("v15", em.op("iadd").eval(vm.get("v10"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v15"));
    vm.put("v16", om.guessArrayIndex(vm.get("v7")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.popArrayElement(vm.get("v7"), vm.get("v17")));
    vm.put("param0", em.op("isub").eval(vm.get("v17"),vm.get("v4")));
    vm.put("v19", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v20", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v21", em.op("isub").eval(vm.get("v15"),vm.get("param0")));
    vm.put("v22", em.op("isub").eval(vm.get("v21"),vm.get("v0")));
    vm.put("v23", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v24", em.op("iadd").eval(vm.get("v23"),vm.get("v3")));
    vm.put("v25", em.op("iadd").eval(vm.get("v23"),vm.get("v0")));
    vm.put("v26", em.op("iadd").eval(vm.get("v23"),vm.get("v2")));
    vm.put("v27", om.getArrayElement(vm.get("v7"), vm.get("v26")));
    vm.put("v28", vm.get("v27"));
    vm.put("v29", em.op("iadd").eval(vm.get("v23"),vm.get("v4")));
    vm.put("v30", om.getArrayElement(vm.get("v7"), vm.get("v25")));
    vm.put("v31", vm.get("v30"));
    vm.put("v32", em.op("iadd").eval(vm.get("v23"),vm.get("v1")));
    vm.put("v33", om.getArrayElement(vm.get("v7"), vm.get("v32")));
    vm.put("v34", vm.get("v33"));
    vm.put("v35", om.getArrayElement(vm.get("v7"), vm.get("v24")));
    vm.put("v36", vm.get("v35"));
    vm.put("v37", om.popArrayElement(vm.get("v7"), vm.get("v20")));
    vm.put("v38", em.op("iadd").eval(vm.get("param0"),vm.get("v3")));
    vm.put("v39", om.popArrayElement(vm.get("v7"), vm.get("v38")));
    vm.put("v40", om.popArrayElement(vm.get("v7"), vm.get("v19")));
    vm.put("v41", em.op("iadd").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v42", om.popArrayElement(vm.get("v7"), vm.get("v41")));
    vm.put("v43", om.getArrayElement(vm.get("v7"), vm.get("v29")));
    vm.put("v44", vm.get("v43"));
    if (!em.ge(vm.get("v11"), vm.get("v22"))) e.abort(String.format("!((v11=%s) ge (v22=%s))", vm.get("v11"), vm.get("v22")));
    if (!em.lt(vm.get("v0"), vm.get("v22"))) e.abort(String.format("!((v0=%s) lt (v22=%s))", vm.get("v0"), vm.get("v22")));
    if (!em.lt(vm.get("v4"), vm.get("v22"))) e.abort(String.format("!((v4=%s) lt (v22=%s))", vm.get("v4"), vm.get("v22")));
    if (!em.lt(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) lt (v15=%s))", vm.get("param0"), vm.get("v15")));
    if (!em.lt(vm.get("v3"), vm.get("v22"))) e.abort(String.format("!((v3=%s) lt (v22=%s))", vm.get("v3"), vm.get("v22")));
    if (!em.lt(vm.get("v1"), vm.get("v22"))) e.abort(String.format("!((v1=%s) lt (v22=%s))", vm.get("v1"), vm.get("v22")));
    if (!em.gt(vm.get("v22"), vm.get("v4"))) e.abort(String.format("!((v22=%s) gt (v4=%s))", vm.get("v22"), vm.get("v4")));
    if (!em.ge(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) ge (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.lt(vm.get("v2"), vm.get("v22"))) e.abort(String.format("!((v2=%s) lt (v22=%s))", vm.get("v2"), vm.get("v22")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void remove3893(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", null);
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", om.getField(vm.get("this"), "elementCount"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)2);
    vm.put("v9", om.popArrayElement(vm.get("v4"), vm.get("v7")));
    vm.put("v10", em.op("iadd").eval(vm.get("v7"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v10"));
    vm.put("v11", vm.get("v5"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", om.guessArrayIndex(vm.get("v4")));
    vm.put("param0", vm.get("v13"));
    vm.put("v14", om.getArrayElement(vm.get("v4"), vm.get("param0")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", em.op("isub").eval(vm.get("v10"),vm.get("param0")));
    vm.put("v17", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v18", em.op("iadd").eval(vm.get("v17"),vm.get("v0")));
    vm.put("v19", em.op("iadd").eval(vm.get("v17"),vm.get("v1")));
    vm.put("v20", om.getArrayElement(vm.get("v4"), vm.get("v18")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.getArrayElement(vm.get("v4"), vm.get("v19")));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v25", om.popArrayElement(vm.get("v4"), vm.get("v24")));
    vm.put("v26", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v27", om.popArrayElement(vm.get("v4"), vm.get("v26")));
    vm.put("v28", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    if (!em.lt(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) lt (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.lt(vm.get("v1"), vm.get("v28"))) e.abort(String.format("!((v1=%s) lt (v28=%s))", vm.get("v1"), vm.get("v28")));
    if (!em.ge(vm.get("v8"), vm.get("v28"))) e.abort(String.format("!((v8=%s) ge (v28=%s))", vm.get("v8"), vm.get("v28")));
    if (!em.lt(vm.get("v0"), vm.get("v28"))) e.abort(String.format("!((v0=%s) lt (v28=%s))", vm.get("v0"), vm.get("v28")));
    if (!em.gt(vm.get("v28"), vm.get("v1"))) e.abort(String.format("!((v28=%s) gt (v1=%s))", vm.get("v28"), vm.get("v1")));
  }
  @IM(clazz="java.util.Vector", name="insertElementAt", desc="(Ljava/lang/Object;I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void insertElementAt1794(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)2);
    vm.put("v2", (int)0);
    vm.put("v3", om.getField(vm.get("this"), "elementCount"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)3);
    vm.put("v8", om.getArrayLength(vm.get("v6")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", vm.get("v4"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    vm.put("v12", vm.get("v3"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v13"));
    vm.put("v14", em.op("iadd").eval(vm.get("v13"),vm.get("v0")));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v9")));
    vm.put("v16", om.guessArrayIndex(vm.get("v6")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.popArrayElement(vm.get("v6"), vm.get("v17")));
    vm.put("v19", em.op("isub").eval(vm.get("v17"),vm.get("v1")));
    vm.put("v20", em.op("iadd").eval(vm.get("v19"),vm.get("v2")));
    vm.put("v21", om.popArrayElement(vm.get("v6"), vm.get("v20")));
    vm.put("v22", em.op("iadd").eval(vm.get("v19"),vm.get("v0")));
    vm.put("v23", om.popArrayElement(vm.get("v6"), vm.get("v22")));
    vm.put("param1", em.op("isub").eval(vm.get("v19"),vm.get("v0")));
    vm.put("v24", em.op("iadd").eval(vm.get("param1"),vm.get("v0")));
    vm.put("v25", om.getArrayElement(vm.get("v6"), vm.get("v24")));
    vm.put("v26", vm.get("v25"));
    vm.put("v27", em.op("iadd").eval(vm.get("param1"),vm.get("v2")));
    vm.put("v28", om.getArrayElement(vm.get("v6"), vm.get("v27")));
    vm.put("v29", vm.get("v28"));
    vm.put("v30", em.op("isub").eval(vm.get("v13"),vm.get("param1")));
    vm.put("v31", om.popArrayElement(vm.get("v6"), vm.get("param1")));
    vm.put("param0", vm.get("v31"));
    vm.put("v32", em.op("iadd").eval(vm.get("param1"),vm.get("v1")));
    vm.put("v33", om.getArrayElement(vm.get("v6"), vm.get("v32")));
    vm.put("v34", vm.get("v33"));
    if (!em.le(vm.get("v15"), vm.get("v2"))) e.abort(String.format("!((v15=%s) le (v2=%s))", vm.get("v15"), vm.get("v2")));
    if (!em.lt(vm.get("v2"), vm.get("v30"))) e.abort(String.format("!((v2=%s) lt (v30=%s))", vm.get("v2"), vm.get("v30")));
    if (!em.le(vm.get("param1"), vm.get("v13"))) e.abort(String.format("!((param1=%s) le (v13=%s))", vm.get("param1"), vm.get("v13")));
    if (!em.ge(vm.get("v7"), vm.get("v30"))) e.abort(String.format("!((v7=%s) ge (v30=%s))", vm.get("v7"), vm.get("v30")));
    if (!em.lt(vm.get("v1"), vm.get("v30"))) e.abort(String.format("!((v1=%s) lt (v30=%s))", vm.get("v1"), vm.get("v30")));
    if (!em.lt(vm.get("v0"), vm.get("v30"))) e.abort(String.format("!((v0=%s) lt (v30=%s))", vm.get("v0"), vm.get("v30")));
  }
  @IM(clazz="java.util.Vector", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void add193(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "elementData"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "elementCount"));
    vm.put("v6", om.getArrayLength(vm.get("v2")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", vm.get("v5"));
    vm.put("v9", em.op("isub").eval(vm.get("v8"),vm.get("v3")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v9"));
    vm.put("v10", em.op("iadd").eval(vm.get("v9"),vm.get("v3")));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v7")));
    vm.put("v12", vm.get("v4"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    vm.put("v14", om.guessArrayIndex(vm.get("v2")));
    vm.put("param0", vm.get("v14"));
    vm.put("v15", om.popArrayElement(vm.get("v2"), vm.get("param0")));
    vm.put("param1", vm.get("v15"));
    vm.put("v16", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v17", em.op("isub").eval(vm.get("v9"),vm.get("param0")));
    vm.put("v18", em.op("iadd").eval(vm.get("param0"),vm.get("v3")));
    vm.put("v19", em.op("iadd").eval(vm.get("v18"),vm.get("v0")));
    vm.put("v20", om.popArrayElement(vm.get("v2"), vm.get("v19")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.getArrayElement(vm.get("v2"), vm.get("v16")));
    if (!em.le(vm.get("v11"), vm.get("v0"))) e.abort(String.format("!((v11=%s) le (v0=%s))", vm.get("v11"), vm.get("v0")));
    if (!em.le(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) le (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.lt(vm.get("v0"), vm.get("v17"))) e.abort(String.format("!((v0=%s) lt (v17=%s))", vm.get("v0"), vm.get("v17")));
    if (!em.ge(vm.get("v3"), vm.get("v17"))) e.abort(String.format("!((v3=%s) ge (v17=%s))", vm.get("v3"), vm.get("v17")));
  }
  @IM(clazz="java.util.Vector", name="removeElement", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  static void removeElement4897(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", (int)0);
    vm.put("v5", null);
    vm.put("v6", vm.get("v2"));
    vm.put("v7", em.op("isub").eval(vm.get("v6"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v7"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.ge(vm.get("v4"), vm.get("v1"))) e.abort(String.format("!((v4=%s) ge (v1=%s))", vm.get("v4"), vm.get("v1")));
    if (!em.ne(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) ne (v5=%s))", vm.get("param0"), vm.get("v5")));
  }
  @IM(clazz="java.util.Vector", name="clear", desc="()V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void clear2893(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", null);
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.popArrayElement(vm.get("v6"), vm.get("v1")));
    vm.put("v8", vm.get("v2"));
    vm.put("v9", em.op("isub").eval(vm.get("v8"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v9"));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v10", om.getField(vm.get("this"), "elementCount"));
    if (!em.ge(vm.get("v3"), vm.get("v10"))) e.abort(String.format("!((v3=%s) ge (v10=%s))", vm.get("v3"), vm.get("v10")));
    if (!em.lt(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) lt (v10=%s))", vm.get("v1"), vm.get("v10")));
  }
  @IM(clazz="java.util.Stack", name="<init>", desc="()V",definedOutput={"this.modCount","this.elementData","this.capacityIncrement"},delta={"this.modCount","this.elementData","this.capacityIncrement"})
  @DG(succ={"sort1355","insertElementAt1799","remove3803","removeElementAt2485","remove3895","remove4599","removeRange532","sort1380","pop3238","remove4579","sort1371","sort1367","add198","remove4566","insertElementAt1798","sort1303","sort1323","removeAllElements4693","removeAllElements4698","remove4587","sort1344","removeRange536","setSize4098","remove3897","sort1325","pop3299","removeElement4818","sort1336","removeElement4895","removeRange510","setSize4027","sort1384","sort1396","removeElementAt2479","insertElementAt1794","addElement699","clear2861","setSize4097","pop3297","removeAllElements4679","set3999","remove3896","sort1334","add2691","setElementAt2999","removeRange528","clear2899","pop3298","removeElement4866","push5097","add195","removeRange587","sort1386","pop3289","sort1399","clear2876","setSize4090","sort1313","pop3271","add189","sort1385","insertElementAt1750","removeRange518","insertElementAt1764","removeRange565","remove4574","sort1374","removeRange596","removeRange562","insertElementAt1793","removeRange576","remove3893","pop3235","remove3817","removeRange591","add193","sort1397","clear2878","sort1353","removeAllElements4696","removeAllElements4660","clear2893","removeElement4824","removeRange517","removeElement4840","removeElement4861","remove4578","sort1350","removeRange586","removeElement4890","removeRange523","clear2867","sort1327","remove4560","trimToSize999","removeRange595","removeElement4870","sort1356","removeAllElements4665","sort1341","removeElementAt2495","setSize4030","removeElementAt2451","removeRange552","removeElementAt2499","remove3899","setSize4068"})
  static void _init_2599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "capacityIncrement"));
    vm.put("v1", (int)0);
    vm.put("v2", om.popField(vm.get("this"), "modCount"));
    vm.put("v3", (int)10);
    vm.put("v4", om.popField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getArrayLength(vm.get("v5")));
    if (!em.eq(vm.get("v1"), (vm.get("v2")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v2")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v3"), (vm.get("v6")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v6")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void remove3803(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)3);
    vm.put("v2", (int)0);
    vm.put("v3", (int)2);
    vm.put("v4", (int)4);
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", null);
    vm.put("v9", om.getField(vm.get("this"), "elementCount"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)5);
    vm.put("v12", om.popArrayElement(vm.get("v7"), vm.get("v10")));
    vm.put("v13", vm.get("v5"));
    vm.put("v14", em.op("iadd").eval(vm.get("v10"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v14"));
    vm.put("v15", em.op("isub").eval(vm.get("v13"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    vm.put("v16", om.guessArrayIndex(vm.get("v7")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.popArrayElement(vm.get("v7"), vm.get("v17")));
    vm.put("param0", em.op("isub").eval(vm.get("v17"),vm.get("v3")));
    vm.put("v19", em.op("iadd").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v20", om.popArrayElement(vm.get("v7"), vm.get("v19")));
    vm.put("v21", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v22", om.getArrayElement(vm.get("v7"), vm.get("param0")));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.popArrayElement(vm.get("v7"), vm.get("v21")));
    vm.put("v25", em.op("iadd").eval(vm.get("param0"),vm.get("v4")));
    vm.put("v26", om.popArrayElement(vm.get("v7"), vm.get("v25")));
    vm.put("v27", em.op("isub").eval(vm.get("v14"),vm.get("param0")));
    vm.put("v28", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v29", om.popArrayElement(vm.get("v7"), vm.get("v28")));
    vm.put("v30", em.op("isub").eval(vm.get("v27"),vm.get("v0")));
    vm.put("v31", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v32", em.op("iadd").eval(vm.get("v31"),vm.get("v1")));
    vm.put("v33", em.op("iadd").eval(vm.get("v31"),vm.get("v0")));
    vm.put("v34", om.getArrayElement(vm.get("v7"), vm.get("v33")));
    vm.put("v35", vm.get("v34"));
    vm.put("v36", em.op("iadd").eval(vm.get("v31"),vm.get("v4")));
    vm.put("v37", om.getArrayElement(vm.get("v7"), vm.get("v36")));
    vm.put("v38", vm.get("v37"));
    vm.put("v39", om.getArrayElement(vm.get("v7"), vm.get("v32")));
    vm.put("v40", vm.get("v39"));
    vm.put("v41", em.op("iadd").eval(vm.get("v31"),vm.get("v2")));
    vm.put("v42", om.getArrayElement(vm.get("v7"), vm.get("v41")));
    vm.put("v43", vm.get("v42"));
    vm.put("v44", em.op("iadd").eval(vm.get("v31"),vm.get("v3")));
    vm.put("v45", om.getArrayElement(vm.get("v7"), vm.get("v44")));
    vm.put("v46", vm.get("v45"));
    if (!em.ge(vm.get("v11"), vm.get("v30"))) e.abort(String.format("!((v11=%s) ge (v30=%s))", vm.get("v11"), vm.get("v30")));
    if (!em.lt(vm.get("v3"), vm.get("v30"))) e.abort(String.format("!((v3=%s) lt (v30=%s))", vm.get("v3"), vm.get("v30")));
    if (!em.lt(vm.get("v0"), vm.get("v30"))) e.abort(String.format("!((v0=%s) lt (v30=%s))", vm.get("v0"), vm.get("v30")));
    if (!em.lt(vm.get("v2"), vm.get("v30"))) e.abort(String.format("!((v2=%s) lt (v30=%s))", vm.get("v2"), vm.get("v30")));
    if (!em.lt(vm.get("v1"), vm.get("v30"))) e.abort(String.format("!((v1=%s) lt (v30=%s))", vm.get("v1"), vm.get("v30")));
    if (!em.lt(vm.get("v4"), vm.get("v30"))) e.abort(String.format("!((v4=%s) lt (v30=%s))", vm.get("v4"), vm.get("v30")));
    if (!em.gt(vm.get("v30"), vm.get("v2"))) e.abort(String.format("!((v30=%s) gt (v2=%s))", vm.get("v30"), vm.get("v2")));
    if (!em.lt(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) lt (v14=%s))", vm.get("param0"), vm.get("v14")));
  }
  @IM(clazz="java.util.Stack", name="pop", desc="()Ljava/lang/Object;",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void pop3289(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", om.getField(vm.get("this"), "elementData"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "elementCount"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)0);
    vm.put("v8", om.popArrayElement(vm.get("v2"), vm.get("v6")));
    vm.put("v9", em.op("iadd").eval(vm.get("v6"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v9"));
    vm.put("v10", em.op("isub").eval(vm.get("v9"),vm.get("v0")));
    vm.put("v11", em.op("isub").eval(vm.get("v9"),vm.get("v10")));
    vm.put("v12", em.op("isub").eval(vm.get("v9"),vm.get("v0")));
    vm.put("v13", om.getArrayElement(vm.get("v2"), vm.get("v12")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v11"),vm.get("v0")));
    vm.put("v16", vm.get("v4"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    if (!em.lt(vm.get("v12"), vm.get("v9"))) e.abort(String.format("!((v12=%s) lt (v9=%s))", vm.get("v12"), vm.get("v9")));
    if (!em.ge(vm.get("v10"), vm.get("v7"))) e.abort(String.format("!((v10=%s) ge (v7=%s))", vm.get("v10"), vm.get("v7")));
    if (!em.lt(vm.get("v10"), vm.get("v9"))) e.abort(String.format("!((v10=%s) lt (v9=%s))", vm.get("v10"), vm.get("v9")));
    if (!em.gt(vm.get("v15"), vm.get("v7"))) e.abort(String.format("!((v15=%s) gt (v7=%s))", vm.get("v15"), vm.get("v7")));
    if (!em.ge(vm.get("v7"), vm.get("v15"))) e.abort(String.format("!((v7=%s) ge (v15=%s))", vm.get("v7"), vm.get("v15")));
    if (!em.ne(vm.get("v9"), vm.get("v7"))) e.abort(String.format("!((v9=%s) ne (v7=%s))", vm.get("v9"), vm.get("v7")));
  }
  @IM(clazz="java.util.Stack", name="pop", desc="()Ljava/lang/Object;",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void pop3271(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)2);
    vm.put("v2", (int)0);
    vm.put("v3", (int)3);
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementCount"));
    vm.put("v7", null);
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", (int)4);
    vm.put("v10", vm.get("v8"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    vm.put("v12", vm.get("v6"));
    vm.put("v13", om.popArrayElement(vm.get("v5"), vm.get("v12")));
    vm.put("v14", em.op("iadd").eval(vm.get("v12"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v14"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    vm.put("v16", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    vm.put("v17", em.op("iadd").eval(vm.get("v16"),vm.get("v0")));
    vm.put("v18", em.op("iadd").eval(vm.get("v16"),vm.get("v3")));
    vm.put("v19", em.op("iadd").eval(vm.get("v16"),vm.get("v2")));
    vm.put("v20", om.popArrayElement(vm.get("v5"), vm.get("v19")));
    vm.put("v21", em.op("iadd").eval(vm.get("v16"),vm.get("v0")));
    vm.put("v22", em.op("iadd").eval(vm.get("v21"),vm.get("v1")));
    vm.put("v23", em.op("iadd").eval(vm.get("v21"),vm.get("v0")));
    vm.put("v24", em.op("iadd").eval(vm.get("v21"),vm.get("v2")));
    vm.put("v25", om.getArrayElement(vm.get("v5"), vm.get("v24")));
    vm.put("v26", vm.get("v25"));
    vm.put("v27", om.popArrayElement(vm.get("v5"), vm.get("v18")));
    vm.put("v28", em.op("iadd").eval(vm.get("v16"),vm.get("v1")));
    vm.put("v29", em.op("iadd").eval(vm.get("v21"),vm.get("v3")));
    vm.put("v30", em.op("isub").eval(vm.get("v14"),vm.get("v16")));
    vm.put("v31", em.op("isub").eval(vm.get("v30"),vm.get("v0")));
    vm.put("v32", om.getArrayElement(vm.get("v5"), vm.get("v29")));
    vm.put("v33", vm.get("v32"));
    vm.put("v34", om.popArrayElement(vm.get("v5"), vm.get("v17")));
    vm.put("v35", vm.get("v34"));
    vm.put("v36", om.getArrayElement(vm.get("v5"), vm.get("v15")));
    vm.put("v37", vm.get("v36"));
    vm.put("v38", om.popArrayElement(vm.get("v5"), vm.get("v28")));
    vm.put("v39", vm.get("v38"));
    vm.put("v40", om.getArrayElement(vm.get("v5"), vm.get("v23")));
    vm.put("v41", om.getArrayElement(vm.get("v5"), vm.get("v22")));
    if (!em.lt(vm.get("v0"), vm.get("v31"))) e.abort(String.format("!((v0=%s) lt (v31=%s))", vm.get("v0"), vm.get("v31")));
    if (!em.lt(vm.get("v15"), vm.get("v14"))) e.abort(String.format("!((v15=%s) lt (v14=%s))", vm.get("v15"), vm.get("v14")));
    if (!em.ne(vm.get("v14"), vm.get("v2"))) e.abort(String.format("!((v14=%s) ne (v2=%s))", vm.get("v14"), vm.get("v2")));
    if (!em.gt(vm.get("v31"), vm.get("v2"))) e.abort(String.format("!((v31=%s) gt (v2=%s))", vm.get("v31"), vm.get("v2")));
    if (!em.lt(vm.get("v3"), vm.get("v31"))) e.abort(String.format("!((v3=%s) lt (v31=%s))", vm.get("v3"), vm.get("v31")));
    if (!em.ge(vm.get("v16"), vm.get("v2"))) e.abort(String.format("!((v16=%s) ge (v2=%s))", vm.get("v16"), vm.get("v2")));
    if (!em.lt(vm.get("v16"), vm.get("v14"))) e.abort(String.format("!((v16=%s) lt (v14=%s))", vm.get("v16"), vm.get("v14")));
    if (!em.lt(vm.get("v2"), vm.get("v31"))) e.abort(String.format("!((v2=%s) lt (v31=%s))", vm.get("v2"), vm.get("v31")));
    if (!em.ge(vm.get("v9"), vm.get("v31"))) e.abort(String.format("!((v9=%s) ge (v31=%s))", vm.get("v9"), vm.get("v31")));
    if (!em.lt(vm.get("v1"), vm.get("v31"))) e.abort(String.format("!((v1=%s) lt (v31=%s))", vm.get("v1"), vm.get("v31")));
  }
  @IM(clazz="java.util.Vector", name="removeElementAt", desc="(I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void removeElementAt2495(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)3);
    vm.put("v2", (int)0);
    vm.put("v3", (int)2);
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", null);
    vm.put("v7", om.getField(vm.get("this"), "elementCount"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("this"), "modCount"));
    vm.put("v10", (int)4);
    vm.put("v11", om.popArrayElement(vm.get("v5"), vm.get("v8")));
    vm.put("v12", vm.get("v9"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    vm.put("v14", em.op("iadd").eval(vm.get("v8"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v14"));
    vm.put("v15", om.guessArrayIndex(vm.get("v5")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.popArrayElement(vm.get("v5"), vm.get("v16")));
    vm.put("param0", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    vm.put("v18", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v19", om.popArrayElement(vm.get("v5"), vm.get("v18")));
    vm.put("v20", em.op("iadd").eval(vm.get("param0"),vm.get("v3")));
    vm.put("v21", om.popArrayElement(vm.get("v5"), vm.get("v20")));
    vm.put("v22", em.op("isub").eval(vm.get("v14"),vm.get("param0")));
    vm.put("v23", em.op("isub").eval(vm.get("v22"),vm.get("v0")));
    vm.put("v24", em.op("iadd").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v25", om.popArrayElement(vm.get("v5"), vm.get("v24")));
    vm.put("v26", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v27", em.op("iadd").eval(vm.get("v26"),vm.get("v2")));
    vm.put("v28", em.op("iadd").eval(vm.get("v26"),vm.get("v0")));
    vm.put("v29", om.getArrayElement(vm.get("v5"), vm.get("v27")));
    vm.put("v30", vm.get("v29"));
    vm.put("v31", om.getArrayElement(vm.get("v5"), vm.get("v28")));
    vm.put("v32", vm.get("v31"));
    vm.put("v33", em.op("iadd").eval(vm.get("v26"),vm.get("v3")));
    vm.put("v34", om.getArrayElement(vm.get("v5"), vm.get("v33")));
    vm.put("v35", vm.get("v34"));
    vm.put("v36", em.op("iadd").eval(vm.get("v26"),vm.get("v1")));
    vm.put("v37", om.getArrayElement(vm.get("v5"), vm.get("v36")));
    vm.put("v38", vm.get("v37"));
    if (!em.gt(vm.get("v23"), vm.get("v2"))) e.abort(String.format("!((v23=%s) gt (v2=%s))", vm.get("v23"), vm.get("v2")));
    if (!em.lt(vm.get("v2"), vm.get("v23"))) e.abort(String.format("!((v2=%s) lt (v23=%s))", vm.get("v2"), vm.get("v23")));
    if (!em.lt(vm.get("v0"), vm.get("v23"))) e.abort(String.format("!((v0=%s) lt (v23=%s))", vm.get("v0"), vm.get("v23")));
    if (!em.lt(vm.get("v3"), vm.get("v23"))) e.abort(String.format("!((v3=%s) lt (v23=%s))", vm.get("v3"), vm.get("v23")));
    if (!em.lt(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) lt (v14=%s))", vm.get("param0"), vm.get("v14")));
    if (!em.ge(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) ge (v2=%s))", vm.get("param0"), vm.get("v2")));
    if (!em.ge(vm.get("v10"), vm.get("v23"))) e.abort(String.format("!((v10=%s) ge (v23=%s))", vm.get("v10"), vm.get("v23")));
    if (!em.lt(vm.get("v1"), vm.get("v23"))) e.abort(String.format("!((v1=%s) lt (v23=%s))", vm.get("v1"), vm.get("v23")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void remove3899(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", null);
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", (int)0);
    vm.put("v7", vm.get("v5"));
    vm.put("v8", vm.get("v0"));
    vm.put("v9", om.popArrayElement(vm.get("v3"), vm.get("v8")));
    vm.put("v10", em.op("iadd").eval(vm.get("v8"),vm.get("v1")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v10"));
    vm.put("v11", em.op("isub").eval(vm.get("v7"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    vm.put("v12", om.guessArrayIndex(vm.get("v3")));
    vm.put("param0", vm.get("v12"));
    vm.put("v13", om.getArrayElement(vm.get("v3"), vm.get("param0")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v10"),vm.get("param0")));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v1")));
    if (!em.lt(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) lt (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.le(vm.get("v16"), vm.get("v6"))) e.abort(String.format("!((v16=%s) le (v6=%s))", vm.get("v16"), vm.get("v6")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1341(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("java.util.ComparableTimSort.$assertionsDisabled"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementCount"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "elementData"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)32);
    vm.put("v12", (int)119151);
    vm.put("v13", (int)1542);
    vm.put("v14", (int)120);
    vm.put("v15", (int)512);
    vm.put("v16", (int)2);
    vm.put("v17", null);
    vm.put("v18", om.getArrayLength(vm.get("v9")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", em.op("isub").eval(vm.get("v7"),vm.get("v10")));
    vm.put("v21", em.op("ishr").eval(vm.get("v20"),vm.get("v1")));
    vm.put("v22", em.op("iand").eval(vm.get("v20"),vm.get("v1")));
    vm.put("v23", em.op("ior").eval(vm.get("v10"),vm.get("v22")));
    vm.put("v24", em.op("isub").eval(vm.get("v20"),vm.get("v1")));
    vm.put("v25", em.op("iadd").eval(vm.get("v21"),vm.get("v23")));
    vm.put("v26", vm.get("v0"));
    vm.put("v27", em.op("isub").eval(vm.get("v26"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v27"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.eq(vm.get("v1"), vm.get("v7"))) e.abort(String.format("!((v1=%s) eq (v7=%s))", vm.get("v1"), vm.get("v7")));
    if (!em.le(vm.get("v7"), vm.get("v19"))) e.abort(String.format("!((v7=%s) le (v19=%s))", vm.get("v7"), vm.get("v19")));
    if (!em.le(vm.get("v7"), vm.get("v19"))) e.abort(String.format("!((v7=%s) le (v19=%s))", vm.get("v7"), vm.get("v19")));
    if (!em.lt(vm.get("v21"), vm.get("v11"))) e.abort(String.format("!((v21=%s) lt (v11=%s))", vm.get("v21"), vm.get("v11")));
    if (!em.ge(vm.get("v19"), vm.get("v13"))) e.abort(String.format("!((v19=%s) ge (v13=%s))", vm.get("v19"), vm.get("v13")));
    if (!em.ge(vm.get("v1"), vm.get("v25"))) e.abort(String.format("!((v1=%s) ge (v25=%s))", vm.get("v1"), vm.get("v25")));
    if (!em.le(vm.get("v10"), vm.get("v7"))) e.abort(String.format("!((v10=%s) le (v7=%s))", vm.get("v10"), vm.get("v7")));
    if (!em.ne(vm.get("v5"), vm.get("v10"))) e.abort(String.format("!((v5=%s) ne (v10=%s))", vm.get("v5"), vm.get("v10")));
    if (!em.ne(vm.get("v5"), vm.get("v10"))) e.abort(String.format("!((v5=%s) ne (v10=%s))", vm.get("v5"), vm.get("v10")));
    if (!em.le(vm.get("v10"), vm.get("v7"))) e.abort(String.format("!((v10=%s) le (v7=%s))", vm.get("v10"), vm.get("v7")));
    if (!em.ge(vm.get("v19"), vm.get("v14"))) e.abort(String.format("!((v19=%s) ge (v14=%s))", vm.get("v19"), vm.get("v14")));
    if (!em.ge(vm.get("v20"), vm.get("v11"))) e.abort(String.format("!((v20=%s) ge (v11=%s))", vm.get("v20"), vm.get("v11")));
    if (!em.ne(vm.get("v9"), vm.get("v17"))) e.abort(String.format("!((v9=%s) ne (v17=%s))", vm.get("v9"), vm.get("v17")));
    if (!em.eq(vm.get("v5"), vm.get("v10"))) e.abort(String.format("!((v5=%s) eq (v10=%s))", vm.get("v5"), vm.get("v10")));
    if (!em.ne(vm.get("v5"), vm.get("v10"))) e.abort(String.format("!((v5=%s) ne (v10=%s))", vm.get("v5"), vm.get("v10")));
    if (!em.lt(vm.get("v19"), vm.get("v15"))) e.abort(String.format("!((v19=%s) lt (v15=%s))", vm.get("v19"), vm.get("v15")));
    if (!em.ge(vm.get("v20"), vm.get("v11"))) e.abort(String.format("!((v20=%s) ge (v11=%s))", vm.get("v20"), vm.get("v11")));
    if (!em.ge(vm.get("v20"), vm.get("v16"))) e.abort(String.format("!((v20=%s) ge (v16=%s))", vm.get("v20"), vm.get("v16")));
    if (!em.ne(vm.get("v5"), vm.get("v10"))) e.abort(String.format("!((v5=%s) ne (v10=%s))", vm.get("v5"), vm.get("v10")));
    if (!em.eq(vm.get("v24"), vm.get("v10"))) e.abort(String.format("!((v24=%s) eq (v10=%s))", vm.get("v24"), vm.get("v10")));
    if (!em.eq(vm.get("param0"), vm.get("v17"))) e.abort(String.format("!((param0=%s) eq (v17=%s))", vm.get("param0"), vm.get("v17")));
    if (!em.ge(vm.get("v19"), vm.get("v12"))) e.abort(String.format("!((v19=%s) ge (v12=%s))", vm.get("v19"), vm.get("v12")));
    if (!em.eq(vm.get("v3"), vm.get("v10"))) e.abort(String.format("!((v3=%s) eq (v10=%s))", vm.get("v3"), vm.get("v10")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1386(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementCount"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getStatic("java.util.TimSort.$assertionsDisabled"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)2);
    vm.put("v12", null);
    vm.put("v13", em.op("isub").eval(vm.get("v5"),vm.get("v10")));
    vm.put("v14", om.getArrayLength(vm.get("v3")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", vm.get("v0"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ne(vm.get("v3"), vm.get("v12"))) e.abort(String.format("!((v3=%s) ne (v12=%s))", vm.get("v3"), vm.get("v12")));
    if (!em.le(vm.get("v10"), vm.get("v5"))) e.abort(String.format("!((v10=%s) le (v5=%s))", vm.get("v10"), vm.get("v5")));
    if (!em.le(vm.get("v5"), vm.get("v15"))) e.abort(String.format("!((v5=%s) le (v15=%s))", vm.get("v5"), vm.get("v15")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.le(vm.get("v10"), vm.get("v5"))) e.abort(String.format("!((v10=%s) le (v5=%s))", vm.get("v10"), vm.get("v5")));
    if (!em.lt(vm.get("v13"), vm.get("v11"))) e.abort(String.format("!((v13=%s) lt (v11=%s))", vm.get("v13"), vm.get("v11")));
    if (!em.le(vm.get("v5"), vm.get("v15"))) e.abort(String.format("!((v5=%s) le (v15=%s))", vm.get("v5"), vm.get("v15")));
    if (!em.eq(vm.get("v7"), vm.get("v10"))) e.abort(String.format("!((v7=%s) eq (v10=%s))", vm.get("v7"), vm.get("v10")));
    if (!em.eq(vm.get("v9"), vm.get("v10"))) e.abort(String.format("!((v9=%s) eq (v10=%s))", vm.get("v9"), vm.get("v10")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1334(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", (int)1);
    vm.put("v8", (int)0);
    vm.put("v9", (int)7);
    vm.put("v10", null);
    vm.put("v11", java.lang.Object[].class);
    vm.put("v12", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v5")));
    vm.put("v13", om.getArrayLength(vm.get("v5")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v8")));
    vm.put("v16", em.op("isub").eval(vm.get("v1"),vm.get("v8")));
    vm.put("v17", em.op("isub").eval(vm.get("v1"),vm.get("v8")));
    vm.put("v18", vm.get("v6"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v7")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.le(vm.get("v15"), vm.get("v16"))) e.abort(String.format("!((v15=%s) le (v16=%s))", vm.get("v15"), vm.get("v16")));
    if (!em.le(vm.get("v1"), vm.get("v14"))) e.abort(String.format("!((v1=%s) le (v14=%s))", vm.get("v1"), vm.get("v14")));
    if (!em.le(vm.get("v8"), vm.get("v1"))) e.abort(String.format("!((v8=%s) le (v1=%s))", vm.get("v8"), vm.get("v1")));
    if (!em.lt(vm.get("v8"), vm.get("v1"))) e.abort(String.format("!((v8=%s) lt (v1=%s))", vm.get("v8"), vm.get("v1")));
    if (!em.ge(vm.get("v16"), vm.get("v8"))) e.abort(String.format("!((v16=%s) ge (v8=%s))", vm.get("v16"), vm.get("v8")));
    if (!em.ne(vm.get("v3"), vm.get("v8"))) e.abort(String.format("!((v3=%s) ne (v8=%s))", vm.get("v3"), vm.get("v8")));
    if (!em.ge(vm.get("v8"), vm.get("v15"))) e.abort(String.format("!((v8=%s) ge (v15=%s))", vm.get("v8"), vm.get("v15")));
    if (!em.ge(vm.get("v7"), vm.get("v1"))) e.abort(String.format("!((v7=%s) ge (v1=%s))", vm.get("v7"), vm.get("v1")));
    if (!em.lt(vm.get("v17"), vm.get("v9"))) e.abort(String.format("!((v17=%s) lt (v9=%s))", vm.get("v17"), vm.get("v9")));
    if (!em.ne(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ne (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.eq(vm.get("v12"), vm.get("v11"))) e.abort(String.format("!((v12=%s) eq (v11=%s))", vm.get("v12"), vm.get("v11")));
    if (!em.ne(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ne (v10=%s))", vm.get("param0"), vm.get("v10")));
  }
  @IM(clazz="java.util.Vector", name="setSize", desc="(I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void setSize4027(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", null);
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", om.getField(vm.get("this"), "elementCount"));
    vm.put("param0", vm.get("v3"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popArrayElement(vm.get("v5"), vm.get("param0")));
    vm.put("v7", em.op("iinc").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v8", om.popArrayElement(vm.get("v5"), vm.get("v7")));
    vm.put("v9", em.op("iinc").eval(vm.get("v7"),vm.get("v0")));
    vm.put("v10", om.popArrayElement(vm.get("v5"), vm.get("v9")));
    vm.put("v11", em.op("iinc").eval(vm.get("v9"),vm.get("v0")));
    vm.put("v12", em.op("iinc").eval(vm.get("v11"),vm.get("v0")));
    vm.put("v13", om.popArrayElement(vm.get("v5"), vm.get("v12")));
    vm.put("v14", em.op("iinc").eval(vm.get("v12"),vm.get("v0")));
    vm.put("v15", om.popArrayElement(vm.get("v5"), vm.get("v11")));
    vm.put("v16", vm.get("v2"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v18", om.getField(vm.get("this"), "elementCount"));
    if (!em.ge(vm.get("v14"), vm.get("v18"))) e.abort(String.format("!((v14=%s) ge (v18=%s))", vm.get("v14"), vm.get("v18")));
    if (!em.lt(vm.get("v12"), vm.get("v18"))) e.abort(String.format("!((v12=%s) lt (v18=%s))", vm.get("v12"), vm.get("v18")));
    if (!em.lt(vm.get("v9"), vm.get("v18"))) e.abort(String.format("!((v9=%s) lt (v18=%s))", vm.get("v9"), vm.get("v18")));
    if (!em.lt(vm.get("v7"), vm.get("v18"))) e.abort(String.format("!((v7=%s) lt (v18=%s))", vm.get("v7"), vm.get("v18")));
    if (!em.lt(vm.get("param0"), vm.get("v18"))) e.abort(String.format("!((param0=%s) lt (v18=%s))", vm.get("param0"), vm.get("v18")));
    if (!em.lt(vm.get("v11"), vm.get("v18"))) e.abort(String.format("!((v11=%s) lt (v18=%s))", vm.get("v11"), vm.get("v18")));
    if (!em.le(vm.get("param0"), vm.get("v18"))) e.abort(String.format("!((param0=%s) le (v18=%s))", vm.get("param0"), vm.get("v18")));
  }
  @IM(clazz="java.util.Vector", name="removeElement", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  static void removeElement4898(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", (int)0);
    vm.put("v5", null);
    vm.put("v6", vm.get("v2"));
    vm.put("v7", em.op("isub").eval(vm.get("v6"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v7"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.eq(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) eq (v5=%s))", vm.get("param0"), vm.get("v5")));
    if (!em.ge(vm.get("v4"), vm.get("v1"))) e.abort(String.format("!((v4=%s) ge (v1=%s))", vm.get("v4"), vm.get("v1")));
  }
  @IM(clazz="java.util.Stack", name="pop", desc="()Ljava/lang/Object;",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void pop3298(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", null);
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementCount"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)2);
    vm.put("v9", om.popArrayElement(vm.get("v5"), vm.get("v7")));
    vm.put("v10", em.op("iadd").eval(vm.get("v7"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v10"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    vm.put("v12", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    vm.put("v13", om.getArrayElement(vm.get("v5"), vm.get("v12")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("iadd").eval(vm.get("v11"),vm.get("v1")));
    vm.put("v16", om.popArrayElement(vm.get("v5"), vm.get("v15")));
    vm.put("v17", em.op("iadd").eval(vm.get("v11"),vm.get("v0")));
    vm.put("v18", om.popArrayElement(vm.get("v5"), vm.get("v17")));
    vm.put("v19", em.op("iadd").eval(vm.get("v11"),vm.get("v0")));
    vm.put("v20", em.op("iadd").eval(vm.get("v19"),vm.get("v0")));
    vm.put("v21", om.getArrayElement(vm.get("v5"), vm.get("v20")));
    vm.put("v22", vm.get("v21"));
    vm.put("v23", em.op("iadd").eval(vm.get("v19"),vm.get("v1")));
    vm.put("v24", om.getArrayElement(vm.get("v5"), vm.get("v23")));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", em.op("isub").eval(vm.get("v10"),vm.get("v11")));
    vm.put("v27", em.op("isub").eval(vm.get("v26"),vm.get("v0")));
    vm.put("v28", vm.get("v3"));
    vm.put("v29", em.op("isub").eval(vm.get("v28"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v29"));
    if (!em.lt(vm.get("v0"), vm.get("v27"))) e.abort(String.format("!((v0=%s) lt (v27=%s))", vm.get("v0"), vm.get("v27")));
    if (!em.ge(vm.get("v11"), vm.get("v1"))) e.abort(String.format("!((v11=%s) ge (v1=%s))", vm.get("v11"), vm.get("v1")));
    if (!em.ne(vm.get("v10"), vm.get("v1"))) e.abort(String.format("!((v10=%s) ne (v1=%s))", vm.get("v10"), vm.get("v1")));
    if (!em.lt(vm.get("v12"), vm.get("v10"))) e.abort(String.format("!((v12=%s) lt (v10=%s))", vm.get("v12"), vm.get("v10")));
    if (!em.lt(vm.get("v1"), vm.get("v27"))) e.abort(String.format("!((v1=%s) lt (v27=%s))", vm.get("v1"), vm.get("v27")));
    if (!em.lt(vm.get("v11"), vm.get("v10"))) e.abort(String.format("!((v11=%s) lt (v10=%s))", vm.get("v11"), vm.get("v10")));
    if (!em.ge(vm.get("v8"), vm.get("v27"))) e.abort(String.format("!((v8=%s) ge (v27=%s))", vm.get("v8"), vm.get("v27")));
    if (!em.gt(vm.get("v27"), vm.get("v1"))) e.abort(String.format("!((v27=%s) gt (v1=%s))", vm.get("v27"), vm.get("v1")));
  }
  @IM(clazz="java.util.Vector", name="add", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void add2691(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getArrayLength(vm.get("v3")));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementCount"));
    vm.put("v7", (int)0);
    vm.put("v8", vm.get("v0"));
    vm.put("v9", em.op("isub").eval(vm.get("v8"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v9"));
    vm.put("v10", vm.get("v6"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v1")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v11"));
    vm.put("v12", om.popArrayElement(vm.get("v3"), vm.get("v11")));
    vm.put("param0", vm.get("v12"));
    vm.put("v13", em.op("iadd").eval(vm.get("v11"),vm.get("v1")));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v5")));
    if (!em.le(vm.get("v14"), vm.get("v7"))) e.abort(String.format("!((v14=%s) le (v7=%s))", vm.get("v14"), vm.get("v7")));
  }
  @IM(clazz="java.util.Vector", name="removeAllElements", desc="()V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void removeAllElements4665(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", (int)0);
    vm.put("v2", (int)4);
    vm.put("v3", null);
    vm.put("v4", (int)2);
    vm.put("v5", (int)1);
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", (int)3);
    vm.put("v8", om.getField(vm.get("this"), "elementData"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)5);
    vm.put("v11", om.popArrayElement(vm.get("v9"), vm.get("v7")));
    vm.put("v12", om.popArrayElement(vm.get("v9"), vm.get("v2")));
    vm.put("v13", om.popArrayElement(vm.get("v9"), vm.get("v5")));
    vm.put("v14", om.popArrayElement(vm.get("v9"), vm.get("v4")));
    vm.put("v15", om.popArrayElement(vm.get("v9"), vm.get("v1")));
    vm.put("v16", vm.get("v6"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v5")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v18", om.getField(vm.get("this"), "elementCount"));
    if (!em.ge(vm.get("v10"), vm.get("v18"))) e.abort(String.format("!((v10=%s) ge (v18=%s))", vm.get("v10"), vm.get("v18")));
    if (!em.lt(vm.get("v4"), vm.get("v18"))) e.abort(String.format("!((v4=%s) lt (v18=%s))", vm.get("v4"), vm.get("v18")));
    if (!em.lt(vm.get("v7"), vm.get("v18"))) e.abort(String.format("!((v7=%s) lt (v18=%s))", vm.get("v7"), vm.get("v18")));
    if (!em.lt(vm.get("v2"), vm.get("v18"))) e.abort(String.format("!((v2=%s) lt (v18=%s))", vm.get("v2"), vm.get("v18")));
    if (!em.lt(vm.get("v5"), vm.get("v18"))) e.abort(String.format("!((v5=%s) lt (v18=%s))", vm.get("v5"), vm.get("v18")));
    if (!em.lt(vm.get("v1"), vm.get("v18"))) e.abort(String.format("!((v1=%s) lt (v18=%s))", vm.get("v1"), vm.get("v18")));
  }
  @IM(clazz="java.util.Vector", name="insertElementAt", desc="(Ljava/lang/Object;I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void insertElementAt1798(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getArrayLength(vm.get("v1")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)0);
    vm.put("v8", vm.get("v2"));
    vm.put("v9", vm.get("v4"));
    vm.put("v10", em.op("isub").eval(vm.get("v9"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v10"));
    vm.put("v11", em.op("isub").eval(vm.get("v8"),vm.get("v3")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v11"));
    vm.put("v12", em.op("iadd").eval(vm.get("v11"),vm.get("v3")));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v6")));
    vm.put("v14", om.guessArrayIndex(vm.get("v1")));
    vm.put("param1", vm.get("v14"));
    vm.put("v15", om.popArrayElement(vm.get("v1"), vm.get("param1")));
    vm.put("param0", vm.get("v15"));
    vm.put("v16", em.op("isub").eval(vm.get("v11"),vm.get("param1")));
    if (!em.le(vm.get("v13"), vm.get("v7"))) e.abort(String.format("!((v13=%s) le (v7=%s))", vm.get("v13"), vm.get("v7")));
    if (!em.le(vm.get("param1"), vm.get("v11"))) e.abort(String.format("!((param1=%s) le (v11=%s))", vm.get("param1"), vm.get("v11")));
    if (!em.ge(vm.get("v7"), vm.get("v16"))) e.abort(String.format("!((v7=%s) ge (v16=%s))", vm.get("v7"), vm.get("v16")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  static void remove4597(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", (int)0);
    vm.put("v5", null);
    vm.put("v6", vm.get("v2"));
    vm.put("v7", em.op("isub").eval(vm.get("v6"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v7"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.eq(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) eq (v5=%s))", vm.get("param0"), vm.get("v5")));
    if (!em.ge(vm.get("v4"), vm.get("v1"))) e.abort(String.format("!((v4=%s) ge (v1=%s))", vm.get("v4"), vm.get("v1")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void remove4560(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "elementCount"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getArrayElement(vm.get("v4"), vm.get("v0")));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getArrayElement(vm.get("v4"), vm.get("v1")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)2);
    vm.put("v12", null);
    vm.put("v13", vm.get("v2"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.ge(vm.get("v11"), vm.get("v6"))) e.abort(String.format("!((v11=%s) ge (v6=%s))", vm.get("v11"), vm.get("v6")));
    if (!em.lt(vm.get("v0"), vm.get("v6"))) e.abort(String.format("!((v0=%s) lt (v6=%s))", vm.get("v0"), vm.get("v6")));
    if (!em.eq(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) eq (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ne(vm.get("v8"), vm.get("v12"))) e.abort(String.format("!((v8=%s) ne (v12=%s))", vm.get("v8"), vm.get("v12")));
    if (!em.ne(vm.get("v10"), vm.get("v12"))) e.abort(String.format("!((v10=%s) ne (v12=%s))", vm.get("v10"), vm.get("v12")));
    if (!em.lt(vm.get("v1"), vm.get("v6"))) e.abort(String.format("!((v1=%s) lt (v6=%s))", vm.get("v1"), vm.get("v6")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void remove3897(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", (int)2);
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementCount"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", (int)3);
    vm.put("v10", om.popArrayElement(vm.get("v5"), vm.get("v7")));
    vm.put("v11", vm.get("v8"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", em.op("iadd").eval(vm.get("v7"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v13"));
    vm.put("v14", om.guessArrayIndex(vm.get("v5")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.popArrayElement(vm.get("v5"), vm.get("v15")));
    vm.put("param0", em.op("isub").eval(vm.get("v15"),vm.get("v2")));
    vm.put("v17", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v18", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v19", em.op("iadd").eval(vm.get("v18"),vm.get("v0")));
    vm.put("v20", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v21", em.op("isub").eval(vm.get("v13"),vm.get("param0")));
    vm.put("v22", om.popArrayElement(vm.get("v5"), vm.get("v17")));
    vm.put("v23", em.op("iadd").eval(vm.get("v18"),vm.get("v1")));
    vm.put("v24", om.getArrayElement(vm.get("v5"), vm.get("param0")));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", em.op("isub").eval(vm.get("v21"),vm.get("v0")));
    vm.put("v27", om.getArrayElement(vm.get("v5"), vm.get("v19")));
    vm.put("v28", vm.get("v27"));
    vm.put("v29", em.op("iadd").eval(vm.get("v18"),vm.get("v2")));
    vm.put("v30", om.getArrayElement(vm.get("v5"), vm.get("v29")));
    vm.put("v31", vm.get("v30"));
    vm.put("v32", om.popArrayElement(vm.get("v5"), vm.get("v20")));
    vm.put("v33", om.getArrayElement(vm.get("v5"), vm.get("v23")));
    vm.put("v34", vm.get("v33"));
    if (!em.lt(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) lt (v13=%s))", vm.get("param0"), vm.get("v13")));
    if (!em.ge(vm.get("v9"), vm.get("v26"))) e.abort(String.format("!((v9=%s) ge (v26=%s))", vm.get("v9"), vm.get("v26")));
    if (!em.lt(vm.get("v0"), vm.get("v26"))) e.abort(String.format("!((v0=%s) lt (v26=%s))", vm.get("v0"), vm.get("v26")));
    if (!em.lt(vm.get("v2"), vm.get("v26"))) e.abort(String.format("!((v2=%s) lt (v26=%s))", vm.get("v2"), vm.get("v26")));
    if (!em.gt(vm.get("v26"), vm.get("v1"))) e.abort(String.format("!((v26=%s) gt (v1=%s))", vm.get("v26"), vm.get("v1")));
    if (!em.lt(vm.get("v1"), vm.get("v26"))) e.abort(String.format("!((v1=%s) lt (v26=%s))", vm.get("v1"), vm.get("v26")));
  }
  @IM(clazz="java.util.Vector", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount","this.elementData[*]","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"})
  @DG(pred={"_init_2599"})
  static void remove4599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", om.getArrayElement(vm.get("v3"), vm.get("v1")));
    vm.put("v8", om.getField(vm.get("this"), "elementCount"));
    vm.put("v9", null);
    vm.put("v10", vm.get("v6"));
    vm.put("v11", vm.get("v8"));
    vm.put("v12", om.popArrayElement(vm.get("v3"), vm.get("v11")));
    vm.put("v13", em.op("iadd").eval(vm.get("v11"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v1")));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    vm.put("v16", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    om.revertArrayElement(vm.get("v3"), vm.get("v1"));
    vm.put("v18", om.getArrayElement(vm.get("v3"), vm.get("v1")));
    if (!em.gt(vm.get("v15"), vm.get("v1"))) e.abort(String.format("!((v15=%s) gt (v1=%s))", vm.get("v15"), vm.get("v1")));
    if (!em.eq(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) eq (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.lt(vm.get("v1"), vm.get("v13"))) e.abort(String.format("!((v1=%s) lt (v13=%s))", vm.get("v1"), vm.get("v13")));
    if (!em.lt(vm.get("v1"), vm.get("v15"))) e.abort(String.format("!((v1=%s) lt (v15=%s))", vm.get("v1"), vm.get("v15")));
    if (!em.lt(vm.get("v1"), vm.get("v13"))) e.abort(String.format("!((v1=%s) lt (v13=%s))", vm.get("v1"), vm.get("v13")));
    if (!em.eq(vm.get("v18"), vm.get("v9"))) e.abort(String.format("!((v18=%s) eq (v9=%s))", vm.get("v18"), vm.get("v9")));
    if (!em.ge(vm.get("v0"), vm.get("v15"))) e.abort(String.format("!((v0=%s) ge (v15=%s))", vm.get("v0"), vm.get("v15")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1374(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementCount"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayLength(vm.get("v5")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)7);
    vm.put("v12", null);
    vm.put("v13", java.lang.Object[].class);
    vm.put("v14", em.op("isub").eval(vm.get("v1"),vm.get("v10")));
    vm.put("v15", em.op("isub").eval(vm.get("v1"),vm.get("v10")));
    vm.put("v16", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v5")));
    vm.put("v17", em.op("isub").eval(vm.get("v9"),vm.get("v10")));
    vm.put("v18", vm.get("v2"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.lt(vm.get("v15"), vm.get("v11"))) e.abort(String.format("!((v15=%s) lt (v11=%s))", vm.get("v15"), vm.get("v11")));
    if (!em.gt(vm.get("v17"), vm.get("v14"))) e.abort(String.format("!((v17=%s) gt (v14=%s))", vm.get("v17"), vm.get("v14")));
    if (!em.eq(vm.get("v16"), vm.get("v13"))) e.abort(String.format("!((v16=%s) eq (v13=%s))", vm.get("v16"), vm.get("v13")));
    if (!em.eq(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) eq (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ge(vm.get("v14"), vm.get("v10"))) e.abort(String.format("!((v14=%s) ge (v10=%s))", vm.get("v14"), vm.get("v10")));
    if (!em.le(vm.get("v10"), vm.get("v1"))) e.abort(String.format("!((v10=%s) le (v1=%s))", vm.get("v10"), vm.get("v1")));
    if (!em.ge(vm.get("v10"), vm.get("v14"))) e.abort(String.format("!((v10=%s) ge (v14=%s))", vm.get("v10"), vm.get("v14")));
    if (!em.ge(vm.get("v10"), vm.get("v1"))) e.abort(String.format("!((v10=%s) ge (v1=%s))", vm.get("v10"), vm.get("v1")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.le(vm.get("v1"), vm.get("v9"))) e.abort(String.format("!((v1=%s) le (v9=%s))", vm.get("v1"), vm.get("v9")));
    if (!em.ne(vm.get("v7"), vm.get("v10"))) e.abort(String.format("!((v7=%s) ne (v10=%s))", vm.get("v7"), vm.get("v10")));
  }
  @IM(clazz="java.util.Vector", name="insertElementAt", desc="(Ljava/lang/Object;I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void insertElementAt1764(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)2);
    vm.put("v2", (int)1);
    vm.put("v3", (int)3);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "elementCount"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)4);
    vm.put("v9", om.getArrayLength(vm.get("v7")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", vm.get("v5"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v2")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v12"));
    vm.put("v13", em.op("iadd").eval(vm.get("v12"),vm.get("v2")));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v10")));
    vm.put("v15", vm.get("v4"));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v16"));
    vm.put("v17", om.guessArrayIndex(vm.get("v7")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.getArrayElement(vm.get("v7"), vm.get("v18")));
    vm.put("v20", vm.get("v19"));
    vm.put("param1", em.op("isub").eval(vm.get("v18"),vm.get("v3")));
    vm.put("v21", em.op("isub").eval(vm.get("v12"),vm.get("param1")));
    vm.put("v22", em.op("iadd").eval(vm.get("param1"),vm.get("v1")));
    vm.put("v23", om.getArrayElement(vm.get("v7"), vm.get("v22")));
    vm.put("v24", vm.get("v23"));
    vm.put("v25", em.op("iadd").eval(vm.get("param1"),vm.get("v0")));
    vm.put("v26", om.getArrayElement(vm.get("v7"), vm.get("v25")));
    vm.put("v27", vm.get("v26"));
    vm.put("v28", em.op("iadd").eval(vm.get("param1"),vm.get("v2")));
    vm.put("v29", om.getArrayElement(vm.get("v7"), vm.get("v28")));
    vm.put("v30", vm.get("v29"));
    vm.put("v31", em.op("iadd").eval(vm.get("param1"),vm.get("v2")));
    vm.put("v32", em.op("iadd").eval(vm.get("v31"),vm.get("v2")));
    vm.put("v33", em.op("iadd").eval(vm.get("v31"),vm.get("v3")));
    vm.put("v34", em.op("iadd").eval(vm.get("v31"),vm.get("v1")));
    vm.put("v35", om.popArrayElement(vm.get("v7"), vm.get("v33")));
    vm.put("v36", om.popArrayElement(vm.get("v7"), vm.get("v34")));
    vm.put("v37", om.popArrayElement(vm.get("v7"), vm.get("v32")));
    vm.put("v38", em.op("iadd").eval(vm.get("v31"),vm.get("v0")));
    vm.put("v39", om.popArrayElement(vm.get("v7"), vm.get("v38")));
    vm.put("v40", om.popArrayElement(vm.get("v7"), vm.get("param1")));
    vm.put("param0", vm.get("v40"));
    if (!em.lt(vm.get("v3"), vm.get("v21"))) e.abort(String.format("!((v3=%s) lt (v21=%s))", vm.get("v3"), vm.get("v21")));
    if (!em.lt(vm.get("v0"), vm.get("v21"))) e.abort(String.format("!((v0=%s) lt (v21=%s))", vm.get("v0"), vm.get("v21")));
    if (!em.le(vm.get("v14"), vm.get("v0"))) e.abort(String.format("!((v14=%s) le (v0=%s))", vm.get("v14"), vm.get("v0")));
    if (!em.lt(vm.get("v1"), vm.get("v21"))) e.abort(String.format("!((v1=%s) lt (v21=%s))", vm.get("v1"), vm.get("v21")));
    if (!em.ge(vm.get("v8"), vm.get("v21"))) e.abort(String.format("!((v8=%s) ge (v21=%s))", vm.get("v8"), vm.get("v21")));
    if (!em.le(vm.get("param1"), vm.get("v12"))) e.abort(String.format("!((param1=%s) le (v12=%s))", vm.get("param1"), vm.get("v12")));
    if (!em.lt(vm.get("v2"), vm.get("v21"))) e.abort(String.format("!((v2=%s) lt (v21=%s))", vm.get("v2"), vm.get("v21")));
  }
  @IM(clazz="java.util.Vector", name="removeElement", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void removeElement4895(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "elementData"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "elementCount"));
    vm.put("v4", (int)1);
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", om.getArrayElement(vm.get("v2"), vm.get("v0")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", null);
    vm.put("v9", vm.get("v3"));
    vm.put("v10", om.popArrayElement(vm.get("v2"), vm.get("v9")));
    vm.put("v11", em.op("iadd").eval(vm.get("v9"),vm.get("v4")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v11"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v0")));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v4")));
    vm.put("v14", vm.get("v5"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v4")));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v4")));
    om.revertField(vm.get("this"), "modCount", vm.get("v16"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.gt(vm.get("v13"), vm.get("v0"))) e.abort(String.format("!((v13=%s) gt (v0=%s))", vm.get("v13"), vm.get("v0")));
    if (!em.lt(vm.get("v0"), vm.get("v11"))) e.abort(String.format("!((v0=%s) lt (v11=%s))", vm.get("v0"), vm.get("v11")));
    if (!em.lt(vm.get("v0"), vm.get("v11"))) e.abort(String.format("!((v0=%s) lt (v11=%s))", vm.get("v0"), vm.get("v11")));
    if (!em.ge(vm.get("v0"), vm.get("v13"))) e.abort(String.format("!((v0=%s) ge (v13=%s))", vm.get("v0"), vm.get("v13")));
    if (!em.eq(vm.get("v7"), vm.get("v8"))) e.abort(String.format("!((v7=%s) eq (v8=%s))", vm.get("v7"), vm.get("v8")));
    if (!em.eq(vm.get("param0"), vm.get("v8"))) e.abort(String.format("!((param0=%s) eq (v8=%s))", vm.get("param0"), vm.get("v8")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1336(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", (int)1);
    vm.put("v8", om.getArrayLength(vm.get("v1")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)7);
    vm.put("v12", null);
    vm.put("v13", java.lang.Object[].class);
    vm.put("v14", em.op("isub").eval(vm.get("v3"),vm.get("v10")));
    vm.put("v15", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v1")));
    vm.put("v16", em.op("isub").eval(vm.get("v3"),vm.get("v10")));
    vm.put("v17", em.op("isub").eval(vm.get("v9"),vm.get("v10")));
    vm.put("v18", vm.get("v6"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v7")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ge(vm.get("v7"), vm.get("v3"))) e.abort(String.format("!((v7=%s) ge (v3=%s))", vm.get("v7"), vm.get("v3")));
    if (!em.lt(vm.get("v14"), vm.get("v11"))) e.abort(String.format("!((v14=%s) lt (v11=%s))", vm.get("v14"), vm.get("v11")));
    if (!em.ge(vm.get("v16"), vm.get("v10"))) e.abort(String.format("!((v16=%s) ge (v10=%s))", vm.get("v16"), vm.get("v10")));
    if (!em.ge(vm.get("v10"), vm.get("v16"))) e.abort(String.format("!((v10=%s) ge (v16=%s))", vm.get("v10"), vm.get("v16")));
    if (!em.gt(vm.get("v17"), vm.get("v16"))) e.abort(String.format("!((v17=%s) gt (v16=%s))", vm.get("v17"), vm.get("v16")));
    if (!em.ne(vm.get("v5"), vm.get("v10"))) e.abort(String.format("!((v5=%s) ne (v10=%s))", vm.get("v5"), vm.get("v10")));
    if (!em.le(vm.get("v3"), vm.get("v9"))) e.abort(String.format("!((v3=%s) le (v9=%s))", vm.get("v3"), vm.get("v9")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.le(vm.get("v10"), vm.get("v3"))) e.abort(String.format("!((v10=%s) le (v3=%s))", vm.get("v10"), vm.get("v3")));
    if (!em.lt(vm.get("v10"), vm.get("v3"))) e.abort(String.format("!((v10=%s) lt (v3=%s))", vm.get("v10"), vm.get("v3")));
    if (!em.ne(vm.get("v15"), vm.get("v13"))) e.abort(String.format("!((v15=%s) ne (v13=%s))", vm.get("v15"), vm.get("v13")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
  }
  @IM(clazz="java.util.Vector", name="removeElementAt", desc="(I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void removeElementAt2479(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", (int)2);
    vm.put("v3", om.getField(vm.get("this"), "elementCount"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", null);
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", (int)3);
    vm.put("v9", vm.get("v7"));
    vm.put("v10", vm.get("v3"));
    vm.put("v11", om.popArrayElement(vm.get("v5"), vm.get("v10")));
    vm.put("v12", em.op("iadd").eval(vm.get("v10"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v12"));
    vm.put("v13", em.op("isub").eval(vm.get("v9"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    vm.put("v14", om.guessArrayIndex(vm.get("v5")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.getArrayElement(vm.get("v5"), vm.get("v15")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", em.op("isub").eval(vm.get("v15"),vm.get("v2")));
    vm.put("v19", em.op("iadd").eval(vm.get("v18"),vm.get("v1")));
    vm.put("v20", om.getArrayElement(vm.get("v5"), vm.get("v19")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", em.op("iadd").eval(vm.get("v18"),vm.get("v0")));
    vm.put("v23", om.getArrayElement(vm.get("v5"), vm.get("v22")));
    vm.put("v24", vm.get("v23"));
    vm.put("param0", em.op("isub").eval(vm.get("v18"),vm.get("v0")));
    vm.put("v25", em.op("isub").eval(vm.get("v12"),vm.get("param0")));
    vm.put("v26", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v27", om.popArrayElement(vm.get("v5"), vm.get("v26")));
    vm.put("v28", em.op("isub").eval(vm.get("v25"),vm.get("v0")));
    vm.put("v29", em.op("iadd").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v30", om.popArrayElement(vm.get("v5"), vm.get("v29")));
    vm.put("v31", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v32", om.popArrayElement(vm.get("v5"), vm.get("v31")));
    if (!em.lt(vm.get("v0"), vm.get("v28"))) e.abort(String.format("!((v0=%s) lt (v28=%s))", vm.get("v0"), vm.get("v28")));
    if (!em.lt(vm.get("v2"), vm.get("v28"))) e.abort(String.format("!((v2=%s) lt (v28=%s))", vm.get("v2"), vm.get("v28")));
    if (!em.lt(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) lt (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ge(vm.get("v8"), vm.get("v28"))) e.abort(String.format("!((v8=%s) ge (v28=%s))", vm.get("v8"), vm.get("v28")));
    if (!em.lt(vm.get("v1"), vm.get("v28"))) e.abort(String.format("!((v1=%s) lt (v28=%s))", vm.get("v1"), vm.get("v28")));
    if (!em.ge(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) ge (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.gt(vm.get("v28"), vm.get("v1"))) e.abort(String.format("!((v28=%s) gt (v1=%s))", vm.get("v28"), vm.get("v1")));
  }
  @IM(clazz="java.util.Vector", name="clear", desc="()V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void clear2867(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", null);
    vm.put("v2", (int)3);
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", (int)1);
    vm.put("v5", (int)2);
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "elementCount"));
    vm.put("v9", (int)4);
    vm.put("v10", om.popArrayElement(vm.get("v7"), vm.get("v4")));
    vm.put("v11", (int)5);
    vm.put("v12", om.popArrayElement(vm.get("v7"), vm.get("v9")));
    vm.put("v13", om.popArrayElement(vm.get("v7"), vm.get("v0")));
    vm.put("v14", om.popArrayElement(vm.get("v7"), vm.get("v2")));
    vm.put("v15", om.popArrayElement(vm.get("v7"), vm.get("v5")));
    vm.put("v16", vm.get("v3"));
    if (!em.eq(vm.get("v0"), (vm.get("v8")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v8")));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v4")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v18", om.getField(vm.get("this"), "elementCount"));
    if (!em.lt(vm.get("v5"), vm.get("v18"))) e.abort(String.format("!((v5=%s) lt (v18=%s))", vm.get("v5"), vm.get("v18")));
    if (!em.lt(vm.get("v4"), vm.get("v18"))) e.abort(String.format("!((v4=%s) lt (v18=%s))", vm.get("v4"), vm.get("v18")));
    if (!em.lt(vm.get("v2"), vm.get("v18"))) e.abort(String.format("!((v2=%s) lt (v18=%s))", vm.get("v2"), vm.get("v18")));
    if (!em.ge(vm.get("v11"), vm.get("v18"))) e.abort(String.format("!((v11=%s) ge (v18=%s))", vm.get("v11"), vm.get("v18")));
    if (!em.lt(vm.get("v9"), vm.get("v18"))) e.abort(String.format("!((v9=%s) lt (v18=%s))", vm.get("v9"), vm.get("v18")));
    if (!em.lt(vm.get("v0"), vm.get("v18"))) e.abort(String.format("!((v0=%s) lt (v18=%s))", vm.get("v0"), vm.get("v18")));
  }
  @IM(clazz="java.util.Vector", name="setSize", desc="(I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void setSize4068(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", null);
    vm.put("v5", om.getField(vm.get("this"), "elementCount"));
    vm.put("param0", vm.get("v5"));
    vm.put("v6", om.popArrayElement(vm.get("v3"), vm.get("param0")));
    vm.put("v7", em.op("iinc").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v8", om.popArrayElement(vm.get("v3"), vm.get("v7")));
    vm.put("v9", em.op("iinc").eval(vm.get("v7"),vm.get("v1")));
    vm.put("v10", em.op("iinc").eval(vm.get("v9"),vm.get("v1")));
    vm.put("v11", om.popArrayElement(vm.get("v3"), vm.get("v9")));
    vm.put("v12", vm.get("v0"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v14", om.getField(vm.get("this"), "elementCount"));
    if (!em.lt(vm.get("v9"), vm.get("v14"))) e.abort(String.format("!((v9=%s) lt (v14=%s))", vm.get("v9"), vm.get("v14")));
    if (!em.le(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) le (v14=%s))", vm.get("param0"), vm.get("v14")));
    if (!em.ge(vm.get("v10"), vm.get("v14"))) e.abort(String.format("!((v10=%s) ge (v14=%s))", vm.get("v10"), vm.get("v14")));
    if (!em.lt(vm.get("v7"), vm.get("v14"))) e.abort(String.format("!((v7=%s) lt (v14=%s))", vm.get("v7"), vm.get("v14")));
    if (!em.lt(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) lt (v14=%s))", vm.get("param0"), vm.get("v14")));
  }
  @IM(clazz="java.util.Vector", name="setSize", desc="(I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void setSize4030(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", (int)1);
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "elementCount"));
    vm.put("param0", vm.get("v5"));
    vm.put("v6", em.op("iinc").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v7", om.popArrayElement(vm.get("v1"), vm.get("param0")));
    vm.put("v8", em.op("iinc").eval(vm.get("v6"),vm.get("v2")));
    vm.put("v9", om.popArrayElement(vm.get("v1"), vm.get("v8")));
    vm.put("v10", em.op("iinc").eval(vm.get("v8"),vm.get("v2")));
    vm.put("v11", om.popArrayElement(vm.get("v1"), vm.get("v10")));
    vm.put("v12", em.op("iinc").eval(vm.get("v10"),vm.get("v2")));
    vm.put("v13", om.popArrayElement(vm.get("v1"), vm.get("v6")));
    vm.put("v14", vm.get("v4"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v16", om.getField(vm.get("this"), "elementCount"));
    if (!em.lt(vm.get("v6"), vm.get("v16"))) e.abort(String.format("!((v6=%s) lt (v16=%s))", vm.get("v6"), vm.get("v16")));
    if (!em.ge(vm.get("v12"), vm.get("v16"))) e.abort(String.format("!((v12=%s) ge (v16=%s))", vm.get("v12"), vm.get("v16")));
    if (!em.lt(vm.get("param0"), vm.get("v16"))) e.abort(String.format("!((param0=%s) lt (v16=%s))", vm.get("param0"), vm.get("v16")));
    if (!em.le(vm.get("param0"), vm.get("v16"))) e.abort(String.format("!((param0=%s) le (v16=%s))", vm.get("param0"), vm.get("v16")));
    if (!em.lt(vm.get("v8"), vm.get("v16"))) e.abort(String.format("!((v8=%s) lt (v16=%s))", vm.get("v8"), vm.get("v16")));
    if (!em.lt(vm.get("v10"), vm.get("v16"))) e.abort(String.format("!((v10=%s) lt (v16=%s))", vm.get("v10"), vm.get("v16")));
  }
  @IM(clazz="java.util.Vector", name="removeElement", desc="(Ljava/lang/Object;)Z",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void removeElement4840(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", null);
    vm.put("v6", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "elementCount"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getArrayElement(vm.get("v3"), vm.get("v1")));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.popArrayElement(vm.get("v3"), vm.get("v9")));
    vm.put("v13", vm.get("v4"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v0")));
    vm.put("v15", em.op("iadd").eval(vm.get("v9"),vm.get("v0")));
    om.revertField(vm.get("this"), "elementCount", vm.get("v15"));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v0")));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    vm.put("v18", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v18"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.ne(vm.get("v11"), vm.get("v5"))) e.abort(String.format("!((v11=%s) ne (v5=%s))", vm.get("v11"), vm.get("v5")));
    if (!em.eq(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) eq (v5=%s))", vm.get("param0"), vm.get("v5")));
    if (!em.lt(vm.get("v0"), vm.get("v15"))) e.abort(String.format("!((v0=%s) lt (v15=%s))", vm.get("v0"), vm.get("v15")));
    if (!em.lt(vm.get("v0"), vm.get("v15"))) e.abort(String.format("!((v0=%s) lt (v15=%s))", vm.get("v0"), vm.get("v15")));
    if (!em.le(vm.get("v17"), vm.get("v1"))) e.abort(String.format("!((v17=%s) le (v1=%s))", vm.get("v17"), vm.get("v1")));
    if (!em.lt(vm.get("v1"), vm.get("v15"))) e.abort(String.format("!((v1=%s) lt (v15=%s))", vm.get("v1"), vm.get("v15")));
    if (!em.eq(vm.get("v7"), vm.get("v5"))) e.abort(String.format("!((v7=%s) eq (v5=%s))", vm.get("v7"), vm.get("v5")));
  }
  @IM(clazz="java.util.Vector", name="removeAllElements", desc="()V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void removeAllElements4679(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2);
    vm.put("v1", null);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementCount"));
    vm.put("v5", (int)0);
    vm.put("v6", (int)3);
    vm.put("v7", (int)1);
    vm.put("v8", om.popArrayElement(vm.get("v3"), vm.get("v5")));
    vm.put("v9", om.getField(vm.get("this"), "modCount"));
    vm.put("v10", (int)4);
    vm.put("v11", om.popArrayElement(vm.get("v3"), vm.get("v7")));
    vm.put("v12", om.popArrayElement(vm.get("v3"), vm.get("v6")));
    vm.put("v13", om.popArrayElement(vm.get("v3"), vm.get("v0")));
    vm.put("v14", vm.get("v9"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v7")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v16", om.getField(vm.get("this"), "elementCount"));
    if (!em.lt(vm.get("v5"), vm.get("v16"))) e.abort(String.format("!((v5=%s) lt (v16=%s))", vm.get("v5"), vm.get("v16")));
    if (!em.ge(vm.get("v10"), vm.get("v16"))) e.abort(String.format("!((v10=%s) ge (v16=%s))", vm.get("v10"), vm.get("v16")));
    if (!em.lt(vm.get("v7"), vm.get("v16"))) e.abort(String.format("!((v7=%s) lt (v16=%s))", vm.get("v7"), vm.get("v16")));
    if (!em.lt(vm.get("v6"), vm.get("v16"))) e.abort(String.format("!((v6=%s) lt (v16=%s))", vm.get("v6"), vm.get("v16")));
    if (!em.lt(vm.get("v0"), vm.get("v16"))) e.abort(String.format("!((v0=%s) lt (v16=%s))", vm.get("v0"), vm.get("v16")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1323(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayLength(vm.get("v1")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)7);
    vm.put("v12", java.lang.Object[].class);
    vm.put("v13", null);
    vm.put("v14", em.op("isub").eval(vm.get("v9"),vm.get("v10")));
    vm.put("v15", em.op("isub").eval(vm.get("v3"),vm.get("v10")));
    vm.put("v16", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v1")));
    vm.put("v17", em.op("isub").eval(vm.get("v3"),vm.get("v10")));
    vm.put("v18", vm.get("v4"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v5")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.lt(vm.get("v15"), vm.get("v11"))) e.abort(String.format("!((v15=%s) lt (v11=%s))", vm.get("v15"), vm.get("v11")));
    if (!em.ne(vm.get("v7"), vm.get("v10"))) e.abort(String.format("!((v7=%s) ne (v10=%s))", vm.get("v7"), vm.get("v10")));
    if (!em.eq(vm.get("v16"), vm.get("v12"))) e.abort(String.format("!((v16=%s) eq (v12=%s))", vm.get("v16"), vm.get("v12")));
    if (!em.le(vm.get("v14"), vm.get("v17"))) e.abort(String.format("!((v14=%s) le (v17=%s))", vm.get("v14"), vm.get("v17")));
    if (!em.le(vm.get("v10"), vm.get("v3"))) e.abort(String.format("!((v10=%s) le (v3=%s))", vm.get("v10"), vm.get("v3")));
    if (!em.ge(vm.get("v10"), vm.get("v14"))) e.abort(String.format("!((v10=%s) ge (v14=%s))", vm.get("v10"), vm.get("v14")));
    if (!em.ge(vm.get("v10"), vm.get("v3"))) e.abort(String.format("!((v10=%s) ge (v3=%s))", vm.get("v10"), vm.get("v3")));
    if (!em.eq(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) eq (v13=%s))", vm.get("param0"), vm.get("v13")));
    if (!em.ge(vm.get("v17"), vm.get("v10"))) e.abort(String.format("!((v17=%s) ge (v10=%s))", vm.get("v17"), vm.get("v10")));
    if (!em.le(vm.get("v3"), vm.get("v9"))) e.abort(String.format("!((v3=%s) le (v9=%s))", vm.get("v3"), vm.get("v9")));
  }
  @IM(clazz="java.util.Vector", name="clear", desc="()V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementData[*]","this.elementCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_2599"})
  static void clear2899(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", null);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementCount"));
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", (int)1);
    vm.put("v7", om.popArrayElement(vm.get("v3"), vm.get("v6")));
    vm.put("v8", (int)2);
    vm.put("v9", om.popArrayElement(vm.get("v3"), vm.get("v0")));
    if (!em.eq(vm.get("v0"), (vm.get("v4")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v4")));
    vm.put("v10", vm.get("v5"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v6")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v12", om.getField(vm.get("this"), "elementCount"));
    if (!em.lt(vm.get("v0"), vm.get("v12"))) e.abort(String.format("!((v0=%s) lt (v12=%s))", vm.get("v0"), vm.get("v12")));
    if (!em.lt(vm.get("v6"), vm.get("v12"))) e.abort(String.format("!((v6=%s) lt (v12=%s))", vm.get("v6"), vm.get("v12")));
    if (!em.ge(vm.get("v8"), vm.get("v12"))) e.abort(String.format("!((v8=%s) ge (v12=%s))", vm.get("v8"), vm.get("v12")));
  }
  @IM(clazz="java.util.Vector", name="setSize", desc="(I)V",revertedInput={"this.modCount","this.elementCount"},definedOutput={"this.modCount","this.elementCount"})
  static void setSize4094(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementCount"));
    vm.put("v3", vm.get("v0"));
    vm.put("v4", em.op("isub").eval(vm.get("v3"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v4"));
    vm.put("param0", vm.get("v2"));
    om.revertField(vm.get("this"), "elementCount");
    vm.put("v5", om.getField(vm.get("this"), "elementCount"));
    if (!em.ge(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) ge (v5=%s))", vm.get("param0"), vm.get("v5")));
    if (!em.le(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) le (v5=%s))", vm.get("param0"), vm.get("v5")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1350(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "elementCount"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", (int)1);
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getArrayLength(vm.get("v6")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)7);
    vm.put("v12", null);
    vm.put("v13", java.lang.Object[].class);
    vm.put("v14", em.op("isub").eval(vm.get("v2"),vm.get("v0")));
    vm.put("v15", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    vm.put("v16", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v6")));
    vm.put("v17", om.getArrayElement(vm.get("v6"), vm.get("v0")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", em.op("isub").eval(vm.get("v2"),vm.get("v0")));
    vm.put("v20", vm.get("v3"));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v4")));
    om.revertField(vm.get("this"), "modCount", vm.get("v21"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.lt(vm.get("v0"), vm.get("v15"))) e.abort(String.format("!((v0=%s) lt (v15=%s))", vm.get("v0"), vm.get("v15")));
    if (!em.lt(vm.get("v14"), vm.get("v11"))) e.abort(String.format("!((v14=%s) lt (v11=%s))", vm.get("v14"), vm.get("v11")));
    if (!em.ne(vm.get("v16"), vm.get("v13"))) e.abort(String.format("!((v16=%s) ne (v13=%s))", vm.get("v16"), vm.get("v13")));
    if (!em.ge(vm.get("v4"), vm.get("v15"))) e.abort(String.format("!((v4=%s) ge (v15=%s))", vm.get("v4"), vm.get("v15")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ge(vm.get("v0"), vm.get("v2"))) e.abort(String.format("!((v0=%s) ge (v2=%s))", vm.get("v0"), vm.get("v2")));
    if (!em.ge(vm.get("v19"), vm.get("v0"))) e.abort(String.format("!((v19=%s) ge (v0=%s))", vm.get("v19"), vm.get("v0")));
    if (!em.le(vm.get("v15"), vm.get("v19"))) e.abort(String.format("!((v15=%s) le (v19=%s))", vm.get("v15"), vm.get("v19")));
    if (!em.ne(vm.get("v8"), vm.get("v0"))) e.abort(String.format("!((v8=%s) ne (v0=%s))", vm.get("v8"), vm.get("v0")));
    if (!em.le(vm.get("v2"), vm.get("v10"))) e.abort(String.format("!((v2=%s) le (v10=%s))", vm.get("v2"), vm.get("v10")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.le(vm.get("v0"), vm.get("v2"))) e.abort(String.format("!((v0=%s) le (v2=%s))", vm.get("v0"), vm.get("v2")));
  }
  @IM(clazz="java.util.Vector", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_2599"})
  static void sort1396(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getArrayLength(vm.get("v1")));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementCount"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", (int)1);
    vm.put("v10", om.getStatic("java.util.TimSort.$assertionsDisabled"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", (int)0);
    vm.put("v13", (int)2);
    vm.put("v14", null);
    vm.put("v15", em.op("isub").eval(vm.get("v7"),vm.get("v12")));
    vm.put("v16", vm.get("v8"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v9")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.lt(vm.get("v15"), vm.get("v13"))) e.abort(String.format("!((v15=%s) lt (v13=%s))", vm.get("v15"), vm.get("v13")));
    if (!em.ne(vm.get("v11"), vm.get("v12"))) e.abort(String.format("!((v11=%s) ne (v12=%s))", vm.get("v11"), vm.get("v12")));
    if (!em.eq(vm.get("v5"), vm.get("v12"))) e.abort(String.format("!((v5=%s) eq (v12=%s))", vm.get("v5"), vm.get("v12")));
    if (!em.le(vm.get("v7"), vm.get("v3"))) e.abort(String.format("!((v7=%s) le (v3=%s))", vm.get("v7"), vm.get("v3")));
    if (!em.ne(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) ne (v14=%s))", vm.get("param0"), vm.get("v14")));
    if (!em.le(vm.get("v12"), vm.get("v7"))) e.abort(String.format("!((v12=%s) le (v7=%s))", vm.get("v12"), vm.get("v7")));
  }
}
