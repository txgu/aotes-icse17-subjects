import org.javelus.aotes.executor.*;
@Defined({"this.first.next.next.next.item","this.first.prev.item","this.last.item","this.last.prev","this.last.prev.prev.next","this.first.prev.prev.next","this.last.prev.next","this.first.prev.prev","this.last.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.item","this.first.next.next.next.next.next.next.next.next.item","this.first.next.next.prev.next","this.first.next.next.prev","this.last.prev.prev","this.last.prev.prev.prev.next","this.first.next.next.item","this.last.prev.prev.prev.item","this.first.next.next.prev.prev.next","this.last.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev","this.first.next.next.next.next.next.item","this.first.next.prev","this.last","this.first.next","this.last.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.next","this.first.next.next.prev.item","this.first.item","this.first.next.item","this.last.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev","this.size","this.last.prev.prev.item","this.last.next","this.first","this.last.prev.prev.prev","this.first.next.next.prev.prev","this.first.prev.next","this.first.next.next.next.next.item","this.last.prev.item","this.modCount","this.first.prev"})
public class AOTES_java_util_LinkedList {
  @IM(clazz="java.util.LinkedList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.last.prev","this.modCount"},definedOutput={"this.size","this.last.prev","this.last.prev.prev.next","this.last.prev.prev","this.last.prev.item","this.modCount","this.last.prev.next"},delta={"this.last.prev.prev.next","this.last.prev.prev","this.last.prev.item","this.last.prev.next"})
  @DG(pred={"removeFirstOccurrence5596","add199","add399","add388","offerLast4799","remove6393","clear2398","linkFirst3199","add198","offerFirst5898","offer6099","linkLast4399","removeFirstOccurrence5525","push1499","addFirst2599","offerLast4798","offer6098","remove3993","linkLast4397","addLast3098","addLast3099"},succ={"add333","add379","remove3992","set5182","add301","set5148","removeLastOccurrence6267"})
  static void add389(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "last"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", null);
    vm.put("v6", (int)0);
    vm.put("v7", om.getField(vm.get("v1"), "prev"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.popField(vm.get("v8"), "item"));
    vm.put("param1", vm.get("v9"));
    vm.put("v10", om.popField(vm.get("v8"), "next"));
    vm.put("v11", om.popField(vm.get("v8"), "prev"));
    vm.put("v12", vm.get("v11"));
    om.revertField(vm.get("v1"), "prev", vm.get("v12"));
    vm.put("v13", om.popField(vm.get("v12"), "next"));
    if (!em.eq(vm.get("v8"), (vm.get("v13")))) e.abort("Inconsistent value for \"v8\": " + vm.get("v8") + " ne " + (vm.get("v13")));
    vm.put("v14", vm.get("v2"));
    if (!em.eq(vm.get("v1"), (vm.get("v10")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v10")));
    vm.put("v15", vm.get("v4"));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v16"));
    vm.put("v17", em.op("isub").eval(vm.get("v14"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v17"));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v3")));
    vm.put("v19", em.op("ishr").eval(vm.get("v17"),vm.get("v3")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.le(vm.get("param0"), vm.get("v17"))) e.abort(String.format("!((param0=%s) le (v17=%s))", vm.get("param0"), vm.get("v17")));
    if (!em.ne(vm.get("param0"), vm.get("v17"))) e.abort(String.format("!((param0=%s) ne (v17=%s))", vm.get("param0"), vm.get("v17")));
    if (!em.ge(vm.get("param0"), vm.get("v6"))) e.abort(String.format("!((param0=%s) ge (v6=%s))", vm.get("param0"), vm.get("v6")));
    if (!em.ne(vm.get("v12"), vm.get("v5"))) e.abort(String.format("!((v12=%s) ne (v5=%s))", vm.get("v12"), vm.get("v5")));
    if (!em.le(vm.get("v18"), vm.get("param0"))) e.abort(String.format("!((v18=%s) le (param0=%s))", vm.get("v18"), vm.get("param0")));
    if (!em.ge(vm.get("param0"), vm.get("v19"))) e.abort(String.format("!((param0=%s) ge (v19=%s))", vm.get("param0"), vm.get("v19")));
  }
  @IM(clazz="java.util.LinkedList", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.size","this.first.prev","this.modCount","this.first.item"},definedOutput={"this.size","this.last.next","this.last","this.first.prev","this.modCount","this.first.item"},delta={"this.last.next","this.last"})
  @DG(pred={"remove3991","add382","remove3992","add301","add388","removeLastOccurrence6298","clear2398","linkFirst3199","add198","offerFirst5898","add333","push1499","add396","addFirst2599","push1495","offerLast4798","linkBefore4697","offerFirst5899","offer6098","addFirst2598","linkFirst3196","linkLast4397","removeLastOccurrence6267","addLast3098"},succ={"remove3991","add382","add333","add379","set5192","remove3992","set5182","add301","add389","set5148","removeLastOccurrence6298","removeLastOccurrence6267"})
  static void remove3993(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.popField(vm.get("this"), "last"));
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "first"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v5"), "prev"));
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", (int)0);
    vm.put("v9", om.getField(vm.get("v5"), "item"));
    vm.put("v10", om.getField(vm.get("v5"), "next"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", vm.get("v1"));
    om.revertField(vm.get("v5"), "prev", vm.get("v12"));
    vm.put("v13", om.popField(vm.get("v12"), "next"));
    if (!em.eq(vm.get("v11"), (vm.get("v13")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v13")));
    if (!em.eq(vm.get("v0"), (vm.get("v6")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v6")));
    vm.put("v14", vm.get("v2"));
    vm.put("v15", em.op("iadd").eval(vm.get("v14"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v15"));
    vm.put("v16", em.op("ishr").eval(vm.get("v15"),vm.get("v3")));
    if (!em.eq(vm.get("v0"), (vm.get("v9")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v9")));
    vm.put("v17", vm.get("v7"));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v18"));
    vm.put("param0", om.newDefaultValue("int"));
    om.revertField(vm.get("v5"), "item");
    vm.put("v19", om.getField(vm.get("v5"), "item"));
    if (!em.lt(vm.get("param0"), vm.get("v16"))) e.abort(String.format("!((param0=%s) lt (v16=%s))", vm.get("param0"), vm.get("v16")));
    if (!em.lt(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) lt (v15=%s))", vm.get("param0"), vm.get("v15")));
    if (!em.eq(vm.get("v11"), vm.get("v0"))) e.abort(String.format("!((v11=%s) eq (v0=%s))", vm.get("v11"), vm.get("v0")));
    if (!em.ge(vm.get("v8"), vm.get("param0"))) e.abort(String.format("!((v8=%s) ge (param0=%s))", vm.get("v8"), vm.get("param0")));
    if (!em.ge(vm.get("param0"), vm.get("v8"))) e.abort(String.format("!((param0=%s) ge (v8=%s))", vm.get("param0"), vm.get("v8")));
    if (!em.ne(vm.get("v12"), vm.get("v0"))) e.abort(String.format("!((v12=%s) ne (v0=%s))", vm.get("v12"), vm.get("v0")));
  }
  @IM(clazz="java.util.LinkedList", name="set", desc="(ILjava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.first.item"},definedOutput={"this.first.item"})
  @DG(pred={"remove3991","add382","remove3992","add301","add388","removeLastOccurrence6298","clear2398","linkFirst3199","add198","offerFirst5898","add333","push1499","add396","addFirst2599","push1495","offerLast4798","linkBefore4697","offerFirst5899","offer6098","addFirst2598","linkFirst3196","linkLast4397","removeLastOccurrence6267","addLast3098"})
  static void set5189(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "first"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", (int)0);
    vm.put("v5", (int)1);
    vm.put("v6", om.getField(vm.get("v1"), "item"));
    vm.put("param1", vm.get("v6"));
    vm.put("v7", em.op("ishr").eval(vm.get("v3"),vm.get("v5")));
    vm.put("param0", om.newDefaultValue("int"));
    om.revertField(vm.get("v1"), "item");
    vm.put("v8", om.getField(vm.get("v1"), "item"));
    if (!em.lt(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) lt (v7=%s))", vm.get("param0"), vm.get("v7")));
    if (!em.ge(vm.get("v4"), vm.get("param0"))) e.abort(String.format("!((v4=%s) ge (param0=%s))", vm.get("v4"), vm.get("param0")));
    if (!em.lt(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) lt (v3=%s))", vm.get("param0"), vm.get("v3")));
    if (!em.ge(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) ge (v4=%s))", vm.get("param0"), vm.get("v4")));
  }
  @IM(clazz="java.util.LinkedList", name="clear", desc="()V",revertedInput={"this.first","this.modCount"},definedOutput={"this.size","this.first","this.last","this.modCount"},delta={"this.size","this.last"})
  @DG(succ={"removeFirstOccurrence5596","add382","set5107","set5189","remove3992","set5183","add301","set5148","removeLastOccurrence6298","set5194","removeFirstOccurrence5525","add333","add326","add374","add389","remove3993","set5141","remove3991","remove6393","add379","set5199","set5192","set5182","removeLastOccurrence6267","set5127"})
  static void clear2398(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "size"));
    vm.put("v1", (int)0);
    vm.put("v2", om.popField(vm.get("this"), "last"));
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("this"), "first"));
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", (int)1);
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    if (!em.eq(vm.get("v3"), (vm.get("v4")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v4")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    vm.put("v7", vm.get("v5"));
    vm.put("v8", em.op("isub").eval(vm.get("v7"),vm.get("v6")));
    om.revertField(vm.get("this"), "modCount", vm.get("v8"));
    om.revertField(vm.get("this"), "first");
    vm.put("v9", om.getField(vm.get("this"), "first"));
    if (!em.eq(vm.get("v9"), vm.get("v3"))) e.abort(String.format("!((v9=%s) eq (v3=%s))", vm.get("v9"), vm.get("v3")));
  }
  @IM(clazz="java.util.LinkedList", name="set", desc="(ILjava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.first.next.next.next.next.next.item"},definedOutput={"this.first.next.next.next.next.next.item"})
  @DG(pred={"remove3991","add382","remove3992","add301","add388","removeLastOccurrence6298","clear2398","linkFirst3199","add198","offerFirst5898","add333","push1499","add396","addFirst2599","push1495","offerLast4798","linkBefore4697","offerFirst5899","offer6098","addFirst2598","linkFirst3196","linkLast4397","removeLastOccurrence6267","addLast3098"})
  static void set5183(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "first"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v1"), "next"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", (int)5);
    vm.put("v7", (int)4);
    vm.put("v8", (int)3);
    vm.put("v9", (int)2);
    vm.put("v10", (int)1);
    vm.put("v11", (int)0);
    vm.put("v12", om.getField(vm.get("v5"), "next"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("v13"), "next"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.getField(vm.get("v15"), "next"));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.getField(vm.get("v17"), "next"));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", om.getField(vm.get("v19"), "item"));
    vm.put("param1", vm.get("v20"));
    vm.put("v21", em.op("ishr").eval(vm.get("v3"),vm.get("v10")));
    vm.put("param0", om.newDefaultValue("int"));
    om.revertField(vm.get("v19"), "item");
    vm.put("v22", om.getField(vm.get("v19"), "item"));
    if (!em.ge(vm.get("v6"), vm.get("param0"))) e.abort(String.format("!((v6=%s) ge (param0=%s))", vm.get("v6"), vm.get("param0")));
    if (!em.lt(vm.get("param0"), vm.get("v21"))) e.abort(String.format("!((param0=%s) lt (v21=%s))", vm.get("param0"), vm.get("v21")));
    if (!em.ge(vm.get("param0"), vm.get("v11"))) e.abort(String.format("!((param0=%s) ge (v11=%s))", vm.get("param0"), vm.get("v11")));
    if (!em.lt(vm.get("v7"), vm.get("param0"))) e.abort(String.format("!((v7=%s) lt (param0=%s))", vm.get("v7"), vm.get("param0")));
    if (!em.lt(vm.get("v10"), vm.get("param0"))) e.abort(String.format("!((v10=%s) lt (param0=%s))", vm.get("v10"), vm.get("param0")));
    if (!em.lt(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) lt (v3=%s))", vm.get("param0"), vm.get("v3")));
    if (!em.lt(vm.get("v9"), vm.get("param0"))) e.abort(String.format("!((v9=%s) lt (param0=%s))", vm.get("v9"), vm.get("param0")));
    if (!em.lt(vm.get("v11"), vm.get("param0"))) e.abort(String.format("!((v11=%s) lt (param0=%s))", vm.get("v11"), vm.get("param0")));
    if (!em.lt(vm.get("v8"), vm.get("param0"))) e.abort(String.format("!((v8=%s) lt (param0=%s))", vm.get("v8"), vm.get("param0")));
  }
  @IM(clazz="java.util.LinkedList", name="removeFirstOccurrence", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.first.next.prev","this.modCount","this.first.next.item"},definedOutput={"this.size","this.last.next","this.first.next.prev","this.last","this.modCount","this.first.next.item"},delta={"this.last.next","this.last"})
  @DG(pred={"remove3991","add382","remove3992","add301","add388","removeLastOccurrence6298","clear2398","linkFirst3199","add198","offerFirst5898","add333","push1499","add396","addFirst2599","push1495","offerLast4798","linkBefore4697","offerFirst5899","offer6098","addFirst2598","linkFirst3196","linkLast4397","removeLastOccurrence6267","addLast3098"},succ={"remove3991","add382","add333","add379","set5192","remove3992","set5182","add301","add389","set5148","removeLastOccurrence6298","removeLastOccurrence6267"})
  static void removeFirstOccurrence5525(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.getField(vm.get("this"), "first"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "size"));
    vm.put("v4", (int)1);
    vm.put("v5", om.popField(vm.get("this"), "last"));
    vm.put("v6", om.getField(vm.get("v2"), "next"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("v2"), "item"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("v7"), "next"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("v7"), "item"));
    vm.put("v13", om.getField(vm.get("this"), "modCount"));
    vm.put("v14", om.getField(vm.get("v7"), "prev"));
    if (!em.eq(vm.get("v0"), (vm.get("v12")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v12")));
    vm.put("v15", vm.get("v3"));
    vm.put("v16", em.op("iadd").eval(vm.get("v15"),vm.get("v4")));
    om.revertField(vm.get("this"), "size", vm.get("v16"));
    vm.put("v17", vm.get("v13"));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v4")));
    om.revertField(vm.get("this"), "modCount", vm.get("v18"));
    if (!em.eq(vm.get("v0"), (vm.get("v14")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v14")));
    vm.put("v19", vm.get("v5"));
    om.revertField(vm.get("v7"), "prev", vm.get("v19"));
    vm.put("v20", om.popField(vm.get("v19"), "next"));
    if (!em.eq(vm.get("v11"), (vm.get("v20")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v20")));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    om.revertField(vm.get("v7"), "item");
    vm.put("v21", om.getField(vm.get("v7"), "item"));
    if (!em.eq(vm.get("v21"), vm.get("v0"))) e.abort(String.format("!((v21=%s) eq (v0=%s))", vm.get("v21"), vm.get("v0")));
    if (!em.ne(vm.get("v9"), vm.get("v0"))) e.abort(String.format("!((v9=%s) ne (v0=%s))", vm.get("v9"), vm.get("v0")));
    if (!em.eq(vm.get("param0"), vm.get("v0"))) e.abort(String.format("!((param0=%s) eq (v0=%s))", vm.get("param0"), vm.get("v0")));
    if (!em.ne(vm.get("v19"), vm.get("v0"))) e.abort(String.format("!((v19=%s) ne (v0=%s))", vm.get("v19"), vm.get("v0")));
    if (!em.ne(vm.get("v7"), vm.get("v0"))) e.abort(String.format("!((v7=%s) ne (v0=%s))", vm.get("v7"), vm.get("v0")));
    if (!em.eq(vm.get("v11"), vm.get("v0"))) e.abort(String.format("!((v11=%s) eq (v0=%s))", vm.get("v11"), vm.get("v0")));
    if (!em.ne(vm.get("v2"), vm.get("v0"))) e.abort(String.format("!((v2=%s) ne (v0=%s))", vm.get("v2"), vm.get("v0")));
  }
  @IM(clazz="java.util.LinkedList", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.size","this.modCount","this.last.prev.prev.prev.next","this.last.prev.prev.prev.item"},definedOutput={"this.size","this.first","this.first.prev","this.modCount","this.last.prev.prev.prev.next","this.last.prev.prev.prev.item"},delta={"this.first","this.first.prev"})
  @DG(pred={"removeFirstOccurrence5596","add382","add301","add399","clear2398","linkFirst3199","add198","linkLast4399","removeFirstOccurrence5525","push1499","offerLast4798","offer6098","add389","remove3993","add199","add388","offerLast4799","remove6393","offerFirst5898","offer6099","add379","addFirst2599","linkLast4397","addLast3098","addLast3099"},succ={"removeFirstOccurrence5596","set5141","set5107","set5189","set5183","remove6393","set5194","removeFirstOccurrence5525","add326","set5199","add374","remove3993","set5127"})
  static void remove3992(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.getField(vm.get("this"), "size"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "last"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("v4"), "prev"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.popField(vm.get("this"), "first"));
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", (int)-1);
    vm.put("v10", (int)0);
    vm.put("v11", om.getField(vm.get("v6"), "prev"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getField(vm.get("v12"), "prev"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.getField(vm.get("v14"), "prev"));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getField(vm.get("v14"), "next"));
    vm.put("v18", om.getField(vm.get("v14"), "item"));
    vm.put("v19", vm.get("v7"));
    om.revertField(vm.get("v14"), "next", vm.get("v19"));
    vm.put("v20", om.popField(vm.get("v19"), "prev"));
    if (!em.eq(vm.get("v16"), (vm.get("v20")))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (vm.get("v20")));
    vm.put("v21", vm.get("v8"));
    vm.put("v22", em.op("isub").eval(vm.get("v21"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v22"));
    vm.put("v23", vm.get("v1"));
    vm.put("v24", em.op("iadd").eval(vm.get("v23"),vm.get("v2")));
    om.revertField(vm.get("this"), "size", vm.get("v24"));
    vm.put("v25", em.op("ishr").eval(vm.get("v24"),vm.get("v2")));
    vm.put("v26", em.op("isub").eval(vm.get("v24"),vm.get("v2")));
    vm.put("v27", em.op("iinc").eval(vm.get("v26"),vm.get("v9")));
    vm.put("v28", em.op("iinc").eval(vm.get("v27"),vm.get("v9")));
    vm.put("v29", em.op("iinc").eval(vm.get("v28"),vm.get("v9")));
    if (!em.eq(vm.get("v0"), (vm.get("v17")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v17")));
    if (!em.eq(vm.get("v0"), (vm.get("v18")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v18")));
    om.revertField(vm.get("v14"), "item");
    vm.put("v30", om.getField(vm.get("v14"), "item"));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.gt(vm.get("v27"), vm.get("param0"))) e.abort(String.format("!((v27=%s) gt (param0=%s))", vm.get("v27"), vm.get("param0")));
    if (!em.gt(vm.get("v28"), vm.get("param0"))) e.abort(String.format("!((v28=%s) gt (param0=%s))", vm.get("v28"), vm.get("param0")));
    if (!em.eq(vm.get("v16"), vm.get("v0"))) e.abort(String.format("!((v16=%s) eq (v0=%s))", vm.get("v16"), vm.get("v0")));
    if (!em.ne(vm.get("v19"), vm.get("v0"))) e.abort(String.format("!((v19=%s) ne (v0=%s))", vm.get("v19"), vm.get("v0")));
    if (!em.ge(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ge (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.gt(vm.get("v26"), vm.get("param0"))) e.abort(String.format("!((v26=%s) gt (param0=%s))", vm.get("v26"), vm.get("param0")));
    if (!em.lt(vm.get("param0"), vm.get("v24"))) e.abort(String.format("!((param0=%s) lt (v24=%s))", vm.get("param0"), vm.get("v24")));
    if (!em.ge(vm.get("param0"), vm.get("v25"))) e.abort(String.format("!((param0=%s) ge (v25=%s))", vm.get("param0"), vm.get("v25")));
    if (!em.le(vm.get("v29"), vm.get("param0"))) e.abort(String.format("!((v29=%s) le (param0=%s))", vm.get("v29"), vm.get("param0")));
  }
  @IM(clazz="java.util.LinkedList", name="<init>", desc="()V",definedOutput={"this.size","this.modCount"},delta={"this.size","this.modCount"})
  static void _init_2999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "modCount"));
    vm.put("v1", (int)0);
    vm.put("v2", om.popField(vm.get("this"), "size"));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v1"), (vm.get("v2")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v2")));
  }
  @IM(clazz="java.util.LinkedList", name="removeLastOccurrence", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.last.next","this.last.item","this.modCount"},definedOutput={"this.size","this.last.next","this.first","this.last.item","this.first.prev","this.modCount"},delta={"this.first","this.first.prev"})
  @DG(pred={"removeFirstOccurrence5596","add199","add399","add388","offerLast4799","remove6393","clear2398","linkFirst3199","add198","offerFirst5898","offer6099","linkLast4399","removeFirstOccurrence5525","push1499","addFirst2599","offerLast4798","offer6098","remove3993","linkLast4397","addLast3098","addLast3099"},succ={"removeFirstOccurrence5596","set5141","set5107","set5189","set5183","remove6393","set5194","removeFirstOccurrence5525","add326","set5199","add374","remove3993","set5127"})
  static void removeLastOccurrence6298(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", (int)1);
    vm.put("v2", null);
    vm.put("v3", om.popField(vm.get("this"), "first"));
    vm.put("v4", om.getField(vm.get("this"), "last"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v5"), "item"));
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", om.getField(vm.get("v5"), "next"));
    vm.put("v9", om.getField(vm.get("v5"), "prev"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", vm.get("v7"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", vm.get("v3"));
    om.revertField(vm.get("v5"), "next", vm.get("v13"));
    vm.put("v14", om.popField(vm.get("v13"), "prev"));
    vm.put("v15", vm.get("v0"));
    if (!em.eq(vm.get("v2"), (vm.get("v8")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v8")));
    if (!em.eq(vm.get("v10"), (vm.get("v14")))) e.abort("Inconsistent value for \"v10\": " + vm.get("v10") + " ne " + (vm.get("v14")));
    vm.put("v16", em.op("iadd").eval(vm.get("v15"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v16"));
    if (!em.eq(vm.get("v2"), (vm.get("v6")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v6")));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    om.revertField(vm.get("v5"), "item");
    vm.put("v17", om.getField(vm.get("v5"), "item"));
    if (!em.eq(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) eq (v2=%s))", vm.get("param0"), vm.get("v2")));
    if (!em.eq(vm.get("v10"), vm.get("v2"))) e.abort(String.format("!((v10=%s) eq (v2=%s))", vm.get("v10"), vm.get("v2")));
    if (!em.ne(vm.get("v5"), vm.get("v2"))) e.abort(String.format("!((v5=%s) ne (v2=%s))", vm.get("v5"), vm.get("v2")));
    if (!em.ne(vm.get("v13"), vm.get("v2"))) e.abort(String.format("!((v13=%s) ne (v2=%s))", vm.get("v13"), vm.get("v2")));
    if (!em.eq(vm.get("v17"), vm.get("v2"))) e.abort(String.format("!((v17=%s) eq (v2=%s))", vm.get("v17"), vm.get("v2")));
  }
  @IM(clazz="java.util.LinkedList", name="set", desc="(ILjava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.first.next.next.item"},definedOutput={"this.first.next.next.item"})
  @DG(pred={"remove3991","add382","remove3992","add301","add388","removeLastOccurrence6298","clear2398","linkFirst3199","add198","offerFirst5898","add333","push1499","add396","addFirst2599","push1495","offerLast4798","linkBefore4697","offerFirst5899","offer6098","addFirst2598","linkFirst3196","linkLast4397","removeLastOccurrence6267","addLast3098"})
  static void set5194(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "first"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v3"), "next"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v5"), "next"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)2);
    vm.put("v9", (int)1);
    vm.put("v10", (int)0);
    vm.put("v11", om.getField(vm.get("v7"), "item"));
    vm.put("param1", vm.get("v11"));
    vm.put("v12", em.op("ishr").eval(vm.get("v1"),vm.get("v9")));
    om.revertField(vm.get("v7"), "item");
    vm.put("v13", om.getField(vm.get("v7"), "item"));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.lt(vm.get("v10"), vm.get("param0"))) e.abort(String.format("!((v10=%s) lt (param0=%s))", vm.get("v10"), vm.get("param0")));
    if (!em.ge(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ge (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.lt(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) lt (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.lt(vm.get("v9"), vm.get("param0"))) e.abort(String.format("!((v9=%s) lt (param0=%s))", vm.get("v9"), vm.get("param0")));
    if (!em.lt(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) lt (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ge(vm.get("v8"), vm.get("param0"))) e.abort(String.format("!((v8=%s) ge (param0=%s))", vm.get("v8"), vm.get("param0")));
  }
  @IM(clazz="java.util.LinkedList", name="add", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.last","this.modCount"},definedOutput={"this.size","this.last.next","this.first","this.last.item","this.last","this.last.prev","this.modCount"},delta={"this.last.next","this.first","this.last.item","this.last.prev"})
  @DG(succ={"removeFirstOccurrence5596","add382","set5107","set5189","remove3992","set5183","add301","set5148","removeLastOccurrence6298","set5194","removeFirstOccurrence5525","add333","add326","add374","add389","remove3993","set5141","remove3991","remove6393","add379","set5199","set5192","set5182","removeLastOccurrence6267","set5127"})
  static void add198(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", null);
    vm.put("v3", om.getField(vm.get("this"), "size"));
    vm.put("v4", om.getField(vm.get("this"), "last"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popField(vm.get("v5"), "item"));
    vm.put("v7", om.popField(vm.get("v5"), "prev"));
    vm.put("v8", vm.get("v7"));
    om.revertField(vm.get("this"), "last", vm.get("v8"));
    vm.put("v9", om.popField(vm.get("this"), "first"));
    vm.put("v10", om.popField(vm.get("v5"), "next"));
    if (!em.eq(vm.get("v5"), (vm.get("v9")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v9")));
    vm.put("param0", vm.get("v6"));
    vm.put("v11", vm.get("v0"));
    if (!em.eq(vm.get("v2"), (vm.get("v10")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v10")));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", vm.get("v3"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v14"));
    if (!em.eq(vm.get("v8"), vm.get("v2"))) e.abort(String.format("!((v8=%s) eq (v2=%s))", vm.get("v8"), vm.get("v2")));
  }
  @IM(clazz="java.util.LinkedList", name="offerLast", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.last","this.modCount"},definedOutput={"this.size","this.first","this.last.next","this.last.item","this.last","this.last.prev","this.modCount"},delta={"this.first","this.last.next","this.last.item","this.last.prev"})
  @DG(succ={"removeFirstOccurrence5596","add382","set5107","set5189","remove3992","set5183","add301","set5148","removeLastOccurrence6298","set5194","removeFirstOccurrence5525","add333","add326","add374","add389","remove3993","set5141","remove3991","remove6393","add379","set5199","set5192","set5182","removeLastOccurrence6267","set5127"})
  static void offerLast4798(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "last"));
    vm.put("v3", om.getField(vm.get("this"), "size"));
    vm.put("v4", om.popField(vm.get("this"), "first"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popField(vm.get("v5"), "item"));
    vm.put("v7", om.popField(vm.get("v5"), "next"));
    vm.put("v8", null);
    vm.put("v9", om.popField(vm.get("v5"), "prev"));
    vm.put("v10", vm.get("v9"));
    om.revertField(vm.get("this"), "last", vm.get("v10"));
    if (!em.eq(vm.get("v8"), (vm.get("v7")))) e.abort("Inconsistent value for \"v8\": " + vm.get("v8") + " ne " + (vm.get("v7")));
    if (!em.eq(vm.get("v5"), (vm.get("v2")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v2")));
    vm.put("v11", vm.get("v0"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", vm.get("v3"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v14"));
    vm.put("param0", vm.get("v6"));
    if (!em.eq(vm.get("v10"), vm.get("v8"))) e.abort(String.format("!((v10=%s) eq (v8=%s))", vm.get("v10"), vm.get("v8")));
  }
  @IM(clazz="java.util.LinkedList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.first","this.first.next.prev","this.modCount"},definedOutput={"this.size","this.first","this.first.next.prev","this.first.next","this.first.prev","this.modCount","this.first.item"},delta={"this.first.next","this.first.prev","this.first.item"})
  @DG(succ={"removeFirstOccurrence5596","set5141","set5107","set5189","set5183","remove6393","set5194","removeFirstOccurrence5525","add326","set5199","add374","remove3993","set5127"})
  static void add396(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "first"));
    vm.put("v1", om.getField(vm.get("this"), "size"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", null);
    vm.put("v5", (int)0);
    vm.put("v6", vm.get("v1"));
    vm.put("v7", vm.get("v3"));
    vm.put("v8", vm.get("v0"));
    vm.put("v9", om.popField(vm.get("v8"), "next"));
    vm.put("v10", vm.get("v9"));
    om.revertField(vm.get("this"), "first", vm.get("v10"));
    vm.put("v11", om.popField(vm.get("v8"), "item"));
    vm.put("param1", vm.get("v11"));
    vm.put("v12", om.popField(vm.get("v8"), "prev"));
    vm.put("v13", vm.get("v12"));
    om.revertField(vm.get("v10"), "prev", vm.get("v13"));
    vm.put("v14", om.getField(vm.get("v10"), "prev"));
    if (!em.eq(vm.get("v8"), (vm.get("v14")))) e.abort("Inconsistent value for \"v8\": " + vm.get("v8") + " ne " + (vm.get("v14")));
    vm.put("v15", em.op("isub").eval(vm.get("v7"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    vm.put("v16", em.op("isub").eval(vm.get("v6"),vm.get("v2")));
    om.revertField(vm.get("this"), "size", vm.get("v16"));
    vm.put("v17", em.op("ishr").eval(vm.get("v16"),vm.get("v2")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.ne(vm.get("param0"), vm.get("v16"))) e.abort(String.format("!((param0=%s) ne (v16=%s))", vm.get("param0"), vm.get("v16")));
    if (!em.lt(vm.get("param0"), vm.get("v17"))) e.abort(String.format("!((param0=%s) lt (v17=%s))", vm.get("param0"), vm.get("v17")));
    if (!em.le(vm.get("param0"), vm.get("v16"))) e.abort(String.format("!((param0=%s) le (v16=%s))", vm.get("param0"), vm.get("v16")));
    if (!em.ge(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) ge (v5=%s))", vm.get("param0"), vm.get("v5")));
    if (!em.ge(vm.get("v5"), vm.get("param0"))) e.abort(String.format("!((v5=%s) ge (param0=%s))", vm.get("v5"), vm.get("param0")));
    if (!em.eq(vm.get("v13"), vm.get("v4"))) e.abort(String.format("!((v13=%s) eq (v4=%s))", vm.get("v13"), vm.get("v4")));
  }
  @IM(clazz="java.util.LinkedList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.last","this.modCount"},definedOutput={"this.size","this.last.next","this.last.item","this.last","this.last.prev","this.modCount","this.last.prev.next"},delta={"this.last.next","this.last.item","this.last.prev","this.last.prev.next"})
  @DG(succ={"remove3991","add382","add333","add379","set5192","remove3992","set5182","add301","add389","set5148","removeLastOccurrence6298","removeLastOccurrence6267"})
  static void add399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.getField(vm.get("this"), "size"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", om.getField(vm.get("this"), "last"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popField(vm.get("v5"), "prev"));
    vm.put("v7", vm.get("v6"));
    om.revertField(vm.get("this"), "last", vm.get("v7"));
    vm.put("v8", om.popField(vm.get("v5"), "item"));
    vm.put("v9", om.popField(vm.get("v7"), "next"));
    vm.put("v10", (int)0);
    vm.put("v11", om.popField(vm.get("v5"), "next"));
    vm.put("v12", vm.get("v1"));
    if (!em.eq(vm.get("v5"), (vm.get("v9")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v9")));
    vm.put("v13", vm.get("v3"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    vm.put("v15", em.op("isub").eval(vm.get("v12"),vm.get("v2")));
    om.revertField(vm.get("this"), "size", vm.get("v15"));
    vm.put("param1", vm.get("v8"));
    if (!em.eq(vm.get("v0"), (vm.get("v11")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v11")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.ne(vm.get("v7"), vm.get("v0"))) e.abort(String.format("!((v7=%s) ne (v0=%s))", vm.get("v7"), vm.get("v0")));
    if (!em.eq(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) eq (v15=%s))", vm.get("param0"), vm.get("v15")));
    if (!em.ge(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ge (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.le(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) le (v15=%s))", vm.get("param0"), vm.get("v15")));
  }
  @IM(clazz="java.util.LinkedList", name="offer", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.last","this.modCount"},definedOutput={"this.size","this.last.next","this.last.item","this.last","this.last.prev","this.modCount","this.last.prev.next"},delta={"this.last.next","this.last.item","this.last.prev","this.last.prev.next"})
  @DG(succ={"remove3991","add382","add333","add379","set5192","remove3992","set5182","add301","add389","set5148","removeLastOccurrence6298","removeLastOccurrence6267"})
  static void offer6099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "last"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.popField(vm.get("v3"), "next"));
    vm.put("v5", null);
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", om.popField(vm.get("v3"), "prev"));
    vm.put("v8", vm.get("v7"));
    om.revertField(vm.get("this"), "last", vm.get("v8"));
    vm.put("v9", om.popField(vm.get("v3"), "item"));
    vm.put("param0", vm.get("v9"));
    vm.put("v10", om.popField(vm.get("v8"), "next"));
    if (!em.eq(vm.get("v3"), (vm.get("v10")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v10")));
    vm.put("v11", vm.get("v6"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v12"));
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    vm.put("v13", vm.get("v0"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    if (!em.ne(vm.get("v8"), vm.get("v5"))) e.abort(String.format("!((v8=%s) ne (v5=%s))", vm.get("v8"), vm.get("v5")));
  }
  @IM(clazz="java.util.LinkedList", name="offer", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.last","this.modCount"},definedOutput={"this.size","this.first","this.last.next","this.last.item","this.last","this.last.prev","this.modCount"},delta={"this.first","this.last.next","this.last.item","this.last.prev"})
  @DG(succ={"removeFirstOccurrence5596","add382","set5107","set5189","remove3992","set5183","add301","set5148","removeLastOccurrence6298","set5194","removeFirstOccurrence5525","add333","add326","add374","add389","remove3993","set5141","remove3991","remove6393","add379","set5199","set5192","set5182","removeLastOccurrence6267","set5127"})
  static void offer6098(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "first"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", om.popField(vm.get("v1"), "next"));
    vm.put("v5", null);
    vm.put("v6", om.getField(vm.get("this"), "last"));
    vm.put("v7", om.getField(vm.get("this"), "size"));
    vm.put("v8", om.popField(vm.get("v1"), "prev"));
    vm.put("v9", vm.get("v8"));
    om.revertField(vm.get("this"), "last", vm.get("v9"));
    vm.put("v10", om.popField(vm.get("v1"), "item"));
    vm.put("param0", vm.get("v10"));
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    vm.put("v11", vm.get("v7"));
    vm.put("v12", vm.get("v2"));
    vm.put("v13", em.op("isub").eval(vm.get("v11"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v12"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    if (!em.eq(vm.get("v1"), (vm.get("v6")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v6")));
    if (!em.eq(vm.get("v9"), vm.get("v5"))) e.abort(String.format("!((v9=%s) eq (v5=%s))", vm.get("v9"), vm.get("v5")));
  }
  @IM(clazz="java.util.LinkedList", name="set", desc="(ILjava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.last.item"},definedOutput={"this.last.item"})
  @DG(pred={"removeFirstOccurrence5596","add199","add399","add388","offerLast4799","remove6393","clear2398","linkFirst3199","add198","offerFirst5898","offer6099","linkLast4399","removeFirstOccurrence5525","push1499","addFirst2599","offerLast4798","offer6098","remove3993","linkLast4397","addLast3098","addLast3099"})
  static void set5192(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "last"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", (int)1);
    vm.put("v5", (int)0);
    vm.put("v6", om.getField(vm.get("v3"), "item"));
    vm.put("param1", vm.get("v6"));
    vm.put("v7", em.op("isub").eval(vm.get("v1"),vm.get("v4")));
    vm.put("v8", em.op("ishr").eval(vm.get("v1"),vm.get("v4")));
    vm.put("param0", om.newDefaultValue("int"));
    om.revertField(vm.get("v3"), "item");
    vm.put("v9", om.getField(vm.get("v3"), "item"));
    if (!em.le(vm.get("v7"), vm.get("param0"))) e.abort(String.format("!((v7=%s) le (param0=%s))", vm.get("v7"), vm.get("param0")));
    if (!em.lt(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) lt (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.ge(vm.get("param0"), vm.get("v8"))) e.abort(String.format("!((param0=%s) ge (v8=%s))", vm.get("param0"), vm.get("v8")));
    if (!em.ge(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) ge (v5=%s))", vm.get("param0"), vm.get("v5")));
  }
  @IM(clazz="java.util.LinkedList", name="addFirst", desc="(Ljava/lang/Object;)V",revertedInput={"this.size","this.first","this.modCount"},definedOutput={"this.size","this.first","this.first.next.prev","this.first.next","this.first.prev","this.modCount","this.first.item"},delta={"this.first.next.prev","this.first.next","this.first.prev","this.first.item"})
  @DG(succ={"removeFirstOccurrence5596","set5141","set5107","set5189","set5183","remove6393","set5194","removeFirstOccurrence5525","add326","set5199","add374","remove3993","set5127"})
  static void addFirst2598(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.getField(vm.get("this"), "modCount"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "first"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "size"));
    vm.put("v6", om.popField(vm.get("v4"), "prev"));
    vm.put("v7", om.popField(vm.get("v4"), "item"));
    vm.put("param0", vm.get("v7"));
    vm.put("v8", om.popField(vm.get("v4"), "next"));
    vm.put("v9", vm.get("v8"));
    om.revertField(vm.get("this"), "first", vm.get("v9"));
    vm.put("v10", om.popField(vm.get("v9"), "prev"));
    if (!em.eq(vm.get("v0"), (vm.get("v6")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v6")));
    if (!em.eq(vm.get("v4"), (vm.get("v10")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v10")));
    vm.put("v11", vm.get("v5"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v2")));
    om.revertField(vm.get("this"), "size", vm.get("v12"));
    vm.put("v13", vm.get("v1"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    if (!em.ne(vm.get("v9"), vm.get("v0"))) e.abort(String.format("!((v9=%s) ne (v0=%s))", vm.get("v9"), vm.get("v0")));
  }
  @IM(clazz="java.util.LinkedList", name="removeFirstOccurrence", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.first.prev","this.modCount","this.first.item"},definedOutput={"this.size","this.last.next","this.last","this.first.prev","this.modCount","this.first.item"},delta={"this.last.next","this.last"})
  @DG(pred={"remove3991","add382","remove3992","add301","add388","removeLastOccurrence6298","clear2398","linkFirst3199","add198","offerFirst5898","add333","push1499","add396","addFirst2599","push1495","offerLast4798","linkBefore4697","offerFirst5899","offer6098","addFirst2598","linkFirst3196","linkLast4397","removeLastOccurrence6267","addLast3098"},succ={"remove3991","add382","add333","add379","set5192","remove3992","set5182","add301","add389","set5148","removeLastOccurrence6298","removeLastOccurrence6267"})
  static void removeFirstOccurrence5596(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.popField(vm.get("this"), "last"));
    vm.put("v2", om.getField(vm.get("this"), "first"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v3"), "item"));
    vm.put("v5", om.getField(vm.get("this"), "size"));
    vm.put("v6", (int)1);
    vm.put("v7", om.getField(vm.get("v3"), "next"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("this"), "modCount"));
    vm.put("v10", om.getField(vm.get("v3"), "prev"));
    vm.put("v11", vm.get("v1"));
    om.revertField(vm.get("v3"), "prev", vm.get("v11"));
    vm.put("v12", om.popField(vm.get("v11"), "next"));
    vm.put("v13", vm.get("v9"));
    if (!em.eq(vm.get("v8"), (vm.get("v12")))) e.abort("Inconsistent value for \"v8\": " + vm.get("v8") + " ne " + (vm.get("v12")));
    if (!em.eq(vm.get("v0"), (vm.get("v10")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v10")));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v6")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    if (!em.eq(vm.get("v0"), (vm.get("v4")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v4")));
    vm.put("v15", vm.get("v5"));
    vm.put("v16", em.op("iadd").eval(vm.get("v15"),vm.get("v6")));
    om.revertField(vm.get("this"), "size", vm.get("v16"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    om.revertField(vm.get("v3"), "item");
    vm.put("v17", om.getField(vm.get("v3"), "item"));
    if (!em.eq(vm.get("v17"), vm.get("v0"))) e.abort(String.format("!((v17=%s) eq (v0=%s))", vm.get("v17"), vm.get("v0")));
    if (!em.ne(vm.get("v11"), vm.get("v0"))) e.abort(String.format("!((v11=%s) ne (v0=%s))", vm.get("v11"), vm.get("v0")));
    if (!em.eq(vm.get("param0"), vm.get("v0"))) e.abort(String.format("!((param0=%s) eq (v0=%s))", vm.get("param0"), vm.get("v0")));
    if (!em.ne(vm.get("v3"), vm.get("v0"))) e.abort(String.format("!((v3=%s) ne (v0=%s))", vm.get("v3"), vm.get("v0")));
    if (!em.eq(vm.get("v8"), vm.get("v0"))) e.abort(String.format("!((v8=%s) eq (v0=%s))", vm.get("v8"), vm.get("v0")));
  }
  @IM(clazz="java.util.LinkedList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.last","this.modCount"},definedOutput={"this.size","this.first","this.last.next","this.last.item","this.last","this.last.prev","this.modCount"},delta={"this.first","this.last.next","this.last.item","this.last.prev"})
  @DG(succ={"removeFirstOccurrence5596","add382","set5107","set5189","remove3992","set5183","add301","set5148","removeLastOccurrence6298","set5194","removeFirstOccurrence5525","add333","add326","add374","add389","remove3993","set5141","remove3991","remove6393","add379","set5199","set5192","set5182","removeLastOccurrence6267","set5127"})
  static void add388(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "last"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.popField(vm.get("this"), "first"));
    vm.put("v5", om.popField(vm.get("v3"), "prev"));
    vm.put("v6", vm.get("v5"));
    om.revertField(vm.get("this"), "last", vm.get("v6"));
    vm.put("v7", om.popField(vm.get("v3"), "next"));
    vm.put("v8", null);
    vm.put("v9", om.getField(vm.get("this"), "size"));
    vm.put("v10", (int)0);
    vm.put("v11", om.popField(vm.get("v3"), "item"));
    vm.put("param1", vm.get("v11"));
    vm.put("v12", vm.get("v9"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v13"));
    if (!em.eq(vm.get("v8"), (vm.get("v7")))) e.abort("Inconsistent value for \"v8\": " + vm.get("v8") + " ne " + (vm.get("v7")));
    vm.put("v14", vm.get("v0"));
    if (!em.eq(vm.get("v3"), (vm.get("v4")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v4")));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.eq(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) eq (v13=%s))", vm.get("param0"), vm.get("v13")));
    if (!em.le(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) le (v13=%s))", vm.get("param0"), vm.get("v13")));
    if (!em.ge(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ge (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.eq(vm.get("v6"), vm.get("v8"))) e.abort(String.format("!((v6=%s) eq (v8=%s))", vm.get("v6"), vm.get("v8")));
  }
  @IM(clazz="java.util.LinkedList", name="removeLastOccurrence", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.last.prev.item","this.modCount","this.last.prev.next"},definedOutput={"this.size","this.first","this.last.prev.item","this.first.prev","this.modCount","this.last.prev.next"},delta={"this.first","this.first.prev"})
  @DG(pred={"removeFirstOccurrence5596","add199","add382","add399","add388","offerLast4799","remove6393","clear2398","linkFirst3199","add198","offerFirst5898","offer6099","linkLast4399","removeFirstOccurrence5525","push1499","addFirst2599","offerLast4798","offer6098","add389","remove3993","linkLast4397","addLast3098","addLast3099"},succ={"removeFirstOccurrence5596","set5141","set5107","set5189","set5183","remove6393","set5194","removeFirstOccurrence5525","add326","set5199","add374","remove3993","set5127"})
  static void removeLastOccurrence6267(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.getField(vm.get("this"), "modCount"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "size"));
    vm.put("v4", om.getField(vm.get("this"), "last"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v5"), "item"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.popField(vm.get("this"), "first"));
    vm.put("v9", om.getField(vm.get("v5"), "prev"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getField(vm.get("v10"), "item"));
    vm.put("v12", om.getField(vm.get("v10"), "prev"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("v10"), "next"));
    if (!em.eq(vm.get("v0"), (vm.get("v11")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v11")));
    vm.put("v15", vm.get("v1"));
    vm.put("v16", vm.get("v8"));
    om.revertField(vm.get("v10"), "next", vm.get("v16"));
    vm.put("v17", om.popField(vm.get("v16"), "prev"));
    vm.put("v18", vm.get("v3"));
    if (!em.eq(vm.get("v13"), (vm.get("v17")))) e.abort("Inconsistent value for \"v13\": " + vm.get("v13") + " ne " + (vm.get("v17")));
    vm.put("v19", em.op("iadd").eval(vm.get("v18"),vm.get("v2")));
    om.revertField(vm.get("this"), "size", vm.get("v19"));
    if (!em.eq(vm.get("v0"), (vm.get("v14")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v14")));
    vm.put("v20", em.op("isub").eval(vm.get("v15"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v20"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    om.revertField(vm.get("v10"), "item");
    vm.put("v21", om.getField(vm.get("v10"), "item"));
    if (!em.ne(vm.get("v5"), vm.get("v0"))) e.abort(String.format("!((v5=%s) ne (v0=%s))", vm.get("v5"), vm.get("v0")));
    if (!em.ne(vm.get("v10"), vm.get("v0"))) e.abort(String.format("!((v10=%s) ne (v0=%s))", vm.get("v10"), vm.get("v0")));
    if (!em.eq(vm.get("param0"), vm.get("v0"))) e.abort(String.format("!((param0=%s) eq (v0=%s))", vm.get("param0"), vm.get("v0")));
    if (!em.ne(vm.get("v7"), vm.get("v0"))) e.abort(String.format("!((v7=%s) ne (v0=%s))", vm.get("v7"), vm.get("v0")));
    if (!em.eq(vm.get("v21"), vm.get("v0"))) e.abort(String.format("!((v21=%s) eq (v0=%s))", vm.get("v21"), vm.get("v0")));
    if (!em.eq(vm.get("v13"), vm.get("v0"))) e.abort(String.format("!((v13=%s) eq (v0=%s))", vm.get("v13"), vm.get("v0")));
    if (!em.ne(vm.get("v16"), vm.get("v0"))) e.abort(String.format("!((v16=%s) ne (v0=%s))", vm.get("v16"), vm.get("v0")));
  }
  @IM(clazz="java.util.LinkedList", name="addLast", desc="(Ljava/lang/Object;)V",revertedInput={"this.size","this.last","this.modCount"},definedOutput={"this.size","this.last.next","this.last.item","this.last","this.last.prev","this.modCount","this.last.prev.next"},delta={"this.last.next","this.last.item","this.last.prev","this.last.prev.next"})
  @DG(succ={"remove3991","add382","add333","add379","set5192","remove3992","set5182","add301","add389","set5148","removeLastOccurrence6298","removeLastOccurrence6267"})
  static void addLast3099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", om.getField(vm.get("this"), "last"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.popField(vm.get("v4"), "next"));
    vm.put("v6", null);
    vm.put("v7", om.popField(vm.get("v4"), "prev"));
    vm.put("v8", vm.get("v7"));
    om.revertField(vm.get("this"), "last", vm.get("v8"));
    vm.put("v9", om.popField(vm.get("v8"), "next"));
    vm.put("v10", om.popField(vm.get("v4"), "item"));
    vm.put("param0", vm.get("v10"));
    if (!em.eq(vm.get("v4"), (vm.get("v9")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v9")));
    if (!em.eq(vm.get("v6"), (vm.get("v5")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v5")));
    vm.put("v11", vm.get("v2"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", vm.get("v0"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v14"));
    if (!em.ne(vm.get("v8"), vm.get("v6"))) e.abort(String.format("!((v8=%s) ne (v6=%s))", vm.get("v8"), vm.get("v6")));
  }
  @IM(clazz="java.util.LinkedList", name="offerFirst", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.first","this.modCount"},definedOutput={"this.size","this.last.next","this.first","this.last.item","this.last","this.last.prev","this.modCount"},delta={"this.last.next","this.last.item","this.last","this.last.prev"})
  @DG(succ={"removeFirstOccurrence5596","add382","set5107","set5189","remove3992","set5183","add301","set5148","removeLastOccurrence6298","set5194","removeFirstOccurrence5525","add333","add326","add374","add389","remove3993","set5141","remove3991","remove6393","add379","set5199","set5192","set5182","removeLastOccurrence6267","set5127"})
  static void offerFirst5898(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.popField(vm.get("this"), "last"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.popField(vm.get("v2"), "next"));
    vm.put("v4", vm.get("v3"));
    om.revertField(vm.get("this"), "first", vm.get("v4"));
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", (int)1);
    vm.put("v7", om.popField(vm.get("v2"), "item"));
    vm.put("v8", om.getField(vm.get("this"), "first"));
    vm.put("v9", om.getField(vm.get("this"), "size"));
    vm.put("v10", om.popField(vm.get("v2"), "prev"));
    if (!em.eq(vm.get("v2"), (vm.get("v8")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v8")));
    vm.put("v11", vm.get("v9"));
    if (!em.eq(vm.get("v0"), (vm.get("v10")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v10")));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v6")));
    om.revertField(vm.get("this"), "size", vm.get("v12"));
    vm.put("param0", vm.get("v7"));
    vm.put("v13", vm.get("v5"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v6")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    if (!em.eq(vm.get("v4"), vm.get("v0"))) e.abort(String.format("!((v4=%s) eq (v0=%s))", vm.get("v4"), vm.get("v0")));
  }
  @IM(clazz="java.util.LinkedList", name="push", desc="(Ljava/lang/Object;)V",revertedInput={"this.size","this.first","this.modCount"},definedOutput={"this.size","this.first","this.last.next","this.last.item","this.last","this.last.prev","this.modCount"},delta={"this.last.next","this.last.item","this.last","this.last.prev"})
  @DG(succ={"removeFirstOccurrence5596","add382","set5107","set5189","remove3992","set5183","add301","set5148","removeLastOccurrence6298","set5194","removeFirstOccurrence5525","add333","add326","add374","add389","remove3993","set5141","remove3991","remove6393","add379","set5199","set5192","set5182","removeLastOccurrence6267","set5127"})
  static void push1499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "last"));
    vm.put("v1", null);
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", om.getField(vm.get("this"), "first"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.popField(vm.get("v6"), "next"));
    vm.put("v8", vm.get("v7"));
    om.revertField(vm.get("this"), "first", vm.get("v8"));
    vm.put("v9", om.popField(vm.get("v6"), "item"));
    vm.put("v10", om.popField(vm.get("v6"), "prev"));
    vm.put("param0", vm.get("v9"));
    vm.put("v11", vm.get("v2"));
    if (!em.eq(vm.get("v6"), (vm.get("v0")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v0")));
    vm.put("v12", vm.get("v4"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v11"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    if (!em.eq(vm.get("v1"), (vm.get("v10")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v10")));
    if (!em.eq(vm.get("v8"), vm.get("v1"))) e.abort(String.format("!((v8=%s) eq (v1=%s))", vm.get("v8"), vm.get("v1")));
  }
  @IM(clazz="java.util.LinkedList", name="add", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.last","this.modCount"},definedOutput={"this.size","this.last.next","this.last.item","this.last","this.last.prev","this.modCount","this.last.prev.next"},delta={"this.last.next","this.last.item","this.last.prev","this.last.prev.next"})
  @DG(succ={"remove3991","add382","add333","add379","set5192","remove3992","set5182","add301","add389","set5148","removeLastOccurrence6298","removeLastOccurrence6267"})
  static void add199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("this"), "last"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popField(vm.get("v5"), "next"));
    vm.put("v7", om.popField(vm.get("v5"), "item"));
    vm.put("param0", vm.get("v7"));
    vm.put("v8", om.popField(vm.get("v5"), "prev"));
    vm.put("v9", vm.get("v8"));
    om.revertField(vm.get("this"), "last", vm.get("v9"));
    vm.put("v10", om.popField(vm.get("v9"), "next"));
    if (!em.eq(vm.get("v3"), (vm.get("v6")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v6")));
    vm.put("v11", vm.get("v0"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v12"));
    vm.put("v13", vm.get("v2"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    if (!em.eq(vm.get("v5"), (vm.get("v10")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v10")));
    if (!em.ne(vm.get("v9"), vm.get("v3"))) e.abort(String.format("!((v9=%s) ne (v3=%s))", vm.get("v9"), vm.get("v3")));
  }
  @IM(clazz="java.util.LinkedList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.first.prev","this.modCount"},definedOutput={"this.first.prev.item","this.size","this.first.prev.next","this.first.prev.prev.next","this.first.prev","this.modCount","this.first.prev.prev"},delta={"this.first.prev.item","this.first.prev.next","this.first.prev.prev.next","this.first.prev.prev"})
  @DG(pred={"remove3991","add382","remove3992","add301","add388","removeLastOccurrence6298","clear2398","linkFirst3199","add198","offerFirst5898","add333","push1499","add396","addFirst2599","push1495","offerLast4798","linkBefore4697","offerFirst5899","offer6098","addFirst2598","linkFirst3196","linkLast4397","removeLastOccurrence6267","addLast3098"})
  static void add326(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "first"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("v1"), "prev"));
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", (int)1);
    vm.put("v5", om.getField(vm.get("this"), "size"));
    vm.put("v6", null);
    vm.put("v7", (int)0);
    vm.put("v8", vm.get("v5"));
    vm.put("v9", em.op("isub").eval(vm.get("v8"),vm.get("v4")));
    om.revertField(vm.get("this"), "size", vm.get("v9"));
    vm.put("v10", em.op("ishr").eval(vm.get("v9"),vm.get("v4")));
    vm.put("v11", vm.get("v2"));
    vm.put("v12", om.popField(vm.get("v11"), "next"));
    vm.put("v13", om.popField(vm.get("v11"), "prev"));
    vm.put("v14", vm.get("v13"));
    om.revertField(vm.get("v1"), "prev", vm.get("v14"));
    vm.put("v15", om.popField(vm.get("v14"), "next"));
    vm.put("v16", om.popField(vm.get("v11"), "item"));
    vm.put("param1", vm.get("v16"));
    if (!em.eq(vm.get("v1"), (vm.get("v12")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v12")));
    if (!em.eq(vm.get("v11"), (vm.get("v15")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v15")));
    vm.put("v17", vm.get("v3"));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v4")));
    om.revertField(vm.get("this"), "modCount", vm.get("v18"));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.ne(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) ne (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.ne(vm.get("v14"), vm.get("v6"))) e.abort(String.format("!((v14=%s) ne (v6=%s))", vm.get("v14"), vm.get("v6")));
    if (!em.ge(vm.get("v7"), vm.get("param0"))) e.abort(String.format("!((v7=%s) ge (param0=%s))", vm.get("v7"), vm.get("param0")));
    if (!em.le(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) le (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.ge(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) ge (v7=%s))", vm.get("param0"), vm.get("v7")));
    if (!em.lt(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) lt (v10=%s))", vm.get("param0"), vm.get("v10")));
  }
  @IM(clazz="java.util.LinkedList", name="offerLast", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.last","this.modCount"},definedOutput={"this.size","this.last.next","this.last.item","this.last","this.last.prev","this.modCount","this.last.prev.next"},delta={"this.last.next","this.last.item","this.last.prev","this.last.prev.next"})
  @DG(succ={"remove3991","add382","add333","add379","set5192","remove3992","set5182","add301","add389","set5148","removeLastOccurrence6298","removeLastOccurrence6267"})
  static void offerLast4799(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", (int)1);
    vm.put("v2", null);
    vm.put("v3", om.getField(vm.get("this"), "last"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.popField(vm.get("v4"), "prev"));
    vm.put("v6", vm.get("v5"));
    om.revertField(vm.get("this"), "last", vm.get("v6"));
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", om.popField(vm.get("v4"), "item"));
    vm.put("param0", vm.get("v8"));
    vm.put("v9", om.popField(vm.get("v4"), "next"));
    vm.put("v10", om.popField(vm.get("v6"), "next"));
    if (!em.eq(vm.get("v4"), (vm.get("v10")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v10")));
    vm.put("v11", vm.get("v0"));
    vm.put("v12", vm.get("v7"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    if (!em.eq(vm.get("v2"), (vm.get("v9")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v9")));
    vm.put("v14", em.op("isub").eval(vm.get("v11"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v14"));
    if (!em.ne(vm.get("v6"), vm.get("v2"))) e.abort(String.format("!((v6=%s) ne (v2=%s))", vm.get("v6"), vm.get("v2")));
  }
  @IM(clazz="java.util.LinkedList", name="set", desc="(ILjava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.last.prev.item"},definedOutput={"this.last.prev.item"})
  @DG(pred={"removeFirstOccurrence5596","add199","add382","add399","add388","offerLast4799","remove6393","clear2398","linkFirst3199","add198","offerFirst5898","offer6099","linkLast4399","removeFirstOccurrence5525","push1499","addFirst2599","offerLast4798","offer6098","add389","remove3993","linkLast4397","addLast3098","addLast3099"})
  static void set5182(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "last"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v1"), "prev"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", (int)1);
    vm.put("v7", (int)-1);
    vm.put("v8", (int)0);
    vm.put("v9", em.op("ishr").eval(vm.get("v3"),vm.get("v6")));
    vm.put("v10", em.op("isub").eval(vm.get("v3"),vm.get("v6")));
    vm.put("v11", em.op("iinc").eval(vm.get("v10"),vm.get("v7")));
    vm.put("v12", om.getField(vm.get("v5"), "item"));
    vm.put("param1", vm.get("v12"));
    vm.put("param0", om.newDefaultValue("int"));
    om.revertField(vm.get("v5"), "item");
    vm.put("v13", om.getField(vm.get("v5"), "item"));
    if (!em.lt(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) lt (v3=%s))", vm.get("param0"), vm.get("v3")));
    if (!em.gt(vm.get("v10"), vm.get("param0"))) e.abort(String.format("!((v10=%s) gt (param0=%s))", vm.get("v10"), vm.get("param0")));
    if (!em.le(vm.get("v11"), vm.get("param0"))) e.abort(String.format("!((v11=%s) le (param0=%s))", vm.get("v11"), vm.get("param0")));
    if (!em.ge(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) ge (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.ge(vm.get("param0"), vm.get("v8"))) e.abort(String.format("!((param0=%s) ge (v8=%s))", vm.get("param0"), vm.get("v8")));
  }
  @IM(clazz="java.util.LinkedList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.last.prev.prev","this.modCount"},definedOutput={"this.size","this.last.prev.prev.item","this.last.prev.prev.prev","this.last.prev.prev.next","this.last.prev.prev","this.modCount","this.last.prev.prev.prev.next"},delta={"this.last.prev.prev.item","this.last.prev.prev.prev","this.last.prev.prev.next","this.last.prev.prev.prev.next"})
  @DG(pred={"removeFirstOccurrence5596","add199","add382","add399","add388","offerLast4799","remove6393","clear2398","linkFirst3199","add198","offerFirst5898","offer6099","linkLast4399","removeFirstOccurrence5525","push1499","addFirst2599","offerLast4798","offer6098","add389","remove3993","linkLast4397","addLast3098","addLast3099"},succ={"remove3992","add333","set5148"})
  static void add379(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "last"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("v1"), "prev"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v5"), "prev"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.popField(vm.get("v7"), "prev"));
    vm.put("v9", vm.get("v8"));
    om.revertField(vm.get("v5"), "prev", vm.get("v9"));
    vm.put("v10", om.popField(vm.get("v7"), "next"));
    vm.put("v11", om.getField(vm.get("this"), "modCount"));
    vm.put("v12", om.popField(vm.get("v7"), "item"));
    vm.put("v13", null);
    vm.put("v14", (int)-1);
    vm.put("v15", (int)0);
    vm.put("v16", om.popField(vm.get("v9"), "next"));
    vm.put("param1", vm.get("v12"));
    vm.put("v17", vm.get("v11"));
    vm.put("v18", vm.get("v2"));
    if (!em.eq(vm.get("v7"), (vm.get("v16")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v16")));
    vm.put("v19", em.op("isub").eval(vm.get("v17"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    if (!em.eq(vm.get("v5"), (vm.get("v10")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v10")));
    vm.put("v20", em.op("isub").eval(vm.get("v18"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v20"));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v3")));
    vm.put("v22", em.op("iinc").eval(vm.get("v21"),vm.get("v14")));
    vm.put("v23", em.op("ishr").eval(vm.get("v20"),vm.get("v3")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.ge(vm.get("param0"), vm.get("v23"))) e.abort(String.format("!((param0=%s) ge (v23=%s))", vm.get("param0"), vm.get("v23")));
    if (!em.ge(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) ge (v15=%s))", vm.get("param0"), vm.get("v15")));
    if (!em.ne(vm.get("v9"), vm.get("v13"))) e.abort(String.format("!((v9=%s) ne (v13=%s))", vm.get("v9"), vm.get("v13")));
    if (!em.ne(vm.get("param0"), vm.get("v20"))) e.abort(String.format("!((param0=%s) ne (v20=%s))", vm.get("param0"), vm.get("v20")));
    if (!em.gt(vm.get("v21"), vm.get("param0"))) e.abort(String.format("!((v21=%s) gt (param0=%s))", vm.get("v21"), vm.get("param0")));
    if (!em.le(vm.get("param0"), vm.get("v20"))) e.abort(String.format("!((param0=%s) le (v20=%s))", vm.get("param0"), vm.get("v20")));
    if (!em.le(vm.get("v22"), vm.get("param0"))) e.abort(String.format("!((v22=%s) le (param0=%s))", vm.get("v22"), vm.get("param0")));
  }
  @IM(clazz="java.util.LinkedList", name="push", desc="(Ljava/lang/Object;)V",revertedInput={"this.size","this.first","this.modCount"},definedOutput={"this.size","this.first","this.first.next.prev","this.first.next","this.first.prev","this.modCount","this.first.item"},delta={"this.first.next.prev","this.first.next","this.first.prev","this.first.item"})
  @DG(succ={"removeFirstOccurrence5596","set5141","set5107","set5189","set5183","remove6393","set5194","removeFirstOccurrence5525","add326","set5199","add374","remove3993","set5127"})
  static void push1495(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "first"));
    vm.put("v1", null);
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", em.op("isub").eval(vm.get("v5"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v6"));
    vm.put("v7", vm.get("v2"));
    vm.put("v8", em.op("isub").eval(vm.get("v7"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v8"));
    vm.put("v9", vm.get("v0"));
    vm.put("v10", om.popField(vm.get("v9"), "item"));
    vm.put("param0", vm.get("v10"));
    vm.put("v11", om.popField(vm.get("v9"), "prev"));
    vm.put("v12", om.popField(vm.get("v9"), "next"));
    vm.put("v13", vm.get("v12"));
    om.revertField(vm.get("this"), "first", vm.get("v13"));
    vm.put("v14", om.popField(vm.get("v13"), "prev"));
    if (!em.eq(vm.get("v1"), (vm.get("v11")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v11")));
    if (!em.eq(vm.get("v9"), (vm.get("v14")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v14")));
    if (!em.ne(vm.get("v13"), vm.get("v1"))) e.abort(String.format("!((v13=%s) ne (v1=%s))", vm.get("v13"), vm.get("v1")));
  }
  @IM(clazz="java.util.LinkedList", name="addFirst", desc="(Ljava/lang/Object;)V",revertedInput={"this.size","this.first","this.modCount"},definedOutput={"this.size","this.last.next","this.first","this.last.item","this.last","this.last.prev","this.modCount"},delta={"this.last.next","this.last.item","this.last","this.last.prev"})
  @DG(succ={"removeFirstOccurrence5596","add382","set5107","set5189","remove3992","set5183","add301","set5148","removeLastOccurrence6298","set5194","removeFirstOccurrence5525","add333","add326","add374","add389","remove3993","set5141","remove3991","remove6393","add379","set5199","set5192","set5182","removeLastOccurrence6267","set5127"})
  static void addFirst2599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.getField(vm.get("this"), "modCount"));
    vm.put("v2", (int)1);
    vm.put("v3", om.popField(vm.get("this"), "last"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.popField(vm.get("v4"), "item"));
    vm.put("v6", om.getField(vm.get("this"), "first"));
    vm.put("v7", om.getField(vm.get("this"), "size"));
    vm.put("v8", om.popField(vm.get("v4"), "next"));
    vm.put("v9", vm.get("v8"));
    om.revertField(vm.get("this"), "first", vm.get("v9"));
    vm.put("v10", om.popField(vm.get("v4"), "prev"));
    vm.put("param0", vm.get("v5"));
    vm.put("v11", vm.get("v7"));
    if (!em.eq(vm.get("v0"), (vm.get("v10")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v10")));
    if (!em.eq(vm.get("v4"), (vm.get("v6")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v6")));
    vm.put("v12", vm.get("v1"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v11"),vm.get("v2")));
    om.revertField(vm.get("this"), "size", vm.get("v14"));
    if (!em.eq(vm.get("v9"), vm.get("v0"))) e.abort(String.format("!((v9=%s) eq (v0=%s))", vm.get("v9"), vm.get("v0")));
  }
  @IM(clazz="java.util.LinkedList", name="set", desc="(ILjava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.first.next.next.next.next.item"},definedOutput={"this.first.next.next.next.next.item"})
  @DG(pred={"remove3991","add382","remove3992","add301","add388","removeLastOccurrence6298","clear2398","linkFirst3199","add198","offerFirst5898","add333","push1499","add396","addFirst2599","push1495","offerLast4798","linkBefore4697","offerFirst5899","offer6098","addFirst2598","linkFirst3196","linkLast4397","removeLastOccurrence6267","addLast3098"})
  static void set5127(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "first"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", (int)4);
    vm.put("v5", (int)3);
    vm.put("v6", (int)2);
    vm.put("v7", (int)1);
    vm.put("v8", (int)0);
    vm.put("v9", om.getField(vm.get("v3"), "next"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", em.op("ishr").eval(vm.get("v1"),vm.get("v7")));
    vm.put("v12", om.getField(vm.get("v10"), "next"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("v13"), "next"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.getField(vm.get("v15"), "next"));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.getField(vm.get("v17"), "item"));
    vm.put("param1", vm.get("v18"));
    om.revertField(vm.get("v17"), "item");
    vm.put("v19", om.getField(vm.get("v17"), "item"));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.ge(vm.get("v4"), vm.get("param0"))) e.abort(String.format("!((v4=%s) ge (param0=%s))", vm.get("v4"), vm.get("param0")));
    if (!em.lt(vm.get("v7"), vm.get("param0"))) e.abort(String.format("!((v7=%s) lt (param0=%s))", vm.get("v7"), vm.get("param0")));
    if (!em.ge(vm.get("param0"), vm.get("v8"))) e.abort(String.format("!((param0=%s) ge (v8=%s))", vm.get("param0"), vm.get("v8")));
    if (!em.lt(vm.get("param0"), vm.get("v11"))) e.abort(String.format("!((param0=%s) lt (v11=%s))", vm.get("param0"), vm.get("v11")));
    if (!em.lt(vm.get("v8"), vm.get("param0"))) e.abort(String.format("!((v8=%s) lt (param0=%s))", vm.get("v8"), vm.get("param0")));
    if (!em.lt(vm.get("v5"), vm.get("param0"))) e.abort(String.format("!((v5=%s) lt (param0=%s))", vm.get("v5"), vm.get("param0")));
    if (!em.lt(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) lt (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.lt(vm.get("v6"), vm.get("param0"))) e.abort(String.format("!((v6=%s) lt (param0=%s))", vm.get("v6"), vm.get("param0")));
  }
  @IM(clazz="java.util.LinkedList", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.size","this.last.next","this.last.item","this.modCount"},definedOutput={"this.size","this.last.next","this.first","this.last.item","this.modCount","this.first.prev"},delta={"this.first","this.first.prev"})
  @DG(pred={"removeFirstOccurrence5596","add199","add399","add388","offerLast4799","remove6393","clear2398","linkFirst3199","add198","offerFirst5898","offer6099","linkLast4399","removeFirstOccurrence5525","push1499","addFirst2599","offerLast4798","offer6098","remove3993","linkLast4397","addLast3098","addLast3099"},succ={"removeFirstOccurrence5596","set5141","set5107","set5189","set5183","remove6393","set5194","removeFirstOccurrence5525","add326","set5199","add374","remove3993","set5127"})
  static void remove3991(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", null);
    vm.put("v3", om.getField(vm.get("this"), "last"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.popField(vm.get("this"), "first"));
    vm.put("v6", vm.get("v5"));
    om.revertField(vm.get("v4"), "next", vm.get("v6"));
    vm.put("v7", om.getField(vm.get("this"), "size"));
    vm.put("v8", om.popField(vm.get("v6"), "prev"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", om.getField(vm.get("v4"), "prev"));
    vm.put("v12", om.getField(vm.get("v4"), "next"));
    vm.put("v13", om.getField(vm.get("v4"), "item"));
    vm.put("v14", vm.get("v7"));
    if (!em.eq(vm.get("v9"), (vm.get("v11")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v11")));
    vm.put("v15", em.op("iadd").eval(vm.get("v14"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v15"));
    vm.put("v16", em.op("ishr").eval(vm.get("v15"),vm.get("v1")));
    vm.put("v17", em.op("isub").eval(vm.get("v15"),vm.get("v1")));
    if (!em.eq(vm.get("v2"), (vm.get("v12")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v12")));
    vm.put("v18", vm.get("v0"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    if (!em.eq(vm.get("v2"), (vm.get("v13")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v13")));
    vm.put("param0", om.newDefaultValue("int"));
    om.revertField(vm.get("v4"), "item");
    vm.put("v20", om.getField(vm.get("v4"), "item"));
    if (!em.ge(vm.get("param0"), vm.get("v16"))) e.abort(String.format("!((param0=%s) ge (v16=%s))", vm.get("param0"), vm.get("v16")));
    if (!em.lt(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) lt (v15=%s))", vm.get("param0"), vm.get("v15")));
    if (!em.le(vm.get("v17"), vm.get("param0"))) e.abort(String.format("!((v17=%s) le (param0=%s))", vm.get("v17"), vm.get("param0")));
    if (!em.ne(vm.get("v6"), vm.get("v2"))) e.abort(String.format("!((v6=%s) ne (v2=%s))", vm.get("v6"), vm.get("v2")));
    if (!em.ge(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ge (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.eq(vm.get("v9"), vm.get("v2"))) e.abort(String.format("!((v9=%s) eq (v2=%s))", vm.get("v9"), vm.get("v2")));
  }
  @IM(clazz="java.util.LinkedList", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.first.prev","this.modCount","this.first.item"},definedOutput={"this.size","this.last.next","this.last","this.first.prev","this.modCount","this.first.item"},delta={"this.last.next","this.last"})
  @DG(pred={"remove3991","add382","remove3992","add301","add388","removeLastOccurrence6298","clear2398","linkFirst3199","add198","offerFirst5898","add333","push1499","add396","addFirst2599","push1495","offerLast4798","linkBefore4697","offerFirst5899","offer6098","addFirst2598","linkFirst3196","linkLast4397","removeLastOccurrence6267","addLast3098"},succ={"remove3991","add382","add333","add379","set5192","remove3992","set5182","add301","add389","set5148","removeLastOccurrence6298","removeLastOccurrence6267"})
  static void remove6393(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.getField(vm.get("this"), "size"));
    vm.put("v2", (int)1);
    vm.put("v3", om.popField(vm.get("this"), "last"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "first"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("v6"), "next"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("v6"), "prev"));
    vm.put("v10", om.getField(vm.get("v6"), "item"));
    if (!em.eq(vm.get("v0"), (vm.get("v9")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v9")));
    if (!em.eq(vm.get("v0"), (vm.get("v10")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v10")));
    vm.put("v11", vm.get("v4"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", vm.get("v1"));
    vm.put("v14", em.op("iadd").eval(vm.get("v13"),vm.get("v2")));
    om.revertField(vm.get("this"), "size", vm.get("v14"));
    vm.put("v15", vm.get("v3"));
    om.revertField(vm.get("v6"), "prev", vm.get("v15"));
    vm.put("v16", om.popField(vm.get("v15"), "next"));
    if (!em.eq(vm.get("v8"), (vm.get("v16")))) e.abort("Inconsistent value for \"v8\": " + vm.get("v8") + " ne " + (vm.get("v16")));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    om.revertField(vm.get("v6"), "item");
    vm.put("v17", om.getField(vm.get("v6"), "item"));
    if (!em.eq(vm.get("v8"), vm.get("v0"))) e.abort(String.format("!((v8=%s) eq (v0=%s))", vm.get("v8"), vm.get("v0")));
    if (!em.ne(vm.get("v15"), vm.get("v0"))) e.abort(String.format("!((v15=%s) ne (v0=%s))", vm.get("v15"), vm.get("v0")));
    if (!em.eq(vm.get("v17"), vm.get("v0"))) e.abort(String.format("!((v17=%s) eq (v0=%s))", vm.get("v17"), vm.get("v0")));
    if (!em.eq(vm.get("param0"), vm.get("v0"))) e.abort(String.format("!((param0=%s) eq (v0=%s))", vm.get("param0"), vm.get("v0")));
    if (!em.ne(vm.get("v6"), vm.get("v0"))) e.abort(String.format("!((v6=%s) ne (v0=%s))", vm.get("v6"), vm.get("v0")));
  }
  @IM(clazz="java.util.LinkedList", name="addLast", desc="(Ljava/lang/Object;)V",revertedInput={"this.size","this.last","this.modCount"},definedOutput={"this.size","this.last.next","this.first","this.last.item","this.last","this.last.prev","this.modCount"},delta={"this.last.next","this.first","this.last.item","this.last.prev"})
  @DG(succ={"removeFirstOccurrence5596","add382","set5107","set5189","remove3992","set5183","add301","set5148","removeLastOccurrence6298","set5194","removeFirstOccurrence5525","add333","add326","add374","add389","remove3993","set5141","remove3991","remove6393","add379","set5199","set5192","set5182","removeLastOccurrence6267","set5127"})
  static void addLast3098(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", om.getField(vm.get("this"), "last"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.popField(vm.get("v4"), "next"));
    vm.put("v6", null);
    vm.put("v7", om.popField(vm.get("v4"), "prev"));
    vm.put("v8", vm.get("v7"));
    om.revertField(vm.get("this"), "last", vm.get("v8"));
    vm.put("v9", om.popField(vm.get("this"), "first"));
    vm.put("v10", om.popField(vm.get("v4"), "item"));
    vm.put("param0", vm.get("v10"));
    vm.put("v11", vm.get("v0"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", vm.get("v2"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v14"));
    if (!em.eq(vm.get("v4"), (vm.get("v9")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v9")));
    if (!em.eq(vm.get("v6"), (vm.get("v5")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v5")));
    if (!em.eq(vm.get("v8"), vm.get("v6"))) e.abort(String.format("!((v8=%s) eq (v6=%s))", vm.get("v8"), vm.get("v6")));
  }
  @IM(clazz="java.util.LinkedList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.last.prev","this.modCount"},definedOutput={"this.size","this.first","this.last.prev","this.last.prev.prev","this.modCount","this.last.prev.item","this.last.prev.next"},delta={"this.first","this.last.prev.prev","this.last.prev.item","this.last.prev.next"})
  @DG(pred={"removeFirstOccurrence5596","add199","add399","add388","offerLast4799","remove6393","clear2398","linkFirst3199","add198","offerFirst5898","offer6099","linkLast4399","removeFirstOccurrence5525","push1499","addFirst2599","offerLast4798","offer6098","remove3993","linkLast4397","addLast3098","addLast3099"},succ={"removeFirstOccurrence5596","set5141","set5107","set5189","remove3992","set5183","add301","set5148","remove6393","set5194","removeFirstOccurrence5525","add333","add379","add326","set5199","add374","set5182","remove3993","removeLastOccurrence6267","set5127"})
  static void add382(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "first"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.popField(vm.get("v1"), "next"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getField(vm.get("this"), "last"));
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", om.popField(vm.get("v1"), "item"));
    vm.put("v9", null);
    vm.put("v10", (int)0);
    vm.put("v11", om.popField(vm.get("v1"), "prev"));
    vm.put("v12", vm.get("v11"));
    om.revertField(vm.get("v3"), "prev", vm.get("v12"));
    vm.put("v13", om.getField(vm.get("v3"), "prev"));
    vm.put("v14", vm.get("v4"));
    vm.put("v15", vm.get("v7"));
    if (!em.eq(vm.get("v3"), (vm.get("v6")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v6")));
    vm.put("param1", vm.get("v8"));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v5")));
    om.revertField(vm.get("this"), "modCount", vm.get("v16"));
    vm.put("v17", em.op("isub").eval(vm.get("v14"),vm.get("v5")));
    om.revertField(vm.get("this"), "size", vm.get("v17"));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v5")));
    vm.put("v19", em.op("ishr").eval(vm.get("v17"),vm.get("v5")));
    if (!em.eq(vm.get("v1"), (vm.get("v13")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v13")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.ne(vm.get("param0"), vm.get("v17"))) e.abort(String.format("!((param0=%s) ne (v17=%s))", vm.get("param0"), vm.get("v17")));
    if (!em.ge(vm.get("param0"), vm.get("v19"))) e.abort(String.format("!((param0=%s) ge (v19=%s))", vm.get("param0"), vm.get("v19")));
    if (!em.le(vm.get("param0"), vm.get("v17"))) e.abort(String.format("!((param0=%s) le (v17=%s))", vm.get("param0"), vm.get("v17")));
    if (!em.eq(vm.get("v12"), vm.get("v9"))) e.abort(String.format("!((v12=%s) eq (v9=%s))", vm.get("v12"), vm.get("v9")));
    if (!em.ge(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ge (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.le(vm.get("v18"), vm.get("param0"))) e.abort(String.format("!((v18=%s) le (param0=%s))", vm.get("v18"), vm.get("param0")));
  }
  @IM(clazz="java.util.LinkedList", name="set", desc="(ILjava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.first.next.next.next.item"},definedOutput={"this.first.next.next.next.item"})
  @DG(pred={"remove3991","add382","remove3992","add301","add388","removeLastOccurrence6298","clear2398","linkFirst3199","add198","offerFirst5898","add333","push1499","add396","addFirst2599","push1495","offerLast4798","linkBefore4697","offerFirst5899","offer6098","addFirst2598","linkFirst3196","linkLast4397","removeLastOccurrence6267","addLast3098"})
  static void set5141(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "first"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("v1"), "next"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v3"), "next"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)3);
    vm.put("v9", (int)2);
    vm.put("v10", (int)1);
    vm.put("v11", (int)0);
    vm.put("v12", em.op("ishr").eval(vm.get("v7"),vm.get("v10")));
    vm.put("v13", om.getField(vm.get("v5"), "next"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.getField(vm.get("v14"), "item"));
    vm.put("param1", vm.get("v15"));
    om.revertField(vm.get("v14"), "item");
    vm.put("v16", om.getField(vm.get("v14"), "item"));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.lt(vm.get("v10"), vm.get("param0"))) e.abort(String.format("!((v10=%s) lt (param0=%s))", vm.get("v10"), vm.get("param0")));
    if (!em.lt(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) lt (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.lt(vm.get("v9"), vm.get("param0"))) e.abort(String.format("!((v9=%s) lt (param0=%s))", vm.get("v9"), vm.get("param0")));
    if (!em.ge(vm.get("v8"), vm.get("param0"))) e.abort(String.format("!((v8=%s) ge (param0=%s))", vm.get("v8"), vm.get("param0")));
    if (!em.lt(vm.get("v11"), vm.get("param0"))) e.abort(String.format("!((v11=%s) lt (param0=%s))", vm.get("v11"), vm.get("param0")));
    if (!em.lt(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) lt (v7=%s))", vm.get("param0"), vm.get("v7")));
    if (!em.ge(vm.get("param0"), vm.get("v11"))) e.abort(String.format("!((param0=%s) ge (v11=%s))", vm.get("param0"), vm.get("v11")));
  }
  @IM(clazz="java.util.LinkedList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.modCount","this.last.prev.prev"},definedOutput={"this.size","this.last.prev.prev.item","this.first","this.last.prev.prev.prev","this.last.prev.prev.next","this.modCount","this.last.prev.prev"},delta={"this.last.prev.prev.item","this.first","this.last.prev.prev.prev","this.last.prev.prev.next"})
  @DG(pred={"removeFirstOccurrence5596","add199","add382","add399","add388","offerLast4799","remove6393","clear2398","linkFirst3199","add198","offerFirst5898","offer6099","linkLast4399","removeFirstOccurrence5525","push1499","addFirst2599","offerLast4798","offer6098","add389","remove3993","linkLast4397","addLast3098","addLast3099"},succ={"removeFirstOccurrence5596","set5141","set5107","set5189","remove3992","set5183","set5148","remove6393","set5194","removeFirstOccurrence5525","add333","add326","set5199","add374","remove3993","set5127"})
  static void add301(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "first"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "last"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", om.popField(vm.get("v1"), "item"));
    vm.put("v8", om.popField(vm.get("v1"), "prev"));
    vm.put("v9", om.getField(vm.get("v5"), "prev"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getField(vm.get("v10"), "prev"));
    vm.put("v12", null);
    vm.put("v13", (int)-1);
    vm.put("v14", (int)0);
    vm.put("v15", om.popField(vm.get("v1"), "next"));
    vm.put("v16", vm.get("v6"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("v18", vm.get("v2"));
    if (!em.eq(vm.get("v1"), (vm.get("v11")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v11")));
    vm.put("v19", vm.get("v8"));
    om.revertField(vm.get("v10"), "prev", vm.get("v19"));
    vm.put("param1", vm.get("v7"));
    if (!em.eq(vm.get("v10"), (vm.get("v15")))) e.abort("Inconsistent value for \"v10\": " + vm.get("v10") + " ne " + (vm.get("v15")));
    vm.put("v20", em.op("isub").eval(vm.get("v18"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v20"));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v3")));
    vm.put("v22", em.op("iinc").eval(vm.get("v21"),vm.get("v13")));
    vm.put("v23", em.op("ishr").eval(vm.get("v20"),vm.get("v3")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.gt(vm.get("v21"), vm.get("param0"))) e.abort(String.format("!((v21=%s) gt (param0=%s))", vm.get("v21"), vm.get("param0")));
    if (!em.ge(vm.get("param0"), vm.get("v23"))) e.abort(String.format("!((param0=%s) ge (v23=%s))", vm.get("param0"), vm.get("v23")));
    if (!em.ne(vm.get("param0"), vm.get("v20"))) e.abort(String.format("!((param0=%s) ne (v20=%s))", vm.get("param0"), vm.get("v20")));
    if (!em.le(vm.get("v22"), vm.get("param0"))) e.abort(String.format("!((v22=%s) le (param0=%s))", vm.get("v22"), vm.get("param0")));
    if (!em.ge(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) ge (v14=%s))", vm.get("param0"), vm.get("v14")));
    if (!em.eq(vm.get("v19"), vm.get("v12"))) e.abort(String.format("!((v19=%s) eq (v12=%s))", vm.get("v19"), vm.get("v12")));
    if (!em.le(vm.get("param0"), vm.get("v20"))) e.abort(String.format("!((param0=%s) le (v20=%s))", vm.get("param0"), vm.get("v20")));
  }
  @IM(clazz="java.util.LinkedList", name="offerFirst", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.first","this.modCount"},definedOutput={"this.size","this.first","this.first.next.prev","this.first.next","this.modCount","this.first.prev","this.first.item"},delta={"this.first.next.prev","this.first.next","this.first.prev","this.first.item"})
  @DG(succ={"removeFirstOccurrence5596","set5141","set5107","set5189","set5183","remove6393","set5194","removeFirstOccurrence5525","add326","set5199","add374","remove3993","set5127"})
  static void offerFirst5899(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "first"));
    vm.put("v1", om.getField(vm.get("this"), "modCount"));
    vm.put("v2", (int)1);
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", vm.get("v0"));
    vm.put("v6", om.popField(vm.get("v5"), "prev"));
    vm.put("v7", om.popField(vm.get("v5"), "item"));
    vm.put("param0", vm.get("v7"));
    vm.put("v8", om.popField(vm.get("v5"), "next"));
    vm.put("v9", vm.get("v8"));
    om.revertField(vm.get("this"), "first", vm.get("v9"));
    vm.put("v10", om.popField(vm.get("v9"), "prev"));
    if (!em.eq(vm.get("v3"), (vm.get("v6")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v6")));
    vm.put("v11", vm.get("v1"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", vm.get("v4"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v2")));
    om.revertField(vm.get("this"), "size", vm.get("v14"));
    if (!em.eq(vm.get("v5"), (vm.get("v10")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v10")));
    if (!em.ne(vm.get("v9"), vm.get("v3"))) e.abort(String.format("!((v9=%s) ne (v3=%s))", vm.get("v9"), vm.get("v3")));
  }
  @IM(clazz="java.util.LinkedList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.modCount","this.last.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev"},definedOutput={"this.size","this.first","this.last.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.next","this.modCount","this.last.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev","this.last.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.item","this.last.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev"},delta={"this.first","this.last.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.next","this.last.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev","this.last.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.prev.item"})
  @DG(pred={"removeFirstOccurrence5596","add382","add301","add399","clear2398","linkFirst3199","add198","linkLast4399","removeFirstOccurrence5525","push1499","offerLast4798","offer6098","add389","remove3993","add199","add388","offerLast4799","remove6393","offerFirst5898","offer6099","add379","addFirst2599","linkLast4397","addLast3098","addLast3099"},succ={"removeFirstOccurrence5596","set5141","set5107","set5189","set5183","remove6393","set5194","removeFirstOccurrence5525","add326","set5199","add374","remove3993","set5127"})
  static void add333(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "last"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v3"), "prev"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popField(vm.get("this"), "first"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", om.popField(vm.get("v7"), "item"));
    vm.put("v10", om.getField(vm.get("v5"), "prev"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", null);
    vm.put("v13", (int)-1);
    vm.put("v14", (int)0);
    vm.put("v15", om.popField(vm.get("v7"), "prev"));
    vm.put("v16", om.getField(vm.get("v11"), "prev"));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.getField(vm.get("v17"), "prev"));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", om.popField(vm.get("v7"), "next"));
    vm.put("v21", om.getField(vm.get("v19"), "prev"));
    vm.put("v22", vm.get("v21"));
    vm.put("v23", om.getField(vm.get("v22"), "prev"));
    vm.put("v24", vm.get("v23"));
    vm.put("v25", om.getField(vm.get("v24"), "prev"));
    vm.put("v26", vm.get("v25"));
    vm.put("v27", om.getField(vm.get("v26"), "prev"));
    vm.put("v28", vm.get("v27"));
    vm.put("v29", om.getField(vm.get("v28"), "prev"));
    vm.put("v30", vm.get("v29"));
    vm.put("v31", om.getField(vm.get("v30"), "prev"));
    vm.put("v32", vm.get("v31"));
    vm.put("v33", om.getField(vm.get("v32"), "prev"));
    vm.put("v34", vm.get("v15"));
    om.revertField(vm.get("v32"), "prev", vm.get("v34"));
    vm.put("param1", vm.get("v9"));
    vm.put("v35", vm.get("v0"));
    vm.put("v36", em.op("isub").eval(vm.get("v35"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v36"));
    vm.put("v37", em.op("isub").eval(vm.get("v36"),vm.get("v1")));
    vm.put("v38", em.op("ishr").eval(vm.get("v36"),vm.get("v1")));
    vm.put("v39", em.op("iinc").eval(vm.get("v37"),vm.get("v13")));
    vm.put("v40", em.op("iinc").eval(vm.get("v39"),vm.get("v13")));
    vm.put("v41", em.op("iinc").eval(vm.get("v40"),vm.get("v13")));
    vm.put("v42", em.op("iinc").eval(vm.get("v41"),vm.get("v13")));
    vm.put("v43", em.op("iinc").eval(vm.get("v42"),vm.get("v13")));
    vm.put("v44", em.op("iinc").eval(vm.get("v43"),vm.get("v13")));
    vm.put("v45", em.op("iinc").eval(vm.get("v44"),vm.get("v13")));
    vm.put("v46", em.op("iinc").eval(vm.get("v45"),vm.get("v13")));
    vm.put("v47", em.op("iinc").eval(vm.get("v46"),vm.get("v13")));
    vm.put("v48", em.op("iinc").eval(vm.get("v47"),vm.get("v13")));
    if (!em.eq(vm.get("v7"), (vm.get("v33")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v33")));
    if (!em.eq(vm.get("v32"), (vm.get("v20")))) e.abort("Inconsistent value for \"v32\": " + vm.get("v32") + " ne " + (vm.get("v20")));
    vm.put("v49", vm.get("v8"));
    vm.put("v50", em.op("isub").eval(vm.get("v49"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v50"));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.ge(vm.get("param0"), vm.get("v38"))) e.abort(String.format("!((param0=%s) ge (v38=%s))", vm.get("param0"), vm.get("v38")));
    if (!em.gt(vm.get("v41"), vm.get("param0"))) e.abort(String.format("!((v41=%s) gt (param0=%s))", vm.get("v41"), vm.get("param0")));
    if (!em.gt(vm.get("v45"), vm.get("param0"))) e.abort(String.format("!((v45=%s) gt (param0=%s))", vm.get("v45"), vm.get("param0")));
    if (!em.gt(vm.get("v37"), vm.get("param0"))) e.abort(String.format("!((v37=%s) gt (param0=%s))", vm.get("v37"), vm.get("param0")));
    if (!em.gt(vm.get("v47"), vm.get("param0"))) e.abort(String.format("!((v47=%s) gt (param0=%s))", vm.get("v47"), vm.get("param0")));
    if (!em.le(vm.get("param0"), vm.get("v36"))) e.abort(String.format("!((param0=%s) le (v36=%s))", vm.get("param0"), vm.get("v36")));
    if (!em.ne(vm.get("param0"), vm.get("v36"))) e.abort(String.format("!((param0=%s) ne (v36=%s))", vm.get("param0"), vm.get("v36")));
    if (!em.eq(vm.get("v34"), vm.get("v12"))) e.abort(String.format("!((v34=%s) eq (v12=%s))", vm.get("v34"), vm.get("v12")));
    if (!em.gt(vm.get("v44"), vm.get("param0"))) e.abort(String.format("!((v44=%s) gt (param0=%s))", vm.get("v44"), vm.get("param0")));
    if (!em.gt(vm.get("v40"), vm.get("param0"))) e.abort(String.format("!((v40=%s) gt (param0=%s))", vm.get("v40"), vm.get("param0")));
    if (!em.gt(vm.get("v42"), vm.get("param0"))) e.abort(String.format("!((v42=%s) gt (param0=%s))", vm.get("v42"), vm.get("param0")));
    if (!em.ge(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) ge (v14=%s))", vm.get("param0"), vm.get("v14")));
    if (!em.gt(vm.get("v39"), vm.get("param0"))) e.abort(String.format("!((v39=%s) gt (param0=%s))", vm.get("v39"), vm.get("param0")));
    if (!em.le(vm.get("v48"), vm.get("param0"))) e.abort(String.format("!((v48=%s) le (param0=%s))", vm.get("v48"), vm.get("param0")));
    if (!em.gt(vm.get("v43"), vm.get("param0"))) e.abort(String.format("!((v43=%s) gt (param0=%s))", vm.get("v43"), vm.get("param0")));
    if (!em.gt(vm.get("v46"), vm.get("param0"))) e.abort(String.format("!((v46=%s) gt (param0=%s))", vm.get("v46"), vm.get("param0")));
  }
  @IM(clazz="java.util.LinkedList", name="set", desc="(ILjava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.first.next.item"},definedOutput={"this.first.next.item"})
  @DG(pred={"remove3991","add382","remove3992","add301","add388","removeLastOccurrence6298","clear2398","linkFirst3199","add198","offerFirst5898","add333","push1499","add396","addFirst2599","push1495","offerLast4798","linkBefore4697","offerFirst5899","offer6098","addFirst2598","linkFirst3196","linkLast4397","removeLastOccurrence6267","addLast3098"})
  static void set5199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "first"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", (int)1);
    vm.put("v5", (int)0);
    vm.put("v6", om.getField(vm.get("v3"), "next"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("v7"), "item"));
    vm.put("param1", vm.get("v8"));
    vm.put("v9", em.op("ishr").eval(vm.get("v1"),vm.get("v4")));
    om.revertField(vm.get("v7"), "item");
    vm.put("v10", om.getField(vm.get("v7"), "item"));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.ge(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) ge (v5=%s))", vm.get("param0"), vm.get("v5")));
    if (!em.lt(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) lt (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.lt(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) lt (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.lt(vm.get("v5"), vm.get("param0"))) e.abort(String.format("!((v5=%s) lt (param0=%s))", vm.get("v5"), vm.get("param0")));
    if (!em.ge(vm.get("v4"), vm.get("param0"))) e.abort(String.format("!((v4=%s) ge (param0=%s))", vm.get("v4"), vm.get("param0")));
  }
  @IM(clazz="java.util.LinkedList", name="set", desc="(ILjava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.first.next.next.next.next.next.next.next.next.item"},definedOutput={"this.first.next.next.next.next.next.next.next.next.item"})
  @DG(pred={"remove3991","add382","remove3992","add301","add388","removeLastOccurrence6298","clear2398","linkFirst3199","add198","offerFirst5898","add333","push1499","add396","addFirst2599","push1495","offerLast4798","linkBefore4697","offerFirst5899","offer6098","addFirst2598","linkFirst3196","linkLast4397","removeLastOccurrence6267","addLast3098"})
  static void set5107(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "first"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", (int)8);
    vm.put("v5", (int)7);
    vm.put("v6", (int)6);
    vm.put("v7", (int)5);
    vm.put("v8", (int)4);
    vm.put("v9", (int)3);
    vm.put("v10", (int)2);
    vm.put("v11", (int)1);
    vm.put("v12", (int)0);
    vm.put("v13", em.op("ishr").eval(vm.get("v3"),vm.get("v11")));
    vm.put("v14", om.getField(vm.get("v1"), "next"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.getField(vm.get("v15"), "next"));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.getField(vm.get("v17"), "next"));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", om.getField(vm.get("v19"), "next"));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.getField(vm.get("v21"), "next"));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.getField(vm.get("v23"), "next"));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", om.getField(vm.get("v25"), "next"));
    vm.put("v27", vm.get("v26"));
    vm.put("v28", om.getField(vm.get("v27"), "next"));
    vm.put("v29", vm.get("v28"));
    vm.put("v30", om.getField(vm.get("v29"), "item"));
    vm.put("param1", vm.get("v30"));
    vm.put("param0", om.newDefaultValue("int"));
    om.revertField(vm.get("v29"), "item");
    vm.put("v31", om.getField(vm.get("v29"), "item"));
    if (!em.lt(vm.get("v6"), vm.get("param0"))) e.abort(String.format("!((v6=%s) lt (param0=%s))", vm.get("v6"), vm.get("param0")));
    if (!em.lt(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) lt (v13=%s))", vm.get("param0"), vm.get("v13")));
    if (!em.lt(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) lt (v3=%s))", vm.get("param0"), vm.get("v3")));
    if (!em.lt(vm.get("v7"), vm.get("param0"))) e.abort(String.format("!((v7=%s) lt (param0=%s))", vm.get("v7"), vm.get("param0")));
    if (!em.lt(vm.get("v5"), vm.get("param0"))) e.abort(String.format("!((v5=%s) lt (param0=%s))", vm.get("v5"), vm.get("param0")));
    if (!em.lt(vm.get("v11"), vm.get("param0"))) e.abort(String.format("!((v11=%s) lt (param0=%s))", vm.get("v11"), vm.get("param0")));
    if (!em.ge(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ge (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ge(vm.get("v4"), vm.get("param0"))) e.abort(String.format("!((v4=%s) ge (param0=%s))", vm.get("v4"), vm.get("param0")));
    if (!em.lt(vm.get("v12"), vm.get("param0"))) e.abort(String.format("!((v12=%s) lt (param0=%s))", vm.get("v12"), vm.get("param0")));
    if (!em.lt(vm.get("v9"), vm.get("param0"))) e.abort(String.format("!((v9=%s) lt (param0=%s))", vm.get("v9"), vm.get("param0")));
    if (!em.lt(vm.get("v10"), vm.get("param0"))) e.abort(String.format("!((v10=%s) lt (param0=%s))", vm.get("v10"), vm.get("param0")));
    if (!em.lt(vm.get("v8"), vm.get("param0"))) e.abort(String.format("!((v8=%s) lt (param0=%s))", vm.get("v8"), vm.get("param0")));
  }
  @IM(clazz="java.util.LinkedList", name="set", desc="(ILjava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.last.prev.prev.item"},definedOutput={"this.last.prev.prev.item"})
  @DG(pred={"removeFirstOccurrence5596","add382","add301","add399","clear2398","linkFirst3199","add198","linkLast4399","removeFirstOccurrence5525","push1499","offerLast4798","offer6098","add389","remove3993","add199","add388","offerLast4799","remove6393","offerFirst5898","offer6099","add379","addFirst2599","linkLast4397","addLast3098","addLast3099"})
  static void set5148(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "last"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v1"), "prev"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", (int)1);
    vm.put("v7", (int)-1);
    vm.put("v8", (int)0);
    vm.put("v9", em.op("ishr").eval(vm.get("v3"),vm.get("v6")));
    vm.put("v10", om.getField(vm.get("v5"), "prev"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("v11"), "item"));
    vm.put("param1", vm.get("v12"));
    vm.put("v13", em.op("isub").eval(vm.get("v3"),vm.get("v6")));
    vm.put("v14", em.op("iinc").eval(vm.get("v13"),vm.get("v7")));
    vm.put("v15", em.op("iinc").eval(vm.get("v14"),vm.get("v7")));
    om.revertField(vm.get("v11"), "item");
    vm.put("v16", om.getField(vm.get("v11"), "item"));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.lt(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) lt (v3=%s))", vm.get("param0"), vm.get("v3")));
    if (!em.ge(vm.get("param0"), vm.get("v8"))) e.abort(String.format("!((param0=%s) ge (v8=%s))", vm.get("param0"), vm.get("v8")));
    if (!em.gt(vm.get("v14"), vm.get("param0"))) e.abort(String.format("!((v14=%s) gt (param0=%s))", vm.get("v14"), vm.get("param0")));
    if (!em.ge(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) ge (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.le(vm.get("v15"), vm.get("param0"))) e.abort(String.format("!((v15=%s) le (param0=%s))", vm.get("v15"), vm.get("param0")));
    if (!em.gt(vm.get("v13"), vm.get("param0"))) e.abort(String.format("!((v13=%s) gt (param0=%s))", vm.get("v13"), vm.get("param0")));
  }
  @IM(clazz="java.util.LinkedList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.first.next.next.prev","this.modCount"},definedOutput={"this.size","this.first.next.next.prev.next","this.first.next.next.prev.prev","this.first.next.next.prev","this.modCount","this.first.next.next.prev.item","this.first.next.next.prev.prev.next"},delta={"this.first.next.next.prev.next","this.first.next.next.prev.prev","this.first.next.next.prev.item","this.first.next.next.prev.prev.next"})
  @DG(pred={"remove3991","add382","remove3992","add301","add388","removeLastOccurrence6298","clear2398","linkFirst3199","add198","offerFirst5898","add333","push1499","add396","addFirst2599","push1495","offerLast4798","linkBefore4697","offerFirst5899","offer6098","addFirst2598","linkFirst3196","linkLast4397","removeLastOccurrence6267","addLast3098"})
  static void add374(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", om.getField(vm.get("this"), "first"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", null);
    vm.put("v6", (int)2);
    vm.put("v7", (int)0);
    vm.put("v8", om.getField(vm.get("v4"), "next"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("v9"), "next"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("v11"), "prev"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.popField(vm.get("v13"), "item"));
    vm.put("param1", vm.get("v14"));
    vm.put("v15", om.popField(vm.get("v13"), "next"));
    vm.put("v16", om.popField(vm.get("v13"), "prev"));
    vm.put("v17", vm.get("v16"));
    om.revertField(vm.get("v11"), "prev", vm.get("v17"));
    vm.put("v18", om.popField(vm.get("v17"), "next"));
    if (!em.eq(vm.get("v13"), (vm.get("v18")))) e.abort("Inconsistent value for \"v13\": " + vm.get("v13") + " ne " + (vm.get("v18")));
    vm.put("v19", vm.get("v0"));
    vm.put("v20", em.op("isub").eval(vm.get("v19"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v20"));
    vm.put("v21", em.op("ishr").eval(vm.get("v20"),vm.get("v1")));
    vm.put("v22", vm.get("v2"));
    vm.put("v23", em.op("isub").eval(vm.get("v22"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v23"));
    if (!em.eq(vm.get("v11"), (vm.get("v15")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v15")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.ne(vm.get("v17"), vm.get("v5"))) e.abort(String.format("!((v17=%s) ne (v5=%s))", vm.get("v17"), vm.get("v5")));
    if (!em.lt(vm.get("v7"), vm.get("param0"))) e.abort(String.format("!((v7=%s) lt (param0=%s))", vm.get("v7"), vm.get("param0")));
    if (!em.ge(vm.get("v6"), vm.get("param0"))) e.abort(String.format("!((v6=%s) ge (param0=%s))", vm.get("v6"), vm.get("param0")));
    if (!em.lt(vm.get("v1"), vm.get("param0"))) e.abort(String.format("!((v1=%s) lt (param0=%s))", vm.get("v1"), vm.get("param0")));
    if (!em.ge(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) ge (v7=%s))", vm.get("param0"), vm.get("v7")));
    if (!em.ne(vm.get("param0"), vm.get("v20"))) e.abort(String.format("!((param0=%s) ne (v20=%s))", vm.get("param0"), vm.get("v20")));
    if (!em.le(vm.get("param0"), vm.get("v20"))) e.abort(String.format("!((param0=%s) le (v20=%s))", vm.get("param0"), vm.get("v20")));
    if (!em.lt(vm.get("param0"), vm.get("v21"))) e.abort(String.format("!((param0=%s) lt (v21=%s))", vm.get("param0"), vm.get("v21")));
  }
}
