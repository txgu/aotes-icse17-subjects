import org.javelus.aotes.executor.*;
@Defined({"this.elementData[*]","this.size","this.elementData","this.modCount"})
public class AOTES_java_util_ArrayList {
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort729(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)2);
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayLength(vm.get("v4")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", om.getArrayElement(vm.get("v4"), vm.get("v2")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getArrayElement(vm.get("v4"), vm.get("v1")));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getArrayElement(vm.get("v4"), vm.get("v0")));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.getField(vm.get("this"), "size"));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", (int)7);
    vm.put("v19", (int)3);
    vm.put("v20", java.lang.Object[].class);
    vm.put("v21", null);
    vm.put("v22", em.op("isub").eval(vm.get("v17"),vm.get("v0")));
    vm.put("v23", em.op("isub").eval(vm.get("v17"),vm.get("v0")));
    vm.put("v24", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v4")));
    vm.put("v25", em.op("isub").eval(vm.get("v6"),vm.get("v0")));
    vm.put("v26", vm.get("v7"));
    vm.put("v27", em.op("isub").eval(vm.get("v26"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v27"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.le(vm.get("v25"), vm.get("v23"))) e.abort(String.format("!((v25=%s) le (v23=%s))", vm.get("v25"), vm.get("v23")));
    if (!em.lt(vm.get("v22"), vm.get("v18"))) e.abort(String.format("!((v22=%s) lt (v18=%s))", vm.get("v22"), vm.get("v18")));
    if (!em.lt(vm.get("v2"), vm.get("v25"))) e.abort(String.format("!((v2=%s) lt (v25=%s))", vm.get("v2"), vm.get("v25")));
    if (!em.le(vm.get("v0"), vm.get("v17"))) e.abort(String.format("!((v0=%s) le (v17=%s))", vm.get("v0"), vm.get("v17")));
    if (!em.ge(vm.get("v23"), vm.get("v0"))) e.abort(String.format("!((v23=%s) ge (v0=%s))", vm.get("v23"), vm.get("v0")));
    if (!em.lt(vm.get("v1"), vm.get("v25"))) e.abort(String.format("!((v1=%s) lt (v25=%s))", vm.get("v1"), vm.get("v25")));
    if (!em.lt(vm.get("v0"), vm.get("v17"))) e.abort(String.format("!((v0=%s) lt (v17=%s))", vm.get("v0"), vm.get("v17")));
    if (!em.eq(vm.get("param0"), vm.get("v21"))) e.abort(String.format("!((param0=%s) eq (v21=%s))", vm.get("param0"), vm.get("v21")));
    if (!em.ne(vm.get("v15"), vm.get("v0"))) e.abort(String.format("!((v15=%s) ne (v0=%s))", vm.get("v15"), vm.get("v0")));
    if (!em.le(vm.get("v17"), vm.get("v6"))) e.abort(String.format("!((v17=%s) le (v6=%s))", vm.get("v17"), vm.get("v6")));
    if (!em.ge(vm.get("v2"), vm.get("v17"))) e.abort(String.format("!((v2=%s) ge (v17=%s))", vm.get("v2"), vm.get("v17")));
    if (!em.ge(vm.get("v19"), vm.get("v25"))) e.abort(String.format("!((v19=%s) ge (v25=%s))", vm.get("v19"), vm.get("v25")));
    if (!em.lt(vm.get("v0"), vm.get("v25"))) e.abort(String.format("!((v0=%s) lt (v25=%s))", vm.get("v0"), vm.get("v25")));
    if (!em.eq(vm.get("v24"), vm.get("v20"))) e.abort(String.format("!((v24=%s) eq (v20=%s))", vm.get("v24"), vm.get("v20")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort799(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "modCount"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "size"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getArrayLength(vm.get("v4")));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)7);
    vm.put("v12", null);
    vm.put("v13", java.lang.Object[].class);
    vm.put("v14", em.op("isub").eval(vm.get("v6"),vm.get("v0")));
    vm.put("v15", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v4")));
    vm.put("v16", em.op("isub").eval(vm.get("v6"),vm.get("v0")));
    vm.put("v17", em.op("isub").eval(vm.get("v8"),vm.get("v0")));
    vm.put("v18", om.getArrayElement(vm.get("v4"), vm.get("v0")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", vm.get("v1"));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v21"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.lt(vm.get("v16"), vm.get("v11"))) e.abort(String.format("!((v16=%s) lt (v11=%s))", vm.get("v16"), vm.get("v11")));
    if (!em.eq(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) eq (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.lt(vm.get("v0"), vm.get("v17"))) e.abort(String.format("!((v0=%s) lt (v17=%s))", vm.get("v0"), vm.get("v17")));
    if (!em.eq(vm.get("v15"), vm.get("v13"))) e.abort(String.format("!((v15=%s) eq (v13=%s))", vm.get("v15"), vm.get("v13")));
    if (!em.ne(vm.get("v10"), vm.get("v0"))) e.abort(String.format("!((v10=%s) ne (v0=%s))", vm.get("v10"), vm.get("v0")));
    if (!em.ge(vm.get("v0"), vm.get("v6"))) e.abort(String.format("!((v0=%s) ge (v6=%s))", vm.get("v0"), vm.get("v6")));
    if (!em.le(vm.get("v0"), vm.get("v6"))) e.abort(String.format("!((v0=%s) le (v6=%s))", vm.get("v0"), vm.get("v6")));
    if (!em.ge(vm.get("v2"), vm.get("v17"))) e.abort(String.format("!((v2=%s) ge (v17=%s))", vm.get("v2"), vm.get("v17")));
    if (!em.le(vm.get("v17"), vm.get("v14"))) e.abort(String.format("!((v17=%s) le (v14=%s))", vm.get("v17"), vm.get("v14")));
    if (!em.le(vm.get("v6"), vm.get("v8"))) e.abort(String.format("!((v6=%s) le (v8=%s))", vm.get("v6"), vm.get("v8")));
    if (!em.ge(vm.get("v14"), vm.get("v0"))) e.abort(String.format("!((v14=%s) ge (v0=%s))", vm.get("v14"), vm.get("v0")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort770(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getStatic("java.util.ComparableTimSort.$assertionsDisabled"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)2);
    vm.put("v12", null);
    vm.put("v13", om.getArrayLength(vm.get("v7")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v5"),vm.get("v10")));
    vm.put("v16", vm.get("v0"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.le(vm.get("v10"), vm.get("v5"))) e.abort(String.format("!((v10=%s) le (v5=%s))", vm.get("v10"), vm.get("v5")));
    if (!em.lt(vm.get("v15"), vm.get("v11"))) e.abort(String.format("!((v15=%s) lt (v11=%s))", vm.get("v15"), vm.get("v11")));
    if (!em.eq(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) eq (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.eq(vm.get("v9"), vm.get("v10"))) e.abort(String.format("!((v9=%s) eq (v10=%s))", vm.get("v9"), vm.get("v10")));
    if (!em.ne(vm.get("v3"), vm.get("v10"))) e.abort(String.format("!((v3=%s) ne (v10=%s))", vm.get("v3"), vm.get("v10")));
    if (!em.le(vm.get("v5"), vm.get("v14"))) e.abort(String.format("!((v5=%s) le (v14=%s))", vm.get("v5"), vm.get("v14")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove1117(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", (int)3);
    vm.put("v3", (int)2);
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", null);
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", om.getArrayElement(vm.get("v5"), vm.get("v2")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("this"), "size"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getArrayElement(vm.get("v5"), vm.get("v1")));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getArrayElement(vm.get("v5"), vm.get("v0")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.getArrayElement(vm.get("v5"), vm.get("v3")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.popArrayElement(vm.get("v5"), vm.get("v11")));
    vm.put("v19", vm.get("v7"));
    vm.put("v20", em.op("isub").eval(vm.get("v19"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v20"));
    vm.put("v21", em.op("iadd").eval(vm.get("v11"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v21"));
    vm.put("v22", em.op("isub").eval(vm.get("v21"),vm.get("v2")));
    vm.put("v23", em.op("isub").eval(vm.get("v22"),vm.get("v1")));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.lt(vm.get("v1"), vm.get("v21"))) e.abort(String.format("!((v1=%s) lt (v21=%s))", vm.get("v1"), vm.get("v21")));
    if (!em.lt(vm.get("v0"), vm.get("v21"))) e.abort(String.format("!((v0=%s) lt (v21=%s))", vm.get("v0"), vm.get("v21")));
    if (!em.lt(vm.get("v3"), vm.get("v21"))) e.abort(String.format("!((v3=%s) lt (v21=%s))", vm.get("v3"), vm.get("v21")));
    if (!em.ge(vm.get("v0"), vm.get("v23"))) e.abort(String.format("!((v0=%s) ge (v23=%s))", vm.get("v0"), vm.get("v23")));
    if (!em.gt(vm.get("v23"), vm.get("v0"))) e.abort(String.format("!((v23=%s) gt (v0=%s))", vm.get("v23"), vm.get("v0")));
    if (!em.eq(vm.get("v9"), vm.get("v6"))) e.abort(String.format("!((v9=%s) eq (v6=%s))", vm.get("v9"), vm.get("v6")));
    if (!em.ne(vm.get("v15"), vm.get("v6"))) e.abort(String.format("!((v15=%s) ne (v6=%s))", vm.get("v15"), vm.get("v6")));
    if (!em.ne(vm.get("v17"), vm.get("v6"))) e.abort(String.format("!((v17=%s) ne (v6=%s))", vm.get("v17"), vm.get("v6")));
    if (!em.ne(vm.get("v13"), vm.get("v6"))) e.abort(String.format("!((v13=%s) ne (v6=%s))", vm.get("v13"), vm.get("v6")));
    if (!em.eq(vm.get("param0"), vm.get("v6"))) e.abort(String.format("!((param0=%s) eq (v6=%s))", vm.get("param0"), vm.get("v6")));
    if (!em.lt(vm.get("v2"), vm.get("v21"))) e.abort(String.format("!((v2=%s) lt (v21=%s))", vm.get("v2"), vm.get("v21")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add4896(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)0);
    vm.put("v8", om.getArrayLength(vm.get("v1")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", vm.get("v2"));
    vm.put("v11", vm.get("v4"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", em.op("isub").eval(vm.get("v10"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v13"));
    vm.put("v14", em.op("iadd").eval(vm.get("v13"),vm.get("v3")));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v9")));
    vm.put("v16", om.guessArrayIndex(vm.get("v1")));
    vm.put("param0", vm.get("v16"));
    vm.put("v17", om.popArrayElement(vm.get("v1"), vm.get("param0")));
    vm.put("param1", vm.get("v17"));
    vm.put("v18", em.op("isub").eval(vm.get("v13"),vm.get("param0")));
    if (!em.le(vm.get("v15"), vm.get("v7"))) e.abort(String.format("!((v15=%s) le (v7=%s))", vm.get("v15"), vm.get("v7")));
    if (!em.ge(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) ge (v7=%s))", vm.get("param0"), vm.get("v7")));
    if (!em.ne(vm.get("v1"), vm.get("v6"))) e.abort(String.format("!((v1=%s) ne (v6=%s))", vm.get("v1"), vm.get("v6")));
    if (!em.ge(vm.get("v7"), vm.get("v18"))) e.abort(String.format("!((v7=%s) ge (v18=%s))", vm.get("v7"), vm.get("v18")));
    if (!em.le(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) le (v13=%s))", vm.get("param0"), vm.get("v13")));
  }
  @IM(clazz="java.util.ArrayList", name="removeAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.elementData[*]","this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void removeAll173(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)4);
    vm.put("v1", (int)1);
    vm.put("v2", (int)0);
    vm.put("v3", (int)3);
    vm.put("v4", (int)2);
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "elementData"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)5);
    vm.put("v11", null);
    vm.put("v12", om.getArrayElement(vm.get("v9"), vm.get("v3")));
    vm.put("v13", vm.get("v12"));
    om.revertArrayElement(vm.get("v9"), vm.get("v3"), vm.get("v13"));
    vm.put("v14", om.getArrayElement(vm.get("v9"), vm.get("v2")));
    vm.put("v15", vm.get("v14"));
    om.revertArrayElement(vm.get("v9"), vm.get("v2"), vm.get("v15"));
    vm.put("v16", om.getArrayElement(vm.get("v9"), vm.get("v0")));
    vm.put("v17", vm.get("v16"));
    om.revertArrayElement(vm.get("v9"), vm.get("v0"), vm.get("v17"));
    vm.put("v18", om.getArrayElement(vm.get("v9"), vm.get("v4")));
    vm.put("v19", vm.get("v18"));
    om.revertArrayElement(vm.get("v9"), vm.get("v4"), vm.get("v19"));
    vm.put("v20", om.getArrayElement(vm.get("v9"), vm.get("v1")));
    vm.put("v21", vm.get("v20"));
    om.revertArrayElement(vm.get("v9"), vm.get("v1"), vm.get("v21"));
    vm.put("v22", em.op("isub").eval(vm.get("v7"),vm.get("v2")));
    vm.put("v23", vm.get("v5"));
    vm.put("v24", em.op("iadd").eval(vm.get("v22"),vm.get("v2")));
    om.revertField(vm.get("this"), "size", vm.get("v24"));
    vm.put("v25", em.op("isub").eval(vm.get("v24"),vm.get("v7")));
    vm.put("v26", em.op("isub").eval(vm.get("v24"),vm.get("v2")));
    vm.put("v27", em.op("isub").eval(vm.get("v23"),vm.get("v25")));
    om.revertField(vm.get("this"), "modCount", vm.get("v27"));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.ge(vm.get("v7"), vm.get("v24"))) e.abort(String.format("!((v7=%s) ge (v24=%s))", vm.get("v7"), vm.get("v24")));
    if (!em.ne(vm.get("v2"), vm.get("v24"))) e.abort(String.format("!((v2=%s) ne (v24=%s))", vm.get("v2"), vm.get("v24")));
    if (!em.lt(vm.get("v3"), vm.get("v26"))) e.abort(String.format("!((v3=%s) lt (v26=%s))", vm.get("v3"), vm.get("v26")));
    if (!em.lt(vm.get("v2"), vm.get("v26"))) e.abort(String.format("!((v2=%s) lt (v26=%s))", vm.get("v2"), vm.get("v26")));
    if (!em.ne(vm.get("param0"), vm.get("v11"))) e.abort(String.format("!((param0=%s) ne (v11=%s))", vm.get("param0"), vm.get("v11")));
    if (!em.lt(vm.get("v0"), vm.get("v26"))) e.abort(String.format("!((v0=%s) lt (v26=%s))", vm.get("v0"), vm.get("v26")));
    if (!em.lt(vm.get("v1"), vm.get("v26"))) e.abort(String.format("!((v1=%s) lt (v26=%s))", vm.get("v1"), vm.get("v26")));
    if (!em.ne(vm.get("v7"), vm.get("v24"))) e.abort(String.format("!((v7=%s) ne (v24=%s))", vm.get("v7"), vm.get("v24")));
    if (!em.ge(vm.get("v10"), vm.get("v26"))) e.abort(String.format("!((v10=%s) ge (v26=%s))", vm.get("v10"), vm.get("v26")));
    if (!em.ge(vm.get("v2"), vm.get("v24"))) e.abort(String.format("!((v2=%s) ge (v24=%s))", vm.get("v2"), vm.get("v24")));
    if (!em.lt(vm.get("v4"), vm.get("v26"))) e.abort(String.format("!((v4=%s) lt (v26=%s))", vm.get("v4"), vm.get("v26")));
  }
  @IM(clazz="java.util.ArrayList", name="set", desc="(ILjava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.elementData[*]"},definedOutput={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void set3299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.guessArrayIndex(vm.get("v3")));
    vm.put("param0", vm.get("v4"));
    vm.put("v5", om.getArrayElement(vm.get("v3"), vm.get("param0")));
    vm.put("param1", vm.get("v5"));
    om.revertArrayElement(vm.get("v3"), vm.get("param0"));
    vm.put("v6", om.getArrayElement(vm.get("v3"), vm.get("param0")));
    if (!em.lt(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) lt (v1=%s))", vm.get("param0"), vm.get("v1")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove3394(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", null);
    vm.put("v3", om.getField(vm.get("this"), "size"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)0);
    vm.put("v8", om.popArrayElement(vm.get("v6"), vm.get("v4")));
    vm.put("v9", em.op("iadd").eval(vm.get("v4"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v9"));
    vm.put("v10", vm.get("v0"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    vm.put("v12", om.guessArrayIndex(vm.get("v6")));
    vm.put("param0", vm.get("v12"));
    vm.put("v13", om.getArrayElement(vm.get("v6"), vm.get("param0")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v9"),vm.get("param0")));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v1")));
    if (!em.lt(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) lt (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.gt(vm.get("v16"), vm.get("v7"))) e.abort(String.format("!((v16=%s) gt (v7=%s))", vm.get("v16"), vm.get("v7")));
    if (!em.ge(vm.get("v7"), vm.get("v16"))) e.abort(String.format("!((v7=%s) ge (v16=%s))", vm.get("v7"), vm.get("v16")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort722(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getArrayLength(vm.get("v5")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", (int)1);
    vm.put("v10", (int)0);
    vm.put("v11", (int)7);
    vm.put("v12", null);
    vm.put("v13", java.lang.Object[].class);
    vm.put("v14", em.op("isub").eval(vm.get("v7"),vm.get("v10")));
    vm.put("v15", em.op("isub").eval(vm.get("v1"),vm.get("v10")));
    vm.put("v16", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v5")));
    vm.put("v17", em.op("isub").eval(vm.get("v1"),vm.get("v10")));
    vm.put("v18", vm.get("v8"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v9")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ge(vm.get("v10"), vm.get("v17"))) e.abort(String.format("!((v10=%s) ge (v17=%s))", vm.get("v10"), vm.get("v17")));
    if (!em.lt(vm.get("v15"), vm.get("v11"))) e.abort(String.format("!((v15=%s) lt (v11=%s))", vm.get("v15"), vm.get("v11")));
    if (!em.le(vm.get("v10"), vm.get("v1"))) e.abort(String.format("!((v10=%s) le (v1=%s))", vm.get("v10"), vm.get("v1")));
    if (!em.ge(vm.get("v17"), vm.get("v10"))) e.abort(String.format("!((v17=%s) ge (v10=%s))", vm.get("v17"), vm.get("v10")));
    if (!em.gt(vm.get("v14"), vm.get("v17"))) e.abort(String.format("!((v14=%s) gt (v17=%s))", vm.get("v14"), vm.get("v17")));
    if (!em.le(vm.get("v1"), vm.get("v7"))) e.abort(String.format("!((v1=%s) le (v7=%s))", vm.get("v1"), vm.get("v7")));
    if (!em.ge(vm.get("v10"), vm.get("v1"))) e.abort(String.format("!((v10=%s) ge (v1=%s))", vm.get("v10"), vm.get("v1")));
    if (!em.ne(vm.get("v3"), vm.get("v10"))) e.abort(String.format("!((v3=%s) ne (v10=%s))", vm.get("v3"), vm.get("v10")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ne(vm.get("v16"), vm.get("v13"))) e.abort(String.format("!((v16=%s) ne (v13=%s))", vm.get("v16"), vm.get("v13")));
  }
  @IM(clazz="java.util.ArrayList", name="retainAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.elementData[*]","this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void retainAll1318(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2);
    vm.put("v1", (int)0);
    vm.put("v2", (int)1);
    vm.put("v3", (int)3);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "size"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getArrayElement(vm.get("v6"), vm.get("v3")));
    vm.put("v10", vm.get("v9"));
    om.revertArrayElement(vm.get("v6"), vm.get("v3"), vm.get("v10"));
    vm.put("v11", om.getArrayElement(vm.get("v6"), vm.get("v2")));
    vm.put("v12", vm.get("v11"));
    om.revertArrayElement(vm.get("v6"), vm.get("v2"), vm.get("v12"));
    vm.put("v13", om.getArrayElement(vm.get("v6"), vm.get("v0")));
    vm.put("v14", vm.get("v13"));
    om.revertArrayElement(vm.get("v6"), vm.get("v0"), vm.get("v14"));
    vm.put("v15", (int)4);
    vm.put("v16", null);
    vm.put("v17", om.getArrayElement(vm.get("v6"), vm.get("v1")));
    vm.put("v18", vm.get("v17"));
    om.revertArrayElement(vm.get("v6"), vm.get("v1"), vm.get("v18"));
    vm.put("v19", vm.get("v4"));
    vm.put("v20", em.op("isub").eval(vm.get("v8"),vm.get("v1")));
    vm.put("v21", em.op("iadd").eval(vm.get("v20"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v21"));
    vm.put("v22", em.op("isub").eval(vm.get("v21"),vm.get("v8")));
    vm.put("v23", em.op("isub").eval(vm.get("v21"),vm.get("v1")));
    vm.put("v24", em.op("isub").eval(vm.get("v19"),vm.get("v22")));
    om.revertField(vm.get("this"), "modCount", vm.get("v24"));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.ge(vm.get("v15"), vm.get("v23"))) e.abort(String.format("!((v15=%s) ge (v23=%s))", vm.get("v15"), vm.get("v23")));
    if (!em.lt(vm.get("v3"), vm.get("v23"))) e.abort(String.format("!((v3=%s) lt (v23=%s))", vm.get("v3"), vm.get("v23")));
    if (!em.lt(vm.get("v2"), vm.get("v23"))) e.abort(String.format("!((v2=%s) lt (v23=%s))", vm.get("v2"), vm.get("v23")));
    if (!em.ge(vm.get("v8"), vm.get("v21"))) e.abort(String.format("!((v8=%s) ge (v21=%s))", vm.get("v8"), vm.get("v21")));
    if (!em.ge(vm.get("v1"), vm.get("v21"))) e.abort(String.format("!((v1=%s) ge (v21=%s))", vm.get("v1"), vm.get("v21")));
    if (!em.ne(vm.get("v8"), vm.get("v21"))) e.abort(String.format("!((v8=%s) ne (v21=%s))", vm.get("v8"), vm.get("v21")));
    if (!em.lt(vm.get("v1"), vm.get("v23"))) e.abort(String.format("!((v1=%s) lt (v23=%s))", vm.get("v1"), vm.get("v23")));
    if (!em.lt(vm.get("v0"), vm.get("v23"))) e.abort(String.format("!((v0=%s) lt (v23=%s))", vm.get("v0"), vm.get("v23")));
    if (!em.ne(vm.get("v1"), vm.get("v21"))) e.abort(String.format("!((v1=%s) ne (v21=%s))", vm.get("v1"), vm.get("v21")));
    if (!em.ne(vm.get("param0"), vm.get("v16"))) e.abort(String.format("!((param0=%s) ne (v16=%s))", vm.get("param0"), vm.get("v16")));
  }
  @IM(clazz="java.util.ArrayList", name="removeAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.size","this.modCount"},definedOutput={"this.size","this.modCount"})
  static void removeAll178(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)0);
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", null);
    vm.put("v7", em.op("isub").eval(vm.get("v5"),vm.get("v3")));
    vm.put("v8", em.op("iadd").eval(vm.get("v7"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v8"));
    vm.put("v9", em.op("isub").eval(vm.get("v8"),vm.get("v3")));
    vm.put("v10", em.op("isub").eval(vm.get("v8"),vm.get("v5")));
    vm.put("v11", vm.get("v2"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v10")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.ne(vm.get("v3"), vm.get("v8"))) e.abort(String.format("!((v3=%s) ne (v8=%s))", vm.get("v3"), vm.get("v8")));
    if (!em.ge(vm.get("v3"), vm.get("v8"))) e.abort(String.format("!((v3=%s) ge (v8=%s))", vm.get("v3"), vm.get("v8")));
    if (!em.ne(vm.get("v5"), vm.get("v8"))) e.abort(String.format("!((v5=%s) ne (v8=%s))", vm.get("v5"), vm.get("v8")));
    if (!em.ge(vm.get("v3"), vm.get("v9"))) e.abort(String.format("!((v3=%s) ge (v9=%s))", vm.get("v3"), vm.get("v9")));
    if (!em.ge(vm.get("v5"), vm.get("v8"))) e.abort(String.format("!((v5=%s) ge (v8=%s))", vm.get("v5"), vm.get("v8")));
    if (!em.ne(vm.get("param0"), vm.get("v6"))) e.abort(String.format("!((param0=%s) ne (v6=%s))", vm.get("param0"), vm.get("v6")));
  }
  @IM(clazz="java.util.ArrayList", name="removeAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.elementData[*]"},definedOutput={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void removeAll124(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "elementData"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "size"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayElement(vm.get("v2"), vm.get("v0")));
    vm.put("v6", vm.get("v5"));
    om.revertArrayElement(vm.get("v2"), vm.get("v0"), vm.get("v6"));
    vm.put("v7", (int)1);
    vm.put("v8", null);
    vm.put("v9", em.op("isub").eval(vm.get("v4"),vm.get("v0")));
    vm.put("v10", em.op("iadd").eval(vm.get("v0"),vm.get("v9")));
    vm.put("v11", em.op("isub").eval(vm.get("v4"),vm.get("v0")));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.ne(vm.get("v0"), vm.get("v4"))) e.abort(String.format("!((v0=%s) ne (v4=%s))", vm.get("v0"), vm.get("v4")));
    if (!em.ge(vm.get("v7"), vm.get("v11"))) e.abort(String.format("!((v7=%s) ge (v11=%s))", vm.get("v7"), vm.get("v11")));
    if (!em.ge(vm.get("v0"), vm.get("v4"))) e.abort(String.format("!((v0=%s) ge (v4=%s))", vm.get("v0"), vm.get("v4")));
    if (!em.ne(vm.get("param0"), vm.get("v8"))) e.abort(String.format("!((param0=%s) ne (v8=%s))", vm.get("param0"), vm.get("v8")));
    if (!em.eq(vm.get("v10"), vm.get("v4"))) e.abort(String.format("!((v10=%s) eq (v4=%s))", vm.get("v10"), vm.get("v4")));
    if (!em.lt(vm.get("v0"), vm.get("v11"))) e.abort(String.format("!((v0=%s) lt (v11=%s))", vm.get("v0"), vm.get("v11")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort745(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayElement(vm.get("v4"), vm.get("v0")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", (int)1);
    vm.put("v9", om.getArrayLength(vm.get("v4")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getField(vm.get("this"), "size"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", (int)7);
    vm.put("v14", java.lang.Object[].class);
    vm.put("v15", null);
    vm.put("v16", em.op("isub").eval(vm.get("v12"),vm.get("v0")));
    vm.put("v17", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    vm.put("v18", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v4")));
    vm.put("v19", em.op("isub").eval(vm.get("v12"),vm.get("v0")));
    vm.put("v20", vm.get("v7"));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v8")));
    om.revertField(vm.get("this"), "modCount", vm.get("v21"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.eq(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) eq (v15=%s))", vm.get("param0"), vm.get("v15")));
    if (!em.lt(vm.get("v16"), vm.get("v13"))) e.abort(String.format("!((v16=%s) lt (v13=%s))", vm.get("v16"), vm.get("v13")));
    if (!em.ne(vm.get("v18"), vm.get("v14"))) e.abort(String.format("!((v18=%s) ne (v14=%s))", vm.get("v18"), vm.get("v14")));
    if (!em.le(vm.get("v12"), vm.get("v10"))) e.abort(String.format("!((v12=%s) le (v10=%s))", vm.get("v12"), vm.get("v10")));
    if (!em.ge(vm.get("v19"), vm.get("v0"))) e.abort(String.format("!((v19=%s) ge (v0=%s))", vm.get("v19"), vm.get("v0")));
    if (!em.ge(vm.get("v0"), vm.get("v12"))) e.abort(String.format("!((v0=%s) ge (v12=%s))", vm.get("v0"), vm.get("v12")));
    if (!em.ne(vm.get("v2"), vm.get("v0"))) e.abort(String.format("!((v2=%s) ne (v0=%s))", vm.get("v2"), vm.get("v0")));
    if (!em.le(vm.get("v0"), vm.get("v12"))) e.abort(String.format("!((v0=%s) le (v12=%s))", vm.get("v0"), vm.get("v12")));
    if (!em.gt(vm.get("v17"), vm.get("v19"))) e.abort(String.format("!((v17=%s) gt (v19=%s))", vm.get("v17"), vm.get("v19")));
    if (!em.ge(vm.get("v8"), vm.get("v19"))) e.abort(String.format("!((v8=%s) ge (v19=%s))", vm.get("v8"), vm.get("v19")));
    if (!em.lt(vm.get("v0"), vm.get("v19"))) e.abort(String.format("!((v0=%s) lt (v19=%s))", vm.get("v0"), vm.get("v19")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort751(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getArrayLength(vm.get("v3")));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayElement(vm.get("v3"), vm.get("v1")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("this"), "modCount"));
    vm.put("v11", om.getField(vm.get("this"), "size"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", (int)7);
    vm.put("v16", null);
    vm.put("v17", (int)2);
    vm.put("v18", java.lang.Object[].class);
    vm.put("v19", em.op("isub").eval(vm.get("v12"),vm.get("v1")));
    vm.put("v20", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v3")));
    vm.put("v21", em.op("isub").eval(vm.get("v5"),vm.get("v1")));
    vm.put("v22", em.op("isub").eval(vm.get("v12"),vm.get("v1")));
    vm.put("v23", vm.get("v10"));
    vm.put("v24", em.op("isub").eval(vm.get("v23"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v24"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.le(vm.get("v1"), vm.get("v12"))) e.abort(String.format("!((v1=%s) le (v12=%s))", vm.get("v1"), vm.get("v12")));
    if (!em.ne(vm.get("v7"), vm.get("v1"))) e.abort(String.format("!((v7=%s) ne (v1=%s))", vm.get("v7"), vm.get("v1")));
    if (!em.ne(vm.get("v20"), vm.get("v18"))) e.abort(String.format("!((v20=%s) ne (v18=%s))", vm.get("v20"), vm.get("v18")));
    if (!em.lt(vm.get("v1"), vm.get("v21"))) e.abort(String.format("!((v1=%s) lt (v21=%s))", vm.get("v1"), vm.get("v21")));
    if (!em.ne(vm.get("param0"), vm.get("v16"))) e.abort(String.format("!((param0=%s) ne (v16=%s))", vm.get("param0"), vm.get("v16")));
    if (!em.ge(vm.get("v17"), vm.get("v21"))) e.abort(String.format("!((v17=%s) ge (v21=%s))", vm.get("v17"), vm.get("v21")));
    if (!em.le(vm.get("v21"), vm.get("v19"))) e.abort(String.format("!((v21=%s) le (v19=%s))", vm.get("v21"), vm.get("v19")));
    if (!em.le(vm.get("v12"), vm.get("v5"))) e.abort(String.format("!((v12=%s) le (v5=%s))", vm.get("v12"), vm.get("v5")));
    if (!em.ne(vm.get("param0"), vm.get("v16"))) e.abort(String.format("!((param0=%s) ne (v16=%s))", vm.get("param0"), vm.get("v16")));
    if (!em.ge(vm.get("v1"), vm.get("v12"))) e.abort(String.format("!((v1=%s) ge (v12=%s))", vm.get("v1"), vm.get("v12")));
    if (!em.lt(vm.get("v22"), vm.get("v15"))) e.abort(String.format("!((v22=%s) lt (v15=%s))", vm.get("v22"), vm.get("v15")));
    if (!em.lt(vm.get("v0"), vm.get("v21"))) e.abort(String.format("!((v0=%s) lt (v21=%s))", vm.get("v0"), vm.get("v21")));
    if (!em.ge(vm.get("v19"), vm.get("v1"))) e.abort(String.format("!((v19=%s) ge (v1=%s))", vm.get("v19"), vm.get("v1")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort736(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)0);
    vm.put("v9", (int)7);
    vm.put("v10", null);
    vm.put("v11", java.lang.Object[].class);
    vm.put("v12", em.op("isub").eval(vm.get("v5"),vm.get("v8")));
    vm.put("v13", em.op("isub").eval(vm.get("v5"),vm.get("v8")));
    vm.put("v14", om.getArrayLength(vm.get("v1")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v1")));
    vm.put("v17", em.op("isub").eval(vm.get("v15"),vm.get("v8")));
    vm.put("v18", vm.get("v2"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ge(vm.get("v8"), vm.get("v17"))) e.abort(String.format("!((v8=%s) ge (v17=%s))", vm.get("v8"), vm.get("v17")));
    if (!em.le(vm.get("v8"), vm.get("v5"))) e.abort(String.format("!((v8=%s) le (v5=%s))", vm.get("v8"), vm.get("v5")));
    if (!em.ne(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ne (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.lt(vm.get("v12"), vm.get("v9"))) e.abort(String.format("!((v12=%s) lt (v9=%s))", vm.get("v12"), vm.get("v9")));
    if (!em.le(vm.get("v17"), vm.get("v13"))) e.abort(String.format("!((v17=%s) le (v13=%s))", vm.get("v17"), vm.get("v13")));
    if (!em.ne(vm.get("v7"), vm.get("v8"))) e.abort(String.format("!((v7=%s) ne (v8=%s))", vm.get("v7"), vm.get("v8")));
    if (!em.le(vm.get("v5"), vm.get("v15"))) e.abort(String.format("!((v5=%s) le (v15=%s))", vm.get("v5"), vm.get("v15")));
    if (!em.eq(vm.get("v16"), vm.get("v11"))) e.abort(String.format("!((v16=%s) eq (v11=%s))", vm.get("v16"), vm.get("v11")));
    if (!em.ge(vm.get("v8"), vm.get("v5"))) e.abort(String.format("!((v8=%s) ge (v5=%s))", vm.get("v8"), vm.get("v5")));
    if (!em.ge(vm.get("v13"), vm.get("v8"))) e.abort(String.format("!((v13=%s) ge (v8=%s))", vm.get("v13"), vm.get("v8")));
    if (!em.ne(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ne (v10=%s))", vm.get("param0"), vm.get("v10")));
  }
  @IM(clazz="java.util.ArrayList", name="<init>", desc="()V",definedOutput={"this.elementData","this.modCount"},delta={"this.elementData","this.modCount"})
  @DG(succ={"retainAll1318","remove1111","add4859","sort755","remove1190","removeRange3485","remove1199","add4896","sort766","removeRange3497","clear4269","clear4299","remove1158","add4893","sort777","clear4256","sort781","fastRemove383","removeAll192","removeRange3419","fastRemove325","sort785","remove3390","trimToSize3697","remove3397","add4848","removeRange3489","sort793","add4895","sort779","remove3393","sort728","fastRemove386","add4833","sort791","removeRange3445","ensureCapacityInternal3589","removeRange3477","sort787","set3299","sort729","remove1117","sort757","add4897","retainAll1392","removeRange3417","removeAll146","removeAll173","retainAll1357","fastRemove332","add4886","removeAll182","add2989","sort741","sort745","removeAll124","add2994","remove3351","add2983","batchRemove4464","fastRemove321","remove1176","sort751","retainAll1398","sort798","sort735","batchRemove4491","removeAll181","remove1198","batchRemove4447","removeRange3490","sort722","remove3399","batchRemove4449","sort786","removeRange3441","add4852","removeRange3460","removeRange3438","removeRange3468","removeRange3416","retainAll1374","removeRange3480","removeAll152","batchRemove4481","clear4289","batchRemove4470","add4875","batchRemove4498","sort743","removeAll177","batchRemove4466","remove1145","removeRange3481","sort782","add4898","sort790","add4861","removeRange3484","remove3394","remove1116","clear4286","removeRange3423","sort732","remove3391","removeRange3407","batchRemove4421","retainAll1389","sort736","sort799","removeRange3494","add4828","sort747","sort770","remove3398","removeRange3499","fastRemove356","sort776"})
  static void _init_3799(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.popField(vm.get("this"), "modCount"));
    vm.put("v3", (int)0);
    vm.put("v4", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    if (!em.eq(vm.get("v1"), (vm.get("v4")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v4")));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.elementData[*]","this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove1116(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "size"));
    vm.put("v6", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayElement(vm.get("v3"), vm.get("v1")));
    vm.put("v9", null);
    vm.put("v10", vm.get("v4"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    vm.put("v12", vm.get("v5"));
    vm.put("v13", om.popArrayElement(vm.get("v3"), vm.get("v12")));
    vm.put("v14", em.op("iadd").eval(vm.get("v12"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v14"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v1")));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    om.revertArrayElement(vm.get("v3"), vm.get("v0"));
    vm.put("v17", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    if (!em.lt(vm.get("v0"), vm.get("v16"))) e.abort(String.format("!((v0=%s) lt (v16=%s))", vm.get("v0"), vm.get("v16")));
    if (!em.gt(vm.get("v16"), vm.get("v0"))) e.abort(String.format("!((v16=%s) gt (v0=%s))", vm.get("v16"), vm.get("v0")));
    if (!em.eq(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) eq (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.ge(vm.get("v1"), vm.get("v16"))) e.abort(String.format("!((v1=%s) ge (v16=%s))", vm.get("v1"), vm.get("v16")));
    if (!em.eq(vm.get("v17"), vm.get("v9"))) e.abort(String.format("!((v17=%s) eq (v9=%s))", vm.get("v17"), vm.get("v9")));
    if (!em.lt(vm.get("v0"), vm.get("v14"))) e.abort(String.format("!((v0=%s) lt (v14=%s))", vm.get("v0"), vm.get("v14")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add4861(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "size"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)10);
    vm.put("v9", om.getArrayLength(vm.get("v7")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", em.op("isub").eval(vm.get("v8"),vm.get("v10")));
    vm.put("v12", vm.get("v4"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    vm.put("v14", vm.get("v5"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v15"));
    vm.put("v16", em.op("iadd").eval(vm.get("v15"),vm.get("v3")));
    vm.put("v17", om.guessArrayIndex(vm.get("v7")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.popArrayElement(vm.get("v7"), vm.get("v18")));
    vm.put("v20", em.op("isub").eval(vm.get("v18"),vm.get("v0")));
    vm.put("param0", em.op("isub").eval(vm.get("v20"),vm.get("v3")));
    vm.put("v21", em.op("isub").eval(vm.get("v15"),vm.get("param0")));
    vm.put("v22", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v23", om.popArrayElement(vm.get("v7"), vm.get("param0")));
    vm.put("param1", vm.get("v23"));
    vm.put("v24", om.getArrayElement(vm.get("v7"), vm.get("v22")));
    vm.put("v25", vm.get("v24"));
    if (!em.lt(vm.get("v0"), vm.get("v21"))) e.abort(String.format("!((v0=%s) lt (v21=%s))", vm.get("v0"), vm.get("v21")));
    if (!em.ge(vm.get("v8"), vm.get("v16"))) e.abort(String.format("!((v8=%s) ge (v16=%s))", vm.get("v8"), vm.get("v16")));
    if (!em.eq(vm.get("v7"), vm.get("v2"))) e.abort(String.format("!((v7=%s) eq (v2=%s))", vm.get("v7"), vm.get("v2")));
    if (!em.le(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) le (v15=%s))", vm.get("param0"), vm.get("v15")));
    if (!em.le(vm.get("v11"), vm.get("v0"))) e.abort(String.format("!((v11=%s) le (v0=%s))", vm.get("v11"), vm.get("v0")));
    if (!em.ge(vm.get("v3"), vm.get("v21"))) e.abort(String.format("!((v3=%s) ge (v21=%s))", vm.get("v3"), vm.get("v21")));
    if (!em.ge(vm.get("param0"), vm.get("v0"))) e.abort(String.format("!((param0=%s) ge (v0=%s))", vm.get("param0"), vm.get("v0")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove1199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", null);
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayElement(vm.get("v4"), vm.get("v0")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getArrayElement(vm.get("v4"), vm.get("v1")));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.popArrayElement(vm.get("v4"), vm.get("v7")));
    vm.put("v13", em.op("iadd").eval(vm.get("v7"),vm.get("v0")));
    om.revertField(vm.get("this"), "size", vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v0")));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    vm.put("v16", vm.get("v5"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.ge(vm.get("v1"), vm.get("v15"))) e.abort(String.format("!((v1=%s) ge (v15=%s))", vm.get("v1"), vm.get("v15")));
    if (!em.lt(vm.get("v0"), vm.get("v13"))) e.abort(String.format("!((v0=%s) lt (v13=%s))", vm.get("v0"), vm.get("v13")));
    if (!em.ne(vm.get("v11"), vm.get("v2"))) e.abort(String.format("!((v11=%s) ne (v2=%s))", vm.get("v11"), vm.get("v2")));
    if (!em.eq(vm.get("v9"), vm.get("v2"))) e.abort(String.format("!((v9=%s) eq (v2=%s))", vm.get("v9"), vm.get("v2")));
    if (!em.lt(vm.get("v1"), vm.get("v13"))) e.abort(String.format("!((v1=%s) lt (v13=%s))", vm.get("v1"), vm.get("v13")));
    if (!em.eq(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) eq (v2=%s))", vm.get("param0"), vm.get("v2")));
    if (!em.gt(vm.get("v15"), vm.get("v1"))) e.abort(String.format("!((v15=%s) gt (v1=%s))", vm.get("v15"), vm.get("v1")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add4848(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2);
    vm.put("v1", (int)1);
    vm.put("v2", (int)3);
    vm.put("v3", (int)0);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("this"), "size"));
    vm.put("v10", (int)4);
    vm.put("v11", (int)10);
    vm.put("v12", om.getArrayLength(vm.get("v6")));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", vm.get("v9"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v15"));
    vm.put("v16", em.op("iadd").eval(vm.get("v15"),vm.get("v1")));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v13")));
    vm.put("v18", vm.get("v4"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("v20", om.guessArrayIndex(vm.get("v6")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.popArrayElement(vm.get("v6"), vm.get("v21")));
    vm.put("v23", em.op("isub").eval(vm.get("v21"),vm.get("v1")));
    vm.put("v24", em.op("iadd").eval(vm.get("v23"),vm.get("v0")));
    vm.put("v25", em.op("iadd").eval(vm.get("v23"),vm.get("v3")));
    vm.put("v26", om.popArrayElement(vm.get("v6"), vm.get("v25")));
    vm.put("v27", om.popArrayElement(vm.get("v6"), vm.get("v24")));
    vm.put("v28", em.op("iadd").eval(vm.get("v23"),vm.get("v2")));
    vm.put("v29", om.popArrayElement(vm.get("v6"), vm.get("v28")));
    vm.put("param0", em.op("isub").eval(vm.get("v23"),vm.get("v1")));
    vm.put("v30", em.op("iadd").eval(vm.get("param0"),vm.get("v3")));
    vm.put("v31", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v32", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v33", om.getArrayElement(vm.get("v6"), vm.get("v32")));
    vm.put("v34", vm.get("v33"));
    vm.put("v35", om.getArrayElement(vm.get("v6"), vm.get("v30")));
    vm.put("v36", vm.get("v35"));
    vm.put("v37", em.op("iadd").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v38", om.getArrayElement(vm.get("v6"), vm.get("v37")));
    vm.put("v39", vm.get("v38"));
    vm.put("v40", om.getArrayElement(vm.get("v6"), vm.get("v31")));
    vm.put("v41", vm.get("v40"));
    vm.put("v42", em.op("isub").eval(vm.get("v15"),vm.get("param0")));
    vm.put("v43", om.popArrayElement(vm.get("v6"), vm.get("param0")));
    vm.put("param1", vm.get("v43"));
    if (!em.le(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) le (v15=%s))", vm.get("param0"), vm.get("v15")));
    if (!em.lt(vm.get("v0"), vm.get("v42"))) e.abort(String.format("!((v0=%s) lt (v42=%s))", vm.get("v0"), vm.get("v42")));
    if (!em.lt(vm.get("v11"), vm.get("v16"))) e.abort(String.format("!((v11=%s) lt (v16=%s))", vm.get("v11"), vm.get("v16")));
    if (!em.le(vm.get("v17"), vm.get("v3"))) e.abort(String.format("!((v17=%s) le (v3=%s))", vm.get("v17"), vm.get("v3")));
    if (!em.lt(vm.get("v3"), vm.get("v42"))) e.abort(String.format("!((v3=%s) lt (v42=%s))", vm.get("v3"), vm.get("v42")));
    if (!em.ge(vm.get("v10"), vm.get("v42"))) e.abort(String.format("!((v10=%s) ge (v42=%s))", vm.get("v10"), vm.get("v42")));
    if (!em.lt(vm.get("v2"), vm.get("v42"))) e.abort(String.format("!((v2=%s) lt (v42=%s))", vm.get("v2"), vm.get("v42")));
    if (!em.lt(vm.get("v1"), vm.get("v42"))) e.abort(String.format("!((v1=%s) lt (v42=%s))", vm.get("v1"), vm.get("v42")));
    if (!em.eq(vm.get("v6"), vm.get("v8"))) e.abort(String.format("!((v6=%s) eq (v8=%s))", vm.get("v6"), vm.get("v8")));
    if (!em.ge(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) ge (v3=%s))", vm.get("param0"), vm.get("v3")));
  }
  @IM(clazz="java.util.ArrayList", name="removeAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.elementData[*]"},definedOutput={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void removeAll146(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)2);
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "size"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getArrayElement(vm.get("v6"), vm.get("v2")));
    vm.put("v8", vm.get("v7"));
    om.revertArrayElement(vm.get("v6"), vm.get("v2"), vm.get("v8"));
    vm.put("v9", om.getArrayElement(vm.get("v6"), vm.get("v1")));
    vm.put("v10", vm.get("v9"));
    om.revertArrayElement(vm.get("v6"), vm.get("v1"), vm.get("v10"));
    vm.put("v11", (int)3);
    vm.put("v12", null);
    vm.put("v13", em.op("isub").eval(vm.get("v4"),vm.get("v0")));
    vm.put("v14", em.op("isub").eval(vm.get("v4"),vm.get("v0")));
    vm.put("v15", em.op("iadd").eval(vm.get("v0"),vm.get("v14")));
    vm.put("v16", om.getArrayElement(vm.get("v6"), vm.get("v0")));
    vm.put("v17", vm.get("v16"));
    om.revertArrayElement(vm.get("v6"), vm.get("v0"), vm.get("v17"));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.lt(vm.get("v0"), vm.get("v13"))) e.abort(String.format("!((v0=%s) lt (v13=%s))", vm.get("v0"), vm.get("v13")));
    if (!em.lt(vm.get("v1"), vm.get("v13"))) e.abort(String.format("!((v1=%s) lt (v13=%s))", vm.get("v1"), vm.get("v13")));
    if (!em.ne(vm.get("v0"), vm.get("v4"))) e.abort(String.format("!((v0=%s) ne (v4=%s))", vm.get("v0"), vm.get("v4")));
    if (!em.ge(vm.get("v0"), vm.get("v4"))) e.abort(String.format("!((v0=%s) ge (v4=%s))", vm.get("v0"), vm.get("v4")));
    if (!em.ge(vm.get("v11"), vm.get("v13"))) e.abort(String.format("!((v11=%s) ge (v13=%s))", vm.get("v11"), vm.get("v13")));
    if (!em.eq(vm.get("v15"), vm.get("v4"))) e.abort(String.format("!((v15=%s) eq (v4=%s))", vm.get("v15"), vm.get("v4")));
    if (!em.lt(vm.get("v2"), vm.get("v13"))) e.abort(String.format("!((v2=%s) lt (v13=%s))", vm.get("v2"), vm.get("v13")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove3393(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "size"));
    vm.put("v6", null);
    vm.put("v7", (int)2);
    vm.put("v8", vm.get("v4"));
    vm.put("v9", em.op("isub").eval(vm.get("v8"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v9"));
    vm.put("v10", vm.get("v5"));
    vm.put("v11", om.popArrayElement(vm.get("v3"), vm.get("v10")));
    vm.put("v12", em.op("iadd").eval(vm.get("v10"),vm.get("v0")));
    om.revertField(vm.get("this"), "size", vm.get("v12"));
    vm.put("v13", om.guessArrayIndex(vm.get("v3")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.popArrayElement(vm.get("v3"), vm.get("v14")));
    vm.put("param0", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    vm.put("v16", om.getArrayElement(vm.get("v3"), vm.get("param0")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v19", em.op("iadd").eval(vm.get("v18"),vm.get("v0")));
    vm.put("v20", om.getArrayElement(vm.get("v3"), vm.get("v19")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", em.op("iadd").eval(vm.get("v18"),vm.get("v1")));
    vm.put("v23", em.op("isub").eval(vm.get("v12"),vm.get("param0")));
    vm.put("v24", em.op("isub").eval(vm.get("v23"),vm.get("v0")));
    vm.put("v25", om.getArrayElement(vm.get("v3"), vm.get("v22")));
    vm.put("v26", vm.get("v25"));
    vm.put("v27", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v28", om.popArrayElement(vm.get("v3"), vm.get("v27")));
    if (!em.lt(vm.get("v0"), vm.get("v24"))) e.abort(String.format("!((v0=%s) lt (v24=%s))", vm.get("v0"), vm.get("v24")));
    if (!em.ge(vm.get("v7"), vm.get("v24"))) e.abort(String.format("!((v7=%s) ge (v24=%s))", vm.get("v7"), vm.get("v24")));
    if (!em.lt(vm.get("v1"), vm.get("v24"))) e.abort(String.format("!((v1=%s) lt (v24=%s))", vm.get("v1"), vm.get("v24")));
    if (!em.lt(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) lt (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.gt(vm.get("v24"), vm.get("v1"))) e.abort(String.format("!((v24=%s) gt (v1=%s))", vm.get("v24"), vm.get("v1")));
  }
  @IM(clazz="java.util.ArrayList", name="retainAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.elementData[*]"},definedOutput={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void retainAll1389(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "size"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayElement(vm.get("v4"), vm.get("v0")));
    vm.put("v6", vm.get("v5"));
    om.revertArrayElement(vm.get("v4"), vm.get("v0"), vm.get("v6"));
    vm.put("v7", (int)1);
    vm.put("v8", null);
    vm.put("v9", em.op("isub").eval(vm.get("v2"),vm.get("v0")));
    vm.put("v10", em.op("iadd").eval(vm.get("v0"),vm.get("v9")));
    vm.put("v11", em.op("isub").eval(vm.get("v2"),vm.get("v0")));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.ne(vm.get("v0"), vm.get("v2"))) e.abort(String.format("!((v0=%s) ne (v2=%s))", vm.get("v0"), vm.get("v2")));
    if (!em.lt(vm.get("v0"), vm.get("v11"))) e.abort(String.format("!((v0=%s) lt (v11=%s))", vm.get("v0"), vm.get("v11")));
    if (!em.ne(vm.get("param0"), vm.get("v8"))) e.abort(String.format("!((param0=%s) ne (v8=%s))", vm.get("param0"), vm.get("v8")));
    if (!em.ge(vm.get("v7"), vm.get("v11"))) e.abort(String.format("!((v7=%s) ge (v11=%s))", vm.get("v7"), vm.get("v11")));
    if (!em.ge(vm.get("v0"), vm.get("v2"))) e.abort(String.format("!((v0=%s) ge (v2=%s))", vm.get("v0"), vm.get("v2")));
    if (!em.eq(vm.get("v10"), vm.get("v2"))) e.abort(String.format("!((v10=%s) eq (v2=%s))", vm.get("v10"), vm.get("v2")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove1176(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "elementData"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", (int)1);
    vm.put("v4", null);
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayElement(vm.get("v2"), vm.get("v0")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.popArrayElement(vm.get("v2"), vm.get("v7")));
    vm.put("v11", em.op("iadd").eval(vm.get("v7"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v11"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v0")));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v3")));
    vm.put("v14", vm.get("v5"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.eq(vm.get("v9"), vm.get("v4"))) e.abort(String.format("!((v9=%s) eq (v4=%s))", vm.get("v9"), vm.get("v4")));
    if (!em.lt(vm.get("v0"), vm.get("v11"))) e.abort(String.format("!((v0=%s) lt (v11=%s))", vm.get("v0"), vm.get("v11")));
    if (!em.le(vm.get("v13"), vm.get("v0"))) e.abort(String.format("!((v13=%s) le (v0=%s))", vm.get("v13"), vm.get("v0")));
    if (!em.eq(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) eq (v4=%s))", vm.get("param0"), vm.get("v4")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add2994(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)0);
    vm.put("v8", om.getArrayLength(vm.get("v1")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", vm.get("v4"));
    vm.put("v11", vm.get("v2"));
    vm.put("v12", em.op("isub").eval(vm.get("v10"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", em.op("isub").eval(vm.get("v11"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v13"));
    vm.put("v14", om.popArrayElement(vm.get("v1"), vm.get("v13")));
    vm.put("param0", vm.get("v14"));
    vm.put("v15", em.op("iadd").eval(vm.get("v13"),vm.get("v3")));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v9")));
    if (!em.le(vm.get("v16"), vm.get("v7"))) e.abort(String.format("!((v16=%s) le (v7=%s))", vm.get("v16"), vm.get("v7")));
    if (!em.ne(vm.get("v1"), vm.get("v6"))) e.abort(String.format("!((v1=%s) ne (v6=%s))", vm.get("v1"), vm.get("v6")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove3399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)3);
    vm.put("v2", (int)4);
    vm.put("v3", (int)2);
    vm.put("v4", (int)5);
    vm.put("v5", (int)0);
    vm.put("v6", null);
    vm.put("v7", om.getField(vm.get("this"), "size"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("this"), "modCount"));
    vm.put("v10", om.getField(vm.get("this"), "elementData"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", (int)6);
    vm.put("v13", om.popArrayElement(vm.get("v11"), vm.get("v8")));
    vm.put("v14", em.op("iadd").eval(vm.get("v8"),vm.get("v0")));
    om.revertField(vm.get("this"), "size", vm.get("v14"));
    vm.put("v15", vm.get("v9"));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v16"));
    vm.put("v17", om.guessArrayIndex(vm.get("v11")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.popArrayElement(vm.get("v11"), vm.get("v18")));
    vm.put("param0", em.op("isub").eval(vm.get("v18"),vm.get("v3")));
    vm.put("v20", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v21", om.popArrayElement(vm.get("v11"), vm.get("v20")));
    vm.put("v22", om.getArrayElement(vm.get("v11"), vm.get("param0")));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v25", em.op("iadd").eval(vm.get("v24"),vm.get("v5")));
    vm.put("v26", om.getArrayElement(vm.get("v11"), vm.get("v25")));
    vm.put("v27", vm.get("v26"));
    vm.put("v28", em.op("iadd").eval(vm.get("param0"),vm.get("v4")));
    vm.put("v29", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v30", em.op("iadd").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v31", om.popArrayElement(vm.get("v11"), vm.get("v30")));
    vm.put("v32", em.op("iadd").eval(vm.get("v24"),vm.get("v0")));
    vm.put("v33", om.getArrayElement(vm.get("v11"), vm.get("v32")));
    vm.put("v34", vm.get("v33"));
    vm.put("v35", em.op("iadd").eval(vm.get("v24"),vm.get("v1")));
    vm.put("v36", om.getArrayElement(vm.get("v11"), vm.get("v35")));
    vm.put("v37", vm.get("v36"));
    vm.put("v38", em.op("isub").eval(vm.get("v14"),vm.get("param0")));
    vm.put("v39", em.op("isub").eval(vm.get("v38"),vm.get("v0")));
    vm.put("v40", em.op("iadd").eval(vm.get("v24"),vm.get("v4")));
    vm.put("v41", om.getArrayElement(vm.get("v11"), vm.get("v40")));
    vm.put("v42", vm.get("v41"));
    vm.put("v43", om.popArrayElement(vm.get("v11"), vm.get("v29")));
    vm.put("v44", om.popArrayElement(vm.get("v11"), vm.get("v28")));
    vm.put("v45", em.op("iadd").eval(vm.get("v24"),vm.get("v3")));
    vm.put("v46", om.getArrayElement(vm.get("v11"), vm.get("v45")));
    vm.put("v47", vm.get("v46"));
    vm.put("v48", em.op("iadd").eval(vm.get("param0"),vm.get("v5")));
    vm.put("v49", om.popArrayElement(vm.get("v11"), vm.get("v48")));
    vm.put("v50", em.op("iadd").eval(vm.get("v24"),vm.get("v2")));
    vm.put("v51", om.getArrayElement(vm.get("v11"), vm.get("v50")));
    vm.put("v52", vm.get("v51"));
    if (!em.lt(vm.get("v5"), vm.get("v39"))) e.abort(String.format("!((v5=%s) lt (v39=%s))", vm.get("v5"), vm.get("v39")));
    if (!em.lt(vm.get("v1"), vm.get("v39"))) e.abort(String.format("!((v1=%s) lt (v39=%s))", vm.get("v1"), vm.get("v39")));
    if (!em.lt(vm.get("v3"), vm.get("v39"))) e.abort(String.format("!((v3=%s) lt (v39=%s))", vm.get("v3"), vm.get("v39")));
    if (!em.lt(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) lt (v14=%s))", vm.get("param0"), vm.get("v14")));
    if (!em.ge(vm.get("v12"), vm.get("v39"))) e.abort(String.format("!((v12=%s) ge (v39=%s))", vm.get("v12"), vm.get("v39")));
    if (!em.lt(vm.get("v0"), vm.get("v39"))) e.abort(String.format("!((v0=%s) lt (v39=%s))", vm.get("v0"), vm.get("v39")));
    if (!em.lt(vm.get("v2"), vm.get("v39"))) e.abort(String.format("!((v2=%s) lt (v39=%s))", vm.get("v2"), vm.get("v39")));
    if (!em.lt(vm.get("v4"), vm.get("v39"))) e.abort(String.format("!((v4=%s) lt (v39=%s))", vm.get("v4"), vm.get("v39")));
    if (!em.gt(vm.get("v39"), vm.get("v5"))) e.abort(String.format("!((v39=%s) gt (v5=%s))", vm.get("v39"), vm.get("v5")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort777(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "elementData"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "size"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", (int)1);
    vm.put("v7", om.getArrayLength(vm.get("v2")));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)7);
    vm.put("v12", java.lang.Object[].class);
    vm.put("v13", null);
    vm.put("v14", em.op("isub").eval(vm.get("v4"),vm.get("v0")));
    vm.put("v15", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v2")));
    vm.put("v16", om.getArrayElement(vm.get("v2"), vm.get("v0")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", em.op("isub").eval(vm.get("v8"),vm.get("v0")));
    vm.put("v19", em.op("isub").eval(vm.get("v4"),vm.get("v0")));
    vm.put("v20", vm.get("v5"));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v6")));
    om.revertField(vm.get("this"), "modCount", vm.get("v21"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ne(vm.get("v15"), vm.get("v12"))) e.abort(String.format("!((v15=%s) ne (v12=%s))", vm.get("v15"), vm.get("v12")));
    if (!em.le(vm.get("v18"), vm.get("v19"))) e.abort(String.format("!((v18=%s) le (v19=%s))", vm.get("v18"), vm.get("v19")));
    if (!em.le(vm.get("v4"), vm.get("v8"))) e.abort(String.format("!((v4=%s) le (v8=%s))", vm.get("v4"), vm.get("v8")));
    if (!em.ne(vm.get("v10"), vm.get("v0"))) e.abort(String.format("!((v10=%s) ne (v0=%s))", vm.get("v10"), vm.get("v0")));
    if (!em.ge(vm.get("v6"), vm.get("v18"))) e.abort(String.format("!((v6=%s) ge (v18=%s))", vm.get("v6"), vm.get("v18")));
    if (!em.ge(vm.get("v0"), vm.get("v4"))) e.abort(String.format("!((v0=%s) ge (v4=%s))", vm.get("v0"), vm.get("v4")));
    if (!em.le(vm.get("v0"), vm.get("v4"))) e.abort(String.format("!((v0=%s) le (v4=%s))", vm.get("v0"), vm.get("v4")));
    if (!em.lt(vm.get("v0"), vm.get("v18"))) e.abort(String.format("!((v0=%s) lt (v18=%s))", vm.get("v0"), vm.get("v18")));
    if (!em.eq(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) eq (v13=%s))", vm.get("param0"), vm.get("v13")));
    if (!em.ge(vm.get("v19"), vm.get("v0"))) e.abort(String.format("!((v19=%s) ge (v0=%s))", vm.get("v19"), vm.get("v0")));
    if (!em.lt(vm.get("v14"), vm.get("v11"))) e.abort(String.format("!((v14=%s) lt (v11=%s))", vm.get("v14"), vm.get("v11")));
  }
  @IM(clazz="java.util.ArrayList", name="removeAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.elementData[*]","this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void removeAll182(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "size"));
    vm.put("v2", null);
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", (int)1);
    vm.put("v7", om.getArrayElement(vm.get("v5"), vm.get("v0")));
    vm.put("v8", vm.get("v7"));
    om.revertArrayElement(vm.get("v5"), vm.get("v0"), vm.get("v8"));
    vm.put("v9", vm.get("v1"));
    vm.put("v10", em.op("iinc").eval(vm.get("v9"),vm.get("v6")));
    vm.put("v11", om.popArrayElement(vm.get("v5"), vm.get("v9")));
    vm.put("v12", em.op("isub").eval(vm.get("v9"),vm.get("v0")));
    vm.put("v13", vm.get("v3"));
    vm.put("v14", em.op("iadd").eval(vm.get("v12"),vm.get("v0")));
    om.revertField(vm.get("this"), "size", vm.get("v14"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    vm.put("v16", em.op("isub").eval(vm.get("v14"),vm.get("v9")));
    vm.put("v17", em.op("isub").eval(vm.get("v13"),vm.get("v16")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.ge(vm.get("v10"), vm.get("v14"))) e.abort(String.format("!((v10=%s) ge (v14=%s))", vm.get("v10"), vm.get("v14")));
    if (!em.ne(vm.get("v9"), vm.get("v14"))) e.abort(String.format("!((v9=%s) ne (v14=%s))", vm.get("v9"), vm.get("v14")));
    if (!em.lt(vm.get("v0"), vm.get("v15"))) e.abort(String.format("!((v0=%s) lt (v15=%s))", vm.get("v0"), vm.get("v15")));
    if (!em.lt(vm.get("v9"), vm.get("v14"))) e.abort(String.format("!((v9=%s) lt (v14=%s))", vm.get("v9"), vm.get("v14")));
    if (!em.ne(vm.get("v0"), vm.get("v14"))) e.abort(String.format("!((v0=%s) ne (v14=%s))", vm.get("v0"), vm.get("v14")));
    if (!em.ge(vm.get("v6"), vm.get("v15"))) e.abort(String.format("!((v6=%s) ge (v15=%s))", vm.get("v6"), vm.get("v15")));
    if (!em.ne(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) ne (v2=%s))", vm.get("param0"), vm.get("v2")));
    if (!em.ge(vm.get("v0"), vm.get("v14"))) e.abort(String.format("!((v0=%s) ge (v14=%s))", vm.get("v0"), vm.get("v14")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort757(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getStatic("java.util.TimSort.$assertionsDisabled"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayLength(vm.get("v1")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", (int)0);
    vm.put("v13", (int)2);
    vm.put("v14", null);
    vm.put("v15", em.op("isub").eval(vm.get("v7"),vm.get("v12")));
    vm.put("v16", vm.get("v2"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.eq(vm.get("v11"), vm.get("v12"))) e.abort(String.format("!((v11=%s) eq (v12=%s))", vm.get("v11"), vm.get("v12")));
    if (!em.ne(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) ne (v14=%s))", vm.get("param0"), vm.get("v14")));
    if (!em.le(vm.get("v12"), vm.get("v7"))) e.abort(String.format("!((v12=%s) le (v7=%s))", vm.get("v12"), vm.get("v7")));
    if (!em.ne(vm.get("v5"), vm.get("v12"))) e.abort(String.format("!((v5=%s) ne (v12=%s))", vm.get("v5"), vm.get("v12")));
    if (!em.lt(vm.get("v15"), vm.get("v13"))) e.abort(String.format("!((v15=%s) lt (v13=%s))", vm.get("v15"), vm.get("v13")));
    if (!em.le(vm.get("v7"), vm.get("v9"))) e.abort(String.format("!((v7=%s) le (v9=%s))", vm.get("v7"), vm.get("v9")));
  }
  @IM(clazz="java.util.ArrayList", name="removeAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void removeAll177(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", null);
    vm.put("v3", om.getField(vm.get("this"), "size"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.popArrayElement(vm.get("v7"), vm.get("v4")));
    vm.put("v9", em.op("iinc").eval(vm.get("v4"),vm.get("v1")));
    vm.put("v10", em.op("iinc").eval(vm.get("v9"),vm.get("v1")));
    vm.put("v11", om.popArrayElement(vm.get("v7"), vm.get("v10")));
    vm.put("v12", om.popArrayElement(vm.get("v7"), vm.get("v9")));
    vm.put("v13", em.op("iinc").eval(vm.get("v10"),vm.get("v1")));
    vm.put("v14", om.popArrayElement(vm.get("v7"), vm.get("v13")));
    vm.put("v15", em.op("iinc").eval(vm.get("v13"),vm.get("v1")));
    vm.put("v16", vm.get("v5"));
    vm.put("v17", em.op("isub").eval(vm.get("v4"),vm.get("v0")));
    vm.put("v18", em.op("iadd").eval(vm.get("v17"),vm.get("v0")));
    om.revertField(vm.get("this"), "size", vm.get("v18"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v0")));
    vm.put("v20", em.op("isub").eval(vm.get("v18"),vm.get("v4")));
    vm.put("v21", em.op("isub").eval(vm.get("v16"),vm.get("v20")));
    om.revertField(vm.get("this"), "modCount", vm.get("v21"));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.ne(vm.get("v0"), vm.get("v18"))) e.abort(String.format("!((v0=%s) ne (v18=%s))", vm.get("v0"), vm.get("v18")));
    if (!em.ge(vm.get("v0"), vm.get("v18"))) e.abort(String.format("!((v0=%s) ge (v18=%s))", vm.get("v0"), vm.get("v18")));
    if (!em.ne(vm.get("v4"), vm.get("v18"))) e.abort(String.format("!((v4=%s) ne (v18=%s))", vm.get("v4"), vm.get("v18")));
    if (!em.ge(vm.get("v15"), vm.get("v18"))) e.abort(String.format("!((v15=%s) ge (v18=%s))", vm.get("v15"), vm.get("v18")));
    if (!em.lt(vm.get("v13"), vm.get("v18"))) e.abort(String.format("!((v13=%s) lt (v18=%s))", vm.get("v13"), vm.get("v18")));
    if (!em.lt(vm.get("v10"), vm.get("v18"))) e.abort(String.format("!((v10=%s) lt (v18=%s))", vm.get("v10"), vm.get("v18")));
    if (!em.lt(vm.get("v4"), vm.get("v18"))) e.abort(String.format("!((v4=%s) lt (v18=%s))", vm.get("v4"), vm.get("v18")));
    if (!em.lt(vm.get("v9"), vm.get("v18"))) e.abort(String.format("!((v9=%s) lt (v18=%s))", vm.get("v9"), vm.get("v18")));
    if (!em.ge(vm.get("v0"), vm.get("v19"))) e.abort(String.format("!((v0=%s) ge (v19=%s))", vm.get("v0"), vm.get("v19")));
    if (!em.ne(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) ne (v2=%s))", vm.get("param0"), vm.get("v2")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add4886(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", om.getArrayLength(vm.get("v5")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", vm.get("v7"));
    vm.put("v11", vm.get("v6"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v12"));
    vm.put("v13", em.op("iadd").eval(vm.get("v12"),vm.get("v1")));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v9")));
    vm.put("v15", em.op("isub").eval(vm.get("v10"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    vm.put("v16", om.guessArrayIndex(vm.get("v5")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.getArrayElement(vm.get("v5"), vm.get("v17")));
    vm.put("v19", vm.get("v18"));
    vm.put("param0", em.op("isub").eval(vm.get("v17"),vm.get("v0")));
    vm.put("v20", om.popArrayElement(vm.get("v5"), vm.get("param0")));
    vm.put("param1", vm.get("v20"));
    vm.put("v21", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v22", em.op("iadd").eval(vm.get("v21"),vm.get("v0")));
    vm.put("v23", om.popArrayElement(vm.get("v5"), vm.get("v22")));
    vm.put("v24", em.op("isub").eval(vm.get("v12"),vm.get("param0")));
    if (!em.ge(vm.get("v1"), vm.get("v24"))) e.abort(String.format("!((v1=%s) ge (v24=%s))", vm.get("v1"), vm.get("v24")));
    if (!em.ge(vm.get("param0"), vm.get("v0"))) e.abort(String.format("!((param0=%s) ge (v0=%s))", vm.get("param0"), vm.get("v0")));
    if (!em.lt(vm.get("v0"), vm.get("v24"))) e.abort(String.format("!((v0=%s) lt (v24=%s))", vm.get("v0"), vm.get("v24")));
    if (!em.le(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) le (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.le(vm.get("v14"), vm.get("v0"))) e.abort(String.format("!((v14=%s) le (v0=%s))", vm.get("v14"), vm.get("v0")));
    if (!em.ne(vm.get("v5"), vm.get("v3"))) e.abort(String.format("!((v5=%s) ne (v3=%s))", vm.get("v5"), vm.get("v3")));
  }
  @IM(clazz="java.util.ArrayList", name="removeAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.elementData[*]","this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void removeAll181(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", null);
    vm.put("v3", om.getField(vm.get("this"), "size"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", em.op("iinc").eval(vm.get("v4"),vm.get("v1")));
    vm.put("v9", om.popArrayElement(vm.get("v7"), vm.get("v8")));
    vm.put("v10", em.op("iinc").eval(vm.get("v8"),vm.get("v1")));
    vm.put("v11", om.popArrayElement(vm.get("v7"), vm.get("v4")));
    vm.put("v12", om.getArrayElement(vm.get("v7"), vm.get("v0")));
    vm.put("v13", vm.get("v12"));
    om.revertArrayElement(vm.get("v7"), vm.get("v0"), vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v4"),vm.get("v0")));
    vm.put("v15", vm.get("v5"));
    vm.put("v16", em.op("iadd").eval(vm.get("v14"),vm.get("v0")));
    om.revertField(vm.get("this"), "size", vm.get("v16"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    vm.put("v18", em.op("isub").eval(vm.get("v16"),vm.get("v4")));
    vm.put("v19", em.op("isub").eval(vm.get("v15"),vm.get("v18")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.ge(vm.get("v10"), vm.get("v16"))) e.abort(String.format("!((v10=%s) ge (v16=%s))", vm.get("v10"), vm.get("v16")));
    if (!em.lt(vm.get("v4"), vm.get("v16"))) e.abort(String.format("!((v4=%s) lt (v16=%s))", vm.get("v4"), vm.get("v16")));
    if (!em.ge(vm.get("v0"), vm.get("v16"))) e.abort(String.format("!((v0=%s) ge (v16=%s))", vm.get("v0"), vm.get("v16")));
    if (!em.ge(vm.get("v1"), vm.get("v17"))) e.abort(String.format("!((v1=%s) ge (v17=%s))", vm.get("v1"), vm.get("v17")));
    if (!em.ne(vm.get("v4"), vm.get("v16"))) e.abort(String.format("!((v4=%s) ne (v16=%s))", vm.get("v4"), vm.get("v16")));
    if (!em.ne(vm.get("v0"), vm.get("v16"))) e.abort(String.format("!((v0=%s) ne (v16=%s))", vm.get("v0"), vm.get("v16")));
    if (!em.ne(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) ne (v2=%s))", vm.get("param0"), vm.get("v2")));
    if (!em.lt(vm.get("v8"), vm.get("v16"))) e.abort(String.format("!((v8=%s) lt (v16=%s))", vm.get("v8"), vm.get("v16")));
    if (!em.lt(vm.get("v0"), vm.get("v17"))) e.abort(String.format("!((v0=%s) lt (v17=%s))", vm.get("v0"), vm.get("v17")));
  }
  @IM(clazz="java.util.ArrayList", name="retainAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void retainAll1392(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)0);
    vm.put("v4", null);
    vm.put("v5", (int)1);
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.popArrayElement(vm.get("v1"), vm.get("v7")));
    vm.put("v9", em.op("iinc").eval(vm.get("v7"),vm.get("v5")));
    vm.put("v10", om.popArrayElement(vm.get("v1"), vm.get("v9")));
    vm.put("v11", em.op("iinc").eval(vm.get("v9"),vm.get("v5")));
    vm.put("v12", vm.get("v2"));
    vm.put("v13", em.op("isub").eval(vm.get("v7"),vm.get("v3")));
    vm.put("v14", em.op("iadd").eval(vm.get("v13"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v14"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v7")));
    vm.put("v16", em.op("isub").eval(vm.get("v14"),vm.get("v3")));
    vm.put("v17", em.op("isub").eval(vm.get("v12"),vm.get("v15")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.ge(vm.get("v3"), vm.get("v14"))) e.abort(String.format("!((v3=%s) ge (v14=%s))", vm.get("v3"), vm.get("v14")));
    if (!em.lt(vm.get("v7"), vm.get("v14"))) e.abort(String.format("!((v7=%s) lt (v14=%s))", vm.get("v7"), vm.get("v14")));
    if (!em.ne(vm.get("v7"), vm.get("v14"))) e.abort(String.format("!((v7=%s) ne (v14=%s))", vm.get("v7"), vm.get("v14")));
    if (!em.ne(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) ne (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.ge(vm.get("v11"), vm.get("v14"))) e.abort(String.format("!((v11=%s) ge (v14=%s))", vm.get("v11"), vm.get("v14")));
    if (!em.ge(vm.get("v3"), vm.get("v16"))) e.abort(String.format("!((v3=%s) ge (v16=%s))", vm.get("v3"), vm.get("v16")));
    if (!em.ne(vm.get("v3"), vm.get("v14"))) e.abort(String.format("!((v3=%s) ne (v14=%s))", vm.get("v3"), vm.get("v14")));
    if (!em.lt(vm.get("v9"), vm.get("v14"))) e.abort(String.format("!((v9=%s) lt (v14=%s))", vm.get("v9"), vm.get("v14")));
  }
  @IM(clazz="java.util.ArrayList", name="<init>", desc="(I)V",definedOutput={"this.elementData","this.modCount"},delta={"this.elementData","this.modCount"})
  @DG(succ={"retainAll1318","remove1111","add4859","sort755","remove1190","removeRange3485","remove1199","add4896","sort766","removeRange3497","clear4269","clear4299","remove1158","add4893","sort777","clear4256","sort781","fastRemove383","removeAll192","removeRange3419","fastRemove325","sort785","remove3390","trimToSize3697","remove3397","add4848","removeRange3489","sort793","add4895","sort779","remove3393","sort728","fastRemove386","add4833","sort791","removeRange3445","ensureCapacityInternal3589","removeRange3477","sort787","set3299","sort729","remove1117","sort757","add4897","retainAll1392","removeRange3417","removeAll146","removeAll173","retainAll1357","fastRemove332","add4886","removeAll182","add2989","sort741","sort745","removeAll124","add2994","remove3351","add2983","batchRemove4464","fastRemove321","remove1176","sort751","retainAll1398","sort798","sort735","batchRemove4491","removeAll181","remove1198","batchRemove4447","removeRange3490","sort722","remove3399","batchRemove4449","sort786","removeRange3441","add4852","removeRange3460","removeRange3438","removeRange3468","removeRange3416","retainAll1374","removeRange3480","removeAll152","batchRemove4481","clear4289","batchRemove4470","add4875","batchRemove4498","sort743","removeAll177","batchRemove4466","remove1145","removeRange3481","sort782","add4898","sort790","add4861","removeRange3484","remove3394","remove1116","clear4286","removeRange3423","sort732","remove3391","removeRange3407","batchRemove4421","retainAll1389","sort736","sort799","removeRange3494","add4828","sort747","sort770","remove3398","removeRange3499","fastRemove356","sort776"})
  static void _init_1497(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "elementData"));
    vm.put("v1", om.popField(vm.get("this"), "modCount"));
    vm.put("v2", (int)0);
    vm.put("v3", vm.get("v0"));
    vm.put("v4", om.getArrayLength(vm.get("v3")));
    vm.put("param0", vm.get("v4"));
    if (!em.eq(vm.get("v2"), (vm.get("v1")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v1")));
    if (!em.gt(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) gt (v2=%s))", vm.get("param0"), vm.get("v2")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort786(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("java.util.TimSort.$assertionsDisabled"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayLength(vm.get("v7")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", (int)0);
    vm.put("v13", (int)32);
    vm.put("v14", (int)2);
    vm.put("v15", null);
    vm.put("v16", em.op("isub").eval(vm.get("v3"),vm.get("v12")));
    vm.put("v17", vm.get("v0"));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v18"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.eq(vm.get("v5"), vm.get("v12"))) e.abort(String.format("!((v5=%s) eq (v12=%s))", vm.get("v5"), vm.get("v12")));
    if (!em.le(vm.get("v12"), vm.get("v3"))) e.abort(String.format("!((v12=%s) le (v3=%s))", vm.get("v12"), vm.get("v3")));
    if (!em.le(vm.get("v12"), vm.get("v3"))) e.abort(String.format("!((v12=%s) le (v3=%s))", vm.get("v12"), vm.get("v3")));
    if (!em.eq(vm.get("v1"), vm.get("v3"))) e.abort(String.format("!((v1=%s) eq (v3=%s))", vm.get("v1"), vm.get("v3")));
    if (!em.lt(vm.get("v16"), vm.get("v13"))) e.abort(String.format("!((v16=%s) lt (v13=%s))", vm.get("v16"), vm.get("v13")));
    if (!em.lt(vm.get("v12"), vm.get("v3"))) e.abort(String.format("!((v12=%s) lt (v3=%s))", vm.get("v12"), vm.get("v3")));
    if (!em.le(vm.get("v3"), vm.get("v9"))) e.abort(String.format("!((v3=%s) le (v9=%s))", vm.get("v3"), vm.get("v9")));
    if (!em.ne(vm.get("v7"), vm.get("v15"))) e.abort(String.format("!((v7=%s) ne (v15=%s))", vm.get("v7"), vm.get("v15")));
    if (!em.eq(vm.get("v11"), vm.get("v12"))) e.abort(String.format("!((v11=%s) eq (v12=%s))", vm.get("v11"), vm.get("v12")));
    if (!em.ge(vm.get("v16"), vm.get("v14"))) e.abort(String.format("!((v16=%s) ge (v14=%s))", vm.get("v16"), vm.get("v14")));
    if (!em.le(vm.get("v3"), vm.get("v9"))) e.abort(String.format("!((v3=%s) le (v9=%s))", vm.get("v3"), vm.get("v9")));
    if (!em.ne(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) ne (v15=%s))", vm.get("param0"), vm.get("v15")));
    if (!em.eq(vm.get("v5"), vm.get("v12"))) e.abort(String.format("!((v5=%s) eq (v12=%s))", vm.get("v5"), vm.get("v12")));
    if (!em.ge(vm.get("v1"), vm.get("v3"))) e.abort(String.format("!((v1=%s) ge (v3=%s))", vm.get("v1"), vm.get("v3")));
    if (!em.ne(vm.get("v5"), vm.get("v12"))) e.abort(String.format("!((v5=%s) ne (v12=%s))", vm.get("v5"), vm.get("v12")));
    if (!em.ne(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) ne (v15=%s))", vm.get("param0"), vm.get("v15")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove1111(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)4);
    vm.put("v1", (int)3);
    vm.put("v2", (int)0);
    vm.put("v3", (int)2);
    vm.put("v4", (int)1);
    vm.put("v5", null);
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", om.getField(vm.get("this"), "elementData"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getArrayElement(vm.get("v10"), vm.get("v1")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getArrayElement(vm.get("v10"), vm.get("v2")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.getArrayElement(vm.get("v10"), vm.get("v3")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.popArrayElement(vm.get("v10"), vm.get("v7")));
    vm.put("v18", om.getArrayElement(vm.get("v10"), vm.get("v0")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", om.getArrayElement(vm.get("v10"), vm.get("v4")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", vm.get("v8"));
    vm.put("v23", em.op("isub").eval(vm.get("v22"),vm.get("v4")));
    om.revertField(vm.get("this"), "modCount", vm.get("v23"));
    vm.put("v24", em.op("iadd").eval(vm.get("v7"),vm.get("v4")));
    om.revertField(vm.get("this"), "size", vm.get("v24"));
    vm.put("v25", em.op("isub").eval(vm.get("v24"),vm.get("v0")));
    vm.put("v26", em.op("isub").eval(vm.get("v25"),vm.get("v4")));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.ne(vm.get("v14"), vm.get("v5"))) e.abort(String.format("!((v14=%s) ne (v5=%s))", vm.get("v14"), vm.get("v5")));
    if (!em.lt(vm.get("v2"), vm.get("v24"))) e.abort(String.format("!((v2=%s) lt (v24=%s))", vm.get("v2"), vm.get("v24")));
    if (!em.ne(vm.get("v21"), vm.get("v5"))) e.abort(String.format("!((v21=%s) ne (v5=%s))", vm.get("v21"), vm.get("v5")));
    if (!em.ne(vm.get("v12"), vm.get("v5"))) e.abort(String.format("!((v12=%s) ne (v5=%s))", vm.get("v12"), vm.get("v5")));
    if (!em.lt(vm.get("v1"), vm.get("v24"))) e.abort(String.format("!((v1=%s) lt (v24=%s))", vm.get("v1"), vm.get("v24")));
    if (!em.ne(vm.get("v16"), vm.get("v5"))) e.abort(String.format("!((v16=%s) ne (v5=%s))", vm.get("v16"), vm.get("v5")));
    if (!em.lt(vm.get("v4"), vm.get("v24"))) e.abort(String.format("!((v4=%s) lt (v24=%s))", vm.get("v4"), vm.get("v24")));
    if (!em.le(vm.get("v26"), vm.get("v2"))) e.abort(String.format("!((v26=%s) le (v2=%s))", vm.get("v26"), vm.get("v2")));
    if (!em.lt(vm.get("v0"), vm.get("v24"))) e.abort(String.format("!((v0=%s) lt (v24=%s))", vm.get("v0"), vm.get("v24")));
    if (!em.eq(vm.get("v19"), vm.get("v5"))) e.abort(String.format("!((v19=%s) eq (v5=%s))", vm.get("v19"), vm.get("v5")));
    if (!em.eq(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) eq (v5=%s))", vm.get("param0"), vm.get("v5")));
    if (!em.lt(vm.get("v3"), vm.get("v24"))) e.abort(String.format("!((v3=%s) lt (v24=%s))", vm.get("v3"), vm.get("v24")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort747(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "size"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "elementData"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getArrayLength(vm.get("v8")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)7);
    vm.put("v12", (int)2);
    vm.put("v13", java.lang.Object[].class);
    vm.put("v14", null);
    vm.put("v15", om.getArrayElement(vm.get("v8"), vm.get("v1")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", em.op("isub").eval(vm.get("v6"),vm.get("v1")));
    vm.put("v18", om.getArrayElement(vm.get("v8"), vm.get("v0")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", em.op("isub").eval(vm.get("v6"),vm.get("v1")));
    vm.put("v21", em.op("isub").eval(vm.get("v10"),vm.get("v1")));
    vm.put("v22", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v8")));
    vm.put("v23", vm.get("v4"));
    vm.put("v24", em.op("isub").eval(vm.get("v23"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v24"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.le(vm.get("v6"), vm.get("v10"))) e.abort(String.format("!((v6=%s) le (v10=%s))", vm.get("v6"), vm.get("v10")));
    if (!em.eq(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) eq (v14=%s))", vm.get("param0"), vm.get("v14")));
    if (!em.lt(vm.get("v20"), vm.get("v11"))) e.abort(String.format("!((v20=%s) lt (v11=%s))", vm.get("v20"), vm.get("v11")));
    if (!em.ge(vm.get("v1"), vm.get("v6"))) e.abort(String.format("!((v1=%s) ge (v6=%s))", vm.get("v1"), vm.get("v6")));
    if (!em.ne(vm.get("v3"), vm.get("v1"))) e.abort(String.format("!((v3=%s) ne (v1=%s))", vm.get("v3"), vm.get("v1")));
    if (!em.ge(vm.get("v17"), vm.get("v1"))) e.abort(String.format("!((v17=%s) ge (v1=%s))", vm.get("v17"), vm.get("v1")));
    if (!em.le(vm.get("v1"), vm.get("v6"))) e.abort(String.format("!((v1=%s) le (v6=%s))", vm.get("v1"), vm.get("v6")));
    if (!em.lt(vm.get("v1"), vm.get("v17"))) e.abort(String.format("!((v1=%s) lt (v17=%s))", vm.get("v1"), vm.get("v17")));
    if (!em.ge(vm.get("v12"), vm.get("v17"))) e.abort(String.format("!((v12=%s) ge (v17=%s))", vm.get("v12"), vm.get("v17")));
    if (!em.gt(vm.get("v21"), vm.get("v17"))) e.abort(String.format("!((v21=%s) gt (v17=%s))", vm.get("v21"), vm.get("v17")));
    if (!em.lt(vm.get("v0"), vm.get("v17"))) e.abort(String.format("!((v0=%s) lt (v17=%s))", vm.get("v0"), vm.get("v17")));
    if (!em.eq(vm.get("v22"), vm.get("v13"))) e.abort(String.format("!((v22=%s) eq (v13=%s))", vm.get("v22"), vm.get("v13")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove3391(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", (int)2);
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", null);
    vm.put("v8", (int)3);
    vm.put("v9", vm.get("v3"));
    vm.put("v10", em.op("isub").eval(vm.get("v9"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v10"));
    vm.put("v11", vm.get("v4"));
    vm.put("v12", om.popArrayElement(vm.get("v6"), vm.get("v11")));
    vm.put("v13", em.op("iadd").eval(vm.get("v11"),vm.get("v0")));
    om.revertField(vm.get("this"), "size", vm.get("v13"));
    vm.put("v14", om.guessArrayIndex(vm.get("v6")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.popArrayElement(vm.get("v6"), vm.get("v15")));
    vm.put("param0", em.op("isub").eval(vm.get("v15"),vm.get("v1")));
    vm.put("v17", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v18", om.getArrayElement(vm.get("v6"), vm.get("param0")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", em.op("iadd").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v21", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v22", om.popArrayElement(vm.get("v6"), vm.get("v21")));
    vm.put("v23", em.op("iadd").eval(vm.get("v17"),vm.get("v1")));
    vm.put("v24", om.popArrayElement(vm.get("v6"), vm.get("v20")));
    vm.put("v25", om.getArrayElement(vm.get("v6"), vm.get("v23")));
    vm.put("v26", vm.get("v25"));
    vm.put("v27", em.op("isub").eval(vm.get("v13"),vm.get("param0")));
    vm.put("v28", em.op("isub").eval(vm.get("v27"),vm.get("v0")));
    vm.put("v29", em.op("iadd").eval(vm.get("v17"),vm.get("v2")));
    vm.put("v30", om.getArrayElement(vm.get("v6"), vm.get("v29")));
    vm.put("v31", vm.get("v30"));
    vm.put("v32", em.op("iadd").eval(vm.get("v17"),vm.get("v0")));
    vm.put("v33", om.getArrayElement(vm.get("v6"), vm.get("v32")));
    vm.put("v34", vm.get("v33"));
    if (!em.lt(vm.get("v0"), vm.get("v28"))) e.abort(String.format("!((v0=%s) lt (v28=%s))", vm.get("v0"), vm.get("v28")));
    if (!em.ge(vm.get("v8"), vm.get("v28"))) e.abort(String.format("!((v8=%s) ge (v28=%s))", vm.get("v8"), vm.get("v28")));
    if (!em.lt(vm.get("v1"), vm.get("v28"))) e.abort(String.format("!((v1=%s) lt (v28=%s))", vm.get("v1"), vm.get("v28")));
    if (!em.lt(vm.get("v2"), vm.get("v28"))) e.abort(String.format("!((v2=%s) lt (v28=%s))", vm.get("v2"), vm.get("v28")));
    if (!em.lt(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) lt (v13=%s))", vm.get("param0"), vm.get("v13")));
    if (!em.gt(vm.get("v28"), vm.get("v1"))) e.abort(String.format("!((v28=%s) gt (v1=%s))", vm.get("v28"), vm.get("v1")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort793(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getArrayLength(vm.get("v3")));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "size"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)7);
    vm.put("v12", null);
    vm.put("v13", java.lang.Object[].class);
    vm.put("v14", em.op("isub").eval(vm.get("v9"),vm.get("v10")));
    vm.put("v15", em.op("isub").eval(vm.get("v5"),vm.get("v10")));
    vm.put("v16", em.op("isub").eval(vm.get("v9"),vm.get("v10")));
    vm.put("v17", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v3")));
    vm.put("v18", vm.get("v0"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.eq(vm.get("v17"), vm.get("v13"))) e.abort(String.format("!((v17=%s) eq (v13=%s))", vm.get("v17"), vm.get("v13")));
    if (!em.lt(vm.get("v14"), vm.get("v11"))) e.abort(String.format("!((v14=%s) lt (v11=%s))", vm.get("v14"), vm.get("v11")));
    if (!em.ge(vm.get("v10"), vm.get("v16"))) e.abort(String.format("!((v10=%s) ge (v16=%s))", vm.get("v10"), vm.get("v16")));
    if (!em.ne(vm.get("v7"), vm.get("v10"))) e.abort(String.format("!((v7=%s) ne (v10=%s))", vm.get("v7"), vm.get("v10")));
    if (!em.le(vm.get("v9"), vm.get("v5"))) e.abort(String.format("!((v9=%s) le (v5=%s))", vm.get("v9"), vm.get("v5")));
    if (!em.ge(vm.get("v10"), vm.get("v9"))) e.abort(String.format("!((v10=%s) ge (v9=%s))", vm.get("v10"), vm.get("v9")));
    if (!em.eq(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) eq (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.le(vm.get("v10"), vm.get("v9"))) e.abort(String.format("!((v10=%s) le (v9=%s))", vm.get("v10"), vm.get("v9")));
    if (!em.gt(vm.get("v15"), vm.get("v16"))) e.abort(String.format("!((v15=%s) gt (v16=%s))", vm.get("v15"), vm.get("v16")));
    if (!em.ge(vm.get("v16"), vm.get("v10"))) e.abort(String.format("!((v16=%s) ge (v10=%s))", vm.get("v16"), vm.get("v10")));
  }
  @IM(clazz="java.util.ArrayList", name="replaceAll", desc="(Ljava/util/function/UnaryOperator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  static void replaceAll2499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", (int)0);
    vm.put("v5", null);
    vm.put("v6", vm.get("v2"));
    vm.put("v7", em.op("isub").eval(vm.get("v6"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v7"));
    vm.put("param0", om.newDefaultValue("java.util.function.UnaryOperator"));
    if (!em.ge(vm.get("v4"), vm.get("v1"))) e.abort(String.format("!((v4=%s) ge (v1=%s))", vm.get("v4"), vm.get("v1")));
    if (!em.ne(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) ne (v5=%s))", vm.get("param0"), vm.get("v5")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort732(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", (int)1);
    vm.put("v8", (int)0);
    vm.put("v9", (int)7);
    vm.put("v10", java.lang.Object[].class);
    vm.put("v11", null);
    vm.put("v12", em.op("isub").eval(vm.get("v1"),vm.get("v8")));
    vm.put("v13", om.getArrayLength(vm.get("v3")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v1"),vm.get("v8")));
    vm.put("v16", em.op("isub").eval(vm.get("v14"),vm.get("v8")));
    vm.put("v17", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v3")));
    vm.put("v18", vm.get("v6"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v7")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.lt(vm.get("v8"), vm.get("v1"))) e.abort(String.format("!((v8=%s) lt (v1=%s))", vm.get("v8"), vm.get("v1")));
    if (!em.lt(vm.get("v15"), vm.get("v9"))) e.abort(String.format("!((v15=%s) lt (v9=%s))", vm.get("v15"), vm.get("v9")));
    if (!em.le(vm.get("v8"), vm.get("v1"))) e.abort(String.format("!((v8=%s) le (v1=%s))", vm.get("v8"), vm.get("v1")));
    if (!em.ge(vm.get("v7"), vm.get("v1"))) e.abort(String.format("!((v7=%s) ge (v1=%s))", vm.get("v7"), vm.get("v1")));
    if (!em.ge(vm.get("v8"), vm.get("v16"))) e.abort(String.format("!((v8=%s) ge (v16=%s))", vm.get("v8"), vm.get("v16")));
    if (!em.le(vm.get("v16"), vm.get("v12"))) e.abort(String.format("!((v16=%s) le (v12=%s))", vm.get("v16"), vm.get("v12")));
    if (!em.ge(vm.get("v12"), vm.get("v8"))) e.abort(String.format("!((v12=%s) ge (v8=%s))", vm.get("v12"), vm.get("v8")));
    if (!em.eq(vm.get("param0"), vm.get("v11"))) e.abort(String.format("!((param0=%s) eq (v11=%s))", vm.get("param0"), vm.get("v11")));
    if (!em.eq(vm.get("v17"), vm.get("v10"))) e.abort(String.format("!((v17=%s) eq (v10=%s))", vm.get("v17"), vm.get("v10")));
    if (!em.le(vm.get("v1"), vm.get("v14"))) e.abort(String.format("!((v1=%s) le (v14=%s))", vm.get("v1"), vm.get("v14")));
    if (!em.ne(vm.get("v5"), vm.get("v8"))) e.abort(String.format("!((v5=%s) ne (v8=%s))", vm.get("v5"), vm.get("v8")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add4852(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", (int)2);
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getArrayLength(vm.get("v5")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("this"), "size"));
    vm.put("v11", (int)3);
    vm.put("v12", (int)10);
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v7")));
    vm.put("v14", vm.get("v3"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    vm.put("v16", vm.get("v10"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v17"));
    vm.put("v18", em.op("iadd").eval(vm.get("v17"),vm.get("v1")));
    vm.put("v19", om.guessArrayIndex(vm.get("v5")));
    vm.put("v20", vm.get("v19"));
    vm.put("v21", om.getArrayElement(vm.get("v5"), vm.get("v20")));
    vm.put("v22", vm.get("v21"));
    vm.put("param0", em.op("isub").eval(vm.get("v20"),vm.get("v0")));
    vm.put("v23", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v24", em.op("iadd").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v25", om.popArrayElement(vm.get("v5"), vm.get("param0")));
    vm.put("param1", vm.get("v25"));
    vm.put("v26", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v27", em.op("iadd").eval(vm.get("v26"),vm.get("v2")));
    vm.put("v28", om.popArrayElement(vm.get("v5"), vm.get("v27")));
    vm.put("v29", vm.get("v28"));
    vm.put("v30", em.op("iadd").eval(vm.get("v26"),vm.get("v1")));
    vm.put("v31", em.op("isub").eval(vm.get("v17"),vm.get("param0")));
    vm.put("v32", om.getArrayElement(vm.get("v5"), vm.get("v24")));
    vm.put("v33", om.popArrayElement(vm.get("v5"), vm.get("v30")));
    vm.put("v34", vm.get("v33"));
    vm.put("v35", om.getArrayElement(vm.get("v5"), vm.get("v23")));
    vm.put("v36", em.op("iadd").eval(vm.get("v26"),vm.get("v0")));
    vm.put("v37", om.popArrayElement(vm.get("v5"), vm.get("v36")));
    if (!em.le(vm.get("v13"), vm.get("v0"))) e.abort(String.format("!((v13=%s) le (v0=%s))", vm.get("v13"), vm.get("v0")));
    if (!em.lt(vm.get("v0"), vm.get("v31"))) e.abort(String.format("!((v0=%s) lt (v31=%s))", vm.get("v0"), vm.get("v31")));
    if (!em.ge(vm.get("param0"), vm.get("v0"))) e.abort(String.format("!((param0=%s) ge (v0=%s))", vm.get("param0"), vm.get("v0")));
    if (!em.eq(vm.get("v5"), vm.get("v9"))) e.abort(String.format("!((v5=%s) eq (v9=%s))", vm.get("v5"), vm.get("v9")));
    if (!em.lt(vm.get("v1"), vm.get("v31"))) e.abort(String.format("!((v1=%s) lt (v31=%s))", vm.get("v1"), vm.get("v31")));
    if (!em.ge(vm.get("v11"), vm.get("v31"))) e.abort(String.format("!((v11=%s) ge (v31=%s))", vm.get("v11"), vm.get("v31")));
    if (!em.lt(vm.get("v2"), vm.get("v31"))) e.abort(String.format("!((v2=%s) lt (v31=%s))", vm.get("v2"), vm.get("v31")));
    if (!em.ge(vm.get("v12"), vm.get("v18"))) e.abort(String.format("!((v12=%s) ge (v18=%s))", vm.get("v12"), vm.get("v18")));
    if (!em.le(vm.get("param0"), vm.get("v17"))) e.abort(String.format("!((param0=%s) le (v17=%s))", vm.get("param0"), vm.get("v17")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove3390(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)2);
    vm.put("v2", (int)0);
    vm.put("v3", (int)3);
    vm.put("v4", (int)4);
    vm.put("v5", null);
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", om.getField(vm.get("this"), "elementData"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)5);
    vm.put("v12", om.popArrayElement(vm.get("v10"), vm.get("v7")));
    vm.put("v13", em.op("iadd").eval(vm.get("v7"),vm.get("v0")));
    om.revertField(vm.get("this"), "size", vm.get("v13"));
    vm.put("v14", vm.get("v8"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    vm.put("v16", om.guessArrayIndex(vm.get("v10")));
    vm.put("param0", vm.get("v16"));
    vm.put("v17", om.getArrayElement(vm.get("v10"), vm.get("param0")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v20", em.op("iadd").eval(vm.get("param0"),vm.get("v4")));
    vm.put("v21", em.op("iadd").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v22", om.popArrayElement(vm.get("v10"), vm.get("v21")));
    vm.put("v23", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v24", om.popArrayElement(vm.get("v10"), vm.get("v20")));
    vm.put("v25", em.op("isub").eval(vm.get("v13"),vm.get("param0")));
    vm.put("v26", em.op("isub").eval(vm.get("v25"),vm.get("v0")));
    vm.put("v27", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v28", em.op("iadd").eval(vm.get("v27"),vm.get("v2")));
    vm.put("v29", om.getArrayElement(vm.get("v10"), vm.get("v28")));
    vm.put("v30", vm.get("v29"));
    vm.put("v31", em.op("iadd").eval(vm.get("v27"),vm.get("v1")));
    vm.put("v32", em.op("iadd").eval(vm.get("v27"),vm.get("v0")));
    vm.put("v33", om.getArrayElement(vm.get("v10"), vm.get("v32")));
    vm.put("v34", vm.get("v33"));
    vm.put("v35", em.op("iadd").eval(vm.get("param0"),vm.get("v3")));
    vm.put("v36", om.getArrayElement(vm.get("v10"), vm.get("v31")));
    vm.put("v37", vm.get("v36"));
    vm.put("v38", om.popArrayElement(vm.get("v10"), vm.get("v23")));
    vm.put("v39", om.popArrayElement(vm.get("v10"), vm.get("v19")));
    vm.put("v40", em.op("iadd").eval(vm.get("v27"),vm.get("v4")));
    vm.put("v41", om.getArrayElement(vm.get("v10"), vm.get("v40")));
    vm.put("v42", vm.get("v41"));
    vm.put("v43", om.popArrayElement(vm.get("v10"), vm.get("v35")));
    vm.put("v44", em.op("iadd").eval(vm.get("v27"),vm.get("v3")));
    vm.put("v45", om.getArrayElement(vm.get("v10"), vm.get("v44")));
    vm.put("v46", vm.get("v45"));
    if (!em.lt(vm.get("v2"), vm.get("v26"))) e.abort(String.format("!((v2=%s) lt (v26=%s))", vm.get("v2"), vm.get("v26")));
    if (!em.gt(vm.get("v26"), vm.get("v2"))) e.abort(String.format("!((v26=%s) gt (v2=%s))", vm.get("v26"), vm.get("v2")));
    if (!em.lt(vm.get("v1"), vm.get("v26"))) e.abort(String.format("!((v1=%s) lt (v26=%s))", vm.get("v1"), vm.get("v26")));
    if (!em.ge(vm.get("v11"), vm.get("v26"))) e.abort(String.format("!((v11=%s) ge (v26=%s))", vm.get("v11"), vm.get("v26")));
    if (!em.lt(vm.get("v0"), vm.get("v26"))) e.abort(String.format("!((v0=%s) lt (v26=%s))", vm.get("v0"), vm.get("v26")));
    if (!em.lt(vm.get("v4"), vm.get("v26"))) e.abort(String.format("!((v4=%s) lt (v26=%s))", vm.get("v4"), vm.get("v26")));
    if (!em.lt(vm.get("v3"), vm.get("v26"))) e.abort(String.format("!((v3=%s) lt (v26=%s))", vm.get("v3"), vm.get("v26")));
    if (!em.lt(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) lt (v13=%s))", vm.get("param0"), vm.get("v13")));
  }
  @IM(clazz="java.util.ArrayList", name="clear", desc="()V",revertedInput={"this.size","this.modCount"},definedOutput={"this.size","this.modCount"})
  static void clear4297(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", vm.get("v2"));
    vm.put("v5", em.op("isub").eval(vm.get("v4"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v5"));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    om.revertField(vm.get("this"), "size");
    vm.put("v6", om.getField(vm.get("this"), "size"));
    if (!em.ge(vm.get("v1"), vm.get("v6"))) e.abort(String.format("!((v1=%s) ge (v6=%s))", vm.get("v1"), vm.get("v6")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort791(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", (int)1);
    vm.put("v8", om.getStatic("java.util.ComparableTimSort.$assertionsDisabled"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)32);
    vm.put("v12", (int)2);
    vm.put("v13", null);
    vm.put("v14", om.getArrayLength(vm.get("v5")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", em.op("isub").eval(vm.get("v1"),vm.get("v10")));
    vm.put("v17", vm.get("v6"));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v7")));
    om.revertField(vm.get("this"), "modCount", vm.get("v18"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.le(vm.get("v10"), vm.get("v1"))) e.abort(String.format("!((v10=%s) le (v1=%s))", vm.get("v10"), vm.get("v1")));
    if (!em.ne(vm.get("v9"), vm.get("v10"))) e.abort(String.format("!((v9=%s) ne (v10=%s))", vm.get("v9"), vm.get("v10")));
    if (!em.lt(vm.get("v16"), vm.get("v11"))) e.abort(String.format("!((v16=%s) lt (v11=%s))", vm.get("v16"), vm.get("v11")));
    if (!em.eq(vm.get("v7"), vm.get("v1"))) e.abort(String.format("!((v7=%s) eq (v1=%s))", vm.get("v7"), vm.get("v1")));
    if (!em.ge(vm.get("v16"), vm.get("v12"))) e.abort(String.format("!((v16=%s) ge (v12=%s))", vm.get("v16"), vm.get("v12")));
    if (!em.eq(vm.get("v9"), vm.get("v10"))) e.abort(String.format("!((v9=%s) eq (v10=%s))", vm.get("v9"), vm.get("v10")));
    if (!em.eq(vm.get("v3"), vm.get("v10"))) e.abort(String.format("!((v3=%s) eq (v10=%s))", vm.get("v3"), vm.get("v10")));
    if (!em.le(vm.get("v10"), vm.get("v1"))) e.abort(String.format("!((v10=%s) le (v1=%s))", vm.get("v10"), vm.get("v1")));
    if (!em.ne(vm.get("v9"), vm.get("v10"))) e.abort(String.format("!((v9=%s) ne (v10=%s))", vm.get("v9"), vm.get("v10")));
    if (!em.le(vm.get("v1"), vm.get("v15"))) e.abort(String.format("!((v1=%s) le (v15=%s))", vm.get("v1"), vm.get("v15")));
    if (!em.ge(vm.get("v7"), vm.get("v1"))) e.abort(String.format("!((v7=%s) ge (v1=%s))", vm.get("v7"), vm.get("v1")));
    if (!em.le(vm.get("v1"), vm.get("v15"))) e.abort(String.format("!((v1=%s) le (v15=%s))", vm.get("v1"), vm.get("v15")));
    if (!em.ne(vm.get("v5"), vm.get("v13"))) e.abort(String.format("!((v5=%s) ne (v13=%s))", vm.get("v5"), vm.get("v13")));
    if (!em.eq(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) eq (v13=%s))", vm.get("param0"), vm.get("v13")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort782(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getArrayLength(vm.get("v3")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)7);
    vm.put("v12", java.lang.Object[].class);
    vm.put("v13", null);
    vm.put("v14", em.op("isub").eval(vm.get("v7"),vm.get("v10")));
    vm.put("v15", em.op("isub").eval(vm.get("v5"),vm.get("v10")));
    vm.put("v16", em.op("isub").eval(vm.get("v5"),vm.get("v10")));
    vm.put("v17", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v3")));
    vm.put("v18", vm.get("v0"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.le(vm.get("v5"), vm.get("v7"))) e.abort(String.format("!((v5=%s) le (v7=%s))", vm.get("v5"), vm.get("v7")));
    if (!em.gt(vm.get("v14"), vm.get("v15"))) e.abort(String.format("!((v14=%s) gt (v15=%s))", vm.get("v14"), vm.get("v15")));
    if (!em.ne(vm.get("v9"), vm.get("v10"))) e.abort(String.format("!((v9=%s) ne (v10=%s))", vm.get("v9"), vm.get("v10")));
    if (!em.le(vm.get("v10"), vm.get("v5"))) e.abort(String.format("!((v10=%s) le (v5=%s))", vm.get("v10"), vm.get("v5")));
    if (!em.ge(vm.get("v15"), vm.get("v10"))) e.abort(String.format("!((v15=%s) ge (v10=%s))", vm.get("v15"), vm.get("v10")));
    if (!em.eq(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) eq (v13=%s))", vm.get("param0"), vm.get("v13")));
    if (!em.eq(vm.get("v17"), vm.get("v12"))) e.abort(String.format("!((v17=%s) eq (v12=%s))", vm.get("v17"), vm.get("v12")));
    if (!em.ge(vm.get("v10"), vm.get("v15"))) e.abort(String.format("!((v10=%s) ge (v15=%s))", vm.get("v10"), vm.get("v15")));
    if (!em.lt(vm.get("v16"), vm.get("v11"))) e.abort(String.format("!((v16=%s) lt (v11=%s))", vm.get("v16"), vm.get("v11")));
    if (!em.ge(vm.get("v10"), vm.get("v5"))) e.abort(String.format("!((v10=%s) ge (v5=%s))", vm.get("v10"), vm.get("v5")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add4898(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getArrayLength(vm.get("v3")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "size"));
    vm.put("v9", (int)0);
    vm.put("v10", (int)10);
    vm.put("v11", vm.get("v8"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v5")));
    om.revertField(vm.get("this"), "size", vm.get("v12"));
    vm.put("v13", em.op("iadd").eval(vm.get("v12"),vm.get("v5")));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v7")));
    vm.put("v15", vm.get("v4"));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v5")));
    om.revertField(vm.get("this"), "modCount", vm.get("v16"));
    vm.put("v17", om.guessArrayIndex(vm.get("v3")));
    vm.put("param0", vm.get("v17"));
    vm.put("v18", om.popArrayElement(vm.get("v3"), vm.get("param0")));
    vm.put("param1", vm.get("v18"));
    vm.put("v19", em.op("isub").eval(vm.get("v12"),vm.get("param0")));
    if (!em.lt(vm.get("v10"), vm.get("v13"))) e.abort(String.format("!((v10=%s) lt (v13=%s))", vm.get("v10"), vm.get("v13")));
    if (!em.le(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) le (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ge(vm.get("v9"), vm.get("v19"))) e.abort(String.format("!((v9=%s) ge (v19=%s))", vm.get("v9"), vm.get("v19")));
    if (!em.le(vm.get("v14"), vm.get("v9"))) e.abort(String.format("!((v14=%s) le (v9=%s))", vm.get("v14"), vm.get("v9")));
    if (!em.eq(vm.get("v3"), vm.get("v1"))) e.abort(String.format("!((v3=%s) eq (v1=%s))", vm.get("v3"), vm.get("v1")));
    if (!em.ge(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) ge (v9=%s))", vm.get("param0"), vm.get("v9")));
  }
  @IM(clazz="java.util.ArrayList", name="removeAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.elementData[*]"},definedOutput={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void removeAll192(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    vm.put("v7", vm.get("v6"));
    om.revertArrayElement(vm.get("v3"), vm.get("v0"), vm.get("v7"));
    vm.put("v8", (int)2);
    vm.put("v9", null);
    vm.put("v10", em.op("isub").eval(vm.get("v5"),vm.get("v1")));
    vm.put("v11", em.op("iadd").eval(vm.get("v1"),vm.get("v10")));
    vm.put("v12", om.getArrayElement(vm.get("v3"), vm.get("v1")));
    vm.put("v13", vm.get("v12"));
    om.revertArrayElement(vm.get("v3"), vm.get("v1"), vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v5"),vm.get("v1")));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.eq(vm.get("v11"), vm.get("v5"))) e.abort(String.format("!((v11=%s) eq (v5=%s))", vm.get("v11"), vm.get("v5")));
    if (!em.lt(vm.get("v0"), vm.get("v14"))) e.abort(String.format("!((v0=%s) lt (v14=%s))", vm.get("v0"), vm.get("v14")));
    if (!em.ge(vm.get("v1"), vm.get("v5"))) e.abort(String.format("!((v1=%s) ge (v5=%s))", vm.get("v1"), vm.get("v5")));
    if (!em.ne(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) ne (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.ge(vm.get("v8"), vm.get("v14"))) e.abort(String.format("!((v8=%s) ge (v14=%s))", vm.get("v8"), vm.get("v14")));
    if (!em.ne(vm.get("v1"), vm.get("v5"))) e.abort(String.format("!((v1=%s) ne (v5=%s))", vm.get("v1"), vm.get("v5")));
    if (!em.lt(vm.get("v1"), vm.get("v14"))) e.abort(String.format("!((v1=%s) lt (v14=%s))", vm.get("v1"), vm.get("v14")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add4875(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", (int)2);
    vm.put("v3", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "size"));
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", om.getField(vm.get("this"), "elementData"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", (int)3);
    vm.put("v10", (int)10);
    vm.put("v11", om.getArrayLength(vm.get("v8")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", vm.get("v6"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    vm.put("v15", vm.get("v5"));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v0")));
    om.revertField(vm.get("this"), "size", vm.get("v16"));
    vm.put("v17", em.op("iadd").eval(vm.get("v16"),vm.get("v0")));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v12")));
    vm.put("v19", om.guessArrayIndex(vm.get("v8")));
    vm.put("v20", vm.get("v19"));
    vm.put("v21", om.getArrayElement(vm.get("v8"), vm.get("v20")));
    vm.put("v22", vm.get("v21"));
    vm.put("param0", em.op("isub").eval(vm.get("v20"),vm.get("v2")));
    vm.put("v23", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v24", om.getArrayElement(vm.get("v8"), vm.get("v23")));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v27", om.getArrayElement(vm.get("v8"), vm.get("v26")));
    vm.put("v28", vm.get("v27"));
    vm.put("v29", om.popArrayElement(vm.get("v8"), vm.get("param0")));
    vm.put("param1", vm.get("v29"));
    vm.put("v30", em.op("isub").eval(vm.get("v16"),vm.get("param0")));
    vm.put("v31", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v32", em.op("iadd").eval(vm.get("v31"),vm.get("v2")));
    vm.put("v33", em.op("iadd").eval(vm.get("v31"),vm.get("v1")));
    vm.put("v34", om.popArrayElement(vm.get("v8"), vm.get("v33")));
    vm.put("v35", om.popArrayElement(vm.get("v8"), vm.get("v32")));
    vm.put("v36", em.op("iadd").eval(vm.get("v31"),vm.get("v0")));
    vm.put("v37", om.popArrayElement(vm.get("v8"), vm.get("v36")));
    if (!em.le(vm.get("param0"), vm.get("v16"))) e.abort(String.format("!((param0=%s) le (v16=%s))", vm.get("param0"), vm.get("v16")));
    if (!em.lt(vm.get("v0"), vm.get("v30"))) e.abort(String.format("!((v0=%s) lt (v30=%s))", vm.get("v0"), vm.get("v30")));
    if (!em.lt(vm.get("v1"), vm.get("v30"))) e.abort(String.format("!((v1=%s) lt (v30=%s))", vm.get("v1"), vm.get("v30")));
    if (!em.eq(vm.get("v8"), vm.get("v4"))) e.abort(String.format("!((v8=%s) eq (v4=%s))", vm.get("v8"), vm.get("v4")));
    if (!em.lt(vm.get("v2"), vm.get("v30"))) e.abort(String.format("!((v2=%s) lt (v30=%s))", vm.get("v2"), vm.get("v30")));
    if (!em.le(vm.get("v18"), vm.get("v1"))) e.abort(String.format("!((v18=%s) le (v1=%s))", vm.get("v18"), vm.get("v1")));
    if (!em.lt(vm.get("v10"), vm.get("v17"))) e.abort(String.format("!((v10=%s) lt (v17=%s))", vm.get("v10"), vm.get("v17")));
    if (!em.ge(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) ge (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.ge(vm.get("v9"), vm.get("v30"))) e.abort(String.format("!((v9=%s) ge (v30=%s))", vm.get("v9"), vm.get("v30")));
  }
  @IM(clazz="java.util.ArrayList", name="clear", desc="()V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void clear4289(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", null);
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "size"));
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", (int)2);
    vm.put("v8", om.popArrayElement(vm.get("v4"), vm.get("v0")));
    vm.put("v9", om.popArrayElement(vm.get("v4"), vm.get("v2")));
    if (!em.eq(vm.get("v0"), (vm.get("v5")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v5")));
    vm.put("v10", vm.get("v6"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    om.revertField(vm.get("this"), "size");
    vm.put("v12", om.getField(vm.get("this"), "size"));
    if (!em.lt(vm.get("v0"), vm.get("v12"))) e.abort(String.format("!((v0=%s) lt (v12=%s))", vm.get("v0"), vm.get("v12")));
    if (!em.ge(vm.get("v7"), vm.get("v12"))) e.abort(String.format("!((v7=%s) ge (v12=%s))", vm.get("v7"), vm.get("v12")));
    if (!em.lt(vm.get("v2"), vm.get("v12"))) e.abort(String.format("!((v2=%s) lt (v12=%s))", vm.get("v2"), vm.get("v12")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort790(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getStatic("java.util.ComparableTimSort.$assertionsDisabled"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "elementData"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)2);
    vm.put("v12", null);
    vm.put("v13", em.op("isub").eval(vm.get("v3"),vm.get("v10")));
    vm.put("v14", om.getArrayLength(vm.get("v9")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", vm.get("v4"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v5")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.eq(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) eq (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.le(vm.get("v3"), vm.get("v15"))) e.abort(String.format("!((v3=%s) le (v15=%s))", vm.get("v3"), vm.get("v15")));
    if (!em.le(vm.get("v10"), vm.get("v3"))) e.abort(String.format("!((v10=%s) le (v3=%s))", vm.get("v10"), vm.get("v3")));
    if (!em.le(vm.get("v10"), vm.get("v3"))) e.abort(String.format("!((v10=%s) le (v3=%s))", vm.get("v10"), vm.get("v3")));
    if (!em.ne(vm.get("v9"), vm.get("v12"))) e.abort(String.format("!((v9=%s) ne (v12=%s))", vm.get("v9"), vm.get("v12")));
    if (!em.le(vm.get("v3"), vm.get("v15"))) e.abort(String.format("!((v3=%s) le (v15=%s))", vm.get("v3"), vm.get("v15")));
    if (!em.eq(vm.get("v7"), vm.get("v10"))) e.abort(String.format("!((v7=%s) eq (v10=%s))", vm.get("v7"), vm.get("v10")));
    if (!em.lt(vm.get("v13"), vm.get("v11"))) e.abort(String.format("!((v13=%s) lt (v11=%s))", vm.get("v13"), vm.get("v11")));
    if (!em.eq(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) eq (v10=%s))", vm.get("v1"), vm.get("v10")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add4828(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayLength(vm.get("v3")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)2);
    vm.put("v11", (int)10);
    vm.put("v12", vm.get("v5"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    vm.put("v14", vm.get("v4"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    om.revertField(vm.get("this"), "size", vm.get("v15"));
    vm.put("v16", em.op("iadd").eval(vm.get("v15"),vm.get("v0")));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v9")));
    vm.put("v18", om.guessArrayIndex(vm.get("v3")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", om.popArrayElement(vm.get("v3"), vm.get("v19")));
    vm.put("v21", em.op("isub").eval(vm.get("v19"),vm.get("v1")));
    vm.put("v22", em.op("iadd").eval(vm.get("v21"),vm.get("v0")));
    vm.put("v23", om.popArrayElement(vm.get("v3"), vm.get("v22")));
    vm.put("param0", em.op("isub").eval(vm.get("v21"),vm.get("v0")));
    vm.put("v24", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v25", om.getArrayElement(vm.get("v3"), vm.get("v24")));
    vm.put("v26", vm.get("v25"));
    vm.put("v27", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v28", om.getArrayElement(vm.get("v3"), vm.get("v27")));
    vm.put("v29", vm.get("v28"));
    vm.put("v30", em.op("isub").eval(vm.get("v15"),vm.get("param0")));
    vm.put("v31", om.popArrayElement(vm.get("v3"), vm.get("param0")));
    vm.put("param1", vm.get("v31"));
    if (!em.ge(vm.get("v10"), vm.get("v30"))) e.abort(String.format("!((v10=%s) ge (v30=%s))", vm.get("v10"), vm.get("v30")));
    if (!em.eq(vm.get("v3"), vm.get("v7"))) e.abort(String.format("!((v3=%s) eq (v7=%s))", vm.get("v3"), vm.get("v7")));
    if (!em.ge(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) ge (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.lt(vm.get("v11"), vm.get("v16"))) e.abort(String.format("!((v11=%s) lt (v16=%s))", vm.get("v11"), vm.get("v16")));
    if (!em.lt(vm.get("v0"), vm.get("v30"))) e.abort(String.format("!((v0=%s) lt (v30=%s))", vm.get("v0"), vm.get("v30")));
    if (!em.lt(vm.get("v1"), vm.get("v30"))) e.abort(String.format("!((v1=%s) lt (v30=%s))", vm.get("v1"), vm.get("v30")));
    if (!em.le(vm.get("v17"), vm.get("v1"))) e.abort(String.format("!((v17=%s) le (v1=%s))", vm.get("v17"), vm.get("v1")));
    if (!em.le(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) le (v15=%s))", vm.get("param0"), vm.get("v15")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add4833(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2);
    vm.put("v1", (int)3);
    vm.put("v2", (int)1);
    vm.put("v3", (int)0);
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", om.getArrayLength(vm.get("v5")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", (int)4);
    vm.put("v13", vm.get("v6"));
    vm.put("v14", vm.get("v7"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    vm.put("v16", em.op("isub").eval(vm.get("v13"),vm.get("v2")));
    om.revertField(vm.get("this"), "size", vm.get("v16"));
    vm.put("v17", em.op("iadd").eval(vm.get("v16"),vm.get("v2")));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v9")));
    vm.put("v19", om.guessArrayIndex(vm.get("v5")));
    vm.put("param0", vm.get("v19"));
    vm.put("v20", om.popArrayElement(vm.get("v5"), vm.get("param0")));
    vm.put("param1", vm.get("v20"));
    vm.put("v21", em.op("isub").eval(vm.get("v16"),vm.get("param0")));
    vm.put("v22", em.op("iadd").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v23", em.op("iadd").eval(vm.get("v22"),vm.get("v1")));
    vm.put("v24", em.op("iadd").eval(vm.get("v22"),vm.get("v2")));
    vm.put("v25", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v26", om.getArrayElement(vm.get("v5"), vm.get("v25")));
    vm.put("v27", vm.get("v26"));
    vm.put("v28", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v29", om.getArrayElement(vm.get("v5"), vm.get("v28")));
    vm.put("v30", vm.get("v29"));
    vm.put("v31", em.op("iadd").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v32", om.getArrayElement(vm.get("v5"), vm.get("v31")));
    vm.put("v33", vm.get("v32"));
    vm.put("v34", om.popArrayElement(vm.get("v5"), vm.get("v23")));
    vm.put("v35", om.popArrayElement(vm.get("v5"), vm.get("v24")));
    vm.put("v36", em.op("iadd").eval(vm.get("v22"),vm.get("v3")));
    vm.put("v37", om.popArrayElement(vm.get("v5"), vm.get("v36")));
    vm.put("v38", em.op("iadd").eval(vm.get("v22"),vm.get("v0")));
    vm.put("v39", om.popArrayElement(vm.get("v5"), vm.get("v38")));
    vm.put("v40", em.op("iadd").eval(vm.get("param0"),vm.get("v3")));
    vm.put("v41", om.getArrayElement(vm.get("v5"), vm.get("v40")));
    vm.put("v42", vm.get("v41"));
    if (!em.ne(vm.get("v5"), vm.get("v11"))) e.abort(String.format("!((v5=%s) ne (v11=%s))", vm.get("v5"), vm.get("v11")));
    if (!em.lt(vm.get("v1"), vm.get("v21"))) e.abort(String.format("!((v1=%s) lt (v21=%s))", vm.get("v1"), vm.get("v21")));
    if (!em.lt(vm.get("v2"), vm.get("v21"))) e.abort(String.format("!((v2=%s) lt (v21=%s))", vm.get("v2"), vm.get("v21")));
    if (!em.lt(vm.get("v0"), vm.get("v21"))) e.abort(String.format("!((v0=%s) lt (v21=%s))", vm.get("v0"), vm.get("v21")));
    if (!em.ge(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) ge (v3=%s))", vm.get("param0"), vm.get("v3")));
    if (!em.le(vm.get("param0"), vm.get("v16"))) e.abort(String.format("!((param0=%s) le (v16=%s))", vm.get("param0"), vm.get("v16")));
    if (!em.ge(vm.get("v12"), vm.get("v21"))) e.abort(String.format("!((v12=%s) ge (v21=%s))", vm.get("v12"), vm.get("v21")));
    if (!em.le(vm.get("v18"), vm.get("v3"))) e.abort(String.format("!((v18=%s) le (v3=%s))", vm.get("v18"), vm.get("v3")));
    if (!em.lt(vm.get("v3"), vm.get("v21"))) e.abort(String.format("!((v3=%s) lt (v21=%s))", vm.get("v3"), vm.get("v21")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add2989(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", (int)0);
    vm.put("v8", (int)10);
    vm.put("v9", om.getArrayLength(vm.get("v5")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", vm.get("v6"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", vm.get("v0"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v14"));
    vm.put("v15", em.op("iadd").eval(vm.get("v14"),vm.get("v1")));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v10")));
    vm.put("v17", om.popArrayElement(vm.get("v5"), vm.get("v14")));
    vm.put("param0", vm.get("v17"));
    if (!em.eq(vm.get("v5"), vm.get("v3"))) e.abort(String.format("!((v5=%s) eq (v3=%s))", vm.get("v5"), vm.get("v3")));
    if (!em.lt(vm.get("v8"), vm.get("v15"))) e.abort(String.format("!((v8=%s) lt (v15=%s))", vm.get("v8"), vm.get("v15")));
    if (!em.le(vm.get("v16"), vm.get("v7"))) e.abort(String.format("!((v16=%s) le (v7=%s))", vm.get("v16"), vm.get("v7")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove3398(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", (int)1);
    vm.put("v4", null);
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", (int)0);
    vm.put("v7", vm.get("v2"));
    vm.put("v8", om.popArrayElement(vm.get("v1"), vm.get("v7")));
    vm.put("v9", em.op("iadd").eval(vm.get("v7"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v9"));
    vm.put("v10", vm.get("v5"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    vm.put("v12", om.guessArrayIndex(vm.get("v1")));
    vm.put("param0", vm.get("v12"));
    vm.put("v13", om.getArrayElement(vm.get("v1"), vm.get("param0")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v9"),vm.get("param0")));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v3")));
    if (!em.le(vm.get("v16"), vm.get("v6"))) e.abort(String.format("!((v16=%s) le (v6=%s))", vm.get("v16"), vm.get("v6")));
    if (!em.lt(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) lt (v9=%s))", vm.get("param0"), vm.get("v9")));
  }
  @IM(clazz="java.util.ArrayList", name="retainAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.elementData[*]","this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void retainAll1374(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2);
    vm.put("v1", (int)0);
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)3);
    vm.put("v9", null);
    vm.put("v10", om.getArrayElement(vm.get("v5"), vm.get("v0")));
    vm.put("v11", vm.get("v10"));
    om.revertArrayElement(vm.get("v5"), vm.get("v0"), vm.get("v11"));
    vm.put("v12", om.getArrayElement(vm.get("v5"), vm.get("v1")));
    vm.put("v13", vm.get("v12"));
    om.revertArrayElement(vm.get("v5"), vm.get("v1"), vm.get("v13"));
    vm.put("v14", om.getArrayElement(vm.get("v5"), vm.get("v2")));
    vm.put("v15", vm.get("v14"));
    om.revertArrayElement(vm.get("v5"), vm.get("v2"), vm.get("v15"));
    vm.put("v16", em.op("isub").eval(vm.get("v7"),vm.get("v1")));
    vm.put("v17", em.op("iadd").eval(vm.get("v16"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v17"));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v1")));
    vm.put("v19", em.op("isub").eval(vm.get("v17"),vm.get("v7")));
    vm.put("v20", vm.get("v3"));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v19")));
    om.revertField(vm.get("this"), "modCount", vm.get("v21"));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.ne(vm.get("v1"), vm.get("v17"))) e.abort(String.format("!((v1=%s) ne (v17=%s))", vm.get("v1"), vm.get("v17")));
    if (!em.lt(vm.get("v2"), vm.get("v18"))) e.abort(String.format("!((v2=%s) lt (v18=%s))", vm.get("v2"), vm.get("v18")));
    if (!em.lt(vm.get("v1"), vm.get("v18"))) e.abort(String.format("!((v1=%s) lt (v18=%s))", vm.get("v1"), vm.get("v18")));
    if (!em.ge(vm.get("v8"), vm.get("v18"))) e.abort(String.format("!((v8=%s) ge (v18=%s))", vm.get("v8"), vm.get("v18")));
    if (!em.lt(vm.get("v0"), vm.get("v18"))) e.abort(String.format("!((v0=%s) lt (v18=%s))", vm.get("v0"), vm.get("v18")));
    if (!em.ge(vm.get("v1"), vm.get("v17"))) e.abort(String.format("!((v1=%s) ge (v17=%s))", vm.get("v1"), vm.get("v17")));
    if (!em.ne(vm.get("v7"), vm.get("v17"))) e.abort(String.format("!((v7=%s) ne (v17=%s))", vm.get("v7"), vm.get("v17")));
    if (!em.ge(vm.get("v7"), vm.get("v17"))) e.abort(String.format("!((v7=%s) ge (v17=%s))", vm.get("v7"), vm.get("v17")));
    if (!em.ne(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) ne (v9=%s))", vm.get("param0"), vm.get("v9")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort785(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getArrayLength(vm.get("v3")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getArrayElement(vm.get("v3"), vm.get("v1")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getField(vm.get("this"), "size"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", (int)7);
    vm.put("v16", null);
    vm.put("v17", (int)2);
    vm.put("v18", java.lang.Object[].class);
    vm.put("v19", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    vm.put("v20", em.op("isub").eval(vm.get("v6"),vm.get("v0")));
    vm.put("v21", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    vm.put("v22", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v3")));
    vm.put("v23", vm.get("v4"));
    vm.put("v24", em.op("isub").eval(vm.get("v23"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v24"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ne(vm.get("v10"), vm.get("v0"))) e.abort(String.format("!((v10=%s) ne (v0=%s))", vm.get("v10"), vm.get("v0")));
    if (!em.ge(vm.get("v17"), vm.get("v21"))) e.abort(String.format("!((v17=%s) ge (v21=%s))", vm.get("v17"), vm.get("v21")));
    if (!em.ge(vm.get("v1"), vm.get("v14"))) e.abort(String.format("!((v1=%s) ge (v14=%s))", vm.get("v1"), vm.get("v14")));
    if (!em.ne(vm.get("param0"), vm.get("v16"))) e.abort(String.format("!((param0=%s) ne (v16=%s))", vm.get("param0"), vm.get("v16")));
    if (!em.gt(vm.get("v20"), vm.get("v21"))) e.abort(String.format("!((v20=%s) gt (v21=%s))", vm.get("v20"), vm.get("v21")));
    if (!em.ne(vm.get("param0"), vm.get("v16"))) e.abort(String.format("!((param0=%s) ne (v16=%s))", vm.get("param0"), vm.get("v16")));
    if (!em.lt(vm.get("v1"), vm.get("v21"))) e.abort(String.format("!((v1=%s) lt (v21=%s))", vm.get("v1"), vm.get("v21")));
    if (!em.ne(vm.get("v22"), vm.get("v18"))) e.abort(String.format("!((v22=%s) ne (v18=%s))", vm.get("v22"), vm.get("v18")));
    if (!em.lt(vm.get("v0"), vm.get("v14"))) e.abort(String.format("!((v0=%s) lt (v14=%s))", vm.get("v0"), vm.get("v14")));
    if (!em.lt(vm.get("v0"), vm.get("v21"))) e.abort(String.format("!((v0=%s) lt (v21=%s))", vm.get("v0"), vm.get("v21")));
    if (!em.le(vm.get("v0"), vm.get("v14"))) e.abort(String.format("!((v0=%s) le (v14=%s))", vm.get("v0"), vm.get("v14")));
    if (!em.ge(vm.get("v21"), vm.get("v0"))) e.abort(String.format("!((v21=%s) ge (v0=%s))", vm.get("v21"), vm.get("v0")));
    if (!em.lt(vm.get("v19"), vm.get("v15"))) e.abort(String.format("!((v19=%s) lt (v15=%s))", vm.get("v19"), vm.get("v15")));
    if (!em.le(vm.get("v14"), vm.get("v6"))) e.abort(String.format("!((v14=%s) le (v6=%s))", vm.get("v14"), vm.get("v6")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.elementData[*]","this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove1190(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2);
    vm.put("v1", (int)0);
    vm.put("v2", (int)1);
    vm.put("v3", (int)3);
    vm.put("v4", null);
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "size"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getArrayElement(vm.get("v6"), vm.get("v1")));
    vm.put("v10", vm.get("v9"));
    om.revertArrayElement(vm.get("v6"), vm.get("v2"), vm.get("v10"));
    vm.put("v11", om.getArrayElement(vm.get("v6"), vm.get("v2")));
    vm.put("v12", vm.get("v11"));
    om.revertArrayElement(vm.get("v6"), vm.get("v0"), vm.get("v12"));
    vm.put("v13", om.getField(vm.get("this"), "modCount"));
    vm.put("v14", om.getArrayElement(vm.get("v6"), vm.get("v3")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.popArrayElement(vm.get("v6"), vm.get("v8")));
    vm.put("v17", om.getArrayElement(vm.get("v6"), vm.get("v0")));
    vm.put("v18", em.op("iadd").eval(vm.get("v8"),vm.get("v2")));
    om.revertField(vm.get("this"), "size", vm.get("v18"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v1")));
    vm.put("v20", em.op("isub").eval(vm.get("v19"),vm.get("v2")));
    vm.put("v21", vm.get("v13"));
    vm.put("v22", em.op("isub").eval(vm.get("v21"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v22"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    om.revertArrayElement(vm.get("v6"), vm.get("v1"));
    vm.put("v23", om.getArrayElement(vm.get("v6"), vm.get("v1")));
    if (!em.gt(vm.get("v20"), vm.get("v1"))) e.abort(String.format("!((v20=%s) gt (v1=%s))", vm.get("v20"), vm.get("v1")));
    if (!em.lt(vm.get("v1"), vm.get("v20"))) e.abort(String.format("!((v1=%s) lt (v20=%s))", vm.get("v1"), vm.get("v20")));
    if (!em.ge(vm.get("v3"), vm.get("v20"))) e.abort(String.format("!((v3=%s) ge (v20=%s))", vm.get("v3"), vm.get("v20")));
    if (!em.eq(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) eq (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.lt(vm.get("v2"), vm.get("v20"))) e.abort(String.format("!((v2=%s) lt (v20=%s))", vm.get("v2"), vm.get("v20")));
    if (!em.lt(vm.get("v0"), vm.get("v20"))) e.abort(String.format("!((v0=%s) lt (v20=%s))", vm.get("v0"), vm.get("v20")));
    if (!em.eq(vm.get("v23"), vm.get("v4"))) e.abort(String.format("!((v23=%s) eq (v4=%s))", vm.get("v23"), vm.get("v4")));
    if (!em.lt(vm.get("v1"), vm.get("v18"))) e.abort(String.format("!((v1=%s) lt (v18=%s))", vm.get("v1"), vm.get("v18")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove3351(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", (int)3);
    vm.put("v3", (int)2);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", null);
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "size"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)4);
    vm.put("v11", om.popArrayElement(vm.get("v7"), vm.get("v9")));
    vm.put("v12", em.op("iadd").eval(vm.get("v9"),vm.get("v0")));
    om.revertField(vm.get("this"), "size", vm.get("v12"));
    vm.put("v13", vm.get("v4"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    vm.put("v15", om.guessArrayIndex(vm.get("v7")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.popArrayElement(vm.get("v7"), vm.get("v16")));
    vm.put("param0", em.op("isub").eval(vm.get("v16"),vm.get("v1")));
    vm.put("v18", em.op("isub").eval(vm.get("v12"),vm.get("param0")));
    vm.put("v19", em.op("iadd").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v20", em.op("iadd").eval(vm.get("param0"),vm.get("v3")));
    vm.put("v21", om.popArrayElement(vm.get("v7"), vm.get("v20")));
    vm.put("v22", om.getArrayElement(vm.get("v7"), vm.get("param0")));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.popArrayElement(vm.get("v7"), vm.get("v19")));
    vm.put("v25", em.op("isub").eval(vm.get("v18"),vm.get("v0")));
    vm.put("v26", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v27", om.popArrayElement(vm.get("v7"), vm.get("v26")));
    vm.put("v28", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v29", em.op("iadd").eval(vm.get("v28"),vm.get("v2")));
    vm.put("v30", em.op("iadd").eval(vm.get("v28"),vm.get("v1")));
    vm.put("v31", em.op("iadd").eval(vm.get("v28"),vm.get("v3")));
    vm.put("v32", om.getArrayElement(vm.get("v7"), vm.get("v31")));
    vm.put("v33", vm.get("v32"));
    vm.put("v34", em.op("iadd").eval(vm.get("v28"),vm.get("v0")));
    vm.put("v35", om.getArrayElement(vm.get("v7"), vm.get("v30")));
    vm.put("v36", vm.get("v35"));
    vm.put("v37", om.getArrayElement(vm.get("v7"), vm.get("v29")));
    vm.put("v38", vm.get("v37"));
    vm.put("v39", om.getArrayElement(vm.get("v7"), vm.get("v34")));
    vm.put("v40", vm.get("v39"));
    if (!em.ge(vm.get("v10"), vm.get("v25"))) e.abort(String.format("!((v10=%s) ge (v25=%s))", vm.get("v10"), vm.get("v25")));
    if (!em.gt(vm.get("v25"), vm.get("v1"))) e.abort(String.format("!((v25=%s) gt (v1=%s))", vm.get("v25"), vm.get("v1")));
    if (!em.lt(vm.get("v2"), vm.get("v25"))) e.abort(String.format("!((v2=%s) lt (v25=%s))", vm.get("v2"), vm.get("v25")));
    if (!em.lt(vm.get("v1"), vm.get("v25"))) e.abort(String.format("!((v1=%s) lt (v25=%s))", vm.get("v1"), vm.get("v25")));
    if (!em.lt(vm.get("v3"), vm.get("v25"))) e.abort(String.format("!((v3=%s) lt (v25=%s))", vm.get("v3"), vm.get("v25")));
    if (!em.lt(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) lt (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.lt(vm.get("v0"), vm.get("v25"))) e.abort(String.format("!((v0=%s) lt (v25=%s))", vm.get("v0"), vm.get("v25")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort766(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "elementData"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayLength(vm.get("v2")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "size"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("this"), "modCount"));
    vm.put("v10", (int)1);
    vm.put("v11", om.getArrayElement(vm.get("v2"), vm.get("v0")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", (int)7);
    vm.put("v14", null);
    vm.put("v15", java.lang.Object[].class);
    vm.put("v16", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v2")));
    vm.put("v17", em.op("isub").eval(vm.get("v6"),vm.get("v0")));
    vm.put("v18", em.op("isub").eval(vm.get("v8"),vm.get("v0")));
    vm.put("v19", em.op("isub").eval(vm.get("v8"),vm.get("v0")));
    vm.put("v20", vm.get("v9"));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v10")));
    om.revertField(vm.get("this"), "modCount", vm.get("v21"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.gt(vm.get("v17"), vm.get("v18"))) e.abort(String.format("!((v17=%s) gt (v18=%s))", vm.get("v17"), vm.get("v18")));
    if (!em.ge(vm.get("v0"), vm.get("v8"))) e.abort(String.format("!((v0=%s) ge (v8=%s))", vm.get("v0"), vm.get("v8")));
    if (!em.ne(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) ne (v14=%s))", vm.get("param0"), vm.get("v14")));
    if (!em.ne(vm.get("v16"), vm.get("v15"))) e.abort(String.format("!((v16=%s) ne (v15=%s))", vm.get("v16"), vm.get("v15")));
    if (!em.le(vm.get("v8"), vm.get("v6"))) e.abort(String.format("!((v8=%s) le (v6=%s))", vm.get("v8"), vm.get("v6")));
    if (!em.le(vm.get("v0"), vm.get("v8"))) e.abort(String.format("!((v0=%s) le (v8=%s))", vm.get("v0"), vm.get("v8")));
    if (!em.lt(vm.get("v19"), vm.get("v13"))) e.abort(String.format("!((v19=%s) lt (v13=%s))", vm.get("v19"), vm.get("v13")));
    if (!em.lt(vm.get("v0"), vm.get("v18"))) e.abort(String.format("!((v0=%s) lt (v18=%s))", vm.get("v0"), vm.get("v18")));
    if (!em.ne(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) ne (v14=%s))", vm.get("param0"), vm.get("v14")));
    if (!em.ge(vm.get("v10"), vm.get("v18"))) e.abort(String.format("!((v10=%s) ge (v18=%s))", vm.get("v10"), vm.get("v18")));
    if (!em.ne(vm.get("v4"), vm.get("v0"))) e.abort(String.format("!((v4=%s) ne (v0=%s))", vm.get("v4"), vm.get("v0")));
    if (!em.ge(vm.get("v18"), vm.get("v0"))) e.abort(String.format("!((v18=%s) ge (v0=%s))", vm.get("v18"), vm.get("v0")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add4859(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "modCount"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "size"));
    vm.put("v8", (int)10);
    vm.put("v9", om.getArrayLength(vm.get("v6")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", vm.get("v7"));
    vm.put("v12", vm.get("v1"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v11"),vm.get("v2")));
    om.revertField(vm.get("this"), "size", vm.get("v14"));
    vm.put("v15", em.op("iadd").eval(vm.get("v14"),vm.get("v2")));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v10")));
    vm.put("v17", om.guessArrayIndex(vm.get("v6")));
    vm.put("param0", vm.get("v17"));
    vm.put("v18", om.popArrayElement(vm.get("v6"), vm.get("param0")));
    vm.put("param1", vm.get("v18"));
    vm.put("v19", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v20", om.getArrayElement(vm.get("v6"), vm.get("v19")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", em.op("iadd").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v23", em.op("iadd").eval(vm.get("v22"),vm.get("v0")));
    vm.put("v24", om.popArrayElement(vm.get("v6"), vm.get("v23")));
    vm.put("v25", em.op("isub").eval(vm.get("v14"),vm.get("param0")));
    if (!em.lt(vm.get("v0"), vm.get("v25"))) e.abort(String.format("!((v0=%s) lt (v25=%s))", vm.get("v0"), vm.get("v25")));
    if (!em.le(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) le (v14=%s))", vm.get("param0"), vm.get("v14")));
    if (!em.eq(vm.get("v6"), vm.get("v4"))) e.abort(String.format("!((v6=%s) eq (v4=%s))", vm.get("v6"), vm.get("v4")));
    if (!em.le(vm.get("v16"), vm.get("v0"))) e.abort(String.format("!((v16=%s) le (v0=%s))", vm.get("v16"), vm.get("v0")));
    if (!em.ge(vm.get("v2"), vm.get("v25"))) e.abort(String.format("!((v2=%s) ge (v25=%s))", vm.get("v2"), vm.get("v25")));
    if (!em.ge(vm.get("param0"), vm.get("v0"))) e.abort(String.format("!((param0=%s) ge (v0=%s))", vm.get("param0"), vm.get("v0")));
    if (!em.lt(vm.get("v8"), vm.get("v15"))) e.abort(String.format("!((v8=%s) lt (v15=%s))", vm.get("v8"), vm.get("v15")));
  }
  @IM(clazz="java.util.ArrayList", name="retainAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.size","this.modCount"},definedOutput={"this.size","this.modCount"})
  static void retainAll1394(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)0);
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", null);
    vm.put("v7", em.op("isub").eval(vm.get("v5"),vm.get("v3")));
    vm.put("v8", em.op("iadd").eval(vm.get("v7"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v8"));
    vm.put("v9", em.op("isub").eval(vm.get("v8"),vm.get("v3")));
    vm.put("v10", em.op("isub").eval(vm.get("v8"),vm.get("v5")));
    vm.put("v11", vm.get("v2"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v10")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.ge(vm.get("v5"), vm.get("v8"))) e.abort(String.format("!((v5=%s) ge (v8=%s))", vm.get("v5"), vm.get("v8")));
    if (!em.ne(vm.get("v3"), vm.get("v8"))) e.abort(String.format("!((v3=%s) ne (v8=%s))", vm.get("v3"), vm.get("v8")));
    if (!em.ge(vm.get("v3"), vm.get("v9"))) e.abort(String.format("!((v3=%s) ge (v9=%s))", vm.get("v3"), vm.get("v9")));
    if (!em.ge(vm.get("v3"), vm.get("v8"))) e.abort(String.format("!((v3=%s) ge (v8=%s))", vm.get("v3"), vm.get("v8")));
    if (!em.ne(vm.get("v5"), vm.get("v8"))) e.abort(String.format("!((v5=%s) ne (v8=%s))", vm.get("v5"), vm.get("v8")));
    if (!em.ne(vm.get("param0"), vm.get("v6"))) e.abort(String.format("!((param0=%s) ne (v6=%s))", vm.get("param0"), vm.get("v6")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add2983(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getArrayLength(vm.get("v1")));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", (int)10);
    vm.put("v10", (int)0);
    vm.put("v11", em.op("isub").eval(vm.get("v9"),vm.get("v3")));
    vm.put("v12", vm.get("v4"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v5")));
    om.revertField(vm.get("this"), "size", vm.get("v13"));
    vm.put("v14", em.op("iadd").eval(vm.get("v13"),vm.get("v5")));
    vm.put("v15", om.popArrayElement(vm.get("v1"), vm.get("v13")));
    vm.put("param0", vm.get("v15"));
    vm.put("v16", vm.get("v8"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v5")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    if (!em.ge(vm.get("v9"), vm.get("v14"))) e.abort(String.format("!((v9=%s) ge (v14=%s))", vm.get("v9"), vm.get("v14")));
    if (!em.le(vm.get("v11"), vm.get("v10"))) e.abort(String.format("!((v11=%s) le (v10=%s))", vm.get("v11"), vm.get("v10")));
    if (!em.eq(vm.get("v1"), vm.get("v7"))) e.abort(String.format("!((v1=%s) eq (v7=%s))", vm.get("v1"), vm.get("v7")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add4895(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)2);
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "size"));
    vm.put("v9", om.getArrayLength(vm.get("v4")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)3);
    vm.put("v12", vm.get("v5"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    vm.put("v14", vm.get("v8"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v2")));
    om.revertField(vm.get("this"), "size", vm.get("v15"));
    vm.put("v16", em.op("iadd").eval(vm.get("v15"),vm.get("v2")));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v10")));
    vm.put("v18", om.guessArrayIndex(vm.get("v4")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", om.getArrayElement(vm.get("v4"), vm.get("v19")));
    vm.put("v21", vm.get("v20"));
    vm.put("param0", em.op("isub").eval(vm.get("v19"),vm.get("v2")));
    vm.put("v22", om.popArrayElement(vm.get("v4"), vm.get("param0")));
    vm.put("param1", vm.get("v22"));
    vm.put("v23", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v24", om.getArrayElement(vm.get("v4"), vm.get("v23")));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", em.op("iadd").eval(vm.get("param0"),vm.get("v2")));
    vm.put("v27", em.op("iadd").eval(vm.get("v26"),vm.get("v0")));
    vm.put("v28", em.op("iadd").eval(vm.get("v26"),vm.get("v2")));
    vm.put("v29", om.popArrayElement(vm.get("v4"), vm.get("v27")));
    vm.put("v30", om.popArrayElement(vm.get("v4"), vm.get("v28")));
    vm.put("v31", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v32", om.getArrayElement(vm.get("v4"), vm.get("v31")));
    vm.put("v33", vm.get("v32"));
    vm.put("v34", em.op("iadd").eval(vm.get("v26"),vm.get("v1")));
    vm.put("v35", em.op("isub").eval(vm.get("v15"),vm.get("param0")));
    vm.put("v36", om.popArrayElement(vm.get("v4"), vm.get("v34")));
    if (!em.lt(vm.get("v0"), vm.get("v35"))) e.abort(String.format("!((v0=%s) lt (v35=%s))", vm.get("v0"), vm.get("v35")));
    if (!em.ne(vm.get("v4"), vm.get("v7"))) e.abort(String.format("!((v4=%s) ne (v7=%s))", vm.get("v4"), vm.get("v7")));
    if (!em.le(vm.get("v17"), vm.get("v0"))) e.abort(String.format("!((v17=%s) le (v0=%s))", vm.get("v17"), vm.get("v0")));
    if (!em.lt(vm.get("v2"), vm.get("v35"))) e.abort(String.format("!((v2=%s) lt (v35=%s))", vm.get("v2"), vm.get("v35")));
    if (!em.ge(vm.get("v11"), vm.get("v35"))) e.abort(String.format("!((v11=%s) ge (v35=%s))", vm.get("v11"), vm.get("v35")));
    if (!em.lt(vm.get("v1"), vm.get("v35"))) e.abort(String.format("!((v1=%s) lt (v35=%s))", vm.get("v1"), vm.get("v35")));
    if (!em.ge(vm.get("param0"), vm.get("v0"))) e.abort(String.format("!((param0=%s) ge (v0=%s))", vm.get("param0"), vm.get("v0")));
    if (!em.le(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) le (v15=%s))", vm.get("param0"), vm.get("v15")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort781(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getArrayLength(vm.get("v3")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)7);
    vm.put("v12", null);
    vm.put("v13", java.lang.Object[].class);
    vm.put("v14", em.op("isub").eval(vm.get("v5"),vm.get("v10")));
    vm.put("v15", em.op("isub").eval(vm.get("v7"),vm.get("v10")));
    vm.put("v16", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v3")));
    vm.put("v17", em.op("isub").eval(vm.get("v5"),vm.get("v10")));
    vm.put("v18", vm.get("v0"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.le(vm.get("v5"), vm.get("v7"))) e.abort(String.format("!((v5=%s) le (v7=%s))", vm.get("v5"), vm.get("v7")));
    if (!em.ne(vm.get("v16"), vm.get("v13"))) e.abort(String.format("!((v16=%s) ne (v13=%s))", vm.get("v16"), vm.get("v13")));
    if (!em.ge(vm.get("v14"), vm.get("v10"))) e.abort(String.format("!((v14=%s) ge (v10=%s))", vm.get("v14"), vm.get("v10")));
    if (!em.ne(vm.get("v9"), vm.get("v10"))) e.abort(String.format("!((v9=%s) ne (v10=%s))", vm.get("v9"), vm.get("v10")));
    if (!em.le(vm.get("v10"), vm.get("v5"))) e.abort(String.format("!((v10=%s) le (v5=%s))", vm.get("v10"), vm.get("v5")));
    if (!em.lt(vm.get("v10"), vm.get("v5"))) e.abort(String.format("!((v10=%s) lt (v5=%s))", vm.get("v10"), vm.get("v5")));
    if (!em.le(vm.get("v15"), vm.get("v14"))) e.abort(String.format("!((v15=%s) le (v14=%s))", vm.get("v15"), vm.get("v14")));
    if (!em.ge(vm.get("v1"), vm.get("v5"))) e.abort(String.format("!((v1=%s) ge (v5=%s))", vm.get("v1"), vm.get("v5")));
    if (!em.eq(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) eq (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ge(vm.get("v10"), vm.get("v15"))) e.abort(String.format("!((v10=%s) ge (v15=%s))", vm.get("v10"), vm.get("v15")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.lt(vm.get("v17"), vm.get("v11"))) e.abort(String.format("!((v17=%s) lt (v11=%s))", vm.get("v17"), vm.get("v11")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add4893(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", om.getField(vm.get("this"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayLength(vm.get("v4")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", (int)0);
    vm.put("v10", (int)10);
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v6")));
    vm.put("v12", vm.get("v0"));
    vm.put("v13", vm.get("v2"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    vm.put("v15", em.op("isub").eval(vm.get("v12"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v15"));
    vm.put("v16", em.op("iadd").eval(vm.get("v15"),vm.get("v1")));
    vm.put("v17", om.guessArrayIndex(vm.get("v4")));
    vm.put("param0", vm.get("v17"));
    vm.put("v18", om.popArrayElement(vm.get("v4"), vm.get("param0")));
    vm.put("param1", vm.get("v18"));
    vm.put("v19", em.op("isub").eval(vm.get("v15"),vm.get("param0")));
    if (!em.le(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) le (v15=%s))", vm.get("param0"), vm.get("v15")));
    if (!em.ge(vm.get("v9"), vm.get("v19"))) e.abort(String.format("!((v9=%s) ge (v19=%s))", vm.get("v9"), vm.get("v19")));
    if (!em.ge(vm.get("v10"), vm.get("v16"))) e.abort(String.format("!((v10=%s) ge (v16=%s))", vm.get("v10"), vm.get("v16")));
    if (!em.eq(vm.get("v4"), vm.get("v8"))) e.abort(String.format("!((v4=%s) eq (v8=%s))", vm.get("v4"), vm.get("v8")));
    if (!em.ge(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) ge (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.le(vm.get("v11"), vm.get("v9"))) e.abort(String.format("!((v11=%s) le (v9=%s))", vm.get("v11"), vm.get("v9")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove1145(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getArrayElement(vm.get("v3"), vm.get("v1")));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", null);
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", vm.get("v8"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", vm.get("v6"));
    vm.put("v14", om.popArrayElement(vm.get("v3"), vm.get("v13")));
    vm.put("v15", em.op("iadd").eval(vm.get("v13"),vm.get("v0")));
    om.revertField(vm.get("this"), "size", vm.get("v15"));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v0")));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.lt(vm.get("v0"), vm.get("v15"))) e.abort(String.format("!((v0=%s) lt (v15=%s))", vm.get("v0"), vm.get("v15")));
    if (!em.le(vm.get("v17"), vm.get("v1"))) e.abort(String.format("!((v17=%s) le (v1=%s))", vm.get("v17"), vm.get("v1")));
    if (!em.eq(vm.get("v10"), vm.get("v7"))) e.abort(String.format("!((v10=%s) eq (v7=%s))", vm.get("v10"), vm.get("v7")));
    if (!em.ne(vm.get("v5"), vm.get("v7"))) e.abort(String.format("!((v5=%s) ne (v7=%s))", vm.get("v5"), vm.get("v7")));
    if (!em.lt(vm.get("v1"), vm.get("v15"))) e.abort(String.format("!((v1=%s) lt (v15=%s))", vm.get("v1"), vm.get("v15")));
    if (!em.eq(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) eq (v7=%s))", vm.get("param0"), vm.get("v7")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort743(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)0);
    vm.put("v9", (int)7);
    vm.put("v10", null);
    vm.put("v11", java.lang.Object[].class);
    vm.put("v12", om.getArrayLength(vm.get("v3")));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", em.op("isub").eval(vm.get("v7"),vm.get("v8")));
    vm.put("v15", em.op("isub").eval(vm.get("v13"),vm.get("v8")));
    vm.put("v16", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v3")));
    vm.put("v17", em.op("isub").eval(vm.get("v7"),vm.get("v8")));
    vm.put("v18", vm.get("v0"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.eq(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) eq (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.ne(vm.get("v5"), vm.get("v8"))) e.abort(String.format("!((v5=%s) ne (v8=%s))", vm.get("v5"), vm.get("v8")));
    if (!em.lt(vm.get("v17"), vm.get("v9"))) e.abort(String.format("!((v17=%s) lt (v9=%s))", vm.get("v17"), vm.get("v9")));
    if (!em.ge(vm.get("v8"), vm.get("v15"))) e.abort(String.format("!((v8=%s) ge (v15=%s))", vm.get("v8"), vm.get("v15")));
    if (!em.le(vm.get("v8"), vm.get("v7"))) e.abort(String.format("!((v8=%s) le (v7=%s))", vm.get("v8"), vm.get("v7")));
    if (!em.le(vm.get("v7"), vm.get("v13"))) e.abort(String.format("!((v7=%s) le (v13=%s))", vm.get("v7"), vm.get("v13")));
    if (!em.ne(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ne (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.eq(vm.get("v16"), vm.get("v11"))) e.abort(String.format("!((v16=%s) eq (v11=%s))", vm.get("v16"), vm.get("v11")));
    if (!em.ge(vm.get("v14"), vm.get("v8"))) e.abort(String.format("!((v14=%s) ge (v8=%s))", vm.get("v14"), vm.get("v8")));
    if (!em.ge(vm.get("v8"), vm.get("v7"))) e.abort(String.format("!((v8=%s) ge (v7=%s))", vm.get("v8"), vm.get("v7")));
    if (!em.le(vm.get("v15"), vm.get("v14"))) e.abort(String.format("!((v15=%s) le (v14=%s))", vm.get("v15"), vm.get("v14")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort755(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", (int)1);
    vm.put("v8", (int)0);
    vm.put("v9", (int)7);
    vm.put("v10", java.lang.Object[].class);
    vm.put("v11", null);
    vm.put("v12", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v3")));
    vm.put("v13", em.op("isub").eval(vm.get("v5"),vm.get("v8")));
    vm.put("v14", em.op("isub").eval(vm.get("v5"),vm.get("v8")));
    vm.put("v15", om.getArrayLength(vm.get("v3")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v8")));
    vm.put("v18", vm.get("v6"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v7")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ge(vm.get("v8"), vm.get("v5"))) e.abort(String.format("!((v8=%s) ge (v5=%s))", vm.get("v8"), vm.get("v5")));
    if (!em.ne(vm.get("v1"), vm.get("v8"))) e.abort(String.format("!((v1=%s) ne (v8=%s))", vm.get("v1"), vm.get("v8")));
    if (!em.lt(vm.get("v13"), vm.get("v9"))) e.abort(String.format("!((v13=%s) lt (v9=%s))", vm.get("v13"), vm.get("v9")));
    if (!em.ne(vm.get("v12"), vm.get("v10"))) e.abort(String.format("!((v12=%s) ne (v10=%s))", vm.get("v12"), vm.get("v10")));
    if (!em.eq(vm.get("param0"), vm.get("v11"))) e.abort(String.format("!((param0=%s) eq (v11=%s))", vm.get("param0"), vm.get("v11")));
    if (!em.ge(vm.get("v8"), vm.get("v17"))) e.abort(String.format("!((v8=%s) ge (v17=%s))", vm.get("v8"), vm.get("v17")));
    if (!em.ge(vm.get("v14"), vm.get("v8"))) e.abort(String.format("!((v14=%s) ge (v8=%s))", vm.get("v14"), vm.get("v8")));
    if (!em.le(vm.get("v5"), vm.get("v16"))) e.abort(String.format("!((v5=%s) le (v16=%s))", vm.get("v5"), vm.get("v16")));
    if (!em.le(vm.get("v17"), vm.get("v14"))) e.abort(String.format("!((v17=%s) le (v14=%s))", vm.get("v17"), vm.get("v14")));
    if (!em.le(vm.get("v8"), vm.get("v5"))) e.abort(String.format("!((v8=%s) le (v5=%s))", vm.get("v8"), vm.get("v5")));
  }
  @IM(clazz="java.util.ArrayList", name="<init>", desc="(I)V",definedOutput={"this.elementData","this.modCount"},delta={"this.elementData","this.modCount"})
  @DG(succ={"retainAll1318","remove1111","add4859","sort755","remove1190","removeRange3485","remove1199","add4896","sort766","removeRange3497","clear4269","clear4299","remove1158","add4893","sort777","clear4256","sort781","fastRemove383","removeAll192","removeRange3419","fastRemove325","sort785","remove3390","trimToSize3697","remove3397","add4848","removeRange3489","sort793","add4895","sort779","remove3393","sort728","fastRemove386","add4833","sort791","removeRange3445","ensureCapacityInternal3589","removeRange3477","sort787","set3299","sort729","remove1117","sort757","add4897","retainAll1392","removeRange3417","removeAll146","removeAll173","retainAll1357","fastRemove332","add4886","removeAll182","add2989","sort741","sort745","removeAll124","add2994","remove3351","add2983","batchRemove4464","fastRemove321","remove1176","sort751","retainAll1398","sort798","sort735","batchRemove4491","removeAll181","remove1198","batchRemove4447","removeRange3490","sort722","remove3399","batchRemove4449","sort786","removeRange3441","add4852","removeRange3460","removeRange3438","removeRange3468","removeRange3416","retainAll1374","removeRange3480","removeAll152","batchRemove4481","clear4289","batchRemove4470","add4875","batchRemove4498","sort743","removeAll177","batchRemove4466","remove1145","removeRange3481","sort782","add4898","sort790","add4861","removeRange3484","remove3394","remove1116","clear4286","removeRange3423","sort732","remove3391","removeRange3407","batchRemove4421","retainAll1389","sort736","sort799","removeRange3494","add4828","sort747","sort770","remove3398","removeRange3499","fastRemove356","sort776"})
  static void _init_1499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", om.getStatic("java.util.ArrayList.EMPTY_ELEMENTDATA"));
    vm.put("v1", vm.get("v0"));
    vm.put("this", om.getTarget());
    vm.put("v2", om.popField(vm.get("this"), "elementData"));
    vm.put("v3", om.popField(vm.get("this"), "modCount"));
    vm.put("v4", (int)0);
    if (!em.eq(vm.get("v1"), (vm.get("v2")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v2")));
    if (!em.eq(vm.get("v4"), (vm.get("v3")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v3")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.eq(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) eq (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.le(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) le (v4=%s))", vm.get("param0"), vm.get("v4")));
  }
  @IM(clazz="java.util.ArrayList", name="retainAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void retainAll1398(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", null);
    vm.put("v5", om.getField(vm.get("this"), "size"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)1);
    vm.put("v8", om.popArrayElement(vm.get("v3"), vm.get("v6")));
    vm.put("v9", em.op("iinc").eval(vm.get("v6"),vm.get("v7")));
    vm.put("v10", em.op("isub").eval(vm.get("v6"),vm.get("v1")));
    vm.put("v11", vm.get("v0"));
    vm.put("v12", em.op("iadd").eval(vm.get("v10"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v12"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v1")));
    vm.put("v14", em.op("isub").eval(vm.get("v12"),vm.get("v6")));
    vm.put("v15", em.op("isub").eval(vm.get("v11"),vm.get("v14")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.ne(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) ne (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.ge(vm.get("v1"), vm.get("v13"))) e.abort(String.format("!((v1=%s) ge (v13=%s))", vm.get("v1"), vm.get("v13")));
    if (!em.ge(vm.get("v9"), vm.get("v12"))) e.abort(String.format("!((v9=%s) ge (v12=%s))", vm.get("v9"), vm.get("v12")));
    if (!em.ne(vm.get("v6"), vm.get("v12"))) e.abort(String.format("!((v6=%s) ne (v12=%s))", vm.get("v6"), vm.get("v12")));
    if (!em.lt(vm.get("v6"), vm.get("v12"))) e.abort(String.format("!((v6=%s) lt (v12=%s))", vm.get("v6"), vm.get("v12")));
    if (!em.ne(vm.get("v1"), vm.get("v12"))) e.abort(String.format("!((v1=%s) ne (v12=%s))", vm.get("v1"), vm.get("v12")));
    if (!em.ge(vm.get("v1"), vm.get("v12"))) e.abort(String.format("!((v1=%s) ge (v12=%s))", vm.get("v1"), vm.get("v12")));
  }
  @IM(clazz="java.util.ArrayList", name="clear", desc="()V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void clear4286(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)4);
    vm.put("v1", null);
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", (int)0);
    vm.put("v4", (int)3);
    vm.put("v5", om.getField(vm.get("this"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)2);
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", (int)1);
    vm.put("v10", om.popArrayElement(vm.get("v6"), vm.get("v9")));
    vm.put("v11", (int)5);
    vm.put("v12", om.popArrayElement(vm.get("v6"), vm.get("v3")));
    vm.put("v13", om.popArrayElement(vm.get("v6"), vm.get("v0")));
    vm.put("v14", om.popArrayElement(vm.get("v6"), vm.get("v4")));
    vm.put("v15", om.popArrayElement(vm.get("v6"), vm.get("v7")));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    vm.put("v16", vm.get("v8"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v9")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    om.revertField(vm.get("this"), "size");
    vm.put("v18", om.getField(vm.get("this"), "size"));
    if (!em.lt(vm.get("v0"), vm.get("v18"))) e.abort(String.format("!((v0=%s) lt (v18=%s))", vm.get("v0"), vm.get("v18")));
    if (!em.lt(vm.get("v3"), vm.get("v18"))) e.abort(String.format("!((v3=%s) lt (v18=%s))", vm.get("v3"), vm.get("v18")));
    if (!em.lt(vm.get("v4"), vm.get("v18"))) e.abort(String.format("!((v4=%s) lt (v18=%s))", vm.get("v4"), vm.get("v18")));
    if (!em.lt(vm.get("v7"), vm.get("v18"))) e.abort(String.format("!((v7=%s) lt (v18=%s))", vm.get("v7"), vm.get("v18")));
    if (!em.ge(vm.get("v11"), vm.get("v18"))) e.abort(String.format("!((v11=%s) ge (v18=%s))", vm.get("v11"), vm.get("v18")));
    if (!em.lt(vm.get("v9"), vm.get("v18"))) e.abort(String.format("!((v9=%s) lt (v18=%s))", vm.get("v9"), vm.get("v18")));
  }
  @IM(clazz="java.util.ArrayList", name="clear", desc="()V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void clear4299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", (int)0);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", (int)1);
    vm.put("v6", om.popArrayElement(vm.get("v1"), vm.get("v3")));
    vm.put("v7", null);
    vm.put("v8", vm.get("v4"));
    vm.put("v9", em.op("isub").eval(vm.get("v8"),vm.get("v5")));
    om.revertField(vm.get("this"), "modCount", vm.get("v9"));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    om.revertField(vm.get("this"), "size");
    vm.put("v10", om.getField(vm.get("this"), "size"));
    if (!em.lt(vm.get("v3"), vm.get("v10"))) e.abort(String.format("!((v3=%s) lt (v10=%s))", vm.get("v3"), vm.get("v10")));
    if (!em.ge(vm.get("v5"), vm.get("v10"))) e.abort(String.format("!((v5=%s) ge (v10=%s))", vm.get("v5"), vm.get("v10")));
  }
  @IM(clazz="java.util.ArrayList", name="clear", desc="()V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void clear4256(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", (int)2);
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", (int)0);
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.popArrayElement(vm.get("v7"), vm.get("v5")));
    vm.put("v9", (int)3);
    vm.put("v10", om.popArrayElement(vm.get("v7"), vm.get("v1")));
    vm.put("v11", om.popArrayElement(vm.get("v7"), vm.get("v2")));
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    vm.put("v12", vm.get("v0"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    om.revertField(vm.get("this"), "size");
    vm.put("v14", om.getField(vm.get("this"), "size"));
    if (!em.lt(vm.get("v5"), vm.get("v14"))) e.abort(String.format("!((v5=%s) lt (v14=%s))", vm.get("v5"), vm.get("v14")));
    if (!em.lt(vm.get("v2"), vm.get("v14"))) e.abort(String.format("!((v2=%s) lt (v14=%s))", vm.get("v2"), vm.get("v14")));
    if (!em.lt(vm.get("v1"), vm.get("v14"))) e.abort(String.format("!((v1=%s) lt (v14=%s))", vm.get("v1"), vm.get("v14")));
    if (!em.ge(vm.get("v9"), vm.get("v14"))) e.abort(String.format("!((v9=%s) ge (v14=%s))", vm.get("v9"), vm.get("v14")));
  }
  @IM(clazz="java.util.ArrayList", name="clear", desc="()V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void clear4269(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", null);
    vm.put("v2", (int)0);
    vm.put("v3", (int)2);
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", (int)3);
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", (int)4);
    vm.put("v10", om.popArrayElement(vm.get("v7"), vm.get("v5")));
    vm.put("v11", om.popArrayElement(vm.get("v7"), vm.get("v0")));
    vm.put("v12", om.popArrayElement(vm.get("v7"), vm.get("v2")));
    vm.put("v13", om.popArrayElement(vm.get("v7"), vm.get("v3")));
    vm.put("v14", vm.get("v8"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    if (!em.eq(vm.get("v2"), (vm.get("v4")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v4")));
    om.revertField(vm.get("this"), "size");
    vm.put("v16", om.getField(vm.get("this"), "size"));
    if (!em.lt(vm.get("v5"), vm.get("v16"))) e.abort(String.format("!((v5=%s) lt (v16=%s))", vm.get("v5"), vm.get("v16")));
    if (!em.lt(vm.get("v2"), vm.get("v16"))) e.abort(String.format("!((v2=%s) lt (v16=%s))", vm.get("v2"), vm.get("v16")));
    if (!em.ge(vm.get("v9"), vm.get("v16"))) e.abort(String.format("!((v9=%s) ge (v16=%s))", vm.get("v9"), vm.get("v16")));
    if (!em.lt(vm.get("v3"), vm.get("v16"))) e.abort(String.format("!((v3=%s) lt (v16=%s))", vm.get("v3"), vm.get("v16")));
    if (!em.lt(vm.get("v0"), vm.get("v16"))) e.abort(String.format("!((v0=%s) lt (v16=%s))", vm.get("v0"), vm.get("v16")));
  }
  @IM(clazz="java.util.ArrayList", name="removeAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void removeAll152(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)0);
    vm.put("v4", null);
    vm.put("v5", om.getField(vm.get("this"), "size"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)1);
    vm.put("v8", om.popArrayElement(vm.get("v1"), vm.get("v6")));
    vm.put("v9", em.op("iinc").eval(vm.get("v6"),vm.get("v7")));
    vm.put("v10", em.op("isub").eval(vm.get("v6"),vm.get("v3")));
    vm.put("v11", vm.get("v2"));
    vm.put("v12", em.op("iadd").eval(vm.get("v10"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v12"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v6")));
    vm.put("v14", em.op("isub").eval(vm.get("v12"),vm.get("v3")));
    vm.put("v15", em.op("isub").eval(vm.get("v11"),vm.get("v13")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.ge(vm.get("v3"), vm.get("v14"))) e.abort(String.format("!((v3=%s) ge (v14=%s))", vm.get("v3"), vm.get("v14")));
    if (!em.ne(vm.get("v6"), vm.get("v12"))) e.abort(String.format("!((v6=%s) ne (v12=%s))", vm.get("v6"), vm.get("v12")));
    if (!em.ge(vm.get("v3"), vm.get("v12"))) e.abort(String.format("!((v3=%s) ge (v12=%s))", vm.get("v3"), vm.get("v12")));
    if (!em.ne(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) ne (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.ne(vm.get("v3"), vm.get("v12"))) e.abort(String.format("!((v3=%s) ne (v12=%s))", vm.get("v3"), vm.get("v12")));
    if (!em.ge(vm.get("v9"), vm.get("v12"))) e.abort(String.format("!((v9=%s) ge (v12=%s))", vm.get("v9"), vm.get("v12")));
    if (!em.lt(vm.get("v6"), vm.get("v12"))) e.abort(String.format("!((v6=%s) lt (v12=%s))", vm.get("v6"), vm.get("v12")));
  }
  @IM(clazz="java.util.ArrayList", name="add", desc="(ILjava/lang/Object;)V",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void add4897(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getField(vm.get("this"), "size"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)2);
    vm.put("v9", om.getArrayLength(vm.get("v7")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", vm.get("v5"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v1")));
    om.revertField(vm.get("this"), "size", vm.get("v12"));
    vm.put("v13", em.op("iadd").eval(vm.get("v12"),vm.get("v1")));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v10")));
    vm.put("v15", vm.get("v4"));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v16"));
    vm.put("v17", om.guessArrayIndex(vm.get("v7")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.getArrayElement(vm.get("v7"), vm.get("v18")));
    vm.put("v20", vm.get("v19"));
    vm.put("param0", em.op("isub").eval(vm.get("v18"),vm.get("v1")));
    vm.put("v21", em.op("isub").eval(vm.get("v12"),vm.get("param0")));
    vm.put("v22", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v23", om.getArrayElement(vm.get("v7"), vm.get("v22")));
    vm.put("v24", vm.get("v23"));
    vm.put("v25", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v26", em.op("iadd").eval(vm.get("v25"),vm.get("v1")));
    vm.put("v27", om.popArrayElement(vm.get("v7"), vm.get("v26")));
    vm.put("v28", om.popArrayElement(vm.get("v7"), vm.get("param0")));
    vm.put("param1", vm.get("v28"));
    vm.put("v29", em.op("iadd").eval(vm.get("v25"),vm.get("v0")));
    vm.put("v30", om.popArrayElement(vm.get("v7"), vm.get("v29")));
    if (!em.le(vm.get("v14"), vm.get("v0"))) e.abort(String.format("!((v14=%s) le (v0=%s))", vm.get("v14"), vm.get("v0")));
    if (!em.ge(vm.get("v8"), vm.get("v21"))) e.abort(String.format("!((v8=%s) ge (v21=%s))", vm.get("v8"), vm.get("v21")));
    if (!em.le(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) le (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ne(vm.get("v7"), vm.get("v3"))) e.abort(String.format("!((v7=%s) ne (v3=%s))", vm.get("v7"), vm.get("v3")));
    if (!em.ge(vm.get("param0"), vm.get("v0"))) e.abort(String.format("!((param0=%s) ge (v0=%s))", vm.get("param0"), vm.get("v0")));
    if (!em.lt(vm.get("v0"), vm.get("v21"))) e.abort(String.format("!((v0=%s) lt (v21=%s))", vm.get("v0"), vm.get("v21")));
    if (!em.lt(vm.get("v1"), vm.get("v21"))) e.abort(String.format("!((v1=%s) lt (v21=%s))", vm.get("v1"), vm.get("v21")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort776(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getStatic("java.util.TimSort.$assertionsDisabled"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "elementData"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)0);
    vm.put("v11", (int)2);
    vm.put("v12", null);
    vm.put("v13", em.op("isub").eval(vm.get("v5"),vm.get("v10")));
    vm.put("v14", om.getArrayLength(vm.get("v9")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", vm.get("v0"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.le(vm.get("v5"), vm.get("v15"))) e.abort(String.format("!((v5=%s) le (v15=%s))", vm.get("v5"), vm.get("v15")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.eq(vm.get("v7"), vm.get("v10"))) e.abort(String.format("!((v7=%s) eq (v10=%s))", vm.get("v7"), vm.get("v10")));
    if (!em.le(vm.get("v10"), vm.get("v5"))) e.abort(String.format("!((v10=%s) le (v5=%s))", vm.get("v10"), vm.get("v5")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.lt(vm.get("v13"), vm.get("v11"))) e.abort(String.format("!((v13=%s) lt (v11=%s))", vm.get("v13"), vm.get("v11")));
    if (!em.le(vm.get("v10"), vm.get("v5"))) e.abort(String.format("!((v10=%s) le (v5=%s))", vm.get("v10"), vm.get("v5")));
    if (!em.ne(vm.get("v9"), vm.get("v12"))) e.abort(String.format("!((v9=%s) ne (v12=%s))", vm.get("v9"), vm.get("v12")));
    if (!em.eq(vm.get("v3"), vm.get("v10"))) e.abort(String.format("!((v3=%s) eq (v10=%s))", vm.get("v3"), vm.get("v10")));
    if (!em.le(vm.get("v5"), vm.get("v15"))) e.abort(String.format("!((v5=%s) le (v15=%s))", vm.get("v5"), vm.get("v15")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.elementData[*]","this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove1198(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)2);
    vm.put("v2", (int)0);
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", null);
    vm.put("v7", om.getField(vm.get("this"), "size"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getArrayElement(vm.get("v5"), vm.get("v2")));
    vm.put("v10", vm.get("v9"));
    om.revertArrayElement(vm.get("v5"), vm.get("v0"), vm.get("v10"));
    vm.put("v11", om.getArrayElement(vm.get("v5"), vm.get("v1")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getArrayElement(vm.get("v5"), vm.get("v0")));
    vm.put("v14", om.popArrayElement(vm.get("v5"), vm.get("v8")));
    vm.put("v15", vm.get("v3"));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v16"));
    vm.put("v17", em.op("iadd").eval(vm.get("v8"),vm.get("v0")));
    om.revertField(vm.get("this"), "size", vm.get("v17"));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v2")));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v0")));
    om.revertArrayElement(vm.get("v5"), vm.get("v2"));
    vm.put("v20", om.getArrayElement(vm.get("v5"), vm.get("v2")));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.eq(vm.get("v20"), vm.get("v6"))) e.abort(String.format("!((v20=%s) eq (v6=%s))", vm.get("v20"), vm.get("v6")));
    if (!em.gt(vm.get("v19"), vm.get("v2"))) e.abort(String.format("!((v19=%s) gt (v2=%s))", vm.get("v19"), vm.get("v2")));
    if (!em.lt(vm.get("v0"), vm.get("v19"))) e.abort(String.format("!((v0=%s) lt (v19=%s))", vm.get("v0"), vm.get("v19")));
    if (!em.lt(vm.get("v2"), vm.get("v19"))) e.abort(String.format("!((v2=%s) lt (v19=%s))", vm.get("v2"), vm.get("v19")));
    if (!em.ge(vm.get("v1"), vm.get("v19"))) e.abort(String.format("!((v1=%s) ge (v19=%s))", vm.get("v1"), vm.get("v19")));
    if (!em.eq(vm.get("param0"), vm.get("v6"))) e.abort(String.format("!((param0=%s) eq (v6=%s))", vm.get("param0"), vm.get("v6")));
    if (!em.lt(vm.get("v2"), vm.get("v17"))) e.abort(String.format("!((v2=%s) lt (v17=%s))", vm.get("v2"), vm.get("v17")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(Ljava/lang/Object;)Z",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove1158(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "elementData"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", (int)1);
    vm.put("v4", null);
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayElement(vm.get("v2"), vm.get("v0")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.popArrayElement(vm.get("v2"), vm.get("v7")));
    vm.put("v11", em.op("iadd").eval(vm.get("v7"),vm.get("v3")));
    om.revertField(vm.get("this"), "size", vm.get("v11"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v0")));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v3")));
    vm.put("v14", vm.get("v5"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v15"));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.lt(vm.get("v0"), vm.get("v11"))) e.abort(String.format("!((v0=%s) lt (v11=%s))", vm.get("v0"), vm.get("v11")));
    if (!em.eq(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) eq (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.gt(vm.get("v13"), vm.get("v0"))) e.abort(String.format("!((v13=%s) gt (v0=%s))", vm.get("v13"), vm.get("v0")));
    if (!em.eq(vm.get("v9"), vm.get("v4"))) e.abort(String.format("!((v9=%s) eq (v4=%s))", vm.get("v9"), vm.get("v4")));
    if (!em.ge(vm.get("v0"), vm.get("v13"))) e.abort(String.format("!((v0=%s) ge (v13=%s))", vm.get("v0"), vm.get("v13")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort787(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)0);
    vm.put("v9", (int)7);
    vm.put("v10", java.lang.Object[].class);
    vm.put("v11", null);
    vm.put("v12", om.getArrayLength(vm.get("v7")));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", em.op("isub").eval(vm.get("v5"),vm.get("v8")));
    vm.put("v15", em.op("isub").eval(vm.get("v13"),vm.get("v8")));
    vm.put("v16", em.op("isub").eval(vm.get("v5"),vm.get("v8")));
    vm.put("v17", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v7")));
    vm.put("v18", vm.get("v0"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ne(vm.get("v17"), vm.get("v10"))) e.abort(String.format("!((v17=%s) ne (v10=%s))", vm.get("v17"), vm.get("v10")));
    if (!em.le(vm.get("v15"), vm.get("v14"))) e.abort(String.format("!((v15=%s) le (v14=%s))", vm.get("v15"), vm.get("v14")));
    if (!em.lt(vm.get("v8"), vm.get("v5"))) e.abort(String.format("!((v8=%s) lt (v5=%s))", vm.get("v8"), vm.get("v5")));
    if (!em.le(vm.get("v5"), vm.get("v13"))) e.abort(String.format("!((v5=%s) le (v13=%s))", vm.get("v5"), vm.get("v13")));
    if (!em.ge(vm.get("v1"), vm.get("v5"))) e.abort(String.format("!((v1=%s) ge (v5=%s))", vm.get("v1"), vm.get("v5")));
    if (!em.ge(vm.get("v14"), vm.get("v8"))) e.abort(String.format("!((v14=%s) ge (v8=%s))", vm.get("v14"), vm.get("v8")));
    if (!em.ne(vm.get("v3"), vm.get("v8"))) e.abort(String.format("!((v3=%s) ne (v8=%s))", vm.get("v3"), vm.get("v8")));
    if (!em.lt(vm.get("v16"), vm.get("v9"))) e.abort(String.format("!((v16=%s) lt (v9=%s))", vm.get("v16"), vm.get("v9")));
    if (!em.ge(vm.get("v8"), vm.get("v15"))) e.abort(String.format("!((v8=%s) ge (v15=%s))", vm.get("v8"), vm.get("v15")));
    if (!em.eq(vm.get("param0"), vm.get("v11"))) e.abort(String.format("!((param0=%s) eq (v11=%s))", vm.get("param0"), vm.get("v11")));
    if (!em.le(vm.get("v8"), vm.get("v5"))) e.abort(String.format("!((v8=%s) le (v5=%s))", vm.get("v8"), vm.get("v5")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort735(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2);
    vm.put("v1", (int)1);
    vm.put("v2", (int)0);
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getArrayElement(vm.get("v5"), vm.get("v2")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "size"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", (int)7);
    vm.put("v13", (int)3);
    vm.put("v14", java.lang.Object[].class);
    vm.put("v15", null);
    vm.put("v16", om.getArrayLength(vm.get("v5")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.getArrayElement(vm.get("v5"), vm.get("v0")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", om.getArrayElement(vm.get("v5"), vm.get("v1")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", em.op("isub").eval(vm.get("v17"),vm.get("v2")));
    vm.put("v23", em.op("isub").eval(vm.get("v9"),vm.get("v2")));
    vm.put("v24", em.op("isub").eval(vm.get("v9"),vm.get("v2")));
    vm.put("v25", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v5")));
    vm.put("v26", vm.get("v3"));
    vm.put("v27", em.op("isub").eval(vm.get("v26"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v27"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ne(vm.get("v11"), vm.get("v2"))) e.abort(String.format("!((v11=%s) ne (v2=%s))", vm.get("v11"), vm.get("v2")));
    if (!em.le(vm.get("v22"), vm.get("v24"))) e.abort(String.format("!((v22=%s) le (v24=%s))", vm.get("v22"), vm.get("v24")));
    if (!em.eq(vm.get("v25"), vm.get("v14"))) e.abort(String.format("!((v25=%s) eq (v14=%s))", vm.get("v25"), vm.get("v14")));
    if (!em.ge(vm.get("v24"), vm.get("v2"))) e.abort(String.format("!((v24=%s) ge (v2=%s))", vm.get("v24"), vm.get("v2")));
    if (!em.ge(vm.get("v2"), vm.get("v9"))) e.abort(String.format("!((v2=%s) ge (v9=%s))", vm.get("v2"), vm.get("v9")));
    if (!em.lt(vm.get("v1"), vm.get("v22"))) e.abort(String.format("!((v1=%s) lt (v22=%s))", vm.get("v1"), vm.get("v22")));
    if (!em.lt(vm.get("v2"), vm.get("v22"))) e.abort(String.format("!((v2=%s) lt (v22=%s))", vm.get("v2"), vm.get("v22")));
    if (!em.le(vm.get("v9"), vm.get("v17"))) e.abort(String.format("!((v9=%s) le (v17=%s))", vm.get("v9"), vm.get("v17")));
    if (!em.lt(vm.get("v23"), vm.get("v12"))) e.abort(String.format("!((v23=%s) lt (v12=%s))", vm.get("v23"), vm.get("v12")));
    if (!em.le(vm.get("v2"), vm.get("v9"))) e.abort(String.format("!((v2=%s) le (v9=%s))", vm.get("v2"), vm.get("v9")));
    if (!em.ge(vm.get("v13"), vm.get("v22"))) e.abort(String.format("!((v13=%s) ge (v22=%s))", vm.get("v13"), vm.get("v22")));
    if (!em.lt(vm.get("v0"), vm.get("v22"))) e.abort(String.format("!((v0=%s) lt (v22=%s))", vm.get("v0"), vm.get("v22")));
    if (!em.eq(vm.get("param0"), vm.get("v15"))) e.abort(String.format("!((param0=%s) eq (v15=%s))", vm.get("param0"), vm.get("v15")));
  }
  @IM(clazz="java.util.ArrayList", name="trimToSize", desc="()V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void trimToSize3697(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "size"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getArrayLength(vm.get("v5")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", vm.get("v2"));
    vm.put("v9", em.op("isub").eval(vm.get("v8"),vm.get("v3")));
    om.revertField(vm.get("this"), "modCount", vm.get("v9"));
    if (!em.ge(vm.get("v1"), vm.get("v7"))) e.abort(String.format("!((v1=%s) ge (v7=%s))", vm.get("v1"), vm.get("v7")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort741(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "elementData"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getArrayLength(vm.get("v1")));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "size"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", (int)1);
    vm.put("v10", (int)0);
    vm.put("v11", (int)7);
    vm.put("v12", java.lang.Object[].class);
    vm.put("v13", null);
    vm.put("v14", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v1")));
    vm.put("v15", em.op("isub").eval(vm.get("v7"),vm.get("v10")));
    vm.put("v16", em.op("isub").eval(vm.get("v7"),vm.get("v10")));
    vm.put("v17", em.op("isub").eval(vm.get("v5"),vm.get("v10")));
    vm.put("v18", vm.get("v8"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v9")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.lt(vm.get("v16"), vm.get("v11"))) e.abort(String.format("!((v16=%s) lt (v11=%s))", vm.get("v16"), vm.get("v11")));
    if (!em.eq(vm.get("param0"), vm.get("v13"))) e.abort(String.format("!((param0=%s) eq (v13=%s))", vm.get("param0"), vm.get("v13")));
    if (!em.ge(vm.get("v10"), vm.get("v15"))) e.abort(String.format("!((v10=%s) ge (v15=%s))", vm.get("v10"), vm.get("v15")));
    if (!em.ge(vm.get("v15"), vm.get("v10"))) e.abort(String.format("!((v15=%s) ge (v10=%s))", vm.get("v15"), vm.get("v10")));
    if (!em.ge(vm.get("v10"), vm.get("v7"))) e.abort(String.format("!((v10=%s) ge (v7=%s))", vm.get("v10"), vm.get("v7")));
    if (!em.le(vm.get("v10"), vm.get("v7"))) e.abort(String.format("!((v10=%s) le (v7=%s))", vm.get("v10"), vm.get("v7")));
    if (!em.gt(vm.get("v17"), vm.get("v15"))) e.abort(String.format("!((v17=%s) gt (v15=%s))", vm.get("v17"), vm.get("v15")));
    if (!em.le(vm.get("v7"), vm.get("v5"))) e.abort(String.format("!((v7=%s) le (v5=%s))", vm.get("v7"), vm.get("v5")));
    if (!em.ne(vm.get("v14"), vm.get("v12"))) e.abort(String.format("!((v14=%s) ne (v12=%s))", vm.get("v14"), vm.get("v12")));
    if (!em.ne(vm.get("v3"), vm.get("v10"))) e.abort(String.format("!((v3=%s) ne (v10=%s))", vm.get("v3"), vm.get("v10")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort798(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "elementData"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getArrayElement(vm.get("v8"), vm.get("v1")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getArrayElement(vm.get("v8"), vm.get("v0")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", (int)7);
    vm.put("v14", null);
    vm.put("v15", (int)2);
    vm.put("v16", java.lang.Object[].class);
    vm.put("v17", em.op("isub").eval(vm.get("v3"),vm.get("v1")));
    vm.put("v18", om.getArrayLength(vm.get("v8")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", em.op("isub").eval(vm.get("v19"),vm.get("v1")));
    vm.put("v21", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v8")));
    vm.put("v22", em.op("isub").eval(vm.get("v3"),vm.get("v1")));
    vm.put("v23", vm.get("v4"));
    vm.put("v24", em.op("isub").eval(vm.get("v23"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v24"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.ne(vm.get("v6"), vm.get("v1"))) e.abort(String.format("!((v6=%s) ne (v1=%s))", vm.get("v6"), vm.get("v1")));
    if (!em.ne(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) ne (v14=%s))", vm.get("param0"), vm.get("v14")));
    if (!em.le(vm.get("v3"), vm.get("v19"))) e.abort(String.format("!((v3=%s) le (v19=%s))", vm.get("v3"), vm.get("v19")));
    if (!em.ne(vm.get("param0"), vm.get("v14"))) e.abort(String.format("!((param0=%s) ne (v14=%s))", vm.get("param0"), vm.get("v14")));
    if (!em.lt(vm.get("v1"), vm.get("v3"))) e.abort(String.format("!((v1=%s) lt (v3=%s))", vm.get("v1"), vm.get("v3")));
    if (!em.ge(vm.get("v0"), vm.get("v3"))) e.abort(String.format("!((v0=%s) ge (v3=%s))", vm.get("v0"), vm.get("v3")));
    if (!em.lt(vm.get("v0"), vm.get("v20"))) e.abort(String.format("!((v0=%s) lt (v20=%s))", vm.get("v0"), vm.get("v20")));
    if (!em.le(vm.get("v20"), vm.get("v22"))) e.abort(String.format("!((v20=%s) le (v22=%s))", vm.get("v20"), vm.get("v22")));
    if (!em.lt(vm.get("v1"), vm.get("v20"))) e.abort(String.format("!((v1=%s) lt (v20=%s))", vm.get("v1"), vm.get("v20")));
    if (!em.ne(vm.get("v21"), vm.get("v16"))) e.abort(String.format("!((v21=%s) ne (v16=%s))", vm.get("v21"), vm.get("v16")));
    if (!em.le(vm.get("v1"), vm.get("v3"))) e.abort(String.format("!((v1=%s) le (v3=%s))", vm.get("v1"), vm.get("v3")));
    if (!em.lt(vm.get("v17"), vm.get("v13"))) e.abort(String.format("!((v17=%s) lt (v13=%s))", vm.get("v17"), vm.get("v13")));
    if (!em.ge(vm.get("v15"), vm.get("v20"))) e.abort(String.format("!((v15=%s) ge (v20=%s))", vm.get("v15"), vm.get("v20")));
    if (!em.ge(vm.get("v22"), vm.get("v1"))) e.abort(String.format("!((v22=%s) ge (v1=%s))", vm.get("v22"), vm.get("v1")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort728(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)0);
    vm.put("v9", (int)7);
    vm.put("v10", java.lang.Object[].class);
    vm.put("v11", null);
    vm.put("v12", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v7")));
    vm.put("v13", em.op("isub").eval(vm.get("v3"),vm.get("v8")));
    vm.put("v14", om.getArrayLength(vm.get("v7")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v8")));
    vm.put("v17", em.op("isub").eval(vm.get("v3"),vm.get("v8")));
    vm.put("v18", vm.get("v0"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.le(vm.get("v16"), vm.get("v13"))) e.abort(String.format("!((v16=%s) le (v13=%s))", vm.get("v16"), vm.get("v13")));
    if (!em.le(vm.get("v8"), vm.get("v3"))) e.abort(String.format("!((v8=%s) le (v3=%s))", vm.get("v8"), vm.get("v3")));
    if (!em.ge(vm.get("v8"), vm.get("v3"))) e.abort(String.format("!((v8=%s) ge (v3=%s))", vm.get("v8"), vm.get("v3")));
    if (!em.le(vm.get("v3"), vm.get("v15"))) e.abort(String.format("!((v3=%s) le (v15=%s))", vm.get("v3"), vm.get("v15")));
    if (!em.eq(vm.get("v12"), vm.get("v10"))) e.abort(String.format("!((v12=%s) eq (v10=%s))", vm.get("v12"), vm.get("v10")));
    if (!em.lt(vm.get("v17"), vm.get("v9"))) e.abort(String.format("!((v17=%s) lt (v9=%s))", vm.get("v17"), vm.get("v9")));
    if (!em.ge(vm.get("v8"), vm.get("v16"))) e.abort(String.format("!((v8=%s) ge (v16=%s))", vm.get("v8"), vm.get("v16")));
    if (!em.eq(vm.get("param0"), vm.get("v11"))) e.abort(String.format("!((param0=%s) eq (v11=%s))", vm.get("param0"), vm.get("v11")));
    if (!em.ge(vm.get("v13"), vm.get("v8"))) e.abort(String.format("!((v13=%s) ge (v8=%s))", vm.get("v13"), vm.get("v8")));
    if (!em.ne(vm.get("v5"), vm.get("v8"))) e.abort(String.format("!((v5=%s) ne (v8=%s))", vm.get("v5"), vm.get("v8")));
  }
  @IM(clazz="java.util.ArrayList", name="retainAll", desc="(Ljava/util/Collection;)Z",revertedInput={"this.elementData[*]"},definedOutput={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void retainAll1357(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "size"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", (int)2);
    vm.put("v7", null);
    vm.put("v8", om.getArrayElement(vm.get("v5"), vm.get("v0")));
    vm.put("v9", vm.get("v8"));
    om.revertArrayElement(vm.get("v5"), vm.get("v0"), vm.get("v9"));
    vm.put("v10", em.op("isub").eval(vm.get("v3"),vm.get("v1")));
    vm.put("v11", em.op("isub").eval(vm.get("v3"),vm.get("v1")));
    vm.put("v12", om.getArrayElement(vm.get("v5"), vm.get("v1")));
    vm.put("v13", vm.get("v12"));
    om.revertArrayElement(vm.get("v5"), vm.get("v1"), vm.get("v13"));
    vm.put("v14", em.op("iadd").eval(vm.get("v1"),vm.get("v11")));
    vm.put("param0", om.newDefaultValue("java.util.Collection"));
    if (!em.lt(vm.get("v0"), vm.get("v10"))) e.abort(String.format("!((v0=%s) lt (v10=%s))", vm.get("v0"), vm.get("v10")));
    if (!em.ge(vm.get("v6"), vm.get("v10"))) e.abort(String.format("!((v6=%s) ge (v10=%s))", vm.get("v6"), vm.get("v10")));
    if (!em.ne(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) ne (v7=%s))", vm.get("param0"), vm.get("v7")));
    if (!em.ge(vm.get("v1"), vm.get("v3"))) e.abort(String.format("!((v1=%s) ge (v3=%s))", vm.get("v1"), vm.get("v3")));
    if (!em.eq(vm.get("v14"), vm.get("v3"))) e.abort(String.format("!((v14=%s) eq (v3=%s))", vm.get("v14"), vm.get("v3")));
    if (!em.ne(vm.get("v1"), vm.get("v3"))) e.abort(String.format("!((v1=%s) ne (v3=%s))", vm.get("v1"), vm.get("v3")));
    if (!em.lt(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) lt (v10=%s))", vm.get("v1"), vm.get("v10")));
  }
  @IM(clazz="java.util.ArrayList", name="remove", desc="(I)Ljava/lang/Object;",revertedInput={"this.size","this.modCount"},definedOutput={"this.elementData[*]","this.size","this.modCount"},delta={"this.elementData[*]"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void remove3397(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "elementData"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "size"));
    vm.put("v5", null);
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", vm.get("v4"));
    vm.put("v8", om.popArrayElement(vm.get("v3"), vm.get("v7")));
    vm.put("v9", em.op("iadd").eval(vm.get("v7"),vm.get("v0")));
    om.revertField(vm.get("this"), "size", vm.get("v9"));
    vm.put("v10", vm.get("v6"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    vm.put("v12", om.guessArrayIndex(vm.get("v3")));
    vm.put("param0", vm.get("v12"));
    vm.put("v13", om.getArrayElement(vm.get("v3"), vm.get("param0")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v9"),vm.get("param0")));
    vm.put("v16", em.op("iadd").eval(vm.get("param0"),vm.get("v0")));
    vm.put("v17", em.op("isub").eval(vm.get("v15"),vm.get("v0")));
    vm.put("v18", em.op("iadd").eval(vm.get("param0"),vm.get("v1")));
    vm.put("v19", om.popArrayElement(vm.get("v3"), vm.get("v18")));
    vm.put("v20", em.op("iadd").eval(vm.get("v16"),vm.get("v1")));
    vm.put("v21", om.getArrayElement(vm.get("v3"), vm.get("v20")));
    vm.put("v22", vm.get("v21"));
    if (!em.ge(vm.get("v0"), vm.get("v17"))) e.abort(String.format("!((v0=%s) ge (v17=%s))", vm.get("v0"), vm.get("v17")));
    if (!em.lt(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) lt (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.lt(vm.get("v1"), vm.get("v17"))) e.abort(String.format("!((v1=%s) lt (v17=%s))", vm.get("v1"), vm.get("v17")));
    if (!em.gt(vm.get("v17"), vm.get("v1"))) e.abort(String.format("!((v17=%s) gt (v1=%s))", vm.get("v17"), vm.get("v1")));
  }
  @IM(clazz="java.util.ArrayList", name="sort", desc="(Ljava/util/Comparator;)V",revertedInput={"this.modCount"},definedOutput={"this.modCount"})
  @DG(pred={"_init_1497","_init_3799","_init_1499"})
  static void sort779(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "elementData"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getArrayLength(vm.get("v2")));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getStatic("java.util.Arrays$LegacyMergeSort.userRequested"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "size"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("this"), "modCount"));
    vm.put("v10", (int)1);
    vm.put("v11", (int)7);
    vm.put("v12", null);
    vm.put("v13", java.lang.Object[].class);
    vm.put("v14", om.getArrayElement(vm.get("v2"), vm.get("v0")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v2")));
    vm.put("v17", em.op("isub").eval(vm.get("v8"),vm.get("v0")));
    vm.put("v18", em.op("isub").eval(vm.get("v8"),vm.get("v0")));
    vm.put("v19", em.op("isub").eval(vm.get("v4"),vm.get("v0")));
    vm.put("v20", vm.get("v9"));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v10")));
    om.revertField(vm.get("this"), "modCount", vm.get("v21"));
    vm.put("param0", om.newDefaultValue("java.util.Comparator"));
    if (!em.lt(vm.get("v18"), vm.get("v11"))) e.abort(String.format("!((v18=%s) lt (v11=%s))", vm.get("v18"), vm.get("v11")));
    if (!em.ne(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) ne (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.eq(vm.get("param0"), vm.get("v12"))) e.abort(String.format("!((param0=%s) eq (v12=%s))", vm.get("param0"), vm.get("v12")));
    if (!em.ge(vm.get("v17"), vm.get("v0"))) e.abort(String.format("!((v17=%s) ge (v0=%s))", vm.get("v17"), vm.get("v0")));
    if (!em.le(vm.get("v8"), vm.get("v4"))) e.abort(String.format("!((v8=%s) le (v4=%s))", vm.get("v8"), vm.get("v4")));
    if (!em.ge(vm.get("v0"), vm.get("v8"))) e.abort(String.format("!((v0=%s) ge (v8=%s))", vm.get("v0"), vm.get("v8")));
    if (!em.gt(vm.get("v19"), vm.get("v17"))) e.abort(String.format("!((v19=%s) gt (v17=%s))", vm.get("v19"), vm.get("v17")));
    if (!em.eq(vm.get("v16"), vm.get("v13"))) e.abort(String.format("!((v16=%s) eq (v13=%s))", vm.get("v16"), vm.get("v13")));
    if (!em.lt(vm.get("v0"), vm.get("v17"))) e.abort(String.format("!((v0=%s) lt (v17=%s))", vm.get("v0"), vm.get("v17")));
    if (!em.ne(vm.get("v6"), vm.get("v0"))) e.abort(String.format("!((v6=%s) ne (v0=%s))", vm.get("v6"), vm.get("v0")));
    if (!em.ge(vm.get("v10"), vm.get("v17"))) e.abort(String.format("!((v10=%s) ge (v17=%s))", vm.get("v10"), vm.get("v17")));
    if (!em.le(vm.get("v0"), vm.get("v8"))) e.abort(String.format("!((v0=%s) le (v8=%s))", vm.get("v0"), vm.get("v8")));
  }
}
