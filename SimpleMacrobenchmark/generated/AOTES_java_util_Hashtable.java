import org.javelus.aotes.executor.*;
@Defined({"this.threshold","this.keySet.c","this.entrySet.c","this.keySet.mutex","this.count","this.entrySet.c.this$0","this.table[*].value","this.values","this.values.c","this.keySet.c.this$0","this.table[*]","this.values.mutex","this.table[*].key","this.table","this.values.c.this$0","this.entrySet.mutex","this.modCount","this.loadFactor","this.table[*].next","this.keySet","this.entrySet","this.table[*].hash"})
public class AOTES_java_util_Hashtable {
  @IM(clazz="java.util.Hashtable", name="put", desc="(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.table[*].value","this.modCount","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void put2249(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "table"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "threshold"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayLength(vm.get("v2")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "count"));
    vm.put("v8", (int)1);
    vm.put("v9", om.getField(vm.get("this"), "modCount"));
    vm.put("v10", null);
    vm.put("v11", vm.get("v7"));
    vm.put("v12", vm.get("v9"));
    vm.put("v13", em.op("isub").eval(vm.get("v11"),vm.get("v8")));
    om.revertField(vm.get("this"), "count", vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v12"),vm.get("v8")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    vm.put("v15", om.guessArrayIndex(vm.get("v2")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getArrayElement(vm.get("v2"), vm.get("v16")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.popField(vm.get("v18"), "value"));
    vm.put("param1", vm.get("v19"));
    vm.put("v20", om.popField(vm.get("v18"), "hash"));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.popField(vm.get("v18"), "next"));
    vm.put("v23", vm.get("v22"));
    om.revertArrayElement(vm.get("v2"), vm.get("v16"), vm.get("v23"));
    vm.put("v24", om.getField(vm.get("v23"), "hash"));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", om.popField(vm.get("v18"), "key"));
    vm.put("param0", vm.get("v26"));
    vm.put("v27", em.op("iand").eval(vm.get("v21"),vm.get("v0")));
    vm.put("v28", om.getField(vm.get("v23"), "next"));
    vm.put("v29", vm.get("v28"));
    vm.put("v30", om.getField(vm.get("v29"), "next"));
    vm.put("v31", vm.get("v30"));
    vm.put("v32", om.getField(vm.get("v29"), "hash"));
    vm.put("v33", vm.get("v32"));
    if (!em.eq(vm.get("v16"), (em.op("irem").eval(vm.get("v27"),vm.get("v6"))))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (em.op("irem").eval(vm.get("v27"),vm.get("v6"))));
    if (!em.lt(vm.get("v13"), vm.get("v4"))) e.abort(String.format("!((v13=%s) lt (v4=%s))", vm.get("v13"), vm.get("v4")));
    if (!em.ne(vm.get("v29"), vm.get("v10"))) e.abort(String.format("!((v29=%s) ne (v10=%s))", vm.get("v29"), vm.get("v10")));
    if (!em.eq(vm.get("v31"), vm.get("v10"))) e.abort(String.format("!((v31=%s) eq (v10=%s))", vm.get("v31"), vm.get("v10")));
    if (!em.ne(vm.get("v25"), vm.get("v21"))) e.abort(String.format("!((v25=%s) ne (v21=%s))", vm.get("v25"), vm.get("v21")));
    if (!em.ne(vm.get("v23"), vm.get("v10"))) e.abort(String.format("!((v23=%s) ne (v10=%s))", vm.get("v23"), vm.get("v10")));
    if (!em.ne(vm.get("param1"), vm.get("v10"))) e.abort(String.format("!((param1=%s) ne (v10=%s))", vm.get("param1"), vm.get("v10")));
    if (!em.ne(vm.get("v33"), vm.get("v21"))) e.abort(String.format("!((v33=%s) ne (v21=%s))", vm.get("v33"), vm.get("v21")));
  }
  @IM(clazz="java.util.Hashtable", name="hashCode", desc="()I",revertedInput={"this.loadFactor"},definedOutput={"this.loadFactor"})
  @DG(pred={"merge43","clear3699","merge63","_init_11307","clear3683","_init_1095","put2249","put2290","_init_1073","addEntry2796","_init_3127","merge98","_init_1898","putIfAbsent1289","clear3606","_init_1855","_init_1892","_init_11308","_init_1879","clear3695","_init_1097","putIfAbsent1297","_init_1094","merge68","putIfAbsent1281","_init_1099","_init_11311","_init_11285","_init_11321","_init_11325","addEntry2799","_init_1889","clear3646","_init_1877","_init_11304","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","_init_1896","put2286","_init_1897","put2257","_init_11328","merge60","_init_1895","_init_1899","_init_11287","merge76"})
  static void hashCode2442(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "table"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getArrayElement(vm.get("v3"), vm.get("v1")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("v5"), "next"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("this"), "count"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("this"), "loadFactor"));
    vm.put("v13", (int)2);
    vm.put("v14", null);
    vm.put("v15", (float)0.0F);
    vm.put("v16", om.getArrayLength(vm.get("v3")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.getField(vm.get("v9"), "next"));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", om.getField(vm.get("v19"), "next"));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.getField(vm.get("v21"), "next"));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", vm.get("v12"));
    vm.put("v25", em.op("fneg").eval(vm.get("v24")));
    vm.put("v26", em.op("fneg").eval(vm.get("v25")));
    om.revertField(vm.get("this"), "loadFactor", vm.get("v26"));
    vm.put("v27", em.op("fcmpg").eval(vm.get("v26"),vm.get("v15")));
    if (!em.eq(vm.get("v7"), vm.get("v14"))) e.abort(String.format("!((v7=%s) eq (v14=%s))", vm.get("v7"), vm.get("v14")));
    if (!em.ge(vm.get("v27"), vm.get("v0"))) e.abort(String.format("!((v27=%s) ge (v0=%s))", vm.get("v27"), vm.get("v0")));
    if (!em.ne(vm.get("v9"), vm.get("v14"))) e.abort(String.format("!((v9=%s) ne (v14=%s))", vm.get("v9"), vm.get("v14")));
    if (!em.eq(vm.get("v23"), vm.get("v14"))) e.abort(String.format("!((v23=%s) eq (v14=%s))", vm.get("v23"), vm.get("v14")));
    if (!em.ne(vm.get("v21"), vm.get("v14"))) e.abort(String.format("!((v21=%s) ne (v14=%s))", vm.get("v21"), vm.get("v14")));
    if (!em.ge(vm.get("v13"), vm.get("v17"))) e.abort(String.format("!((v13=%s) ge (v17=%s))", vm.get("v13"), vm.get("v17")));
    if (!em.lt(vm.get("v1"), vm.get("v17"))) e.abort(String.format("!((v1=%s) lt (v17=%s))", vm.get("v1"), vm.get("v17")));
    if (!em.ne(vm.get("v19"), vm.get("v14"))) e.abort(String.format("!((v19=%s) ne (v14=%s))", vm.get("v19"), vm.get("v14")));
    if (!em.ne(vm.get("v5"), vm.get("v14"))) e.abort(String.format("!((v5=%s) ne (v14=%s))", vm.get("v5"), vm.get("v14")));
    if (!em.ne(vm.get("v11"), vm.get("v0"))) e.abort(String.format("!((v11=%s) ne (v0=%s))", vm.get("v11"), vm.get("v0")));
    if (!em.lt(vm.get("v0"), vm.get("v17"))) e.abort(String.format("!((v0=%s) lt (v17=%s))", vm.get("v0"), vm.get("v17")));
  }
  @IM(clazz="java.util.Hashtable", name="put", desc="(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.table[*].value","this.modCount","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void put2299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "table"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "threshold"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "count"));
    vm.put("v6", (int)1);
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", null);
    vm.put("v9", om.getArrayLength(vm.get("v2")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", vm.get("v5"));
    vm.put("v12", vm.get("v7"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v6")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v11"),vm.get("v6")));
    om.revertField(vm.get("this"), "count", vm.get("v14"));
    vm.put("v15", om.guessArrayIndex(vm.get("v2")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getArrayElement(vm.get("v2"), vm.get("v16")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.popField(vm.get("v18"), "value"));
    vm.put("param1", vm.get("v19"));
    vm.put("v20", om.popField(vm.get("v18"), "hash"));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.popField(vm.get("v18"), "next"));
    vm.put("v23", vm.get("v22"));
    om.revertArrayElement(vm.get("v2"), vm.get("v16"), vm.get("v23"));
    vm.put("v24", om.popField(vm.get("v18"), "key"));
    vm.put("param0", vm.get("v24"));
    vm.put("v25", em.op("iand").eval(vm.get("v21"),vm.get("v0")));
    if (!em.eq(vm.get("v16"), (em.op("irem").eval(vm.get("v25"),vm.get("v10"))))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (em.op("irem").eval(vm.get("v25"),vm.get("v10"))));
    if (!em.eq(vm.get("v23"), vm.get("v8"))) e.abort(String.format("!((v23=%s) eq (v8=%s))", vm.get("v23"), vm.get("v8")));
    if (!em.ne(vm.get("param1"), vm.get("v8"))) e.abort(String.format("!((param1=%s) ne (v8=%s))", vm.get("param1"), vm.get("v8")));
    if (!em.lt(vm.get("v14"), vm.get("v4"))) e.abort(String.format("!((v14=%s) lt (v4=%s))", vm.get("v14"), vm.get("v4")));
  }
  @IM(clazz="java.util.Hashtable", name="putIfAbsent", desc="(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.modCount","this.table[*].value","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void putIfAbsent1209(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "modCount"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "count"));
    vm.put("v4", om.getField(vm.get("this"), "table"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "threshold"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", null);
    vm.put("v9", om.getArrayLength(vm.get("v5")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", vm.get("v1"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", vm.get("v3"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v2")));
    om.revertField(vm.get("this"), "count", vm.get("v14"));
    vm.put("v15", om.guessArrayIndex(vm.get("v5")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getArrayElement(vm.get("v5"), vm.get("v16")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.popField(vm.get("v18"), "key"));
    vm.put("param0", vm.get("v19"));
    vm.put("v20", om.popField(vm.get("v18"), "value"));
    vm.put("param1", vm.get("v20"));
    vm.put("v21", om.popField(vm.get("v18"), "next"));
    vm.put("v22", vm.get("v21"));
    om.revertArrayElement(vm.get("v5"), vm.get("v16"), vm.get("v22"));
    vm.put("v23", em.op("java.lang.Object.hashCode()I").eval(vm.get("param0")));
    vm.put("v24", em.op("iand").eval(vm.get("v23"),vm.get("v0")));
    vm.put("v25", om.getField(vm.get("v22"), "next"));
    vm.put("v26", vm.get("v25"));
    vm.put("v27", om.getField(vm.get("v26"), "hash"));
    vm.put("v28", vm.get("v27"));
    vm.put("v29", om.getField(vm.get("v26"), "next"));
    vm.put("v30", vm.get("v29"));
    vm.put("v31", om.getField(vm.get("v22"), "hash"));
    vm.put("v32", vm.get("v31"));
    vm.put("v33", om.popField(vm.get("v18"), "hash"));
    if (!em.eq(vm.get("v23"), (vm.get("v33")))) e.abort("Inconsistent value for \"v23\": " + vm.get("v23") + " ne " + (vm.get("v33")));
    if (!em.eq(vm.get("v16"), (em.op("irem").eval(vm.get("v24"),vm.get("v10"))))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (em.op("irem").eval(vm.get("v24"),vm.get("v10"))));
    if (!em.ne(vm.get("v26"), vm.get("v8"))) e.abort(String.format("!((v26=%s) ne (v8=%s))", vm.get("v26"), vm.get("v8")));
    if (!em.ne(vm.get("v22"), vm.get("v8"))) e.abort(String.format("!((v22=%s) ne (v8=%s))", vm.get("v22"), vm.get("v8")));
    if (!em.ne(vm.get("v28"), vm.get("v23"))) e.abort(String.format("!((v28=%s) ne (v23=%s))", vm.get("v28"), vm.get("v23")));
    if (!em.eq(vm.get("v30"), vm.get("v8"))) e.abort(String.format("!((v30=%s) eq (v8=%s))", vm.get("v30"), vm.get("v8")));
    if (!em.lt(vm.get("v14"), vm.get("v7"))) e.abort(String.format("!((v14=%s) lt (v7=%s))", vm.get("v14"), vm.get("v7")));
    if (!em.ne(vm.get("v32"), vm.get("v23"))) e.abort(String.format("!((v32=%s) ne (v23=%s))", vm.get("v32"), vm.get("v23")));
    if (!em.ne(vm.get("param1"), vm.get("v8"))) e.abort(String.format("!((param1=%s) ne (v8=%s))", vm.get("param1"), vm.get("v8")));
  }
  @IM(clazz="java.util.Hashtable", name="putIfAbsent", desc="(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.table[*].value","this.modCount","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void putIfAbsent1297(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "threshold"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "table"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "count"));
    vm.put("v6", (int)1);
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", om.getArrayLength(vm.get("v4")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", null);
    vm.put("v11", vm.get("v7"));
    vm.put("v12", vm.get("v5"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v6")));
    om.revertField(vm.get("this"), "count", vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v11"),vm.get("v6")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    vm.put("v15", om.guessArrayIndex(vm.get("v4")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getArrayElement(vm.get("v4"), vm.get("v16")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.popField(vm.get("v18"), "value"));
    vm.put("param1", vm.get("v19"));
    vm.put("v20", om.popField(vm.get("v18"), "next"));
    vm.put("v21", vm.get("v20"));
    om.revertArrayElement(vm.get("v4"), vm.get("v16"), vm.get("v21"));
    vm.put("v22", om.popField(vm.get("v18"), "hash"));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", em.op("iand").eval(vm.get("v23"),vm.get("v0")));
    vm.put("v25", om.popField(vm.get("v18"), "key"));
    vm.put("param0", vm.get("v25"));
    if (!em.eq(vm.get("v16"), (em.op("irem").eval(vm.get("v24"),vm.get("v9"))))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (em.op("irem").eval(vm.get("v24"),vm.get("v9"))));
    if (!em.lt(vm.get("v13"), vm.get("v2"))) e.abort(String.format("!((v13=%s) lt (v2=%s))", vm.get("v13"), vm.get("v2")));
    if (!em.ne(vm.get("param1"), vm.get("v10"))) e.abort(String.format("!((param1=%s) ne (v10=%s))", vm.get("param1"), vm.get("v10")));
    if (!em.eq(vm.get("v21"), vm.get("v10"))) e.abort(String.format("!((v21=%s) eq (v10=%s))", vm.get("v21"), vm.get("v10")));
  }
  @IM(clazz="java.util.Hashtable", name="entrySet", desc="()Ljava/util/Set;",revertedInput={"this.entrySet"},definedOutput={"this.entrySet.c","this.entrySet.mutex","this.entrySet.c.this$0","this.entrySet"},delta={"this.entrySet.c","this.entrySet.mutex","this.entrySet.c.this$0"})
  static void entrySet4595(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "entrySet"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", null);
    vm.put("v3", om.popField(vm.get("v1"), "mutex"));
    vm.put("v4", om.popField(vm.get("v1"), "c"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popField(vm.get("v5"), "this$0"));
    if (!em.eq(vm.get("this"), (vm.get("v6")))) e.abort("Inconsistent value for \"this\": " + vm.get("this") + " ne " + (vm.get("v6")));
    if (!em.eq(vm.get("this"), (vm.get("v3")))) e.abort("Inconsistent value for \"this\": " + vm.get("this") + " ne " + (vm.get("v3")));
    om.revertField(vm.get("this"), "entrySet");
    vm.put("v7", om.getField(vm.get("this"), "entrySet"));
    if (!em.eq(vm.get("v7"), vm.get("v2"))) e.abort(String.format("!((v7=%s) eq (v2=%s))", vm.get("v7"), vm.get("v2")));
  }
  @IM(clazz="java.util.Hashtable", name="put", desc="(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.modCount","this.table[*].value","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void put2290(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "table"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", (int)1);
    vm.put("v5", om.getField(vm.get("this"), "threshold"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getArrayLength(vm.get("v2")));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("this"), "count"));
    vm.put("v10", null);
    vm.put("v11", vm.get("v3"));
    vm.put("v12", vm.get("v9"));
    vm.put("v13", em.op("isub").eval(vm.get("v11"),vm.get("v4")));
    om.revertField(vm.get("this"), "modCount", vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v12"),vm.get("v4")));
    om.revertField(vm.get("this"), "count", vm.get("v14"));
    vm.put("v15", om.guessArrayIndex(vm.get("v2")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getArrayElement(vm.get("v2"), vm.get("v16")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.popField(vm.get("v18"), "key"));
    vm.put("param0", vm.get("v19"));
    vm.put("v20", om.popField(vm.get("v18"), "next"));
    vm.put("v21", vm.get("v20"));
    om.revertArrayElement(vm.get("v2"), vm.get("v16"), vm.get("v21"));
    vm.put("v22", om.getField(vm.get("v21"), "hash"));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.popField(vm.get("v18"), "hash"));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", em.op("iand").eval(vm.get("v25"),vm.get("v0")));
    vm.put("v27", om.getField(vm.get("v21"), "next"));
    vm.put("v28", vm.get("v27"));
    vm.put("v29", om.popField(vm.get("v18"), "value"));
    vm.put("param1", vm.get("v29"));
    if (!em.eq(vm.get("v16"), (em.op("irem").eval(vm.get("v26"),vm.get("v8"))))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (em.op("irem").eval(vm.get("v26"),vm.get("v8"))));
    if (!em.lt(vm.get("v14"), vm.get("v6"))) e.abort(String.format("!((v14=%s) lt (v6=%s))", vm.get("v14"), vm.get("v6")));
    if (!em.ne(vm.get("v23"), vm.get("v25"))) e.abort(String.format("!((v23=%s) ne (v25=%s))", vm.get("v23"), vm.get("v25")));
    if (!em.eq(vm.get("v28"), vm.get("v10"))) e.abort(String.format("!((v28=%s) eq (v10=%s))", vm.get("v28"), vm.get("v10")));
    if (!em.ne(vm.get("v21"), vm.get("v10"))) e.abort(String.format("!((v21=%s) ne (v10=%s))", vm.get("v21"), vm.get("v10")));
    if (!em.ne(vm.get("param1"), vm.get("v10"))) e.abort(String.format("!((param1=%s) ne (v10=%s))", vm.get("param1"), vm.get("v10")));
  }
  @IM(clazz="java.util.Hashtable", name="putIfAbsent", desc="(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.modCount","this.table[*].value","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void putIfAbsent1289(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "threshold"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "table"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "modCount"));
    vm.put("v6", (int)1);
    vm.put("v7", om.getField(vm.get("this"), "count"));
    vm.put("v8", null);
    vm.put("v9", om.getArrayLength(vm.get("v4")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", vm.get("v5"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v6")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", vm.get("v7"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v6")));
    om.revertField(vm.get("this"), "count", vm.get("v14"));
    vm.put("v15", om.guessArrayIndex(vm.get("v4")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getArrayElement(vm.get("v4"), vm.get("v16")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.popField(vm.get("v18"), "next"));
    vm.put("v20", vm.get("v19"));
    om.revertArrayElement(vm.get("v4"), vm.get("v16"), vm.get("v20"));
    vm.put("v21", om.popField(vm.get("v18"), "key"));
    vm.put("param0", vm.get("v21"));
    vm.put("v22", om.getField(vm.get("v20"), "next"));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.getField(vm.get("v20"), "hash"));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", om.popField(vm.get("v18"), "hash"));
    vm.put("v27", vm.get("v26"));
    vm.put("v28", em.op("iand").eval(vm.get("v27"),vm.get("v0")));
    vm.put("v29", om.popField(vm.get("v18"), "value"));
    vm.put("param1", vm.get("v29"));
    if (!em.eq(vm.get("v16"), (em.op("irem").eval(vm.get("v28"),vm.get("v10"))))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (em.op("irem").eval(vm.get("v28"),vm.get("v10"))));
    if (!em.eq(vm.get("v23"), vm.get("v8"))) e.abort(String.format("!((v23=%s) eq (v8=%s))", vm.get("v23"), vm.get("v8")));
    if (!em.lt(vm.get("v14"), vm.get("v2"))) e.abort(String.format("!((v14=%s) lt (v2=%s))", vm.get("v14"), vm.get("v2")));
    if (!em.ne(vm.get("param1"), vm.get("v8"))) e.abort(String.format("!((param1=%s) ne (v8=%s))", vm.get("param1"), vm.get("v8")));
    if (!em.ne(vm.get("v25"), vm.get("v27"))) e.abort(String.format("!((v25=%s) ne (v27=%s))", vm.get("v25"), vm.get("v27")));
    if (!em.ne(vm.get("v20"), vm.get("v8"))) e.abort(String.format("!((v20=%s) ne (v8=%s))", vm.get("v20"), vm.get("v8")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(I)V",definedOutput={"this.threshold","this.table","this.modCount","this.loadFactor"},delta={"this.threshold","this.table","this.modCount","this.loadFactor"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_1896(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "modCount"));
    vm.put("v1", (int)0);
    vm.put("v2", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v3", (float)0.75F);
    vm.put("v4", om.popField(vm.get("this"), "threshold"));
    vm.put("v5", om.popField(vm.get("this"), "table"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getArrayLength(vm.get("v6")));
    vm.put("param0", vm.get("v7"));
    vm.put("v8", em.op("i2f").eval(vm.get("param0")));
    vm.put("v9", em.op("fmul").eval(vm.get("v8"),vm.get("v3")));
    vm.put("v10", em.op("fcmpl").eval(vm.get("v9"),vm.get("v9")));
    vm.put("v11", em.op("f2i").eval(vm.get("v9")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    if (!em.eq(vm.get("v11"), (vm.get("v4")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v4")));
    if (!em.ge(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) ge (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.ne(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) ne (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.ne(vm.get("v10"), vm.get("v1"))) e.abort(String.format("!((v10=%s) ne (v1=%s))", vm.get("v10"), vm.get("v1")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(IF)V",definedOutput={"this.threshold","this.table","this.modCount","this.loadFactor"},delta={"this.threshold","this.table","this.modCount","this.loadFactor"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_3127(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "threshold"));
    vm.put("v1", (float)2.14748365E9F);
    vm.put("v2", om.popField(vm.get("this"), "table"));
    vm.put("v3", (int)1);
    vm.put("v4", om.popField(vm.get("this"), "modCount"));
    vm.put("v5", (int)0);
    vm.put("v6", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v7", (float)0.0F);
    vm.put("v8", em.op("i2f").eval(vm.get("v3")));
    vm.put("v9", em.op("f2i").eval(vm.get("v1")));
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    vm.put("param1", vm.get("v6"));
    vm.put("v10", em.op("fmul").eval(vm.get("v8"),vm.get("param1")));
    vm.put("v11", em.op("fcmpl").eval(vm.get("v10"),vm.get("v10")));
    vm.put("v12", em.op("fcmpg").eval(vm.get("v10"),vm.get("v1")));
    vm.put("v13", em.op("fcmpl").eval(vm.get("v10"),vm.get("v7")));
    vm.put("v14", em.op("fcmpl").eval(vm.get("param1"),vm.get("param1")));
    vm.put("v15", em.op("fcmpg").eval(vm.get("param1"),vm.get("v7")));
    if (!em.eq(vm.get("v9"), (vm.get("v0")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v0")));
    vm.put("v16", vm.get("v2"));
    vm.put("v17", om.getArrayLength(vm.get("v16")));
    if (!em.eq(vm.get("v3"), (vm.get("v17")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v17")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.eq(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) eq (v5=%s))", vm.get("param0"), vm.get("v5")));
    if (!em.gt(vm.get("v12"), vm.get("v5"))) e.abort(String.format("!((v12=%s) gt (v5=%s))", vm.get("v12"), vm.get("v5")));
    if (!em.gt(vm.get("v15"), vm.get("v5"))) e.abort(String.format("!((v15=%s) gt (v5=%s))", vm.get("v15"), vm.get("v5")));
    if (!em.ne(vm.get("v13"), vm.get("v5"))) e.abort(String.format("!((v13=%s) ne (v5=%s))", vm.get("v13"), vm.get("v5")));
    if (!em.ge(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) ge (v5=%s))", vm.get("param0"), vm.get("v5")));
    if (!em.eq(vm.get("v14"), vm.get("v5"))) e.abort(String.format("!((v14=%s) eq (v5=%s))", vm.get("v14"), vm.get("v5")));
    if (!em.eq(vm.get("v11"), vm.get("v5"))) e.abort(String.format("!((v11=%s) eq (v5=%s))", vm.get("v11"), vm.get("v5")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(I)V",definedOutput={"this.threshold","this.table","this.loadFactor","this.modCount"},delta={"this.threshold","this.table","this.loadFactor","this.modCount"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_1855(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v1", (float)0.75F);
    vm.put("v2", om.popField(vm.get("this"), "threshold"));
    vm.put("v3", (float)2.14748365E9F);
    vm.put("v4", om.popField(vm.get("this"), "table"));
    vm.put("v5", (int)1);
    vm.put("v6", om.popField(vm.get("this"), "modCount"));
    vm.put("v7", (int)0);
    vm.put("v8", (float)0.0F);
    vm.put("v9", em.op("i2f").eval(vm.get("v5")));
    vm.put("v10", em.op("fmul").eval(vm.get("v9"),vm.get("v1")));
    vm.put("v11", em.op("fcmpl").eval(vm.get("v10"),vm.get("v8")));
    vm.put("v12", em.op("fcmpg").eval(vm.get("v10"),vm.get("v3")));
    vm.put("v13", em.op("f2i").eval(vm.get("v3")));
    vm.put("v14", em.op("fcmpl").eval(vm.get("v10"),vm.get("v10")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v13"), (vm.get("v2")))) e.abort("Inconsistent value for \"v13\": " + vm.get("v13") + " ne " + (vm.get("v2")));
    if (!em.eq(vm.get("v7"), (vm.get("v6")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v6")));
    vm.put("v15", vm.get("v4"));
    vm.put("v16", om.getArrayLength(vm.get("v15")));
    if (!em.eq(vm.get("v5"), (vm.get("v16")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v16")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.ne(vm.get("v11"), vm.get("v7"))) e.abort(String.format("!((v11=%s) ne (v7=%s))", vm.get("v11"), vm.get("v7")));
    if (!em.eq(vm.get("v14"), vm.get("v7"))) e.abort(String.format("!((v14=%s) eq (v7=%s))", vm.get("v14"), vm.get("v7")));
    if (!em.eq(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) eq (v7=%s))", vm.get("param0"), vm.get("v7")));
    if (!em.gt(vm.get("v12"), vm.get("v7"))) e.abort(String.format("!((v12=%s) gt (v7=%s))", vm.get("v12"), vm.get("v7")));
    if (!em.ge(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) ge (v7=%s))", vm.get("param0"), vm.get("v7")));
  }
  @IM(clazz="java.util.Hashtable", name="merge", desc="(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/function/BiFunction;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.table[*].value","this.modCount","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void merge68(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "modCount"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "count"));
    vm.put("v4", om.getField(vm.get("this"), "threshold"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "table"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", null);
    vm.put("v9", om.getArrayLength(vm.get("v7")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", vm.get("v3"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v2")));
    om.revertField(vm.get("this"), "count", vm.get("v12"));
    vm.put("v13", vm.get("v1"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    vm.put("v15", om.guessArrayIndex(vm.get("v7")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getArrayElement(vm.get("v7"), vm.get("v16")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.popField(vm.get("v18"), "key"));
    vm.put("param0", vm.get("v19"));
    vm.put("v20", om.popField(vm.get("v18"), "next"));
    vm.put("v21", vm.get("v20"));
    om.revertArrayElement(vm.get("v7"), vm.get("v16"), vm.get("v21"));
    vm.put("v22", em.op("java.lang.Object.hashCode()I").eval(vm.get("param0")));
    vm.put("v23", em.op("iand").eval(vm.get("v22"),vm.get("v0")));
    vm.put("v24", om.popField(vm.get("v18"), "hash"));
    vm.put("v25", om.popField(vm.get("v18"), "value"));
    vm.put("param1", vm.get("v25"));
    if (!em.eq(vm.get("v22"), (vm.get("v24")))) e.abort("Inconsistent value for \"v22\": " + vm.get("v22") + " ne " + (vm.get("v24")));
    vm.put("param2", om.newDefaultValue("java.util.function.BiFunction"));
    if (!em.eq(vm.get("v16"), (em.op("irem").eval(vm.get("v23"),vm.get("v10"))))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (em.op("irem").eval(vm.get("v23"),vm.get("v10"))));
    if (!em.ne(vm.get("param1"), vm.get("v8"))) e.abort(String.format("!((param1=%s) ne (v8=%s))", vm.get("param1"), vm.get("v8")));
    if (!em.lt(vm.get("v12"), vm.get("v5"))) e.abort(String.format("!((v12=%s) lt (v5=%s))", vm.get("v12"), vm.get("v5")));
    if (!em.eq(vm.get("v21"), vm.get("v8"))) e.abort(String.format("!((v21=%s) eq (v8=%s))", vm.get("v21"), vm.get("v8")));
    if (!em.ne(vm.get("param2"), vm.get("v8"))) e.abort(String.format("!((param2=%s) ne (v8=%s))", vm.get("param2"), vm.get("v8")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="()V",definedOutput={"this.threshold","this.table","this.modCount","this.loadFactor"},delta={"this.threshold","this.table","this.modCount","this.loadFactor"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_1097(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", (int)11);
    vm.put("this", om.getTarget());
    vm.put("v1", om.popField(vm.get("this"), "threshold"));
    vm.put("v2", (float)0.75F);
    vm.put("v3", om.popField(vm.get("this"), "modCount"));
    vm.put("v4", (int)0);
    vm.put("v5", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v6", om.popField(vm.get("this"), "table"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (float)2.14748365E9F);
    vm.put("v9", (float)0.0F);
    vm.put("v10", em.op("i2f").eval(vm.get("v0")));
    vm.put("v11", em.op("fmul").eval(vm.get("v10"),vm.get("v2")));
    vm.put("v12", em.op("fcmpg").eval(vm.get("v11"),vm.get("v8")));
    vm.put("v13", em.op("f2i").eval(vm.get("v11")));
    vm.put("v14", em.op("fcmpl").eval(vm.get("v11"),vm.get("v9")));
    vm.put("v15", om.getArrayLength(vm.get("v7")));
    vm.put("v16", em.op("fcmpl").eval(vm.get("v11"),vm.get("v11")));
    if (!em.eq(vm.get("v2"), (vm.get("v5")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v5")));
    if (!em.eq(vm.get("v0"), (vm.get("v15")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v15")));
    if (!em.eq(vm.get("v13"), (vm.get("v1")))) e.abort("Inconsistent value for \"v13\": " + vm.get("v13") + " ne " + (vm.get("v1")));
    if (!em.eq(vm.get("v4"), (vm.get("v3")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v3")));
    if (!em.le(vm.get("v12"), vm.get("v4"))) e.abort(String.format("!((v12=%s) le (v4=%s))", vm.get("v12"), vm.get("v4")));
    if (!em.eq(vm.get("v16"), vm.get("v4"))) e.abort(String.format("!((v16=%s) eq (v4=%s))", vm.get("v16"), vm.get("v4")));
    if (!em.ne(vm.get("v14"), vm.get("v4"))) e.abort(String.format("!((v14=%s) ne (v4=%s))", vm.get("v14"), vm.get("v4")));
  }
  @IM(clazz="java.util.Hashtable", name="keySet", desc="()Ljava/util/Set;",revertedInput={"this.keySet"},definedOutput={"this.keySet.c","this.keySet.mutex","this.keySet","this.keySet.c.this$0"},delta={"this.keySet.c","this.keySet.mutex","this.keySet.c.this$0"})
  static void keySet1698(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "keySet"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.popField(vm.get("v1"), "c"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", null);
    vm.put("v5", om.popField(vm.get("v1"), "mutex"));
    vm.put("v6", om.popField(vm.get("v3"), "this$0"));
    if (!em.eq(vm.get("this"), (vm.get("v5")))) e.abort("Inconsistent value for \"this\": " + vm.get("this") + " ne " + (vm.get("v5")));
    if (!em.eq(vm.get("this"), (vm.get("v6")))) e.abort("Inconsistent value for \"this\": " + vm.get("this") + " ne " + (vm.get("v6")));
    om.revertField(vm.get("this"), "keySet");
    vm.put("v7", om.getField(vm.get("this"), "keySet"));
    if (!em.eq(vm.get("v7"), vm.get("v4"))) e.abort(String.format("!((v7=%s) eq (v4=%s))", vm.get("v7"), vm.get("v4")));
  }
  @IM(clazz="java.util.Hashtable", name="merge", desc="(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/function/BiFunction;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.table[*].value","this.modCount","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void merge98(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "count"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "table"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "threshold"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", null);
    vm.put("v9", om.getArrayLength(vm.get("v4")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", vm.get("v1"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v2")));
    om.revertField(vm.get("this"), "count", vm.get("v12"));
    vm.put("v13", vm.get("v7"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    vm.put("v15", om.guessArrayIndex(vm.get("v4")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getArrayElement(vm.get("v4"), vm.get("v16")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.popField(vm.get("v18"), "value"));
    vm.put("param1", vm.get("v19"));
    vm.put("v20", om.popField(vm.get("v18"), "next"));
    vm.put("v21", vm.get("v20"));
    om.revertArrayElement(vm.get("v4"), vm.get("v16"), vm.get("v21"));
    vm.put("v22", om.getField(vm.get("v21"), "next"));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.getField(vm.get("v21"), "hash"));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", om.popField(vm.get("v18"), "key"));
    vm.put("param0", vm.get("v26"));
    vm.put("v27", em.op("java.lang.Object.hashCode()I").eval(vm.get("param0")));
    vm.put("v28", em.op("iand").eval(vm.get("v27"),vm.get("v0")));
    vm.put("v29", om.popField(vm.get("v18"), "hash"));
    if (!em.eq(vm.get("v27"), (vm.get("v29")))) e.abort("Inconsistent value for \"v27\": " + vm.get("v27") + " ne " + (vm.get("v29")));
    vm.put("param2", om.newDefaultValue("java.util.function.BiFunction"));
    if (!em.eq(vm.get("v16"), (em.op("irem").eval(vm.get("v28"),vm.get("v10"))))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (em.op("irem").eval(vm.get("v28"),vm.get("v10"))));
    if (!em.lt(vm.get("v12"), vm.get("v6"))) e.abort(String.format("!((v12=%s) lt (v6=%s))", vm.get("v12"), vm.get("v6")));
    if (!em.eq(vm.get("v23"), vm.get("v8"))) e.abort(String.format("!((v23=%s) eq (v8=%s))", vm.get("v23"), vm.get("v8")));
    if (!em.ne(vm.get("param2"), vm.get("v8"))) e.abort(String.format("!((param2=%s) ne (v8=%s))", vm.get("param2"), vm.get("v8")));
    if (!em.ne(vm.get("v25"), vm.get("v27"))) e.abort(String.format("!((v25=%s) ne (v27=%s))", vm.get("v25"), vm.get("v27")));
    if (!em.ne(vm.get("v21"), vm.get("v8"))) e.abort(String.format("!((v21=%s) ne (v8=%s))", vm.get("v21"), vm.get("v8")));
    if (!em.ne(vm.get("param1"), vm.get("v8"))) e.abort(String.format("!((param1=%s) ne (v8=%s))", vm.get("param1"), vm.get("v8")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(IF)V",definedOutput={"this.table","this.threshold","this.modCount","this.loadFactor"},delta={"this.table","this.threshold","this.modCount","this.loadFactor"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_11321(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "modCount"));
    vm.put("v1", (int)0);
    vm.put("v2", om.popField(vm.get("this"), "table"));
    vm.put("v3", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v4", om.popField(vm.get("this"), "threshold"));
    vm.put("v5", (int)1);
    vm.put("v6", (float)0.0F);
    vm.put("v7", em.op("i2f").eval(vm.get("v5")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    vm.put("param1", vm.get("v3"));
    vm.put("v8", em.op("fcmpl").eval(vm.get("param1"),vm.get("param1")));
    vm.put("v9", em.op("fcmpg").eval(vm.get("param1"),vm.get("v6")));
    vm.put("v10", em.op("fmul").eval(vm.get("v7"),vm.get("param1")));
    vm.put("v11", em.op("f2i").eval(vm.get("v10")));
    vm.put("v12", em.op("fcmpl").eval(vm.get("v10"),vm.get("v10")));
    if (!em.eq(vm.get("v11"), (vm.get("v4")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v4")));
    vm.put("v13", vm.get("v2"));
    vm.put("v14", om.getArrayLength(vm.get("v13")));
    if (!em.eq(vm.get("v5"), (vm.get("v14")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v14")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.ne(vm.get("v12"), vm.get("v1"))) e.abort(String.format("!((v12=%s) ne (v1=%s))", vm.get("v12"), vm.get("v1")));
    if (!em.ge(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) ge (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.eq(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) eq (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.gt(vm.get("v9"), vm.get("v1"))) e.abort(String.format("!((v9=%s) gt (v1=%s))", vm.get("v9"), vm.get("v1")));
    if (!em.eq(vm.get("v8"), vm.get("v1"))) e.abort(String.format("!((v8=%s) eq (v1=%s))", vm.get("v8"), vm.get("v1")));
  }
  @IM(clazz="java.util.Hashtable", name="merge", desc="(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/function/BiFunction;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.modCount","this.table[*].value","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void merge60(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "table"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "threshold"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayLength(vm.get("v2")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", (int)1);
    vm.put("v9", om.getField(vm.get("this"), "count"));
    vm.put("v10", (int)2147483639);
    vm.put("v11", (int)0);
    vm.put("v12", null);
    vm.put("v13", em.op("ishl").eval(vm.get("v6"),vm.get("v8")));
    vm.put("v14", em.op("iadd").eval(vm.get("v13"),vm.get("v8")));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v10")));
    vm.put("v16", vm.get("v7"));
    vm.put("v17", vm.get("v9"));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v8")));
    om.revertField(vm.get("this"), "count", vm.get("v18"));
    vm.put("v19", em.op("isub").eval(vm.get("v16"),vm.get("v8")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("v20", om.guessArrayIndex(vm.get("v2")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.getArrayElement(vm.get("v2"), vm.get("v21")));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.guessArrayIndex(vm.get("v2")));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", om.getArrayElement(vm.get("v2"), vm.get("v25")));
    vm.put("v27", vm.get("v26"));
    vm.put("v28", om.popField(vm.get("v27"), "value"));
    vm.put("param1", vm.get("v28"));
    vm.put("v29", om.popField(vm.get("v27"), "next"));
    vm.put("v30", vm.get("v29"));
    om.revertArrayElement(vm.get("v2"), vm.get("v25"), vm.get("v30"));
    vm.put("v31", om.popField(vm.get("v27"), "key"));
    vm.put("param0", vm.get("v31"));
    vm.put("v32", em.op("java.lang.Object.hashCode()I").eval(vm.get("param0")));
    vm.put("v33", em.op("iand").eval(vm.get("v32"),vm.get("v0")));
    vm.put("v34", om.popField(vm.get("v27"), "hash"));
    vm.put("v35", vm.get("v34"));
    vm.put("v36", em.op("iand").eval(vm.get("v35"),vm.get("v0")));
    vm.put("param2", om.newDefaultValue("java.util.function.BiFunction"));
    if (!em.eq(vm.get("v21"), (em.op("irem").eval(vm.get("v33"),vm.get("v6"))))) e.abort("Inconsistent value for \"v21\": " + vm.get("v21") + " ne " + (em.op("irem").eval(vm.get("v33"),vm.get("v6"))));
    if (!em.eq(vm.get("v35"), (em.op("java.lang.Object.hashCode()I").eval(vm.get("param0"))))) e.abort("Inconsistent value for \"v35\": " + vm.get("v35") + " ne " + (em.op("java.lang.Object.hashCode()I").eval(vm.get("param0"))));
    if (!em.eq(vm.get("v25"), (em.op("irem").eval(vm.get("v36"),vm.get("v6"))))) e.abort("Inconsistent value for \"v25\": " + vm.get("v25") + " ne " + (em.op("irem").eval(vm.get("v36"),vm.get("v6"))));
    if (!em.eq(vm.get("v23"), vm.get("v12"))) e.abort(String.format("!((v23=%s) eq (v12=%s))", vm.get("v23"), vm.get("v12")));
    if (!em.eq(vm.get("v6"), vm.get("v10"))) e.abort(String.format("!((v6=%s) eq (v10=%s))", vm.get("v6"), vm.get("v10")));
    if (!em.ne(vm.get("param1"), vm.get("v12"))) e.abort(String.format("!((param1=%s) ne (v12=%s))", vm.get("param1"), vm.get("v12")));
    if (!em.gt(vm.get("v15"), vm.get("v11"))) e.abort(String.format("!((v15=%s) gt (v11=%s))", vm.get("v15"), vm.get("v11")));
    if (!em.ge(vm.get("v18"), vm.get("v4"))) e.abort(String.format("!((v18=%s) ge (v4=%s))", vm.get("v18"), vm.get("v4")));
    if (!em.ne(vm.get("param2"), vm.get("v12"))) e.abort(String.format("!((param2=%s) ne (v12=%s))", vm.get("param2"), vm.get("v12")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="()V",definedOutput={"this.threshold","this.table","this.modCount","this.loadFactor"},delta={"this.threshold","this.table","this.modCount","this.loadFactor"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_1073(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "threshold"));
    vm.put("v1", (float)2.14748365E9F);
    vm.put("v2", om.popField(vm.get("this"), "modCount"));
    vm.put("v3", (int)0);
    vm.put("v4", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v5", (float)0.75F);
    vm.put("v6", (int)11);
    vm.put("v7", om.popField(vm.get("this"), "table"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", (float)0.0F);
    vm.put("v10", om.getArrayLength(vm.get("v8")));
    vm.put("v11", em.op("f2i").eval(vm.get("v1")));
    vm.put("v12", em.op("i2f").eval(vm.get("v6")));
    vm.put("v13", em.op("fmul").eval(vm.get("v12"),vm.get("v5")));
    vm.put("v14", em.op("fcmpl").eval(vm.get("v13"),vm.get("v9")));
    vm.put("v15", em.op("fcmpl").eval(vm.get("v13"),vm.get("v13")));
    vm.put("v16", em.op("fcmpg").eval(vm.get("v13"),vm.get("v1")));
    if (!em.eq(vm.get("v6"), (vm.get("v10")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v10")));
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    if (!em.eq(vm.get("v11"), (vm.get("v0")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v0")));
    if (!em.gt(vm.get("v16"), vm.get("v3"))) e.abort(String.format("!((v16=%s) gt (v3=%s))", vm.get("v16"), vm.get("v3")));
    if (!em.eq(vm.get("v15"), vm.get("v3"))) e.abort(String.format("!((v15=%s) eq (v3=%s))", vm.get("v15"), vm.get("v3")));
    if (!em.ne(vm.get("v14"), vm.get("v3"))) e.abort(String.format("!((v14=%s) ne (v3=%s))", vm.get("v14"), vm.get("v3")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="()V",definedOutput={"this.table","this.threshold","this.modCount","this.loadFactor"},delta={"this.table","this.threshold","this.modCount","this.loadFactor"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_1095(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "table"));
    vm.put("v1", om.popField(vm.get("this"), "modCount"));
    vm.put("v2", (int)0);
    vm.put("v3", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v4", (float)0.75F);
    vm.put("v5", om.popField(vm.get("this"), "threshold"));
    vm.put("v6", (int)11);
    vm.put("v7", (float)2.14748365E9F);
    vm.put("v8", (float)0.0F);
    vm.put("v9", em.op("i2f").eval(vm.get("v6")));
    vm.put("v10", em.op("fmul").eval(vm.get("v9"),vm.get("v4")));
    vm.put("v11", em.op("f2i").eval(vm.get("v10")));
    vm.put("v12", em.op("fcmpl").eval(vm.get("v10"),vm.get("v10")));
    vm.put("v13", em.op("fcmpg").eval(vm.get("v10"),vm.get("v7")));
    vm.put("v14", em.op("fcmpl").eval(vm.get("v10"),vm.get("v8")));
    vm.put("v15", vm.get("v0"));
    vm.put("v16", om.getArrayLength(vm.get("v15")));
    if (!em.eq(vm.get("v6"), (vm.get("v16")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v16")));
    if (!em.eq(vm.get("v4"), (vm.get("v3")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v3")));
    if (!em.eq(vm.get("v2"), (vm.get("v1")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v1")));
    if (!em.eq(vm.get("v11"), (vm.get("v5")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v5")));
    if (!em.eq(vm.get("v12"), vm.get("v2"))) e.abort(String.format("!((v12=%s) eq (v2=%s))", vm.get("v12"), vm.get("v2")));
    if (!em.le(vm.get("v13"), vm.get("v2"))) e.abort(String.format("!((v13=%s) le (v2=%s))", vm.get("v13"), vm.get("v2")));
    if (!em.eq(vm.get("v14"), vm.get("v2"))) e.abort(String.format("!((v14=%s) eq (v2=%s))", vm.get("v14"), vm.get("v2")));
  }
  @IM(clazz="java.util.Hashtable", name="hashCode", desc="()I",revertedInput={"this.loadFactor"},definedOutput={"this.loadFactor"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"})
  static void hashCode2470(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "count"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "loadFactor"));
    vm.put("v4", om.getField(vm.get("this"), "table"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", (int)1);
    vm.put("v7", null);
    vm.put("v8", (float)0.0F);
    vm.put("v9", om.getArrayElement(vm.get("v5"), vm.get("v0")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getArrayLength(vm.get("v5")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", vm.get("v3"));
    vm.put("v14", em.op("fneg").eval(vm.get("v13")));
    vm.put("v15", em.op("fneg").eval(vm.get("v14")));
    om.revertField(vm.get("this"), "loadFactor", vm.get("v15"));
    vm.put("v16", em.op("fcmpg").eval(vm.get("v15"),vm.get("v8")));
    if (!em.eq(vm.get("v10"), vm.get("v7"))) e.abort(String.format("!((v10=%s) eq (v7=%s))", vm.get("v10"), vm.get("v7")));
    if (!em.ge(vm.get("v16"), vm.get("v0"))) e.abort(String.format("!((v16=%s) ge (v0=%s))", vm.get("v16"), vm.get("v0")));
    if (!em.ne(vm.get("v2"), vm.get("v0"))) e.abort(String.format("!((v2=%s) ne (v0=%s))", vm.get("v2"), vm.get("v0")));
    if (!em.lt(vm.get("v0"), vm.get("v12"))) e.abort(String.format("!((v0=%s) lt (v12=%s))", vm.get("v0"), vm.get("v12")));
    if (!em.ge(vm.get("v6"), vm.get("v12"))) e.abort(String.format("!((v6=%s) ge (v12=%s))", vm.get("v6"), vm.get("v12")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(I)V",definedOutput={"this.table","this.threshold","this.loadFactor","this.modCount"},delta={"this.table","this.threshold","this.loadFactor","this.modCount"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_1898(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "table"));
    vm.put("v1", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v2", (float)0.75F);
    vm.put("v3", om.popField(vm.get("this"), "modCount"));
    vm.put("v4", (int)0);
    vm.put("v5", om.popField(vm.get("this"), "threshold"));
    vm.put("v6", (float)2.14748365E9F);
    vm.put("v7", (float)0.0F);
    if (!em.eq(vm.get("v4"), (vm.get("v3")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v3")));
    vm.put("v8", vm.get("v0"));
    vm.put("v9", om.getArrayLength(vm.get("v8")));
    vm.put("param0", vm.get("v9"));
    vm.put("v10", em.op("i2f").eval(vm.get("param0")));
    vm.put("v11", em.op("fmul").eval(vm.get("v10"),vm.get("v2")));
    vm.put("v12", em.op("fcmpg").eval(vm.get("v11"),vm.get("v6")));
    vm.put("v13", em.op("fcmpl").eval(vm.get("v11"),vm.get("v7")));
    vm.put("v14", em.op("f2i").eval(vm.get("v11")));
    vm.put("v15", em.op("fcmpl").eval(vm.get("v11"),vm.get("v11")));
    if (!em.eq(vm.get("v14"), (vm.get("v5")))) e.abort("Inconsistent value for \"v14\": " + vm.get("v14") + " ne " + (vm.get("v5")));
    if (!em.eq(vm.get("v2"), (vm.get("v1")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v1")));
    if (!em.ne(vm.get("v13"), vm.get("v4"))) e.abort(String.format("!((v13=%s) ne (v4=%s))", vm.get("v13"), vm.get("v4")));
    if (!em.ne(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) ne (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.le(vm.get("v12"), vm.get("v4"))) e.abort(String.format("!((v12=%s) le (v4=%s))", vm.get("v12"), vm.get("v4")));
    if (!em.ge(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) ge (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.eq(vm.get("v15"), vm.get("v4"))) e.abort(String.format("!((v15=%s) eq (v4=%s))", vm.get("v15"), vm.get("v4")));
  }
  @IM(clazz="java.util.Hashtable", name="putIfAbsent", desc="(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.modCount","this.table[*].value","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void putIfAbsent1281(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "modCount"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "table"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "count"));
    vm.put("v6", om.getField(vm.get("this"), "threshold"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayLength(vm.get("v4")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)2147483639);
    vm.put("v11", (int)0);
    vm.put("v12", null);
    vm.put("v13", em.op("ishl").eval(vm.get("v9"),vm.get("v2")));
    vm.put("v14", em.op("iadd").eval(vm.get("v13"),vm.get("v2")));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v10")));
    vm.put("v16", vm.get("v1"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v17"));
    vm.put("v18", vm.get("v5"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v2")));
    om.revertField(vm.get("this"), "count", vm.get("v19"));
    vm.put("v20", om.guessArrayIndex(vm.get("v4")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.getArrayElement(vm.get("v4"), vm.get("v21")));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.getField(vm.get("v23"), "next"));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", om.getField(vm.get("v23"), "hash"));
    vm.put("v27", vm.get("v26"));
    vm.put("v28", om.guessArrayIndex(vm.get("v4")));
    vm.put("v29", vm.get("v28"));
    vm.put("v30", om.getArrayElement(vm.get("v4"), vm.get("v29")));
    vm.put("v31", vm.get("v30"));
    vm.put("v32", om.popField(vm.get("v31"), "key"));
    vm.put("param0", vm.get("v32"));
    vm.put("v33", em.op("java.lang.Object.hashCode()I").eval(vm.get("param0")));
    vm.put("v34", em.op("java.lang.Object.hashCode()I").eval(vm.get("param0")));
    vm.put("v35", om.popField(vm.get("v31"), "next"));
    vm.put("v36", vm.get("v35"));
    om.revertArrayElement(vm.get("v4"), vm.get("v29"), vm.get("v36"));
    vm.put("v37", em.op("iand").eval(vm.get("v34"),vm.get("v0")));
    vm.put("v38", em.op("iand").eval(vm.get("v33"),vm.get("v0")));
    vm.put("v39", om.popField(vm.get("v31"), "hash"));
    vm.put("v40", om.popField(vm.get("v31"), "value"));
    vm.put("param1", vm.get("v40"));
    if (!em.eq(vm.get("v34"), (vm.get("v39")))) e.abort("Inconsistent value for \"v34\": " + vm.get("v34") + " ne " + (vm.get("v39")));
    if (!em.eq(vm.get("v21"), (em.op("irem").eval(vm.get("v38"),vm.get("v9"))))) e.abort("Inconsistent value for \"v21\": " + vm.get("v21") + " ne " + (em.op("irem").eval(vm.get("v38"),vm.get("v9"))));
    if (!em.eq(vm.get("v29"), (em.op("irem").eval(vm.get("v37"),vm.get("v9"))))) e.abort("Inconsistent value for \"v29\": " + vm.get("v29") + " ne " + (em.op("irem").eval(vm.get("v37"),vm.get("v9"))));
    if (!em.eq(vm.get("v25"), vm.get("v12"))) e.abort(String.format("!((v25=%s) eq (v12=%s))", vm.get("v25"), vm.get("v12")));
    if (!em.gt(vm.get("v15"), vm.get("v11"))) e.abort(String.format("!((v15=%s) gt (v11=%s))", vm.get("v15"), vm.get("v11")));
    if (!em.eq(vm.get("v9"), vm.get("v10"))) e.abort(String.format("!((v9=%s) eq (v10=%s))", vm.get("v9"), vm.get("v10")));
    if (!em.ne(vm.get("v23"), vm.get("v12"))) e.abort(String.format("!((v23=%s) ne (v12=%s))", vm.get("v23"), vm.get("v12")));
    if (!em.ne(vm.get("param1"), vm.get("v12"))) e.abort(String.format("!((param1=%s) ne (v12=%s))", vm.get("param1"), vm.get("v12")));
    if (!em.ge(vm.get("v19"), vm.get("v7"))) e.abort(String.format("!((v19=%s) ge (v7=%s))", vm.get("v19"), vm.get("v7")));
    if (!em.ne(vm.get("v27"), vm.get("v33"))) e.abort(String.format("!((v27=%s) ne (v33=%s))", vm.get("v27"), vm.get("v33")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(IF)V",definedOutput={"this.threshold","this.table","this.loadFactor","this.modCount"},delta={"this.threshold","this.table","this.loadFactor","this.modCount"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_11328(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "threshold"));
    vm.put("v1", om.popField(vm.get("this"), "loadFactor"));
    vm.put("param1", vm.get("v1"));
    vm.put("v2", om.popField(vm.get("this"), "modCount"));
    vm.put("v3", (int)0);
    vm.put("v4", om.popField(vm.get("this"), "table"));
    vm.put("v5", (float)0.0F);
    vm.put("v6", em.op("fcmpg").eval(vm.get("param1"),vm.get("v5")));
    vm.put("v7", em.op("fcmpl").eval(vm.get("param1"),vm.get("param1")));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    vm.put("v8", vm.get("v4"));
    vm.put("v9", om.getArrayLength(vm.get("v8")));
    vm.put("param0", vm.get("v9"));
    vm.put("v10", em.op("i2f").eval(vm.get("param0")));
    vm.put("v11", em.op("fmul").eval(vm.get("v10"),vm.get("param1")));
    vm.put("v12", em.op("f2i").eval(vm.get("v11")));
    vm.put("v13", em.op("fcmpl").eval(vm.get("v11"),vm.get("v11")));
    if (!em.eq(vm.get("v12"), (vm.get("v0")))) e.abort("Inconsistent value for \"v12\": " + vm.get("v12") + " ne " + (vm.get("v0")));
    if (!em.ne(vm.get("v13"), vm.get("v3"))) e.abort(String.format("!((v13=%s) ne (v3=%s))", vm.get("v13"), vm.get("v3")));
    if (!em.eq(vm.get("v7"), vm.get("v3"))) e.abort(String.format("!((v7=%s) eq (v3=%s))", vm.get("v7"), vm.get("v3")));
    if (!em.ge(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) ge (v3=%s))", vm.get("param0"), vm.get("v3")));
    if (!em.gt(vm.get("v6"), vm.get("v3"))) e.abort(String.format("!((v6=%s) gt (v3=%s))", vm.get("v6"), vm.get("v3")));
    if (!em.ne(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) ne (v3=%s))", vm.get("param0"), vm.get("v3")));
  }
  @IM(clazz="java.util.Hashtable", name="put", desc="(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.modCount","this.table[*].value","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void put2257(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "threshold"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", (int)1);
    vm.put("v5", om.getField(vm.get("this"), "table"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getArrayLength(vm.get("v6")));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("this"), "count"));
    vm.put("v10", (int)2147483639);
    vm.put("v11", (int)0);
    vm.put("v12", null);
    vm.put("v13", em.op("ishl").eval(vm.get("v8"),vm.get("v4")));
    vm.put("v14", em.op("iadd").eval(vm.get("v13"),vm.get("v4")));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v10")));
    vm.put("v16", vm.get("v9"));
    vm.put("v17", vm.get("v3"));
    vm.put("v18", em.op("isub").eval(vm.get("v16"),vm.get("v4")));
    om.revertField(vm.get("this"), "count", vm.get("v18"));
    vm.put("v19", em.op("isub").eval(vm.get("v17"),vm.get("v4")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("v20", om.guessArrayIndex(vm.get("v6")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.getArrayElement(vm.get("v6"), vm.get("v21")));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.popField(vm.get("v23"), "next"));
    vm.put("v25", vm.get("v24"));
    om.revertArrayElement(vm.get("v6"), vm.get("v21"), vm.get("v25"));
    vm.put("v26", om.popField(vm.get("v23"), "key"));
    vm.put("param0", vm.get("v26"));
    vm.put("v27", em.op("java.lang.Object.hashCode()I").eval(vm.get("param0")));
    vm.put("v28", em.op("java.lang.Object.hashCode()I").eval(vm.get("param0")));
    vm.put("v29", om.popField(vm.get("v23"), "value"));
    vm.put("param1", vm.get("v29"));
    vm.put("v30", om.popField(vm.get("v23"), "hash"));
    vm.put("v31", em.op("iand").eval(vm.get("v28"),vm.get("v0")));
    vm.put("v32", em.op("iand").eval(vm.get("v27"),vm.get("v0")));
    vm.put("v33", em.op("irem").eval(vm.get("v32"),vm.get("v8")));
    vm.put("v34", om.getArrayElement(vm.get("v6"), vm.get("v33")));
    vm.put("v35", vm.get("v34"));
    vm.put("v36", om.getField(vm.get("v35"), "next"));
    vm.put("v37", vm.get("v36"));
    vm.put("v38", om.getField(vm.get("v37"), "next"));
    vm.put("v39", vm.get("v38"));
    vm.put("v40", om.getField(vm.get("v39"), "next"));
    vm.put("v41", vm.get("v40"));
    vm.put("v42", om.getField(vm.get("v37"), "hash"));
    vm.put("v43", vm.get("v42"));
    vm.put("v44", om.getField(vm.get("v39"), "hash"));
    vm.put("v45", vm.get("v44"));
    vm.put("v46", om.getField(vm.get("v35"), "hash"));
    vm.put("v47", vm.get("v46"));
    if (!em.eq(vm.get("v28"), (vm.get("v30")))) e.abort("Inconsistent value for \"v28\": " + vm.get("v28") + " ne " + (vm.get("v30")));
    if (!em.eq(vm.get("v21"), (em.op("irem").eval(vm.get("v31"),vm.get("v8"))))) e.abort("Inconsistent value for \"v21\": " + vm.get("v21") + " ne " + (em.op("irem").eval(vm.get("v31"),vm.get("v8"))));
    if (!em.gt(vm.get("v15"), vm.get("v11"))) e.abort(String.format("!((v15=%s) gt (v11=%s))", vm.get("v15"), vm.get("v11")));
    if (!em.ne(vm.get("v35"), vm.get("v12"))) e.abort(String.format("!((v35=%s) ne (v12=%s))", vm.get("v35"), vm.get("v12")));
    if (!em.ne(vm.get("param1"), vm.get("v12"))) e.abort(String.format("!((param1=%s) ne (v12=%s))", vm.get("param1"), vm.get("v12")));
    if (!em.ge(vm.get("v18"), vm.get("v2"))) e.abort(String.format("!((v18=%s) ge (v2=%s))", vm.get("v18"), vm.get("v2")));
    if (!em.ne(vm.get("v47"), vm.get("v27"))) e.abort(String.format("!((v47=%s) ne (v27=%s))", vm.get("v47"), vm.get("v27")));
    if (!em.ne(vm.get("v39"), vm.get("v12"))) e.abort(String.format("!((v39=%s) ne (v12=%s))", vm.get("v39"), vm.get("v12")));
    if (!em.ne(vm.get("v43"), vm.get("v27"))) e.abort(String.format("!((v43=%s) ne (v27=%s))", vm.get("v43"), vm.get("v27")));
    if (!em.ne(vm.get("v37"), vm.get("v12"))) e.abort(String.format("!((v37=%s) ne (v12=%s))", vm.get("v37"), vm.get("v12")));
    if (!em.eq(vm.get("v41"), vm.get("v12"))) e.abort(String.format("!((v41=%s) eq (v12=%s))", vm.get("v41"), vm.get("v12")));
    if (!em.ne(vm.get("v45"), vm.get("v27"))) e.abort(String.format("!((v45=%s) ne (v27=%s))", vm.get("v45"), vm.get("v27")));
    if (!em.eq(vm.get("v8"), vm.get("v10"))) e.abort(String.format("!((v8=%s) eq (v10=%s))", vm.get("v8"), vm.get("v10")));
  }
  @IM(clazz="java.util.Hashtable", name="clear", desc="()V",revertedInput={"this.modCount"},definedOutput={"this.table[*]","this.count","this.modCount"},delta={"this.table[*]","this.count"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void clear3699(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "count"));
    vm.put("v1", (int)0);
    vm.put("v2", (int)-1);
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getField(vm.get("this"), "table"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayLength(vm.get("v7")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", em.op("iinc").eval(vm.get("v9"),vm.get("v2")));
    vm.put("v11", em.op("iinc").eval(vm.get("v10"),vm.get("v2")));
    vm.put("v12", om.popArrayElement(vm.get("v7"), vm.get("v10")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    vm.put("v13", vm.get("v4"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v5")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    if (!em.ge(vm.get("v10"), vm.get("v1"))) e.abort(String.format("!((v10=%s) ge (v1=%s))", vm.get("v10"), vm.get("v1")));
    if (!em.lt(vm.get("v11"), vm.get("v1"))) e.abort(String.format("!((v11=%s) lt (v1=%s))", vm.get("v11"), vm.get("v1")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(IF)V",definedOutput={"this.threshold","this.table","this.modCount","this.loadFactor"},delta={"this.threshold","this.table","this.modCount","this.loadFactor"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_11311(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "modCount"));
    vm.put("v1", (int)0);
    vm.put("v2", om.popField(vm.get("this"), "threshold"));
    vm.put("v3", (float)2.14748365E9F);
    vm.put("v4", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v5", om.popField(vm.get("this"), "table"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (float)0.0F);
    vm.put("v8", om.getArrayLength(vm.get("v6")));
    vm.put("param0", vm.get("v8"));
    vm.put("v9", em.op("i2f").eval(vm.get("param0")));
    vm.put("v10", em.op("f2i").eval(vm.get("v3")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v10"), (vm.get("v2")))) e.abort("Inconsistent value for \"v10\": " + vm.get("v10") + " ne " + (vm.get("v2")));
    vm.put("param1", vm.get("v4"));
    vm.put("v11", em.op("fmul").eval(vm.get("v9"),vm.get("param1")));
    vm.put("v12", em.op("fcmpl").eval(vm.get("v11"),vm.get("v7")));
    vm.put("v13", em.op("fcmpl").eval(vm.get("v11"),vm.get("v11")));
    vm.put("v14", em.op("fcmpg").eval(vm.get("v11"),vm.get("v3")));
    vm.put("v15", em.op("fcmpl").eval(vm.get("param1"),vm.get("param1")));
    vm.put("v16", em.op("fcmpg").eval(vm.get("param1"),vm.get("v7")));
    if (!em.ne(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) ne (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.gt(vm.get("v14"), vm.get("v1"))) e.abort(String.format("!((v14=%s) gt (v1=%s))", vm.get("v14"), vm.get("v1")));
    if (!em.eq(vm.get("v15"), vm.get("v1"))) e.abort(String.format("!((v15=%s) eq (v1=%s))", vm.get("v15"), vm.get("v1")));
    if (!em.gt(vm.get("v16"), vm.get("v1"))) e.abort(String.format("!((v16=%s) gt (v1=%s))", vm.get("v16"), vm.get("v1")));
    if (!em.ge(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) ge (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.eq(vm.get("v13"), vm.get("v1"))) e.abort(String.format("!((v13=%s) eq (v1=%s))", vm.get("v13"), vm.get("v1")));
    if (!em.eq(vm.get("v12"), vm.get("v1"))) e.abort(String.format("!((v12=%s) eq (v1=%s))", vm.get("v12"), vm.get("v1")));
  }
  @IM(clazz="java.util.Hashtable", name="merge", desc="(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/function/BiFunction;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.table[*].value","this.modCount","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void merge76(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "table"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", (int)1);
    vm.put("v5", om.getArrayLength(vm.get("v2")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "count"));
    vm.put("v8", om.getField(vm.get("this"), "threshold"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", null);
    vm.put("v11", vm.get("v3"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v4")));
    om.revertField(vm.get("this"), "modCount", vm.get("v12"));
    vm.put("v13", vm.get("v7"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v4")));
    om.revertField(vm.get("this"), "count", vm.get("v14"));
    vm.put("v15", om.guessArrayIndex(vm.get("v2")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getArrayElement(vm.get("v2"), vm.get("v16")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.popField(vm.get("v18"), "key"));
    vm.put("param0", vm.get("v19"));
    vm.put("v20", om.popField(vm.get("v18"), "hash"));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", em.op("iand").eval(vm.get("v21"),vm.get("v0")));
    vm.put("v23", om.popField(vm.get("v18"), "value"));
    vm.put("param1", vm.get("v23"));
    vm.put("v24", om.popField(vm.get("v18"), "next"));
    vm.put("v25", vm.get("v24"));
    om.revertArrayElement(vm.get("v2"), vm.get("v16"), vm.get("v25"));
    vm.put("v26", om.getField(vm.get("v25"), "hash"));
    vm.put("v27", vm.get("v26"));
    vm.put("v28", om.getField(vm.get("v25"), "next"));
    vm.put("v29", vm.get("v28"));
    vm.put("v30", om.getField(vm.get("v29"), "hash"));
    vm.put("v31", vm.get("v30"));
    vm.put("v32", om.getField(vm.get("v29"), "next"));
    vm.put("v33", vm.get("v32"));
    vm.put("param2", om.newDefaultValue("java.util.function.BiFunction"));
    if (!em.eq(vm.get("v16"), (em.op("irem").eval(vm.get("v22"),vm.get("v6"))))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (em.op("irem").eval(vm.get("v22"),vm.get("v6"))));
    if (!em.ne(vm.get("v27"), vm.get("v21"))) e.abort(String.format("!((v27=%s) ne (v21=%s))", vm.get("v27"), vm.get("v21")));
    if (!em.ne(vm.get("param2"), vm.get("v10"))) e.abort(String.format("!((param2=%s) ne (v10=%s))", vm.get("param2"), vm.get("v10")));
    if (!em.ne(vm.get("v31"), vm.get("v21"))) e.abort(String.format("!((v31=%s) ne (v21=%s))", vm.get("v31"), vm.get("v21")));
    if (!em.ne(vm.get("v25"), vm.get("v10"))) e.abort(String.format("!((v25=%s) ne (v10=%s))", vm.get("v25"), vm.get("v10")));
    if (!em.eq(vm.get("v33"), vm.get("v10"))) e.abort(String.format("!((v33=%s) eq (v10=%s))", vm.get("v33"), vm.get("v10")));
    if (!em.ne(vm.get("v29"), vm.get("v10"))) e.abort(String.format("!((v29=%s) ne (v10=%s))", vm.get("v29"), vm.get("v10")));
    if (!em.lt(vm.get("v14"), vm.get("v9"))) e.abort(String.format("!((v14=%s) lt (v9=%s))", vm.get("v14"), vm.get("v9")));
    if (!em.ne(vm.get("param1"), vm.get("v10"))) e.abort(String.format("!((param1=%s) ne (v10=%s))", vm.get("param1"), vm.get("v10")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(IF)V",definedOutput={"this.threshold","this.table","this.loadFactor","this.modCount"},delta={"this.threshold","this.table","this.loadFactor","this.modCount"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_11307(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v1", om.popField(vm.get("this"), "modCount"));
    vm.put("v2", (int)0);
    vm.put("v3", om.popField(vm.get("this"), "threshold"));
    vm.put("v4", (int)1);
    vm.put("v5", om.popField(vm.get("this"), "table"));
    vm.put("v6", (float)2.14748365E9F);
    vm.put("v7", (float)0.0F);
    vm.put("v8", em.op("i2f").eval(vm.get("v4")));
    vm.put("v9", vm.get("v5"));
    vm.put("v10", om.getArrayLength(vm.get("v9")));
    if (!em.eq(vm.get("v4"), (vm.get("v10")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v10")));
    if (!em.eq(vm.get("v2"), (vm.get("v1")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v1")));
    vm.put("param1", vm.get("v0"));
    vm.put("v11", em.op("fmul").eval(vm.get("v8"),vm.get("param1")));
    vm.put("v12", em.op("f2i").eval(vm.get("v11")));
    vm.put("v13", em.op("fcmpl").eval(vm.get("v11"),vm.get("v7")));
    vm.put("v14", em.op("fcmpg").eval(vm.get("param1"),vm.get("v7")));
    vm.put("v15", em.op("fcmpl").eval(vm.get("v11"),vm.get("v11")));
    vm.put("v16", em.op("fcmpg").eval(vm.get("v11"),vm.get("v6")));
    vm.put("v17", em.op("fcmpl").eval(vm.get("param1"),vm.get("param1")));
    if (!em.eq(vm.get("v12"), (vm.get("v3")))) e.abort("Inconsistent value for \"v12\": " + vm.get("v12") + " ne " + (vm.get("v3")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.le(vm.get("v16"), vm.get("v2"))) e.abort(String.format("!((v16=%s) le (v2=%s))", vm.get("v16"), vm.get("v2")));
    if (!em.gt(vm.get("v14"), vm.get("v2"))) e.abort(String.format("!((v14=%s) gt (v2=%s))", vm.get("v14"), vm.get("v2")));
    if (!em.ne(vm.get("v13"), vm.get("v2"))) e.abort(String.format("!((v13=%s) ne (v2=%s))", vm.get("v13"), vm.get("v2")));
    if (!em.eq(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) eq (v2=%s))", vm.get("param0"), vm.get("v2")));
    if (!em.ge(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) ge (v2=%s))", vm.get("param0"), vm.get("v2")));
    if (!em.eq(vm.get("v15"), vm.get("v2"))) e.abort(String.format("!((v15=%s) eq (v2=%s))", vm.get("v15"), vm.get("v2")));
    if (!em.eq(vm.get("v17"), vm.get("v2"))) e.abort(String.format("!((v17=%s) eq (v2=%s))", vm.get("v17"), vm.get("v2")));
  }
  @IM(clazz="java.util.Hashtable", name="clear", desc="()V",revertedInput={"this.modCount"},definedOutput={"this.count","this.modCount"},delta={"this.count"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"})
  static void clear3697(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "modCount"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "table"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.popField(vm.get("this"), "count"));
    vm.put("v5", (int)0);
    vm.put("v6", (int)-1);
    vm.put("v7", om.getArrayLength(vm.get("v3")));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", em.op("iinc").eval(vm.get("v8"),vm.get("v6")));
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    vm.put("v10", vm.get("v0"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v1")));
    om.revertField(vm.get("this"), "modCount", vm.get("v11"));
    if (!em.lt(vm.get("v9"), vm.get("v5"))) e.abort(String.format("!((v9=%s) lt (v5=%s))", vm.get("v9"), vm.get("v5")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="()V",definedOutput={"this.threshold","this.table","this.loadFactor","this.modCount"},delta={"this.threshold","this.table","this.loadFactor","this.modCount"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_1094(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "threshold"));
    vm.put("v1", (float)2.14748365E9F);
    vm.put("v2", om.popField(vm.get("this"), "table"));
    vm.put("v3", (int)11);
    vm.put("v4", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v5", (float)0.75F);
    vm.put("v6", om.popField(vm.get("this"), "modCount"));
    vm.put("v7", (int)0);
    vm.put("v8", (float)0.0F);
    vm.put("v9", em.op("i2f").eval(vm.get("v3")));
    vm.put("v10", em.op("fmul").eval(vm.get("v9"),vm.get("v5")));
    vm.put("v11", em.op("fcmpg").eval(vm.get("v10"),vm.get("v1")));
    vm.put("v12", em.op("fcmpl").eval(vm.get("v10"),vm.get("v8")));
    vm.put("v13", em.op("f2i").eval(vm.get("v1")));
    vm.put("v14", em.op("fcmpl").eval(vm.get("v10"),vm.get("v10")));
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    if (!em.eq(vm.get("v7"), (vm.get("v6")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v6")));
    if (!em.eq(vm.get("v13"), (vm.get("v0")))) e.abort("Inconsistent value for \"v13\": " + vm.get("v13") + " ne " + (vm.get("v0")));
    vm.put("v15", vm.get("v2"));
    vm.put("v16", om.getArrayLength(vm.get("v15")));
    if (!em.eq(vm.get("v3"), (vm.get("v16")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v16")));
    if (!em.eq(vm.get("v14"), vm.get("v7"))) e.abort(String.format("!((v14=%s) eq (v7=%s))", vm.get("v14"), vm.get("v7")));
    if (!em.eq(vm.get("v12"), vm.get("v7"))) e.abort(String.format("!((v12=%s) eq (v7=%s))", vm.get("v12"), vm.get("v7")));
    if (!em.gt(vm.get("v11"), vm.get("v7"))) e.abort(String.format("!((v11=%s) gt (v7=%s))", vm.get("v11"), vm.get("v7")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(I)V",definedOutput={"this.threshold","this.table","this.loadFactor","this.modCount"},delta={"this.threshold","this.table","this.loadFactor","this.modCount"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_1895(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "threshold"));
    vm.put("v1", (int)1);
    vm.put("v2", (float)0.75F);
    vm.put("v3", om.popField(vm.get("this"), "table"));
    vm.put("v4", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v5", om.popField(vm.get("this"), "modCount"));
    vm.put("v6", (int)0);
    vm.put("v7", em.op("i2f").eval(vm.get("v1")));
    vm.put("v8", em.op("fmul").eval(vm.get("v7"),vm.get("v2")));
    vm.put("v9", em.op("f2i").eval(vm.get("v8")));
    vm.put("v10", em.op("fcmpl").eval(vm.get("v8"),vm.get("v8")));
    if (!em.eq(vm.get("v9"), (vm.get("v0")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v2"), (vm.get("v4")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v4")));
    if (!em.eq(vm.get("v6"), (vm.get("v5")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v5")));
    vm.put("v11", vm.get("v3"));
    vm.put("v12", om.getArrayLength(vm.get("v11")));
    if (!em.eq(vm.get("v1"), (vm.get("v12")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v12")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.eq(vm.get("param0"), vm.get("v6"))) e.abort(String.format("!((param0=%s) eq (v6=%s))", vm.get("param0"), vm.get("v6")));
    if (!em.ge(vm.get("param0"), vm.get("v6"))) e.abort(String.format("!((param0=%s) ge (v6=%s))", vm.get("param0"), vm.get("v6")));
    if (!em.ne(vm.get("v10"), vm.get("v6"))) e.abort(String.format("!((v10=%s) ne (v6=%s))", vm.get("v10"), vm.get("v6")));
  }
  @IM(clazz="java.util.Hashtable", name="putIfAbsent", desc="(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.modCount","this.table[*].value","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void putIfAbsent1271(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "count"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "modCount"));
    vm.put("v4", om.getField(vm.get("this"), "table"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "threshold"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayLength(vm.get("v5")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)2147483639);
    vm.put("v11", (int)0);
    vm.put("v12", null);
    vm.put("v13", em.op("ishl").eval(vm.get("v9"),vm.get("v2")));
    vm.put("v14", em.op("iadd").eval(vm.get("v13"),vm.get("v2")));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v10")));
    vm.put("v16", vm.get("v1"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v2")));
    om.revertField(vm.get("this"), "count", vm.get("v17"));
    vm.put("v18", vm.get("v3"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("v20", om.guessArrayIndex(vm.get("v5")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.getArrayElement(vm.get("v5"), vm.get("v21")));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.popField(vm.get("v23"), "value"));
    vm.put("param1", vm.get("v24"));
    vm.put("v25", om.popField(vm.get("v23"), "hash"));
    vm.put("v26", vm.get("v25"));
    vm.put("v27", em.op("iand").eval(vm.get("v26"),vm.get("v0")));
    vm.put("v28", om.popField(vm.get("v23"), "key"));
    vm.put("param0", vm.get("v28"));
    vm.put("v29", em.op("java.lang.Object.hashCode()I").eval(vm.get("param0")));
    vm.put("v30", em.op("iand").eval(vm.get("v29"),vm.get("v0")));
    vm.put("v31", em.op("irem").eval(vm.get("v30"),vm.get("v9")));
    vm.put("v32", om.popField(vm.get("v23"), "next"));
    vm.put("v33", vm.get("v32"));
    om.revertArrayElement(vm.get("v5"), vm.get("v21"), vm.get("v33"));
    vm.put("v34", om.getArrayElement(vm.get("v5"), vm.get("v31")));
    vm.put("v35", vm.get("v34"));
    if (!em.eq(vm.get("v26"), (em.op("java.lang.Object.hashCode()I").eval(vm.get("param0"))))) e.abort("Inconsistent value for \"v26\": " + vm.get("v26") + " ne " + (em.op("java.lang.Object.hashCode()I").eval(vm.get("param0"))));
    if (!em.eq(vm.get("v21"), (em.op("irem").eval(vm.get("v27"),vm.get("v9"))))) e.abort("Inconsistent value for \"v21\": " + vm.get("v21") + " ne " + (em.op("irem").eval(vm.get("v27"),vm.get("v9"))));
    if (!em.gt(vm.get("v15"), vm.get("v11"))) e.abort(String.format("!((v15=%s) gt (v11=%s))", vm.get("v15"), vm.get("v11")));
    if (!em.eq(vm.get("v9"), vm.get("v10"))) e.abort(String.format("!((v9=%s) eq (v10=%s))", vm.get("v9"), vm.get("v10")));
    if (!em.eq(vm.get("v35"), vm.get("v12"))) e.abort(String.format("!((v35=%s) eq (v12=%s))", vm.get("v35"), vm.get("v12")));
    if (!em.ge(vm.get("v17"), vm.get("v7"))) e.abort(String.format("!((v17=%s) ge (v7=%s))", vm.get("v17"), vm.get("v7")));
    if (!em.ne(vm.get("param1"), vm.get("v12"))) e.abort(String.format("!((param1=%s) ne (v12=%s))", vm.get("param1"), vm.get("v12")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(I)V",definedOutput={"this.table","this.threshold","this.modCount","this.loadFactor"},delta={"this.table","this.threshold","this.modCount","this.loadFactor"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_1892(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "modCount"));
    vm.put("v1", (int)0);
    vm.put("v2", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v3", (float)0.75F);
    vm.put("v4", om.popField(vm.get("this"), "table"));
    vm.put("v5", om.popField(vm.get("this"), "threshold"));
    vm.put("v6", (float)2.14748365E9F);
    vm.put("v7", (int)1);
    vm.put("v8", (float)0.0F);
    vm.put("v9", em.op("f2i").eval(vm.get("v6")));
    vm.put("v10", em.op("i2f").eval(vm.get("v7")));
    vm.put("v11", em.op("fmul").eval(vm.get("v10"),vm.get("v3")));
    vm.put("v12", em.op("fcmpg").eval(vm.get("v11"),vm.get("v6")));
    vm.put("v13", em.op("fcmpl").eval(vm.get("v11"),vm.get("v11")));
    vm.put("v14", em.op("fcmpl").eval(vm.get("v11"),vm.get("v8")));
    if (!em.eq(vm.get("v9"), (vm.get("v5")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v5")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    vm.put("v15", vm.get("v4"));
    vm.put("v16", om.getArrayLength(vm.get("v15")));
    if (!em.eq(vm.get("v7"), (vm.get("v16")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v16")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.gt(vm.get("v12"), vm.get("v1"))) e.abort(String.format("!((v12=%s) gt (v1=%s))", vm.get("v12"), vm.get("v1")));
    if (!em.eq(vm.get("v14"), vm.get("v1"))) e.abort(String.format("!((v14=%s) eq (v1=%s))", vm.get("v14"), vm.get("v1")));
    if (!em.eq(vm.get("v13"), vm.get("v1"))) e.abort(String.format("!((v13=%s) eq (v1=%s))", vm.get("v13"), vm.get("v1")));
    if (!em.ge(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) ge (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.eq(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) eq (v1=%s))", vm.get("param0"), vm.get("v1")));
  }
  @IM(clazz="java.util.Hashtable", name="hashCode", desc="()I",revertedInput={"this.loadFactor"},definedOutput={"this.loadFactor"})
  @DG(pred={"merge43","clear3699","merge63","_init_11307","clear3683","_init_1095","put2249","put2290","_init_1073","addEntry2796","_init_3127","merge98","_init_1898","putIfAbsent1289","clear3606","_init_1855","_init_1892","_init_11308","_init_1879","clear3695","_init_1097","putIfAbsent1297","_init_1094","merge68","putIfAbsent1281","_init_1099","_init_11311","_init_11285","_init_11321","_init_11325","addEntry2799","_init_1889","clear3646","_init_1877","_init_11304","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","_init_1896","put2286","_init_1897","put2257","_init_11328","merge60","_init_1895","_init_1899","_init_11287","merge76"})
  static void hashCode2485(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "table"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "loadFactor"));
    vm.put("v4", om.getArrayLength(vm.get("v2")));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getArrayElement(vm.get("v2"), vm.get("v0")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "count"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)1);
    vm.put("v11", null);
    vm.put("v12", (float)0.0F);
    vm.put("v13", om.getField(vm.get("v7"), "next"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", vm.get("v3"));
    vm.put("v16", em.op("fneg").eval(vm.get("v15")));
    vm.put("v17", em.op("fneg").eval(vm.get("v16")));
    om.revertField(vm.get("this"), "loadFactor", vm.get("v17"));
    vm.put("v18", em.op("fcmpg").eval(vm.get("v17"),vm.get("v12")));
    if (!em.eq(vm.get("v14"), vm.get("v11"))) e.abort(String.format("!((v14=%s) eq (v11=%s))", vm.get("v14"), vm.get("v11")));
    if (!em.lt(vm.get("v0"), vm.get("v5"))) e.abort(String.format("!((v0=%s) lt (v5=%s))", vm.get("v0"), vm.get("v5")));
    if (!em.ne(vm.get("v9"), vm.get("v0"))) e.abort(String.format("!((v9=%s) ne (v0=%s))", vm.get("v9"), vm.get("v0")));
    if (!em.ge(vm.get("v18"), vm.get("v0"))) e.abort(String.format("!((v18=%s) ge (v0=%s))", vm.get("v18"), vm.get("v0")));
    if (!em.ne(vm.get("v7"), vm.get("v11"))) e.abort(String.format("!((v7=%s) ne (v11=%s))", vm.get("v7"), vm.get("v11")));
    if (!em.ge(vm.get("v10"), vm.get("v5"))) e.abort(String.format("!((v10=%s) ge (v5=%s))", vm.get("v10"), vm.get("v5")));
  }
  @IM(clazz="java.util.Hashtable", name="clear", desc="()V",revertedInput={"this.modCount"},definedOutput={"this.table[*]","this.count","this.modCount"},delta={"this.table[*]","this.count"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void clear3683(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)-1);
    vm.put("v1", null);
    vm.put("v2", om.getField(vm.get("this"), "table"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getArrayLength(vm.get("v3")));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "modCount"));
    vm.put("v7", (int)1);
    vm.put("v8", om.popField(vm.get("this"), "count"));
    vm.put("v9", (int)0);
    vm.put("v10", em.op("iinc").eval(vm.get("v5"),vm.get("v0")));
    vm.put("v11", om.popArrayElement(vm.get("v3"), vm.get("v10")));
    vm.put("v12", em.op("iinc").eval(vm.get("v10"),vm.get("v0")));
    vm.put("v13", om.popArrayElement(vm.get("v3"), vm.get("v12")));
    vm.put("v14", em.op("iinc").eval(vm.get("v12"),vm.get("v0")));
    vm.put("v15", em.op("iinc").eval(vm.get("v14"),vm.get("v0")));
    vm.put("v16", em.op("iinc").eval(vm.get("v15"),vm.get("v0")));
    vm.put("v17", em.op("iinc").eval(vm.get("v16"),vm.get("v0")));
    vm.put("v18", em.op("iinc").eval(vm.get("v17"),vm.get("v0")));
    vm.put("v19", om.popArrayElement(vm.get("v3"), vm.get("v17")));
    vm.put("v20", om.popArrayElement(vm.get("v3"), vm.get("v16")));
    vm.put("v21", om.popArrayElement(vm.get("v3"), vm.get("v15")));
    vm.put("v22", om.popArrayElement(vm.get("v3"), vm.get("v14")));
    if (!em.eq(vm.get("v9"), (vm.get("v8")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v8")));
    vm.put("v23", vm.get("v6"));
    vm.put("v24", em.op("isub").eval(vm.get("v23"),vm.get("v7")));
    om.revertField(vm.get("this"), "modCount", vm.get("v24"));
    if (!em.ge(vm.get("v10"), vm.get("v9"))) e.abort(String.format("!((v10=%s) ge (v9=%s))", vm.get("v10"), vm.get("v9")));
    if (!em.ge(vm.get("v14"), vm.get("v9"))) e.abort(String.format("!((v14=%s) ge (v9=%s))", vm.get("v14"), vm.get("v9")));
    if (!em.ge(vm.get("v12"), vm.get("v9"))) e.abort(String.format("!((v12=%s) ge (v9=%s))", vm.get("v12"), vm.get("v9")));
    if (!em.ge(vm.get("v16"), vm.get("v9"))) e.abort(String.format("!((v16=%s) ge (v9=%s))", vm.get("v16"), vm.get("v9")));
    if (!em.ge(vm.get("v15"), vm.get("v9"))) e.abort(String.format("!((v15=%s) ge (v9=%s))", vm.get("v15"), vm.get("v9")));
    if (!em.lt(vm.get("v18"), vm.get("v9"))) e.abort(String.format("!((v18=%s) lt (v9=%s))", vm.get("v18"), vm.get("v9")));
    if (!em.ge(vm.get("v17"), vm.get("v9"))) e.abort(String.format("!((v17=%s) ge (v9=%s))", vm.get("v17"), vm.get("v9")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(I)V",definedOutput={"this.threshold","this.table","this.modCount","this.loadFactor"},delta={"this.threshold","this.table","this.modCount","this.loadFactor"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_1889(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "threshold"));
    vm.put("v1", (int)1);
    vm.put("v2", (float)0.75F);
    vm.put("v3", om.popField(vm.get("this"), "modCount"));
    vm.put("v4", (int)0);
    vm.put("v5", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v6", om.popField(vm.get("this"), "table"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (float)2.14748365E9F);
    vm.put("v9", (float)0.0F);
    vm.put("v10", om.getArrayLength(vm.get("v7")));
    vm.put("v11", em.op("i2f").eval(vm.get("v1")));
    vm.put("v12", em.op("fmul").eval(vm.get("v11"),vm.get("v2")));
    vm.put("v13", em.op("f2i").eval(vm.get("v12")));
    vm.put("v14", em.op("fcmpg").eval(vm.get("v12"),vm.get("v8")));
    vm.put("v15", em.op("fcmpl").eval(vm.get("v12"),vm.get("v9")));
    vm.put("v16", em.op("fcmpl").eval(vm.get("v12"),vm.get("v12")));
    if (!em.eq(vm.get("v4"), (vm.get("v3")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v3")));
    if (!em.eq(vm.get("v13"), (vm.get("v0")))) e.abort("Inconsistent value for \"v13\": " + vm.get("v13") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v2"), (vm.get("v5")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v5")));
    if (!em.eq(vm.get("v1"), (vm.get("v10")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v10")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.le(vm.get("v14"), vm.get("v4"))) e.abort(String.format("!((v14=%s) le (v4=%s))", vm.get("v14"), vm.get("v4")));
    if (!em.ge(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) ge (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.ne(vm.get("v15"), vm.get("v4"))) e.abort(String.format("!((v15=%s) ne (v4=%s))", vm.get("v15"), vm.get("v4")));
    if (!em.eq(vm.get("v16"), vm.get("v4"))) e.abort(String.format("!((v16=%s) eq (v4=%s))", vm.get("v16"), vm.get("v4")));
    if (!em.eq(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) eq (v4=%s))", vm.get("param0"), vm.get("v4")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(IF)V",definedOutput={"this.threshold","this.table","this.modCount","this.loadFactor"},delta={"this.threshold","this.table","this.modCount","this.loadFactor"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_11325(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", (int)1);
    vm.put("this", om.getTarget());
    vm.put("v1", om.popField(vm.get("this"), "modCount"));
    vm.put("v2", (int)0);
    vm.put("v3", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v4", om.popField(vm.get("this"), "threshold"));
    vm.put("v5", om.popField(vm.get("this"), "table"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (float)2.14748365E9F);
    vm.put("v8", (float)0.0F);
    vm.put("v9", om.getArrayLength(vm.get("v6")));
    vm.put("v10", em.op("i2f").eval(vm.get("v0")));
    vm.put("v11", vm.get("v4"));
    vm.put("param1", vm.get("v3"));
    vm.put("v12", em.op("fmul").eval(vm.get("v10"),vm.get("param1")));
    vm.put("v13", em.op("fcmpl").eval(vm.get("v12"),vm.get("v12")));
    vm.put("v14", em.op("fcmpg").eval(vm.get("param1"),vm.get("v8")));
    vm.put("v15", em.op("fcmpl").eval(vm.get("param1"),vm.get("param1")));
    vm.put("v16", em.op("fcmpl").eval(vm.get("v12"),vm.get("v8")));
    vm.put("v17", em.op("fcmpg").eval(vm.get("v12"),vm.get("v7")));
    if (!em.eq(vm.get("v0"), (vm.get("v9")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v9")));
    if (!em.eq(vm.get("v2"), (vm.get("v1")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v1")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.eq(vm.get("v11"), (em.op("f2i").eval(vm.get("v12"))))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (em.op("f2i").eval(vm.get("v12"))));
    if (!em.le(vm.get("v17"), vm.get("v2"))) e.abort(String.format("!((v17=%s) le (v2=%s))", vm.get("v17"), vm.get("v2")));
    if (!em.gt(vm.get("v14"), vm.get("v2"))) e.abort(String.format("!((v14=%s) gt (v2=%s))", vm.get("v14"), vm.get("v2")));
    if (!em.ge(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) ge (v2=%s))", vm.get("param0"), vm.get("v2")));
    if (!em.eq(vm.get("v16"), vm.get("v2"))) e.abort(String.format("!((v16=%s) eq (v2=%s))", vm.get("v16"), vm.get("v2")));
    if (!em.eq(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) eq (v2=%s))", vm.get("param0"), vm.get("v2")));
    if (!em.eq(vm.get("v13"), vm.get("v2"))) e.abort(String.format("!((v13=%s) eq (v2=%s))", vm.get("v13"), vm.get("v2")));
    if (!em.eq(vm.get("v15"), vm.get("v2"))) e.abort(String.format("!((v15=%s) eq (v2=%s))", vm.get("v15"), vm.get("v2")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(I)V",definedOutput={"this.threshold","this.table","this.loadFactor","this.modCount"},delta={"this.threshold","this.table","this.loadFactor","this.modCount"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_1899(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v1", (float)0.75F);
    vm.put("v2", om.popField(vm.get("this"), "modCount"));
    vm.put("v3", (int)0);
    vm.put("v4", om.popField(vm.get("this"), "threshold"));
    vm.put("v5", om.popField(vm.get("this"), "table"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (float)2.14748365E9F);
    vm.put("v8", (float)0.0F);
    vm.put("v9", om.getArrayLength(vm.get("v6")));
    vm.put("param0", vm.get("v9"));
    vm.put("v10", em.op("i2f").eval(vm.get("param0")));
    vm.put("v11", em.op("fmul").eval(vm.get("v10"),vm.get("v1")));
    vm.put("v12", em.op("fcmpg").eval(vm.get("v11"),vm.get("v7")));
    vm.put("v13", em.op("fcmpl").eval(vm.get("v11"),vm.get("v11")));
    vm.put("v14", em.op("f2i").eval(vm.get("v11")));
    vm.put("v15", em.op("fcmpl").eval(vm.get("v11"),vm.get("v8")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    if (!em.eq(vm.get("v14"), (vm.get("v4")))) e.abort("Inconsistent value for \"v14\": " + vm.get("v14") + " ne " + (vm.get("v4")));
    if (!em.le(vm.get("v12"), vm.get("v3"))) e.abort(String.format("!((v12=%s) le (v3=%s))", vm.get("v12"), vm.get("v3")));
    if (!em.eq(vm.get("v13"), vm.get("v3"))) e.abort(String.format("!((v13=%s) eq (v3=%s))", vm.get("v13"), vm.get("v3")));
    if (!em.ge(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) ge (v3=%s))", vm.get("param0"), vm.get("v3")));
    if (!em.ne(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) ne (v3=%s))", vm.get("param0"), vm.get("v3")));
    if (!em.eq(vm.get("v15"), vm.get("v3"))) e.abort(String.format("!((v15=%s) eq (v3=%s))", vm.get("v15"), vm.get("v3")));
  }
  @IM(clazz="java.util.Hashtable", name="hashCode", desc="()I",revertedInput={"this.loadFactor"},definedOutput={"this.loadFactor"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"})
  static void hashCode2488(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "loadFactor"));
    vm.put("v3", om.getField(vm.get("this"), "count"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "table"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)2);
    vm.put("v8", null);
    vm.put("v9", (float)0.0F);
    vm.put("v10", om.getArrayLength(vm.get("v6")));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getArrayElement(vm.get("v6"), vm.get("v1")));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getArrayElement(vm.get("v6"), vm.get("v0")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", vm.get("v2"));
    vm.put("v17", em.op("fneg").eval(vm.get("v16")));
    vm.put("v18", em.op("fneg").eval(vm.get("v17")));
    om.revertField(vm.get("this"), "loadFactor", vm.get("v18"));
    vm.put("v19", em.op("fcmpg").eval(vm.get("v18"),vm.get("v9")));
    if (!em.eq(vm.get("v13"), vm.get("v8"))) e.abort(String.format("!((v13=%s) eq (v8=%s))", vm.get("v13"), vm.get("v8")));
    if (!em.ne(vm.get("v4"), vm.get("v1"))) e.abort(String.format("!((v4=%s) ne (v1=%s))", vm.get("v4"), vm.get("v1")));
    if (!em.eq(vm.get("v15"), vm.get("v8"))) e.abort(String.format("!((v15=%s) eq (v8=%s))", vm.get("v15"), vm.get("v8")));
    if (!em.ge(vm.get("v19"), vm.get("v1"))) e.abort(String.format("!((v19=%s) ge (v1=%s))", vm.get("v19"), vm.get("v1")));
    if (!em.lt(vm.get("v1"), vm.get("v11"))) e.abort(String.format("!((v1=%s) lt (v11=%s))", vm.get("v1"), vm.get("v11")));
    if (!em.lt(vm.get("v0"), vm.get("v11"))) e.abort(String.format("!((v0=%s) lt (v11=%s))", vm.get("v0"), vm.get("v11")));
    if (!em.ge(vm.get("v7"), vm.get("v11"))) e.abort(String.format("!((v7=%s) ge (v11=%s))", vm.get("v7"), vm.get("v11")));
  }
  @IM(clazz="java.util.Hashtable", name="clear", desc="()V",revertedInput={"this.modCount"},definedOutput={"this.table[*]","this.count","this.modCount"},delta={"this.table[*]","this.count"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void clear3606(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "table"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", (int)-1);
    vm.put("v3", null);
    vm.put("v4", om.getArrayLength(vm.get("v1")));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popField(vm.get("this"), "count"));
    vm.put("v7", (int)0);
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", (int)1);
    vm.put("v10", em.op("iinc").eval(vm.get("v5"),vm.get("v2")));
    vm.put("v11", em.op("iinc").eval(vm.get("v10"),vm.get("v2")));
    vm.put("v12", om.popArrayElement(vm.get("v1"), vm.get("v11")));
    vm.put("v13", em.op("iinc").eval(vm.get("v11"),vm.get("v2")));
    vm.put("v14", om.popArrayElement(vm.get("v1"), vm.get("v13")));
    vm.put("v15", om.popArrayElement(vm.get("v1"), vm.get("v10")));
    vm.put("v16", em.op("iinc").eval(vm.get("v13"),vm.get("v2")));
    vm.put("v17", em.op("iinc").eval(vm.get("v16"),vm.get("v2")));
    vm.put("v18", om.popArrayElement(vm.get("v1"), vm.get("v16")));
    vm.put("v19", vm.get("v8"));
    if (!em.eq(vm.get("v7"), (vm.get("v6")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v6")));
    vm.put("v20", em.op("isub").eval(vm.get("v19"),vm.get("v9")));
    om.revertField(vm.get("this"), "modCount", vm.get("v20"));
    if (!em.lt(vm.get("v17"), vm.get("v7"))) e.abort(String.format("!((v17=%s) lt (v7=%s))", vm.get("v17"), vm.get("v7")));
    if (!em.ge(vm.get("v10"), vm.get("v7"))) e.abort(String.format("!((v10=%s) ge (v7=%s))", vm.get("v10"), vm.get("v7")));
    if (!em.ge(vm.get("v11"), vm.get("v7"))) e.abort(String.format("!((v11=%s) ge (v7=%s))", vm.get("v11"), vm.get("v7")));
    if (!em.ge(vm.get("v13"), vm.get("v7"))) e.abort(String.format("!((v13=%s) ge (v7=%s))", vm.get("v13"), vm.get("v7")));
    if (!em.ge(vm.get("v16"), vm.get("v7"))) e.abort(String.format("!((v16=%s) ge (v7=%s))", vm.get("v16"), vm.get("v7")));
  }
  @IM(clazz="java.util.Hashtable", name="clear", desc="()V",revertedInput={"this.modCount"},definedOutput={"this.table[*]","this.count","this.modCount"},delta={"this.table[*]","this.count"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void clear3689(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "count"));
    vm.put("v1", (int)0);
    vm.put("v2", (int)-1);
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getField(vm.get("this"), "table"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayLength(vm.get("v7")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", em.op("iinc").eval(vm.get("v9"),vm.get("v2")));
    vm.put("v11", om.popArrayElement(vm.get("v7"), vm.get("v10")));
    vm.put("v12", em.op("iinc").eval(vm.get("v10"),vm.get("v2")));
    vm.put("v13", om.popArrayElement(vm.get("v7"), vm.get("v12")));
    vm.put("v14", em.op("iinc").eval(vm.get("v12"),vm.get("v2")));
    vm.put("v15", em.op("iinc").eval(vm.get("v14"),vm.get("v2")));
    vm.put("v16", om.popArrayElement(vm.get("v7"), vm.get("v14")));
    vm.put("v17", vm.get("v4"));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v5")));
    om.revertField(vm.get("this"), "modCount", vm.get("v18"));
    if (!em.ge(vm.get("v14"), vm.get("v1"))) e.abort(String.format("!((v14=%s) ge (v1=%s))", vm.get("v14"), vm.get("v1")));
    if (!em.lt(vm.get("v15"), vm.get("v1"))) e.abort(String.format("!((v15=%s) lt (v1=%s))", vm.get("v15"), vm.get("v1")));
    if (!em.ge(vm.get("v12"), vm.get("v1"))) e.abort(String.format("!((v12=%s) ge (v1=%s))", vm.get("v12"), vm.get("v1")));
    if (!em.ge(vm.get("v10"), vm.get("v1"))) e.abort(String.format("!((v10=%s) ge (v1=%s))", vm.get("v10"), vm.get("v1")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(IF)V",definedOutput={"this.threshold","this.table","this.loadFactor","this.modCount"},delta={"this.threshold","this.table","this.loadFactor","this.modCount"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_11304(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "threshold"));
    vm.put("v1", (float)2.14748365E9F);
    vm.put("v2", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v3", om.popField(vm.get("this"), "modCount"));
    vm.put("v4", (int)0);
    vm.put("v5", om.popField(vm.get("this"), "table"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (float)0.0F);
    vm.put("v8", em.op("f2i").eval(vm.get("v1")));
    vm.put("v9", om.getArrayLength(vm.get("v6")));
    vm.put("param0", vm.get("v9"));
    vm.put("v10", em.op("i2f").eval(vm.get("param0")));
    vm.put("param1", vm.get("v2"));
    vm.put("v11", em.op("fcmpl").eval(vm.get("param1"),vm.get("param1")));
    vm.put("v12", em.op("fmul").eval(vm.get("v10"),vm.get("param1")));
    vm.put("v13", em.op("fcmpl").eval(vm.get("v12"),vm.get("v7")));
    vm.put("v14", em.op("fcmpl").eval(vm.get("v12"),vm.get("v12")));
    vm.put("v15", em.op("fcmpg").eval(vm.get("param1"),vm.get("v7")));
    vm.put("v16", em.op("fcmpg").eval(vm.get("v12"),vm.get("v1")));
    if (!em.eq(vm.get("v4"), (vm.get("v3")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v3")));
    if (!em.eq(vm.get("v8"), (vm.get("v0")))) e.abort("Inconsistent value for \"v8\": " + vm.get("v8") + " ne " + (vm.get("v0")));
    if (!em.ne(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) ne (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.ge(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) ge (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.gt(vm.get("v16"), vm.get("v4"))) e.abort(String.format("!((v16=%s) gt (v4=%s))", vm.get("v16"), vm.get("v4")));
    if (!em.eq(vm.get("v14"), vm.get("v4"))) e.abort(String.format("!((v14=%s) eq (v4=%s))", vm.get("v14"), vm.get("v4")));
    if (!em.eq(vm.get("v11"), vm.get("v4"))) e.abort(String.format("!((v11=%s) eq (v4=%s))", vm.get("v11"), vm.get("v4")));
    if (!em.ne(vm.get("v13"), vm.get("v4"))) e.abort(String.format("!((v13=%s) ne (v4=%s))", vm.get("v13"), vm.get("v4")));
    if (!em.gt(vm.get("v15"), vm.get("v4"))) e.abort(String.format("!((v15=%s) gt (v4=%s))", vm.get("v15"), vm.get("v4")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="()V",definedOutput={"this.table","this.threshold","this.loadFactor","this.modCount"},delta={"this.table","this.threshold","this.loadFactor","this.modCount"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_1099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "table"));
    vm.put("v1", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v2", (float)0.75F);
    vm.put("v3", om.popField(vm.get("this"), "modCount"));
    vm.put("v4", (int)0);
    vm.put("v5", (int)11);
    vm.put("v6", om.popField(vm.get("this"), "threshold"));
    vm.put("v7", em.op("i2f").eval(vm.get("v5")));
    vm.put("v8", em.op("fmul").eval(vm.get("v7"),vm.get("v2")));
    vm.put("v9", em.op("f2i").eval(vm.get("v8")));
    vm.put("v10", em.op("fcmpl").eval(vm.get("v8"),vm.get("v8")));
    if (!em.eq(vm.get("v2"), (vm.get("v1")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v1")));
    vm.put("v11", vm.get("v0"));
    vm.put("v12", om.getArrayLength(vm.get("v11")));
    if (!em.eq(vm.get("v4"), (vm.get("v3")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v3")));
    if (!em.eq(vm.get("v9"), (vm.get("v6")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v6")));
    if (!em.eq(vm.get("v5"), (vm.get("v12")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v12")));
    if (!em.ne(vm.get("v10"), vm.get("v4"))) e.abort(String.format("!((v10=%s) ne (v4=%s))", vm.get("v10"), vm.get("v4")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(IF)V",definedOutput={"this.table","this.threshold","this.modCount","this.loadFactor"},delta={"this.table","this.threshold","this.modCount","this.loadFactor"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_11285(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "table"));
    vm.put("v1", om.popField(vm.get("this"), "threshold"));
    vm.put("v2", om.popField(vm.get("this"), "modCount"));
    vm.put("v3", (int)0);
    vm.put("v4", om.popField(vm.get("this"), "loadFactor"));
    vm.put("param1", vm.get("v4"));
    vm.put("v5", (float)2.14748365E9F);
    vm.put("v6", (float)0.0F);
    vm.put("v7", em.op("fcmpl").eval(vm.get("param1"),vm.get("param1")));
    vm.put("v8", em.op("fcmpg").eval(vm.get("param1"),vm.get("v6")));
    vm.put("v9", vm.get("v1"));
    vm.put("v10", vm.get("v0"));
    vm.put("v11", om.getArrayLength(vm.get("v10")));
    vm.put("param0", vm.get("v11"));
    vm.put("v12", em.op("i2f").eval(vm.get("param0")));
    vm.put("v13", em.op("fmul").eval(vm.get("v12"),vm.get("param1")));
    vm.put("v14", em.op("fcmpg").eval(vm.get("v13"),vm.get("v5")));
    vm.put("v15", em.op("fcmpl").eval(vm.get("v13"),vm.get("v13")));
    vm.put("v16", em.op("fcmpl").eval(vm.get("v13"),vm.get("v6")));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    if (!em.eq(vm.get("v9"), (em.op("f2i").eval(vm.get("v13"))))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (em.op("f2i").eval(vm.get("v13"))));
    if (!em.ne(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) ne (v3=%s))", vm.get("param0"), vm.get("v3")));
    if (!em.gt(vm.get("v8"), vm.get("v3"))) e.abort(String.format("!((v8=%s) gt (v3=%s))", vm.get("v8"), vm.get("v3")));
    if (!em.eq(vm.get("v16"), vm.get("v3"))) e.abort(String.format("!((v16=%s) eq (v3=%s))", vm.get("v16"), vm.get("v3")));
    if (!em.ge(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) ge (v3=%s))", vm.get("param0"), vm.get("v3")));
    if (!em.le(vm.get("v14"), vm.get("v3"))) e.abort(String.format("!((v14=%s) le (v3=%s))", vm.get("v14"), vm.get("v3")));
    if (!em.eq(vm.get("v15"), vm.get("v3"))) e.abort(String.format("!((v15=%s) eq (v3=%s))", vm.get("v15"), vm.get("v3")));
    if (!em.eq(vm.get("v7"), vm.get("v3"))) e.abort(String.format("!((v7=%s) eq (v3=%s))", vm.get("v7"), vm.get("v3")));
  }
  @IM(clazz="java.util.Hashtable", name="put", desc="(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.modCount","this.table[*].value","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void put2286(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "threshold"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "count"));
    vm.put("v4", (int)1);
    vm.put("v5", om.getField(vm.get("this"), "table"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "modCount"));
    vm.put("v8", (int)2147483639);
    vm.put("v9", (int)0);
    vm.put("v10", null);
    vm.put("v11", om.getArrayLength(vm.get("v6")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", em.op("ishl").eval(vm.get("v12"),vm.get("v4")));
    vm.put("v14", em.op("iadd").eval(vm.get("v13"),vm.get("v4")));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v8")));
    vm.put("v16", vm.get("v3"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v4")));
    om.revertField(vm.get("this"), "count", vm.get("v17"));
    vm.put("v18", vm.get("v7"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v4")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("v20", om.guessArrayIndex(vm.get("v6")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.getArrayElement(vm.get("v6"), vm.get("v21")));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.guessArrayIndex(vm.get("v6")));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", om.getArrayElement(vm.get("v6"), vm.get("v25")));
    vm.put("v27", vm.get("v26"));
    vm.put("v28", om.popField(vm.get("v27"), "value"));
    vm.put("param1", vm.get("v28"));
    vm.put("v29", om.popField(vm.get("v27"), "hash"));
    vm.put("v30", vm.get("v29"));
    vm.put("v31", om.popField(vm.get("v27"), "key"));
    vm.put("param0", vm.get("v31"));
    vm.put("v32", om.popField(vm.get("v27"), "next"));
    vm.put("v33", vm.get("v32"));
    om.revertArrayElement(vm.get("v6"), vm.get("v25"), vm.get("v33"));
    vm.put("v34", em.op("java.lang.Object.hashCode()I").eval(vm.get("param0")));
    vm.put("v35", em.op("iand").eval(vm.get("v34"),vm.get("v0")));
    vm.put("v36", em.op("iand").eval(vm.get("v30"),vm.get("v0")));
    if (!em.eq(vm.get("v25"), (em.op("irem").eval(vm.get("v36"),vm.get("v12"))))) e.abort("Inconsistent value for \"v25\": " + vm.get("v25") + " ne " + (em.op("irem").eval(vm.get("v36"),vm.get("v12"))));
    if (!em.eq(vm.get("v21"), (em.op("irem").eval(vm.get("v35"),vm.get("v12"))))) e.abort("Inconsistent value for \"v21\": " + vm.get("v21") + " ne " + (em.op("irem").eval(vm.get("v35"),vm.get("v12"))));
    if (!em.eq(vm.get("v30"), (em.op("java.lang.Object.hashCode()I").eval(vm.get("param0"))))) e.abort("Inconsistent value for \"v30\": " + vm.get("v30") + " ne " + (em.op("java.lang.Object.hashCode()I").eval(vm.get("param0"))));
    if (!em.ge(vm.get("v17"), vm.get("v2"))) e.abort(String.format("!((v17=%s) ge (v2=%s))", vm.get("v17"), vm.get("v2")));
    if (!em.eq(vm.get("v23"), vm.get("v10"))) e.abort(String.format("!((v23=%s) eq (v10=%s))", vm.get("v23"), vm.get("v10")));
    if (!em.ne(vm.get("param1"), vm.get("v10"))) e.abort(String.format("!((param1=%s) ne (v10=%s))", vm.get("param1"), vm.get("v10")));
    if (!em.eq(vm.get("v12"), vm.get("v8"))) e.abort(String.format("!((v12=%s) eq (v8=%s))", vm.get("v12"), vm.get("v8")));
    if (!em.gt(vm.get("v15"), vm.get("v9"))) e.abort(String.format("!((v15=%s) gt (v9=%s))", vm.get("v15"), vm.get("v9")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(IF)V",definedOutput={"this.threshold","this.table","this.modCount","this.loadFactor"},delta={"this.threshold","this.table","this.modCount","this.loadFactor"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_11308(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "threshold"));
    vm.put("v1", om.popField(vm.get("this"), "modCount"));
    vm.put("v2", (int)0);
    vm.put("v3", om.popField(vm.get("this"), "loadFactor"));
    vm.put("param1", vm.get("v3"));
    vm.put("v4", om.popField(vm.get("this"), "table"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", (float)2.14748365E9F);
    vm.put("v7", (float)0.0F);
    vm.put("v8", om.getArrayLength(vm.get("v5")));
    vm.put("param0", vm.get("v8"));
    vm.put("v9", em.op("fcmpl").eval(vm.get("param1"),vm.get("param1")));
    vm.put("v10", em.op("fcmpg").eval(vm.get("param1"),vm.get("v7")));
    vm.put("v11", em.op("i2f").eval(vm.get("param0")));
    vm.put("v12", em.op("fmul").eval(vm.get("v11"),vm.get("param1")));
    vm.put("v13", em.op("fcmpl").eval(vm.get("v12"),vm.get("v7")));
    vm.put("v14", em.op("fcmpl").eval(vm.get("v12"),vm.get("v12")));
    vm.put("v15", em.op("f2i").eval(vm.get("v12")));
    vm.put("v16", em.op("fcmpg").eval(vm.get("v12"),vm.get("v6")));
    if (!em.eq(vm.get("v15"), (vm.get("v0")))) e.abort("Inconsistent value for \"v15\": " + vm.get("v15") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v2"), (vm.get("v1")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v1")));
    if (!em.le(vm.get("v16"), vm.get("v2"))) e.abort(String.format("!((v16=%s) le (v2=%s))", vm.get("v16"), vm.get("v2")));
    if (!em.gt(vm.get("v10"), vm.get("v2"))) e.abort(String.format("!((v10=%s) gt (v2=%s))", vm.get("v10"), vm.get("v2")));
    if (!em.ge(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) ge (v2=%s))", vm.get("param0"), vm.get("v2")));
    if (!em.eq(vm.get("v14"), vm.get("v2"))) e.abort(String.format("!((v14=%s) eq (v2=%s))", vm.get("v14"), vm.get("v2")));
    if (!em.ne(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) ne (v2=%s))", vm.get("param0"), vm.get("v2")));
    if (!em.ne(vm.get("v13"), vm.get("v2"))) e.abort(String.format("!((v13=%s) ne (v2=%s))", vm.get("v13"), vm.get("v2")));
    if (!em.eq(vm.get("v9"), vm.get("v2"))) e.abort(String.format("!((v9=%s) eq (v2=%s))", vm.get("v9"), vm.get("v2")));
  }
  @IM(clazz="java.util.Hashtable", name="merge", desc="(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/function/BiFunction;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.table[*].value","this.modCount","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void merge63(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "table"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "threshold"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayLength(vm.get("v2")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "count"));
    vm.put("v8", (int)1);
    vm.put("v9", om.getField(vm.get("this"), "modCount"));
    vm.put("v10", null);
    vm.put("v11", vm.get("v7"));
    vm.put("v12", vm.get("v9"));
    vm.put("v13", em.op("isub").eval(vm.get("v11"),vm.get("v8")));
    om.revertField(vm.get("this"), "count", vm.get("v13"));
    vm.put("v14", em.op("isub").eval(vm.get("v12"),vm.get("v8")));
    om.revertField(vm.get("this"), "modCount", vm.get("v14"));
    vm.put("v15", om.guessArrayIndex(vm.get("v2")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getArrayElement(vm.get("v2"), vm.get("v16")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.popField(vm.get("v18"), "value"));
    vm.put("param1", vm.get("v19"));
    vm.put("v20", om.popField(vm.get("v18"), "next"));
    vm.put("v21", vm.get("v20"));
    om.revertArrayElement(vm.get("v2"), vm.get("v16"), vm.get("v21"));
    vm.put("v22", om.popField(vm.get("v18"), "hash"));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.getField(vm.get("v21"), "hash"));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", om.popField(vm.get("v18"), "key"));
    vm.put("param0", vm.get("v26"));
    vm.put("v27", em.op("iand").eval(vm.get("v23"),vm.get("v0")));
    vm.put("v28", om.getField(vm.get("v21"), "next"));
    vm.put("v29", vm.get("v28"));
    vm.put("v30", om.getField(vm.get("v29"), "next"));
    vm.put("v31", vm.get("v30"));
    vm.put("v32", om.getField(vm.get("v29"), "hash"));
    vm.put("v33", vm.get("v32"));
    vm.put("v34", om.getField(vm.get("v31"), "hash"));
    vm.put("v35", vm.get("v34"));
    vm.put("v36", om.getField(vm.get("v31"), "next"));
    vm.put("v37", vm.get("v36"));
    vm.put("v38", om.getField(vm.get("v37"), "next"));
    vm.put("v39", vm.get("v38"));
    vm.put("v40", om.getField(vm.get("v37"), "hash"));
    vm.put("v41", vm.get("v40"));
    vm.put("param2", om.newDefaultValue("java.util.function.BiFunction"));
    if (!em.eq(vm.get("v16"), (em.op("irem").eval(vm.get("v27"),vm.get("v6"))))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (em.op("irem").eval(vm.get("v27"),vm.get("v6"))));
    if (!em.ne(vm.get("v33"), vm.get("v23"))) e.abort(String.format("!((v33=%s) ne (v23=%s))", vm.get("v33"), vm.get("v23")));
    if (!em.ne(vm.get("param1"), vm.get("v10"))) e.abort(String.format("!((param1=%s) ne (v10=%s))", vm.get("param1"), vm.get("v10")));
    if (!em.ne(vm.get("v29"), vm.get("v10"))) e.abort(String.format("!((v29=%s) ne (v10=%s))", vm.get("v29"), vm.get("v10")));
    if (!em.ne(vm.get("v37"), vm.get("v10"))) e.abort(String.format("!((v37=%s) ne (v10=%s))", vm.get("v37"), vm.get("v10")));
    if (!em.ne(vm.get("v25"), vm.get("v23"))) e.abort(String.format("!((v25=%s) ne (v23=%s))", vm.get("v25"), vm.get("v23")));
    if (!em.ne(vm.get("param2"), vm.get("v10"))) e.abort(String.format("!((param2=%s) ne (v10=%s))", vm.get("param2"), vm.get("v10")));
    if (!em.ne(vm.get("v41"), vm.get("v23"))) e.abort(String.format("!((v41=%s) ne (v23=%s))", vm.get("v41"), vm.get("v23")));
    if (!em.ne(vm.get("v21"), vm.get("v10"))) e.abort(String.format("!((v21=%s) ne (v10=%s))", vm.get("v21"), vm.get("v10")));
    if (!em.lt(vm.get("v13"), vm.get("v4"))) e.abort(String.format("!((v13=%s) lt (v4=%s))", vm.get("v13"), vm.get("v4")));
    if (!em.eq(vm.get("v39"), vm.get("v10"))) e.abort(String.format("!((v39=%s) eq (v10=%s))", vm.get("v39"), vm.get("v10")));
    if (!em.ne(vm.get("v35"), vm.get("v23"))) e.abort(String.format("!((v35=%s) ne (v23=%s))", vm.get("v35"), vm.get("v23")));
    if (!em.ne(vm.get("v31"), vm.get("v10"))) e.abort(String.format("!((v31=%s) ne (v10=%s))", vm.get("v31"), vm.get("v10")));
  }
  @IM(clazz="java.util.Hashtable", name="hashCode", desc="()I",revertedInput={"this.loadFactor"},definedOutput={"this.loadFactor"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"})
  static void hashCode2499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "loadFactor"));
    vm.put("v1", om.getField(vm.get("this"), "table"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "count"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", (int)0);
    vm.put("v6", (float)0.0F);
    vm.put("v7", om.getArrayLength(vm.get("v2")));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", vm.get("v0"));
    vm.put("v10", em.op("fneg").eval(vm.get("v9")));
    vm.put("v11", em.op("fneg").eval(vm.get("v10")));
    om.revertField(vm.get("this"), "loadFactor", vm.get("v11"));
    vm.put("v12", em.op("fcmpg").eval(vm.get("v11"),vm.get("v6")));
    if (!em.ge(vm.get("v5"), vm.get("v8"))) e.abort(String.format("!((v5=%s) ge (v8=%s))", vm.get("v5"), vm.get("v8")));
    if (!em.ge(vm.get("v12"), vm.get("v5"))) e.abort(String.format("!((v12=%s) ge (v5=%s))", vm.get("v12"), vm.get("v5")));
    if (!em.ne(vm.get("v4"), vm.get("v5"))) e.abort(String.format("!((v4=%s) ne (v5=%s))", vm.get("v4"), vm.get("v5")));
  }
  @IM(clazz="java.util.Hashtable", name="clear", desc="()V",revertedInput={"this.modCount"},definedOutput={"this.table[*]","this.count","this.modCount"},delta={"this.table[*]","this.count"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void clear3695(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "count"));
    vm.put("v1", (int)0);
    vm.put("v2", (int)-1);
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("this"), "modCount"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getField(vm.get("this"), "table"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayLength(vm.get("v7")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", em.op("iinc").eval(vm.get("v9"),vm.get("v2")));
    vm.put("v11", em.op("iinc").eval(vm.get("v10"),vm.get("v2")));
    vm.put("v12", om.popArrayElement(vm.get("v7"), vm.get("v11")));
    vm.put("v13", em.op("iinc").eval(vm.get("v11"),vm.get("v2")));
    vm.put("v14", om.popArrayElement(vm.get("v7"), vm.get("v10")));
    vm.put("v15", vm.get("v4"));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v5")));
    om.revertField(vm.get("this"), "modCount", vm.get("v16"));
    if (!em.ge(vm.get("v10"), vm.get("v1"))) e.abort(String.format("!((v10=%s) ge (v1=%s))", vm.get("v10"), vm.get("v1")));
    if (!em.lt(vm.get("v13"), vm.get("v1"))) e.abort(String.format("!((v13=%s) lt (v1=%s))", vm.get("v13"), vm.get("v1")));
    if (!em.ge(vm.get("v11"), vm.get("v1"))) e.abort(String.format("!((v11=%s) ge (v1=%s))", vm.get("v11"), vm.get("v1")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(I)V",definedOutput={"this.table","this.threshold","this.loadFactor","this.modCount"},delta={"this.table","this.threshold","this.loadFactor","this.modCount"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_1879(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v1", (float)0.75F);
    vm.put("v2", om.popField(vm.get("this"), "table"));
    vm.put("v3", om.popField(vm.get("this"), "modCount"));
    vm.put("v4", (int)0);
    vm.put("v5", om.popField(vm.get("this"), "threshold"));
    vm.put("v6", (float)2.14748365E9F);
    vm.put("v7", (float)0.0F);
    vm.put("v8", em.op("f2i").eval(vm.get("v6")));
    vm.put("v9", vm.get("v2"));
    vm.put("v10", om.getArrayLength(vm.get("v9")));
    vm.put("param0", vm.get("v10"));
    vm.put("v11", em.op("i2f").eval(vm.get("param0")));
    vm.put("v12", em.op("fmul").eval(vm.get("v11"),vm.get("v1")));
    vm.put("v13", em.op("fcmpl").eval(vm.get("v12"),vm.get("v12")));
    vm.put("v14", em.op("fcmpl").eval(vm.get("v12"),vm.get("v7")));
    vm.put("v15", em.op("fcmpg").eval(vm.get("v12"),vm.get("v6")));
    if (!em.eq(vm.get("v4"), (vm.get("v3")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v3")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v8"), (vm.get("v5")))) e.abort("Inconsistent value for \"v8\": " + vm.get("v8") + " ne " + (vm.get("v5")));
    if (!em.eq(vm.get("v13"), vm.get("v4"))) e.abort(String.format("!((v13=%s) eq (v4=%s))", vm.get("v13"), vm.get("v4")));
    if (!em.ge(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) ge (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.gt(vm.get("v15"), vm.get("v4"))) e.abort(String.format("!((v15=%s) gt (v4=%s))", vm.get("v15"), vm.get("v4")));
    if (!em.ne(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) ne (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.eq(vm.get("v14"), vm.get("v4"))) e.abort(String.format("!((v14=%s) eq (v4=%s))", vm.get("v14"), vm.get("v4")));
  }
  @IM(clazz="java.util.Hashtable", name="merge", desc="(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/function/BiFunction;)Ljava/lang/Object;",revertedInput={"this.table[*]","this.count","this.modCount"},definedOutput={"this.table[*]","this.table[*].key","this.count","this.modCount","this.table[*].value","this.table[*].next","this.table[*].hash"},delta={"this.table[*].key","this.table[*].value","this.table[*].next","this.table[*].hash"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void merge43(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2147483647);
    vm.put("v1", om.getField(vm.get("this"), "modCount"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "table"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "count"));
    vm.put("v6", om.getField(vm.get("this"), "threshold"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayLength(vm.get("v4")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)2147483639);
    vm.put("v11", (int)0);
    vm.put("v12", null);
    vm.put("v13", em.op("ishl").eval(vm.get("v9"),vm.get("v2")));
    vm.put("v14", em.op("iadd").eval(vm.get("v13"),vm.get("v2")));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v10")));
    vm.put("v16", vm.get("v5"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v2")));
    om.revertField(vm.get("this"), "count", vm.get("v17"));
    vm.put("v18", vm.get("v1"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v2")));
    om.revertField(vm.get("this"), "modCount", vm.get("v19"));
    vm.put("v20", om.guessArrayIndex(vm.get("v4")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.getArrayElement(vm.get("v4"), vm.get("v21")));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.popField(vm.get("v23"), "hash"));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", em.op("iand").eval(vm.get("v25"),vm.get("v0")));
    vm.put("v27", om.popField(vm.get("v23"), "value"));
    vm.put("param1", vm.get("v27"));
    vm.put("v28", om.popField(vm.get("v23"), "key"));
    vm.put("param0", vm.get("v28"));
    vm.put("v29", em.op("java.lang.Object.hashCode()I").eval(vm.get("param0")));
    vm.put("v30", em.op("iand").eval(vm.get("v29"),vm.get("v0")));
    vm.put("v31", em.op("irem").eval(vm.get("v30"),vm.get("v9")));
    vm.put("v32", om.getArrayElement(vm.get("v4"), vm.get("v31")));
    vm.put("v33", vm.get("v32"));
    vm.put("v34", om.getField(vm.get("v33"), "hash"));
    vm.put("v35", vm.get("v34"));
    vm.put("v36", om.getField(vm.get("v33"), "next"));
    vm.put("v37", vm.get("v36"));
    vm.put("v38", om.popField(vm.get("v23"), "next"));
    vm.put("v39", vm.get("v38"));
    om.revertArrayElement(vm.get("v4"), vm.get("v21"), vm.get("v39"));
    vm.put("param2", om.newDefaultValue("java.util.function.BiFunction"));
    if (!em.eq(vm.get("v21"), (em.op("irem").eval(vm.get("v26"),vm.get("v9"))))) e.abort("Inconsistent value for \"v21\": " + vm.get("v21") + " ne " + (em.op("irem").eval(vm.get("v26"),vm.get("v9"))));
    if (!em.eq(vm.get("v25"), (em.op("java.lang.Object.hashCode()I").eval(vm.get("param0"))))) e.abort("Inconsistent value for \"v25\": " + vm.get("v25") + " ne " + (em.op("java.lang.Object.hashCode()I").eval(vm.get("param0"))));
    if (!em.eq(vm.get("v37"), vm.get("v12"))) e.abort(String.format("!((v37=%s) eq (v12=%s))", vm.get("v37"), vm.get("v12")));
    if (!em.ne(vm.get("v35"), vm.get("v29"))) e.abort(String.format("!((v35=%s) ne (v29=%s))", vm.get("v35"), vm.get("v29")));
    if (!em.eq(vm.get("v9"), vm.get("v10"))) e.abort(String.format("!((v9=%s) eq (v10=%s))", vm.get("v9"), vm.get("v10")));
    if (!em.ne(vm.get("v33"), vm.get("v12"))) e.abort(String.format("!((v33=%s) ne (v12=%s))", vm.get("v33"), vm.get("v12")));
    if (!em.ne(vm.get("param2"), vm.get("v12"))) e.abort(String.format("!((param2=%s) ne (v12=%s))", vm.get("param2"), vm.get("v12")));
    if (!em.ge(vm.get("v17"), vm.get("v7"))) e.abort(String.format("!((v17=%s) ge (v7=%s))", vm.get("v17"), vm.get("v7")));
    if (!em.ne(vm.get("param1"), vm.get("v12"))) e.abort(String.format("!((param1=%s) ne (v12=%s))", vm.get("param1"), vm.get("v12")));
    if (!em.gt(vm.get("v15"), vm.get("v11"))) e.abort(String.format("!((v15=%s) gt (v11=%s))", vm.get("v15"), vm.get("v11")));
  }
  @IM(clazz="java.util.Hashtable", name="values", desc="()Ljava/util/Collection;",revertedInput={"this.values"},definedOutput={"this.values.mutex","this.values.c.this$0","this.values","this.values.c"},delta={"this.values.mutex","this.values.c.this$0","this.values.c"})
  static void values2597(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "values"));
    vm.put("v1", null);
    vm.put("v2", vm.get("v0"));
    vm.put("v3", om.popField(vm.get("v2"), "c"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.popField(vm.get("v2"), "mutex"));
    vm.put("v6", om.popField(vm.get("v4"), "this$0"));
    if (!em.eq(vm.get("this"), (vm.get("v5")))) e.abort("Inconsistent value for \"this\": " + vm.get("this") + " ne " + (vm.get("v5")));
    if (!em.eq(vm.get("this"), (vm.get("v6")))) e.abort("Inconsistent value for \"this\": " + vm.get("this") + " ne " + (vm.get("v6")));
    om.revertField(vm.get("this"), "values");
    vm.put("v7", om.getField(vm.get("this"), "values"));
    if (!em.eq(vm.get("v7"), vm.get("v1"))) e.abort(String.format("!((v7=%s) eq (v1=%s))", vm.get("v7"), vm.get("v1")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(IF)V",definedOutput={"this.table","this.threshold","this.loadFactor","this.modCount"},delta={"this.table","this.threshold","this.loadFactor","this.modCount"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_11287(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v1", om.popField(vm.get("this"), "table"));
    vm.put("v2", om.popField(vm.get("this"), "modCount"));
    vm.put("v3", (int)0);
    vm.put("v4", om.popField(vm.get("this"), "threshold"));
    vm.put("v5", (float)2.14748365E9F);
    vm.put("v6", (int)1);
    vm.put("v7", (float)0.0F);
    vm.put("v8", em.op("i2f").eval(vm.get("v6")));
    vm.put("v9", em.op("f2i").eval(vm.get("v5")));
    vm.put("param1", vm.get("v0"));
    vm.put("v10", em.op("fcmpg").eval(vm.get("param1"),vm.get("v7")));
    vm.put("v11", em.op("fmul").eval(vm.get("v8"),vm.get("param1")));
    vm.put("v12", em.op("fcmpl").eval(vm.get("v11"),vm.get("v7")));
    vm.put("v13", em.op("fcmpl").eval(vm.get("v11"),vm.get("v11")));
    vm.put("v14", em.op("fcmpg").eval(vm.get("v11"),vm.get("v5")));
    vm.put("v15", em.op("fcmpl").eval(vm.get("param1"),vm.get("param1")));
    vm.put("v16", vm.get("v1"));
    vm.put("v17", om.getArrayLength(vm.get("v16")));
    if (!em.eq(vm.get("v6"), (vm.get("v17")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v17")));
    if (!em.eq(vm.get("v9"), (vm.get("v4")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v4")));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.gt(vm.get("v14"), vm.get("v3"))) e.abort(String.format("!((v14=%s) gt (v3=%s))", vm.get("v14"), vm.get("v3")));
    if (!em.eq(vm.get("v12"), vm.get("v3"))) e.abort(String.format("!((v12=%s) eq (v3=%s))", vm.get("v12"), vm.get("v3")));
    if (!em.eq(vm.get("v15"), vm.get("v3"))) e.abort(String.format("!((v15=%s) eq (v3=%s))", vm.get("v15"), vm.get("v3")));
    if (!em.gt(vm.get("v10"), vm.get("v3"))) e.abort(String.format("!((v10=%s) gt (v3=%s))", vm.get("v10"), vm.get("v3")));
    if (!em.ge(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) ge (v3=%s))", vm.get("param0"), vm.get("v3")));
    if (!em.eq(vm.get("v13"), vm.get("v3"))) e.abort(String.format("!((v13=%s) eq (v3=%s))", vm.get("v13"), vm.get("v3")));
    if (!em.eq(vm.get("param0"), vm.get("v3"))) e.abort(String.format("!((param0=%s) eq (v3=%s))", vm.get("param0"), vm.get("v3")));
  }
  @IM(clazz="java.util.Hashtable", name="hashCode", desc="()I",revertedInput={"this.loadFactor"},definedOutput={"this.loadFactor"})
  @DG(pred={"merge43","clear3699","merge63","_init_11307","clear3683","_init_1095","put2249","put2290","_init_1073","addEntry2796","_init_3127","merge98","_init_1898","putIfAbsent1289","clear3606","_init_1855","_init_1892","_init_11308","_init_1879","clear3695","_init_1097","putIfAbsent1297","_init_1094","merge68","putIfAbsent1281","_init_1099","_init_11311","_init_11285","_init_11321","_init_11325","addEntry2799","_init_1889","clear3646","_init_1877","_init_11304","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","_init_1896","put2286","_init_1897","put2257","_init_11328","merge60","_init_1895","_init_1899","_init_11287","merge76"})
  static void hashCode2438(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "table"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "count"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayLength(vm.get("v2")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "loadFactor"));
    vm.put("v8", (int)1);
    vm.put("v9", null);
    vm.put("v10", (float)0.0F);
    vm.put("v11", om.getArrayElement(vm.get("v2"), vm.get("v0")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getField(vm.get("v12"), "next"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.getField(vm.get("v14"), "next"));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getField(vm.get("v16"), "next"));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.getField(vm.get("v18"), "next"));
    vm.put("v20", vm.get("v19"));
    vm.put("v21", om.getField(vm.get("v20"), "next"));
    vm.put("v22", vm.get("v21"));
    vm.put("v23", om.getField(vm.get("v22"), "next"));
    vm.put("v24", vm.get("v23"));
    vm.put("v25", om.getField(vm.get("v24"), "next"));
    vm.put("v26", vm.get("v25"));
    vm.put("v27", vm.get("v7"));
    vm.put("v28", em.op("fneg").eval(vm.get("v27")));
    vm.put("v29", em.op("fneg").eval(vm.get("v28")));
    om.revertField(vm.get("this"), "loadFactor", vm.get("v29"));
    vm.put("v30", em.op("fcmpg").eval(vm.get("v29"),vm.get("v10")));
    if (!em.ge(vm.get("v8"), vm.get("v6"))) e.abort(String.format("!((v8=%s) ge (v6=%s))", vm.get("v8"), vm.get("v6")));
    if (!em.ne(vm.get("v12"), vm.get("v9"))) e.abort(String.format("!((v12=%s) ne (v9=%s))", vm.get("v12"), vm.get("v9")));
    if (!em.ne(vm.get("v14"), vm.get("v9"))) e.abort(String.format("!((v14=%s) ne (v9=%s))", vm.get("v14"), vm.get("v9")));
    if (!em.ne(vm.get("v18"), vm.get("v9"))) e.abort(String.format("!((v18=%s) ne (v9=%s))", vm.get("v18"), vm.get("v9")));
    if (!em.lt(vm.get("v0"), vm.get("v6"))) e.abort(String.format("!((v0=%s) lt (v6=%s))", vm.get("v0"), vm.get("v6")));
    if (!em.ge(vm.get("v30"), vm.get("v0"))) e.abort(String.format("!((v30=%s) ge (v0=%s))", vm.get("v30"), vm.get("v0")));
    if (!em.ne(vm.get("v16"), vm.get("v9"))) e.abort(String.format("!((v16=%s) ne (v9=%s))", vm.get("v16"), vm.get("v9")));
    if (!em.eq(vm.get("v26"), vm.get("v9"))) e.abort(String.format("!((v26=%s) eq (v9=%s))", vm.get("v26"), vm.get("v9")));
    if (!em.ne(vm.get("v24"), vm.get("v9"))) e.abort(String.format("!((v24=%s) ne (v9=%s))", vm.get("v24"), vm.get("v9")));
    if (!em.ne(vm.get("v20"), vm.get("v9"))) e.abort(String.format("!((v20=%s) ne (v9=%s))", vm.get("v20"), vm.get("v9")));
    if (!em.ne(vm.get("v4"), vm.get("v0"))) e.abort(String.format("!((v4=%s) ne (v0=%s))", vm.get("v4"), vm.get("v0")));
    if (!em.ne(vm.get("v22"), vm.get("v9"))) e.abort(String.format("!((v22=%s) ne (v9=%s))", vm.get("v22"), vm.get("v9")));
  }
  @IM(clazz="java.util.Hashtable", name="clear", desc="()V",revertedInput={"this.modCount"},definedOutput={"this.table[*]","this.count","this.modCount"},delta={"this.table[*]","this.count"})
  @DG(pred={"_init_1097","_init_1094","_init_11307","_init_1099","_init_11311","_init_1095","_init_11285","_init_11321","_init_11325","_init_1889","_init_1073","_init_1877","_init_11304","_init_3127","_init_1896","_init_1897","_init_1898","_init_11328","_init_1895","_init_1899","_init_11287","_init_1855","_init_1892","_init_11308","_init_1879"},succ={"hashCode2442","hashCode2438","hashCode2485"})
  static void clear3646(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "table"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", (int)-1);
    vm.put("v3", null);
    vm.put("v4", om.getArrayLength(vm.get("v1")));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popField(vm.get("this"), "count"));
    vm.put("v7", (int)0);
    vm.put("v8", om.getField(vm.get("this"), "modCount"));
    vm.put("v9", (int)1);
    vm.put("v10", em.op("iinc").eval(vm.get("v5"),vm.get("v2")));
    vm.put("v11", em.op("iinc").eval(vm.get("v10"),vm.get("v2")));
    vm.put("v12", om.popArrayElement(vm.get("v1"), vm.get("v11")));
    vm.put("v13", em.op("iinc").eval(vm.get("v11"),vm.get("v2")));
    vm.put("v14", om.popArrayElement(vm.get("v1"), vm.get("v10")));
    vm.put("v15", em.op("iinc").eval(vm.get("v13"),vm.get("v2")));
    vm.put("v16", em.op("iinc").eval(vm.get("v15"),vm.get("v2")));
    vm.put("v17", om.popArrayElement(vm.get("v1"), vm.get("v16")));
    vm.put("v18", om.popArrayElement(vm.get("v1"), vm.get("v13")));
    vm.put("v19", em.op("iinc").eval(vm.get("v16"),vm.get("v2")));
    vm.put("v20", om.popArrayElement(vm.get("v1"), vm.get("v15")));
    vm.put("v21", vm.get("v8"));
    vm.put("v22", em.op("isub").eval(vm.get("v21"),vm.get("v9")));
    om.revertField(vm.get("this"), "modCount", vm.get("v22"));
    if (!em.eq(vm.get("v7"), (vm.get("v6")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v6")));
    if (!em.ge(vm.get("v16"), vm.get("v7"))) e.abort(String.format("!((v16=%s) ge (v7=%s))", vm.get("v16"), vm.get("v7")));
    if (!em.ge(vm.get("v11"), vm.get("v7"))) e.abort(String.format("!((v11=%s) ge (v7=%s))", vm.get("v11"), vm.get("v7")));
    if (!em.ge(vm.get("v13"), vm.get("v7"))) e.abort(String.format("!((v13=%s) ge (v7=%s))", vm.get("v13"), vm.get("v7")));
    if (!em.ge(vm.get("v15"), vm.get("v7"))) e.abort(String.format("!((v15=%s) ge (v7=%s))", vm.get("v15"), vm.get("v7")));
    if (!em.lt(vm.get("v19"), vm.get("v7"))) e.abort(String.format("!((v19=%s) lt (v7=%s))", vm.get("v19"), vm.get("v7")));
    if (!em.ge(vm.get("v10"), vm.get("v7"))) e.abort(String.format("!((v10=%s) ge (v7=%s))", vm.get("v10"), vm.get("v7")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(I)V",definedOutput={"this.table","this.threshold","this.loadFactor","this.modCount"},delta={"this.table","this.threshold","this.loadFactor","this.modCount"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_1897(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "table"));
    vm.put("v1", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v2", (float)0.75F);
    vm.put("v3", om.popField(vm.get("this"), "modCount"));
    vm.put("v4", (int)0);
    vm.put("v5", (int)1);
    vm.put("v6", om.popField(vm.get("this"), "threshold"));
    vm.put("v7", (float)2.14748365E9F);
    vm.put("v8", (float)0.0F);
    vm.put("v9", em.op("i2f").eval(vm.get("v5")));
    vm.put("v10", em.op("fmul").eval(vm.get("v9"),vm.get("v2")));
    vm.put("v11", em.op("fcmpl").eval(vm.get("v10"),vm.get("v8")));
    vm.put("v12", em.op("f2i").eval(vm.get("v10")));
    vm.put("v13", em.op("fcmpg").eval(vm.get("v10"),vm.get("v7")));
    vm.put("v14", em.op("fcmpl").eval(vm.get("v10"),vm.get("v10")));
    vm.put("v15", vm.get("v0"));
    vm.put("v16", om.getArrayLength(vm.get("v15")));
    if (!em.eq(vm.get("v2"), (vm.get("v1")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v1")));
    if (!em.eq(vm.get("v4"), (vm.get("v3")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v3")));
    if (!em.eq(vm.get("v5"), (vm.get("v16")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v16")));
    if (!em.eq(vm.get("v12"), (vm.get("v6")))) e.abort("Inconsistent value for \"v12\": " + vm.get("v12") + " ne " + (vm.get("v6")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.le(vm.get("v13"), vm.get("v4"))) e.abort(String.format("!((v13=%s) le (v4=%s))", vm.get("v13"), vm.get("v4")));
    if (!em.eq(vm.get("v14"), vm.get("v4"))) e.abort(String.format("!((v14=%s) eq (v4=%s))", vm.get("v14"), vm.get("v4")));
    if (!em.eq(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) eq (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.ge(vm.get("param0"), vm.get("v4"))) e.abort(String.format("!((param0=%s) ge (v4=%s))", vm.get("param0"), vm.get("v4")));
    if (!em.eq(vm.get("v11"), vm.get("v4"))) e.abort(String.format("!((v11=%s) eq (v4=%s))", vm.get("v11"), vm.get("v4")));
  }
  @IM(clazz="java.util.Hashtable", name="<init>", desc="(I)V",definedOutput={"this.threshold","this.table","this.loadFactor","this.modCount"},delta={"this.threshold","this.table","this.loadFactor","this.modCount"})
  @DG(succ={"merge43","putIfAbsent1297","clear3699","merge63","merge68","putIfAbsent1281","hashCode2438","clear3683","hashCode2485","clear3697","put2249","addEntry2799","hashCode2499","clear3646","hashCode2470","put2290","addEntry2796","put2299","clear3689","putIfAbsent1209","putIfAbsent1271","merge98","put2286","hashCode2442","putIfAbsent1289","put2257","merge60","hashCode2488","clear3606","merge76","clear3695"})
  static void _init_1877(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "loadFactor"));
    vm.put("v1", (float)0.75F);
    vm.put("v2", om.popField(vm.get("this"), "threshold"));
    vm.put("v3", (float)2.14748365E9F);
    vm.put("v4", om.popField(vm.get("this"), "table"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popField(vm.get("this"), "modCount"));
    vm.put("v7", (int)0);
    vm.put("v8", (float)0.0F);
    vm.put("v9", em.op("f2i").eval(vm.get("v3")));
    vm.put("v10", om.getArrayLength(vm.get("v5")));
    vm.put("param0", vm.get("v10"));
    vm.put("v11", em.op("i2f").eval(vm.get("param0")));
    vm.put("v12", em.op("fmul").eval(vm.get("v11"),vm.get("v1")));
    vm.put("v13", em.op("fcmpg").eval(vm.get("v12"),vm.get("v3")));
    vm.put("v14", em.op("fcmpl").eval(vm.get("v12"),vm.get("v8")));
    vm.put("v15", em.op("fcmpl").eval(vm.get("v12"),vm.get("v12")));
    if (!em.eq(vm.get("v7"), (vm.get("v6")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v6")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v9"), (vm.get("v2")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v2")));
    if (!em.ge(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) ge (v7=%s))", vm.get("param0"), vm.get("v7")));
    if (!em.ne(vm.get("v14"), vm.get("v7"))) e.abort(String.format("!((v14=%s) ne (v7=%s))", vm.get("v14"), vm.get("v7")));
    if (!em.eq(vm.get("v15"), vm.get("v7"))) e.abort(String.format("!((v15=%s) eq (v7=%s))", vm.get("v15"), vm.get("v7")));
    if (!em.ne(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) ne (v7=%s))", vm.get("param0"), vm.get("v7")));
    if (!em.gt(vm.get("v13"), vm.get("v7"))) e.abort(String.format("!((v13=%s) gt (v7=%s))", vm.get("v13"), vm.get("v7")));
  }
}
