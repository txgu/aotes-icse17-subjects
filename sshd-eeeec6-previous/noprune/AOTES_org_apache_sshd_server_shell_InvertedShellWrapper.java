import org.javelus.aotes.executor.*;
@Defined({"this.executor","this.in","this.out","this.callback","this.shell","this.bufferSize","this.err"})
public class AOTES_org_apache_sshd_server_shell_InvertedShellWrapper {
  @IM(clazz="org.apache.sshd.server.shell.InvertedShellWrapper", name="setErrorStream", desc="(Ljava/io/OutputStream;)V",definedOutput={"this.err"},delta={"this.err"})
  static void setErrorStream299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "err"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.shell.InvertedShellWrapper", name="<init>", desc="(Lorg/apache/sshd/server/shell/InvertedShell;Ljava/util/concurrent/Executor;I)V",definedOutput={"this.executor","this.shell","this.bufferSize"},delta={"this.executor","this.shell","this.bufferSize"})
  static void _init_499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "bufferSize"));
    vm.put("v1", om.popField(vm.get("this"), "executor"));
    vm.put("v2", om.popField(vm.get("this"), "shell"));
    vm.put("param1", vm.get("v1"));
    vm.put("param2", vm.get("v0"));
    vm.put("param0", vm.get("v2"));
  }
  @IM(clazz="org.apache.sshd.server.shell.InvertedShellWrapper", name="<init>", desc="(Lorg/apache/sshd/server/shell/InvertedShell;Ljava/util/concurrent/Executor;)V",definedOutput={"this.executor","this.shell","this.bufferSize"},delta={"this.executor","this.shell","this.bufferSize"})
  static void _init_599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "bufferSize"));
    vm.put("v1", (int)8192);
    vm.put("v2", om.popField(vm.get("this"), "shell"));
    vm.put("v3", om.popField(vm.get("this"), "executor"));
    vm.put("param1", vm.get("v3"));
    vm.put("param0", vm.get("v2"));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
  }
  @IM(clazz="org.apache.sshd.server.shell.InvertedShellWrapper", name="setInputStream", desc="(Ljava/io/InputStream;)V",definedOutput={"this.in"},delta={"this.in"})
  static void setInputStream1299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "in"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.shell.InvertedShellWrapper", name="setOutputStream", desc="(Ljava/io/OutputStream;)V",definedOutput={"this.out"},delta={"this.out"})
  static void setOutputStream799(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "out"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.shell.InvertedShellWrapper", name="setExitCallback", desc="(Lorg/apache/sshd/server/ExitCallback;)V",definedOutput={"this.callback"},delta={"this.callback"})
  static void setExitCallback199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "callback"));
    vm.put("param0", vm.get("v0"));
  }
}
