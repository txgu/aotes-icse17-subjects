import org.javelus.aotes.executor.*;
@Defined({"this.this$0","this.log"})
public class AOTES_org_apache_sshd_common_channel_AbstractChannel$GracefulChannelCloseable {
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel$GracefulChannelCloseable", name="<init>", desc="(Lorg/apache/sshd/common/channel/AbstractChannel;)V",definedOutput={"this.this$0","this.log"},delta={"this.this$0","this.log"})
  static void _init_2599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "this$0"));
    vm.put("v1", om.popField(vm.get("this"), "log"));
    vm.put("v2", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("this")));
    vm.put("v3", em.op("org.slf4j.LoggerFactory.getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;").eval(vm.get("v2")));
    vm.put("param0", vm.get("v0"));
    if (!em.eq(vm.get("v3"), (vm.get("v1")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v1")));
  }
}
