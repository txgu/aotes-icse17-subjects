import org.javelus.aotes.executor.*;
@Defined({"this.state.value","this.eof","this.gracefulFuture.listeners","this.gracefulFuture.listeners.this$0","this.gracefulFuture.listeners[*].this$0","this.gracefulFuture.listeners[*]","this.closeFuture.result"})
public class AOTES_org_apache_sshd_common_channel_AbstractChannel {
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="doCloseImmediately", desc="()V",revertedInput={"this.gracefulFuture.listeners"},definedOutput={"this.gracefulFuture.listeners","this.gracefulFuture.listeners.this$0"},delta={"this.gracefulFuture.listeners.this$0"})
  static void doCloseImmediately85090(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "service"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "gracefulFuture"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v3"), "listeners"));
    vm.put("v5", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", null);
    vm.put("v8", om.getField(vm.get("v3"), "lock"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("v3"), "result"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", vm.get("v4"));
    vm.put("v13", om.popField(vm.get("v12"), "this$0"));
    if (!em.eq(vm.get("this"), (vm.get("v13")))) e.abort("Inconsistent value for \"this\": " + vm.get("this") + " ne " + (vm.get("v13")));
    om.revertField(vm.get("v3"), "listeners");
    vm.put("v14", om.getField(vm.get("v3"), "listeners"));
    if (!em.eq(vm.get("v11"), vm.get("v7"))) e.abort(String.format("!((v11=%s) eq (v7=%s))", vm.get("v11"), vm.get("v7")));
    if (!em.eq(vm.get("v1"), vm.get("v7"))) e.abort(String.format("!((v1=%s) eq (v7=%s))", vm.get("v1"), vm.get("v7")));
    if (!em.eq(vm.get("v14"), vm.get("v7"))) e.abort(String.format("!((v14=%s) eq (v7=%s))", vm.get("v14"), vm.get("v7")));
    if (!em.ne(vm.get("v11"), vm.get("v7"))) e.abort(String.format("!((v11=%s) ne (v7=%s))", vm.get("v11"), vm.get("v7")));
  }
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="doCloseImmediately", desc="()V",revertedInput={"this.gracefulFuture.listeners"},definedOutput={"this.gracefulFuture.listeners","this.gracefulFuture.listeners[*].this$0","this.gracefulFuture.listeners[*]"},delta={"this.gracefulFuture.listeners[*].this$0","this.gracefulFuture.listeners[*]"})
  static void doCloseImmediately85061(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", (int)0);
    vm.put("v4", om.getField(vm.get("this"), "service"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", (int)2);
    vm.put("v7", om.getField(vm.get("this"), "gracefulFuture"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", null);
    vm.put("v10", om.getField(vm.get("v8"), "listeners"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("v8"), "result"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.popArrayElement(vm.get("v11"), vm.get("v3")));
    vm.put("v15", vm.get("v14"));
    om.revertField(vm.get("v8"), "listeners", vm.get("v15"));
    vm.put("v16", om.getField(vm.get("v8"), "lock"));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.getArrayLength(vm.get("v11")));
    vm.put("v19", om.popArrayElement(vm.get("v11"), vm.get("v0")));
    vm.put("v20", vm.get("v19"));
    vm.put("v21", om.popField(vm.get("v20"), "this$0"));
    if (!em.eq(vm.get("this"), (vm.get("v21")))) e.abort("Inconsistent value for \"this\": " + vm.get("this") + " ne " + (vm.get("v21")));
    if (!em.eq(vm.get("v6"), (vm.get("v18")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v18")));
    if (!em.ne(vm.get("v15"), vm.get("v9"))) e.abort(String.format("!((v15=%s) ne (v9=%s))", vm.get("v15"), vm.get("v9")));
    if (!em.ne(vm.get("v13"), vm.get("v9"))) e.abort(String.format("!((v13=%s) ne (v9=%s))", vm.get("v13"), vm.get("v9")));
    if (!em.eq(vm.get("v13"), vm.get("v9"))) e.abort(String.format("!((v13=%s) eq (v9=%s))", vm.get("v13"), vm.get("v9")));
    if (!em.eq(vm.get("v5"), vm.get("v9"))) e.abort(String.format("!((v5=%s) eq (v9=%s))", vm.get("v5"), vm.get("v9")));
  }
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="doCloseImmediately", desc="()V",definedOutput={"this.state.value"},delta={"this.state.value"})
  static void doCloseImmediately85103(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "state"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "gracefulFuture"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "service"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getStatic("org.apache.sshd.common.util.CloseableUtils$AbstractCloseable$State.Closed"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("this"), "closeFuture"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("v3"), "result"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("v11"), "lock"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.getField(vm.get("v11"), "result"));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.popField(vm.get("v1"), "value"));
    vm.put("v19", null);
    vm.put("v20", om.getField(vm.get("v3"), "lock"));
    vm.put("v21", vm.get("v20"));
    if (!em.eq(vm.get("v9"), (vm.get("v18")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v18")));
    if (!em.ne(vm.get("v13"), vm.get("v19"))) e.abort(String.format("!((v13=%s) ne (v19=%s))", vm.get("v13"), vm.get("v19")));
    if (!em.ne(vm.get("v13"), vm.get("v19"))) e.abort(String.format("!((v13=%s) ne (v19=%s))", vm.get("v13"), vm.get("v19")));
    if (!em.eq(vm.get("v7"), vm.get("v19"))) e.abort(String.format("!((v7=%s) eq (v19=%s))", vm.get("v7"), vm.get("v19")));
    if (!em.ne(vm.get("v17"), vm.get("v19"))) e.abort(String.format("!((v17=%s) ne (v19=%s))", vm.get("v17"), vm.get("v19")));
  }
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="doCloseImmediately", desc="()V",revertedInput={"this.closeFuture.result"},definedOutput={"this.state.value","this.closeFuture.result"},delta={"this.state.value"})
  static void doCloseImmediately14809(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "gracefulFuture"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "state"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v1"), "result"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "service"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getStatic("org.apache.sshd.common.util.CloseableUtils$AbstractCloseable$State.Closed"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.popField(vm.get("v3"), "value"));
    vm.put("v13", om.getField(vm.get("v1"), "lock"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.getField(vm.get("this"), "closeFuture"));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getField(vm.get("v16"), "listeners"));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", null);
    vm.put("v20", om.getField(vm.get("v16"), "lock"));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.getField(vm.get("v16"), "result"));
    if (!em.eq(vm.get("v11"), (vm.get("v12")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v12")));
    if (!em.eq(vm.get("v5"), (vm.get("v22")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v22")));
    om.revertField(vm.get("v16"), "result");
    vm.put("v23", om.getField(vm.get("v16"), "result"));
    if (!em.ne(vm.get("v5"), vm.get("v19"))) e.abort(String.format("!((v5=%s) ne (v19=%s))", vm.get("v5"), vm.get("v19")));
    if (!em.ne(vm.get("v7"), vm.get("v19"))) e.abort(String.format("!((v7=%s) ne (v19=%s))", vm.get("v7"), vm.get("v19")));
    if (!em.eq(vm.get("v18"), vm.get("v19"))) e.abort(String.format("!((v18=%s) eq (v19=%s))", vm.get("v18"), vm.get("v19")));
    if (!em.eq(vm.get("v23"), vm.get("v19"))) e.abort(String.format("!((v23=%s) eq (v19=%s))", vm.get("v23"), vm.get("v19")));
    if (!em.ne(vm.get("v7"), vm.get("v19"))) e.abort(String.format("!((v7=%s) ne (v19=%s))", vm.get("v7"), vm.get("v19")));
    if (!em.eq(vm.get("v9"), vm.get("v19"))) e.abort(String.format("!((v9=%s) eq (v19=%s))", vm.get("v9"), vm.get("v19")));
  }
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="doCloseImmediately", desc="()V",revertedInput={"this.closeFuture.result"},definedOutput={"this.state.value","this.closeFuture.result"},delta={"this.state.value"})
  static void doCloseImmediately85044(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "closeFuture"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "gracefulFuture"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("org.apache.sshd.common.future.DefaultSshFuture.NULL"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "state"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("v1"), "result"));
    vm.put("v9", om.getField(vm.get("v3"), "result"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getField(vm.get("v3"), "lock"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.popField(vm.get("v7"), "value"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getField(vm.get("v1"), "lock"));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.getField(vm.get("this"), "service"));
    vm.put("v20", vm.get("v19"));
    vm.put("v21", om.getStatic("org.apache.sshd.common.util.CloseableUtils$AbstractCloseable$State.Closed"));
    vm.put("v22", om.getField(vm.get("v1"), "listeners"));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", null);
    if (!em.eq(vm.get("v5"), (vm.get("v8")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v8")));
    if (!em.eq(vm.get("v14"), (vm.get("v21")))) e.abort("Inconsistent value for \"v14\": " + vm.get("v14") + " ne " + (vm.get("v21")));
    om.revertField(vm.get("v1"), "result");
    vm.put("v25", om.getField(vm.get("v1"), "result"));
    if (!em.ne(vm.get("v10"), vm.get("v24"))) e.abort(String.format("!((v10=%s) ne (v24=%s))", vm.get("v10"), vm.get("v24")));
    if (!em.eq(vm.get("v23"), vm.get("v24"))) e.abort(String.format("!((v23=%s) eq (v24=%s))", vm.get("v23"), vm.get("v24")));
    if (!em.eq(vm.get("v25"), vm.get("v24"))) e.abort(String.format("!((v25=%s) eq (v24=%s))", vm.get("v25"), vm.get("v24")));
    if (!em.ne(vm.get("v10"), vm.get("v24"))) e.abort(String.format("!((v10=%s) ne (v24=%s))", vm.get("v10"), vm.get("v24")));
    if (!em.eq(vm.get("v16"), vm.get("v24"))) e.abort(String.format("!((v16=%s) eq (v24=%s))", vm.get("v16"), vm.get("v24")));
    if (!em.eq(vm.get("v20"), vm.get("v24"))) e.abort(String.format("!((v20=%s) eq (v24=%s))", vm.get("v20"), vm.get("v24")));
  }
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="handleEof", desc="()V",definedOutput={"this.eof"},delta={"this.eof"})
  static void handleEof37899(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "eof"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "lock"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "log"));
    vm.put("v5", vm.get("v4"));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
  }
}
