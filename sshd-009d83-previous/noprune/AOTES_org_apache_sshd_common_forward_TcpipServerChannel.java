import org.javelus.aotes.executor.*;
@Defined({"this.gracefulFuture.listeners[*]","this.eof","this.gracefulFuture.listeners","this.gracefulFuture.listeners.this$0","this.closeFuture.result","this.gracefulFuture.listeners[*].this$0","this.state.value"})
public class AOTES_org_apache_sshd_common_forward_TcpipServerChannel {
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="doCloseImmediately", desc="()V",revertedInput={"this.gracefulFuture.listeners"},definedOutput={"this.gracefulFuture.listeners","this.gracefulFuture.listeners.this$0"},delta={"this.gracefulFuture.listeners.this$0"})
  static void doCloseImmediately90836(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "service"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "gracefulFuture"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v5"), "lock"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", null);
    vm.put("v9", om.getField(vm.get("v5"), "listeners"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getField(vm.get("v5"), "result"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.popField(vm.get("v10"), "this$0"));
    if (!em.eq(vm.get("this"), (vm.get("v13")))) e.abort("Inconsistent value for \"this\": " + vm.get("this") + " ne " + (vm.get("v13")));
    om.revertField(vm.get("v5"), "listeners");
    vm.put("v14", om.getField(vm.get("v5"), "listeners"));
    if (!em.eq(vm.get("v14"), vm.get("v8"))) e.abort(String.format("!((v14=%s) eq (v8=%s))", vm.get("v14"), vm.get("v8")));
    if (!em.eq(vm.get("v1"), vm.get("v8"))) e.abort(String.format("!((v1=%s) eq (v8=%s))", vm.get("v1"), vm.get("v8")));
    if (!em.ne(vm.get("v12"), vm.get("v8"))) e.abort(String.format("!((v12=%s) ne (v8=%s))", vm.get("v12"), vm.get("v8")));
    if (!em.eq(vm.get("v12"), vm.get("v8"))) e.abort(String.format("!((v12=%s) eq (v8=%s))", vm.get("v12"), vm.get("v8")));
  }
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="doCloseImmediately", desc="()V",revertedInput={"this.gracefulFuture.listeners"},definedOutput={"this.gracefulFuture.listeners[*]","this.gracefulFuture.listeners","this.gracefulFuture.listeners[*].this$0"},delta={"this.gracefulFuture.listeners[*]","this.gracefulFuture.listeners[*].this$0"})
  static void doCloseImmediately90890(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2);
    vm.put("v1", om.getField(vm.get("this"), "gracefulFuture"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("v2"), "lock"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("v2"), "listeners"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", (int)0);
    vm.put("v10", om.getField(vm.get("v2"), "result"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("this"), "service"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", (int)1);
    vm.put("v15", null);
    vm.put("v16", om.popArrayElement(vm.get("v6"), vm.get("v9")));
    vm.put("v17", vm.get("v16"));
    om.revertField(vm.get("v2"), "listeners", vm.get("v17"));
    vm.put("v18", om.popArrayElement(vm.get("v6"), vm.get("v14")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", om.popField(vm.get("v19"), "this$0"));
    vm.put("v21", om.getArrayLength(vm.get("v6")));
    if (!em.eq(vm.get("this"), (vm.get("v20")))) e.abort("Inconsistent value for \"this\": " + vm.get("this") + " ne " + (vm.get("v20")));
    if (!em.eq(vm.get("v0"), (vm.get("v21")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v21")));
    if (!em.eq(vm.get("v11"), vm.get("v15"))) e.abort(String.format("!((v11=%s) eq (v15=%s))", vm.get("v11"), vm.get("v15")));
    if (!em.ne(vm.get("v11"), vm.get("v15"))) e.abort(String.format("!((v11=%s) ne (v15=%s))", vm.get("v11"), vm.get("v15")));
    if (!em.eq(vm.get("v13"), vm.get("v15"))) e.abort(String.format("!((v13=%s) eq (v15=%s))", vm.get("v13"), vm.get("v15")));
    if (!em.ne(vm.get("v17"), vm.get("v15"))) e.abort(String.format("!((v17=%s) ne (v15=%s))", vm.get("v17"), vm.get("v15")));
  }
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="doCloseImmediately", desc="()V",definedOutput={"this.state.value"},delta={"this.state.value"})
  static void doCloseImmediately90883(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "state"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "gracefulFuture"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v3"), "result"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("org.apache.sshd.common.util.CloseableUtils$AbstractCloseable$State.Closed"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("v3"), "lock"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("this"), "closeFuture"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.popField(vm.get("v1"), "value"));
    vm.put("v15", om.getField(vm.get("this"), "service"));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", null);
    vm.put("v18", om.getField(vm.get("v13"), "result"));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", om.getField(vm.get("v13"), "lock"));
    vm.put("v21", vm.get("v20"));
    if (!em.eq(vm.get("v7"), (vm.get("v14")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v14")));
    if (!em.ne(vm.get("v19"), vm.get("v17"))) e.abort(String.format("!((v19=%s) ne (v17=%s))", vm.get("v19"), vm.get("v17")));
    if (!em.ne(vm.get("v5"), vm.get("v17"))) e.abort(String.format("!((v5=%s) ne (v17=%s))", vm.get("v5"), vm.get("v17")));
    if (!em.ne(vm.get("v5"), vm.get("v17"))) e.abort(String.format("!((v5=%s) ne (v17=%s))", vm.get("v5"), vm.get("v17")));
    if (!em.eq(vm.get("v16"), vm.get("v17"))) e.abort(String.format("!((v16=%s) eq (v17=%s))", vm.get("v16"), vm.get("v17")));
  }
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="doCloseImmediately", desc="()V",revertedInput={"this.closeFuture.result"},definedOutput={"this.closeFuture.result","this.state.value"},delta={"this.state.value"})
  static void doCloseImmediately90867(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "closeFuture"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "state"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popField(vm.get("v5"), "value"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "gracefulFuture"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("v3"), "listeners"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("this"), "service"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("v9"), "result"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.getStatic("org.apache.sshd.common.util.CloseableUtils$AbstractCloseable$State.Closed"));
    vm.put("v17", null);
    vm.put("v18", om.getField(vm.get("v3"), "result"));
    vm.put("v19", om.getField(vm.get("v9"), "lock"));
    vm.put("v20", vm.get("v19"));
    vm.put("v21", om.getField(vm.get("v3"), "lock"));
    vm.put("v22", vm.get("v21"));
    if (!em.eq(vm.get("v7"), (vm.get("v16")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v16")));
    if (!em.eq(vm.get("v1"), (vm.get("v18")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v18")));
    om.revertField(vm.get("v3"), "result");
    vm.put("v23", om.getField(vm.get("v3"), "result"));
    if (!em.ne(vm.get("v15"), vm.get("v17"))) e.abort(String.format("!((v15=%s) ne (v17=%s))", vm.get("v15"), vm.get("v17")));
    if (!em.ne(vm.get("v1"), vm.get("v17"))) e.abort(String.format("!((v1=%s) ne (v17=%s))", vm.get("v1"), vm.get("v17")));
    if (!em.ne(vm.get("v15"), vm.get("v17"))) e.abort(String.format("!((v15=%s) ne (v17=%s))", vm.get("v15"), vm.get("v17")));
    if (!em.eq(vm.get("v13"), vm.get("v17"))) e.abort(String.format("!((v13=%s) eq (v17=%s))", vm.get("v13"), vm.get("v17")));
    if (!em.eq(vm.get("v11"), vm.get("v17"))) e.abort(String.format("!((v11=%s) eq (v17=%s))", vm.get("v11"), vm.get("v17")));
    if (!em.eq(vm.get("v23"), vm.get("v17"))) e.abort(String.format("!((v23=%s) eq (v17=%s))", vm.get("v23"), vm.get("v17")));
  }
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="doCloseImmediately", desc="()V",revertedInput={"this.closeFuture.result"},definedOutput={"this.closeFuture.result","this.state.value"},delta={"this.state.value"})
  static void doCloseImmediately90845(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("org.apache.sshd.common.future.DefaultSshFuture.NULL"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("org.apache.sshd.common.util.CloseableUtils$AbstractCloseable$State.Closed"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "gracefulFuture"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "state"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("this"), "service"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("v7"), "lock"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("this"), "closeFuture"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.getField(vm.get("v7"), "result"));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", null);
    vm.put("v19", om.getField(vm.get("v15"), "lock"));
    vm.put("v20", vm.get("v19"));
    vm.put("v21", om.getField(vm.get("v15"), "result"));
    vm.put("v22", om.getField(vm.get("v15"), "listeners"));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.popField(vm.get("v9"), "value"));
    if (!em.eq(vm.get("v5"), (vm.get("v24")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v24")));
    if (!em.eq(vm.get("v3"), (vm.get("v21")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v21")));
    om.revertField(vm.get("v15"), "result");
    vm.put("v25", om.getField(vm.get("v15"), "result"));
    if (!em.ne(vm.get("v17"), vm.get("v18"))) e.abort(String.format("!((v17=%s) ne (v18=%s))", vm.get("v17"), vm.get("v18")));
    if (!em.eq(vm.get("v11"), vm.get("v18"))) e.abort(String.format("!((v11=%s) eq (v18=%s))", vm.get("v11"), vm.get("v18")));
    if (!em.eq(vm.get("v23"), vm.get("v18"))) e.abort(String.format("!((v23=%s) eq (v18=%s))", vm.get("v23"), vm.get("v18")));
    if (!em.eq(vm.get("v1"), vm.get("v18"))) e.abort(String.format("!((v1=%s) eq (v18=%s))", vm.get("v1"), vm.get("v18")));
    if (!em.ne(vm.get("v17"), vm.get("v18"))) e.abort(String.format("!((v17=%s) ne (v18=%s))", vm.get("v17"), vm.get("v18")));
    if (!em.eq(vm.get("v25"), vm.get("v18"))) e.abort(String.format("!((v25=%s) eq (v18=%s))", vm.get("v25"), vm.get("v18")));
  }
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="handleEof", desc="()V",definedOutput={"this.eof"},delta={"this.eof"})
  static void handleEof37999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "eof"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "log"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "lock"));
    vm.put("v5", vm.get("v4"));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
  }
}
