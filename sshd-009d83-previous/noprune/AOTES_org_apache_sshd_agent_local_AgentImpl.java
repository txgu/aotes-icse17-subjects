import org.javelus.aotes.executor.*;
@Defined({"this.keys.elementData","this.closed","this.keys.elementData[*]","this.keys.modCount","this.keys.elementData[*].second","this.keys","this.keys.size","this.keys.elementData[*].first"})
public class AOTES_org_apache_sshd_agent_local_AgentImpl {
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="removeAllIdentities", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.keys.elementData[*]","this.keys.modCount","this.keys.size"},delta={"this.keys.elementData[*]"})
  @DG(pred={"_init_26399"})
  static void removeAllIdentities70707(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "closed"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", (int)4);
    vm.put("v3", null);
    vm.put("v4", (int)2);
    vm.put("v5", (int)6);
    vm.put("v6", (int)0);
    vm.put("v7", (int)1);
    vm.put("v8", om.getField(vm.get("this"), "keys"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)5);
    vm.put("v11", (int)7);
    vm.put("v12", om.getField(vm.get("v9"), "size"));
    vm.put("v13", (int)3);
    vm.put("v14", (int)8);
    vm.put("v15", om.getField(vm.get("v9"), "elementData"));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.popArrayElement(vm.get("v16"), vm.get("v11")));
    vm.put("v18", om.popArrayElement(vm.get("v16"), vm.get("v10")));
    vm.put("v19", om.popArrayElement(vm.get("v16"), vm.get("v7")));
    vm.put("v20", om.popArrayElement(vm.get("v16"), vm.get("v5")));
    vm.put("v21", om.popArrayElement(vm.get("v16"), vm.get("v2")));
    vm.put("v22", om.popArrayElement(vm.get("v16"), vm.get("v4")));
    vm.put("v23", om.getField(vm.get("v9"), "modCount"));
    vm.put("v24", vm.get("v23"));
    vm.put("v25", om.popArrayElement(vm.get("v16"), vm.get("v6")));
    vm.put("v26", om.popArrayElement(vm.get("v16"), vm.get("v13")));
    if (!em.eq(vm.get("v6"), (vm.get("v12")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v12")));
    vm.put("v27", em.op("isub").eval(vm.get("v24"),vm.get("v7")));
    om.revertField(vm.get("v9"), "modCount", vm.get("v27"));
    om.revertField(vm.get("v9"), "size");
    vm.put("v28", om.getField(vm.get("v9"), "size"));
    if (!em.lt(vm.get("v10"), vm.get("v28"))) e.abort(String.format("!((v10=%s) lt (v28=%s))", vm.get("v10"), vm.get("v28")));
    if (!em.lt(vm.get("v4"), vm.get("v28"))) e.abort(String.format("!((v4=%s) lt (v28=%s))", vm.get("v4"), vm.get("v28")));
    if (!em.lt(vm.get("v11"), vm.get("v28"))) e.abort(String.format("!((v11=%s) lt (v28=%s))", vm.get("v11"), vm.get("v28")));
    if (!em.ge(vm.get("v14"), vm.get("v28"))) e.abort(String.format("!((v14=%s) ge (v28=%s))", vm.get("v14"), vm.get("v28")));
    if (!em.lt(vm.get("v2"), vm.get("v28"))) e.abort(String.format("!((v2=%s) lt (v28=%s))", vm.get("v2"), vm.get("v28")));
    if (!em.lt(vm.get("v6"), vm.get("v28"))) e.abort(String.format("!((v6=%s) lt (v28=%s))", vm.get("v6"), vm.get("v28")));
    if (!em.lt(vm.get("v7"), vm.get("v28"))) e.abort(String.format("!((v7=%s) lt (v28=%s))", vm.get("v7"), vm.get("v28")));
    if (!em.lt(vm.get("v13"), vm.get("v28"))) e.abort(String.format("!((v13=%s) lt (v28=%s))", vm.get("v13"), vm.get("v28")));
    if (!em.eq(vm.get("v1"), vm.get("v6"))) e.abort(String.format("!((v1=%s) eq (v6=%s))", vm.get("v1"), vm.get("v6")));
    if (!em.lt(vm.get("v5"), vm.get("v28"))) e.abort(String.format("!((v5=%s) lt (v28=%s))", vm.get("v5"), vm.get("v28")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="removeAllIdentities", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.keys.elementData[*]","this.keys.modCount","this.keys.size"},delta={"this.keys.elementData[*]"})
  @DG(pred={"_init_26399"})
  static void removeAllIdentities70783(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)3);
    vm.put("v1", null);
    vm.put("v2", (int)2);
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "keys"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v5"), "elementData"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "closed"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("v5"), "size"));
    vm.put("v11", (int)0);
    vm.put("v12", om.popArrayElement(vm.get("v7"), vm.get("v11")));
    vm.put("v13", om.getField(vm.get("v5"), "modCount"));
    vm.put("v14", (int)4);
    vm.put("v15", om.popArrayElement(vm.get("v7"), vm.get("v3")));
    vm.put("v16", om.popArrayElement(vm.get("v7"), vm.get("v0")));
    vm.put("v17", om.popArrayElement(vm.get("v7"), vm.get("v2")));
    if (!em.eq(vm.get("v11"), (vm.get("v10")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v10")));
    vm.put("v18", vm.get("v13"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v3")));
    om.revertField(vm.get("v5"), "modCount", vm.get("v19"));
    om.revertField(vm.get("v5"), "size");
    vm.put("v20", om.getField(vm.get("v5"), "size"));
    if (!em.lt(vm.get("v3"), vm.get("v20"))) e.abort(String.format("!((v3=%s) lt (v20=%s))", vm.get("v3"), vm.get("v20")));
    if (!em.ge(vm.get("v14"), vm.get("v20"))) e.abort(String.format("!((v14=%s) ge (v20=%s))", vm.get("v14"), vm.get("v20")));
    if (!em.eq(vm.get("v9"), vm.get("v11"))) e.abort(String.format("!((v9=%s) eq (v11=%s))", vm.get("v9"), vm.get("v11")));
    if (!em.lt(vm.get("v0"), vm.get("v20"))) e.abort(String.format("!((v0=%s) lt (v20=%s))", vm.get("v0"), vm.get("v20")));
    if (!em.lt(vm.get("v2"), vm.get("v20"))) e.abort(String.format("!((v2=%s) lt (v20=%s))", vm.get("v2"), vm.get("v20")));
    if (!em.lt(vm.get("v11"), vm.get("v20"))) e.abort(String.format("!((v11=%s) lt (v20=%s))", vm.get("v11"), vm.get("v20")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="removeAllIdentities", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.keys.elementData[*]","this.keys.modCount","this.keys.size"},delta={"this.keys.elementData[*]"})
  @DG(pred={"_init_26399"})
  static void removeAllIdentities70765(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)4);
    vm.put("v1", null);
    vm.put("v2", (int)1);
    vm.put("v3", (int)2);
    vm.put("v4", om.getField(vm.get("this"), "closed"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", (int)0);
    vm.put("v7", (int)3);
    vm.put("v8", om.getField(vm.get("this"), "keys"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)5);
    vm.put("v11", om.getField(vm.get("v9"), "size"));
    vm.put("v12", om.getField(vm.get("v9"), "modCount"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("v9"), "elementData"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.popArrayElement(vm.get("v15"), vm.get("v6")));
    vm.put("v17", om.popArrayElement(vm.get("v15"), vm.get("v3")));
    vm.put("v18", om.popArrayElement(vm.get("v15"), vm.get("v0")));
    vm.put("v19", om.popArrayElement(vm.get("v15"), vm.get("v2")));
    vm.put("v20", om.popArrayElement(vm.get("v15"), vm.get("v7")));
    vm.put("v21", em.op("isub").eval(vm.get("v13"),vm.get("v2")));
    om.revertField(vm.get("v9"), "modCount", vm.get("v21"));
    if (!em.eq(vm.get("v6"), (vm.get("v11")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v11")));
    om.revertField(vm.get("v9"), "size");
    vm.put("v22", om.getField(vm.get("v9"), "size"));
    if (!em.lt(vm.get("v7"), vm.get("v22"))) e.abort(String.format("!((v7=%s) lt (v22=%s))", vm.get("v7"), vm.get("v22")));
    if (!em.lt(vm.get("v2"), vm.get("v22"))) e.abort(String.format("!((v2=%s) lt (v22=%s))", vm.get("v2"), vm.get("v22")));
    if (!em.lt(vm.get("v3"), vm.get("v22"))) e.abort(String.format("!((v3=%s) lt (v22=%s))", vm.get("v3"), vm.get("v22")));
    if (!em.ge(vm.get("v10"), vm.get("v22"))) e.abort(String.format("!((v10=%s) ge (v22=%s))", vm.get("v10"), vm.get("v22")));
    if (!em.lt(vm.get("v6"), vm.get("v22"))) e.abort(String.format("!((v6=%s) lt (v22=%s))", vm.get("v6"), vm.get("v22")));
    if (!em.lt(vm.get("v0"), vm.get("v22"))) e.abort(String.format("!((v0=%s) lt (v22=%s))", vm.get("v0"), vm.get("v22")));
    if (!em.eq(vm.get("v5"), vm.get("v6"))) e.abort(String.format("!((v5=%s) eq (v6=%s))", vm.get("v5"), vm.get("v6")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="removeAllIdentities", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.keys.elementData[*]","this.keys.modCount","this.keys.size"},delta={"this.keys.elementData[*]"})
  @DG(pred={"_init_26399"})
  static void removeAllIdentities70796(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "keys"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "closed"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v1"), "size"));
    vm.put("v5", (int)0);
    vm.put("v6", om.getField(vm.get("v1"), "modCount"));
    vm.put("v7", (int)1);
    vm.put("v8", om.getField(vm.get("v1"), "elementData"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.popArrayElement(vm.get("v9"), vm.get("v5")));
    vm.put("v11", null);
    vm.put("v12", vm.get("v6"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v7")));
    om.revertField(vm.get("v1"), "modCount", vm.get("v13"));
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    om.revertField(vm.get("v1"), "size");
    vm.put("v14", om.getField(vm.get("v1"), "size"));
    if (!em.ge(vm.get("v7"), vm.get("v14"))) e.abort(String.format("!((v7=%s) ge (v14=%s))", vm.get("v7"), vm.get("v14")));
    if (!em.lt(vm.get("v5"), vm.get("v14"))) e.abort(String.format("!((v5=%s) lt (v14=%s))", vm.get("v5"), vm.get("v14")));
    if (!em.eq(vm.get("v3"), vm.get("v5"))) e.abort(String.format("!((v3=%s) eq (v5=%s))", vm.get("v3"), vm.get("v5")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="removeAllIdentities", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.keys.elementData[*]","this.keys.modCount","this.keys.size"},delta={"this.keys.elementData[*]"})
  @DG(pred={"_init_26399"})
  static void removeAllIdentities70710(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "closed"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", (int)5);
    vm.put("v4", null);
    vm.put("v5", (int)3);
    vm.put("v6", (int)2);
    vm.put("v7", om.getField(vm.get("this"), "keys"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", (int)4);
    vm.put("v10", om.getField(vm.get("v8"), "modCount"));
    vm.put("v11", (int)1);
    vm.put("v12", (int)6);
    vm.put("v13", (int)7);
    vm.put("v14", om.getField(vm.get("v8"), "size"));
    vm.put("v15", om.getField(vm.get("v8"), "elementData"));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.popArrayElement(vm.get("v16"), vm.get("v11")));
    vm.put("v18", om.popArrayElement(vm.get("v16"), vm.get("v6")));
    vm.put("v19", om.popArrayElement(vm.get("v16"), vm.get("v9")));
    vm.put("v20", om.popArrayElement(vm.get("v16"), vm.get("v0")));
    vm.put("v21", om.popArrayElement(vm.get("v16"), vm.get("v5")));
    vm.put("v22", om.popArrayElement(vm.get("v16"), vm.get("v12")));
    vm.put("v23", om.popArrayElement(vm.get("v16"), vm.get("v3")));
    if (!em.eq(vm.get("v0"), (vm.get("v14")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v14")));
    vm.put("v24", vm.get("v10"));
    vm.put("v25", em.op("isub").eval(vm.get("v24"),vm.get("v11")));
    om.revertField(vm.get("v8"), "modCount", vm.get("v25"));
    om.revertField(vm.get("v8"), "size");
    vm.put("v26", om.getField(vm.get("v8"), "size"));
    if (!em.lt(vm.get("v11"), vm.get("v26"))) e.abort(String.format("!((v11=%s) lt (v26=%s))", vm.get("v11"), vm.get("v26")));
    if (!em.lt(vm.get("v5"), vm.get("v26"))) e.abort(String.format("!((v5=%s) lt (v26=%s))", vm.get("v5"), vm.get("v26")));
    if (!em.lt(vm.get("v6"), vm.get("v26"))) e.abort(String.format("!((v6=%s) lt (v26=%s))", vm.get("v6"), vm.get("v26")));
    if (!em.eq(vm.get("v2"), vm.get("v0"))) e.abort(String.format("!((v2=%s) eq (v0=%s))", vm.get("v2"), vm.get("v0")));
    if (!em.lt(vm.get("v12"), vm.get("v26"))) e.abort(String.format("!((v12=%s) lt (v26=%s))", vm.get("v12"), vm.get("v26")));
    if (!em.lt(vm.get("v0"), vm.get("v26"))) e.abort(String.format("!((v0=%s) lt (v26=%s))", vm.get("v0"), vm.get("v26")));
    if (!em.lt(vm.get("v3"), vm.get("v26"))) e.abort(String.format("!((v3=%s) lt (v26=%s))", vm.get("v3"), vm.get("v26")));
    if (!em.ge(vm.get("v13"), vm.get("v26"))) e.abort(String.format("!((v13=%s) ge (v26=%s))", vm.get("v13"), vm.get("v26")));
    if (!em.lt(vm.get("v9"), vm.get("v26"))) e.abort(String.format("!((v9=%s) lt (v26=%s))", vm.get("v9"), vm.get("v26")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="removeAllIdentities", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.keys.elementData[*]","this.keys.modCount","this.keys.size"},delta={"this.keys.elementData[*]"})
  @DG(pred={"_init_26399"})
  static void removeAllIdentities70772(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "closed"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "keys"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", null);
    vm.put("v6", om.getField(vm.get("v4"), "modCount"));
    vm.put("v7", (int)1);
    vm.put("v8", (int)2);
    vm.put("v9", (int)3);
    vm.put("v10", om.getField(vm.get("v4"), "size"));
    vm.put("v11", om.getField(vm.get("v4"), "elementData"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.popArrayElement(vm.get("v12"), vm.get("v0")));
    vm.put("v14", om.popArrayElement(vm.get("v12"), vm.get("v7")));
    vm.put("v15", om.popArrayElement(vm.get("v12"), vm.get("v8")));
    if (!em.eq(vm.get("v0"), (vm.get("v10")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v10")));
    vm.put("v16", vm.get("v6"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v7")));
    om.revertField(vm.get("v4"), "modCount", vm.get("v17"));
    om.revertField(vm.get("v4"), "size");
    vm.put("v18", om.getField(vm.get("v4"), "size"));
    if (!em.lt(vm.get("v7"), vm.get("v18"))) e.abort(String.format("!((v7=%s) lt (v18=%s))", vm.get("v7"), vm.get("v18")));
    if (!em.eq(vm.get("v2"), vm.get("v0"))) e.abort(String.format("!((v2=%s) eq (v0=%s))", vm.get("v2"), vm.get("v0")));
    if (!em.ge(vm.get("v9"), vm.get("v18"))) e.abort(String.format("!((v9=%s) ge (v18=%s))", vm.get("v9"), vm.get("v18")));
    if (!em.lt(vm.get("v0"), vm.get("v18"))) e.abort(String.format("!((v0=%s) lt (v18=%s))", vm.get("v0"), vm.get("v18")));
    if (!em.lt(vm.get("v8"), vm.get("v18"))) e.abort(String.format("!((v8=%s) lt (v18=%s))", vm.get("v8"), vm.get("v18")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="removeAllIdentities", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.keys.elementData[*]","this.keys.modCount","this.keys.size"},delta={"this.keys.elementData[*]"})
  @DG(pred={"_init_26399"})
  static void removeAllIdentities70795(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", om.getField(vm.get("this"), "keys"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "closed"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("v2"), "size"));
    vm.put("v6", (int)0);
    vm.put("v7", om.getField(vm.get("v2"), "elementData"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.popArrayElement(vm.get("v8"), vm.get("v0")));
    vm.put("v10", null);
    vm.put("v11", om.popArrayElement(vm.get("v8"), vm.get("v6")));
    vm.put("v12", (int)2);
    vm.put("v13", om.getField(vm.get("v2"), "modCount"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    om.revertField(vm.get("v2"), "modCount", vm.get("v15"));
    if (!em.eq(vm.get("v6"), (vm.get("v5")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v5")));
    om.revertField(vm.get("v2"), "size");
    vm.put("v16", om.getField(vm.get("v2"), "size"));
    if (!em.lt(vm.get("v0"), vm.get("v16"))) e.abort(String.format("!((v0=%s) lt (v16=%s))", vm.get("v0"), vm.get("v16")));
    if (!em.eq(vm.get("v4"), vm.get("v6"))) e.abort(String.format("!((v4=%s) eq (v6=%s))", vm.get("v4"), vm.get("v6")));
    if (!em.ge(vm.get("v12"), vm.get("v16"))) e.abort(String.format("!((v12=%s) ge (v16=%s))", vm.get("v12"), vm.get("v16")));
    if (!em.lt(vm.get("v6"), vm.get("v16"))) e.abort(String.format("!((v6=%s) lt (v16=%s))", vm.get("v6"), vm.get("v16")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="removeAllIdentities", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.keys.modCount","this.keys.size"})
  @DG(pred={"_init_26399"})
  static void removeAllIdentities70799(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "keys"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "closed"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v3"), "modCount"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("v3"), "size"));
    vm.put("v9", em.op("isub").eval(vm.get("v7"),vm.get("v0")));
    om.revertField(vm.get("v3"), "modCount", vm.get("v9"));
    if (!em.eq(vm.get("v1"), (vm.get("v8")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v8")));
    om.revertField(vm.get("v3"), "size");
    vm.put("v10", om.getField(vm.get("v3"), "size"));
    if (!em.eq(vm.get("v5"), vm.get("v1"))) e.abort(String.format("!((v5=%s) eq (v1=%s))", vm.get("v5"), vm.get("v1")));
    if (!em.ge(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) ge (v10=%s))", vm.get("v1"), vm.get("v10")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="addIdentity", desc="(Ljava/security/KeyPair;Ljava/lang/String;)V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.keys.elementData[*]","this.keys.modCount","this.keys.elementData[*].second","this.keys.size","this.keys.elementData[*].first"},delta={"this.keys.elementData[*]","this.keys.elementData[*].second","this.keys.elementData[*].first"})
  @DG(pred={"_init_26399"})
  static void addIdentity52099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "closed"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "keys"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v5"), "modCount"));
    vm.put("v7", (int)1);
    vm.put("v8", om.getField(vm.get("v5"), "size"));
    vm.put("v9", om.getField(vm.get("v5"), "elementData"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)0);
    vm.put("v12", om.getArrayLength(vm.get("v10")));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", vm.get("v6"));
    vm.put("v15", vm.get("v8"));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v7")));
    om.revertField(vm.get("v5"), "size", vm.get("v16"));
    vm.put("v17", om.popArrayElement(vm.get("v10"), vm.get("v16")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", em.op("iadd").eval(vm.get("v16"),vm.get("v7")));
    vm.put("v20", em.op("isub").eval(vm.get("v19"),vm.get("v13")));
    vm.put("v21", om.popField(vm.get("v18"), "first"));
    vm.put("param0", vm.get("v21"));
    vm.put("v22", om.popField(vm.get("v18"), "second"));
    vm.put("param1", vm.get("v22"));
    vm.put("v23", em.op("isub").eval(vm.get("v14"),vm.get("v7")));
    om.revertField(vm.get("v5"), "modCount", vm.get("v23"));
    if (!em.eq(vm.get("v1"), vm.get("v11"))) e.abort(String.format("!((v1=%s) eq (v11=%s))", vm.get("v1"), vm.get("v11")));
    if (!em.ne(vm.get("v10"), vm.get("v3"))) e.abort(String.format("!((v10=%s) ne (v3=%s))", vm.get("v10"), vm.get("v3")));
    if (!em.le(vm.get("v20"), vm.get("v11"))) e.abort(String.format("!((v20=%s) le (v11=%s))", vm.get("v20"), vm.get("v11")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="addIdentity", desc="(Ljava/security/KeyPair;Ljava/lang/String;)V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.keys.elementData[*]","this.keys.modCount","this.keys.elementData[*].second","this.keys.size","this.keys.elementData[*].first"},delta={"this.keys.elementData[*]","this.keys.elementData[*].second","this.keys.elementData[*].first"})
  @DG(pred={"_init_26399"})
  static void addIdentity52092(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "keys"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v1"), "modCount"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getField(vm.get("v1"), "size"));
    vm.put("v7", om.getField(vm.get("this"), "closed"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("v1"), "elementData"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getArrayLength(vm.get("v10")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", (int)10);
    vm.put("v14", (int)0);
    vm.put("v15", em.op("isub").eval(vm.get("v13"),vm.get("v12")));
    vm.put("v16", vm.get("v6"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v5")));
    om.revertField(vm.get("v1"), "size", vm.get("v17"));
    vm.put("v18", em.op("iadd").eval(vm.get("v17"),vm.get("v5")));
    vm.put("v19", om.popArrayElement(vm.get("v10"), vm.get("v17")));
    vm.put("v20", vm.get("v19"));
    vm.put("v21", om.popField(vm.get("v20"), "first"));
    vm.put("param0", vm.get("v21"));
    vm.put("v22", om.popField(vm.get("v20"), "second"));
    vm.put("param1", vm.get("v22"));
    vm.put("v23", vm.get("v4"));
    vm.put("v24", em.op("isub").eval(vm.get("v23"),vm.get("v5")));
    om.revertField(vm.get("v1"), "modCount", vm.get("v24"));
    if (!em.ge(vm.get("v13"), vm.get("v18"))) e.abort(String.format("!((v13=%s) ge (v18=%s))", vm.get("v13"), vm.get("v18")));
    if (!em.le(vm.get("v15"), vm.get("v14"))) e.abort(String.format("!((v15=%s) le (v14=%s))", vm.get("v15"), vm.get("v14")));
    if (!em.eq(vm.get("v10"), vm.get("v3"))) e.abort(String.format("!((v10=%s) eq (v3=%s))", vm.get("v10"), vm.get("v3")));
    if (!em.eq(vm.get("v8"), vm.get("v14"))) e.abort(String.format("!((v8=%s) eq (v14=%s))", vm.get("v8"), vm.get("v14")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="addIdentity", desc="(Ljava/security/KeyPair;Ljava/lang/String;)V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.keys.elementData[*]","this.keys.elementData[*].second","this.keys.modCount","this.keys.size","this.keys.elementData[*].first"},delta={"this.keys.elementData[*]","this.keys.elementData[*].second","this.keys.elementData[*].first"})
  @DG(pred={"_init_26399"})
  static void addIdentity52098(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", om.getField(vm.get("this"), "closed"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "keys"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)0);
    vm.put("v8", (int)10);
    vm.put("v9", om.getField(vm.get("v6"), "modCount"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getField(vm.get("v6"), "elementData"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getArrayLength(vm.get("v12")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.getField(vm.get("v6"), "size"));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    om.revertField(vm.get("v6"), "size", vm.get("v17"));
    vm.put("v18", em.op("iadd").eval(vm.get("v17"),vm.get("v0")));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v14")));
    vm.put("v20", om.popArrayElement(vm.get("v12"), vm.get("v17")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.popField(vm.get("v21"), "first"));
    vm.put("param0", vm.get("v22"));
    vm.put("v23", om.popField(vm.get("v21"), "second"));
    vm.put("param1", vm.get("v23"));
    vm.put("v24", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    om.revertField(vm.get("v6"), "modCount", vm.get("v24"));
    if (!em.le(vm.get("v19"), vm.get("v7"))) e.abort(String.format("!((v19=%s) le (v7=%s))", vm.get("v19"), vm.get("v7")));
    if (!em.lt(vm.get("v8"), vm.get("v18"))) e.abort(String.format("!((v8=%s) lt (v18=%s))", vm.get("v8"), vm.get("v18")));
    if (!em.eq(vm.get("v12"), vm.get("v4"))) e.abort(String.format("!((v12=%s) eq (v4=%s))", vm.get("v12"), vm.get("v4")));
    if (!em.eq(vm.get("v2"), vm.get("v7"))) e.abort(String.format("!((v2=%s) eq (v7=%s))", vm.get("v2"), vm.get("v7")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="<init>", desc="()V",definedOutput={"this.keys.elementData","this.keys.modCount","this.keys"},delta={"this.keys.elementData","this.keys.modCount","this.keys"})
  @DG(succ={"addIdentity52098","removeAllIdentities70783","addIdentity52092","removeAllIdentities70765","close29199","close29196","removeAllIdentities70710","removeAllIdentities70796","removeAllIdentities70772","close29191","removeAllIdentities70795","close29198","close29137","removeAllIdentities70799","close29130","close29110","addIdentity52099","removeAllIdentities70707","close29194"})
  static void _init_26399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v1", vm.get("v0"));
    vm.put("this", om.getTarget());
    vm.put("v2", om.popField(vm.get("this"), "keys"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.popField(vm.get("v3"), "modCount"));
    vm.put("v5", (int)0);
    vm.put("v6", om.popField(vm.get("v3"), "elementData"));
    if (!em.eq(vm.get("v1"), (vm.get("v6")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v6")));
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="close", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.closed","this.keys.elementData[*]","this.keys.modCount","this.keys.size"},delta={"this.closed","this.keys.elementData[*]"})
  @DG(pred={"_init_26399"})
  static void close29137(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", null);
    vm.put("v2", (int)1);
    vm.put("v3", om.popField(vm.get("this"), "closed"));
    vm.put("v4", (int)6);
    vm.put("v5", om.getField(vm.get("this"), "keys"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)4);
    vm.put("v8", (int)5);
    vm.put("v9", (int)2);
    vm.put("v10", om.getField(vm.get("v6"), "elementData"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", (int)3);
    vm.put("v13", om.getField(vm.get("v6"), "size"));
    vm.put("v14", (int)7);
    vm.put("v15", (int)8);
    vm.put("v16", om.popArrayElement(vm.get("v11"), vm.get("v12")));
    vm.put("v17", om.popArrayElement(vm.get("v11"), vm.get("v2")));
    vm.put("v18", om.getField(vm.get("v6"), "modCount"));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", om.popArrayElement(vm.get("v11"), vm.get("v0")));
    vm.put("v21", om.popArrayElement(vm.get("v11"), vm.get("v8")));
    vm.put("v22", om.popArrayElement(vm.get("v11"), vm.get("v14")));
    vm.put("v23", om.popArrayElement(vm.get("v11"), vm.get("v7")));
    vm.put("v24", om.popArrayElement(vm.get("v11"), vm.get("v4")));
    vm.put("v25", om.popArrayElement(vm.get("v11"), vm.get("v9")));
    if (!em.eq(vm.get("v2"), (vm.get("v3")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v3")));
    if (!em.eq(vm.get("v0"), (vm.get("v13")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v13")));
    vm.put("v26", em.op("isub").eval(vm.get("v19"),vm.get("v2")));
    om.revertField(vm.get("v6"), "modCount", vm.get("v26"));
    om.revertField(vm.get("v6"), "size");
    vm.put("v27", om.getField(vm.get("v6"), "size"));
    if (!em.lt(vm.get("v9"), vm.get("v27"))) e.abort(String.format("!((v9=%s) lt (v27=%s))", vm.get("v9"), vm.get("v27")));
    if (!em.lt(vm.get("v2"), vm.get("v27"))) e.abort(String.format("!((v2=%s) lt (v27=%s))", vm.get("v2"), vm.get("v27")));
    if (!em.lt(vm.get("v0"), vm.get("v27"))) e.abort(String.format("!((v0=%s) lt (v27=%s))", vm.get("v0"), vm.get("v27")));
    if (!em.lt(vm.get("v12"), vm.get("v27"))) e.abort(String.format("!((v12=%s) lt (v27=%s))", vm.get("v12"), vm.get("v27")));
    if (!em.ge(vm.get("v15"), vm.get("v27"))) e.abort(String.format("!((v15=%s) ge (v27=%s))", vm.get("v15"), vm.get("v27")));
    if (!em.lt(vm.get("v4"), vm.get("v27"))) e.abort(String.format("!((v4=%s) lt (v27=%s))", vm.get("v4"), vm.get("v27")));
    if (!em.lt(vm.get("v8"), vm.get("v27"))) e.abort(String.format("!((v8=%s) lt (v27=%s))", vm.get("v8"), vm.get("v27")));
    if (!em.lt(vm.get("v14"), vm.get("v27"))) e.abort(String.format("!((v14=%s) lt (v27=%s))", vm.get("v14"), vm.get("v27")));
    if (!em.lt(vm.get("v7"), vm.get("v27"))) e.abort(String.format("!((v7=%s) lt (v27=%s))", vm.get("v7"), vm.get("v27")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="close", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.closed","this.keys.elementData[*]","this.keys.modCount","this.keys.size"},delta={"this.closed","this.keys.elementData[*]"})
  @DG(pred={"_init_26399"})
  static void close29199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", null);
    vm.put("v2", (int)1);
    vm.put("v3", om.popField(vm.get("this"), "closed"));
    vm.put("v4", om.getField(vm.get("this"), "keys"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", (int)2);
    vm.put("v7", om.getField(vm.get("v5"), "elementData"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", (int)3);
    vm.put("v10", om.getField(vm.get("v5"), "modCount"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.popArrayElement(vm.get("v8"), vm.get("v0")));
    vm.put("v13", om.popArrayElement(vm.get("v8"), vm.get("v2")));
    vm.put("v14", om.getField(vm.get("v5"), "size"));
    vm.put("v15", om.popArrayElement(vm.get("v8"), vm.get("v6")));
    if (!em.eq(vm.get("v2"), (vm.get("v3")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v3")));
    vm.put("v16", em.op("isub").eval(vm.get("v11"),vm.get("v2")));
    om.revertField(vm.get("v5"), "modCount", vm.get("v16"));
    if (!em.eq(vm.get("v0"), (vm.get("v14")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v14")));
    om.revertField(vm.get("v5"), "size");
    vm.put("v17", om.getField(vm.get("v5"), "size"));
    if (!em.lt(vm.get("v2"), vm.get("v17"))) e.abort(String.format("!((v2=%s) lt (v17=%s))", vm.get("v2"), vm.get("v17")));
    if (!em.lt(vm.get("v0"), vm.get("v17"))) e.abort(String.format("!((v0=%s) lt (v17=%s))", vm.get("v0"), vm.get("v17")));
    if (!em.ge(vm.get("v9"), vm.get("v17"))) e.abort(String.format("!((v9=%s) ge (v17=%s))", vm.get("v9"), vm.get("v17")));
    if (!em.lt(vm.get("v6"), vm.get("v17"))) e.abort(String.format("!((v6=%s) lt (v17=%s))", vm.get("v6"), vm.get("v17")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="close", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.closed","this.keys.elementData[*]","this.keys.modCount","this.keys.size"},delta={"this.closed","this.keys.elementData[*]"})
  @DG(pred={"_init_26399"})
  static void close29196(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)5);
    vm.put("v1", null);
    vm.put("v2", om.getField(vm.get("this"), "keys"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", (int)0);
    vm.put("v5", om.getField(vm.get("v3"), "size"));
    vm.put("v6", (int)1);
    vm.put("v7", (int)4);
    vm.put("v8", om.getField(vm.get("v3"), "elementData"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)3);
    vm.put("v11", (int)2);
    vm.put("v12", om.getField(vm.get("v3"), "modCount"));
    vm.put("v13", om.popField(vm.get("this"), "closed"));
    vm.put("v14", (int)6);
    vm.put("v15", om.popArrayElement(vm.get("v9"), vm.get("v11")));
    vm.put("v16", om.popArrayElement(vm.get("v9"), vm.get("v0")));
    vm.put("v17", om.popArrayElement(vm.get("v9"), vm.get("v7")));
    vm.put("v18", om.popArrayElement(vm.get("v9"), vm.get("v6")));
    vm.put("v19", om.popArrayElement(vm.get("v9"), vm.get("v4")));
    vm.put("v20", om.popArrayElement(vm.get("v9"), vm.get("v10")));
    if (!em.eq(vm.get("v4"), (vm.get("v5")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v5")));
    vm.put("v21", vm.get("v12"));
    vm.put("v22", em.op("isub").eval(vm.get("v21"),vm.get("v6")));
    om.revertField(vm.get("v3"), "modCount", vm.get("v22"));
    if (!em.eq(vm.get("v6"), (vm.get("v13")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v13")));
    om.revertField(vm.get("v3"), "size");
    vm.put("v23", om.getField(vm.get("v3"), "size"));
    if (!em.lt(vm.get("v10"), vm.get("v23"))) e.abort(String.format("!((v10=%s) lt (v23=%s))", vm.get("v10"), vm.get("v23")));
    if (!em.lt(vm.get("v11"), vm.get("v23"))) e.abort(String.format("!((v11=%s) lt (v23=%s))", vm.get("v11"), vm.get("v23")));
    if (!em.lt(vm.get("v4"), vm.get("v23"))) e.abort(String.format("!((v4=%s) lt (v23=%s))", vm.get("v4"), vm.get("v23")));
    if (!em.lt(vm.get("v6"), vm.get("v23"))) e.abort(String.format("!((v6=%s) lt (v23=%s))", vm.get("v6"), vm.get("v23")));
    if (!em.lt(vm.get("v0"), vm.get("v23"))) e.abort(String.format("!((v0=%s) lt (v23=%s))", vm.get("v0"), vm.get("v23")));
    if (!em.ge(vm.get("v14"), vm.get("v23"))) e.abort(String.format("!((v14=%s) ge (v23=%s))", vm.get("v14"), vm.get("v23")));
    if (!em.lt(vm.get("v7"), vm.get("v23"))) e.abort(String.format("!((v7=%s) lt (v23=%s))", vm.get("v7"), vm.get("v23")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="close", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.closed","this.keys.elementData[*]","this.keys.modCount","this.keys.size"},delta={"this.closed","this.keys.elementData[*]"})
  @DG(pred={"_init_26399"})
  static void close29110(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "closed"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "keys"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v3"), "size"));
    vm.put("v5", (int)0);
    vm.put("v6", (int)5);
    vm.put("v7", null);
    vm.put("v8", om.getField(vm.get("v3"), "elementData"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", (int)2);
    vm.put("v11", (int)3);
    vm.put("v12", om.popArrayElement(vm.get("v9"), vm.get("v1")));
    vm.put("v13", (int)6);
    vm.put("v14", (int)4);
    vm.put("v15", om.popArrayElement(vm.get("v9"), vm.get("v5")));
    vm.put("v16", (int)7);
    vm.put("v17", om.popArrayElement(vm.get("v9"), vm.get("v11")));
    vm.put("v18", om.popArrayElement(vm.get("v9"), vm.get("v14")));
    vm.put("v19", om.popArrayElement(vm.get("v9"), vm.get("v10")));
    vm.put("v20", om.popArrayElement(vm.get("v9"), vm.get("v13")));
    vm.put("v21", om.getField(vm.get("v3"), "modCount"));
    vm.put("v22", vm.get("v21"));
    vm.put("v23", om.popArrayElement(vm.get("v9"), vm.get("v6")));
    vm.put("v24", em.op("isub").eval(vm.get("v22"),vm.get("v1")));
    om.revertField(vm.get("v3"), "modCount", vm.get("v24"));
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    om.revertField(vm.get("v3"), "size");
    vm.put("v25", om.getField(vm.get("v3"), "size"));
    if (!em.lt(vm.get("v6"), vm.get("v25"))) e.abort(String.format("!((v6=%s) lt (v25=%s))", vm.get("v6"), vm.get("v25")));
    if (!em.lt(vm.get("v14"), vm.get("v25"))) e.abort(String.format("!((v14=%s) lt (v25=%s))", vm.get("v14"), vm.get("v25")));
    if (!em.lt(vm.get("v10"), vm.get("v25"))) e.abort(String.format("!((v10=%s) lt (v25=%s))", vm.get("v10"), vm.get("v25")));
    if (!em.lt(vm.get("v11"), vm.get("v25"))) e.abort(String.format("!((v11=%s) lt (v25=%s))", vm.get("v11"), vm.get("v25")));
    if (!em.lt(vm.get("v1"), vm.get("v25"))) e.abort(String.format("!((v1=%s) lt (v25=%s))", vm.get("v1"), vm.get("v25")));
    if (!em.lt(vm.get("v13"), vm.get("v25"))) e.abort(String.format("!((v13=%s) lt (v25=%s))", vm.get("v13"), vm.get("v25")));
    if (!em.ge(vm.get("v16"), vm.get("v25"))) e.abort(String.format("!((v16=%s) ge (v25=%s))", vm.get("v16"), vm.get("v25")));
    if (!em.lt(vm.get("v5"), vm.get("v25"))) e.abort(String.format("!((v5=%s) lt (v25=%s))", vm.get("v5"), vm.get("v25")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="close", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.closed","this.keys.modCount","this.keys.size"},delta={"this.closed"})
  @DG(pred={"_init_26399"})
  static void close29198(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "closed"));
    vm.put("v1", (int)1);
    vm.put("v2", (int)0);
    vm.put("v3", om.getField(vm.get("this"), "keys"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("v4"), "modCount"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("v4"), "size"));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    vm.put("v8", em.op("isub").eval(vm.get("v6"),vm.get("v1")));
    om.revertField(vm.get("v4"), "modCount", vm.get("v8"));
    if (!em.eq(vm.get("v2"), (vm.get("v7")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v7")));
    om.revertField(vm.get("v4"), "size");
    vm.put("v9", om.getField(vm.get("v4"), "size"));
    if (!em.ge(vm.get("v2"), vm.get("v9"))) e.abort(String.format("!((v2=%s) ge (v9=%s))", vm.get("v2"), vm.get("v9")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="close", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.closed","this.keys.elementData[*]","this.keys.modCount","this.keys.size"},delta={"this.closed","this.keys.elementData[*]"})
  @DG(pred={"_init_26399"})
  static void close29194(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "keys"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", (int)0);
    vm.put("v3", null);
    vm.put("v4", om.popField(vm.get("this"), "closed"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getField(vm.get("v1"), "size"));
    vm.put("v7", om.getField(vm.get("v1"), "elementData"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("v1"), "modCount"));
    vm.put("v10", om.popArrayElement(vm.get("v8"), vm.get("v2")));
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    vm.put("v11", vm.get("v9"));
    vm.put("v12", em.op("isub").eval(vm.get("v11"),vm.get("v5")));
    om.revertField(vm.get("v1"), "modCount", vm.get("v12"));
    if (!em.eq(vm.get("v2"), (vm.get("v6")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v6")));
    om.revertField(vm.get("v1"), "size");
    vm.put("v13", om.getField(vm.get("v1"), "size"));
    if (!em.ge(vm.get("v5"), vm.get("v13"))) e.abort(String.format("!((v5=%s) ge (v13=%s))", vm.get("v5"), vm.get("v13")));
    if (!em.lt(vm.get("v2"), vm.get("v13"))) e.abort(String.format("!((v2=%s) lt (v13=%s))", vm.get("v2"), vm.get("v13")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="close", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.closed","this.keys.elementData[*]","this.keys.modCount","this.keys.size"},delta={"this.closed","this.keys.elementData[*]"})
  @DG(pred={"_init_26399"})
  static void close29191(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "keys"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.popField(vm.get("this"), "closed"));
    vm.put("v4", (int)1);
    vm.put("v5", null);
    vm.put("v6", om.getField(vm.get("v2"), "modCount"));
    vm.put("v7", om.getField(vm.get("v2"), "elementData"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", (int)2);
    vm.put("v10", om.getField(vm.get("v2"), "size"));
    vm.put("v11", om.popArrayElement(vm.get("v8"), vm.get("v0")));
    vm.put("v12", om.popArrayElement(vm.get("v8"), vm.get("v4")));
    if (!em.eq(vm.get("v4"), (vm.get("v3")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v3")));
    if (!em.eq(vm.get("v0"), (vm.get("v10")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v10")));
    vm.put("v13", vm.get("v6"));
    vm.put("v14", em.op("isub").eval(vm.get("v13"),vm.get("v4")));
    om.revertField(vm.get("v2"), "modCount", vm.get("v14"));
    om.revertField(vm.get("v2"), "size");
    vm.put("v15", om.getField(vm.get("v2"), "size"));
    if (!em.ge(vm.get("v9"), vm.get("v15"))) e.abort(String.format("!((v9=%s) ge (v15=%s))", vm.get("v9"), vm.get("v15")));
    if (!em.lt(vm.get("v4"), vm.get("v15"))) e.abort(String.format("!((v4=%s) lt (v15=%s))", vm.get("v4"), vm.get("v15")));
    if (!em.lt(vm.get("v0"), vm.get("v15"))) e.abort(String.format("!((v0=%s) lt (v15=%s))", vm.get("v0"), vm.get("v15")));
  }
  @IM(clazz="org.apache.sshd.agent.local.AgentImpl", name="close", desc="()V",revertedInput={"this.keys.modCount","this.keys.size"},definedOutput={"this.closed","this.keys.elementData[*]","this.keys.modCount","this.keys.size"},delta={"this.closed","this.keys.elementData[*]"})
  @DG(pred={"_init_26399"})
  static void close29130(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", null);
    vm.put("v2", (int)1);
    vm.put("v3", (int)3);
    vm.put("v4", (int)2);
    vm.put("v5", om.popField(vm.get("this"), "closed"));
    vm.put("v6", om.getField(vm.get("this"), "keys"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)4);
    vm.put("v9", om.getField(vm.get("v7"), "size"));
    vm.put("v10", om.getField(vm.get("v7"), "modCount"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("v7"), "elementData"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.popArrayElement(vm.get("v13"), vm.get("v0")));
    vm.put("v15", om.popArrayElement(vm.get("v13"), vm.get("v3")));
    vm.put("v16", om.popArrayElement(vm.get("v13"), vm.get("v4")));
    vm.put("v17", om.popArrayElement(vm.get("v13"), vm.get("v2")));
    if (!em.eq(vm.get("v0"), (vm.get("v9")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v9")));
    if (!em.eq(vm.get("v2"), (vm.get("v5")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v5")));
    vm.put("v18", em.op("isub").eval(vm.get("v11"),vm.get("v2")));
    om.revertField(vm.get("v7"), "modCount", vm.get("v18"));
    om.revertField(vm.get("v7"), "size");
    vm.put("v19", om.getField(vm.get("v7"), "size"));
    if (!em.lt(vm.get("v3"), vm.get("v19"))) e.abort(String.format("!((v3=%s) lt (v19=%s))", vm.get("v3"), vm.get("v19")));
    if (!em.lt(vm.get("v4"), vm.get("v19"))) e.abort(String.format("!((v4=%s) lt (v19=%s))", vm.get("v4"), vm.get("v19")));
    if (!em.lt(vm.get("v2"), vm.get("v19"))) e.abort(String.format("!((v2=%s) lt (v19=%s))", vm.get("v2"), vm.get("v19")));
    if (!em.lt(vm.get("v0"), vm.get("v19"))) e.abort(String.format("!((v0=%s) lt (v19=%s))", vm.get("v0"), vm.get("v19")));
    if (!em.ge(vm.get("v8"), vm.get("v19"))) e.abort(String.format("!((v8=%s) ge (v19=%s))", vm.get("v8"), vm.get("v19")));
  }
}
