import org.javelus.aotes.executor.*;
@Defined({"org.apache.sshd.agent.unix.ChannelAgentForwarding$ChannelAgentForwardingFactory.INSTANCE"})
public class AOTES_org_apache_sshd_agent_unix_ChannelAgentForwarding$ChannelAgentForwardingFactory {
  @IM(clazz="org.apache.sshd.agent.unix.ChannelAgentForwarding$ChannelAgentForwardingFactory", name="<clinit>", desc="()V",definedOutput={"org.apache.sshd.agent.unix.ChannelAgentForwarding$ChannelAgentForwardingFactory.INSTANCE"},delta={"org.apache.sshd.agent.unix.ChannelAgentForwarding$ChannelAgentForwardingFactory.INSTANCE"})
  static void _clinit_54999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", om.popStatic("org.apache.sshd.agent.unix.ChannelAgentForwarding$ChannelAgentForwardingFactory.INSTANCE"));
    vm.put("v1", vm.get("v0"));
  }
}
