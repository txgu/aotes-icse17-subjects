import org.javelus.aotes.executor.*;
@Defined({"this.buffer.data","this.buffer","this.buffer.rpos","this.agent","this.buffer.workBuf","this.log","this.buffer.wpos"})
public class AOTES_org_apache_sshd_agent_common_AbstractAgentClient {
  @IM(clazz="org.apache.sshd.agent.common.AbstractAgentClient", name="<init>", desc="(Lorg/apache/sshd/agent/SshAgent;)V",definedOutput={"this.buffer.data","this.buffer","this.buffer.rpos","this.buffer.workBuf","this.agent","this.log","this.buffer.wpos"},delta={"this.buffer.data","this.buffer","this.buffer.rpos","this.buffer.workBuf","this.agent","this.log","this.buffer.wpos"})
  static void _init_7299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", (int)0);
    vm.put("this", om.getTarget());
    vm.put("v1", om.popField(vm.get("this"), "buffer"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.popField(vm.get("this"), "agent"));
    vm.put("v4", om.popField(vm.get("v2"), "wpos"));
    vm.put("v5", (int)8);
    vm.put("v6", om.popField(vm.get("this"), "log"));
    vm.put("v7", (int)256);
    vm.put("v8", om.popField(vm.get("v2"), "rpos"));
    vm.put("v9", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("this")));
    vm.put("v10", em.op("org.slf4j.LoggerFactory.getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;").eval(vm.get("v9")));
    vm.put("v11", om.popField(vm.get("v2"), "data"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getArrayLength(vm.get("v12")));
    vm.put("v14", om.popField(vm.get("v2"), "workBuf"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.getArrayLength(vm.get("v15")));
    if (!em.eq(vm.get("v10"), (vm.get("v6")))) e.abort("Inconsistent value for \"v10\": " + vm.get("v10") + " ne " + (vm.get("v6")));
    vm.put("param0", vm.get("v3"));
    if (!em.eq(vm.get("v7"), (vm.get("v13")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v13")));
    if (!em.eq(vm.get("v0"), (vm.get("v8")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v8")));
    if (!em.eq(vm.get("v0"), (vm.get("v4")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v4")));
    if (!em.eq(vm.get("v5"), (vm.get("v16")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v16")));
  }
}
