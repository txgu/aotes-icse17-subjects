import org.javelus.aotes.executor.*;
@Defined({"this.type"})
public class AOTES_org_apache_sshd_common_forward_TcpipServerChannel$TcpipFactory {
  @IM(clazz="org.apache.sshd.common.forward.TcpipServerChannel$TcpipFactory", name="<init>", desc="(Lorg/apache/sshd/common/forward/TcpipServerChannel$Type;)V",definedOutput={"this.type"},delta={"this.type"})
  static void _init_74399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "type"));
    vm.put("param0", vm.get("v0"));
  }
}
