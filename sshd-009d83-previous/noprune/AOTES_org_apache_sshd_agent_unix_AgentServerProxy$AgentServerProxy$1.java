import org.javelus.aotes.executor.*;
@Defined({"this.blocker","this.uncaughtExceptionHandler","this.contextClassLoader","this.name","this.this$0.innerFinished","this.daemon"})
public class AOTES_org_apache_sshd_agent_unix_AgentServerProxy$AgentServerProxy$1 {
  @IM(clazz="java.lang.Thread", name="setUncaughtExceptionHandler", desc="(Ljava/lang/Thread$UncaughtExceptionHandler;)V",definedOutput={"this.uncaughtExceptionHandler"},delta={"this.uncaughtExceptionHandler"})
  static void setUncaughtExceptionHandler63397(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", om.getStatic("java.lang.System.security"));
    vm.put("v1", vm.get("v0"));
    vm.put("this", om.getTarget());
    vm.put("v2", om.popField(vm.get("this"), "uncaughtExceptionHandler"));
    vm.put("v3", null);
    vm.put("param0", vm.get("v2"));
    if (!em.eq(vm.get("v1"), vm.get("v3"))) e.abort(String.format("!((v1=%s) eq (v3=%s))", vm.get("v1"), vm.get("v3")));
  }
  @IM(clazz="java.lang.Thread", name="setContextClassLoader", desc="(Ljava/lang/ClassLoader;)V",definedOutput={"this.contextClassLoader"},delta={"this.contextClassLoader"})
  static void setContextClassLoader3893(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", om.getStatic("java.lang.System.security"));
    vm.put("v1", vm.get("v0"));
    vm.put("this", om.getTarget());
    vm.put("v2", om.popField(vm.get("this"), "contextClassLoader"));
    vm.put("v3", null);
    vm.put("param0", vm.get("v2"));
    if (!em.eq(vm.get("v1"), vm.get("v3"))) e.abort(String.format("!((v1=%s) eq (v3=%s))", vm.get("v1"), vm.get("v3")));
  }
  @IM(clazz="java.lang.Thread", name="setName", desc="(Ljava/lang/String;)V",definedOutput={"this.name"},delta={"this.name"})
  static void setName57799(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getStatic("java.lang.System.security"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.popField(vm.get("this"), "name"));
    vm.put("v3", om.getField(vm.get("this"), "threadStatus"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", (int)0);
    vm.put("v6", null);
    vm.put("param0", vm.get("v2"));
    if (!em.eq(vm.get("v1"), vm.get("v6"))) e.abort(String.format("!((v1=%s) eq (v6=%s))", vm.get("v1"), vm.get("v6")));
    if (!em.ne(vm.get("param0"), vm.get("v6"))) e.abort(String.format("!((param0=%s) ne (v6=%s))", vm.get("param0"), vm.get("v6")));
    if (!em.eq(vm.get("v4"), vm.get("v5"))) e.abort(String.format("!((v4=%s) eq (v5=%s))", vm.get("v4"), vm.get("v5")));
  }
  @IM(clazz="org.apache.sshd.agent.unix.AgentServerProxy$1", name="run", desc="()V",definedOutput={"this.this$0.innerFinished"},delta={"this.this$0.innerFinished"})
  static void run57599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "this$0"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("v1"), "closed"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.popField(vm.get("v1"), "innerFinished"));
    vm.put("v5", (int)1);
    vm.put("v6", (int)0);
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    if (!em.ne(vm.get("v3"), vm.get("v6"))) e.abort(String.format("!((v3=%s) ne (v6=%s))", vm.get("v3"), vm.get("v6")));
  }
  @IM(clazz="java.lang.Thread", name="setDaemon", desc="(Z)V",definedOutput={"this.daemon"},delta={"this.daemon"})
  static void setDaemon25198(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "daemon"));
    vm.put("v1", om.getStatic("java.lang.System.security"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", (int)0);
    vm.put("v4", null);
    vm.put("v5", em.op("java.lang.Thread.isAlive()Z").eval(vm.get("this")));
    vm.put("param0", vm.get("v0"));
    if (!em.eq(vm.get("v5"), vm.get("v3"))) e.abort(String.format("!((v5=%s) eq (v3=%s))", vm.get("v5"), vm.get("v3")));
    if (!em.eq(vm.get("v2"), vm.get("v4"))) e.abort(String.format("!((v2=%s) eq (v4=%s))", vm.get("v2"), vm.get("v4")));
  }
  @IM(clazz="java.lang.Thread", name="blockedOn", desc="(Lsun/nio/ch/Interruptible;)V",definedOutput={"this.blocker"},delta={"this.blocker"})
  static void blockedOn15999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "blockerLock"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.popField(vm.get("this"), "blocker"));
    vm.put("param0", vm.get("v2"));
  }
}
