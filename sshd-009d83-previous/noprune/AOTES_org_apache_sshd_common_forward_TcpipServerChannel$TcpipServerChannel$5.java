import org.javelus.aotes.executor.*;
@Defined({"this.this$0.localWindow.size","this.val$len","this.this$0"})
public class AOTES_org_apache_sshd_common_forward_TcpipServerChannel$TcpipServerChannel$5 {
  @IM(clazz="org.apache.sshd.common.forward.TcpipServerChannel$5", name="operationComplete", desc="(Lorg/apache/sshd/common/io/IoWriteFuture;)V",revertedInput={"this.this$0.localWindow.size"},definedOutput={"this.this$0.localWindow.size"})
  @DG(pred={"_init_22299"})
  static void operationComplete36599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "this$0"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "val$len"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v1"), "localWindow"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", (int)2);
    vm.put("v7", (int)0);
    vm.put("v8", om.getField(vm.get("v5"), "size"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("v5"), "log"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", em.op("org.slf4j.Logger.isTraceEnabled()Z").eval(vm.get("v11")));
    vm.put("v13", om.getField(vm.get("v5"), "lock"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.getField(vm.get("v5"), "maxSize"));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", em.op("idiv").eval(vm.get("v16"),vm.get("v6")));
    vm.put("v18", em.op("iadd").eval(vm.get("v9"),vm.get("v3")));
    om.revertField(vm.get("v5"), "size", vm.get("v18"));
    if (!em.eq(vm.get("v12"), vm.get("v7"))) e.abort(String.format("!((v12=%s) eq (v7=%s))", vm.get("v12"), vm.get("v7")));
    if (!em.ge(vm.get("v9"), vm.get("v17"))) e.abort(String.format("!((v9=%s) ge (v17=%s))", vm.get("v9"), vm.get("v17")));
  }
  @IM(clazz="org.apache.sshd.common.forward.TcpipServerChannel$5", name="<init>", desc="(Lorg/apache/sshd/common/forward/TcpipServerChannel;I)V",definedOutput={"this.val$len","this.this$0"},delta={"this.val$len","this.this$0"})
  @DG(succ={"operationComplete36599"})
  static void _init_22299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "this$0"));
    vm.put("v1", om.popField(vm.get("this"), "val$len"));
    vm.put("param1", vm.get("v1"));
    vm.put("param0", vm.get("v0"));
  }
}
