import org.javelus.aotes.executor.*;
@Defined({"this.executors","this.fileSystem","this.optR","this.name","this.err","this.in","this.callback","this.optF","this.optT","this.receiveBufferSize","this.shutdownExecutor","this.optD","this.out","this.listener","this.sendBufferSize","this.log"})
public class AOTES_org_apache_sshd_server_command_ScpCommand {
  @IM(clazz="org.apache.sshd.server.command.ScpCommand", name="setErrorStream", desc="(Ljava/io/OutputStream;)V",definedOutput={"this.err"},delta={"this.err"})
  static void setErrorStream4899(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "err"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.command.ScpCommand", name="setOutputStream", desc="(Ljava/io/OutputStream;)V",definedOutput={"this.out"},delta={"this.out"})
  static void setOutputStream81699(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "out"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.command.ScpCommand", name="setInputStream", desc="(Ljava/io/InputStream;)V",definedOutput={"this.in"},delta={"this.in"})
  static void setInputStream43499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "in"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.command.ScpCommand", name="setExitCallback", desc="(Lorg/apache/sshd/server/ExitCallback;)V",definedOutput={"this.callback"},delta={"this.callback"})
  static void setExitCallback35999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "callback"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.command.ScpCommand", name="<init>", desc="(Ljava/lang/String;Ljava/util/concurrent/ExecutorService;ZIILorg/apache/sshd/common/scp/ScpTransferEventListener;)V",definedOutput={"this.optT","this.optF","this.executors","this.optR","this.name","this.receiveBufferSize","this.shutdownExecutor","this.optD","this.listener","this.log","this.sendBufferSize"},delta={"this.optT","this.optF","this.executors","this.optR","this.name","this.receiveBufferSize","this.shutdownExecutor","this.optD","this.listener","this.log","this.sendBufferSize"})
  static void _init_44809(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "log"));
    vm.put("v1", om.popField(vm.get("this"), "optR"));
    vm.put("v2", (int)1);
    vm.put("v3", om.popField(vm.get("this"), "name"));
    vm.put("v4", om.popField(vm.get("this"), "optT"));
    vm.put("v5", om.popField(vm.get("this"), "optD"));
    vm.put("v6", om.popField(vm.get("this"), "optF"));
    vm.put("v7", om.popField(vm.get("this"), "shutdownExecutor"));
    vm.put("v8", om.getStatic("org.apache.sshd.common.scp.ScpTransferEventListener.EMPTY"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.popField(vm.get("this"), "receiveBufferSize"));
    vm.put("v11", om.popField(vm.get("this"), "listener"));
    vm.put("v12", om.popField(vm.get("this"), "sendBufferSize"));
    vm.put("v13", om.popField(vm.get("this"), "executors"));
    vm.put("v14", (int)3);
    vm.put("v15", " ");
    vm.put("v16", (int)2);
    vm.put("v17", (int)0);
    vm.put("v18", (int)45);
    vm.put("v19", (int)6);
    vm.put("v20", (int)4);
    vm.put("v21", (int)5);
    vm.put("v22", null);
    vm.put("v23", (int)127);
    vm.put("v24", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("this")));
    vm.put("v25", em.op("org.slf4j.LoggerFactory.getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;").eval(vm.get("v24")));
    if (!em.eq(vm.get("v2"), (vm.get("v6")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v6")));
    vm.put("param1", vm.get("v13"));
    vm.put("param3", vm.get("v12"));
    vm.put("param2", vm.get("v7"));
    if (!em.eq(vm.get("v25"), (vm.get("v0")))) e.abort("Inconsistent value for \"v25\": " + vm.get("v25") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v2"), (vm.get("v1")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v1")));
    vm.put("param0", vm.get("v3"));
    vm.put("v26", em.op("java.lang.String.split(Ljava/lang/String;)[Ljava/lang/String;").eval(vm.get("param0"),vm.get("v15")));
    vm.put("v27", em.op("xaload").eval(vm.get("v26"),vm.get("v16")));
    vm.put("v28", em.op("java.lang.String.charAt(I)C").eval(vm.get("v27"),vm.get("v17")));
    vm.put("v29", em.op("arraylength").eval(vm.get("v26")));
    vm.put("v30", em.op("arraylength").eval(vm.get("v26")));
    vm.put("v31", em.op("java.lang.String.length()I").eval(vm.get("v27")));
    vm.put("v32", em.op("java.lang.String.charAt(I)C").eval(vm.get("v27"),vm.get("v2")));
    vm.put("v33", em.op("java.lang.String.length()I").eval(vm.get("v27")));
    vm.put("v34", em.op("arraylength").eval(vm.get("v26")));
    vm.put("v35", em.op("xaload").eval(vm.get("v26"),vm.get("v2")));
    vm.put("v36", em.op("java.lang.String.charAt(I)C").eval(vm.get("v35"),vm.get("v17")));
    vm.put("v37", em.op("java.lang.String.length()I").eval(vm.get("v35")));
    vm.put("v38", em.op("java.lang.String.charAt(I)C").eval(vm.get("v35"),vm.get("v2")));
    vm.put("v39", em.op("java.lang.String.length()I").eval(vm.get("v35")));
    vm.put("v40", em.op("java.lang.String.charAt(I)C").eval(vm.get("v35"),vm.get("v16")));
    vm.put("v41", em.op("java.lang.String.length()I").eval(vm.get("v35")));
    vm.put("v42", em.op("java.lang.String.charAt(I)C").eval(vm.get("v35"),vm.get("v14")));
    vm.put("v43", em.op("java.lang.String.length()I").eval(vm.get("v35")));
    vm.put("v44", em.op("java.lang.String.charAt(I)C").eval(vm.get("v35"),vm.get("v20")));
    vm.put("v45", em.op("java.lang.String.length()I").eval(vm.get("v35")));
    vm.put("v46", em.op("java.lang.String.charAt(I)C").eval(vm.get("v35"),vm.get("v21")));
    vm.put("v47", em.op("java.lang.String.length()I").eval(vm.get("v35")));
    vm.put("param4", vm.get("v10"));
    if (!em.eq(vm.get("v2"), (vm.get("v4")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v4")));
    if (!em.eq(vm.get("v9"), (vm.get("v11")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v11")));
    if (!em.eq(vm.get("v2"), (vm.get("v5")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v5")));
    vm.put("param5", om.newDefaultValue("org.apache.sshd.common.scp.ScpTransferEventListener"));
    vm.put("v48", (int)116);
    vm.put("v49", (int)102);
    vm.put("v50", (int)100);
    vm.put("v51", (int)112);
    vm.put("v52", (int)114);
    if (!em.ge(vm.get("v19"), vm.get("v47"))) e.abort(String.format("!((v19=%s) ge (v47=%s))", vm.get("v19"), vm.get("v47")));
    if (!em.eq(vm.get("param5"), vm.get("v22"))) e.abort(String.format("!((param5=%s) eq (v22=%s))", vm.get("param5"), vm.get("v22")));
    if (!em.lt(vm.get("v20"), vm.get("v43"))) e.abort(String.format("!((v20=%s) lt (v43=%s))", vm.get("v20"), vm.get("v43")));
    if (!em.lt(vm.get("v2"), vm.get("v34"))) e.abort(String.format("!((v2=%s) lt (v34=%s))", vm.get("v2"), vm.get("v34")));
    if (!em.ge(vm.get("v16"), vm.get("v33"))) e.abort(String.format("!((v16=%s) ge (v33=%s))", vm.get("v16"), vm.get("v33")));
    if (!em.eq(vm.get("v36"), vm.get("v18"))) e.abort(String.format("!((v36=%s) eq (v18=%s))", vm.get("v36"), vm.get("v18")));
    if (!em.eq(vm.get("v28"), vm.get("v18"))) e.abort(String.format("!((v28=%s) eq (v18=%s))", vm.get("v28"), vm.get("v18")));
    if (!em.ge(vm.get("v14"), vm.get("v29"))) e.abort(String.format("!((v14=%s) ge (v29=%s))", vm.get("v14"), vm.get("v29")));
    if (!em.lt(vm.get("v2"), vm.get("v31"))) e.abort(String.format("!((v2=%s) lt (v31=%s))", vm.get("v2"), vm.get("v31")));
    if (!em.lt(vm.get("v21"), vm.get("v45"))) e.abort(String.format("!((v21=%s) lt (v45=%s))", vm.get("v21"), vm.get("v45")));
    if (!em.lt(vm.get("v14"), vm.get("v41"))) e.abort(String.format("!((v14=%s) lt (v41=%s))", vm.get("v14"), vm.get("v41")));
    if (!em.ge(vm.get("param4"), vm.get("v23"))) e.abort(String.format("!((param4=%s) ge (v23=%s))", vm.get("param4"), vm.get("v23")));
    if (!em.lt(vm.get("v16"), vm.get("v39"))) e.abort(String.format("!((v16=%s) lt (v39=%s))", vm.get("v16"), vm.get("v39")));
    if (!em.lt(vm.get("v16"), vm.get("v30"))) e.abort(String.format("!((v16=%s) lt (v30=%s))", vm.get("v16"), vm.get("v30")));
    if (!em.ne(vm.get("param1"), vm.get("v22"))) e.abort(String.format("!((param1=%s) ne (v22=%s))", vm.get("param1"), vm.get("v22")));
    if (!em.ge(vm.get("param3"), vm.get("v23"))) e.abort(String.format("!((param3=%s) ge (v23=%s))", vm.get("param3"), vm.get("v23")));
    if (!em.lt(vm.get("v2"), vm.get("v37"))) e.abort(String.format("!((v2=%s) lt (v37=%s))", vm.get("v2"), vm.get("v37")));
    if (!em.eq(vm.get("v46"), vm.get("v48"))) e.abort(String.format("!((v46=%s) eq (v48=%s))", vm.get("v46"), vm.get("v48")));
    if (!em.eq(vm.get("v32"), vm.get("v49"))) e.abort(String.format("!((v32=%s) eq (v49=%s))", vm.get("v32"), vm.get("v49")));
    if (!em.ne(vm.get("v42"), vm.get("v50"))) e.abort(String.format("!((v42=%s) ne (v50=%s))", vm.get("v42"), vm.get("v50")));
    if (!em.ne(vm.get("v42"), vm.get("v49"))) e.abort(String.format("!((v42=%s) ne (v49=%s))", vm.get("v42"), vm.get("v49")));
    if (!em.ne(vm.get("v42"), vm.get("v51"))) e.abort(String.format("!((v42=%s) ne (v51=%s))", vm.get("v42"), vm.get("v51")));
    if (!em.ne(vm.get("v42"), vm.get("v52"))) e.abort(String.format("!((v42=%s) ne (v52=%s))", vm.get("v42"), vm.get("v52")));
    if (!em.ne(vm.get("v42"), vm.get("v48"))) e.abort(String.format("!((v42=%s) ne (v48=%s))", vm.get("v42"), vm.get("v48")));
    if (!em.ne(vm.get("v38"), vm.get("v50"))) e.abort(String.format("!((v38=%s) ne (v50=%s))", vm.get("v38"), vm.get("v50")));
    if (!em.ne(vm.get("v38"), vm.get("v49"))) e.abort(String.format("!((v38=%s) ne (v49=%s))", vm.get("v38"), vm.get("v49")));
    if (!em.ne(vm.get("v38"), vm.get("v51"))) e.abort(String.format("!((v38=%s) ne (v51=%s))", vm.get("v38"), vm.get("v51")));
    if (!em.ne(vm.get("v38"), vm.get("v52"))) e.abort(String.format("!((v38=%s) ne (v52=%s))", vm.get("v38"), vm.get("v52")));
    if (!em.ne(vm.get("v38"), vm.get("v48"))) e.abort(String.format("!((v38=%s) ne (v48=%s))", vm.get("v38"), vm.get("v48")));
    if (!em.eq(vm.get("v44"), vm.get("v52"))) e.abort(String.format("!((v44=%s) eq (v52=%s))", vm.get("v44"), vm.get("v52")));
    if (!em.eq(vm.get("v40"), vm.get("v50"))) e.abort(String.format("!((v40=%s) eq (v50=%s))", vm.get("v40"), vm.get("v50")));
  }
  @IM(clazz="org.apache.sshd.server.command.ScpCommand", name="setFileSystem", desc="(Ljava/nio/file/FileSystem;)V",definedOutput={"this.fileSystem"},delta={"this.fileSystem"})
  static void setFileSystem47399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "fileSystem"));
    vm.put("param0", vm.get("v0"));
  }
}
