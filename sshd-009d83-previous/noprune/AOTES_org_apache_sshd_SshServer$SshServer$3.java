import org.javelus.aotes.executor.*;
@Defined({"this.this$0","this.this$0.sessionTimeoutListener"})
public class AOTES_org_apache_sshd_SshServer$SshServer$3 {
  @IM(clazz="org.apache.sshd.SshServer$3", name="run", desc="()V",definedOutput={"this.this$0.sessionTimeoutListener"},delta={"this.this$0.sessionTimeoutListener"})
  @DG(pred={"_init_79999"})
  static void run25799(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.getField(vm.get("this"), "this$0"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("v2"), "timeoutListenerFuture"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.popField(vm.get("v2"), "sessionTimeoutListener"));
    vm.put("v6", om.getField(vm.get("v2"), "sessionFactory"));
    vm.put("v7", vm.get("v6"));
    if (!em.eq(vm.get("v0"), (vm.get("v5")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v5")));
    if (!em.eq(vm.get("v4"), vm.get("v0"))) e.abort(String.format("!((v4=%s) eq (v0=%s))", vm.get("v4"), vm.get("v0")));
    if (!em.eq(vm.get("v7"), vm.get("v0"))) e.abort(String.format("!((v7=%s) eq (v0=%s))", vm.get("v7"), vm.get("v0")));
  }
  @IM(clazz="org.apache.sshd.SshServer$3", name="run", desc="()V",revertedInput={"this.this$0.sessionTimeoutListener"},definedOutput={"this.this$0.sessionTimeoutListener"})
  @DG(pred={"_init_79999"})
  static void run25791(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "this$0"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("v1"), "sessionFactory"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v1"), "timeoutListenerFuture"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v1"), "sessionTimeoutListener"));
    vm.put("v7", null);
    if (!em.eq(vm.get("v7"), (vm.get("v6")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v6")));
    om.revertField(vm.get("v1"), "sessionTimeoutListener");
    vm.put("v8", om.getField(vm.get("v1"), "sessionTimeoutListener"));
    if (!em.ne(vm.get("v3"), vm.get("v7"))) e.abort(String.format("!((v3=%s) ne (v7=%s))", vm.get("v3"), vm.get("v7")));
    if (!em.eq(vm.get("v5"), vm.get("v7"))) e.abort(String.format("!((v5=%s) eq (v7=%s))", vm.get("v5"), vm.get("v7")));
    if (!em.eq(vm.get("v8"), vm.get("v7"))) e.abort(String.format("!((v8=%s) eq (v7=%s))", vm.get("v8"), vm.get("v7")));
  }
  @IM(clazz="org.apache.sshd.SshServer$3", name="<init>", desc="(Lorg/apache/sshd/SshServer;)V",definedOutput={"this.this$0"},delta={"this.this$0"})
  @DG(succ={"run25791","run25799"})
  static void _init_79999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "this$0"));
    vm.put("param0", vm.get("v0"));
  }
}
