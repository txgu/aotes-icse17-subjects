import org.javelus.aotes.executor.*;
@Defined({"this.this$0","this.this$0.sessionTimeoutListener"})
public class AOTES_org_apache_sshd_SshClient$SshClient$3 {
  @IM(clazz="org.apache.sshd.SshClient$3", name="<init>", desc="(Lorg/apache/sshd/SshClient;)V",definedOutput={"this.this$0"},delta={"this.this$0"})
  @DG(succ={"run15096","run15092"})
  static void _init_20399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "this$0"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.SshClient$3", name="run", desc="()V",definedOutput={"this.this$0.sessionTimeoutListener"},delta={"this.this$0.sessionTimeoutListener"})
  @DG(pred={"_init_20399"})
  static void run15092(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.getField(vm.get("this"), "this$0"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("v2"), "timeoutListenerFuture"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("v2"), "sessionFactory"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.popField(vm.get("v2"), "sessionTimeoutListener"));
    if (!em.eq(vm.get("v0"), (vm.get("v7")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v7")));
    if (!em.eq(vm.get("v6"), vm.get("v0"))) e.abort(String.format("!((v6=%s) eq (v0=%s))", vm.get("v6"), vm.get("v0")));
    if (!em.eq(vm.get("v4"), vm.get("v0"))) e.abort(String.format("!((v4=%s) eq (v0=%s))", vm.get("v4"), vm.get("v0")));
  }
  @IM(clazz="org.apache.sshd.SshClient$3", name="run", desc="()V",revertedInput={"this.this$0.sessionTimeoutListener"},definedOutput={"this.this$0.sessionTimeoutListener"})
  @DG(pred={"_init_20399"})
  static void run15096(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "this$0"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("v1"), "sessionFactory"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v1"), "sessionTimeoutListener"));
    vm.put("v5", null);
    vm.put("v6", om.getField(vm.get("v1"), "timeoutListenerFuture"));
    vm.put("v7", vm.get("v6"));
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    om.revertField(vm.get("v1"), "sessionTimeoutListener");
    vm.put("v8", om.getField(vm.get("v1"), "sessionTimeoutListener"));
    if (!em.ne(vm.get("v3"), vm.get("v5"))) e.abort(String.format("!((v3=%s) ne (v5=%s))", vm.get("v3"), vm.get("v5")));
    if (!em.eq(vm.get("v8"), vm.get("v5"))) e.abort(String.format("!((v8=%s) eq (v5=%s))", vm.get("v8"), vm.get("v5")));
    if (!em.eq(vm.get("v7"), vm.get("v5"))) e.abort(String.format("!((v7=%s) eq (v5=%s))", vm.get("v7"), vm.get("v5")));
  }
}
