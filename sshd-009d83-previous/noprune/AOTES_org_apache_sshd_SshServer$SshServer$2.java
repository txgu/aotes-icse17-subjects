import org.javelus.aotes.executor.*;
@Defined({"this.this$0.acceptor","this.this$0.ioServiceFactory","this.this$0"})
public class AOTES_org_apache_sshd_SshServer$SshServer$2 {
  @IM(clazz="org.apache.sshd.SshServer$2", name="run", desc="()V",definedOutput={"this.this$0.acceptor","this.this$0.ioServiceFactory"},delta={"this.this$0.acceptor","this.this$0.ioServiceFactory"})
  @DG(pred={"_init_8199"})
  static void run58999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.getField(vm.get("this"), "this$0"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.popField(vm.get("v2"), "acceptor"));
    vm.put("v4", (int)0);
    vm.put("v5", om.popField(vm.get("v2"), "ioServiceFactory"));
    vm.put("v6", om.getField(vm.get("v2"), "shutdownExecutor"));
    vm.put("v7", vm.get("v6"));
    if (!em.eq(vm.get("v0"), (vm.get("v5")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v5")));
    if (!em.eq(vm.get("v0"), (vm.get("v3")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v3")));
    if (!em.eq(vm.get("v7"), vm.get("v4"))) e.abort(String.format("!((v7=%s) eq (v4=%s))", vm.get("v7"), vm.get("v4")));
  }
  @IM(clazz="org.apache.sshd.SshServer$2", name="run", desc="()V",definedOutput={"this.this$0.acceptor","this.this$0.ioServiceFactory"},delta={"this.this$0.acceptor","this.this$0.ioServiceFactory"})
  @DG(pred={"_init_8199"})
  static void run58998(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.getField(vm.get("this"), "this$0"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("v2"), "executor"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("v2"), "shutdownExecutor"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.popField(vm.get("v2"), "ioServiceFactory"));
    vm.put("v8", (int)0);
    vm.put("v9", om.popField(vm.get("v2"), "acceptor"));
    if (!em.eq(vm.get("v0"), (vm.get("v7")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v7")));
    if (!em.eq(vm.get("v0"), (vm.get("v9")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v9")));
    if (!em.eq(vm.get("v4"), vm.get("v0"))) e.abort(String.format("!((v4=%s) eq (v0=%s))", vm.get("v4"), vm.get("v0")));
    if (!em.ne(vm.get("v6"), vm.get("v8"))) e.abort(String.format("!((v6=%s) ne (v8=%s))", vm.get("v6"), vm.get("v8")));
  }
  @IM(clazz="org.apache.sshd.SshServer$2", name="<init>", desc="(Lorg/apache/sshd/SshServer;)V",definedOutput={"this.this$0"},delta={"this.this$0"})
  @DG(succ={"run58999","run58998"})
  static void _init_8199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "this$0"));
    vm.put("param0", vm.get("v0"));
  }
}
