import org.javelus.aotes.executor.*;
@Defined({"this.agent"})
public class AOTES_org_apache_sshd_agent_common_AgentDelegate {
  @IM(clazz="org.apache.sshd.agent.common.AgentDelegate", name="<init>", desc="(Lorg/apache/sshd/agent/SshAgent;)V",definedOutput={"this.agent"},delta={"this.agent"})
  static void _init_46099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "agent"));
    vm.put("param0", vm.get("v0"));
  }
}
