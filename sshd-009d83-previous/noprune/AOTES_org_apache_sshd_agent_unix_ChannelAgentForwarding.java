import org.javelus.aotes.executor.*;
@Defined({"this.gracefulFuture.listeners[*].this$0","this.eof","this.gracefulFuture.listeners","this.gracefulFuture.listeners[*]","this.closeFuture.result","this.gracefulFuture.listeners.this$0","this.state.value"})
public class AOTES_org_apache_sshd_agent_unix_ChannelAgentForwarding {
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="doCloseImmediately", desc="()V",revertedInput={"this.gracefulFuture.listeners"},definedOutput={"this.gracefulFuture.listeners","this.gracefulFuture.listeners.this$0"},delta={"this.gracefulFuture.listeners.this$0"})
  static void doCloseImmediately86496(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "service"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "gracefulFuture"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v3"), "lock"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("v3"), "listeners"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", null);
    vm.put("v11", om.popField(vm.get("v9"), "this$0"));
    vm.put("v12", om.getField(vm.get("v3"), "result"));
    vm.put("v13", vm.get("v12"));
    if (!em.eq(vm.get("this"), (vm.get("v11")))) e.abort("Inconsistent value for \"this\": " + vm.get("this") + " ne " + (vm.get("v11")));
    om.revertField(vm.get("v3"), "listeners");
    vm.put("v14", om.getField(vm.get("v3"), "listeners"));
    if (!em.eq(vm.get("v13"), vm.get("v10"))) e.abort(String.format("!((v13=%s) eq (v10=%s))", vm.get("v13"), vm.get("v10")));
    if (!em.ne(vm.get("v13"), vm.get("v10"))) e.abort(String.format("!((v13=%s) ne (v10=%s))", vm.get("v13"), vm.get("v10")));
    if (!em.eq(vm.get("v14"), vm.get("v10"))) e.abort(String.format("!((v14=%s) eq (v10=%s))", vm.get("v14"), vm.get("v10")));
    if (!em.eq(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) eq (v10=%s))", vm.get("v1"), vm.get("v10")));
  }
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="doCloseImmediately", desc="()V",revertedInput={"this.gracefulFuture.listeners"},definedOutput={"this.gracefulFuture.listeners[*].this$0","this.gracefulFuture.listeners","this.gracefulFuture.listeners[*]"},delta={"this.gracefulFuture.listeners[*].this$0","this.gracefulFuture.listeners[*]"})
  static void doCloseImmediately86461(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "gracefulFuture"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", (int)2);
    vm.put("v3", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", (int)0);
    vm.put("v6", om.getField(vm.get("v1"), "result"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("v1"), "listeners"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("v1"), "lock"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("this"), "service"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", (int)1);
    vm.put("v15", null);
    vm.put("v16", om.getArrayLength(vm.get("v9")));
    vm.put("v17", om.popArrayElement(vm.get("v9"), vm.get("v14")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.popArrayElement(vm.get("v9"), vm.get("v5")));
    vm.put("v20", vm.get("v19"));
    om.revertField(vm.get("v1"), "listeners", vm.get("v20"));
    vm.put("v21", om.popField(vm.get("v18"), "this$0"));
    if (!em.eq(vm.get("this"), (vm.get("v21")))) e.abort("Inconsistent value for \"this\": " + vm.get("this") + " ne " + (vm.get("v21")));
    if (!em.eq(vm.get("v2"), (vm.get("v16")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v16")));
    if (!em.ne(vm.get("v20"), vm.get("v15"))) e.abort(String.format("!((v20=%s) ne (v15=%s))", vm.get("v20"), vm.get("v15")));
    if (!em.eq(vm.get("v7"), vm.get("v15"))) e.abort(String.format("!((v7=%s) eq (v15=%s))", vm.get("v7"), vm.get("v15")));
    if (!em.eq(vm.get("v13"), vm.get("v15"))) e.abort(String.format("!((v13=%s) eq (v15=%s))", vm.get("v13"), vm.get("v15")));
    if (!em.ne(vm.get("v7"), vm.get("v15"))) e.abort(String.format("!((v7=%s) ne (v15=%s))", vm.get("v7"), vm.get("v15")));
  }
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="doCloseImmediately", desc="()V",definedOutput={"this.state.value"},delta={"this.state.value"})
  static void doCloseImmediately86498(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "state"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "gracefulFuture"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popField(vm.get("v1"), "value"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("v3"), "result"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("this"), "service"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("this"), "closeFuture"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("v3"), "lock"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.getStatic("org.apache.sshd.common.util.CloseableUtils$AbstractCloseable$State.Closed"));
    vm.put("v17", om.getField(vm.get("v13"), "result"));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", null);
    vm.put("v20", om.getField(vm.get("v13"), "lock"));
    vm.put("v21", vm.get("v20"));
    if (!em.eq(vm.get("v7"), (vm.get("v16")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v16")));
    if (!em.eq(vm.get("v11"), vm.get("v19"))) e.abort(String.format("!((v11=%s) eq (v19=%s))", vm.get("v11"), vm.get("v19")));
    if (!em.ne(vm.get("v9"), vm.get("v19"))) e.abort(String.format("!((v9=%s) ne (v19=%s))", vm.get("v9"), vm.get("v19")));
    if (!em.ne(vm.get("v18"), vm.get("v19"))) e.abort(String.format("!((v18=%s) ne (v19=%s))", vm.get("v18"), vm.get("v19")));
    if (!em.ne(vm.get("v9"), vm.get("v19"))) e.abort(String.format("!((v9=%s) ne (v19=%s))", vm.get("v9"), vm.get("v19")));
  }
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="doCloseImmediately", desc="()V",revertedInput={"this.closeFuture.result"},definedOutput={"this.closeFuture.result","this.state.value"},delta={"this.state.value"})
  static void doCloseImmediately19755(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "closeFuture"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "gracefulFuture"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "state"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("v3"), "result"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("this"), "service"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getStatic("org.apache.sshd.common.util.CloseableUtils$AbstractCloseable$State.Closed"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("v1"), "listeners"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.getField(vm.get("v1"), "lock"));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.popField(vm.get("v7"), "value"));
    vm.put("v19", om.getField(vm.get("v1"), "result"));
    vm.put("v20", om.getField(vm.get("v3"), "lock"));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", null);
    if (!em.eq(vm.get("v5"), (vm.get("v19")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v19")));
    if (!em.eq(vm.get("v13"), (vm.get("v18")))) e.abort("Inconsistent value for \"v13\": " + vm.get("v13") + " ne " + (vm.get("v18")));
    om.revertField(vm.get("v1"), "result");
    vm.put("v23", om.getField(vm.get("v1"), "result"));
    if (!em.ne(vm.get("v5"), vm.get("v22"))) e.abort(String.format("!((v5=%s) ne (v22=%s))", vm.get("v5"), vm.get("v22")));
    if (!em.eq(vm.get("v23"), vm.get("v22"))) e.abort(String.format("!((v23=%s) eq (v22=%s))", vm.get("v23"), vm.get("v22")));
    if (!em.ne(vm.get("v9"), vm.get("v22"))) e.abort(String.format("!((v9=%s) ne (v22=%s))", vm.get("v9"), vm.get("v22")));
    if (!em.eq(vm.get("v11"), vm.get("v22"))) e.abort(String.format("!((v11=%s) eq (v22=%s))", vm.get("v11"), vm.get("v22")));
    if (!em.ne(vm.get("v9"), vm.get("v22"))) e.abort(String.format("!((v9=%s) ne (v22=%s))", vm.get("v9"), vm.get("v22")));
    if (!em.eq(vm.get("v15"), vm.get("v22"))) e.abort(String.format("!((v15=%s) eq (v22=%s))", vm.get("v15"), vm.get("v22")));
  }
  @IM(clazz="org.apache.sshd.common.channel.AbstractChannel", name="doCloseImmediately", desc="()V",revertedInput={"this.closeFuture.result"},definedOutput={"this.closeFuture.result","this.state.value"},delta={"this.state.value"})
  static void doCloseImmediately86463(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "gracefulFuture"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("v1"), "result"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "state"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("v1"), "lock"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getStatic("org.apache.sshd.common.future.DefaultSshFuture.NULL"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("this"), "closeFuture"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("v13"), "lock"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.getStatic("org.apache.sshd.common.util.CloseableUtils$AbstractCloseable$State.Closed"));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.getField(vm.get("this"), "service"));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", null);
    vm.put("v21", om.getField(vm.get("v13"), "listeners"));
    vm.put("v22", vm.get("v21"));
    vm.put("v23", om.getField(vm.get("v13"), "result"));
    vm.put("v24", om.popField(vm.get("v5"), "value"));
    if (!em.eq(vm.get("v11"), (vm.get("v23")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v23")));
    if (!em.eq(vm.get("v17"), (vm.get("v24")))) e.abort("Inconsistent value for \"v17\": " + vm.get("v17") + " ne " + (vm.get("v24")));
    om.revertField(vm.get("v13"), "result");
    vm.put("v25", om.getField(vm.get("v13"), "result"));
    if (!em.eq(vm.get("v19"), vm.get("v20"))) e.abort(String.format("!((v19=%s) eq (v20=%s))", vm.get("v19"), vm.get("v20")));
    if (!em.eq(vm.get("v25"), vm.get("v20"))) e.abort(String.format("!((v25=%s) eq (v20=%s))", vm.get("v25"), vm.get("v20")));
    if (!em.eq(vm.get("v22"), vm.get("v20"))) e.abort(String.format("!((v22=%s) eq (v20=%s))", vm.get("v22"), vm.get("v20")));
    if (!em.ne(vm.get("v3"), vm.get("v20"))) e.abort(String.format("!((v3=%s) ne (v20=%s))", vm.get("v3"), vm.get("v20")));
    if (!em.eq(vm.get("v7"), vm.get("v20"))) e.abort(String.format("!((v7=%s) eq (v20=%s))", vm.get("v7"), vm.get("v20")));
    if (!em.ne(vm.get("v3"), vm.get("v20"))) e.abort(String.format("!((v3=%s) ne (v20=%s))", vm.get("v3"), vm.get("v20")));
  }
  @IM(clazz="org.apache.sshd.agent.unix.ChannelAgentForwarding", name="handleEof", desc="()V",definedOutput={"this.eof"},delta={"this.eof"})
  static void handleEof81099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "eof"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "log"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "lock"));
    vm.put("v5", vm.get("v4"));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
  }
}
