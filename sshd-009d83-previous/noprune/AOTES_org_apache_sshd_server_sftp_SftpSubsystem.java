import org.javelus.aotes.executor.*;
@Defined({"org.apache.sshd.server.sftp.SftpSubsystem.DEFAULT_UNIX_VIEW.modCount","this.session","org.apache.sshd.server.sftp.SftpSubsystem.ALL_SFTP_IMPL","org.apache.sshd.server.sftp.SftpSubsystem.ALL_SFTP_IMPL.value","org.apache.sshd.server.sftp.SftpSubsystem.DEFAULT_UNIX_VIEW","org.apache.sshd.server.sftp.SftpSubsystem.HIGHER_SFTP_IMPL","this.callback","org.apache.sshd.server.sftp.SftpSubsystem.MONTHS[*]","this.in","org.apache.sshd.server.sftp.SftpSubsystem.MONTHS","org.apache.sshd.server.sftp.SftpSubsystem.LOWER_SFTP_IMPL","this.out","org.apache.sshd.server.sftp.SftpSubsystem.DEFAULT_UNIX_VIEW.element","this.err"})
public class AOTES_org_apache_sshd_server_sftp_SftpSubsystem {
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem", name="setErrorStream", desc="(Ljava/io/OutputStream;)V",definedOutput={"this.err"},delta={"this.err"})
  static void setErrorStream52799(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "err"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem", name="setOutputStream", desc="(Ljava/io/OutputStream;)V",definedOutput={"this.out"},delta={"this.out"})
  static void setOutputStream58499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "out"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem", name="setExitCallback", desc="(Lorg/apache/sshd/server/ExitCallback;)V",definedOutput={"this.callback"},delta={"this.callback"})
  static void setExitCallback65299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "callback"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem", name="setSession", desc="(Lorg/apache/sshd/server/session/ServerSession;)V",definedOutput={"this.session"},delta={"this.session"})
  static void setSession11599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "session"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem", name="setInputStream", desc="(Ljava/io/InputStream;)V",definedOutput={"this.in"},delta={"this.in"})
  static void setInputStream73299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "in"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem", name="<clinit>", desc="()V",definedOutput={"org.apache.sshd.server.sftp.SftpSubsystem.DEFAULT_UNIX_VIEW.modCount","org.apache.sshd.server.sftp.SftpSubsystem.MONTHS","org.apache.sshd.server.sftp.SftpSubsystem.ALL_SFTP_IMPL","org.apache.sshd.server.sftp.SftpSubsystem.ALL_SFTP_IMPL.value","org.apache.sshd.server.sftp.SftpSubsystem.DEFAULT_UNIX_VIEW","org.apache.sshd.server.sftp.SftpSubsystem.LOWER_SFTP_IMPL","org.apache.sshd.server.sftp.SftpSubsystem.HIGHER_SFTP_IMPL","org.apache.sshd.server.sftp.SftpSubsystem.DEFAULT_UNIX_VIEW.element","org.apache.sshd.server.sftp.SftpSubsystem.MONTHS[*]"},delta={"org.apache.sshd.server.sftp.SftpSubsystem.DEFAULT_UNIX_VIEW.modCount","org.apache.sshd.server.sftp.SftpSubsystem.MONTHS","org.apache.sshd.server.sftp.SftpSubsystem.ALL_SFTP_IMPL","org.apache.sshd.server.sftp.SftpSubsystem.ALL_SFTP_IMPL.value","org.apache.sshd.server.sftp.SftpSubsystem.DEFAULT_UNIX_VIEW","org.apache.sshd.server.sftp.SftpSubsystem.LOWER_SFTP_IMPL","org.apache.sshd.server.sftp.SftpSubsystem.HIGHER_SFTP_IMPL","org.apache.sshd.server.sftp.SftpSubsystem.DEFAULT_UNIX_VIEW.element","org.apache.sshd.server.sftp.SftpSubsystem.MONTHS[*]"})
  static void _clinit_7099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", om.popStatic("org.apache.sshd.server.sftp.SftpSubsystem.LOWER_SFTP_IMPL"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", (int)9);
    vm.put("v3", "Oct");
    vm.put("v4", (int)11);
    vm.put("v5", "Dec");
    vm.put("v6", (int)0);
    vm.put("v7", "Jan");
    vm.put("v8", "");
    vm.put("v9", (int)10);
    vm.put("v10", "Nov");
    vm.put("v11", (int)2);
    vm.put("v12", "Mar");
    vm.put("v13", om.popStatic("org.apache.sshd.server.sftp.SftpSubsystem.HIGHER_SFTP_IMPL"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", (int)12);
    vm.put("v16", (int)5);
    vm.put("v17", "Jun");
    vm.put("v18", "unix:*");
    vm.put("v19", (int)7);
    vm.put("v20", "Aug");
    vm.put("v21", (int)6);
    vm.put("v22", "Jul");
    vm.put("v23", (int)8);
    vm.put("v24", "Sep");
    vm.put("v25", (int)4);
    vm.put("v26", "May");
    vm.put("v27", (int)3);
    vm.put("v28", "Apr");
    vm.put("v29", om.getStatic("org.apache.sshd.common.sftp.SftpConstants.SFTP_V6"));
    vm.put("v30", om.getStatic("org.apache.sshd.common.sftp.SftpConstants.SFTP_V3"));
    vm.put("v31", om.popStatic("org.apache.sshd.server.sftp.SftpSubsystem.DEFAULT_UNIX_VIEW"));
    vm.put("v32", vm.get("v31"));
    vm.put("v33", (int)1);
    vm.put("v34", "Feb");
    vm.put("v35", om.popStatic("org.apache.sshd.server.sftp.SftpSubsystem.MONTHS"));
    vm.put("v36", vm.get("v35"));
    vm.put("v37", om.popStatic("org.apache.sshd.server.sftp.SftpSubsystem.ALL_SFTP_IMPL"));
    vm.put("v38", vm.get("v37"));
    vm.put("v39", om.popArrayElement(vm.get("v36"), vm.get("v11")));
    vm.put("v40", om.popArrayElement(vm.get("v36"), vm.get("v21")));
    vm.put("v41", om.getArrayLength(vm.get("v36")));
    vm.put("v42", em.op("getfield_or_static").eval(vm.get("v8")));
    vm.put("v43", om.popArrayElement(vm.get("v36"), vm.get("v19")));
    vm.put("v44", om.popField(vm.get("v32"), "modCount"));
    vm.put("v45", om.popField(vm.get("v38"), "value"));
    vm.put("v46", om.popArrayElement(vm.get("v36"), vm.get("v6")));
    vm.put("v47", om.popField(vm.get("v32"), "element"));
    vm.put("v48", em.op("isub").eval(vm.get("v14"),vm.get("v1")));
    vm.put("v49", om.popArrayElement(vm.get("v36"), vm.get("v23")));
    vm.put("v50", om.popArrayElement(vm.get("v36"), vm.get("v33")));
    vm.put("v51", om.popArrayElement(vm.get("v36"), vm.get("v25")));
    vm.put("v52", om.popArrayElement(vm.get("v36"), vm.get("v16")));
    vm.put("v53", om.popArrayElement(vm.get("v36"), vm.get("v9")));
    vm.put("v54", om.popArrayElement(vm.get("v36"), vm.get("v2")));
    vm.put("v55", om.popArrayElement(vm.get("v36"), vm.get("v27")));
    vm.put("v56", em.op("iadd").eval(vm.get("v33"),vm.get("v48")));
    vm.put("v57", em.op("imul").eval(vm.get("v11"),vm.get("v56")));
    vm.put("v58", om.popArrayElement(vm.get("v36"), vm.get("v4")));
    if (!em.eq(vm.get("v18"), (vm.get("v47")))) e.abort("Inconsistent value for \"v18\": " + vm.get("v18") + " ne " + (vm.get("v47")));
    if (!em.eq(vm.get("v15"), (vm.get("v41")))) e.abort("Inconsistent value for \"v15\": " + vm.get("v15") + " ne " + (vm.get("v41")));
    if (!em.eq(vm.get("v6"), (vm.get("v44")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v44")));
    if (!em.eq(vm.get("v42"), (vm.get("v45")))) e.abort("Inconsistent value for \"v42\": " + vm.get("v42") + " ne " + (vm.get("v45")));
    if (!em.eq(vm.get("v14"), (vm.get("v29")))) e.abort("Inconsistent value for \"v14\": " + vm.get("v14") + " ne " + (vm.get("v29")));
    if (!em.eq(vm.get("v1"), (vm.get("v30")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v30")));
    if (!em.le(vm.get("v6"), vm.get("v57"))) e.abort(String.format("!((v6=%s) le (v57=%s))", vm.get("v6"), vm.get("v57")));
    if (!em.gt(vm.get("v1"), vm.get("v14"))) e.abort(String.format("!((v1=%s) gt (v14=%s))", vm.get("v1"), vm.get("v14")));
  }
}
