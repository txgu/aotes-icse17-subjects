import org.javelus.aotes.executor.*;
@Defined({"this.log","this.agent.keys.elementData","this.agent","this.agent.keys","this.agent.keys.modCount"})
public class AOTES_org_apache_sshd_agent_unix_AgentServer {
  @IM(clazz="org.apache.sshd.agent.unix.AgentServer", name="<init>", desc="()V",definedOutput={"this.log","this.agent.keys.elementData","this.agent.keys","this.agent","this.agent.keys.modCount"},delta={"this.log","this.agent.keys.elementData","this.agent.keys","this.agent","this.agent.keys.modCount"})
  static void _init_36199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v1", vm.get("v0"));
    vm.put("this", om.getTarget());
    vm.put("v2", om.popField(vm.get("this"), "log"));
    vm.put("v3", (int)0);
    vm.put("v4", om.popField(vm.get("this"), "agent"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popField(vm.get("v5"), "keys"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.popField(vm.get("v7"), "modCount"));
    vm.put("v9", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("this")));
    vm.put("v10", em.op("org.slf4j.LoggerFactory.getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;").eval(vm.get("v9")));
    vm.put("v11", om.popField(vm.get("v7"), "elementData"));
    if (!em.eq(vm.get("v10"), (vm.get("v2")))) e.abort("Inconsistent value for \"v10\": " + vm.get("v10") + " ne " + (vm.get("v2")));
    if (!em.eq(vm.get("v3"), (vm.get("v8")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v8")));
    if (!em.eq(vm.get("v1"), (vm.get("v11")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v11")));
  }
}
