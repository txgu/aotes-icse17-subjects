import org.javelus.aotes.executor.*;
@Defined({"this.this$0.innerFinished"})
public class AOTES_org_apache_sshd_agent_unix_AgentServerProxy$AgentServerProxy$1 {
  @IM(clazz="org.apache.sshd.agent.unix.AgentServerProxy$1", name="run", desc="()V",definedOutput={"this.this$0.innerFinished"},delta={"this.this$0.innerFinished"})
  static void run57599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "this$0"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("v1"), "closed"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.popField(vm.get("v1"), "innerFinished"));
    vm.put("v5", (int)1);
    vm.put("v6", (int)0);
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    if (!em.ne(vm.get("v3"), vm.get("v6"))) e.abort(String.format("!((v3=%s) ne (v6=%s))", vm.get("v3"), vm.get("v6")));
  }
}
