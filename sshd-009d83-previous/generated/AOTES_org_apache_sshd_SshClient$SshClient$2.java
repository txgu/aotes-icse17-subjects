import org.javelus.aotes.executor.*;
@Defined({"this.this$0.ioServiceFactory","this.this$0.connector","this.this$0"})
public class AOTES_org_apache_sshd_SshClient$SshClient$2 {
  @IM(clazz="org.apache.sshd.SshClient$2", name="run", desc="()V",definedOutput={"this.this$0.ioServiceFactory","this.this$0.connector"},delta={"this.this$0.ioServiceFactory","this.this$0.connector"})
  @DG(pred={"_init_3199"})
  static void run76699(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.getField(vm.get("this"), "this$0"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", (int)0);
    vm.put("v4", om.getField(vm.get("v2"), "shutdownExecutor"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popField(vm.get("v2"), "ioServiceFactory"));
    vm.put("v7", om.popField(vm.get("v2"), "connector"));
    if (!em.eq(vm.get("v0"), (vm.get("v6")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v6")));
    if (!em.eq(vm.get("v0"), (vm.get("v7")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v7")));
    if (!em.eq(vm.get("v5"), vm.get("v3"))) e.abort(String.format("!((v5=%s) eq (v3=%s))", vm.get("v5"), vm.get("v3")));
  }
  @IM(clazz="org.apache.sshd.SshClient$2", name="<init>", desc="(Lorg/apache/sshd/SshClient;)V",definedOutput={"this.this$0"},delta={"this.this$0"})
  @DG(succ={"run76698","run76699"})
  static void _init_3199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "this$0"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.SshClient$2", name="run", desc="()V",definedOutput={"this.this$0.ioServiceFactory","this.this$0.connector"},delta={"this.this$0.ioServiceFactory","this.this$0.connector"})
  @DG(pred={"_init_3199"})
  static void run76698(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", null);
    vm.put("v1", om.getField(vm.get("this"), "this$0"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", (int)0);
    vm.put("v4", om.getField(vm.get("v2"), "executor"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v2"), "shutdownExecutor"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.popField(vm.get("v2"), "connector"));
    vm.put("v9", om.popField(vm.get("v2"), "ioServiceFactory"));
    if (!em.eq(vm.get("v0"), (vm.get("v8")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v8")));
    if (!em.eq(vm.get("v0"), (vm.get("v9")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v9")));
    if (!em.eq(vm.get("v5"), vm.get("v0"))) e.abort(String.format("!((v5=%s) eq (v0=%s))", vm.get("v5"), vm.get("v0")));
    if (!em.ne(vm.get("v7"), vm.get("v3"))) e.abort(String.format("!((v7=%s) ne (v3=%s))", vm.get("v7"), vm.get("v3")));
  }
}
