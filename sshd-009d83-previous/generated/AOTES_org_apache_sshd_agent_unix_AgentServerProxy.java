import org.javelus.aotes.executor.*;
@Defined({"this.closed"})
public class AOTES_org_apache_sshd_agent_unix_AgentServerProxy {
  @IM(clazz="org.apache.sshd.agent.unix.AgentServerProxy", name="close", desc="()V",revertedInput={"this.closed"},definedOutput={"this.closed"})
  static void close11679(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getStatic("java.lang.Throwable.SUPPRESSED_SENTINEL"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "closed"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("this"), "authSocket"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "handle"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "log"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getStatic("java.lang.Throwable.UNASSIGNED_STACK"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", (int)0);
    vm.put("v13", null);
    vm.put("v14", (long)0L);
    vm.put("v15", em.op("lcmp").eval(vm.get("v7"),vm.get("v14")));
    vm.put("v16", em.op("org.slf4j.Logger.isDebugEnabled()Z").eval(vm.get("v9")));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    om.revertField(vm.get("this"), "closed");
    vm.put("v17", om.getField(vm.get("this"), "closed"));
    if (!em.eq(vm.get("v16"), vm.get("v12"))) e.abort(String.format("!((v16=%s) eq (v12=%s))", vm.get("v16"), vm.get("v12")));
    if (!em.ne(vm.get("v5"), vm.get("v13"))) e.abort(String.format("!((v5=%s) ne (v13=%s))", vm.get("v5"), vm.get("v13")));
    if (!em.eq(vm.get("v15"), vm.get("v12"))) e.abort(String.format("!((v15=%s) eq (v12=%s))", vm.get("v15"), vm.get("v12")));
    if (!em.eq(vm.get("v17"), vm.get("v12"))) e.abort(String.format("!((v17=%s) eq (v12=%s))", vm.get("v17"), vm.get("v12")));
    if (!em.ne(vm.get("v11"), vm.get("v13"))) e.abort(String.format("!((v11=%s) ne (v13=%s))", vm.get("v11"), vm.get("v13")));
    if (!em.eq(vm.get("v5"), vm.get("v13"))) e.abort(String.format("!((v5=%s) eq (v13=%s))", vm.get("v5"), vm.get("v13")));
  }
  @IM(clazz="org.apache.sshd.agent.unix.AgentServerProxy", name="close", desc="()V",revertedInput={"this.closed"},definedOutput={"this.closed"})
  static void close11690(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "closed"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "handle"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "authSocket"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "log"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", null);
    vm.put("v9", (long)0L);
    vm.put("v10", (int)0);
    vm.put("v11", em.op("lcmp").eval(vm.get("v3"),vm.get("v9")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    om.revertField(vm.get("this"), "closed");
    vm.put("v12", om.getField(vm.get("this"), "closed"));
    if (!em.eq(vm.get("v11"), vm.get("v10"))) e.abort(String.format("!((v11=%s) eq (v10=%s))", vm.get("v11"), vm.get("v10")));
    if (!em.eq(vm.get("v12"), vm.get("v10"))) e.abort(String.format("!((v12=%s) eq (v10=%s))", vm.get("v12"), vm.get("v10")));
    if (!em.eq(vm.get("v5"), vm.get("v8"))) e.abort(String.format("!((v5=%s) eq (v8=%s))", vm.get("v5"), vm.get("v8")));
  }
  @IM(clazz="org.apache.sshd.agent.unix.AgentServerProxy", name="close", desc="()V",revertedInput={"this.closed"},definedOutput={"this.closed"})
  static void close11670(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "closed"));
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "log"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "authSocket"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("java.lang.Throwable.UNASSIGNED_STACK"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getStatic("java.lang.Throwable.SUPPRESSED_SENTINEL"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("this"), "handle"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", (int)0);
    vm.put("v13", null);
    vm.put("v14", (long)0L);
    vm.put("v15", em.op("org.slf4j.Logger.isDebugEnabled()Z").eval(vm.get("v3")));
    vm.put("v16", em.op("lcmp").eval(vm.get("v11"),vm.get("v14")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    om.revertField(vm.get("this"), "closed");
    vm.put("v17", om.getField(vm.get("this"), "closed"));
    if (!em.eq(vm.get("v5"), vm.get("v13"))) e.abort(String.format("!((v5=%s) eq (v13=%s))", vm.get("v5"), vm.get("v13")));
    if (!em.ne(vm.get("v5"), vm.get("v13"))) e.abort(String.format("!((v5=%s) ne (v13=%s))", vm.get("v5"), vm.get("v13")));
    if (!em.eq(vm.get("v17"), vm.get("v12"))) e.abort(String.format("!((v17=%s) eq (v12=%s))", vm.get("v17"), vm.get("v12")));
    if (!em.eq(vm.get("v15"), vm.get("v12"))) e.abort(String.format("!((v15=%s) eq (v12=%s))", vm.get("v15"), vm.get("v12")));
    if (!em.eq(vm.get("v7"), vm.get("v13"))) e.abort(String.format("!((v7=%s) eq (v13=%s))", vm.get("v7"), vm.get("v13")));
    if (!em.eq(vm.get("v16"), vm.get("v12"))) e.abort(String.format("!((v16=%s) eq (v12=%s))", vm.get("v16"), vm.get("v12")));
  }
}
