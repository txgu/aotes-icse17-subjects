import java.io.IOException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;

import org.apache.sshd.agent.SshAgent.Pair;
import org.apache.sshd.agent.local.AgentImpl;
import org.javelus.aotes.asm.Options;
import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;

@SuppressWarnings("unchecked")
public class AgentImplTest {

    static class DummyPublicKey implements PublicKey {

        /**
         * 
         */
        private static final long serialVersionUID = 1L;
        private String algorithm;

        public DummyPublicKey(String algorithm) {
            this.algorithm = algorithm;
        }

        public String toString() {
            return algorithm;
        }

        @Override
        public String getAlgorithm() {
            return algorithm;
        }

        @Override
        public String getFormat() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public byte[] getEncoded() {
            // TODO Auto-generated method stub
            return null;
        }

    }

    public static class DummyPrivateKey implements PrivateKey {
        /**
         * 
         */
        private static final long serialVersionUID = 1L;
        private String algorithm;

        public DummyPrivateKey(String algorithm) {
            this.algorithm = algorithm;
        }

        public String toString() {
            return algorithm;
        }
        @Override
        public String getAlgorithm() {
            return algorithm;
        }

        @Override
        public String getFormat() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public byte[] getEncoded() {
            // TODO Auto-generated method stub
            return null;
        }

    }

    public static class Test1 extends AOTESTestCase<AgentImpl> {

        @Override
        protected AgentImpl buildInternal() {
            AgentImpl agent = new AgentImpl();
            return agent;
        }

        @Override
        protected void checkInternal(AgentImpl updated) throws Exception {
            assertFieldEquals(readField(updated, "open"), "value", 1);

            List<Pair<KeyPair, String>> keys = (List<Pair<KeyPair, String>>) readField(updated, "keys");
            assertTrue(keys != null);
            assertTrue(keys.size() == 0);
        }

    }

    public static class Test2 extends AOTESTestCase<AgentImpl> {

        @Override
        protected AgentImpl buildInternal() {
            AgentImpl agent = new AgentImpl();
            try {
                agent.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            return agent;
        }

        @Override
        protected void checkInternal(AgentImpl updated) throws Exception {
            assertFieldEquals(readField(updated, "open"), "value", 0);
            List<Pair<KeyPair, String>> keys = (List<Pair<KeyPair, String>>) readField(updated, "keys");
            assertTrue(keys != null);
            assertTrue(keys.size() == 0);
        }

    }

    public static class Test3 extends AOTESTestCase<AgentImpl> {

        KeyPair keyPair;

        @Override
        protected AgentImpl buildInternal() {
            AgentImpl agent = new AgentImpl();
            keyPair = new KeyPair(new DummyPublicKey("pub1"), new DummyPrivateKey("pri1"));
            try {
                agent.addIdentity(keyPair, "dummy key pair");
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            return agent;
        }

        @Override
        protected void checkInternal(AgentImpl updated) throws Exception {
            // TODO Auto-generated method stub
            assertFieldEquals(readField(updated, "open"), "value", 1);

            List<Pair<KeyPair, String>> keys = (List<Pair<KeyPair, String>>) readField(updated, "keys");
            assertTrue(keys != null);
            assertTrue(keys.size() == 1);
            Pair<KeyPair, String> pair = keys.get(0);
            assertTrue(pair.getFirst() == keyPair);
        }

    }

    public static class Test4 extends AOTESTestCase<AgentImpl> {
        KeyPair keyPair;

        @Override
        protected AgentImpl buildInternal() {
            AgentImpl agent = new AgentImpl();
            keyPair = new KeyPair(new DummyPublicKey("pub1"), new DummyPrivateKey("pri1"));

            try {
                agent.addIdentity(keyPair, "dummy key pair");
                agent.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            return agent;
        }

        @Override
        protected void checkInternal(AgentImpl updated) throws Exception {
            // TODO Auto-generated method stub
            assertFieldEquals(readField(updated, "open"), "value", 0);

            List<Pair<KeyPair, String>> keys = (List<Pair<KeyPair, String>>) readField(updated, "keys");
            assertTrue(keys != null);
            assertTrue(keys.size() == 0);
        }

    }

    public static class Test5 extends AOTESTestCase<AgentImpl> {
        KeyPair keyPair;

        @Override
        protected AgentImpl buildInternal() {
            AgentImpl agent = new AgentImpl();
            keyPair = new KeyPair(new DummyPublicKey("pub1"), new DummyPrivateKey("pri1"));

            try {
                agent.addIdentity(keyPair, "dummy key pair");
                agent.addIdentity(keyPair, "dummy key pair");
                agent.addIdentity(keyPair, "dummy key pair");
                agent.addIdentity(keyPair, "dummy key pair");
                agent.addIdentity(keyPair, "dummy key pair");

                agent.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            return agent;
        }

        @Override
        protected void checkInternal(AgentImpl updated) throws Exception {
            assertFieldEquals(readField(updated, "open"), "value", 0);

            List<Pair<KeyPair, String>> keys = (List<Pair<KeyPair, String>>) readField(updated, "keys");
            assertTrue(keys != null);
            assertTrue(keys.size() == 0);
        }

    }
    
    public static void main(String[] args) throws Exception {
        Options.setDebug("executor", false);
        AOTESTestRunner.run(
                Test1.class,
                Test2.class,
                Test3.class,
                Test4.class,
                Test5.class
                );
    }
}
