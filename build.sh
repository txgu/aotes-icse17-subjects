#! /bin/bash

DIR=$(pushd $(dirname $BASH_SOURCE[0]) > /dev/null && pwd && popd > /dev/null)

BUILD=${DIR}/build.xml

for BM in $(cat ${DIR}/subjects.txt)
do
    PROJ=${BM%%-*}
    BM_DIR=${DIR}/$BM
    cp $BUILD $BM_DIR
    mkdir ${BM_DIR}/lib > /dev/null
    ant -f $BM_DIR/build.xml -Dbm=$BM  -Dextra.lib.dir=${DIR}/${PROJ}-lib

    BM=$BM-previous
    BM_DIR=${DIR}/$BM
    cp $BUILD $BM_DIR
    mkdir ${BM_DIR}/lib > /dev/null
    ant -f $BM_DIR/build.xml -Dbm=$BM  -Dextra.lib.dir=${DIR}/${PROJ}-lib
done
