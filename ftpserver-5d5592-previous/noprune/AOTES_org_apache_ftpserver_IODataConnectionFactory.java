import org.javelus.aotes.executor.*;
@Defined({"this.passive","org.apache.ftpserver.IODataConnectionFactory.LOG","this.serverContext","this.port","this.serverControlAddress","this.isZip","this.secure","this.session","this.requestTime"})
public class AOTES_org_apache_ftpserver_IODataConnectionFactory {
  @IM(clazz="org.apache.ftpserver.IODataConnectionFactory", name="setSecure", desc="(Z)V",definedOutput={"this.secure"},delta={"this.secure"})
  static void setSecure5199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "secure"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.ftpserver.IODataConnectionFactory", name="closeDataConnection", desc="()V",definedOutput={"this.requestTime"},delta={"this.requestTime"})
  static void closeDataConnection4695(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "dataSoc"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "servSoc"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.popField(vm.get("this"), "requestTime"));
    vm.put("v5", (long)0L);
    vm.put("v6", null);
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    if (!em.eq(vm.get("v1"), vm.get("v6"))) e.abort(String.format("!((v1=%s) eq (v6=%s))", vm.get("v1"), vm.get("v6")));
    if (!em.eq(vm.get("v3"), vm.get("v6"))) e.abort(String.format("!((v3=%s) eq (v6=%s))", vm.get("v3"), vm.get("v6")));
  }
  @IM(clazz="org.apache.ftpserver.IODataConnectionFactory", name="<clinit>", desc="()V",definedOutput={"org.apache.ftpserver.IODataConnectionFactory.LOG"},delta={"org.apache.ftpserver.IODataConnectionFactory.LOG"})
  static void _clinit_4499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", om.popStatic("org.apache.ftpserver.IODataConnectionFactory.LOG"));
    vm.put("v1", org.apache.ftpserver.IODataConnectionFactory.class);
    vm.put("v2", em.op("org.slf4j.LoggerFactory.getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;").eval(vm.get("v1")));
    if (!em.eq(vm.get("v2"), (vm.get("v0")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v0")));
  }
  @IM(clazz="org.apache.ftpserver.IODataConnectionFactory", name="dispose", desc="()V",definedOutput={"this.requestTime"},delta={"this.requestTime"})
  static void dispose6796(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "servSoc"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "dataSoc"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.popField(vm.get("this"), "requestTime"));
    vm.put("v5", (long)0L);
    vm.put("v6", null);
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    if (!em.eq(vm.get("v1"), vm.get("v6"))) e.abort(String.format("!((v1=%s) eq (v6=%s))", vm.get("v1"), vm.get("v6")));
    if (!em.eq(vm.get("v3"), vm.get("v6"))) e.abort(String.format("!((v3=%s) eq (v6=%s))", vm.get("v3"), vm.get("v6")));
  }
  @IM(clazz="org.apache.ftpserver.IODataConnectionFactory", name="setServerControlAddress", desc="(Ljava/net/InetAddress;)V",definedOutput={"this.serverControlAddress"},delta={"this.serverControlAddress"})
  static void setServerControlAddress3199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "serverControlAddress"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.ftpserver.IODataConnectionFactory", name="<init>", desc="(Lorg/apache/ftpserver/interfaces/FtpServerContext;Lorg/apache/ftpserver/interfaces/FtpIoSession;)V",definedOutput={"this.passive","this.serverContext","this.port","this.isZip","this.session","this.secure","this.requestTime"},delta={"this.passive","this.serverContext","this.port","this.isZip","this.session","this.secure","this.requestTime"})
  static void _init_5399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "serverContext"));
    vm.put("v1", om.popField(vm.get("this"), "passive"));
    vm.put("v2", (int)0);
    vm.put("v3", om.popField(vm.get("this"), "requestTime"));
    vm.put("v4", (long)0L);
    vm.put("v5", om.popField(vm.get("this"), "port"));
    vm.put("v6", om.popField(vm.get("this"), "session"));
    vm.put("v7", om.popField(vm.get("this"), "secure"));
    vm.put("v8", om.popField(vm.get("this"), "isZip"));
    if (!em.eq(vm.get("v4"), (vm.get("v3")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v3")));
    if (!em.eq(vm.get("v2"), (vm.get("v1")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v1")));
    if (!em.eq(vm.get("v2"), (vm.get("v8")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v8")));
    vm.put("param1", vm.get("v6"));
    vm.put("param0", vm.get("v0"));
    if (!em.eq(vm.get("v2"), (vm.get("v7")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v7")));
    if (!em.eq(vm.get("v2"), (vm.get("v5")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v5")));
  }
  @IM(clazz="org.apache.ftpserver.IODataConnectionFactory", name="setZipMode", desc="(Z)V",definedOutput={"this.isZip"},delta={"this.isZip"})
  static void setZipMode2199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "isZip"));
    vm.put("param0", vm.get("v0"));
  }
}
