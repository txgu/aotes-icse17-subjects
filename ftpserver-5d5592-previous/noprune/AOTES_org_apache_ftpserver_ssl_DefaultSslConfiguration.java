import org.javelus.aotes.executor.*;
@Defined({"this.keystoreAlgorithm","this.trustStoreAlgorithm","this.keystorePass","this.enabledCipherSuites","this.keystoreType","this.trustStorePass","this.keyPass","this.keyAlias","this.sslProtocol","this.trustStoreFile","this.keystoreFile","this.trustStoreType"})
public class AOTES_org_apache_ftpserver_ssl_DefaultSslConfiguration {
  @IM(clazz="org.apache.ftpserver.ssl.DefaultSslConfiguration", name="setKeystorePassword", desc="(Ljava/lang/String;)V",definedOutput={"this.keystorePass"},delta={"this.keystorePass"})
  static void setKeystorePassword99(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "keystorePass"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.ftpserver.ssl.DefaultSslConfiguration", name="setTruststoreFile", desc="(Ljava/io/File;)V",definedOutput={"this.trustStoreFile"},delta={"this.trustStoreFile"})
  static void setTruststoreFile1899(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "trustStoreFile"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.ftpserver.ssl.DefaultSslConfiguration", name="setTruststoreAlgorithm", desc="(Ljava/lang/String;)V",definedOutput={"this.trustStoreAlgorithm"},delta={"this.trustStoreAlgorithm"})
  static void setTruststoreAlgorithm1599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "trustStoreAlgorithm"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.ftpserver.ssl.DefaultSslConfiguration", name="setSslProtocol", desc="(Ljava/lang/String;)V",definedOutput={"this.sslProtocol"},delta={"this.sslProtocol"})
  static void setSslProtocol6499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "sslProtocol"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.ftpserver.ssl.DefaultSslConfiguration", name="setKeystoreAlgorithm", desc="(Ljava/lang/String;)V",definedOutput={"this.keystoreAlgorithm"},delta={"this.keystoreAlgorithm"})
  static void setKeystoreAlgorithm6599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "keystoreAlgorithm"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.ftpserver.ssl.DefaultSslConfiguration", name="setKeyPassword", desc="(Ljava/lang/String;)V",definedOutput={"this.keyPass"},delta={"this.keyPass"})
  static void setKeyPassword8099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "keyPass"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.ftpserver.ssl.DefaultSslConfiguration", name="setKeystoreFile", desc="(Ljava/io/File;)V",definedOutput={"this.keystoreFile"},delta={"this.keystoreFile"})
  static void setKeystoreFile799(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "keystoreFile"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.ftpserver.ssl.DefaultSslConfiguration", name="setTruststoreType", desc="(Ljava/lang/String;)V",definedOutput={"this.trustStoreType"},delta={"this.trustStoreType"})
  static void setTruststoreType4899(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "trustStoreType"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.ftpserver.ssl.DefaultSslConfiguration", name="setEnabledCipherSuites", desc="([Ljava/lang/String;)V",definedOutput={"this.enabledCipherSuites"},delta={"this.enabledCipherSuites"})
  static void setEnabledCipherSuites6699(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "enabledCipherSuites"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.ftpserver.ssl.DefaultSslConfiguration", name="setTruststorePassword", desc="(Ljava/lang/String;)V",definedOutput={"this.trustStorePass"},delta={"this.trustStorePass"})
  static void setTruststorePassword1099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "trustStorePass"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.ftpserver.ssl.DefaultSslConfiguration", name="setKeystoreType", desc="(Ljava/lang/String;)V",definedOutput={"this.keystoreType"},delta={"this.keystoreType"})
  static void setKeystoreType6899(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "keystoreType"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.ftpserver.ssl.DefaultSslConfiguration", name="setKeyAlias", desc="(Ljava/lang/String;)V",definedOutput={"this.keyAlias"},delta={"this.keyAlias"})
  static void setKeyAlias4399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "keyAlias"));
    vm.put("param0", vm.get("v0"));
  }
}
