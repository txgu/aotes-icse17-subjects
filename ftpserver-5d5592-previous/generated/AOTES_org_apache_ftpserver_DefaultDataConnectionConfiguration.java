import org.javelus.aotes.executor.*;
@Defined({"this.maxIdleTimeMillis"})
public class AOTES_org_apache_ftpserver_DefaultDataConnectionConfiguration {
  @IM(clazz="org.apache.ftpserver.DefaultDataConnectionConfiguration", name="setIdleTime", desc="(I)V",definedOutput={"this.maxIdleTimeMillis"},delta={"this.maxIdleTimeMillis"})
  static void setIdleTime2799(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "maxIdleTimeMillis"));
    vm.put("v1", (int)1000);
    vm.put("v2", vm.get("v0"));
    vm.put("param0", em.op("idiv").eval(vm.get("v2"),vm.get("v1")));
  }
}
