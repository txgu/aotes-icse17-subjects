#! /usr/bin/python

import sys, traceback, re

TEST_NAME_PATTERN=re.compile(r'^Run test: ([a-zA-Z]+) in ([-a-zA-Z0-9]+)$')
TEST_RESULT_PATTERN=re.compile(r'^([0-9]+)\t([0-9]+)$')

class TestResult(object):
    pass

def run(logfile_name):

    TOTAL_TEST = 0
    TOTAL_SUCC = 0
    TEST_RESULTS = []
    with open(logfile_name, 'r') as logfile:
        for line in [l.strip() for l in logfile.readlines()]:
            m = TEST_NAME_PATTERN.match(line)
            if m:
                current_test = m.group(2)
                continue

            m = TEST_RESULT_PATTERN.match(line)
            if m:
                total = int(m.group(1))
                fail  = int(m.group(2))
                succ  = total - fail
                TOTAL_TEST = TOTAL_TEST + total
                TOTAL_SUCC = TOTAL_SUCC + succ
                result = TestResult()
                result.test = current_test
                result.total = total
                result.succ = succ
                TEST_RESULTS.append(result)

    maxlen = 0
    for result in TEST_RESULTS:
        l = len(result.test)
        if l > maxlen:
            maxlen = l

    fmt = '%%%ds\t%%d\t%%d\t%%d' % (maxlen,)
    for result in TEST_RESULTS:
        print(fmt % (result.test, result.total, result.succ, result.total - result.succ))

    print(fmt % ('Total', TOTAL_TEST, TOTAL_SUCC, TOTAL_TEST - TOTAL_SUCC))

if __name__ == '__main__':
    try:
        run(sys.argv[1])
    except Exception as e:
        traceback.print_exc(e)
        exit(1)
