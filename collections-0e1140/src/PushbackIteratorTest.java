import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.iterators.PushbackIterator;
import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;

public class PushbackIteratorTest {
    
    static String[] testArray = { "a", "b", "c" };


    static List<String> testList = null;

    static {
        testList = new ArrayList<String>(Arrays.asList(testArray));
    }

    public static class Test1 extends AOTESTestCase<PushbackIterator> {

        @Override
        protected PushbackIterator buildInternal() {
            return PushbackIterator.pushbackIterator(testList.iterator());
        
        }

        @Override
        protected void checkInternal(PushbackIterator updated) throws Exception {
            assertEquals("a", updated.next());
            assertEquals("b", updated.next());
            assertEquals("c", updated.next());
            assertTrue(!updated.hasNext());
        }
        
    }
    
    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        AOTESTestRunner.run(
                Test1.class
                );
    }
}
