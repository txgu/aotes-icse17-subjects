import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.iterators.IteratorChain;
import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;

public class IteratorChainTest {
    
    protected static String[] testArray = {
            "One", "Two", "Three", "Four", "Five", "Six"
        };



    static List<String> list1 = null;
    static List<String> list2 = null;
    static List<String> list3 = null;

    static {
        list1 = new ArrayList<String>();
        list1.add("One");
        list1.add("Two");
        list1.add("Three");
        list2 = new ArrayList<String>();
        list2.add("Four");
        list3 = new ArrayList<String>();
        list3.add("Five");
        list3.add("Six");
    }

    public static class Test1 extends AOTESTestCase<IteratorChain> {

        @Override
        protected IteratorChain buildInternal() {
            final IteratorChain<String> chain = new IteratorChain<String>();
            chain.addIterator(list1.iterator());
            chain.addIterator(list2.iterator());
            chain.addIterator(list3.iterator());
            return chain;
        
        }

        @Override
        protected void checkInternal(IteratorChain updated) throws Exception {
            for (final String testValue : testArray) {
                final Object iterValue = updated.next();
                assertEquals(testValue, iterValue );
            }
       }
        
    }
    
    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        AOTESTestRunner.run(
                Test1.class
                );
    }
}
