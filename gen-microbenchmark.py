#! /usr/bin/python

import sys, traceback, re

TEST_NAME_PATTERN=re.compile(r'^Run test: ([a-zA-Z]+) in ([-a-zA-Z0-9]+)$')
TEST_RESULT_PATTERN=re.compile(r'^([0-9]+)\t([0-9]+)$')

class TestResult(object):
    pass


def add_test(group, t):
    a1 = group.get(t.type)
    if not a1:
        a1 = {}
        group[t.type] = a1

    a2 = a1.get(t.class_name)
    if not a2:
        a2 = []
        a1[t.class_name] = a2

    a2.append(t.records)
    return a2

def cat(list_of_list):
    a = []
    for list in list_of_list:
        a = a + list

    return a

def gen_header(length):
    hfmt = '\t'.join(['type%d', 'class%d', 'result%d', 'im_count%d', 'elements%d', 'history%d', 'time%d'])

    a = []
    for i in range(0, length):
        a = a + [i] * 7

    return ((hfmt*length) % tuple(a)) + '\n'


def run(logfile_name):

    test_results = {}
    test_array = []
    with open(logfile_name, 'r') as logfile:
        for line in [l.strip() for l in logfile.readlines()]:
            if line.startswith('##:'):
                rec = line[3:].split()
                t = TestResult()
                t.type = rec[0]
                t.class_name = rec[1]
                #if t.type == 'noprune':
                #    rec[6] = str(float(rec[6]) / 1000)
                t.records = rec
                add_test(test_results, t)
                test_array.append(t)

    maxlen = 0
    for result in test_array:
        l = len(result.class_name)
        if l > maxlen:
            maxlen = l

    fmt = '%%8s\t%%%ds\t%%s\t%%s\t%%s\t%%s\t%%s' % (maxlen,)

    zipped = zip(*test_results['noprune'].values())
    with open('noprune.dat', 'w') as f:
        f.write(gen_header(len(zipped[0])))
        for row in zipped:
            f.write(fmt*len(row) % tuple(cat(row)) + '\n')

    zipped = zip(*test_results['prune'].values())
    with open('prune.dat', 'w') as f:
        f.write(gen_header(len(zipped[0])))
        for row in zipped:
            f.write(fmt*len(row) % tuple(cat(row)) + '\n')




if __name__ == '__main__':
    try:
        run(sys.argv[1])
    except Exception as e:
        traceback.print_exc(e)
        exit(1)
