import org.javelus.aotes.executor.*;
@Defined({"this.reservedPorts[*]"})
public class AOTES_org_apache_ftpserver_impl_PassivePorts {
  @IM(clazz="org.apache.ftpserver.impl.PassivePorts", name="releasePort", desc="(I)V",definedOutput={"this.reservedPorts[*]"},delta={"this.reservedPorts[*]"})
  @DG(pred={"_init_814","_init_837","_init_866","_init_842","_init_857","_init_839","_init_840","_init_870","_init_826","_init_898","_init_813","_init_844","_init_892","_init_816","_init_862","_init_871","_init_886","_init_863","_init_853","_init_804","_init_846","_init_850","_init_868","_init_893","_init_869","_init_883","_init_899","_init_854","_init_890","_init_855","_init_827","_init_848","_init_881","_init_891","_init_818","_init_864","_init_845","_init_875","_init_835","_init_882","_init_897","_init_877","_init_887","_init_874","_init_831","_init_895","_init_817","_init_885","_init_873"})
  static void releasePort3101(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)3);
    vm.put("v1", (int)5);
    vm.put("v2", (int)1);
    vm.put("v3", (int)4);
    vm.put("v4", (int)0);
    vm.put("v5", (int)2);
    vm.put("v6", om.getField(vm.get("this"), "passivePorts"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayElement(vm.get("v7"), vm.get("v4")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getArrayElement(vm.get("v7"), vm.get("v2")));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getArrayElement(vm.get("v7"), vm.get("v1")));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("this"), "reservedPorts"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.getArrayElement(vm.get("v7"), vm.get("v5")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.getArrayLength(vm.get("v7")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", om.getArrayElement(vm.get("v7"), vm.get("v3")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.getArrayElement(vm.get("v7"), vm.get("v0")));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.popArrayElement(vm.get("v15"), vm.get("v1")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.ne(vm.get("v9"), vm.get("param0"))) e.abort(String.format("!((v9=%s) ne (param0=%s))", vm.get("v9"), vm.get("param0")));
    if (!em.ne(vm.get("v11"), vm.get("param0"))) e.abort(String.format("!((v11=%s) ne (param0=%s))", vm.get("v11"), vm.get("param0")));
    if (!em.lt(vm.get("v4"), vm.get("v19"))) e.abort(String.format("!((v4=%s) lt (v19=%s))", vm.get("v4"), vm.get("v19")));
    if (!em.ne(vm.get("v23"), vm.get("param0"))) e.abort(String.format("!((v23=%s) ne (param0=%s))", vm.get("v23"), vm.get("param0")));
    if (!em.lt(vm.get("v2"), vm.get("v19"))) e.abort(String.format("!((v2=%s) lt (v19=%s))", vm.get("v2"), vm.get("v19")));
    if (!em.lt(vm.get("v1"), vm.get("v19"))) e.abort(String.format("!((v1=%s) lt (v19=%s))", vm.get("v1"), vm.get("v19")));
    if (!em.ne(vm.get("v21"), vm.get("param0"))) e.abort(String.format("!((v21=%s) ne (param0=%s))", vm.get("v21"), vm.get("param0")));
    if (!em.lt(vm.get("v5"), vm.get("v19"))) e.abort(String.format("!((v5=%s) lt (v19=%s))", vm.get("v5"), vm.get("v19")));
    if (!em.lt(vm.get("v0"), vm.get("v19"))) e.abort(String.format("!((v0=%s) lt (v19=%s))", vm.get("v0"), vm.get("v19")));
    if (!em.eq(vm.get("v13"), vm.get("param0"))) e.abort(String.format("!((v13=%s) eq (param0=%s))", vm.get("v13"), vm.get("param0")));
    if (!em.lt(vm.get("v3"), vm.get("v19"))) e.abort(String.format("!((v3=%s) lt (v19=%s))", vm.get("v3"), vm.get("v19")));
    if (!em.ne(vm.get("v17"), vm.get("param0"))) e.abort(String.format("!((v17=%s) ne (param0=%s))", vm.get("v17"), vm.get("param0")));
  }
  @IM(clazz="org.apache.ftpserver.impl.PassivePorts", name="releasePort", desc="(I)V",definedOutput={"this.reservedPorts[*]"},delta={"this.reservedPorts[*]"})
  @DG(pred={"_init_814","_init_837","_init_866","_init_842","_init_857","_init_839","_init_840","_init_870","_init_826","_init_898","_init_813","_init_844","_init_892","_init_816","_init_862","_init_871","_init_886","_init_863","_init_853","_init_804","_init_846","_init_850","_init_868","_init_893","_init_869","_init_883","_init_899","_init_854","_init_890","_init_855","_init_827","_init_848","_init_881","_init_891","_init_818","_init_864","_init_845","_init_875","_init_835","_init_882","_init_897","_init_877","_init_887","_init_874","_init_831","_init_895","_init_817","_init_885","_init_873"})
  static void releasePort3198(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "passivePorts"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "reservedPorts"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayElement(vm.get("v3"), vm.get("v1")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getArrayLength(vm.get("v3")));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.popArrayElement(vm.get("v7"), vm.get("v1")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.lt(vm.get("v1"), vm.get("v11"))) e.abort(String.format("!((v1=%s) lt (v11=%s))", vm.get("v1"), vm.get("v11")));
    if (!em.lt(vm.get("v0"), vm.get("v11"))) e.abort(String.format("!((v0=%s) lt (v11=%s))", vm.get("v0"), vm.get("v11")));
    if (!em.eq(vm.get("v9"), vm.get("param0"))) e.abort(String.format("!((v9=%s) eq (param0=%s))", vm.get("v9"), vm.get("param0")));
    if (!em.ne(vm.get("v5"), vm.get("param0"))) e.abort(String.format("!((v5=%s) ne (param0=%s))", vm.get("v5"), vm.get("param0")));
  }
  @IM(clazz="org.apache.ftpserver.impl.PassivePorts", name="releasePort", desc="(I)V",definedOutput={"this.reservedPorts[*]"},delta={"this.reservedPorts[*]"})
  @DG(pred={"_init_814","_init_837","_init_866","_init_842","_init_857","_init_839","_init_840","_init_870","_init_826","_init_898","_init_813","_init_844","_init_892","_init_816","_init_862","_init_871","_init_886","_init_863","_init_853","_init_804","_init_846","_init_850","_init_868","_init_893","_init_869","_init_883","_init_899","_init_854","_init_890","_init_855","_init_827","_init_848","_init_881","_init_891","_init_818","_init_864","_init_845","_init_875","_init_835","_init_882","_init_897","_init_877","_init_887","_init_874","_init_831","_init_895","_init_817","_init_885","_init_873"})
  static void releasePort3108(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)2);
    vm.put("v2", (int)0);
    vm.put("v3", om.getField(vm.get("this"), "reservedPorts"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "passivePorts"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.popArrayElement(vm.get("v4"), vm.get("v1")));
    vm.put("v8", om.getArrayElement(vm.get("v6"), vm.get("v0")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getArrayElement(vm.get("v6"), vm.get("v1")));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getArrayElement(vm.get("v6"), vm.get("v2")));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getArrayLength(vm.get("v6")));
    vm.put("v15", vm.get("v14"));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.eq(vm.get("v11"), vm.get("param0"))) e.abort(String.format("!((v11=%s) eq (param0=%s))", vm.get("v11"), vm.get("param0")));
    if (!em.ne(vm.get("v9"), vm.get("param0"))) e.abort(String.format("!((v9=%s) ne (param0=%s))", vm.get("v9"), vm.get("param0")));
    if (!em.lt(vm.get("v0"), vm.get("v15"))) e.abort(String.format("!((v0=%s) lt (v15=%s))", vm.get("v0"), vm.get("v15")));
    if (!em.ne(vm.get("v13"), vm.get("param0"))) e.abort(String.format("!((v13=%s) ne (param0=%s))", vm.get("v13"), vm.get("param0")));
    if (!em.lt(vm.get("v1"), vm.get("v15"))) e.abort(String.format("!((v1=%s) lt (v15=%s))", vm.get("v1"), vm.get("v15")));
    if (!em.lt(vm.get("v2"), vm.get("v15"))) e.abort(String.format("!((v2=%s) lt (v15=%s))", vm.get("v2"), vm.get("v15")));
  }
  @IM(clazz="org.apache.ftpserver.impl.PassivePorts", name="reserveNextPort", desc="()I",revertedInput={"this.reservedPorts[*]"},definedOutput={"this.reservedPorts[*]"})
  @DG(pred={"_init_814","_init_837","_init_866","_init_842","_init_857","_init_839","_init_840","_init_870","_init_826","_init_898","_init_813","_init_844","_init_892","_init_816","_init_862","_init_871","_init_886","_init_863","_init_853","_init_804","_init_846","_init_850","_init_868","_init_893","_init_869","_init_883","_init_899","_init_854","_init_890","_init_855","_init_827","_init_848","_init_881","_init_891","_init_818","_init_864","_init_845","_init_875","_init_835","_init_882","_init_897","_init_877","_init_887","_init_874","_init_831","_init_895","_init_817","_init_885","_init_873"})
  static void reserveNextPort788(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "reservedPorts"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "passivePorts"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getArrayElement(vm.get("v5"), vm.get("v0")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayElement(vm.get("v3"), vm.get("v1")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    vm.put("v11", om.getArrayLength(vm.get("v5")));
    vm.put("v12", vm.get("v11"));
    om.revertArrayElement(vm.get("v3"), vm.get("v0"));
    vm.put("v13", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    if (!em.ne(vm.get("v9"), vm.get("v1"))) e.abort(String.format("!((v9=%s) ne (v1=%s))", vm.get("v9"), vm.get("v1")));
    if (!em.lt(vm.get("v0"), vm.get("v12"))) e.abort(String.format("!((v0=%s) lt (v12=%s))", vm.get("v0"), vm.get("v12")));
    if (!em.ne(vm.get("v7"), vm.get("v1"))) e.abort(String.format("!((v7=%s) ne (v1=%s))", vm.get("v7"), vm.get("v1")));
    if (!em.eq(vm.get("v13"), vm.get("v1"))) e.abort(String.format("!((v13=%s) eq (v1=%s))", vm.get("v13"), vm.get("v1")));
    if (!em.lt(vm.get("v1"), vm.get("v12"))) e.abort(String.format("!((v1=%s) lt (v12=%s))", vm.get("v1"), vm.get("v12")));
  }
  @IM(clazz="org.apache.ftpserver.impl.PassivePorts", name="reserveNextPort", desc="()I",revertedInput={"this.reservedPorts[*]"},definedOutput={"this.reservedPorts[*]"})
  @DG(pred={"_init_814","_init_837","_init_866","_init_842","_init_857","_init_839","_init_840","_init_870","_init_826","_init_898","_init_813","_init_844","_init_892","_init_816","_init_862","_init_871","_init_886","_init_863","_init_853","_init_804","_init_846","_init_850","_init_868","_init_893","_init_869","_init_883","_init_899","_init_854","_init_890","_init_855","_init_827","_init_848","_init_881","_init_891","_init_818","_init_864","_init_845","_init_875","_init_835","_init_882","_init_897","_init_877","_init_887","_init_874","_init_831","_init_895","_init_817","_init_885","_init_873"})
  static void reserveNextPort793(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "reservedPorts"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "passivePorts"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getArrayElement(vm.get("v5"), vm.get("v0")));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    vm.put("v9", om.getArrayLength(vm.get("v5")));
    vm.put("v10", vm.get("v9"));
    om.revertArrayElement(vm.get("v3"), vm.get("v0"));
    vm.put("v11", om.getArrayElement(vm.get("v3"), vm.get("v0")));
    if (!em.eq(vm.get("v11"), vm.get("v0"))) e.abort(String.format("!((v11=%s) eq (v0=%s))", vm.get("v11"), vm.get("v0")));
    if (!em.lt(vm.get("v0"), vm.get("v10"))) e.abort(String.format("!((v0=%s) lt (v10=%s))", vm.get("v0"), vm.get("v10")));
    if (!em.ne(vm.get("v7"), vm.get("v0"))) e.abort(String.format("!((v7=%s) ne (v0=%s))", vm.get("v7"), vm.get("v0")));
  }
  @IM(clazz="org.apache.ftpserver.impl.PassivePorts", name="releasePort", desc="(I)V",definedOutput={"this.reservedPorts[*]"},delta={"this.reservedPorts[*]"})
  @DG(pred={"_init_814","_init_837","_init_866","_init_842","_init_857","_init_839","_init_840","_init_870","_init_826","_init_898","_init_813","_init_844","_init_892","_init_816","_init_862","_init_871","_init_886","_init_863","_init_853","_init_804","_init_846","_init_850","_init_868","_init_893","_init_869","_init_883","_init_899","_init_854","_init_890","_init_855","_init_827","_init_848","_init_881","_init_891","_init_818","_init_864","_init_845","_init_875","_init_835","_init_882","_init_897","_init_877","_init_887","_init_874","_init_831","_init_895","_init_817","_init_885","_init_873"})
  static void releasePort3197(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "reservedPorts"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "passivePorts"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayLength(vm.get("v4")));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getArrayElement(vm.get("v4"), vm.get("v0")));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.popArrayElement(vm.get("v2"), vm.get("v0")));
    vm.put("param0", om.newDefaultValue("int"));
    if (!em.eq(vm.get("v8"), vm.get("param0"))) e.abort(String.format("!((v8=%s) eq (param0=%s))", vm.get("v8"), vm.get("param0")));
    if (!em.lt(vm.get("v0"), vm.get("v6"))) e.abort(String.format("!((v0=%s) lt (v6=%s))", vm.get("v0"), vm.get("v6")));
  }
}
