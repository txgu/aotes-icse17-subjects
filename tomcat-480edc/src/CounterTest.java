import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;


public class CounterTest {

    public static class Test1 extends AOTESTestCase<Counter> {

        @Override
        protected Counter buildInternal() {
            Counter c = new Counter();
            c.inc();
            c.inc();
            c.inc();
            return c;
        }

        @Override
        protected void checkInternal(Counter updated) throws Exception {
            assertTrue(updated.getCounter() == 3);
        }
        
    }
    
    public static class Test2 extends AOTESTestCase<Counter> {

        @Override
        protected Counter buildInternal() {
            Counter c = new Counter();
            c.inc();
            c.setCounter(3);
            return c;
        }

        @Override
        protected void checkInternal(Counter updated) throws Exception {
            assertTrue(updated.getCounter() == 3);
        }
        
    }
    
    public static class Test3 extends AOTESTestCase<Counter> {

        @Override
        protected Counter buildInternal() {
            Counter c = new Counter();
            return c;
        }

        @Override
        protected void checkInternal(Counter updated) throws Exception {
            assertTrue(updated.getCounter() == 0);
        }
        
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        AOTESTestRunner.run(
                Test1.class,
                Test2.class,
                Test3.class
                );
    }
}
