import org.javelus.aotes.executor.*;
@Defined({"this.className","this.allowedPluggability"})
public class AOTES_org_apache_catalina_deploy_ApplicationListener {
  @IM(clazz="org.apache.catalina.deploy.ApplicationListener", name="<init>", desc="(Ljava/lang/String;Z)V",definedOutput={"this.className","this.allowedPluggability"},delta={"this.className","this.allowedPluggability"})
  static void _init_99(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "className"));
    vm.put("v1", om.popField(vm.get("this"), "allowedPluggability"));
    vm.put("param0", vm.get("v0"));
    vm.put("param1", vm.get("v1"));
  }
}
