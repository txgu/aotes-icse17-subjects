import org.javelus.aotes.executor.*;
@Defined({"this.jmxNameBase","this.maxIdle","this.testOnCreate","this.jmxEnabled","this.testWhileIdle","this.softMinEvictableIdleTimeMillis","this.fairness","this.testOnBorrow","this.maxTotal","this.maxWaitMillis","this.testOnReturn","this.minIdle","this.numTestsPerEvictionRun","this.minEvictableIdleTimeMillis","this.lifo","this.blockWhenExhausted","this.evictionPolicyClassName","this.timeBetweenEvictionRunsMillis","this.jmxNamePrefix"})
public class AOTES_org_apache_tomcat_dbcp_pool2_impl_GenericObjectPoolConfig {
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setJmxNameBase", desc="(Ljava/lang/String;)V",definedOutput={"this.jmxNameBase"},delta={"this.jmxNameBase"})
  static void setJmxNameBase23617(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "jmxNameBase"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setMinEvictableIdleTimeMillis", desc="(J)V",definedOutput={"this.minEvictableIdleTimeMillis"},delta={"this.minEvictableIdleTimeMillis"})
  static void setMinEvictableIdleTimeMillis39782(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "minEvictableIdleTimeMillis"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setTestOnCreate", desc="(Z)V",definedOutput={"this.testOnCreate"},delta={"this.testOnCreate"})
  static void setTestOnCreate66616(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "testOnCreate"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setMaxWaitMillis", desc="(J)V",definedOutput={"this.maxWaitMillis"},delta={"this.maxWaitMillis"})
  static void setMaxWaitMillis61627(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "maxWaitMillis"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setJmxEnabled", desc="(Z)V",definedOutput={"this.jmxEnabled"},delta={"this.jmxEnabled"})
  static void setJmxEnabled41869(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "jmxEnabled"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setLifo", desc="(Z)V",definedOutput={"this.lifo"},delta={"this.lifo"})
  static void setLifo52892(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "lifo"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.GenericObjectPoolConfig", name="setMinIdle", desc="(I)V",definedOutput={"this.minIdle"},delta={"this.minIdle"})
  static void setMinIdle99927(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "minIdle"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setJmxNamePrefix", desc="(Ljava/lang/String;)V",definedOutput={"this.jmxNamePrefix"},delta={"this.jmxNamePrefix"})
  static void setJmxNamePrefix43589(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "jmxNamePrefix"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setEvictionPolicyClassName", desc="(Ljava/lang/String;)V",definedOutput={"this.evictionPolicyClassName"},delta={"this.evictionPolicyClassName"})
  static void setEvictionPolicyClassName61701(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "evictionPolicyClassName"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setSoftMinEvictableIdleTimeMillis", desc="(J)V",definedOutput={"this.softMinEvictableIdleTimeMillis"},delta={"this.softMinEvictableIdleTimeMillis"})
  static void setSoftMinEvictableIdleTimeMillis29073(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "softMinEvictableIdleTimeMillis"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setTestOnBorrow", desc="(Z)V",definedOutput={"this.testOnBorrow"},delta={"this.testOnBorrow"})
  static void setTestOnBorrow53055(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "testOnBorrow"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setTimeBetweenEvictionRunsMillis", desc="(J)V",definedOutput={"this.timeBetweenEvictionRunsMillis"},delta={"this.timeBetweenEvictionRunsMillis"})
  static void setTimeBetweenEvictionRunsMillis4367(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "timeBetweenEvictionRunsMillis"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setTestWhileIdle", desc="(Z)V",definedOutput={"this.testWhileIdle"},delta={"this.testWhileIdle"})
  static void setTestWhileIdle29264(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "testWhileIdle"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.GenericObjectPoolConfig", name="setMaxIdle", desc="(I)V",definedOutput={"this.maxIdle"},delta={"this.maxIdle"})
  static void setMaxIdle78186(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "maxIdle"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setNumTestsPerEvictionRun", desc="(I)V",definedOutput={"this.numTestsPerEvictionRun"},delta={"this.numTestsPerEvictionRun"})
  static void setNumTestsPerEvictionRun38652(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "numTestsPerEvictionRun"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.GenericObjectPoolConfig", name="<init>", desc="()V",definedOutput={"this.jmxNameBase","this.maxIdle","this.testOnCreate","this.jmxEnabled","this.testWhileIdle","this.fairness","this.softMinEvictableIdleTimeMillis","this.testOnBorrow","this.maxTotal","this.maxWaitMillis","this.testOnReturn","this.minIdle","this.numTestsPerEvictionRun","this.blockWhenExhausted","this.minEvictableIdleTimeMillis","this.lifo","this.evictionPolicyClassName","this.timeBetweenEvictionRunsMillis","this.jmxNamePrefix"},delta={"this.jmxNameBase","this.maxIdle","this.testOnCreate","this.jmxEnabled","this.testWhileIdle","this.fairness","this.softMinEvictableIdleTimeMillis","this.testOnBorrow","this.maxTotal","this.maxWaitMillis","this.testOnReturn","this.minIdle","this.numTestsPerEvictionRun","this.blockWhenExhausted","this.minEvictableIdleTimeMillis","this.lifo","this.evictionPolicyClassName","this.timeBetweenEvictionRunsMillis","this.jmxNamePrefix"})
  static void _init_25770(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "fairness"));
    vm.put("v1", (int)0);
    vm.put("v2", om.popField(vm.get("this"), "blockWhenExhausted"));
    vm.put("v3", (int)1);
    vm.put("v4", om.popField(vm.get("this"), "testOnReturn"));
    vm.put("v5", om.popField(vm.get("this"), "minEvictableIdleTimeMillis"));
    vm.put("v6", (long)1800000L);
    vm.put("v7", om.popField(vm.get("this"), "lifo"));
    vm.put("v8", om.popField(vm.get("this"), "jmxNameBase"));
    vm.put("v9", "pool");
    vm.put("v10", om.popField(vm.get("this"), "softMinEvictableIdleTimeMillis"));
    vm.put("v11", om.popField(vm.get("this"), "testOnBorrow"));
    vm.put("v12", om.popField(vm.get("this"), "maxWaitMillis"));
    vm.put("v13", (long)-1L);
    vm.put("v14", om.popField(vm.get("this"), "evictionPolicyClassName"));
    vm.put("v15", "org.apache.tomcat.dbcp.pool2.impl.DefaultEvictionPolicy");
    vm.put("v16", om.popField(vm.get("this"), "testOnCreate"));
    vm.put("v17", om.popField(vm.get("this"), "jmxEnabled"));
    vm.put("v18", om.popField(vm.get("this"), "minIdle"));
    vm.put("v19", om.popField(vm.get("this"), "numTestsPerEvictionRun"));
    vm.put("v20", (int)3);
    vm.put("v21", om.popField(vm.get("this"), "testWhileIdle"));
    vm.put("v22", om.popField(vm.get("this"), "maxTotal"));
    vm.put("v23", (int)8);
    vm.put("v24", om.popField(vm.get("this"), "timeBetweenEvictionRunsMillis"));
    vm.put("v25", om.popField(vm.get("this"), "jmxNamePrefix"));
    vm.put("v26", "pool");
    vm.put("v27", om.popField(vm.get("this"), "maxIdle"));
    if (!em.eq(vm.get("v9"), (vm.get("v8")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v8")));
    if (!em.eq(vm.get("v3"), (vm.get("v17")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v17")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v20"), (vm.get("v19")))) e.abort("Inconsistent value for \"v20\": " + vm.get("v20") + " ne " + (vm.get("v19")));
    if (!em.eq(vm.get("v6"), (vm.get("v10")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v10")));
    if (!em.eq(vm.get("v1"), (vm.get("v18")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v18")));
    if (!em.eq(vm.get("v15"), (vm.get("v14")))) e.abort("Inconsistent value for \"v15\": " + vm.get("v15") + " ne " + (vm.get("v14")));
    if (!em.eq(vm.get("v1"), (vm.get("v21")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v21")));
    if (!em.eq(vm.get("v26"), (vm.get("v25")))) e.abort("Inconsistent value for \"v26\": " + vm.get("v26") + " ne " + (vm.get("v25")));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    if (!em.eq(vm.get("v1"), (vm.get("v11")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v11")));
    if (!em.eq(vm.get("v13"), (vm.get("v24")))) e.abort("Inconsistent value for \"v13\": " + vm.get("v13") + " ne " + (vm.get("v24")));
    if (!em.eq(vm.get("v3"), (vm.get("v7")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v7")));
    if (!em.eq(vm.get("v23"), (vm.get("v27")))) e.abort("Inconsistent value for \"v23\": " + vm.get("v23") + " ne " + (vm.get("v27")));
    if (!em.eq(vm.get("v1"), (vm.get("v4")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v4")));
    if (!em.eq(vm.get("v23"), (vm.get("v22")))) e.abort("Inconsistent value for \"v23\": " + vm.get("v23") + " ne " + (vm.get("v22")));
    if (!em.eq(vm.get("v1"), (vm.get("v16")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v16")));
    if (!em.eq(vm.get("v13"), (vm.get("v12")))) e.abort("Inconsistent value for \"v13\": " + vm.get("v13") + " ne " + (vm.get("v12")));
    if (!em.eq(vm.get("v6"), (vm.get("v5")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v5")));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setBlockWhenExhausted", desc="(Z)V",definedOutput={"this.blockWhenExhausted"},delta={"this.blockWhenExhausted"})
  static void setBlockWhenExhausted14260(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "blockWhenExhausted"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.GenericObjectPoolConfig", name="setMaxTotal", desc="(I)V",definedOutput={"this.maxTotal"},delta={"this.maxTotal"})
  static void setMaxTotal14155(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "maxTotal"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setTestOnReturn", desc="(Z)V",definedOutput={"this.testOnReturn"},delta={"this.testOnReturn"})
  static void setTestOnReturn11189(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "testOnReturn"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setFairness", desc="(Z)V",definedOutput={"this.fairness"},delta={"this.fairness"})
  static void setFairness22206(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "fairness"));
    vm.put("param0", vm.get("v0"));
  }
}
