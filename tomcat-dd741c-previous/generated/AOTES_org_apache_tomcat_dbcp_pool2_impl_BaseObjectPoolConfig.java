import org.javelus.aotes.executor.*;
@Defined({"this.fairness","this.timeBetweenEvictionRunsMillis","this.jmxNameBase","this.maxWaitMillis","this.minEvictableIdleTimeMillis","this.testWhileIdle","this.lifo","this.blockWhenExhausted","this.jmxNamePrefix","this.numTestsPerEvictionRun","this.testOnBorrow","this.jmxEnabled","this.testOnReturn","this.testOnCreate","this.evictionPolicyClassName","this.softMinEvictableIdleTimeMillis"})
public class AOTES_org_apache_tomcat_dbcp_pool2_impl_BaseObjectPoolConfig {
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="<init>", desc="()V",definedOutput={"this.fairness","this.timeBetweenEvictionRunsMillis","this.jmxNameBase","this.maxWaitMillis","this.minEvictableIdleTimeMillis","this.testWhileIdle","this.lifo","this.numTestsPerEvictionRun","this.jmxNamePrefix","this.blockWhenExhausted","this.jmxEnabled","this.testOnBorrow","this.testOnReturn","this.testOnCreate","this.evictionPolicyClassName","this.softMinEvictableIdleTimeMillis"},delta={"this.fairness","this.timeBetweenEvictionRunsMillis","this.jmxNameBase","this.maxWaitMillis","this.minEvictableIdleTimeMillis","this.testWhileIdle","this.lifo","this.numTestsPerEvictionRun","this.jmxNamePrefix","this.blockWhenExhausted","this.jmxEnabled","this.testOnBorrow","this.testOnReturn","this.testOnCreate","this.evictionPolicyClassName","this.softMinEvictableIdleTimeMillis"})
  static void _init_2999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "maxWaitMillis"));
    vm.put("v1", (long)-1L);
    vm.put("v2", om.popField(vm.get("this"), "jmxNameBase"));
    vm.put("v3", "pool");
    vm.put("v4", om.popField(vm.get("this"), "evictionPolicyClassName"));
    vm.put("v5", "org.apache.tomcat.dbcp.pool2.impl.DefaultEvictionPolicy");
    vm.put("v6", om.popField(vm.get("this"), "jmxEnabled"));
    vm.put("v7", (int)1);
    vm.put("v8", om.popField(vm.get("this"), "numTestsPerEvictionRun"));
    vm.put("v9", (int)3);
    vm.put("v10", om.popField(vm.get("this"), "softMinEvictableIdleTimeMillis"));
    vm.put("v11", (long)1800000L);
    vm.put("v12", om.popField(vm.get("this"), "jmxNamePrefix"));
    vm.put("v13", "pool");
    vm.put("v14", om.popField(vm.get("this"), "blockWhenExhausted"));
    vm.put("v15", om.popField(vm.get("this"), "fairness"));
    vm.put("v16", (int)0);
    vm.put("v17", om.popField(vm.get("this"), "testOnReturn"));
    vm.put("v18", om.popField(vm.get("this"), "testOnBorrow"));
    vm.put("v19", om.popField(vm.get("this"), "minEvictableIdleTimeMillis"));
    vm.put("v20", om.popField(vm.get("this"), "testWhileIdle"));
    vm.put("v21", om.popField(vm.get("this"), "testOnCreate"));
    vm.put("v22", om.popField(vm.get("this"), "lifo"));
    vm.put("v23", om.popField(vm.get("this"), "timeBetweenEvictionRunsMillis"));
    if (!em.eq(vm.get("v16"), (vm.get("v20")))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (vm.get("v20")));
    if (!em.eq(vm.get("v16"), (vm.get("v15")))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (vm.get("v15")));
    if (!em.eq(vm.get("v7"), (vm.get("v22")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v22")));
    if (!em.eq(vm.get("v11"), (vm.get("v19")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v19")));
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    if (!em.eq(vm.get("v1"), (vm.get("v23")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v23")));
    if (!em.eq(vm.get("v7"), (vm.get("v6")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v6")));
    if (!em.eq(vm.get("v16"), (vm.get("v18")))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (vm.get("v18")));
    if (!em.eq(vm.get("v16"), (vm.get("v21")))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (vm.get("v21")));
    if (!em.eq(vm.get("v11"), (vm.get("v10")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v10")));
    if (!em.eq(vm.get("v7"), (vm.get("v14")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v14")));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v13"), (vm.get("v12")))) e.abort("Inconsistent value for \"v13\": " + vm.get("v13") + " ne " + (vm.get("v12")));
    if (!em.eq(vm.get("v9"), (vm.get("v8")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v8")));
    if (!em.eq(vm.get("v16"), (vm.get("v17")))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (vm.get("v17")));
  }
}
