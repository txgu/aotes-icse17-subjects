import org.javelus.aotes.executor.*;
@Defined({"this.fairness","this.timeBetweenEvictionRunsMillis","this.jmxNameBase","this.maxWaitMillis","this.minEvictableIdleTimeMillis","this.testWhileIdle","this.lifo","this.blockWhenExhausted","this.jmxNamePrefix","this.numTestsPerEvictionRun","org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig.DEFAULT_JMX_NAME_BASE","this.testOnBorrow","this.jmxEnabled","this.testOnReturn","this.testOnCreate","this.evictionPolicyClassName","this.softMinEvictableIdleTimeMillis"})
public class AOTES_org_apache_tomcat_dbcp_pool2_impl_BaseObjectPoolConfig {
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setTestOnCreate", desc="(Z)V",definedOutput={"this.testOnCreate"},delta={"this.testOnCreate"})
  static void setTestOnCreate899(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "testOnCreate"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setMinEvictableIdleTimeMillis", desc="(J)V",definedOutput={"this.minEvictableIdleTimeMillis"},delta={"this.minEvictableIdleTimeMillis"})
  static void setMinEvictableIdleTimeMillis499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "minEvictableIdleTimeMillis"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setMaxWaitMillis", desc="(J)V",definedOutput={"this.maxWaitMillis"},delta={"this.maxWaitMillis"})
  static void setMaxWaitMillis1399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "maxWaitMillis"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setTimeBetweenEvictionRunsMillis", desc="(J)V",definedOutput={"this.timeBetweenEvictionRunsMillis"},delta={"this.timeBetweenEvictionRunsMillis"})
  static void setTimeBetweenEvictionRunsMillis1799(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "timeBetweenEvictionRunsMillis"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setBlockWhenExhausted", desc="(Z)V",definedOutput={"this.blockWhenExhausted"},delta={"this.blockWhenExhausted"})
  static void setBlockWhenExhausted2699(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "blockWhenExhausted"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setEvictionPolicyClassName", desc="(Ljava/lang/String;)V",definedOutput={"this.evictionPolicyClassName"},delta={"this.evictionPolicyClassName"})
  static void setEvictionPolicyClassName1499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "evictionPolicyClassName"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setTestWhileIdle", desc="(Z)V",definedOutput={"this.testWhileIdle"},delta={"this.testWhileIdle"})
  static void setTestWhileIdle1699(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "testWhileIdle"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setTestOnBorrow", desc="(Z)V",definedOutput={"this.testOnBorrow"},delta={"this.testOnBorrow"})
  static void setTestOnBorrow2099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "testOnBorrow"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setJmxNamePrefix", desc="(Ljava/lang/String;)V",definedOutput={"this.jmxNamePrefix"},delta={"this.jmxNamePrefix"})
  static void setJmxNamePrefix599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "jmxNamePrefix"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setNumTestsPerEvictionRun", desc="(I)V",definedOutput={"this.numTestsPerEvictionRun"},delta={"this.numTestsPerEvictionRun"})
  static void setNumTestsPerEvictionRun2599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "numTestsPerEvictionRun"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setTestOnReturn", desc="(Z)V",definedOutput={"this.testOnReturn"},delta={"this.testOnReturn"})
  static void setTestOnReturn699(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "testOnReturn"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setJmxNameBase", desc="(Ljava/lang/String;)V",definedOutput={"this.jmxNameBase"},delta={"this.jmxNameBase"})
  static void setJmxNameBase299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "jmxNameBase"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="<clinit>", desc="()V",definedOutput={"org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig.DEFAULT_JMX_NAME_BASE"},delta={"org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig.DEFAULT_JMX_NAME_BASE"})
  static void _clinit_2799(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", om.popStatic("org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig.DEFAULT_JMX_NAME_BASE"));
    vm.put("v1", null);
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setLifo", desc="(Z)V",definedOutput={"this.lifo"},delta={"this.lifo"})
  static void setLifo1299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "lifo"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setJmxEnabled", desc="(Z)V",definedOutput={"this.jmxEnabled"},delta={"this.jmxEnabled"})
  static void setJmxEnabled3099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "jmxEnabled"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="<init>", desc="()V",definedOutput={"this.fairness","this.timeBetweenEvictionRunsMillis","this.jmxNameBase","this.maxWaitMillis","this.minEvictableIdleTimeMillis","this.testWhileIdle","this.lifo","this.numTestsPerEvictionRun","this.jmxNamePrefix","this.blockWhenExhausted","this.jmxEnabled","this.testOnBorrow","this.testOnReturn","this.testOnCreate","this.evictionPolicyClassName","this.softMinEvictableIdleTimeMillis"},delta={"this.fairness","this.timeBetweenEvictionRunsMillis","this.jmxNameBase","this.maxWaitMillis","this.minEvictableIdleTimeMillis","this.testWhileIdle","this.lifo","this.numTestsPerEvictionRun","this.jmxNamePrefix","this.blockWhenExhausted","this.jmxEnabled","this.testOnBorrow","this.testOnReturn","this.testOnCreate","this.evictionPolicyClassName","this.softMinEvictableIdleTimeMillis"})
  static void _init_2999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "maxWaitMillis"));
    vm.put("v1", (long)-1L);
    vm.put("v2", om.popField(vm.get("this"), "jmxNameBase"));
    vm.put("v3", "pool");
    vm.put("v4", om.popField(vm.get("this"), "evictionPolicyClassName"));
    vm.put("v5", "org.apache.tomcat.dbcp.pool2.impl.DefaultEvictionPolicy");
    vm.put("v6", om.popField(vm.get("this"), "jmxEnabled"));
    vm.put("v7", (int)1);
    vm.put("v8", om.popField(vm.get("this"), "numTestsPerEvictionRun"));
    vm.put("v9", (int)3);
    vm.put("v10", om.popField(vm.get("this"), "softMinEvictableIdleTimeMillis"));
    vm.put("v11", (long)1800000L);
    vm.put("v12", om.popField(vm.get("this"), "jmxNamePrefix"));
    vm.put("v13", "pool");
    vm.put("v14", om.popField(vm.get("this"), "blockWhenExhausted"));
    vm.put("v15", om.popField(vm.get("this"), "fairness"));
    vm.put("v16", (int)0);
    vm.put("v17", om.popField(vm.get("this"), "testOnReturn"));
    vm.put("v18", om.popField(vm.get("this"), "testOnBorrow"));
    vm.put("v19", om.popField(vm.get("this"), "minEvictableIdleTimeMillis"));
    vm.put("v20", om.popField(vm.get("this"), "testWhileIdle"));
    vm.put("v21", om.popField(vm.get("this"), "testOnCreate"));
    vm.put("v22", om.popField(vm.get("this"), "lifo"));
    vm.put("v23", om.popField(vm.get("this"), "timeBetweenEvictionRunsMillis"));
    if (!em.eq(vm.get("v16"), (vm.get("v20")))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (vm.get("v20")));
    if (!em.eq(vm.get("v16"), (vm.get("v15")))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (vm.get("v15")));
    if (!em.eq(vm.get("v7"), (vm.get("v22")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v22")));
    if (!em.eq(vm.get("v11"), (vm.get("v19")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v19")));
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    if (!em.eq(vm.get("v1"), (vm.get("v23")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v23")));
    if (!em.eq(vm.get("v7"), (vm.get("v6")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v6")));
    if (!em.eq(vm.get("v16"), (vm.get("v18")))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (vm.get("v18")));
    if (!em.eq(vm.get("v16"), (vm.get("v21")))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (vm.get("v21")));
    if (!em.eq(vm.get("v11"), (vm.get("v10")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v10")));
    if (!em.eq(vm.get("v7"), (vm.get("v14")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v14")));
    if (!em.eq(vm.get("v3"), (vm.get("v2")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v2")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v13"), (vm.get("v12")))) e.abort("Inconsistent value for \"v13\": " + vm.get("v13") + " ne " + (vm.get("v12")));
    if (!em.eq(vm.get("v9"), (vm.get("v8")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v8")));
    if (!em.eq(vm.get("v16"), (vm.get("v17")))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (vm.get("v17")));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setFairness", desc="(Z)V",definedOutput={"this.fairness"},delta={"this.fairness"})
  static void setFairness3199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "fairness"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig", name="setSoftMinEvictableIdleTimeMillis", desc="(J)V",definedOutput={"this.softMinEvictableIdleTimeMillis"},delta={"this.softMinEvictableIdleTimeMillis"})
  static void setSoftMinEvictableIdleTimeMillis1099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "softMinEvictableIdleTimeMillis"));
    vm.put("param0", vm.get("v0"));
  }
}
