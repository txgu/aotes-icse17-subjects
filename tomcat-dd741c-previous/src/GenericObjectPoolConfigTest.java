import org.apache.tomcat.dbcp.pool2.impl.BaseObjectPoolConfig;
import org.apache.tomcat.dbcp.pool2.impl.GenericObjectPoolConfig;
import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;

public class GenericObjectPoolConfigTest {

    /**
     * 0,0
     * @author tianxiao
     *
     */
    public static class Test1 extends AOTESTestCase<GenericObjectPoolConfig> {

        @Override
        protected GenericObjectPoolConfig buildInternal() {
            GenericObjectPoolConfig o = new GenericObjectPoolConfig();
            return o;
        }

        @Override
        protected void checkInternal(GenericObjectPoolConfig updated) throws Exception {
            assertFieldEquals(updated, "jmxNameBase", null);
        }
        
    }

    /**
     * 1,0
     * @author tianxiao
     *
     */
    public static class Test2 extends AOTESTestCase<GenericObjectPoolConfig> {

        String jmxNameBase = "Non-default name base";
        @Override
        protected GenericObjectPoolConfig buildInternal() {
            GenericObjectPoolConfig o = new GenericObjectPoolConfig();
            o.setJmxNameBase(jmxNameBase);
            return o;
        }

        @Override
        protected void checkInternal(GenericObjectPoolConfig updated) throws Exception {
            assertFieldEquals(updated, "jmxNameBase", jmxNameBase);
        }
        
    }
    
    /**
     * 0,1
     * @author tianxiao
     *
     */
    public static class Test3 extends AOTESTestCase<GenericObjectPoolConfig> {

        @Override
        protected GenericObjectPoolConfig buildInternal() {
            GenericObjectPoolConfig o = new GenericObjectPoolConfig();
            o.setFairness(true);
            return o;
        }

        @Override
        protected void checkInternal(GenericObjectPoolConfig updated) throws Exception {
            assertFieldEquals(updated, "jmxNameBase", null);
        }
        
    }

    /**
     * 1,1
     * @author tianxiao
     *
     */
    public static class Test4 extends AOTESTestCase<GenericObjectPoolConfig> {

        String jmxNameBase = "Non-default name base";

        @Override
        protected GenericObjectPoolConfig buildInternal() {
            GenericObjectPoolConfig o = new GenericObjectPoolConfig();
            o.setFairness(true);
            o.setJmxNameBase(jmxNameBase);
            return o;
        }

        @Override
        protected void checkInternal(GenericObjectPoolConfig updated) throws Exception {
            assertFieldEquals(updated, "jmxNameBase", jmxNameBase);
        }
        
    }
    
    
    /**
     * *,1
     * @author tianxiao
     *
     */
    public static class Test5 extends AOTESTestCase<GenericObjectPoolConfig> {

        String jmxNameBase = BaseObjectPoolConfig.DEFAULT_JMX_NAME_PREFIX;

        @Override
        protected GenericObjectPoolConfig buildInternal() {
            GenericObjectPoolConfig o = new GenericObjectPoolConfig();
            o.setFairness(true);
            o.setJmxNameBase(jmxNameBase);
            return o;
        }

        @Override
        protected void checkInternal(GenericObjectPoolConfig updated) throws Exception {
            assertFieldEquals(updated, "jmxNameBase", jmxNameBase);
        }
        
    }

    /**
     * *,0
     * @author tianxiao
     *
     */
    public static class Test6 extends AOTESTestCase<GenericObjectPoolConfig> {

        String jmxNameBase = BaseObjectPoolConfig.DEFAULT_JMX_NAME_PREFIX;

        @Override
        protected GenericObjectPoolConfig buildInternal() {
            GenericObjectPoolConfig o = new GenericObjectPoolConfig();
            o.setJmxNameBase(jmxNameBase);
            return o;
        }

        @Override
        protected void checkInternal(GenericObjectPoolConfig updated) throws Exception {
            assertFieldEquals(updated, "jmxNameBase", jmxNameBase);
        }
        
    }
    
    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        AOTESTestRunner.run(
                Test1.class,
                Test2.class,
                Test3.class,
                Test4.class,
                Test5.class,
                Test6.class
                );
    }

}
