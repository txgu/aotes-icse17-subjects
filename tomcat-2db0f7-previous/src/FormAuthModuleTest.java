import org.apache.catalina.Context;
import org.apache.catalina.Realm;
import org.apache.catalina.authenticator.jaspic.provider.modules.FormAuthModule;
import org.apache.catalina.startup.Tomcat;
import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;

public class FormAuthModuleTest {

    public static class Test1 extends AOTESTestCase<FormAuthModule> {

        Realm realm;
        
        @Override
        protected FormAuthModule buildInternal() {
            Tomcat tomcat = new Tomcat();
            Context c = tomcat.addContext("/test-context", ".");
            FormAuthModule form = new FormAuthModule(c);
            realm = c.getRealm();
            return form;
        }

        @Override
        protected void checkInternal(FormAuthModule updated) throws Exception {
            assertMixedFieldNotEquals(updated, "realm", null);
            assertMixedFieldEquals(updated, "realm", realm);
        }
        
    }
    
    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        AOTESTestRunner.run(
                Test1.class
                );
    }
}
