import org.javelus.aotes.executor.*;
@Defined({"this.supportedMessageTypes[*]","this.supportedMessageTypes","this.context"})
public class AOTES_org_apache_catalina_authenticator_jaspic_provider_modules_FormAuthModule {
  @IM(clazz="org.apache.catalina.authenticator.jaspic.provider.modules.FormAuthModule", name="<init>", desc="(Lorg/apache/catalina/Context;)V",definedOutput={"this.supportedMessageTypes[*]","this.supportedMessageTypes","this.context"},delta={"this.supportedMessageTypes[*]","this.supportedMessageTypes","this.context"})
  static void _init_199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", (int)2);
    vm.put("this", om.getTarget());
    vm.put("v1", om.popField(vm.get("this"), "context"));
    vm.put("v2", om.popField(vm.get("this"), "supportedMessageTypes"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", (int)0);
    vm.put("v5", javax.servlet.http.HttpServletRequest.class);
    vm.put("v6", (int)1);
    vm.put("v7", javax.servlet.http.HttpServletResponse.class);
    vm.put("v8", om.popArrayElement(vm.get("v3"), vm.get("v4")));
    vm.put("v9", om.popArrayElement(vm.get("v3"), vm.get("v6")));
    vm.put("v10", om.getArrayLength(vm.get("v3")));
    vm.put("param0", vm.get("v1"));
    if (!em.eq(vm.get("v0"), (vm.get("v10")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v10")));
  }
}
