import org.javelus.aotes.executor.*;
@Defined({"this.ordinal","this.id","this.payloadSizeValidator","this.streamZero","this.streamNonZero","this.name","this.payloadErrorFatal"})
public class AOTES_org_apache_coyote_http2_FrameType {
  @IM(clazz="org.apache.coyote.http2.FrameType", name="<init>", desc="(Ljava/lang/String;IIZZLjava/util/function/IntPredicate;Z)V",definedOutput={"this.ordinal","this.id","this.streamZero","this.payloadSizeValidator","this.streamNonZero","this.name","this.payloadErrorFatal"},delta={"this.ordinal","this.id","this.streamZero","this.payloadSizeValidator","this.streamNonZero","this.name","this.payloadErrorFatal"})
  static void _init_4399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "payloadErrorFatal"));
    vm.put("v1", om.popField(vm.get("this"), "id"));
    vm.put("v2", om.popField(vm.get("this"), "streamZero"));
    vm.put("v3", om.popField(vm.get("this"), "payloadSizeValidator"));
    vm.put("v4", om.popField(vm.get("this"), "streamNonZero"));
    vm.put("v5", om.popField(vm.get("this"), "name"));
    vm.put("v6", om.popField(vm.get("this"), "ordinal"));
    vm.put("param2", vm.get("v1"));
    vm.put("param5", vm.get("v3"));
    vm.put("param0", vm.get("v5"));
    vm.put("param4", vm.get("v4"));
    vm.put("param1", vm.get("v6"));
    vm.put("param6", vm.get("v0"));
    vm.put("param3", vm.get("v2"));
  }
}
