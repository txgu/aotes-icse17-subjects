import org.javelus.aotes.executor.*;
@Defined({"this.pollerRunning","this.closeList.size","this.closeList.timeouts[*]","this.closeList.flags[*]","this.closeList.sockets[*]"})
public class AOTES_org_apache_tomcat_util_net_AprEndpoint$Poller {
  @IM(clazz="org.apache.tomcat.util.net.AprEndpoint$Poller", name="close", desc="(J)V",revertedInput={"this.closeList.size"},definedOutput={"this.closeList.size","this.closeList.timeouts[*]","this.closeList.flags[*]","this.closeList.sockets[*]"},delta={"this.closeList.timeouts[*]","this.closeList.flags[*]","this.closeList.sockets[*]"})
  static void close999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "closeList"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("v2"), "size"));
    vm.put("v4", (int)1);
    vm.put("v5", (long)0L);
    vm.put("v6", om.getField(vm.get("v2"), "sockets"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayLength(vm.get("v7")));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getField(vm.get("v2"), "flags"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("v2"), "timeouts"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getArrayElement(vm.get("v7"), vm.get("v0")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", vm.get("v3"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v4")));
    om.revertField(vm.get("v2"), "size", vm.get("v17"));
    vm.put("v18", om.popArrayElement(vm.get("v11"), vm.get("v17")));
    vm.put("v19", om.popArrayElement(vm.get("v7"), vm.get("v17")));
    vm.put("param0", vm.get("v19"));
    vm.put("v20", em.op("lcmp").eval(vm.get("v15"),vm.get("param0")));
    vm.put("v21", om.popArrayElement(vm.get("v13"), vm.get("v17")));
    if (!em.ne(vm.get("v17"), vm.get("v9"))) e.abort(String.format("!((v17=%s) ne (v9=%s))", vm.get("v17"), vm.get("v9")));
    if (!em.ge(vm.get("v4"), vm.get("v17"))) e.abort(String.format("!((v4=%s) ge (v17=%s))", vm.get("v4"), vm.get("v17")));
    if (!em.ne(vm.get("v20"), vm.get("v0"))) e.abort(String.format("!((v20=%s) ne (v0=%s))", vm.get("v20"), vm.get("v0")));
    if (!em.lt(vm.get("v0"), vm.get("v17"))) e.abort(String.format("!((v0=%s) lt (v17=%s))", vm.get("v0"), vm.get("v17")));
  }
  @IM(clazz="org.apache.tomcat.util.net.AprEndpoint$Poller", name="close", desc="(J)V",revertedInput={"this.closeList.size"},definedOutput={"this.closeList.size","this.closeList.timeouts[*]","this.closeList.flags[*]","this.closeList.sockets[*]"},delta={"this.closeList.timeouts[*]","this.closeList.flags[*]","this.closeList.sockets[*]"})
  static void close992(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "closeList"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("v2"), "size"));
    vm.put("v4", (int)1);
    vm.put("v5", (long)0L);
    vm.put("v6", om.getField(vm.get("v2"), "timeouts"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("v2"), "sockets"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getArrayLength(vm.get("v9")));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("v2"), "flags"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", vm.get("v3"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v4")));
    om.revertField(vm.get("v2"), "size", vm.get("v15"));
    vm.put("v16", om.popArrayElement(vm.get("v13"), vm.get("v15")));
    vm.put("v17", om.popArrayElement(vm.get("v7"), vm.get("v15")));
    vm.put("v18", om.popArrayElement(vm.get("v9"), vm.get("v15")));
    vm.put("param0", vm.get("v18"));
    if (!em.ge(vm.get("v0"), vm.get("v15"))) e.abort(String.format("!((v0=%s) ge (v15=%s))", vm.get("v0"), vm.get("v15")));
    if (!em.ne(vm.get("v15"), vm.get("v11"))) e.abort(String.format("!((v15=%s) ne (v11=%s))", vm.get("v15"), vm.get("v11")));
  }
  @IM(clazz="org.apache.tomcat.util.net.AprEndpoint$Poller", name="stop", desc="()V",definedOutput={"this.pollerRunning"},delta={"this.pollerRunning"})
  static void stop1299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "pollerRunning"));
    vm.put("v1", (int)0);
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
  }
}
