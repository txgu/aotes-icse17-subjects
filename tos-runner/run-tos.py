#! /usr/bin/python

import sys, os, subprocess

import traceback, re, shutil

JDK_HOME='D:/DevTools/Java/jdk1.8.0_65'
JAVA=os.path.join(JDK_HOME, 'bin', 'java')
JDK_TOOLS=os.path.join(JDK_HOME, 'lib', 'tools.jar')

TOS_LIB=os.path.join(os.path.abspath(os.path.dirname(__file__)), 'tos.jar')
TOS_MAIN='edu.umd.MatchSnapshots.Main'

DUMP_PATTERN=re.compile(r'([a-z]+)-([^_]+)_(old|new)_([^_]+)_(.+)[.]hprof')

class Update(object):
    def __init__(self):
        pass

class Dump(object):

    def __init__(self):
        pass


def prepare(folder):
    updates = {}
    for f in os.listdir(folder):
        if not os.path.isfile(f):
            continue

        m = DUMP_PATTERN.match(f)

        if not m:
            continue

        d = Dump()
        d.proj_name = m.group(1)
        d.update = m.group(2)
        d.version = m.group(3)
        d.class_name = m.group(4)

        d.fields = m.group(5).split('_')

        dst = os.path.join(folder, d.proj_name, d.update, d.version)

        d.dump_dir = dst
        if not os.path.exists(dst):
            os.makedirs(dst)

        id = '-'.join([d.proj_name,d.update,d.class_name])
        print(id)
        if id in updates:
            u = updates[id]
        else:
            u = Update()
            u.id = id
            updates[id] = u

        u.__dict__[d.version] = d


        dst = os.path.join(dst, f)
        if not os.path.exists(dst):
            shutil.move(os.path.join(folder, f), dst)

    return updates

def run_tos(folder, u):
    old_dump = u.old
    new_dump = u.new

    fields = old_dump.fields
    class_name = old_dump.class_name

    for field in fields:
        log_name = os.path.join(folder, u.id + '_' + field + '.log')
        run_tos_with_field(old_dump.dump_dir, new_dump.dump_dir, class_name, field, log_name)

def run_tos_with_field(old_dump_dir, new_dump_dir, class_name, field_name, log_name):
    with open(log_name, 'w') as log:
        cmd = [JAVA, '-cp', os.pathsep.join([TOS_LIB, JDK_TOOLS]), TOS_MAIN, old_dump_dir, old_dump_dir, new_dump_dir, class_name, field_name]
        ret = subprocess.call(cmd, stdout=log, stderr=log)

        if ret != 0:
            print("Run tos with " + ' '.join(cmd) + ' failed!')


def run(folder):
    updates = prepare(folder)

    for i,u in updates.iteritems():
        run_tos(folder, u)

if __name__ == '__main__':

    try:
        run(os.path.abspath(os.path.dirname(__file__)))
    except Exception as e:
        traceback.print_exc(e)
