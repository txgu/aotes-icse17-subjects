import org.javelus.aotes.executor.*;
@Defined({"this.out","this.in","this.err","this.callback","this.shell"})
public class AOTES_org_apache_sshd_server_shell_InvertedShellWrapper {
  @IM(clazz="org.apache.sshd.server.shell.InvertedShellWrapper", name="<init>", desc="(Lorg/apache/sshd/server/shell/InvertedShell;)V",definedOutput={"this.shell"},delta={"this.shell"})
  static void _init_1299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "shell"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.shell.InvertedShellWrapper", name="setErrorStream", desc="(Ljava/io/OutputStream;)V",definedOutput={"this.err"},delta={"this.err"})
  static void setErrorStream1499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "err"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.shell.InvertedShellWrapper", name="setExitCallback", desc="(Lorg/apache/sshd/server/ShellFactory$ExitCallback;)V",definedOutput={"this.callback"},delta={"this.callback"})
  static void setExitCallback2099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "callback"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.shell.InvertedShellWrapper", name="setOutputStream", desc="(Ljava/io/OutputStream;)V",definedOutput={"this.out"},delta={"this.out"})
  static void setOutputStream3899(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "out"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.shell.InvertedShellWrapper", name="setInputStream", desc="(Ljava/io/InputStream;)V",definedOutput={"this.in"},delta={"this.in"})
  static void setInputStream3799(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "in"));
    vm.put("param0", vm.get("v0"));
  }
}
