# READ ME

## Build

1. Make a directory. We later refer this directory as `$AOTES_HOME`.
2. Change directory to `$AOTES_HOME` and clone the following projects

    1. [AOTES](https://bitbucket.org/txgu/aotes-asm)
    2. [AOTES-ICSE17-SUBJECTS](https://bitbucket.org/txgu/aotes-icse17-subjects)
    3. [Developer Interface of Javelus](https://bitbucket.org/javelus/developer-interface)
    4. [Dynamic Patch Generator](https://bitbucket.org/javelus/dpg)
    5. [Javelus](https://bitbucket.org/javelus/)

3. Open eclipse and import all projects in `$APTES_HOME`


## Run

If you set up everything successfully,
you can run tests in every sub project whose name ends with `-previous`
in `aotes-icse17-subjects` in eclipse with a standard JVM.
The test will print the synthesized sequence of method invocations.



Building and installing Javelus is not easy in windows.
So, we have not provided the guide here.
You can refer to this [link](http://moon.nju.edu.cn/dse/javelus/).


