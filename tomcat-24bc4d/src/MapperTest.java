import org.apache.catalina.Host;
import org.apache.catalina.core.StandardHost;
import org.apache.catalina.mapper.Mapper;
import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;

public class MapperTest {

    public static class Test1 extends AOTESTestCase<Mapper> {

        @Override
        protected Mapper buildInternal() {
            Mapper mapper = new Mapper();
            return mapper;
        }

        @Override
        protected void checkInternal(Mapper updated) throws Exception {
            assertFieldEquals(updated, "defaultHost", null);
        }
        
    }
    
    public static class Test2 extends AOTESTestCase<Mapper> {

        Host host;
        
        @Override
        protected Mapper buildInternal() {
            Mapper mapper = new Mapper();
            mapper.setDefaultHostName("localhost");
            host = new StandardHost();
            mapper.addHost("localhost", new String[]{}, host);
            return mapper;
        }

        @Override
        protected void checkInternal(Mapper updated) throws Exception {
            assertFieldNotEquals(updated, "defaultHost", null);
            Object mappedHost = readField(updated, "defaultHost");
            assertFieldEquals(mappedHost, "object", host);
        }
        
    }
    
    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        AOTESTestRunner.run(
                Test1.class, 
                Test2.class
                );
    }
}
