import org.javelus.aotes.executor.*;
@Defined({"this.hosts","this.defaultHostName","this.contextObjectToContextVersionMap"})
public class AOTES_org_apache_catalina_mapper_Mapper {
  @IM(clazz="org.apache.catalina.mapper.Mapper", name="setDefaultHostName", desc="(Ljava/lang/String;)V",definedOutput={"this.defaultHostName"},delta={"this.defaultHostName"})
  static void setDefaultHostName10899(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "defaultHostName"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.catalina.mapper.Mapper", name="<init>", desc="()V",definedOutput={"this.hosts","this.defaultHostName","this.contextObjectToContextVersionMap"},delta={"this.hosts","this.defaultHostName","this.contextObjectToContextVersionMap"})
  static void _init_1499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "defaultHostName"));
    vm.put("v1", null);
    vm.put("v2", om.popField(vm.get("this"), "contextObjectToContextVersionMap"));
    vm.put("v3", om.popField(vm.get("this"), "hosts"));
    vm.put("v4", (int)0);
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    vm.put("v5", vm.get("v3"));
    vm.put("v6", om.getArrayLength(vm.get("v5")));
    if (!em.eq(vm.get("v4"), (vm.get("v6")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v6")));
    vm.put("v7", vm.get("v2"));
  }
}
