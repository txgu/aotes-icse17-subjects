import org.javelus.aotes.executor.*;
@Defined({"this.pollerRunning","this.closeList.sockets[*]","this.closeList.size","this.closeList.timeouts[*]","this.closeList.flags[*]"})
public class AOTES_org_apache_tomcat_util_net_AprEndpoint$Poller {
  @IM(clazz="org.apache.tomcat.util.net.AprEndpoint$Poller", name="close", desc="(J)V",revertedInput={"this.closeList.size"},definedOutput={"this.closeList.size","this.closeList.sockets[*]","this.closeList.timeouts[*]","this.closeList.flags[*]"},delta={"this.closeList.sockets[*]","this.closeList.timeouts[*]","this.closeList.flags[*]"})
  static void close5397(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "closeList"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("v1"), "size"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("v1"), "flags"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v1"), "timeouts"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (long)0L);
    vm.put("v9", om.getField(vm.get("v1"), "sockets"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)0);
    vm.put("v12", om.getArrayLength(vm.get("v10")));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", vm.get("v2"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v3")));
    om.revertField(vm.get("v1"), "size", vm.get("v15"));
    vm.put("v16", om.popArrayElement(vm.get("v7"), vm.get("v15")));
    vm.put("v17", om.popArrayElement(vm.get("v10"), vm.get("v15")));
    vm.put("param0", vm.get("v17"));
    vm.put("v18", om.popArrayElement(vm.get("v5"), vm.get("v15")));
    if (!em.ge(vm.get("v11"), vm.get("v15"))) e.abort(String.format("!((v11=%s) ge (v15=%s))", vm.get("v11"), vm.get("v15")));
    if (!em.ne(vm.get("v15"), vm.get("v13"))) e.abort(String.format("!((v15=%s) ne (v13=%s))", vm.get("v15"), vm.get("v13")));
  }
  @IM(clazz="org.apache.tomcat.util.net.AprEndpoint$Poller", name="close", desc="(J)V",revertedInput={"this.closeList.size"},definedOutput={"this.closeList.sockets[*]","this.closeList.size","this.closeList.timeouts[*]","this.closeList.flags[*]"},delta={"this.closeList.sockets[*]","this.closeList.timeouts[*]","this.closeList.flags[*]"})
  static void close5374(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (long)0L);
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "closeList"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("v4"), "sockets"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getArrayLength(vm.get("v6")));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getArrayElement(vm.get("v6"), vm.get("v0")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getField(vm.get("v4"), "flags"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getField(vm.get("v4"), "size"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.getField(vm.get("v4"), "timeouts"));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", em.op("isub").eval(vm.get("v14"),vm.get("v2")));
    om.revertField(vm.get("v4"), "size", vm.get("v17"));
    vm.put("v18", om.popArrayElement(vm.get("v6"), vm.get("v17")));
    vm.put("param0", vm.get("v18"));
    vm.put("v19", em.op("lcmp").eval(vm.get("v10"),vm.get("param0")));
    vm.put("v20", om.popArrayElement(vm.get("v16"), vm.get("v17")));
    vm.put("v21", om.popArrayElement(vm.get("v12"), vm.get("v17")));
    if (!em.ne(vm.get("v19"), vm.get("v0"))) e.abort(String.format("!((v19=%s) ne (v0=%s))", vm.get("v19"), vm.get("v0")));
    if (!em.lt(vm.get("v0"), vm.get("v17"))) e.abort(String.format("!((v0=%s) lt (v17=%s))", vm.get("v0"), vm.get("v17")));
    if (!em.ne(vm.get("v17"), vm.get("v8"))) e.abort(String.format("!((v17=%s) ne (v8=%s))", vm.get("v17"), vm.get("v8")));
    if (!em.ge(vm.get("v2"), vm.get("v17"))) e.abort(String.format("!((v2=%s) ge (v17=%s))", vm.get("v2"), vm.get("v17")));
  }
  @IM(clazz="org.apache.tomcat.util.net.AprEndpoint$Poller", name="close", desc="(J)V",revertedInput={"this.closeList.size"},definedOutput={"this.closeList.sockets[*]","this.closeList.size","this.closeList.timeouts[*]","this.closeList.flags[*]"},delta={"this.closeList.sockets[*]","this.closeList.timeouts[*]","this.closeList.flags[*]"})
  static void close5391(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)2);
    vm.put("v1", (int)1);
    vm.put("v2", (int)4);
    vm.put("v3", (int)0);
    vm.put("v4", (int)3);
    vm.put("v5", (long)0L);
    vm.put("v6", om.getField(vm.get("this"), "closeList"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)5);
    vm.put("v9", om.getField(vm.get("v7"), "size"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getField(vm.get("v7"), "sockets"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getArrayElement(vm.get("v12"), vm.get("v1")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.getArrayLength(vm.get("v12")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getArrayElement(vm.get("v12"), vm.get("v3")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", om.getField(vm.get("v7"), "timeouts"));
    vm.put("v20", vm.get("v19"));
    vm.put("v21", om.getArrayElement(vm.get("v12"), vm.get("v2")));
    vm.put("v22", vm.get("v21"));
    vm.put("v23", om.getField(vm.get("v7"), "flags"));
    vm.put("v24", vm.get("v23"));
    vm.put("v25", om.getArrayElement(vm.get("v12"), vm.get("v0")));
    vm.put("v26", vm.get("v25"));
    vm.put("v27", om.getArrayElement(vm.get("v12"), vm.get("v4")));
    vm.put("v28", vm.get("v27"));
    vm.put("v29", em.op("isub").eval(vm.get("v10"),vm.get("v1")));
    om.revertField(vm.get("v7"), "size", vm.get("v29"));
    vm.put("v30", om.popArrayElement(vm.get("v12"), vm.get("v29")));
    vm.put("param0", vm.get("v30"));
    vm.put("v31", em.op("lcmp").eval(vm.get("v28"),vm.get("param0")));
    vm.put("v32", em.op("lcmp").eval(vm.get("v14"),vm.get("param0")));
    vm.put("v33", em.op("lcmp").eval(vm.get("v18"),vm.get("param0")));
    vm.put("v34", em.op("lcmp").eval(vm.get("v22"),vm.get("param0")));
    vm.put("v35", em.op("lcmp").eval(vm.get("v26"),vm.get("param0")));
    vm.put("v36", om.popArrayElement(vm.get("v20"), vm.get("v29")));
    vm.put("v37", om.popArrayElement(vm.get("v24"), vm.get("v29")));
    if (!em.ne(vm.get("v34"), vm.get("v3"))) e.abort(String.format("!((v34=%s) ne (v3=%s))", vm.get("v34"), vm.get("v3")));
    if (!em.lt(vm.get("v4"), vm.get("v29"))) e.abort(String.format("!((v4=%s) lt (v29=%s))", vm.get("v4"), vm.get("v29")));
    if (!em.ne(vm.get("v33"), vm.get("v3"))) e.abort(String.format("!((v33=%s) ne (v3=%s))", vm.get("v33"), vm.get("v3")));
    if (!em.lt(vm.get("v2"), vm.get("v29"))) e.abort(String.format("!((v2=%s) lt (v29=%s))", vm.get("v2"), vm.get("v29")));
    if (!em.lt(vm.get("v0"), vm.get("v29"))) e.abort(String.format("!((v0=%s) lt (v29=%s))", vm.get("v0"), vm.get("v29")));
    if (!em.lt(vm.get("v1"), vm.get("v29"))) e.abort(String.format("!((v1=%s) lt (v29=%s))", vm.get("v1"), vm.get("v29")));
    if (!em.ne(vm.get("v35"), vm.get("v3"))) e.abort(String.format("!((v35=%s) ne (v3=%s))", vm.get("v35"), vm.get("v3")));
    if (!em.ne(vm.get("v29"), vm.get("v16"))) e.abort(String.format("!((v29=%s) ne (v16=%s))", vm.get("v29"), vm.get("v16")));
    if (!em.ge(vm.get("v8"), vm.get("v29"))) e.abort(String.format("!((v8=%s) ge (v29=%s))", vm.get("v8"), vm.get("v29")));
    if (!em.ne(vm.get("v31"), vm.get("v3"))) e.abort(String.format("!((v31=%s) ne (v3=%s))", vm.get("v31"), vm.get("v3")));
    if (!em.ne(vm.get("v32"), vm.get("v3"))) e.abort(String.format("!((v32=%s) ne (v3=%s))", vm.get("v32"), vm.get("v3")));
    if (!em.lt(vm.get("v3"), vm.get("v29"))) e.abort(String.format("!((v3=%s) lt (v29=%s))", vm.get("v3"), vm.get("v29")));
  }
  @IM(clazz="org.apache.tomcat.util.net.AprEndpoint$Poller", name="stop", desc="()V",definedOutput={"this.pollerRunning"},delta={"this.pollerRunning"})
  static void stop5499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "pollerRunning"));
    vm.put("v1", (int)0);
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
  }
}
