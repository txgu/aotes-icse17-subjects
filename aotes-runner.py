#! /usr/bin/python

import os, sys, subprocess, traceback, StringIO
import shutil

try:
    import ConfigParser as CP
except:
    import configparser as CP


DIR=os.path.abspath(os.path.dirname(__file__))
AOTES_DIST = os.getenv('AOTES_DIST', os.path.join(DIR, '../aotes-asm/dist'))
DPG_DIST = os.getenv('DPG_DIST', os.path.join(DIR, '../dpg/dist'))

if os.name == 'nt':
    EXE = 'run.bat'
else:
    EXE = 'run.sh'

AOTES_BIN=os.path.join(AOTES_DIST, EXE)
DPG_BIN=os.path.join(DPG_DIST, EXE)


def read_config(java_properties):
    ini_str = '[root]\n'
    with open(java_properties, 'r') as f:
        ini_str = ini_str + f.read()

    ini_fp = StringIO.StringIO(ini_str)
    config = CP.RawConfigParser()
    config.readfp(ini_fp)
    return { key: config.get('root', key) for key in config.options('root') }


def collect_jar_in(root):
    if not root:
        return ''
    jars = []
    for dirpath, dirnames, filenames in os.walk(root):
        for filename in filenames:
            if filename.endswith('.jar'):
                jars.append(os.path.join(dirpath, filename))

    return os.pathsep.join(jars)


def generate_class_path(*args):
    return os.pathsep.join(args)

def run_with_java_properties(java_properties):
    config = read_config(java_properties)
    config['base'] = os.path.abspath(os.path.dirname(java_properties))
    return run_with_config(config)


def run_with_config(config):
    base = config['base']
    proj_name = config['name']
    old_version = config['old.version']
    new_version = config['new.version']
    lib_dir = config.get('libdir')
    return run(base, proj_name, old_version, new_version, lib_dir)

def run(base, proj_name, old_version, new_version, lib_dir):
    old_dir = os.path.join(base, '%s-%s' % (proj_name, old_version))
    new_dir = os.path.join(base, '%s-%s' % (proj_name, new_version))
    output_dir = os.path.join(old_dir, 'generated')
    if os.path.exists(output_dir):
        # shutil.rmtree(output_dir)
        # os.makedirs(output_dir)
        pass
    else:
        os.makedirs(output_dir)

    lib_dir = os.path.join(base, proj_name + '-' + lib_dir) if lib_dir else None

    old_class_path = collect_jar_in(old_dir)
    new_class_path = collect_jar_in(new_dir)
    lib_class_path = collect_jar_in(lib_dir)

    cmd = [DPG_BIN, '-x', '-d', output_dir, '-o', old_class_path, '-n', new_class_path]
    with open(os.path.join(output_dir, 'dpg.log'), 'w') as dpg_log:
        dpg_log.write('DPG cmds: ' + ' '.join(cmd))
        ret = subprocess.call(cmd, stdout=dpg_log, stderr=dpg_log)

    if ret:
        print('retcode of dpg: ' + str(ret))
        return ret

    cmd = [DPG_BIN, '-d', old_dir, '-o', old_class_path, '-n', new_class_path]
    with open(os.path.join(old_dir, 'dpg.log'), 'w') as dpg_log:
        dpg_log.write('DPG cmds: ' + ' '.join(cmd))
        ret = subprocess.call(cmd, stdout=dpg_log, stderr=dpg_log)

    if ret:
        print('retcode of dpg2: ' + str(ret))
        return ret

    dynamic_patch_path = os.path.join(output_dir, 'javelus.xml')

    vm_opts = ['-Daotes.prune.noaddedfield=false', \
            '-Daotes.prune.unmatched=true', \
            '-Daotes.prune.unreachable=true', \
            '-Daotes.prune.static=true']

    cmd = [AOTES_BIN] + vm_opts + ['-v', '10', '-o', output_dir, '-dp', dynamic_patch_path, '-cp', generate_class_path(old_class_path, lib_class_path)]
    with open(os.path.join(output_dir, 'aotes.log'), 'w') as aotes_log:
        aotes_log.write('AOTES cmds: ' + ' '.join(cmd))
        ret = subprocess.call(cmd, stdout=aotes_log, stderr=aotes_log)
        #ret = subprocess.call(cmd)

    if ret:
        print('retcode of aotes: ' + str(ret))

    return ret

def run_all_in_current_folder(base):
    for f in os.listdir(base):
        if not os.path.isdir(f):
            continue

        if not f.endswith('-previous'):
            continue

        ids = f.split('-')
        if len(ids) != 3:
            continue

        new_version_dir = '-'.join(ids[0:2])
        if not os.path.exists(os.path.join(base, new_version_dir)):
            continue

        name = ids[0]
        old_version = '-'.join(ids[1:])
        new_version = ids[1]
        lib_dir = 'lib'
        with open(os.path.join(base, '-'.join(ids[0:2]) + '.txt'), 'w') as f:
            config = 'name=%s\nold.version=%s\nnew.version=%s\nlibdir=%s' % (name, old_version, new_version, lib_dir)
            print('========================')
            print(config)
            print('========================\n')
            f.write(config)

        ret = run(base, name, old_version, new_version, lib_dir)
        if ret != 0:
            return ret

if __name__ == '__main__':
    try:
        if len(sys.argv) == 2:
            ret = run_with_java_properties(sys.argv[1])
        else:
            ret = run_all_in_current_folder(os.path.dirname(os.path.abspath(__file__)))
        exit(ret)
    except Exception as e:
        traceback.print_exc(e)
        exit(1)
