import org.javelus.aotes.executor.*;
@Defined({"this.server","this.listeners.lock","this.listeners.lock.sync","this.listeners","this.listeners.array"})
public class AOTES_org_apache_sshd_server_session_SessionFactory {
  @IM(clazz="org.apache.sshd.server.session.SessionFactory", name="setServer", desc="(Lorg/apache/sshd/server/ServerFactoryManager;)V",definedOutput={"this.server"},delta={"this.server"})
  static void setServer64599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "server"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.session.SessionFactory", name="<init>", desc="()V",definedOutput={"this.listeners.lock","this.listeners.lock.sync","this.listeners","this.listeners.array"},delta={"this.listeners.lock","this.listeners.lock.sync","this.listeners","this.listeners.array"})
  static void _init_18499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "listeners"));
    vm.put("v1", (int)0);
    vm.put("v2", vm.get("v0"));
    vm.put("v3", om.popField(vm.get("v2"), "array"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getArrayLength(vm.get("v4")));
    vm.put("v6", om.popField(vm.get("v2"), "lock"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.popField(vm.get("v7"), "sync"));
    vm.put("v9", vm.get("v8"));
    if (!em.eq(vm.get("v1"), (vm.get("v5")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v5")));
  }
}
