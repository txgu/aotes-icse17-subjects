import org.javelus.aotes.executor.*;
@Defined({"this.log"})
public class AOTES_org_apache_sshd_server_auth_UserAuthPublicKey {
  @IM(clazz="org.apache.sshd.server.auth.UserAuthPublicKey", name="<init>", desc="()V",definedOutput={"this.log"},delta={"this.log"})
  static void _init_2499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "log"));
    vm.put("v1", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("this")));
    vm.put("v2", em.op("org.slf4j.LoggerFactory.getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;").eval(vm.get("v1")));
    if (!em.eq(vm.get("v2"), (vm.get("v0")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v0")));
  }
}
