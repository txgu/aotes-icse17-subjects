import org.javelus.aotes.executor.*;
@Defined({"org.apache.sshd.common.session.AbstractSession.$assertionsDisabled","this.username","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]"})
public class AOTES_org_apache_sshd_common_session_AbstractSession {
  @IM(clazz="org.apache.sshd.common.session.AbstractSession", name="getInnerCloseable", desc="()Lorg/apache/sshd/common/Closeable;",definedOutput={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]"},delta={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]"})
  static void getInnerCloseable4441(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "currentService"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "lock"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", (int)0);
    vm.put("v6", om.getField(vm.get("this"), "ioSession"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", null);
    vm.put("v11", (int)10);
    vm.put("v12", om.popArrayElement(vm.get("v9"), vm.get("v2")));
    vm.put("v13", om.popArrayElement(vm.get("v9"), vm.get("v5")));
    vm.put("v14", om.getArrayLength(vm.get("v9")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", em.op("isub").eval(vm.get("v11"),vm.get("v15")));
    vm.put("v17", em.op("isub").eval(vm.get("v11"),vm.get("v15")));
    if (!em.ne(vm.get("v7"), vm.get("v10"))) e.abort(String.format("!((v7=%s) ne (v10=%s))", vm.get("v7"), vm.get("v10")));
    if (!em.le(vm.get("v17"), vm.get("v5"))) e.abort(String.format("!((v17=%s) le (v5=%s))", vm.get("v17"), vm.get("v5")));
    if (!em.ne(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) ne (v10=%s))", vm.get("v1"), vm.get("v10")));
    if (!em.le(vm.get("v16"), vm.get("v5"))) e.abort(String.format("!((v16=%s) le (v5=%s))", vm.get("v16"), vm.get("v5")));
    if (!em.ne(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) ne (v10=%s))", vm.get("v1"), vm.get("v10")));
    if (!em.eq(vm.get("v4"), vm.get("v10"))) e.abort(String.format("!((v4=%s) eq (v10=%s))", vm.get("v4"), vm.get("v10")));
  }
  @IM(clazz="org.apache.sshd.common.session.AbstractSession", name="getInnerCloseable", desc="()Lorg/apache/sshd/common/Closeable;",definedOutput={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]"},delta={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]"})
  static void getInnerCloseable4469(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "currentService"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "ioSession"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "lock"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", null);
    vm.put("v11", (int)10);
    vm.put("v12", om.popArrayElement(vm.get("v7"), vm.get("v0")));
    vm.put("v13", om.popArrayElement(vm.get("v7"), vm.get("v5")));
    vm.put("v14", om.getArrayLength(vm.get("v7")));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", em.op("isub").eval(vm.get("v11"),vm.get("v15")));
    vm.put("v17", em.op("isub").eval(vm.get("v11"),vm.get("v15")));
    if (!em.ne(vm.get("v4"), vm.get("v10"))) e.abort(String.format("!((v4=%s) ne (v10=%s))", vm.get("v4"), vm.get("v10")));
    if (!em.le(vm.get("v16"), vm.get("v0"))) e.abort(String.format("!((v16=%s) le (v0=%s))", vm.get("v16"), vm.get("v0")));
    if (!em.ne(vm.get("v2"), vm.get("v10"))) e.abort(String.format("!((v2=%s) ne (v10=%s))", vm.get("v2"), vm.get("v10")));
    if (!em.le(vm.get("v17"), vm.get("v0"))) e.abort(String.format("!((v17=%s) le (v0=%s))", vm.get("v17"), vm.get("v0")));
    if (!em.ne(vm.get("v2"), vm.get("v10"))) e.abort(String.format("!((v2=%s) ne (v10=%s))", vm.get("v2"), vm.get("v10")));
    if (!em.ne(vm.get("v9"), vm.get("v10"))) e.abort(String.format("!((v9=%s) ne (v10=%s))", vm.get("v9"), vm.get("v10")));
  }
  @IM(clazz="org.apache.sshd.common.session.AbstractSession", name="setUsername", desc="(Ljava/lang/String;)V",definedOutput={"this.username"},delta={"this.username"})
  static void setUsername34199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "username"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.common.session.AbstractSession", name="<clinit>", desc="()V",definedOutput={"org.apache.sshd.common.session.AbstractSession.$assertionsDisabled"},delta={"org.apache.sshd.common.session.AbstractSession.$assertionsDisabled"})
  static void _clinit_53199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", om.popStatic("org.apache.sshd.common.session.AbstractSession.$assertionsDisabled"));
    vm.put("v1", (int)0);
    vm.put("v2", org.apache.sshd.common.session.AbstractSession.class);
    vm.put("v3", em.op("java.lang.Class.desiredAssertionStatus()Z").eval(vm.get("v2")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.ne(vm.get("v3"), vm.get("v1"))) e.abort(String.format("!((v3=%s) ne (v1=%s))", vm.get("v3"), vm.get("v1")));
  }
  @IM(clazz="org.apache.sshd.common.session.AbstractSession", name="<clinit>", desc="()V",definedOutput={"org.apache.sshd.common.session.AbstractSession.$assertionsDisabled"},delta={"org.apache.sshd.common.session.AbstractSession.$assertionsDisabled"})
  static void _clinit_53198(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", om.popStatic("org.apache.sshd.common.session.AbstractSession.$assertionsDisabled"));
    vm.put("v1", (int)1);
    vm.put("v2", org.apache.sshd.common.session.AbstractSession.class);
    vm.put("v3", (int)0);
    vm.put("v4", em.op("java.lang.Class.desiredAssertionStatus()Z").eval(vm.get("v2")));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v4"), vm.get("v3"))) e.abort(String.format("!((v4=%s) eq (v3=%s))", vm.get("v4"), vm.get("v3")));
  }
}
