import org.javelus.aotes.executor.*;
@Defined({"this.username","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables","org.apache.sshd.client.session.ClientSessionImpl.PASSWORD_IDENTITY_COMPARATOR","this.scpListener","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closing","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a","org.apache.sshd.client.session.ClientSessionImpl.KEYPAIR_IDENTITY_COMPARATOR","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future","this.userInteraction","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.modCount","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.lock"})
public class AOTES_org_apache_sshd_client_session_ClientSessionImpl {
  @IM(clazz="org.apache.sshd.client.session.ClientSessionImpl", name="setUserInteraction", desc="(Lorg/apache/sshd/client/UserInteraction;)V",definedOutput={"this.userInteraction"},delta={"this.userInteraction"})
  static void setUserInteraction74099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "userInteraction"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.common.session.AbstractSession", name="setUsername", desc="(Ljava/lang/String;)V",definedOutput={"this.username"},delta={"this.username"})
  static void setUsername25499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "username"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.client.session.ClientSessionImpl", name="setScpTransferEventListener", desc="(Lorg/apache/sshd/common/scp/ScpTransferEventListener;)V",definedOutput={"this.scpListener"},delta={"this.scpListener"})
  static void setScpTransferEventListener49099(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "scpListener"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.common.session.AbstractSession", name="getInnerCloseable", desc="()Lorg/apache/sshd/common/Closeable;",definedOutput={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closing","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.lock","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.modCount"},delta={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closing","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.lock","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.modCount"})
  static void getInnerCloseable19069(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "ioSession"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "nextService"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", (int)2);
    vm.put("v7", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getArrayLength(vm.get("v8")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getField(vm.get("this"), "currentService"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getField(vm.get("this"), "lock"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", null);
    vm.put("v16", (int)10);
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v10")));
    vm.put("v18", om.popArrayElement(vm.get("v8"), vm.get("v1")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", om.popField(vm.get("v19"), "log"));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.popField(vm.get("v19"), "future"));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.popField(vm.get("v23"), "log"));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v23")));
    vm.put("v27", om.popField(vm.get("v19"), "closeables"));
    vm.put("v28", vm.get("v27"));
    vm.put("v29", om.popField(vm.get("v28"), "modCount"));
    vm.put("v30", om.popField(vm.get("v28"), "a"));
    vm.put("v31", vm.get("v30"));
    vm.put("v32", om.getArrayLength(vm.get("v31")));
    vm.put("v33", om.popArrayElement(vm.get("v31"), vm.get("v0")));
    vm.put("v34", om.popField(vm.get("v23"), "lock"));
    vm.put("v35", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v19")));
    vm.put("v36", om.popArrayElement(vm.get("v31"), vm.get("v1")));
    vm.put("v37", om.popField(vm.get("v19"), "closing"));
    vm.put("v38", vm.get("v37"));
    vm.put("v39", em.op("isub").eval(vm.get("v16"),vm.get("v10")));
    vm.put("v40", om.popArrayElement(vm.get("v8"), vm.get("v0")));
    if (!em.eq(vm.get("v1"), (vm.get("v29")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v29")));
    if (!em.eq(vm.get("v23"), (vm.get("v34")))) e.abort("Inconsistent value for \"v23\": " + vm.get("v23") + " ne " + (vm.get("v34")));
    if (!em.eq(vm.get("v6"), (vm.get("v32")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v32")));
    if (!em.ne(vm.get("v14"), vm.get("v15"))) e.abort(String.format("!((v14=%s) ne (v15=%s))", vm.get("v14"), vm.get("v15")));
    if (!em.le(vm.get("v39"), vm.get("v1"))) e.abort(String.format("!((v39=%s) le (v1=%s))", vm.get("v39"), vm.get("v1")));
    if (!em.ne(vm.get("v3"), vm.get("v15"))) e.abort(String.format("!((v3=%s) ne (v15=%s))", vm.get("v3"), vm.get("v15")));
    if (!em.ne(vm.get("v5"), vm.get("v15"))) e.abort(String.format("!((v5=%s) ne (v15=%s))", vm.get("v5"), vm.get("v15")));
    if (!em.le(vm.get("v17"), vm.get("v1"))) e.abort(String.format("!((v17=%s) le (v1=%s))", vm.get("v17"), vm.get("v1")));
    if (!em.eq(vm.get("v14"), vm.get("v15"))) e.abort(String.format("!((v14=%s) eq (v15=%s))", vm.get("v14"), vm.get("v15")));
  }
  @IM(clazz="org.apache.sshd.common.session.AbstractSession", name="getInnerCloseable", desc="()Lorg/apache/sshd/common/Closeable;",definedOutput={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closing","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.lock","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.modCount"},delta={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closing","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.lock","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.modCount"})
  static void getInnerCloseable19094(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", om.getField(vm.get("this"), "nextService"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", (int)0);
    vm.put("v6", om.getField(vm.get("this"), "lock"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)2);
    vm.put("v9", om.getArrayLength(vm.get("v4")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.popArrayElement(vm.get("v4"), vm.get("v5")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getField(vm.get("this"), "currentService"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.getField(vm.get("this"), "ioSession"));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.popField(vm.get("v12"), "closeables"));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", null);
    vm.put("v20", (int)10);
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v10")));
    vm.put("v22", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v12")));
    vm.put("v23", om.popField(vm.get("v12"), "future"));
    vm.put("v24", vm.get("v23"));
    vm.put("v25", om.popField(vm.get("v18"), "modCount"));
    vm.put("v26", om.popField(vm.get("v12"), "log"));
    vm.put("v27", vm.get("v26"));
    vm.put("v28", om.popField(vm.get("v24"), "log"));
    vm.put("v29", vm.get("v28"));
    vm.put("v30", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v24")));
    vm.put("v31", om.popField(vm.get("v12"), "closing"));
    vm.put("v32", vm.get("v31"));
    vm.put("v33", em.op("isub").eval(vm.get("v20"),vm.get("v10")));
    vm.put("v34", om.popField(vm.get("v18"), "a"));
    vm.put("v35", vm.get("v34"));
    vm.put("v36", om.popArrayElement(vm.get("v4"), vm.get("v0")));
    vm.put("v37", om.popField(vm.get("v24"), "lock"));
    vm.put("v38", om.popArrayElement(vm.get("v35"), vm.get("v0")));
    vm.put("v39", om.getArrayLength(vm.get("v35")));
    vm.put("v40", om.popArrayElement(vm.get("v35"), vm.get("v5")));
    if (!em.eq(vm.get("v24"), (vm.get("v37")))) e.abort("Inconsistent value for \"v24\": " + vm.get("v24") + " ne " + (vm.get("v37")));
    if (!em.eq(vm.get("v5"), (vm.get("v25")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v25")));
    if (!em.eq(vm.get("v8"), (vm.get("v39")))) e.abort("Inconsistent value for \"v8\": " + vm.get("v8") + " ne " + (vm.get("v39")));
    if (!em.le(vm.get("v21"), vm.get("v5"))) e.abort(String.format("!((v21=%s) le (v5=%s))", vm.get("v21"), vm.get("v5")));
    if (!em.ne(vm.get("v2"), vm.get("v19"))) e.abort(String.format("!((v2=%s) ne (v19=%s))", vm.get("v2"), vm.get("v19")));
    if (!em.eq(vm.get("v7"), vm.get("v19"))) e.abort(String.format("!((v7=%s) eq (v19=%s))", vm.get("v7"), vm.get("v19")));
    if (!em.eq(vm.get("v7"), vm.get("v19"))) e.abort(String.format("!((v7=%s) eq (v19=%s))", vm.get("v7"), vm.get("v19")));
    if (!em.ne(vm.get("v16"), vm.get("v19"))) e.abort(String.format("!((v16=%s) ne (v19=%s))", vm.get("v16"), vm.get("v19")));
    if (!em.le(vm.get("v33"), vm.get("v5"))) e.abort(String.format("!((v33=%s) le (v5=%s))", vm.get("v33"), vm.get("v5")));
  }
  @IM(clazz="org.apache.sshd.common.session.AbstractSession", name="getInnerCloseable", desc="()Lorg/apache/sshd/common/Closeable;",definedOutput={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closing","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.lock","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.modCount"},delta={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closing","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.lock","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.modCount"})
  static void getInnerCloseable19090(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "nextService"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getArrayLength(vm.get("v3")));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", (int)0);
    vm.put("v7", om.getField(vm.get("this"), "ioSession"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("this"), "currentService"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)1);
    vm.put("v12", (int)2);
    vm.put("v13", om.getField(vm.get("this"), "lock"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.popArrayElement(vm.get("v3"), vm.get("v6")));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", null);
    vm.put("v18", (int)10);
    vm.put("v19", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v16")));
    vm.put("v20", em.op("org.slf4j.LoggerFactory.getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;").eval(vm.get("v19")));
    vm.put("v21", om.popField(vm.get("v16"), "future"));
    vm.put("v22", vm.get("v21"));
    vm.put("v23", om.popField(vm.get("v16"), "closing"));
    vm.put("v24", vm.get("v23"));
    vm.put("v25", om.popField(vm.get("v16"), "closeables"));
    vm.put("v26", vm.get("v25"));
    vm.put("v27", om.popField(vm.get("v22"), "log"));
    vm.put("v28", vm.get("v27"));
    vm.put("v29", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v22")));
    vm.put("v30", om.popField(vm.get("v26"), "a"));
    vm.put("v31", vm.get("v30"));
    vm.put("v32", om.popArrayElement(vm.get("v31"), vm.get("v11")));
    vm.put("v33", om.popField(vm.get("v22"), "lock"));
    vm.put("v34", om.popField(vm.get("v16"), "log"));
    vm.put("v35", om.popArrayElement(vm.get("v31"), vm.get("v6")));
    vm.put("v36", om.getArrayLength(vm.get("v31")));
    vm.put("v37", em.op("isub").eval(vm.get("v18"),vm.get("v5")));
    vm.put("v38", om.popField(vm.get("v26"), "modCount"));
    if (!em.eq(vm.get("v22"), (vm.get("v33")))) e.abort("Inconsistent value for \"v22\": " + vm.get("v22") + " ne " + (vm.get("v33")));
    if (!em.eq(vm.get("v20"), (vm.get("v34")))) e.abort("Inconsistent value for \"v20\": " + vm.get("v20") + " ne " + (vm.get("v34")));
    if (!em.eq(vm.get("v12"), (vm.get("v36")))) e.abort("Inconsistent value for \"v12\": " + vm.get("v12") + " ne " + (vm.get("v36")));
    if (!em.eq(vm.get("v6"), (vm.get("v38")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v38")));
    if (!em.eq(vm.get("v8"), vm.get("v17"))) e.abort(String.format("!((v8=%s) eq (v17=%s))", vm.get("v8"), vm.get("v17")));
    if (!em.le(vm.get("v37"), vm.get("v6"))) e.abort(String.format("!((v37=%s) le (v6=%s))", vm.get("v37"), vm.get("v6")));
    if (!em.ne(vm.get("v1"), vm.get("v17"))) e.abort(String.format("!((v1=%s) ne (v17=%s))", vm.get("v1"), vm.get("v17")));
    if (!em.eq(vm.get("v14"), vm.get("v17"))) e.abort(String.format("!((v14=%s) eq (v17=%s))", vm.get("v14"), vm.get("v17")));
  }
  @IM(clazz="org.apache.sshd.common.session.AbstractSession", name="getInnerCloseable", desc="()Lorg/apache/sshd/common/Closeable;",definedOutput={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closing","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.modCount","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.lock"},delta={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closing","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.modCount","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.lock"})
  static void getInnerCloseable19068(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "currentService"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "nextService"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.popArrayElement(vm.get("v6"), vm.get("v2")));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", (int)0);
    vm.put("v10", om.getField(vm.get("this"), "lock"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("this"), "ioSession"));
    vm.put("v13", (int)2);
    vm.put("v14", null);
    vm.put("v15", (int)10);
    vm.put("v16", om.getArrayLength(vm.get("v6")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", em.op("isub").eval(vm.get("v15"),vm.get("v17")));
    vm.put("v19", em.op("isub").eval(vm.get("v15"),vm.get("v17")));
    vm.put("v20", om.popArrayElement(vm.get("v6"), vm.get("v9")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v21")));
    vm.put("v23", em.op("org.slf4j.LoggerFactory.getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;").eval(vm.get("v22")));
    vm.put("v24", om.popField(vm.get("v21"), "closing"));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", om.popField(vm.get("v21"), "log"));
    vm.put("v27", om.popField(vm.get("v21"), "future"));
    vm.put("v28", vm.get("v27"));
    vm.put("v29", om.popField(vm.get("v28"), "log"));
    vm.put("v30", vm.get("v29"));
    vm.put("v31", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v28")));
    vm.put("v32", om.popField(vm.get("v28"), "lock"));
    vm.put("v33", om.popField(vm.get("v21"), "closeables"));
    vm.put("v34", vm.get("v33"));
    vm.put("v35", om.popField(vm.get("v34"), "modCount"));
    vm.put("v36", om.popField(vm.get("v34"), "a"));
    vm.put("v37", vm.get("v36"));
    vm.put("v38", om.popArrayElement(vm.get("v37"), vm.get("v2")));
    vm.put("v39", om.getArrayLength(vm.get("v37")));
    vm.put("v40", om.popArrayElement(vm.get("v37"), vm.get("v9")));
    if (!em.eq(vm.get("v9"), (vm.get("v35")))) e.abort("Inconsistent value for \"v9\": " + vm.get("v9") + " ne " + (vm.get("v35")));
    if (!em.eq(vm.get("v8"), (vm.get("v12")))) e.abort("Inconsistent value for \"v8\": " + vm.get("v8") + " ne " + (vm.get("v12")));
    if (!em.eq(vm.get("v13"), (vm.get("v39")))) e.abort("Inconsistent value for \"v13\": " + vm.get("v13") + " ne " + (vm.get("v39")));
    if (!em.eq(vm.get("v23"), (vm.get("v26")))) e.abort("Inconsistent value for \"v23\": " + vm.get("v23") + " ne " + (vm.get("v26")));
    if (!em.eq(vm.get("v11"), (vm.get("v32")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v32")));
    if (!em.ne(vm.get("v8"), vm.get("v14"))) e.abort(String.format("!((v8=%s) ne (v14=%s))", vm.get("v8"), vm.get("v14")));
    if (!em.ne(vm.get("v4"), vm.get("v14"))) e.abort(String.format("!((v4=%s) ne (v14=%s))", vm.get("v4"), vm.get("v14")));
    if (!em.le(vm.get("v19"), vm.get("v9"))) e.abort(String.format("!((v19=%s) le (v9=%s))", vm.get("v19"), vm.get("v9")));
    if (!em.ne(vm.get("v11"), vm.get("v14"))) e.abort(String.format("!((v11=%s) ne (v14=%s))", vm.get("v11"), vm.get("v14")));
    if (!em.le(vm.get("v18"), vm.get("v9"))) e.abort(String.format("!((v18=%s) le (v9=%s))", vm.get("v18"), vm.get("v9")));
    if (!em.ne(vm.get("v11"), vm.get("v14"))) e.abort(String.format("!((v11=%s) ne (v14=%s))", vm.get("v11"), vm.get("v14")));
  }
  @IM(clazz="org.apache.sshd.common.session.AbstractSession", name="getInnerCloseable", desc="()Lorg/apache/sshd/common/Closeable;",definedOutput={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closing","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.modCount","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.lock"},delta={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closing","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.a","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].log","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].closeables.modCount","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*].future.lock"})
  static void getInnerCloseable19086(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", om.getField(vm.get("this"), "ioSession"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "lock"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.popArrayElement(vm.get("v6"), vm.get("v0")));
    vm.put("v8", (int)1);
    vm.put("v9", om.getArrayLength(vm.get("v6")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)2);
    vm.put("v12", om.getField(vm.get("this"), "currentService"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("this"), "nextService"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", null);
    vm.put("v17", (int)10);
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v10")));
    vm.put("v19", vm.get("v7"));
    vm.put("v20", om.popField(vm.get("v19"), "log"));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.popField(vm.get("v19"), "closing"));
    vm.put("v23", vm.get("v22"));
    vm.put("v24", om.popField(vm.get("v19"), "closeables"));
    vm.put("v25", vm.get("v24"));
    vm.put("v26", om.popField(vm.get("v19"), "future"));
    vm.put("v27", vm.get("v26"));
    vm.put("v28", om.popField(vm.get("v27"), "log"));
    vm.put("v29", vm.get("v28"));
    vm.put("v30", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v27")));
    vm.put("v31", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("v19")));
    vm.put("v32", om.popField(vm.get("v25"), "modCount"));
    vm.put("v33", om.popField(vm.get("v27"), "lock"));
    vm.put("v34", om.popField(vm.get("v25"), "a"));
    vm.put("v35", vm.get("v34"));
    vm.put("v36", om.getArrayLength(vm.get("v35")));
    vm.put("v37", om.popArrayElement(vm.get("v35"), vm.get("v0")));
    vm.put("v38", om.popArrayElement(vm.get("v35"), vm.get("v8")));
    if (!em.eq(vm.get("v4"), (vm.get("v33")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v33")));
    if (!em.eq(vm.get("v11"), (vm.get("v36")))) e.abort("Inconsistent value for \"v11\": " + vm.get("v11") + " ne " + (vm.get("v36")));
    if (!em.eq(vm.get("v0"), (vm.get("v32")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v32")));
    if (!em.eq(vm.get("v2"), vm.get("v16"))) e.abort(String.format("!((v2=%s) eq (v16=%s))", vm.get("v2"), vm.get("v16")));
    if (!em.ne(vm.get("v4"), vm.get("v16"))) e.abort(String.format("!((v4=%s) ne (v16=%s))", vm.get("v4"), vm.get("v16")));
    if (!em.le(vm.get("v18"), vm.get("v0"))) e.abort(String.format("!((v18=%s) le (v0=%s))", vm.get("v18"), vm.get("v0")));
    if (!em.ne(vm.get("v15"), vm.get("v16"))) e.abort(String.format("!((v15=%s) ne (v16=%s))", vm.get("v15"), vm.get("v16")));
  }
  @IM(clazz="org.apache.sshd.client.session.ClientSessionImpl", name="<clinit>", desc="()V",definedOutput={"org.apache.sshd.client.session.ClientSessionImpl.PASSWORD_IDENTITY_COMPARATOR","org.apache.sshd.client.session.ClientSessionImpl.KEYPAIR_IDENTITY_COMPARATOR"},delta={"org.apache.sshd.client.session.ClientSessionImpl.PASSWORD_IDENTITY_COMPARATOR","org.apache.sshd.client.session.ClientSessionImpl.KEYPAIR_IDENTITY_COMPARATOR"})
  static void _clinit_10499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", om.popStatic("org.apache.sshd.client.session.ClientSessionImpl.KEYPAIR_IDENTITY_COMPARATOR"));
    vm.put("v1", om.popStatic("org.apache.sshd.client.session.ClientSessionImpl.PASSWORD_IDENTITY_COMPARATOR"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", vm.get("v0"));
  }
}
