import org.javelus.aotes.executor.*;
@Defined({"this.keyType","this.keyData"})
public class AOTES_org_apache_sshd_common_config_keys_PublicKeyEntry {
  @IM(clazz="org.apache.sshd.common.config.keys.PublicKeyEntry", name="<init>", desc="(Ljava/lang/String;[B)V",definedOutput={"this.keyType","this.keyData"},delta={"this.keyType","this.keyData"})
  static void _init_6799(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "keyData"));
    vm.put("v1", om.popField(vm.get("this"), "keyType"));
    vm.put("param0", vm.get("v1"));
    vm.put("param1", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.common.config.keys.PublicKeyEntry", name="setKeyData", desc="([B)V",definedOutput={"this.keyData"},delta={"this.keyData"})
  static void setKeyData66599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "keyData"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.common.config.keys.PublicKeyEntry", name="setKeyType", desc="(Ljava/lang/String;)V",definedOutput={"this.keyType"},delta={"this.keyType"})
  static void setKeyType71599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "keyType"));
    vm.put("param0", vm.get("v0"));
  }
}
