import org.javelus.aotes.executor.*;
@Defined({"org.apache.sshd.common.util.GenericUtils.naturalOrderComparator","org.apache.sshd.common.util.GenericUtils.EMPTY_STRING_ARRAY","org.apache.sshd.common.util.GenericUtils.EMPTY_BYTE_ARRAY","org.apache.sshd.common.util.GenericUtils.EMPTY_OBJECT_ARRAY"})
public class AOTES_org_apache_sshd_common_util_GenericUtils {
  @IM(clazz="org.apache.sshd.common.util.GenericUtils", name="<clinit>", desc="()V",definedOutput={"org.apache.sshd.common.util.GenericUtils.naturalOrderComparator","org.apache.sshd.common.util.GenericUtils.EMPTY_STRING_ARRAY","org.apache.sshd.common.util.GenericUtils.EMPTY_BYTE_ARRAY","org.apache.sshd.common.util.GenericUtils.EMPTY_OBJECT_ARRAY"},delta={"org.apache.sshd.common.util.GenericUtils.naturalOrderComparator","org.apache.sshd.common.util.GenericUtils.EMPTY_STRING_ARRAY","org.apache.sshd.common.util.GenericUtils.EMPTY_BYTE_ARRAY","org.apache.sshd.common.util.GenericUtils.EMPTY_OBJECT_ARRAY"})
  static void _clinit_67499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", (int)0);
    vm.put("v1", om.popStatic("org.apache.sshd.common.util.GenericUtils.naturalOrderComparator"));
    vm.put("v2", om.popStatic("org.apache.sshd.common.util.GenericUtils.EMPTY_STRING_ARRAY"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.popStatic("org.apache.sshd.common.util.GenericUtils.EMPTY_BYTE_ARRAY"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popStatic("org.apache.sshd.common.util.GenericUtils.EMPTY_OBJECT_ARRAY"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getArrayLength(vm.get("v7")));
    vm.put("v9", om.getArrayLength(vm.get("v3")));
    vm.put("v10", om.getArrayLength(vm.get("v5")));
    vm.put("v11", vm.get("v1"));
    if (!em.eq(vm.get("v0"), (vm.get("v8")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v8")));
    if (!em.eq(vm.get("v0"), (vm.get("v9")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v9")));
    if (!em.eq(vm.get("v0"), (vm.get("v10")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v10")));
  }
}
