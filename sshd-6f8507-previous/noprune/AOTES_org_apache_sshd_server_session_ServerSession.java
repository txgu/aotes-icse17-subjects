import org.javelus.aotes.executor.*;
@Defined({"this.username","java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]"})
public class AOTES_org_apache_sshd_server_session_ServerSession {
  @IM(clazz="org.apache.sshd.common.session.AbstractSession", name="getInnerCloseable", desc="()Lorg/apache/sshd/common/Closeable;",definedOutput={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]"},delta={"java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA[*]"})
  static void getInnerCloseable39168(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "ioSession"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", (int)0);
    vm.put("v6", om.getField(vm.get("this"), "currentService"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "lock"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", null);
    vm.put("v11", (int)10);
    vm.put("v12", om.getArrayLength(vm.get("v4")));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", em.op("isub").eval(vm.get("v11"),vm.get("v13")));
    vm.put("v15", em.op("isub").eval(vm.get("v11"),vm.get("v13")));
    vm.put("v16", om.popArrayElement(vm.get("v4"), vm.get("v5")));
    vm.put("v17", om.popArrayElement(vm.get("v4"), vm.get("v2")));
    if (!em.le(vm.get("v15"), vm.get("v5"))) e.abort(String.format("!((v15=%s) le (v5=%s))", vm.get("v15"), vm.get("v5")));
    if (!em.ne(vm.get("v1"), vm.get("v10"))) e.abort(String.format("!((v1=%s) ne (v10=%s))", vm.get("v1"), vm.get("v10")));
    if (!em.eq(vm.get("v9"), vm.get("v10"))) e.abort(String.format("!((v9=%s) eq (v10=%s))", vm.get("v9"), vm.get("v10")));
    if (!em.le(vm.get("v14"), vm.get("v5"))) e.abort(String.format("!((v14=%s) le (v5=%s))", vm.get("v14"), vm.get("v5")));
    if (!em.ne(vm.get("v7"), vm.get("v10"))) e.abort(String.format("!((v7=%s) ne (v10=%s))", vm.get("v7"), vm.get("v10")));
    if (!em.ne(vm.get("v7"), vm.get("v10"))) e.abort(String.format("!((v7=%s) ne (v10=%s))", vm.get("v7"), vm.get("v10")));
  }
  @IM(clazz="org.apache.sshd.common.session.AbstractSession", name="setUsername", desc="(Ljava/lang/String;)V",definedOutput={"this.username"},delta={"this.username"})
  static void setUsername399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "username"));
    vm.put("param0", vm.get("v0"));
  }
}
