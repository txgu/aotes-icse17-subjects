import org.javelus.aotes.executor.*;
@Defined({"this.val$path","this.this$0","this.val$options"})
public class AOTES_org_apache_sshd_client_sftp_SftpFileSystemProvider$SftpFileSystemProvider$2 {
  @IM(clazz="org.apache.sshd.client.sftp.SftpFileSystemProvider$2", name="<init>", desc="(Lorg/apache/sshd/client/sftp/SftpFileSystemProvider;Ljava/nio/file/Path;[Ljava/nio/file/LinkOption;)V",definedOutput={"this.val$path","this.this$0","this.val$options"},delta={"this.val$path","this.this$0","this.val$options"})
  static void _init_63599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "val$options"));
    vm.put("v1", om.popField(vm.get("this"), "this$0"));
    vm.put("v2", om.popField(vm.get("this"), "val$path"));
    vm.put("param0", vm.get("v1"));
    vm.put("param2", vm.get("v0"));
    vm.put("param1", vm.get("v2"));
  }
}
