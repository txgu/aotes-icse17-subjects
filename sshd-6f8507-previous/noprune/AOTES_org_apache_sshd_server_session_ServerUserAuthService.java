import org.javelus.aotes.executor.*;
@Defined({"this.closeFuture.result","this.state.value"})
public class AOTES_org_apache_sshd_server_session_ServerUserAuthService {
  @IM(clazz="org.apache.sshd.common.util.CloseableUtils$AbstractCloseable", name="doCloseImmediately", desc="()V",definedOutput={"this.state.value"},delta={"this.state.value"})
  static void doCloseImmediately29699(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("org.apache.sshd.common.util.CloseableUtils$AbstractCloseable$State.Closed"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "state"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.popField(vm.get("v5"), "value"));
    vm.put("v7", om.getField(vm.get("this"), "closeFuture"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", null);
    vm.put("v10", om.getField(vm.get("v8"), "result"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getField(vm.get("v8"), "lock"));
    vm.put("v13", vm.get("v12"));
    if (!em.eq(vm.get("v3"), (vm.get("v6")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v6")));
    if (!em.ne(vm.get("v11"), vm.get("v9"))) e.abort(String.format("!((v11=%s) ne (v9=%s))", vm.get("v11"), vm.get("v9")));
  }
  @IM(clazz="org.apache.sshd.common.util.CloseableUtils$AbstractCloseable", name="doCloseImmediately", desc="()V",revertedInput={"this.closeFuture.result"},definedOutput={"this.closeFuture.result","this.state.value"},delta={"this.state.value"})
  static void doCloseImmediately29686(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getStatic("org.apache.sshd.common.util.CloseableUtils$AbstractCloseable$State.Closed"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("org.apache.sshd.common.future.DefaultSshFuture.NULL"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "closeFuture"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("this"), "state"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", null);
    vm.put("v11", om.getField(vm.get("v7"), "lock"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getField(vm.get("v7"), "result"));
    vm.put("v14", om.getField(vm.get("v7"), "listeners"));
    vm.put("v15", vm.get("v14"));
    vm.put("v16", om.popField(vm.get("v9"), "value"));
    if (!em.eq(vm.get("v1"), (vm.get("v16")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v16")));
    if (!em.eq(vm.get("v5"), (vm.get("v13")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v13")));
    om.revertField(vm.get("v7"), "result");
    vm.put("v17", om.getField(vm.get("v7"), "result"));
    if (!em.eq(vm.get("v15"), vm.get("v10"))) e.abort(String.format("!((v15=%s) eq (v10=%s))", vm.get("v15"), vm.get("v10")));
    if (!em.eq(vm.get("v17"), vm.get("v10"))) e.abort(String.format("!((v17=%s) eq (v10=%s))", vm.get("v17"), vm.get("v10")));
    if (!em.eq(vm.get("v3"), vm.get("v10"))) e.abort(String.format("!((v3=%s) eq (v10=%s))", vm.get("v3"), vm.get("v10")));
  }
  @IM(clazz="org.apache.sshd.common.util.CloseableUtils$AbstractCloseable", name="doCloseImmediately", desc="()V",revertedInput={"this.closeFuture.result"},definedOutput={"this.closeFuture.result","this.state.value"},delta={"this.state.value"})
  static void doCloseImmediately29693(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "state"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.popField(vm.get("v3"), "value"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "closeFuture"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", om.getField(vm.get("v7"), "listeners"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.getStatic("org.apache.sshd.common.util.CloseableUtils$AbstractCloseable$State.Closed"));
    vm.put("v11", null);
    vm.put("v12", om.getField(vm.get("v7"), "lock"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("v7"), "result"));
    if (!em.eq(vm.get("v1"), (vm.get("v14")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v14")));
    if (!em.eq(vm.get("v5"), (vm.get("v10")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v10")));
    om.revertField(vm.get("v7"), "result");
    vm.put("v15", om.getField(vm.get("v7"), "result"));
    if (!em.ne(vm.get("v1"), vm.get("v11"))) e.abort(String.format("!((v1=%s) ne (v11=%s))", vm.get("v1"), vm.get("v11")));
    if (!em.eq(vm.get("v9"), vm.get("v11"))) e.abort(String.format("!((v9=%s) eq (v11=%s))", vm.get("v9"), vm.get("v11")));
    if (!em.eq(vm.get("v15"), vm.get("v11"))) e.abort(String.format("!((v15=%s) eq (v11=%s))", vm.get("v15"), vm.get("v11")));
  }
}
