import org.javelus.aotes.executor.*;
@Defined({"this.allowMoreSessions"})
public class AOTES_org_apache_sshd_server_session_ServerConnectionService {
  @IM(clazz="org.apache.sshd.common.session.AbstractConnectionService", name="setAllowMoreSessions", desc="(Z)V",definedOutput={"this.allowMoreSessions"},delta={"this.allowMoreSessions"})
  static void setAllowMoreSessions48999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "allowMoreSessions"));
    vm.put("param0", vm.get("v0"));
  }
}
