import org.javelus.aotes.executor.*;
@Defined({"this.root","this.names","this.fileSystem"})
public class AOTES_org_apache_sshd_common_file_util_BasePath {
  @IM(clazz="org.apache.sshd.common.file.util.BasePath", name="<init>", desc="(Lorg/apache/sshd/common/file/util/BaseFileSystem;Ljava/lang/String;Lorg/apache/sshd/common/file/util/ImmutableList;)V",definedOutput={"this.root","this.names","this.fileSystem"},delta={"this.root","this.names","this.fileSystem"})
  static void _init_31199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "names"));
    vm.put("v1", om.popField(vm.get("this"), "root"));
    vm.put("v2", om.popField(vm.get("this"), "fileSystem"));
    vm.put("param2", vm.get("v0"));
    vm.put("param0", vm.get("v2"));
    vm.put("param1", vm.get("v1"));
  }
}
