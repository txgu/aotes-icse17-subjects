import org.javelus.aotes.executor.*;
@Defined({"this.result","this.lock","this.log","this.listeners"})
public class AOTES_org_apache_sshd_client_future_DefaultAuthFuture {
  @IM(clazz="org.apache.sshd.client.future.DefaultAuthFuture", name="<init>", desc="(Ljava/lang/Object;)V",definedOutput={"this.lock","this.log"},delta={"this.lock","this.log"})
  static void _init_13899(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "log"));
    vm.put("v1", om.popField(vm.get("this"), "lock"));
    vm.put("v2", null);
    vm.put("v3", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("this")));
    vm.put("v4", em.op("org.slf4j.LoggerFactory.getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;").eval(vm.get("v3")));
    vm.put("param0", vm.get("v1"));
    if (!em.eq(vm.get("v4"), (vm.get("v0")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v0")));
    if (!em.ne(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) ne (v2=%s))", vm.get("param0"), vm.get("v2")));
  }
  @IM(clazz="org.apache.sshd.client.future.DefaultAuthFuture", name="<init>", desc="(Ljava/lang/Object;)V",definedOutput={"this.lock","this.log"},delta={"this.lock","this.log"})
  static void _init_13894(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "log"));
    vm.put("v1", om.popField(vm.get("this"), "lock"));
    vm.put("v2", null);
    vm.put("v3", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("this")));
    vm.put("v4", em.op("org.slf4j.LoggerFactory.getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;").eval(vm.get("v3")));
    if (!em.eq(vm.get("this"), (vm.get("v1")))) e.abort("Inconsistent value for \"this\": " + vm.get("this") + " ne " + (vm.get("v1")));
    if (!em.eq(vm.get("v4"), (vm.get("v0")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v0")));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    if (!em.eq(vm.get("param0"), vm.get("v2"))) e.abort(String.format("!((param0=%s) eq (v2=%s))", vm.get("param0"), vm.get("v2")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="addListener", desc="(Lorg/apache/sshd/common/future/SshFutureListener;)Lorg/apache/sshd/common/future/SshFuture;",revertedInput={"this.listeners"},definedOutput={"this.listeners"})
  static void addListener2199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "listeners"));
    vm.put("v1", om.getField(vm.get("this"), "result"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "lock"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", null);
    vm.put("param0", vm.get("v0"));
    om.revertField(vm.get("this"), "listeners");
    vm.put("v6", om.getField(vm.get("this"), "listeners"));
    if (!em.eq(vm.get("v6"), vm.get("v5"))) e.abort(String.format("!((v6=%s) eq (v5=%s))", vm.get("v6"), vm.get("v5")));
    if (!em.eq(vm.get("v2"), vm.get("v5"))) e.abort(String.format("!((v2=%s) eq (v5=%s))", vm.get("v2"), vm.get("v5")));
    if (!em.ne(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) ne (v5=%s))", vm.get("param0"), vm.get("v5")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="setValue", desc="(Ljava/lang/Object;)V",revertedInput={"this.result"},definedOutput={"this.result"})
  static void setValue58996(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "result"));
    vm.put("v1", om.getField(vm.get("this"), "listeners"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "lock"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", null);
    vm.put("param0", vm.get("v0"));
    om.revertField(vm.get("this"), "result");
    vm.put("v6", om.getField(vm.get("this"), "result"));
    if (!em.eq(vm.get("v2"), vm.get("v5"))) e.abort(String.format("!((v2=%s) eq (v5=%s))", vm.get("v2"), vm.get("v5")));
    if (!em.ne(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) ne (v5=%s))", vm.get("param0"), vm.get("v5")));
    if (!em.eq(vm.get("v6"), vm.get("v5"))) e.abort(String.format("!((v6=%s) eq (v5=%s))", vm.get("v6"), vm.get("v5")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="setValue", desc="(Ljava/lang/Object;)V",revertedInput={"this.result"},definedOutput={"this.result"})
  static void setValue58988(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "lock"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "listeners"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "result"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("org.apache.sshd.common.future.DefaultSshFuture.NULL"));
    vm.put("v7", null);
    if (!em.eq(vm.get("v5"), (vm.get("v6")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v6")));
    vm.put("param0", om.newDefaultValue("java.lang.Object"));
    om.revertField(vm.get("this"), "result");
    vm.put("v8", om.getField(vm.get("this"), "result"));
    if (!em.eq(vm.get("v3"), vm.get("v7"))) e.abort(String.format("!((v3=%s) eq (v7=%s))", vm.get("v3"), vm.get("v7")));
    if (!em.eq(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) eq (v7=%s))", vm.get("param0"), vm.get("v7")));
    if (!em.eq(vm.get("v8"), vm.get("v7"))) e.abort(String.format("!((v8=%s) eq (v7=%s))", vm.get("v8"), vm.get("v7")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="removeListener", desc="(Lorg/apache/sshd/common/future/SshFutureListener;)Lorg/apache/sshd/common/future/SshFuture;",revertedInput={"this.listeners"},definedOutput={"this.listeners"})
  static void removeListener70387(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "listeners"));
    vm.put("v1", null);
    vm.put("v2", om.getField(vm.get("this"), "result"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "lock"));
    vm.put("v5", vm.get("v4"));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    om.revertField(vm.get("this"), "listeners");
    vm.put("v6", om.getField(vm.get("this"), "listeners"));
    vm.put("param0", om.newDefaultValue("org.apache.sshd.common.future.SshFutureListener"));
    if (!em.ne(vm.get("param0"), vm.get("v1"))) e.abort(String.format("!((param0=%s) ne (v1=%s))", vm.get("param0"), vm.get("v1")));
    if (!em.ne(vm.get("v6"), vm.get("v1"))) e.abort(String.format("!((v6=%s) ne (v1=%s))", vm.get("v6"), vm.get("v1")));
    if (!em.eq(vm.get("v6"), vm.get("param0"))) e.abort(String.format("!((v6=%s) eq (param0=%s))", vm.get("v6"), vm.get("param0")));
    if (!em.eq(vm.get("v3"), vm.get("v1"))) e.abort(String.format("!((v3=%s) eq (v1=%s))", vm.get("v3"), vm.get("v1")));
  }
  @IM(clazz="org.apache.sshd.client.future.DefaultAuthFuture", name="setAuthed", desc="(Z)V",revertedInput={"this.result"},definedOutput={"this.result"})
  static void setAuthed59985(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "result"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "listeners"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "lock"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getStatic("java.lang.Boolean.FALSE"));
    vm.put("v7", null);
    vm.put("v8", (int)0);
    if (!em.eq(vm.get("v1"), (vm.get("v6")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v6")));
    om.revertField(vm.get("this"), "result");
    vm.put("v9", om.getField(vm.get("this"), "result"));
    vm.put("param0", om.newDefaultValue("boolean"));
    if (!em.eq(vm.get("param0"), vm.get("v8"))) e.abort(String.format("!((param0=%s) eq (v8=%s))", vm.get("param0"), vm.get("v8")));
    if (!em.eq(vm.get("v3"), vm.get("v7"))) e.abort(String.format("!((v3=%s) eq (v7=%s))", vm.get("v3"), vm.get("v7")));
    if (!em.eq(vm.get("v9"), vm.get("v7"))) e.abort(String.format("!((v9=%s) eq (v7=%s))", vm.get("v9"), vm.get("v7")));
    if (!em.ne(vm.get("v1"), vm.get("v7"))) e.abort(String.format("!((v1=%s) ne (v7=%s))", vm.get("v1"), vm.get("v7")));
  }
  @IM(clazz="org.apache.sshd.client.future.DefaultAuthFuture", name="setAuthed", desc="(Z)V",revertedInput={"this.result"},definedOutput={"this.result"})
  static void setAuthed59997(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "result"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("java.lang.Boolean.FALSE"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("org.apache.sshd.common.future.DefaultSshFuture.NULL"));
    vm.put("v5", om.getField(vm.get("this"), "lock"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "listeners"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", null);
    vm.put("v10", (int)0);
    if (!em.eq(vm.get("v1"), (vm.get("v4")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v4")));
    vm.put("param0", om.newDefaultValue("boolean"));
    om.revertField(vm.get("this"), "result");
    vm.put("v11", om.getField(vm.get("this"), "result"));
    if (!em.eq(vm.get("v8"), vm.get("v9"))) e.abort(String.format("!((v8=%s) eq (v9=%s))", vm.get("v8"), vm.get("v9")));
    if (!em.eq(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) eq (v10=%s))", vm.get("param0"), vm.get("v10")));
    if (!em.eq(vm.get("v3"), vm.get("v9"))) e.abort(String.format("!((v3=%s) eq (v9=%s))", vm.get("v3"), vm.get("v9")));
    if (!em.eq(vm.get("v11"), vm.get("v9"))) e.abort(String.format("!((v11=%s) eq (v9=%s))", vm.get("v11"), vm.get("v9")));
  }
  @IM(clazz="org.apache.sshd.client.future.DefaultAuthFuture", name="setAuthed", desc="(Z)V",revertedInput={"this.result"},definedOutput={"this.result"})
  static void setAuthed59996(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "result"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("org.apache.sshd.common.future.DefaultSshFuture.NULL"));
    vm.put("v3", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("this"), "lock"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("this"), "listeners"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", null);
    vm.put("v10", (int)0);
    if (!em.eq(vm.get("v1"), (vm.get("v2")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v2")));
    om.revertField(vm.get("this"), "result");
    vm.put("v11", om.getField(vm.get("this"), "result"));
    vm.put("param0", om.newDefaultValue("boolean"));
    if (!em.eq(vm.get("v8"), vm.get("v9"))) e.abort(String.format("!((v8=%s) eq (v9=%s))", vm.get("v8"), vm.get("v9")));
    if (!em.eq(vm.get("v11"), vm.get("v9"))) e.abort(String.format("!((v11=%s) eq (v9=%s))", vm.get("v11"), vm.get("v9")));
    if (!em.eq(vm.get("v4"), vm.get("v9"))) e.abort(String.format("!((v4=%s) eq (v9=%s))", vm.get("v4"), vm.get("v9")));
    if (!em.ne(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ne (v10=%s))", vm.get("param0"), vm.get("v10")));
  }
  @IM(clazz="org.apache.sshd.client.future.DefaultAuthFuture", name="setAuthed", desc="(Z)V",revertedInput={"this.result"},definedOutput={"this.result"})
  static void setAuthed59994(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "lock"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("java.lang.Boolean.TRUE"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "listeners"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("this"), "result"));
    vm.put("v7", null);
    vm.put("v8", (int)0);
    if (!em.eq(vm.get("v3"), (vm.get("v6")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v6")));
    vm.put("param0", om.newDefaultValue("boolean"));
    om.revertField(vm.get("this"), "result");
    vm.put("v9", om.getField(vm.get("this"), "result"));
    if (!em.ne(vm.get("param0"), vm.get("v8"))) e.abort(String.format("!((param0=%s) ne (v8=%s))", vm.get("param0"), vm.get("v8")));
    if (!em.eq(vm.get("v9"), vm.get("v7"))) e.abort(String.format("!((v9=%s) eq (v7=%s))", vm.get("v9"), vm.get("v7")));
    if (!em.ne(vm.get("v3"), vm.get("v7"))) e.abort(String.format("!((v3=%s) ne (v7=%s))", vm.get("v3"), vm.get("v7")));
    if (!em.eq(vm.get("v5"), vm.get("v7"))) e.abort(String.format("!((v5=%s) eq (v7=%s))", vm.get("v5"), vm.get("v7")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="cancel", desc="()V",revertedInput={"this.result"},definedOutput={"this.result"})
  static void cancel50391(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "result"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "listeners"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("org.apache.sshd.common.future.DefaultSshFuture.NULL"));
    vm.put("v5", om.getField(vm.get("this"), "lock"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getStatic("org.apache.sshd.common.future.DefaultSshFuture.CANCELED"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", null);
    if (!em.eq(vm.get("v1"), (vm.get("v4")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v4")));
    om.revertField(vm.get("this"), "result");
    vm.put("v10", om.getField(vm.get("this"), "result"));
    if (!em.eq(vm.get("v3"), vm.get("v9"))) e.abort(String.format("!((v3=%s) eq (v9=%s))", vm.get("v3"), vm.get("v9")));
    if (!em.eq(vm.get("v8"), vm.get("v9"))) e.abort(String.format("!((v8=%s) eq (v9=%s))", vm.get("v8"), vm.get("v9")));
    if (!em.eq(vm.get("v10"), vm.get("v9"))) e.abort(String.format("!((v10=%s) eq (v9=%s))", vm.get("v10"), vm.get("v9")));
  }
  @IM(clazz="org.apache.sshd.common.future.DefaultSshFuture", name="cancel", desc="()V",revertedInput={"this.result"},definedOutput={"this.result"})
  static void cancel50394(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "lock"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "result"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("org.apache.sshd.common.future.DefaultSshFuture.CANCELED"));
    vm.put("v5", om.getField(vm.get("this"), "listeners"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", null);
    if (!em.eq(vm.get("v3"), (vm.get("v4")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v4")));
    om.revertField(vm.get("this"), "result");
    vm.put("v8", om.getField(vm.get("this"), "result"));
    if (!em.eq(vm.get("v6"), vm.get("v7"))) e.abort(String.format("!((v6=%s) eq (v7=%s))", vm.get("v6"), vm.get("v7")));
    if (!em.eq(vm.get("v8"), vm.get("v7"))) e.abort(String.format("!((v8=%s) eq (v7=%s))", vm.get("v8"), vm.get("v7")));
    if (!em.ne(vm.get("v3"), vm.get("v7"))) e.abort(String.format("!((v3=%s) ne (v7=%s))", vm.get("v3"), vm.get("v7")));
  }
  @IM(clazz="org.apache.sshd.client.future.DefaultAuthFuture", name="setException", desc="(Ljava/lang/Throwable;)V",revertedInput={"this.result"},definedOutput={"this.result"})
  static void setException62792(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "listeners"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("this"), "lock"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "result"));
    vm.put("v5", null);
    vm.put("param0", vm.get("v4"));
    om.revertField(vm.get("this"), "result");
    vm.put("v6", om.getField(vm.get("this"), "result"));
    if (!em.eq(vm.get("v6"), vm.get("v5"))) e.abort(String.format("!((v6=%s) eq (v5=%s))", vm.get("v6"), vm.get("v5")));
    if (!em.ne(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) ne (v5=%s))", vm.get("param0"), vm.get("v5")));
    if (!em.eq(vm.get("v1"), vm.get("v5"))) e.abort(String.format("!((v1=%s) eq (v5=%s))", vm.get("v1"), vm.get("v5")));
    if (!em.ne(vm.get("param0"), vm.get("v5"))) e.abort(String.format("!((param0=%s) ne (v5=%s))", vm.get("param0"), vm.get("v5")));
  }
  @IM(clazz="org.apache.sshd.client.future.DefaultAuthFuture", name="setException", desc="(Ljava/lang/Throwable;)V",revertedInput={"this.result"},definedOutput={"this.result"})
  static void setException62790(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "lock"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getStatic("org.apache.sshd.common.future.DefaultSshFuture.NULL"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("this"), "result"));
    vm.put("v5", om.getField(vm.get("this"), "listeners"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", null);
    if (!em.eq(vm.get("v3"), (vm.get("v4")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v4")));
    vm.put("param0", om.newDefaultValue("java.lang.Throwable"));
    om.revertField(vm.get("this"), "result");
    vm.put("v8", om.getField(vm.get("this"), "result"));
    if (!em.eq(vm.get("v6"), vm.get("v7"))) e.abort(String.format("!((v6=%s) eq (v7=%s))", vm.get("v6"), vm.get("v7")));
    if (!em.ne(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) ne (v7=%s))", vm.get("param0"), vm.get("v7")));
    if (!em.eq(vm.get("param0"), vm.get("v7"))) e.abort(String.format("!((param0=%s) eq (v7=%s))", vm.get("param0"), vm.get("v7")));
    if (!em.eq(vm.get("v8"), vm.get("v7"))) e.abort(String.format("!((v8=%s) eq (v7=%s))", vm.get("v8"), vm.get("v7")));
  }
}
