import org.javelus.aotes.executor.*;
@Defined({"this.cache","this.authenticator"})
public class AOTES_org_apache_sshd_server_auth_CachingPublicKeyAuthenticator {
  @IM(clazz="org.apache.sshd.server.auth.CachingPublicKeyAuthenticator", name="<init>", desc="(Lorg/apache/sshd/server/PublickeyAuthenticator;)V",definedOutput={"this.cache","this.authenticator"},delta={"this.cache","this.authenticator"})
  static void _init_67199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "authenticator"));
    vm.put("v1", om.popField(vm.get("this"), "cache"));
    vm.put("v2", vm.get("v1"));
    vm.put("param0", vm.get("v0"));
  }
}
