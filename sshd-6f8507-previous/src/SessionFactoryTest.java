import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;

import org.apache.sshd.agent.SshAgentFactory;
import org.apache.sshd.common.Factory;
import org.apache.sshd.common.NamedFactory;
import org.apache.sshd.common.ServiceFactory;
import org.apache.sshd.common.channel.Channel;
import org.apache.sshd.common.channel.RequestHandler;
import org.apache.sshd.common.cipher.Cipher;
import org.apache.sshd.common.compression.Compression;
import org.apache.sshd.common.file.FileSystemFactory;
import org.apache.sshd.common.forward.TcpipForwarderFactory;
import org.apache.sshd.common.io.IoServiceFactory;
import org.apache.sshd.common.kex.KeyExchange;
import org.apache.sshd.common.keyprovider.KeyPairProvider;
import org.apache.sshd.common.mac.Mac;
import org.apache.sshd.common.random.Random;
import org.apache.sshd.common.session.ConnectionService;
import org.apache.sshd.common.signature.Signature;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.CommandFactory;
import org.apache.sshd.server.PasswordAuthenticator;
import org.apache.sshd.server.PublickeyAuthenticator;
import org.apache.sshd.server.ServerFactoryManager;
import org.apache.sshd.server.UserAuth;
import org.apache.sshd.server.auth.gss.GSSAuthenticator;
import org.apache.sshd.server.forward.ForwardingFilter;
import org.apache.sshd.server.session.SessionFactory;
import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;

public class SessionFactoryTest {
    
    public static class DummyServerFactoryManager implements ServerFactoryManager {

        @Override
        public SshAgentFactory getAgentFactory() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<NamedFactory<Channel>> getChannelFactories() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<NamedFactory<Cipher>> getCipherFactories() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<NamedFactory<Compression>> getCompressionFactories() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public FileSystemFactory getFileSystemFactory() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<RequestHandler<ConnectionService>> getGlobalRequestHandlers() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IoServiceFactory getIoServiceFactory() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<NamedFactory<KeyExchange>> getKeyExchangeFactories() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public KeyPairProvider getKeyPairProvider() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<NamedFactory<Mac>> getMacFactories() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Map<String, Object> getProperties() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Factory<Random> getRandomFactory() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public ScheduledExecutorService getScheduledExecutorService() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<ServiceFactory> getServiceFactories() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<NamedFactory<Signature>> getSignatureFactories() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public TcpipForwarderFactory getTcpipForwarderFactory() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public ForwardingFilter getTcpipForwardingFilter() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getVersion() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public CommandFactory getCommandFactory() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public GSSAuthenticator getGSSAuthenticator() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public PasswordAuthenticator getPasswordAuthenticator() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public PublickeyAuthenticator getPublickeyAuthenticator() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Factory<Command> getShellFactory() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<NamedFactory<Command>> getSubsystemFactories() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<NamedFactory<UserAuth>> getUserAuthFactories() {
            // TODO Auto-generated method stub
            return null;
        }
        
        private String name;
        
        public DummyServerFactoryManager(String name) {
            this.name = name;
        }
        
        public String toString() {
            return name;
        } 
    }
    
    public static class Test1 extends AOTESTestCase<SessionFactory> {

        @Override
        protected SessionFactory buildInternal() {
            SessionFactory factory = new SessionFactory();
            return factory;
        }

        @Override
        protected void checkInternal(SessionFactory updated) throws Exception {
            assertFieldEquals(updated, "manager", null);
        }
        
    }
    
    public static class Test2 extends AOTESTestCase<SessionFactory> {

        ServerFactoryManager server;
        
        @Override
        protected SessionFactory buildInternal() {
            SessionFactory factory = new SessionFactory();
            server = new DummyServerFactoryManager("dummy server factory manager");
            factory.setServer(server);
            return factory;
        }

        @Override
        protected void checkInternal(SessionFactory updated) throws Exception {
            assertFieldEquals(updated, "manager", server);
        }
    }
    
    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        AOTESTestRunner.run(
                Test1.class,
                Test2.class
                );
    }
}
