import org.javelus.aotes.executor.*;
@Defined({"this.shell"})
public class AOTES_org_apache_sshd_server_shell_InvertedShellWrapper {
  @IM(clazz="org.apache.sshd.server.shell.InvertedShellWrapper", name="<init>", desc="(Lorg/apache/sshd/server/shell/InvertedShell;)V",definedOutput={"this.shell"},delta={"this.shell"})
  static void _init_99(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "shell"));
    vm.put("param0", vm.get("v0"));
  }
}
