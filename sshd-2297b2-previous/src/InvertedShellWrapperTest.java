import org.apache.sshd.server.shell.ProcessShellFactory;
import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;
import org.apache.sshd.server.shell.InvertedShellWrapper;

public class InvertedShellWrapperTest {
    
    public static void test() {
        InvertedShellWrapper t = (InvertedShellWrapper) new ProcessShellFactory().create();
        System.out.println(org.javelus.aotes.executor.java.JavaExecutorDriver.transform(t));
    }
    
    public static class Test1 extends AOTESTestCase<InvertedShellWrapper> {

        @Override
        protected InvertedShellWrapper buildInternal() {
            InvertedShellWrapper t = (InvertedShellWrapper) new ProcessShellFactory().create();
            return t;
        }

        @Override
        protected void checkInternal(InvertedShellWrapper updated) throws Exception {
            assertFieldEquals(updated, "bufferSize", 8192);
        }

    }
    
    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        AOTESTestRunner.run(
                Test1.class
                );
     }
}
