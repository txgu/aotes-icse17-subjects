#! /usr/bin/python

import os, sys, subprocess, traceback, StringIO
import shutil

try:
    import ConfigParser as CP
except:
    import configparser as CP

CWD=os.path.dirname(os.path.abspath(__file__))
MODE=os.getenv('AOTES_TEST_MODE', 'javelus')
JAVELUS_TARGET_DIR=os.path.abspath(os.getenv('JAVELUS_TARGET_DIR', 'javelus/build/vs-amd64/compiler2/fastdebug'))
AOTES_DIST = os.path.abspath(os.getenv('AOTES_DIST','aotes-asm/dist'))
DPG_DIST = os.path.abspath(os.getenv('DPG_DIST','dpg/dist'))

if os.name == 'nt':
    EXE = 'run.bat'
else:
    EXE = 'run.sh'

AOTES_BIN=os.path.join(AOTES_DIST, EXE)
DPG_BIN=os.path.join(DPG_DIST, EXE)


JAVA='java'
JAVA_OPTS='-XX:+UseSerialGC -XX:-TieredCompilation -XX:-UseCompressedOops -XX:-UseCompressedClassPointers -XX:-UseBiasedLocking'.split()
JAVELUS_OPTS=('-Dsun.java.launcher=gamma -XXaltjvm=' + JAVELUS_TARGET_DIR).split()



def read_config(java_properties):
    ini_str = '[root]\n'
    with open(java_properties, 'r') as f:
        ini_str = ini_str + f.read()

    ini_fp = StringIO.StringIO(ini_str)
    config = CP.RawConfigParser()
    config.readfp(ini_fp)
    return { key: config.get('root', key) for key in config.options('root') }


def collect_jar_in(root):
    if not root:
        return ''
    jars = []
    for dirpath, dirnames, filenames in os.walk(root):
        for filename in filenames:
            if filename.endswith('.jar'):
                jars.append(os.path.join(dirpath, filename))

    return os.pathsep.join(jars)


def generate_class_path(*args):
    return os.pathsep.join(args)

def run_with_java_properties(java_properties):
    config = read_config(java_properties)
    config['base'] = os.path.abspath(os.path.dirname(java_properties))
    return run_with_config(config)


def run_with_config(config):
    base = config['base']
    proj_name = config['name']
    old_version = config['old.version']
    new_version = config['new.version']
    lib_dir = config.get('libdir')
    return run(base, proj_name, old_version, new_version, lib_dir)

def collect_tests_in(folder):
    tests = []
    for f in os.listdir(folder):
        if f.endswith('Test.class'):
            tests.append(f[:-len('.class')])

    return tests

def run(base, proj_name, old_version, new_version, lib_dir):
    old_dir = os.path.join(base, '%s-%s' % (proj_name, old_version))
    new_dir = os.path.join(base, '%s-%s' % (proj_name, new_version))

    lib_dir = os.path.join(base, proj_name + '-' + lib_dir) if lib_dir else None

    old_class_path = collect_jar_in(old_dir)
    new_class_path = collect_jar_in(new_dir)
    lib_class_path = collect_jar_in(lib_dir)
    aotes_class_path = collect_jar_in(AOTES_DIST)

    dynamic_patch_path = os.path.join(old_dir, 'javelus.dsu')

    test_dir = os.path.join(old_dir, 'bin') # Use eclipse to build all tests...
    tests = collect_tests_in(test_dir)

    print(MODE)
    if MODE == 'javelus' or MODE == 'default':
        VM_OPTS = JAVA_OPTS + JAVELUS_OPTS
        classpath = generate_class_path(test_dir, old_class_path, lib_class_path, aotes_class_path)
        cmd_base = [JAVA] + VM_OPTS + ['-Daotes.mode=' + MODE, '-Dorg.javelus.dynamicPatch=' + dynamic_patch_path, '-cp', classpath]
        for test in tests:
            print('Run test: ' + test + ' in ' + proj_name + '-' + new_version)
            cmd = cmd_base + [test]
            logfilename = os.path.join(old_dir, test + '-' + MODE +'.log')
            with open(logfilename, 'w') as logfile:
                run_cmd(cmd, logfile)
                logfile.write('cmds: ' + ' '.join(cmd))

            with open(logfilename, 'r') as logfile:
                print(logfile.read())

    elif MODE == 'tos':
        VM_OPTS = JAVA_OPTS
        old_classpath = generate_class_path(test_dir, old_class_path, lib_class_path, aotes_class_path)
        new_classpath = generate_class_path(os.path.join(new_dir, 'bin'), new_class_path, lib_class_path, aotes_class_path) # test may not be compatible with new version
        for test in tests:
            # example: sshd-0.6.0-0.7.0_new_org.apache.sshd.server.sftp.SftpSubsystem$DirectoryHandle_fileList
            class_name, field_name = get_tos_params(old_dir)
            old_heap_dump_filename = '%s-%s_old_%s_%s.hprof' % (proj_name, new_version, class_name, field_name)
            cmd = [JAVA] + VM_OPTS + ['-Daotes.mode=' + MODE, '-Daotes.tos.heap=' + old_heap_dump_filename, '-cp', old_classpath, test]
            logfilename = os.path.join(old_dir, test + '-' + MODE +'-old.log')
            print('Run test: ' + test + ' in ' + proj_name + '-' + old_version)
            with open(logfilename, 'w') as logfile:
                run_cmd(cmd, logfile)
                logfile.write('cmds: ' + ' '.join(cmd))

            with open(logfilename, 'r') as logfile:
                print(logfile.read())

            new_heap_dump_filename = '%s-%s_new_%s_%s.hprof' % (proj_name, new_version, class_name, field_name)
            cmd = [JAVA] + VM_OPTS + ['-Daotes.mode=' + MODE, '-Daotes.tos.heap=' + new_heap_dump_filename, '-cp', new_classpath, test]
            logfilename = os.path.join(old_dir, test + '-' + MODE +'-new.log')
            print('Run test: ' + test + ' in ' + proj_name + '-' + new_version)
            with open(logfilename, 'w') as logfile:
                run_cmd(cmd, logfile)
                logfile.write('cmds: ' + ' '.join(cmd))

            with open(logfilename, 'r') as logfile:
                print(logfile.read())

    else:
        raise RuntimeError('Unknown MODE ' + MODE)


def get_tos_params(old_dir):
    with open(os.path.join(old_dir, 'tos.params'), 'r') as f:
        return f.read().split()


def run_cmd(cmd, logfile=None):
    if logfile:
        subprocess.check_call(cmd, stdout=logfile, stderr=logfile)
    else:
        subprocess.check_call(cmd)



def run_all_in_current_folder(base):
    for f in os.listdir(base):
        if not os.path.isdir(f):
            continue

        if not f.endswith('-previous'):
            continue

        ids = f.split('-')
        if len(ids) != 3:
            continue

        new_version_dir = '-'.join(ids[0:2])
        if not os.path.exists(os.path.join(base, new_version_dir)):
            continue

        name = ids[0]
        old_version = '-'.join(ids[1:])
        new_version = ids[1]
        lib_dir = 'lib'
        config = 'name=%s\nold.version=%s\nnew.version=%s\nlibdir=%s' % (name, old_version, new_version, lib_dir)
        print('========================')
        print(config)
        try:
            run(base, name, old_version, new_version, lib_dir)
        except Exception as e:
            traceback.print_exc(e)
        print('========================\n')


if __name__ == '__main__':
    try:
        if len(sys.argv) == 2:
            ret = run_with_java_properties(sys.argv[1])
        else:
            ret = run_all_in_current_folder(os.path.dirname(os.path.abspath(__file__)))
        exit(ret)
    except Exception as e:
        traceback.print_exc(e)
        exit(1)
