import org.apache.ftpserver.DefaultDataConnectionConfiguration;
import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;

public class DefaultDataConnectionConfigurationTest {
    public static class Test1 extends AOTESTestCase<DefaultDataConnectionConfiguration> {

        @Override
        protected DefaultDataConnectionConfiguration buildInternal() {
            DefaultDataConnectionConfiguration d = new DefaultDataConnectionConfiguration();
            return d;
        }

        @Override
        protected void checkInternal(DefaultDataConnectionConfiguration updated) throws Exception {
            assertFieldEquals(updated, "idleTime", 300);
        }
    }
    
    public static class Test2 extends AOTESTestCase<DefaultDataConnectionConfiguration> {

        @Override
        protected DefaultDataConnectionConfiguration buildInternal() {
            DefaultDataConnectionConfiguration d = new DefaultDataConnectionConfiguration();
            d.setIdleTime(300);
            return d;
        }

        @Override
        protected void checkInternal(DefaultDataConnectionConfiguration updated) throws Exception {
            assertFieldEquals(updated, "idleTime", 300);
        }
    }
    
    public static class Test3 extends AOTESTestCase<DefaultDataConnectionConfiguration> {

        @Override
        protected DefaultDataConnectionConfiguration buildInternal() {
            DefaultDataConnectionConfiguration d = new DefaultDataConnectionConfiguration();
            d.setIdleTime(10);
            return d;
        }

        @Override
        protected void checkInternal(DefaultDataConnectionConfiguration updated) throws Exception {
            assertFieldEquals(updated, "idleTime", 10);
        }
    }
    
    public static class Test4 extends AOTESTestCase<DefaultDataConnectionConfiguration> {

        @Override
        protected DefaultDataConnectionConfiguration buildInternal() {
            DefaultDataConnectionConfiguration d = new DefaultDataConnectionConfiguration();
            d.setIdleTime(100);
            return d;
        }

        @Override
        protected void checkInternal(DefaultDataConnectionConfiguration updated) throws Exception {
            assertFieldEquals(updated, "idleTime", 100);
        }
    }
    
    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        AOTESTestRunner.run(
                Test1.class,
                Test2.class,
                Test3.class,
                Test4.class
                );
    }
}
