package org.apache.sshd.server.channel;



import java.lang.reflect.Field;
import java.util.Map;

import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;
import org.javelus.aotes.executor.java.JavaExecutorDriver;

@SuppressWarnings("rawtypes")
public class ChannelSessionTest {

    static {
        org.apache.sshd.server.ShellFactory.Shell.class.getClass();
    }
    
    public static class Test1 extends AOTESTestCase<ChannelSession> {

        @Override
        protected ChannelSession buildInternal() {
            ChannelSession session = new ChannelSession();
            session.addEnvVariable("name1", "value1");
            return session;
        }

        @Override
        protected void checkInternal(ChannelSession updated) throws Exception {
            Class<?> clazz = updated.getClass();
            try {
                Field field = clazz.getDeclaredField("env");
                field.setAccessible(true);
                Object env = field.get(updated);
                JavaExecutorDriver.assertTrue(env != null);
                Field field2 = env.getClass().getDeclaredField("env");
                field2.setAccessible(true);
                Object env2 = field2.get(env);
                JavaExecutorDriver.assertTrue(env2 != null);
                Map map = (Map) env2;
                JavaExecutorDriver.assertTrue(map.get("name1") != null);
                JavaExecutorDriver.assertTrue(map.get("name1").equals("value1"));
            } catch (Exception e) {
                throw new RuntimeException("Assertion failed!");
            }
        }

    }


    public static class Test2 extends AOTESTestCase<ChannelSession> {

        @Override
        protected ChannelSession buildInternal() {
            ChannelSession session = new ChannelSession();
            return session;
        }

        @Override
        protected void checkInternal(ChannelSession updated) throws Exception {
            assertFieldEquals(updated, "env", null);
        }
    }

    public static class Test3 extends AOTESTestCase<ChannelSession> {

        @Override
        protected ChannelSession buildInternal() {
            ChannelSession session = new ChannelSession();
            session.addEnvVariable("name1", "value1");
            session.addEnvVariable("name2", "value2");
            session.addEnvVariable("name3", "value3");
            return session;
        }

        @Override
        protected void checkInternal(ChannelSession updated) throws Exception {
            Class<?> clazz = updated.getClass();
            try {
                Field field = clazz.getDeclaredField("env");
                field.setAccessible(true);
                Object env = field.get(updated);
                JavaExecutorDriver.assertTrue(env != null);
                Field field2 = env.getClass().getDeclaredField("env");
                field2.setAccessible(true);
                Object env2 = field2.get(env);
                JavaExecutorDriver.assertTrue(env2 != null);
                Map map = (Map) env2;
                JavaExecutorDriver.assertTrue(map.get("name1") != null);
                JavaExecutorDriver.assertTrue(map.get("name1").equals("value1"));
                JavaExecutorDriver.assertTrue(map.get("name2") != null);
                JavaExecutorDriver.assertTrue(map.get("name2").equals("value2"));
                JavaExecutorDriver.assertTrue(map.get("name3") != null);
                JavaExecutorDriver.assertTrue(map.get("name3").equals("value3"));
            } catch (Exception e) {
                throw new RuntimeException("Assertion failed!");
            }
        }

    }

    public static void test() {
        org.apache.sshd.server.channel.ChannelSession t = new org.apache.sshd.server.channel.ChannelSession();
        System.out.println(org.javelus.aotes.executor.java.JavaExecutorDriver.transform(t));

    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        AOTESTestRunner.run(
                Test1.class,
                Test2.class,
                Test3.class
                );
    }
}
