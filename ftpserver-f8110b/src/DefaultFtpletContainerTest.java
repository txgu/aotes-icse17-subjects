import java.io.IOException;

import org.apache.ftpserver.ftplet.DefaultFtpletContainer;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.FtpRequest;
import org.apache.ftpserver.ftplet.FtpSession;
import org.apache.ftpserver.ftplet.Ftplet;
import org.apache.ftpserver.ftplet.FtpletContext;
import org.apache.ftpserver.ftplet.FtpletEnum;
import org.javelus.aotes.asm.Options;
import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;



public class DefaultFtpletContainerTest {

    static class DummyFtplet implements Ftplet {

        String name;

        public DummyFtplet(String name) {
            this.name = name;
        }

        public String toString() {
            return name;
        }

        @Override
        public void destroy() {

        }

        @Override
        public void init(FtpletContext arg0) throws FtpException {

        }

        @Override
        public FtpletEnum onAppendEnd(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onAppendStart(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onConnect(FtpSession arg0) throws FtpException,
        IOException {
            return null;
        }

        @Override
        public FtpletEnum onDeleteEnd(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onDeleteStart(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onDisconnect(FtpSession arg0) throws FtpException,
        IOException {
            return null;
        }

        @Override
        public FtpletEnum onDownloadEnd(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onDownloadStart(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onLogin(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onMkdirEnd(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onMkdirStart(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onRenameEnd(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onRenameStart(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onRmdirEnd(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onRmdirStart(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onSite(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onUploadEnd(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onUploadStart(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onUploadUniqueEnd(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }

        @Override
        public FtpletEnum onUploadUniqueStart(FtpSession arg0, FtpRequest arg1)
                throws FtpException, IOException {
            return null;
        }
    }

    public static class Test1 extends AOTESTestCase<DefaultFtpletContainer> {

        DummyFtplet df1;

        @Override
        protected DefaultFtpletContainer buildInternal() {
            DefaultFtpletContainer dfc = new DefaultFtpletContainer();
            df1 = new DummyFtplet("Ftplet1");
            dfc.addFtplet(df1.name, df1);
            return dfc;
        }

        @Override
        protected void checkInternal(DefaultFtpletContainer updated) throws Exception {
            assertEquals(updated.getFtplet(df1.name), df1);
        }
        
    }

    public static class Test2 extends AOTESTestCase<DefaultFtpletContainer> {

        DummyFtplet df1;
        DummyFtplet df2;
        @Override
        protected DefaultFtpletContainer buildInternal() {
            DefaultFtpletContainer dfc = new DefaultFtpletContainer();
            df1 = new DummyFtplet("Ftplet1");
            df2 = new DummyFtplet("Ftplet2");
            dfc.addFtplet(df1.name, df1);
            dfc.addFtplet(df2.name, df2);
            return dfc;
        }

        @Override
        protected void checkInternal(DefaultFtpletContainer updated) throws Exception {
            assertEquals(updated.getFtplet(df1.name), df1);
            assertEquals(updated.getFtplet(df2.name), df2);
        }
        
    }
    
    public static class Test3 extends AOTESTestCase<DefaultFtpletContainer> {

        DummyFtplet df1;
        DummyFtplet df2;
        DummyFtplet df3;
        @Override
        protected DefaultFtpletContainer buildInternal() {
            DefaultFtpletContainer dfc = new DefaultFtpletContainer();
            df1 = new DummyFtplet("Ftplet1");
            df2 = new DummyFtplet("Ftplet2");
            df3 = new DummyFtplet("Ftplet3");
            dfc.addFtplet(df1.name, df1);
            dfc.addFtplet(df2.name, df2);
            dfc.addFtplet(df3.name, df3);
            return dfc;
        }

        @Override
        protected void checkInternal(DefaultFtpletContainer updated) throws Exception {
            assertEquals(updated.getFtplet(df1.name), df1);
            assertEquals(updated.getFtplet(df2.name), df2);
            assertEquals(updated.getFtplet(df3.name), df3);
        }
        
    }

    public static class Test4 extends AOTESTestCase<DefaultFtpletContainer> {

        DummyFtplet df1;
        DummyFtplet df2;
        @Override
        protected DefaultFtpletContainer buildInternal() {
            DefaultFtpletContainer dfc = new DefaultFtpletContainer();
            df1 = new DummyFtplet("Ftplet1");
            df2 = new DummyFtplet("Ftplet2");
            dfc.addFtplet(df1.name, df1);
            dfc.addFtplet(df2.name, df2);
            dfc.removeFtplet(df1.name);
            return dfc;
        }

        @Override
        protected void checkInternal(DefaultFtpletContainer updated) throws Exception {
            assertEquals(updated.getFtplet(df1.name), null);
            assertEquals(updated.getFtplet(df2.name), df2);
        }
        
    }

    public static class Test5 extends AOTESTestCase<DefaultFtpletContainer> {

        DummyFtplet df1;
        DummyFtplet df2;
        @Override
        protected DefaultFtpletContainer buildInternal() {
            DefaultFtpletContainer dfc = new DefaultFtpletContainer();
            df1 = new DummyFtplet("Ftplet1");
            df2 = new DummyFtplet("Ftplet2");
            dfc.addFtplet(df1.name, df1);
            dfc.addFtplet(df2.name, df2);
            dfc.removeFtplet(df1.name);
            dfc.removeFtplet(df2.name);
            return dfc;
        }

        @Override
        protected void checkInternal(DefaultFtpletContainer updated) throws Exception {
            assertEquals(updated.getFtplet(df1.name), null);
            assertEquals(updated.getFtplet(df2.name), null);
        }
        
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        Options.setDebug("executor", false);
        AOTESTestRunner.run(
                Test1.class,
                Test2.class,
                Test3.class,
                Test4.class,
                Test5.class
                );
    }
}
