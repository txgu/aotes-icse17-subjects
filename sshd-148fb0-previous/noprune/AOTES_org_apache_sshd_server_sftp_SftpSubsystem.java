import org.javelus.aotes.executor.*;
@Defined({"this.err","this.in","this.closed","this.handles","this.session","org.apache.sshd.server.sftp.SftpSubsystem.MONTHS[*]","this.out","this.callback","this.root","org.apache.sshd.server.sftp.SftpSubsystem.MONTHS","this.log","this.handles.loadFactor"})
public class AOTES_org_apache_sshd_server_sftp_SftpSubsystem {
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem", name="setErrorStream", desc="(Ljava/io/OutputStream;)V",definedOutput={"this.err"},delta={"this.err"})
  static void setErrorStream1199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "err"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem", name="setInputStream", desc="(Ljava/io/InputStream;)V",definedOutput={"this.in"},delta={"this.in"})
  static void setInputStream199(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "in"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem", name="setOutputStream", desc="(Ljava/io/OutputStream;)V",definedOutput={"this.out"},delta={"this.out"})
  static void setOutputStream899(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "out"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem", name="setFileSystemView", desc="(Lorg/apache/sshd/server/FileSystemView;)V",definedOutput={"this.root"},delta={"this.root"})
  static void setFileSystemView99(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "root"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem", name="setExitCallback", desc="(Lorg/apache/sshd/server/ExitCallback;)V",definedOutput={"this.callback"},delta={"this.callback"})
  static void setExitCallback399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "callback"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem", name="setSession", desc="(Lorg/apache/sshd/server/session/ServerSession;)V",definedOutput={"this.session"},delta={"this.session"})
  static void setSession999(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "session"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem", name="<clinit>", desc="()V",definedOutput={"org.apache.sshd.server.sftp.SftpSubsystem.MONTHS[*]","org.apache.sshd.server.sftp.SftpSubsystem.MONTHS"},delta={"org.apache.sshd.server.sftp.SftpSubsystem.MONTHS[*]","org.apache.sshd.server.sftp.SftpSubsystem.MONTHS"})
  static void _clinit_1399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("v0", (int)0);
    vm.put("v1", "Jan");
    vm.put("v2", (int)5);
    vm.put("v3", "Jun");
    vm.put("v4", (int)9);
    vm.put("v5", "Oct");
    vm.put("v6", (int)7);
    vm.put("v7", "Aug");
    vm.put("v8", (int)8);
    vm.put("v9", "Sep");
    vm.put("v10", (int)4);
    vm.put("v11", "May");
    vm.put("v12", (int)11);
    vm.put("v13", "Dec");
    vm.put("v14", (int)2);
    vm.put("v15", "Mar");
    vm.put("v16", (int)12);
    vm.put("v17", (int)6);
    vm.put("v18", "Jul");
    vm.put("v19", (int)10);
    vm.put("v20", "Nov");
    vm.put("v21", (int)3);
    vm.put("v22", "Apr");
    vm.put("v23", (int)1);
    vm.put("v24", "Feb");
    vm.put("v25", om.popStatic("org.apache.sshd.server.sftp.SftpSubsystem.MONTHS"));
    vm.put("v26", vm.get("v25"));
    vm.put("v27", om.popArrayElement(vm.get("v26"), vm.get("v8")));
    vm.put("v28", om.getArrayLength(vm.get("v26")));
    vm.put("v29", om.popArrayElement(vm.get("v26"), vm.get("v17")));
    vm.put("v30", om.popArrayElement(vm.get("v26"), vm.get("v6")));
    vm.put("v31", om.popArrayElement(vm.get("v26"), vm.get("v0")));
    vm.put("v32", om.popArrayElement(vm.get("v26"), vm.get("v14")));
    vm.put("v33", om.popArrayElement(vm.get("v26"), vm.get("v21")));
    vm.put("v34", om.popArrayElement(vm.get("v26"), vm.get("v10")));
    vm.put("v35", om.popArrayElement(vm.get("v26"), vm.get("v2")));
    vm.put("v36", om.popArrayElement(vm.get("v26"), vm.get("v12")));
    vm.put("v37", om.popArrayElement(vm.get("v26"), vm.get("v4")));
    vm.put("v38", om.popArrayElement(vm.get("v26"), vm.get("v23")));
    vm.put("v39", om.popArrayElement(vm.get("v26"), vm.get("v19")));
    if (!em.eq(vm.get("v16"), (vm.get("v28")))) e.abort("Inconsistent value for \"v16\": " + vm.get("v16") + " ne " + (vm.get("v28")));
  }
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem", name="<init>", desc="()V",definedOutput={"this.closed","this.handles","this.log","this.handles.loadFactor"},delta={"this.closed","this.handles","this.log","this.handles.loadFactor"})
  static void _init_1599(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "closed"));
    vm.put("v1", (int)0);
    vm.put("v2", om.popField(vm.get("this"), "log"));
    vm.put("v3", (float)0.75F);
    vm.put("v4", om.popField(vm.get("this"), "handles"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", em.op("java.lang.Object.getClass()Ljava/lang/Class;").eval(vm.get("this")));
    vm.put("v7", em.op("org.slf4j.LoggerFactory.getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;").eval(vm.get("v6")));
    vm.put("v8", om.popField(vm.get("v5"), "loadFactor"));
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
    if (!em.eq(vm.get("v7"), (vm.get("v2")))) e.abort("Inconsistent value for \"v7\": " + vm.get("v7") + " ne " + (vm.get("v2")));
    if (!em.eq(vm.get("v3"), (vm.get("v8")))) e.abort("Inconsistent value for \"v3\": " + vm.get("v3") + " ne " + (vm.get("v8")));
  }
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem", name="destroy", desc="()V",definedOutput={"this.closed"},delta={"this.closed"})
  static void destroy499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "closed"));
    vm.put("v1", (int)1);
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
  }
}
