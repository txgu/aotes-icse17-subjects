import org.javelus.aotes.executor.*;
@Defined({"this.file","this.done"})
public class AOTES_org_apache_sshd_server_sftp_SftpSubsystem$DirectoryHandle {
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem$DirectoryHandle", name="setDone", desc="(Z)V",definedOutput={"this.done"},delta={"this.done"})
  static void setDone1299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "done"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="org.apache.sshd.server.sftp.SftpSubsystem$DirectoryHandle", name="<init>", desc="(Lorg/apache/sshd/server/SshFile;)V",definedOutput={"this.file"},delta={"this.file"})
  static void _init_2399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "file"));
    vm.put("param0", vm.get("v0"));
  }
}
