import org.apache.commons.collections.functors.EqualPredicate;
import org.apache.commons.collections.functors.NullPredicate;
import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;

public class EqualPredicateTest {

    private static final EqualsTestObject FALSE_OBJECT = new EqualsTestObject(false);
    private static final EqualsTestObject TRUE_OBJECT = new EqualsTestObject(true);

    public static class EqualsTestObject {
        private final boolean b;

        public EqualsTestObject(boolean b) {
            this.b = b;
        }

        @Override
        public boolean equals(Object obj) {
            return b;
        }
    }

    public static class Test1 extends AOTESTestCase<EqualPredicate> {
        @Override
        protected EqualPredicate buildInternal() {
            return (EqualPredicate) EqualPredicate.equalPredicate(null);
        }

        @Override
        protected void checkInternal(EqualPredicate updated) throws Exception {
            assertEquals(NullPredicate.nullPredicate(), updated);
        }
    }

    public static class Test2 extends AOTESTestCase<EqualPredicate> {
        @Override
        protected EqualPredicate buildInternal() {
            return (EqualPredicate) EqualPredicate.equalPredicate(FALSE_OBJECT);
        }

        @Override
        protected void checkInternal(EqualPredicate predicate) throws Exception {
            assertTrue(!predicate.equals(FALSE_OBJECT));
        }
    }
    
    public static class Test3 extends AOTESTestCase<EqualPredicate> {
        @Override
        protected EqualPredicate buildInternal() {
            return (EqualPredicate) EqualPredicate.equalPredicate(TRUE_OBJECT);
        }

        @Override
        protected void checkInternal(EqualPredicate predicate) throws Exception {
            assertTrue(predicate.equals(TRUE_OBJECT));
        }
    }
    
    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        System.out.println(NullPredicate.nullPredicate());
        System.out.println(EqualPredicate.equalPredicate(null));
        AOTESTestRunner.run(
                Test1.class,
                Test2.class,
                Test3.class
                );
    }
}
