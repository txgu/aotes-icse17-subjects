import org.javelus.aotes.executor.*;
@Defined({"this.value"})
public class AOTES_Counter {
  @IM(clazz="Counter", name="inc", desc="()I",revertedInput={"this.value"},definedOutput={"this.value"})
  static void inc99(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "value"));
    vm.put("v1", (int)1);
    vm.put("v2", vm.get("v0"));
    vm.put("v3", em.op("isub").eval(vm.get("v2"),vm.get("v1")));
    om.revertField(vm.get("this"), "value", vm.get("v3"));
  }
  @IM(clazz="Counter", name="setCounter", desc="(I)V",definedOutput={"this.value"},delta={"this.value"})
  static void setCounter399(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "value"));
    vm.put("param0", vm.get("v0"));
  }
  @IM(clazz="Counter", name="<init>", desc="()V",definedOutput={"this.value"},delta={"this.value"})
  static void _init_499(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "value"));
    vm.put("v1", (int)0);
    if (!em.eq(vm.get("v1"), (vm.get("v0")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v0")));
  }
}
