
public class Counter {
    private int value = 0;

    public int getCounter() {
        return value;
    }

    public synchronized void setCounter(int counter) {
        this.value = counter;
    }

    public synchronized int inc() {
        return ++value;
    }

    public String toString() {
        return String.valueOf(getCounter());
    }
}