import org.javelus.aotes.executor.*;
@Defined({"this.ftplets.modCount","this.ftplets","this.LOG","this.ftplets.elementData[*]","this.ftplets.elementData[*].name","this.ftplets.elementData[*].ftplet","this.ftplets.elementData","this.ftplets.size"})
public class AOTES_org_apache_ftpserver_ftplet_DefaultFtpletContainer {
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="dispose", desc="()V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.size"},delta={"this.ftplets.elementData[*]"})
  @DG(pred={"_init_299"})
  static void dispose4396(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", null);
    vm.put("v2", om.getField(vm.get("this"), "ftplets"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", (int)2);
    vm.put("v5", (int)0);
    vm.put("v6", om.getField(vm.get("v3"), "size"));
    vm.put("v7", (int)3);
    vm.put("v8", om.getField(vm.get("v3"), "modCount"));
    vm.put("v9", om.getField(vm.get("v3"), "elementData"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", (int)4);
    vm.put("v12", om.popArrayElement(vm.get("v10"), vm.get("v5")));
    vm.put("v13", om.popArrayElement(vm.get("v10"), vm.get("v7")));
    vm.put("v14", om.popArrayElement(vm.get("v10"), vm.get("v4")));
    vm.put("v15", om.popArrayElement(vm.get("v10"), vm.get("v0")));
    if (!em.eq(vm.get("v5"), (vm.get("v6")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v6")));
    vm.put("v16", vm.get("v8"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    om.revertField(vm.get("v3"), "modCount", vm.get("v17"));
    om.revertField(vm.get("v3"), "size");
    vm.put("v18", om.getField(vm.get("v3"), "size"));
    if (!em.lt(vm.get("v4"), vm.get("v18"))) e.abort(String.format("!((v4=%s) lt (v18=%s))", vm.get("v4"), vm.get("v18")));
    if (!em.lt(vm.get("v5"), vm.get("v18"))) e.abort(String.format("!((v5=%s) lt (v18=%s))", vm.get("v5"), vm.get("v18")));
    if (!em.ge(vm.get("v5"), vm.get("v18"))) e.abort(String.format("!((v5=%s) ge (v18=%s))", vm.get("v5"), vm.get("v18")));
    if (!em.lt(vm.get("v0"), vm.get("v18"))) e.abort(String.format("!((v0=%s) lt (v18=%s))", vm.get("v0"), vm.get("v18")));
    if (!em.lt(vm.get("v7"), vm.get("v18"))) e.abort(String.format("!((v7=%s) lt (v18=%s))", vm.get("v7"), vm.get("v18")));
    if (!em.ge(vm.get("v11"), vm.get("v18"))) e.abort(String.format("!((v11=%s) ge (v18=%s))", vm.get("v11"), vm.get("v18")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="destroy", desc="()V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.size"},delta={"this.ftplets.elementData[*]"})
  @DG(pred={"_init_299"})
  static void destroy3031(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", null);
    vm.put("v2", (int)1);
    vm.put("v3", (int)2);
    vm.put("v4", om.getField(vm.get("this"), "ftplets"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", (int)3);
    vm.put("v7", om.getField(vm.get("v5"), "size"));
    vm.put("v8", om.getField(vm.get("v5"), "elementData"));
    vm.put("v9", vm.get("v8"));
    vm.put("v10", om.popArrayElement(vm.get("v9"), vm.get("v2")));
    vm.put("v11", om.popArrayElement(vm.get("v9"), vm.get("v0")));
    vm.put("v12", om.getField(vm.get("v5"), "modCount"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.popArrayElement(vm.get("v9"), vm.get("v3")));
    if (!em.eq(vm.get("v0"), (vm.get("v7")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v7")));
    vm.put("v15", em.op("isub").eval(vm.get("v13"),vm.get("v2")));
    om.revertField(vm.get("v5"), "modCount", vm.get("v15"));
    om.revertField(vm.get("v5"), "size");
    vm.put("v16", om.getField(vm.get("v5"), "size"));
    if (!em.lt(vm.get("v3"), vm.get("v16"))) e.abort(String.format("!((v3=%s) lt (v16=%s))", vm.get("v3"), vm.get("v16")));
    if (!em.lt(vm.get("v2"), vm.get("v16"))) e.abort(String.format("!((v2=%s) lt (v16=%s))", vm.get("v2"), vm.get("v16")));
    if (!em.ge(vm.get("v6"), vm.get("v16"))) e.abort(String.format("!((v6=%s) ge (v16=%s))", vm.get("v6"), vm.get("v16")));
    if (!em.lt(vm.get("v0"), vm.get("v16"))) e.abort(String.format("!((v0=%s) lt (v16=%s))", vm.get("v0"), vm.get("v16")));
    if (!em.ge(vm.get("v0"), vm.get("v16"))) e.abort(String.format("!((v0=%s) ge (v16=%s))", vm.get("v0"), vm.get("v16")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="destroy", desc="()V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.size"},delta={"this.ftplets.elementData[*]"})
  @DG(pred={"_init_299"})
  static void destroy3028(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", null);
    vm.put("v2", (int)0);
    vm.put("v3", (int)4);
    vm.put("v4", (int)2);
    vm.put("v5", om.getField(vm.get("this"), "ftplets"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("v6"), "size"));
    vm.put("v8", (int)3);
    vm.put("v9", (int)5);
    vm.put("v10", om.getField(vm.get("v6"), "elementData"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.popArrayElement(vm.get("v11"), vm.get("v0")));
    vm.put("v13", om.popArrayElement(vm.get("v11"), vm.get("v2")));
    vm.put("v14", om.popArrayElement(vm.get("v11"), vm.get("v4")));
    vm.put("v15", om.popArrayElement(vm.get("v11"), vm.get("v8")));
    vm.put("v16", om.getField(vm.get("v6"), "modCount"));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.popArrayElement(vm.get("v11"), vm.get("v3")));
    vm.put("v19", em.op("isub").eval(vm.get("v17"),vm.get("v0")));
    om.revertField(vm.get("v6"), "modCount", vm.get("v19"));
    if (!em.eq(vm.get("v2"), (vm.get("v7")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v7")));
    om.revertField(vm.get("v6"), "size");
    vm.put("v20", om.getField(vm.get("v6"), "size"));
    if (!em.lt(vm.get("v4"), vm.get("v20"))) e.abort(String.format("!((v4=%s) lt (v20=%s))", vm.get("v4"), vm.get("v20")));
    if (!em.ge(vm.get("v9"), vm.get("v20"))) e.abort(String.format("!((v9=%s) ge (v20=%s))", vm.get("v9"), vm.get("v20")));
    if (!em.lt(vm.get("v3"), vm.get("v20"))) e.abort(String.format("!((v3=%s) lt (v20=%s))", vm.get("v3"), vm.get("v20")));
    if (!em.lt(vm.get("v2"), vm.get("v20"))) e.abort(String.format("!((v2=%s) lt (v20=%s))", vm.get("v2"), vm.get("v20")));
    if (!em.lt(vm.get("v0"), vm.get("v20"))) e.abort(String.format("!((v0=%s) lt (v20=%s))", vm.get("v0"), vm.get("v20")));
    if (!em.lt(vm.get("v8"), vm.get("v20"))) e.abort(String.format("!((v8=%s) lt (v20=%s))", vm.get("v8"), vm.get("v20")));
    if (!em.ge(vm.get("v2"), vm.get("v20"))) e.abort(String.format("!((v2=%s) ge (v20=%s))", vm.get("v2"), vm.get("v20")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="addFtplet", desc="(Ljava/lang/String;Lorg/apache/ftpserver/ftplet/Ftplet;)V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.elementData[*].name","this.ftplets.elementData[*].ftplet","this.ftplets.size"},delta={"this.ftplets.elementData[*]","this.ftplets.elementData[*].name","this.ftplets.elementData[*].ftplet"})
  @DG(pred={"_init_299"})
  static void addFtplet2698(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", om.getField(vm.get("this"), "ftplets"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("v2"), "elementData"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)0);
    vm.put("v8", null);
    vm.put("v9", om.getArrayLength(vm.get("v4")));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getField(vm.get("v2"), "size"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getField(vm.get("v2"), "modCount"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v12"),vm.get("v0")));
    om.revertField(vm.get("v2"), "size", vm.get("v15"));
    vm.put("v16", om.popArrayElement(vm.get("v4"), vm.get("v15")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.popField(vm.get("v17"), "ftplet"));
    vm.put("param1", vm.get("v18"));
    vm.put("v19", om.popField(vm.get("v17"), "name"));
    vm.put("param0", vm.get("v19"));
    vm.put("v20", em.op("iadd").eval(vm.get("v15"),vm.get("v0")));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v10")));
    vm.put("v22", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    om.revertField(vm.get("v2"), "modCount", vm.get("v22"));
    if (!em.le(vm.get("v21"), vm.get("v7"))) e.abort(String.format("!((v21=%s) le (v7=%s))", vm.get("v21"), vm.get("v7")));
    if (!em.eq(vm.get("param0"), vm.get("v8"))) e.abort(String.format("!((param0=%s) eq (v8=%s))", vm.get("param0"), vm.get("v8")));
    if (!em.ne(vm.get("v4"), vm.get("v6"))) e.abort(String.format("!((v4=%s) ne (v6=%s))", vm.get("v4"), vm.get("v6")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="addFtplet", desc="(Ljava/lang/String;Lorg/apache/ftpserver/ftplet/Ftplet;)V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.elementData[*].name","this.ftplets.elementData[*].ftplet","this.ftplets.size"},delta={"this.ftplets.elementData[*]","this.ftplets.elementData[*].name","this.ftplets.elementData[*].ftplet"})
  @DG(pred={"_init_299"})
  static void addFtplet2621(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", (int)1);
    vm.put("v2", om.getField(vm.get("this"), "ftplets"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v3"), "size"));
    vm.put("v7", om.getField(vm.get("v3"), "elementData"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", (int)10);
    vm.put("v10", null);
    vm.put("v11", om.getArrayElement(vm.get("v8"), vm.get("v0")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getField(vm.get("v12"), "name"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", om.getField(vm.get("v3"), "modCount"));
    vm.put("v16", vm.get("v15"));
    vm.put("v17", om.getArrayLength(vm.get("v8")));
    vm.put("v18", vm.get("v17"));
    vm.put("v19", em.op("isub").eval(vm.get("v9"),vm.get("v18")));
    vm.put("v20", vm.get("v6"));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v1")));
    om.revertField(vm.get("v3"), "size", vm.get("v21"));
    vm.put("v22", em.op("iadd").eval(vm.get("v21"),vm.get("v1")));
    vm.put("v23", om.popArrayElement(vm.get("v8"), vm.get("v21")));
    vm.put("v24", vm.get("v23"));
    vm.put("v25", om.popField(vm.get("v24"), "ftplet"));
    vm.put("param1", vm.get("v25"));
    vm.put("v26", om.popField(vm.get("v24"), "name"));
    vm.put("param0", vm.get("v26"));
    vm.put("v27", em.op("java.lang.String.equals(Ljava/lang/Object;)Z").eval(vm.get("v14"),vm.get("param0")));
    vm.put("v28", em.op("isub").eval(vm.get("v16"),vm.get("v1")));
    om.revertField(vm.get("v3"), "modCount", vm.get("v28"));
    if (!em.eq(vm.get("v27"), vm.get("v0"))) e.abort(String.format("!((v27=%s) eq (v0=%s))", vm.get("v27"), vm.get("v0")));
    if (!em.ge(vm.get("v9"), vm.get("v22"))) e.abort(String.format("!((v9=%s) ge (v22=%s))", vm.get("v9"), vm.get("v22")));
    if (!em.eq(vm.get("v8"), vm.get("v5"))) e.abort(String.format("!((v8=%s) eq (v5=%s))", vm.get("v8"), vm.get("v5")));
    if (!em.lt(vm.get("v0"), vm.get("v21"))) e.abort(String.format("!((v0=%s) lt (v21=%s))", vm.get("v0"), vm.get("v21")));
    if (!em.le(vm.get("v19"), vm.get("v0"))) e.abort(String.format("!((v19=%s) le (v0=%s))", vm.get("v19"), vm.get("v0")));
    if (!em.ge(vm.get("v1"), vm.get("v21"))) e.abort(String.format("!((v1=%s) ge (v21=%s))", vm.get("v1"), vm.get("v21")));
    if (!em.lt(vm.get("v0"), vm.get("v21"))) e.abort(String.format("!((v0=%s) lt (v21=%s))", vm.get("v0"), vm.get("v21")));
    if (!em.ne(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ne (v10=%s))", vm.get("param0"), vm.get("v10")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="destroy", desc="()V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.size"},delta={"this.ftplets.elementData[*]"})
  @DG(pred={"_init_299"})
  static void destroy3038(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", null);
    vm.put("v2", om.getField(vm.get("this"), "ftplets"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v3"), "modCount"));
    vm.put("v5", (int)1);
    vm.put("v6", om.getField(vm.get("v3"), "size"));
    vm.put("v7", om.getField(vm.get("v3"), "elementData"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.popArrayElement(vm.get("v8"), vm.get("v0")));
    vm.put("v10", vm.get("v4"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v5")));
    om.revertField(vm.get("v3"), "modCount", vm.get("v11"));
    if (!em.eq(vm.get("v0"), (vm.get("v6")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v6")));
    om.revertField(vm.get("v3"), "size");
    vm.put("v12", om.getField(vm.get("v3"), "size"));
    if (!em.lt(vm.get("v0"), vm.get("v12"))) e.abort(String.format("!((v0=%s) lt (v12=%s))", vm.get("v0"), vm.get("v12")));
    if (!em.ge(vm.get("v5"), vm.get("v12"))) e.abort(String.format("!((v5=%s) ge (v12=%s))", vm.get("v5"), vm.get("v12")));
    if (!em.ge(vm.get("v0"), vm.get("v12"))) e.abort(String.format("!((v0=%s) ge (v12=%s))", vm.get("v0"), vm.get("v12")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="destroy", desc="()V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.size"},delta={"this.ftplets.elementData[*]"})
  @DG(pred={"_init_299"})
  static void destroy3027(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "ftplets"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("v1"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("v1"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", (int)0);
    vm.put("v7", null);
    vm.put("v8", om.popArrayElement(vm.get("v5"), vm.get("v3")));
    vm.put("v9", om.getField(vm.get("v1"), "size"));
    vm.put("v10", (int)2);
    vm.put("v11", om.popArrayElement(vm.get("v5"), vm.get("v6")));
    if (!em.eq(vm.get("v6"), (vm.get("v9")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v9")));
    vm.put("v12", vm.get("v2"));
    vm.put("v13", em.op("isub").eval(vm.get("v12"),vm.get("v3")));
    om.revertField(vm.get("v1"), "modCount", vm.get("v13"));
    om.revertField(vm.get("v1"), "size");
    vm.put("v14", om.getField(vm.get("v1"), "size"));
    if (!em.ge(vm.get("v6"), vm.get("v14"))) e.abort(String.format("!((v6=%s) ge (v14=%s))", vm.get("v6"), vm.get("v14")));
    if (!em.lt(vm.get("v6"), vm.get("v14"))) e.abort(String.format("!((v6=%s) lt (v14=%s))", vm.get("v6"), vm.get("v14")));
    if (!em.lt(vm.get("v3"), vm.get("v14"))) e.abort(String.format("!((v3=%s) lt (v14=%s))", vm.get("v3"), vm.get("v14")));
    if (!em.ge(vm.get("v10"), vm.get("v14"))) e.abort(String.format("!((v10=%s) ge (v14=%s))", vm.get("v10"), vm.get("v14")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="dispose", desc="()V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.size"})
  @DG(pred={"_init_299"})
  static void dispose4422(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", om.getField(vm.get("this"), "ftplets"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", om.getField(vm.get("v3"), "modCount"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v3"), "size"));
    vm.put("v7", em.op("isub").eval(vm.get("v5"),vm.get("v0")));
    om.revertField(vm.get("v3"), "modCount", vm.get("v7"));
    if (!em.eq(vm.get("v1"), (vm.get("v6")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v6")));
    om.revertField(vm.get("v3"), "size");
    vm.put("v8", om.getField(vm.get("v3"), "size"));
    if (!em.ge(vm.get("v1"), vm.get("v8"))) e.abort(String.format("!((v1=%s) ge (v8=%s))", vm.get("v1"), vm.get("v8")));
    if (!em.ge(vm.get("v1"), vm.get("v8"))) e.abort(String.format("!((v1=%s) ge (v8=%s))", vm.get("v1"), vm.get("v8")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="dispose", desc="()V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.size"},delta={"this.ftplets.elementData[*]"})
  @DG(pred={"_init_299"})
  static void dispose4412(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", null);
    vm.put("v3", om.getField(vm.get("this"), "ftplets"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("v4"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)2);
    vm.put("v8", om.getField(vm.get("v4"), "size"));
    vm.put("v9", om.popArrayElement(vm.get("v6"), vm.get("v0")));
    vm.put("v10", om.getField(vm.get("v4"), "modCount"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.popArrayElement(vm.get("v6"), vm.get("v1")));
    if (!em.eq(vm.get("v1"), (vm.get("v8")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v8")));
    vm.put("v13", em.op("isub").eval(vm.get("v11"),vm.get("v0")));
    om.revertField(vm.get("v4"), "modCount", vm.get("v13"));
    om.revertField(vm.get("v4"), "size");
    vm.put("v14", om.getField(vm.get("v4"), "size"));
    if (!em.lt(vm.get("v0"), vm.get("v14"))) e.abort(String.format("!((v0=%s) lt (v14=%s))", vm.get("v0"), vm.get("v14")));
    if (!em.ge(vm.get("v7"), vm.get("v14"))) e.abort(String.format("!((v7=%s) ge (v14=%s))", vm.get("v7"), vm.get("v14")));
    if (!em.lt(vm.get("v1"), vm.get("v14"))) e.abort(String.format("!((v1=%s) lt (v14=%s))", vm.get("v1"), vm.get("v14")));
    if (!em.ge(vm.get("v1"), vm.get("v14"))) e.abort(String.format("!((v1=%s) ge (v14=%s))", vm.get("v1"), vm.get("v14")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="dispose", desc="()V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.size"},delta={"this.ftplets.elementData[*]"})
  @DG(pred={"_init_299"})
  static void dispose4388(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "ftplets"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", (int)0);
    vm.put("v3", null);
    vm.put("v4", om.getField(vm.get("v1"), "elementData"));
    vm.put("v5", vm.get("v4"));
    vm.put("v6", om.getField(vm.get("v1"), "modCount"));
    vm.put("v7", (int)1);
    vm.put("v8", (int)4);
    vm.put("v9", om.getField(vm.get("v1"), "size"));
    vm.put("v10", (int)2);
    vm.put("v11", (int)3);
    vm.put("v12", om.popArrayElement(vm.get("v5"), vm.get("v7")));
    vm.put("v13", (int)5);
    vm.put("v14", om.popArrayElement(vm.get("v5"), vm.get("v11")));
    vm.put("v15", om.popArrayElement(vm.get("v5"), vm.get("v2")));
    vm.put("v16", om.popArrayElement(vm.get("v5"), vm.get("v8")));
    vm.put("v17", om.popArrayElement(vm.get("v5"), vm.get("v10")));
    vm.put("v18", vm.get("v6"));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v7")));
    om.revertField(vm.get("v1"), "modCount", vm.get("v19"));
    if (!em.eq(vm.get("v2"), (vm.get("v9")))) e.abort("Inconsistent value for \"v2\": " + vm.get("v2") + " ne " + (vm.get("v9")));
    om.revertField(vm.get("v1"), "size");
    vm.put("v20", om.getField(vm.get("v1"), "size"));
    if (!em.lt(vm.get("v10"), vm.get("v20"))) e.abort(String.format("!((v10=%s) lt (v20=%s))", vm.get("v10"), vm.get("v20")));
    if (!em.lt(vm.get("v11"), vm.get("v20"))) e.abort(String.format("!((v11=%s) lt (v20=%s))", vm.get("v11"), vm.get("v20")));
    if (!em.lt(vm.get("v8"), vm.get("v20"))) e.abort(String.format("!((v8=%s) lt (v20=%s))", vm.get("v8"), vm.get("v20")));
    if (!em.ge(vm.get("v2"), vm.get("v20"))) e.abort(String.format("!((v2=%s) ge (v20=%s))", vm.get("v2"), vm.get("v20")));
    if (!em.lt(vm.get("v2"), vm.get("v20"))) e.abort(String.format("!((v2=%s) lt (v20=%s))", vm.get("v2"), vm.get("v20")));
    if (!em.lt(vm.get("v7"), vm.get("v20"))) e.abort(String.format("!((v7=%s) lt (v20=%s))", vm.get("v7"), vm.get("v20")));
    if (!em.ge(vm.get("v13"), vm.get("v20"))) e.abort(String.format("!((v13=%s) ge (v20=%s))", vm.get("v13"), vm.get("v20")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="destroy", desc="()V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.size"},delta={"this.ftplets.elementData[*]"})
  @DG(pred={"_init_299"})
  static void destroy2984(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", null);
    vm.put("v2", om.getField(vm.get("this"), "ftplets"));
    vm.put("v3", vm.get("v2"));
    vm.put("v4", (int)2);
    vm.put("v5", om.getField(vm.get("v3"), "modCount"));
    vm.put("v6", (int)1);
    vm.put("v7", (int)3);
    vm.put("v8", om.getField(vm.get("v3"), "size"));
    vm.put("v9", (int)4);
    vm.put("v10", om.getField(vm.get("v3"), "elementData"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.popArrayElement(vm.get("v11"), vm.get("v4")));
    vm.put("v13", om.popArrayElement(vm.get("v11"), vm.get("v7")));
    vm.put("v14", om.popArrayElement(vm.get("v11"), vm.get("v0")));
    vm.put("v15", om.popArrayElement(vm.get("v11"), vm.get("v6")));
    vm.put("v16", vm.get("v5"));
    if (!em.eq(vm.get("v0"), (vm.get("v8")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v8")));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v6")));
    om.revertField(vm.get("v3"), "modCount", vm.get("v17"));
    om.revertField(vm.get("v3"), "size");
    vm.put("v18", om.getField(vm.get("v3"), "size"));
    if (!em.lt(vm.get("v6"), vm.get("v18"))) e.abort(String.format("!((v6=%s) lt (v18=%s))", vm.get("v6"), vm.get("v18")));
    if (!em.lt(vm.get("v0"), vm.get("v18"))) e.abort(String.format("!((v0=%s) lt (v18=%s))", vm.get("v0"), vm.get("v18")));
    if (!em.lt(vm.get("v7"), vm.get("v18"))) e.abort(String.format("!((v7=%s) lt (v18=%s))", vm.get("v7"), vm.get("v18")));
    if (!em.ge(vm.get("v0"), vm.get("v18"))) e.abort(String.format("!((v0=%s) ge (v18=%s))", vm.get("v0"), vm.get("v18")));
    if (!em.lt(vm.get("v4"), vm.get("v18"))) e.abort(String.format("!((v4=%s) lt (v18=%s))", vm.get("v4"), vm.get("v18")));
    if (!em.ge(vm.get("v9"), vm.get("v18"))) e.abort(String.format("!((v9=%s) ge (v18=%s))", vm.get("v9"), vm.get("v18")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="dispose", desc="()V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.size"},delta={"this.ftplets.elementData[*]"})
  @DG(pred={"_init_299"})
  static void dispose4346(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)3);
    vm.put("v1", null);
    vm.put("v2", (int)2);
    vm.put("v3", (int)1);
    vm.put("v4", (int)0);
    vm.put("v5", (int)4);
    vm.put("v6", om.getField(vm.get("this"), "ftplets"));
    vm.put("v7", vm.get("v6"));
    vm.put("v8", (int)5);
    vm.put("v9", om.getField(vm.get("v7"), "elementData"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getField(vm.get("v7"), "size"));
    vm.put("v12", (int)6);
    vm.put("v13", om.popArrayElement(vm.get("v10"), vm.get("v3")));
    vm.put("v14", om.popArrayElement(vm.get("v10"), vm.get("v2")));
    vm.put("v15", om.popArrayElement(vm.get("v10"), vm.get("v4")));
    vm.put("v16", om.getField(vm.get("v7"), "modCount"));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.popArrayElement(vm.get("v10"), vm.get("v5")));
    vm.put("v19", om.popArrayElement(vm.get("v10"), vm.get("v0")));
    vm.put("v20", om.popArrayElement(vm.get("v10"), vm.get("v8")));
    if (!em.eq(vm.get("v4"), (vm.get("v11")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v11")));
    vm.put("v21", em.op("isub").eval(vm.get("v17"),vm.get("v3")));
    om.revertField(vm.get("v7"), "modCount", vm.get("v21"));
    om.revertField(vm.get("v7"), "size");
    vm.put("v22", om.getField(vm.get("v7"), "size"));
    if (!em.lt(vm.get("v4"), vm.get("v22"))) e.abort(String.format("!((v4=%s) lt (v22=%s))", vm.get("v4"), vm.get("v22")));
    if (!em.lt(vm.get("v5"), vm.get("v22"))) e.abort(String.format("!((v5=%s) lt (v22=%s))", vm.get("v5"), vm.get("v22")));
    if (!em.lt(vm.get("v0"), vm.get("v22"))) e.abort(String.format("!((v0=%s) lt (v22=%s))", vm.get("v0"), vm.get("v22")));
    if (!em.lt(vm.get("v8"), vm.get("v22"))) e.abort(String.format("!((v8=%s) lt (v22=%s))", vm.get("v8"), vm.get("v22")));
    if (!em.lt(vm.get("v3"), vm.get("v22"))) e.abort(String.format("!((v3=%s) lt (v22=%s))", vm.get("v3"), vm.get("v22")));
    if (!em.ge(vm.get("v12"), vm.get("v22"))) e.abort(String.format("!((v12=%s) ge (v22=%s))", vm.get("v12"), vm.get("v22")));
    if (!em.lt(vm.get("v2"), vm.get("v22"))) e.abort(String.format("!((v2=%s) lt (v22=%s))", vm.get("v2"), vm.get("v22")));
    if (!em.ge(vm.get("v4"), vm.get("v22"))) e.abort(String.format("!((v4=%s) ge (v22=%s))", vm.get("v4"), vm.get("v22")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="addFtplet", desc="(Ljava/lang/String;Lorg/apache/ftpserver/ftplet/Ftplet;)V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.elementData[*].name","this.ftplets.elementData[*].ftplet","this.ftplets.size"},delta={"this.ftplets.elementData[*]","this.ftplets.elementData[*].name","this.ftplets.elementData[*].ftplet"})
  @DG(pred={"_init_299"})
  static void addFtplet2690(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "ftplets"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("v4"), "size"));
    vm.put("v6", (int)0);
    vm.put("v7", (int)10);
    vm.put("v8", null);
    vm.put("v9", om.getField(vm.get("v4"), "modCount"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getField(vm.get("v4"), "elementData"));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getArrayLength(vm.get("v12")));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v10"),vm.get("v0")));
    om.revertField(vm.get("v4"), "modCount", vm.get("v15"));
    vm.put("v16", vm.get("v5"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    om.revertField(vm.get("v4"), "size", vm.get("v17"));
    vm.put("v18", om.popArrayElement(vm.get("v12"), vm.get("v17")));
    vm.put("v19", vm.get("v18"));
    vm.put("v20", om.popField(vm.get("v19"), "name"));
    vm.put("param0", vm.get("v20"));
    vm.put("v21", em.op("iadd").eval(vm.get("v17"),vm.get("v0")));
    vm.put("v22", em.op("isub").eval(vm.get("v21"),vm.get("v14")));
    vm.put("v23", om.popField(vm.get("v19"), "ftplet"));
    vm.put("param1", vm.get("v23"));
    if (!em.eq(vm.get("v12"), vm.get("v2"))) e.abort(String.format("!((v12=%s) eq (v2=%s))", vm.get("v12"), vm.get("v2")));
    if (!em.eq(vm.get("param0"), vm.get("v8"))) e.abort(String.format("!((param0=%s) eq (v8=%s))", vm.get("param0"), vm.get("v8")));
    if (!em.le(vm.get("v22"), vm.get("v6"))) e.abort(String.format("!((v22=%s) le (v6=%s))", vm.get("v22"), vm.get("v6")));
    if (!em.lt(vm.get("v7"), vm.get("v21"))) e.abort(String.format("!((v7=%s) lt (v21=%s))", vm.get("v7"), vm.get("v21")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="addFtplet", desc="(Ljava/lang/String;Lorg/apache/ftpserver/ftplet/Ftplet;)V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.elementData[*].name","this.ftplets.elementData[*].ftplet","this.ftplets.size"},delta={"this.ftplets.elementData[*]","this.ftplets.elementData[*].name","this.ftplets.elementData[*].ftplet"})
  @DG(pred={"_init_299"})
  static void addFtplet2640(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "ftplets"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("v4"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.getField(vm.get("v4"), "size"));
    vm.put("v8", (int)0);
    vm.put("v9", (int)10);
    vm.put("v10", null);
    vm.put("v11", om.getArrayLength(vm.get("v6")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getField(vm.get("v4"), "modCount"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v14"),vm.get("v2")));
    om.revertField(vm.get("v4"), "modCount", vm.get("v15"));
    vm.put("v16", vm.get("v7"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v2")));
    om.revertField(vm.get("v4"), "size", vm.get("v17"));
    vm.put("v18", em.op("iadd").eval(vm.get("v17"),vm.get("v2")));
    vm.put("v19", em.op("isub").eval(vm.get("v18"),vm.get("v12")));
    vm.put("v20", om.popArrayElement(vm.get("v6"), vm.get("v17")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.popField(vm.get("v21"), "ftplet"));
    vm.put("param1", vm.get("v22"));
    vm.put("v23", om.popField(vm.get("v21"), "name"));
    vm.put("param0", vm.get("v23"));
    if (!em.ge(vm.get("v8"), vm.get("v17"))) e.abort(String.format("!((v8=%s) ge (v17=%s))", vm.get("v8"), vm.get("v17")));
    if (!em.le(vm.get("v19"), vm.get("v8"))) e.abort(String.format("!((v19=%s) le (v8=%s))", vm.get("v19"), vm.get("v8")));
    if (!em.lt(vm.get("v9"), vm.get("v18"))) e.abort(String.format("!((v9=%s) lt (v18=%s))", vm.get("v9"), vm.get("v18")));
    if (!em.eq(vm.get("v6"), vm.get("v1"))) e.abort(String.format("!((v6=%s) eq (v1=%s))", vm.get("v6"), vm.get("v1")));
    if (!em.ne(vm.get("param0"), vm.get("v10"))) e.abort(String.format("!((param0=%s) ne (v10=%s))", vm.get("param0"), vm.get("v10")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="dispose", desc="()V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.size"},delta={"this.ftplets.elementData[*]"})
  @DG(pred={"_init_299"})
  static void dispose4397(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)0);
    vm.put("v1", null);
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "ftplets"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("v4"), "elementData"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", om.popArrayElement(vm.get("v6"), vm.get("v0")));
    vm.put("v8", om.getField(vm.get("v4"), "size"));
    vm.put("v9", om.getField(vm.get("v4"), "modCount"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", em.op("isub").eval(vm.get("v10"),vm.get("v2")));
    om.revertField(vm.get("v4"), "modCount", vm.get("v11"));
    if (!em.eq(vm.get("v0"), (vm.get("v8")))) e.abort("Inconsistent value for \"v0\": " + vm.get("v0") + " ne " + (vm.get("v8")));
    om.revertField(vm.get("v4"), "size");
    vm.put("v12", om.getField(vm.get("v4"), "size"));
    if (!em.ge(vm.get("v2"), vm.get("v12"))) e.abort(String.format("!((v2=%s) ge (v12=%s))", vm.get("v2"), vm.get("v12")));
    if (!em.lt(vm.get("v0"), vm.get("v12"))) e.abort(String.format("!((v0=%s) lt (v12=%s))", vm.get("v0"), vm.get("v12")));
    if (!em.ge(vm.get("v0"), vm.get("v12"))) e.abort(String.format("!((v0=%s) ge (v12=%s))", vm.get("v0"), vm.get("v12")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="<init>", desc="()V",definedOutput={"this.ftplets.modCount","this.ftplets","this.LOG","this.ftplets.elementData"},delta={"this.ftplets.modCount","this.ftplets","this.LOG","this.ftplets.elementData"})
  @DG(succ={"dispose4346","dispose4397","dispose4388","destroy3038","addFtplet2669","dispose4396","destroy2984","destroy3031","destroy3037","addFtplet2678","dispose4422","addFtplet2690","dispose2287","dispose4412","addFtplet2698","addFtplet2621","addFtplet2640","destroy3028","addFtplet2682","destroy3027"})
  static void _init_299(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.popField(vm.get("this"), "LOG"));
    vm.put("v1", org.apache.ftpserver.ftplet.DefaultFtpletContainer.class);
    vm.put("v2", om.popField(vm.get("this"), "ftplets"));
    vm.put("v3", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", (int)0);
    vm.put("v6", em.op("org.slf4j.LoggerFactory.getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;").eval(vm.get("v1")));
    if (!em.eq(vm.get("v6"), (vm.get("v0")))) e.abort("Inconsistent value for \"v6\": " + vm.get("v6") + " ne " + (vm.get("v0")));
    vm.put("v7", vm.get("v2"));
    vm.put("v8", om.popField(vm.get("v7"), "elementData"));
    vm.put("v9", om.popField(vm.get("v7"), "modCount"));
    if (!em.eq(vm.get("v4"), (vm.get("v8")))) e.abort("Inconsistent value for \"v4\": " + vm.get("v4") + " ne " + (vm.get("v8")));
    if (!em.eq(vm.get("v5"), (vm.get("v9")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v9")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="dispose", desc="()V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.size"},delta={"this.ftplets.elementData[*]"})
  @DG(pred={"_init_299"})
  static void dispose2287(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", (int)0);
    vm.put("v2", null);
    vm.put("v3", om.getField(vm.get("this"), "ftplets"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", (int)2);
    vm.put("v6", (int)3);
    vm.put("v7", om.getField(vm.get("v4"), "elementData"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.popArrayElement(vm.get("v8"), vm.get("v0")));
    vm.put("v10", om.popArrayElement(vm.get("v8"), vm.get("v1")));
    vm.put("v11", om.popArrayElement(vm.get("v8"), vm.get("v5")));
    vm.put("v12", om.getField(vm.get("v4"), "modCount"));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", om.getField(vm.get("v4"), "size"));
    vm.put("v15", em.op("isub").eval(vm.get("v13"),vm.get("v0")));
    om.revertField(vm.get("v4"), "modCount", vm.get("v15"));
    if (!em.eq(vm.get("v1"), (vm.get("v14")))) e.abort("Inconsistent value for \"v1\": " + vm.get("v1") + " ne " + (vm.get("v14")));
    om.revertField(vm.get("v4"), "size");
    vm.put("v16", om.getField(vm.get("v4"), "size"));
    if (!em.lt(vm.get("v5"), vm.get("v16"))) e.abort(String.format("!((v5=%s) lt (v16=%s))", vm.get("v5"), vm.get("v16")));
    if (!em.lt(vm.get("v0"), vm.get("v16"))) e.abort(String.format("!((v0=%s) lt (v16=%s))", vm.get("v0"), vm.get("v16")));
    if (!em.ge(vm.get("v1"), vm.get("v16"))) e.abort(String.format("!((v1=%s) ge (v16=%s))", vm.get("v1"), vm.get("v16")));
    if (!em.ge(vm.get("v6"), vm.get("v16"))) e.abort(String.format("!((v6=%s) ge (v16=%s))", vm.get("v6"), vm.get("v16")));
    if (!em.lt(vm.get("v1"), vm.get("v16"))) e.abort(String.format("!((v1=%s) lt (v16=%s))", vm.get("v1"), vm.get("v16")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="addFtplet", desc="(Ljava/lang/String;Lorg/apache/ftpserver/ftplet/Ftplet;)V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.elementData[*].name","this.ftplets.elementData[*].ftplet","this.ftplets.size"},delta={"this.ftplets.elementData[*]","this.ftplets.elementData[*].name","this.ftplets.elementData[*].ftplet"})
  @DG(pred={"_init_299"})
  static void addFtplet2669(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "ftplets"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("v1"), "size"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("v1"), "modCount"));
    vm.put("v5", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v6", vm.get("v5"));
    vm.put("v7", (int)10);
    vm.put("v8", (int)0);
    vm.put("v9", null);
    vm.put("v10", om.getField(vm.get("v1"), "elementData"));
    vm.put("v11", vm.get("v10"));
    vm.put("v12", om.getArrayLength(vm.get("v11")));
    vm.put("v13", vm.get("v12"));
    vm.put("v14", em.op("isub").eval(vm.get("v7"),vm.get("v13")));
    vm.put("v15", vm.get("v4"));
    vm.put("v16", em.op("isub").eval(vm.get("v15"),vm.get("v3")));
    om.revertField(vm.get("v1"), "modCount", vm.get("v16"));
    vm.put("v17", vm.get("v2"));
    vm.put("v18", em.op("isub").eval(vm.get("v17"),vm.get("v3")));
    om.revertField(vm.get("v1"), "size", vm.get("v18"));
    vm.put("v19", em.op("iadd").eval(vm.get("v18"),vm.get("v3")));
    vm.put("v20", om.popArrayElement(vm.get("v11"), vm.get("v18")));
    vm.put("v21", vm.get("v20"));
    vm.put("v22", om.popField(vm.get("v21"), "ftplet"));
    vm.put("param1", vm.get("v22"));
    vm.put("v23", om.popField(vm.get("v21"), "name"));
    vm.put("param0", vm.get("v23"));
    if (!em.le(vm.get("v14"), vm.get("v8"))) e.abort(String.format("!((v14=%s) le (v8=%s))", vm.get("v14"), vm.get("v8")));
    if (!em.ne(vm.get("param0"), vm.get("v9"))) e.abort(String.format("!((param0=%s) ne (v9=%s))", vm.get("param0"), vm.get("v9")));
    if (!em.ge(vm.get("v7"), vm.get("v19"))) e.abort(String.format("!((v7=%s) ge (v19=%s))", vm.get("v7"), vm.get("v19")));
    if (!em.ge(vm.get("v8"), vm.get("v18"))) e.abort(String.format("!((v8=%s) ge (v18=%s))", vm.get("v8"), vm.get("v18")));
    if (!em.eq(vm.get("v11"), vm.get("v6"))) e.abort(String.format("!((v11=%s) eq (v6=%s))", vm.get("v11"), vm.get("v6")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="addFtplet", desc="(Ljava/lang/String;Lorg/apache/ftpserver/ftplet/Ftplet;)V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.elementData[*].name","this.ftplets.elementData[*].ftplet","this.ftplets.size"},delta={"this.ftplets.elementData[*]","this.ftplets.elementData[*].name","this.ftplets.elementData[*].ftplet"})
  @DG(pred={"_init_299"})
  static void addFtplet2678(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", (int)1);
    vm.put("v3", om.getField(vm.get("this"), "ftplets"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", (int)0);
    vm.put("v6", null);
    vm.put("v7", om.getField(vm.get("v4"), "elementData"));
    vm.put("v8", vm.get("v7"));
    vm.put("v9", om.getField(vm.get("v4"), "size"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getArrayLength(vm.get("v8")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getField(vm.get("v4"), "modCount"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v10"),vm.get("v2")));
    om.revertField(vm.get("v4"), "size", vm.get("v15"));
    vm.put("v16", om.popArrayElement(vm.get("v8"), vm.get("v15")));
    vm.put("v17", vm.get("v16"));
    vm.put("v18", om.popField(vm.get("v17"), "name"));
    vm.put("param0", vm.get("v18"));
    vm.put("v19", om.popField(vm.get("v17"), "ftplet"));
    vm.put("param1", vm.get("v19"));
    vm.put("v20", em.op("iadd").eval(vm.get("v15"),vm.get("v2")));
    vm.put("v21", em.op("isub").eval(vm.get("v20"),vm.get("v12")));
    vm.put("v22", em.op("isub").eval(vm.get("v14"),vm.get("v2")));
    om.revertField(vm.get("v4"), "modCount", vm.get("v22"));
    if (!em.le(vm.get("v21"), vm.get("v5"))) e.abort(String.format("!((v21=%s) le (v5=%s))", vm.get("v21"), vm.get("v5")));
    if (!em.ne(vm.get("param0"), vm.get("v6"))) e.abort(String.format("!((param0=%s) ne (v6=%s))", vm.get("param0"), vm.get("v6")));
    if (!em.ge(vm.get("v5"), vm.get("v15"))) e.abort(String.format("!((v5=%s) ge (v15=%s))", vm.get("v5"), vm.get("v15")));
    if (!em.ne(vm.get("v8"), vm.get("v1"))) e.abort(String.format("!((v8=%s) ne (v1=%s))", vm.get("v8"), vm.get("v1")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="destroy", desc="()V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.size"})
  @DG(pred={"_init_299"})
  static void destroy3037(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", om.getField(vm.get("this"), "ftplets"));
    vm.put("v1", vm.get("v0"));
    vm.put("v2", om.getField(vm.get("v1"), "modCount"));
    vm.put("v3", (int)1);
    vm.put("v4", om.getField(vm.get("v1"), "size"));
    vm.put("v5", (int)0);
    if (!em.eq(vm.get("v5"), (vm.get("v4")))) e.abort("Inconsistent value for \"v5\": " + vm.get("v5") + " ne " + (vm.get("v4")));
    vm.put("v6", vm.get("v2"));
    vm.put("v7", em.op("isub").eval(vm.get("v6"),vm.get("v3")));
    om.revertField(vm.get("v1"), "modCount", vm.get("v7"));
    om.revertField(vm.get("v1"), "size");
    vm.put("v8", om.getField(vm.get("v1"), "size"));
    if (!em.ge(vm.get("v5"), vm.get("v8"))) e.abort(String.format("!((v5=%s) ge (v8=%s))", vm.get("v5"), vm.get("v8")));
    if (!em.ge(vm.get("v5"), vm.get("v8"))) e.abort(String.format("!((v5=%s) ge (v8=%s))", vm.get("v5"), vm.get("v8")));
  }
  @IM(clazz="org.apache.ftpserver.ftplet.DefaultFtpletContainer", name="addFtplet", desc="(Ljava/lang/String;Lorg/apache/ftpserver/ftplet/Ftplet;)V",revertedInput={"this.ftplets.modCount","this.ftplets.size"},definedOutput={"this.ftplets.modCount","this.ftplets.elementData[*]","this.ftplets.elementData[*].name","this.ftplets.elementData[*].ftplet","this.ftplets.size"},delta={"this.ftplets.elementData[*]","this.ftplets.elementData[*].name","this.ftplets.elementData[*].ftplet"})
  @DG(pred={"_init_299"})
  static void addFtplet2682(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {
    vm.put("this", om.getTarget());
    vm.put("v0", (int)1);
    vm.put("v1", om.getStatic("java.util.ArrayList.DEFAULTCAPACITY_EMPTY_ELEMENTDATA"));
    vm.put("v2", vm.get("v1"));
    vm.put("v3", om.getField(vm.get("this"), "ftplets"));
    vm.put("v4", vm.get("v3"));
    vm.put("v5", om.getField(vm.get("v4"), "modCount"));
    vm.put("v6", (int)10);
    vm.put("v7", (int)0);
    vm.put("v8", null);
    vm.put("v9", om.getField(vm.get("v4"), "elementData"));
    vm.put("v10", vm.get("v9"));
    vm.put("v11", om.getArrayLength(vm.get("v10")));
    vm.put("v12", vm.get("v11"));
    vm.put("v13", om.getField(vm.get("v4"), "size"));
    vm.put("v14", vm.get("v13"));
    vm.put("v15", em.op("isub").eval(vm.get("v6"),vm.get("v12")));
    vm.put("v16", vm.get("v5"));
    vm.put("v17", em.op("isub").eval(vm.get("v16"),vm.get("v0")));
    om.revertField(vm.get("v4"), "modCount", vm.get("v17"));
    vm.put("v18", em.op("isub").eval(vm.get("v14"),vm.get("v0")));
    om.revertField(vm.get("v4"), "size", vm.get("v18"));
    vm.put("v19", om.popArrayElement(vm.get("v10"), vm.get("v18")));
    vm.put("v20", vm.get("v19"));
    vm.put("v21", om.popField(vm.get("v20"), "ftplet"));
    vm.put("param1", vm.get("v21"));
    vm.put("v22", em.op("iadd").eval(vm.get("v18"),vm.get("v0")));
    vm.put("v23", om.popField(vm.get("v20"), "name"));
    vm.put("param0", vm.get("v23"));
    if (!em.eq(vm.get("v10"), vm.get("v2"))) e.abort(String.format("!((v10=%s) eq (v2=%s))", vm.get("v10"), vm.get("v2")));
    if (!em.le(vm.get("v15"), vm.get("v7"))) e.abort(String.format("!((v15=%s) le (v7=%s))", vm.get("v15"), vm.get("v7")));
    if (!em.eq(vm.get("param0"), vm.get("v8"))) e.abort(String.format("!((param0=%s) eq (v8=%s))", vm.get("param0"), vm.get("v8")));
    if (!em.ge(vm.get("v6"), vm.get("v22"))) e.abort(String.format("!((v6=%s) ge (v22=%s))", vm.get("v6"), vm.get("v22")));
  }
}
