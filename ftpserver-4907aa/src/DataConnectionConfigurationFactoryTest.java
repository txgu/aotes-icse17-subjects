import org.apache.ftpserver.DataConnectionConfigurationFactory;
import org.javelus.aotes.executor.AOTESTestCase;
import org.javelus.aotes.executor.AOTESTestRunner;

public class DataConnectionConfigurationFactoryTest {
    
    public static class Test1 extends AOTESTestCase<DataConnectionConfigurationFactory> {

        @Override
        protected DataConnectionConfigurationFactory buildInternal() {
            DataConnectionConfigurationFactory factory = new org.apache.ftpserver.DataConnectionConfigurationFactory();
            return factory;
        }

        @Override
        protected void checkInternal(DataConnectionConfigurationFactory updatedObject) throws Exception {
            int port = updatedObject.requestPassivePort();
            updatedObject.releasePassivePort(port);
        }
        
    }
    
    public static class Test2 extends AOTESTestCase<DataConnectionConfigurationFactory> {

        int port;
        @Override
        protected DataConnectionConfigurationFactory buildInternal() {
            DataConnectionConfigurationFactory factory = new org.apache.ftpserver.DataConnectionConfigurationFactory();
            port = factory.requestPassivePort();
            return factory;
        }

        @Override
        protected void checkInternal(DataConnectionConfigurationFactory updatedObject) throws Exception {
            // TODO Auto-generated method stub
            updatedObject.releasePassivePort(port);
            port = updatedObject.requestPassivePort();
            updatedObject.releasePassivePort(port);
        }
        
    }
    
    public static class Test3 extends AOTESTestCase<DataConnectionConfigurationFactory> {

        @Override
        protected DataConnectionConfigurationFactory buildInternal() {
            DataConnectionConfigurationFactory factory = new org.apache.ftpserver.DataConnectionConfigurationFactory();
            factory.setPassivePorts("10000-11000");
            return factory;
        }

        @Override
        protected void checkInternal(DataConnectionConfigurationFactory updatedObject) throws Exception {
            int port = updatedObject.requestPassivePort();
            updatedObject.releasePassivePort(port);
        }
        
    }
    
    public static class Test4 extends AOTESTestCase<DataConnectionConfigurationFactory> {

        int port;
        @Override
        protected DataConnectionConfigurationFactory buildInternal() {
            DataConnectionConfigurationFactory factory = new org.apache.ftpserver.DataConnectionConfigurationFactory();
            factory.setPassivePorts("10000-11000");
            port = factory.requestPassivePort();
            return factory;
        }

        @Override
        protected void checkInternal(DataConnectionConfigurationFactory updatedObject) throws Exception {
            updatedObject.releasePassivePort(port);
            port = updatedObject.requestPassivePort();
            updatedObject.releasePassivePort(port);
        }
    }
    
    public static void test() {
        org.apache.ftpserver.DataConnectionConfigurationFactory factory = new org.apache.ftpserver.DataConnectionConfigurationFactory();
        System.out.println(org.javelus.aotes.executor.java.JavaExecutorDriver.transform(factory));
        
    }
    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        AOTESTestRunner.run(
                Test1.class,
                Test2.class,
                Test3.class,
                Test4.class
                );
    }
}
